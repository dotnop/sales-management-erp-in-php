-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2019 at 10:10 AM
-- Server version: 10.2.27-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `softest8bdtask_branch_hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_account_name`
--

CREATE TABLE IF NOT EXISTS `acc_account_name` (
  `account_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) NOT NULL,
  `account_type` int(11) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acc_account_name`
--

INSERT INTO `acc_account_name` (`account_id`, `account_name`, `account_type`) VALUES
(1, 'Employee Salary', 0),
(3, 'Example', 1),
(4, 'Loan Expense', 0),
(5, 'Loan Income', 1);

-- --------------------------------------------------------

--
-- Table structure for table `acc_coa`
--

CREATE TABLE IF NOT EXISTS `acc_coa` (
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UpdateDate` datetime NOT NULL,
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`HeadName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_coa`
--

INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `branch_id`) VALUES
('502020201', '0-Mdsu', 'Manufacturer Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-26 15:17:22', '', '0000-00-00 00:00:00', 1),
('502020302', '1-Md Lalon', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-31 07:04:47', '', '0000-00-00 00:00:00', NULL),
('502020301', '1-Md Nuruddin', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-29 08:15:59', '', '0000-00-00 00:00:00', NULL),
('502020309', '10-jade ', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-09 12:48:28', '', '0000-00-00 00:00:00', NULL),
('502020310', '11-jade ', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-09 12:48:28', '', '0000-00-00 00:00:00', NULL),
('502020310', '12-Al Mahmud', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-11 06:00:28', '', '0000-00-00 00:00:00', NULL),
('102030200040', '121233043131426-c-xc', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-03-13 05:21:51', '', '0000-00-00 00:00:00', NULL),
('102030200042', '121233043131426-fgdf-fg', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-03-13 05:41:19', '', '0000-00-00 00:00:00', NULL),
('102030200041', '121233043131426-Md-Tuhin', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-03-13 05:24:45', '', '0000-00-00 00:00:00', NULL),
('102030200039', '121233043131426-Merry-Trumb', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-03-13 05:16:49', '', '0000-00-00 00:00:00', NULL),
('102030200043', '121233043131426-xcv-zxc', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-03-13 05:47:36', '', '0000-00-00 00:00:00', NULL),
('502020310', '13-wajeeh kazmi', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-11 08:49:08', '', '0000-00-00 00:00:00', NULL),
('502020310', '14-Ishaq sorkar', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-12 08:45:17', '', '0000-00-00 00:00:00', NULL),
('502020310', '15-S.M Faruk', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-12 11:24:04', '', '0000-00-00 00:00:00', NULL),
('502020310', '16-tan zil', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-15 10:32:38', '', '0000-00-00 00:00:00', NULL),
('502020310', '17-Sm Isahaq', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-15 13:23:30', '', '0000-00-00 00:00:00', NULL),
('502020310', '18-Sumch Mohammad Tarek', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 07:17:20', '', '0000-00-00 00:00:00', NULL),
('502020310', '19-Sumch Mohammad Tarek', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 07:18:00', '', '0000-00-00 00:00:00', NULL),
('502020303', '2-Md Lalon', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-31 07:04:47', '', '0000-00-00 00:00:00', NULL),
('502020310', '20-Sumch Mohammad Tarek', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 07:20:45', '', '0000-00-00 00:00:00', NULL),
('502020310', '21-jakir mia', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 07:50:09', '', '0000-00-00 00:00:00', NULL),
('502020310', '22-hhh sdfsdf', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 07:52:55', '', '0000-00-00 00:00:00', NULL),
('502020310', '23-tuhhin Mia', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 07:55:24', '', '0000-00-00 00:00:00', NULL),
('502020310', '24-kkk ', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 07:57:22', '', '0000-00-00 00:00:00', NULL),
('502020310', '25-ooo ', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 08:00:50', '', '0000-00-00 00:00:00', NULL),
('502020310', '26-ishaq hossain', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-16 08:03:38', '', '0000-00-00 00:00:00', NULL),
('502020304', '3-ss 23423', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-31 07:07:05', '', '0000-00-00 00:00:00', NULL),
('502020100002', '38-Sharon-Wooten', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-01-26 10:18:50', '', '0000-00-00 00:00:00', NULL),
('502020100001', '39-Joe-keller', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-01-26 10:20:46', '', '0000-00-00 00:00:00', NULL),
('502020305', '4-ss 23423', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-31 07:07:05', '', '0000-00-00 00:00:00', NULL),
('502020100003', '40-Kevin-Wright', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-01-26 10:21:29', '', '0000-00-00 00:00:00', NULL),
('502020109', '48-John-Carry', 'Employee Payable', 4, 1, 0, 1, 'L', 0, 0, 0.00, '2', '2019-01-26 10:25:40', '', '0000-00-00 00:00:00', NULL),
('502020110', '50-James-Smith', 'Employee Payable', 4, 1, 0, 1, 'L', 0, 0, 0.00, '2', '2019-01-26 10:26:29', '', '0000-00-00 00:00:00', NULL),
('502020202', '6-saiful Islam', 'Manufacturer Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-26 15:19:53', '', '0000-00-00 00:00:00', 1),
('502020100004', '67-Kaitlyn-Campion', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-01-26 10:22:04', '', '0000-00-00 00:00:00', NULL),
('102030200044', '67567ghfghf-Md-Tuhin', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-03-13 06:11:49', '', '0000-00-00 00:00:00', NULL),
('502020100005', '69-Jhon-William', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-01-13 13:37:25', '', '0000-00-00 00:00:00', NULL),
('502020306', '7-Md shofiqul', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-31 08:58:21', '', '0000-00-00 00:00:00', NULL),
('502020100006', '70-Sumon-Ahmed', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-01-15 14:06:41', '', '0000-00-00 00:00:00', NULL),
('502020100007', '71-Shahriar-Kabir', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-01-21 12:12:23', '', '0000-00-00 00:00:00', NULL),
('502020100008', '72-Sayan-Rahman', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '71', '2019-01-21 12:17:00', '', '0000-00-00 00:00:00', NULL),
('502020111', '74-Md-Kuddus', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-22 11:23:43', '', '0000-00-00 00:00:00', NULL),
('502020112', '75-Hm-Isahaq', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-08-22 11:39:08', '', '0000-00-00 00:00:00', NULL),
('502020113', '77-Tanzil-Ahmad', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-07 11:05:47', '', '0000-00-00 00:00:00', 0),
('502020114', '78-kawser-Ahmad', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-07 13:43:52', '', '0000-00-00 00:00:00', 1),
('502020115', '79-drwaj-drkaz', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-09 12:52:20', '', '0000-00-00 00:00:00', 0),
('502020307', '8-jade ', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-09 12:47:45', '', '0000-00-00 00:00:00', NULL),
('502020116', '81-waj-kaz', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-11 08:43:25', '', '0000-00-00 00:00:00', 0),
('502020117', '83-Sumch Mohammad-Tarek', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-12 08:37:30', '', '0000-00-00 00:00:00', 0),
('502020308', '9-jade ', 'Referrer Ledger', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-09 12:47:45', '', '0000-00-00 00:00:00', NULL),
('502020118', '97-waj-kaz', 'Employee Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, '2', '2019-09-17 22:48:37', '', '0000-00-00 00:00:00', 0),
('4021403', 'AC', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:33:55', '', '0000-00-00 00:00:00', NULL),
('50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, 0.00, 'admin', '2015-10-15 19:50:43', '', '0000-00-00 00:00:00', NULL),
('10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, 0.00, '', '0000-00-00 00:00:00', 'admin', '2013-09-18 15:29:35', NULL),
('1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, 0.00, 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32', NULL),
('102020103', 'Advance House Rent', 'Advance', 4, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-10-02 16:55:38', 'admin', '2016-10-02 16:57:32', NULL),
('10202', 'Advance, Deposit And Pre-payments', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, 0.00, '', '0000-00-00 00:00:00', 'admin', '2015-12-31 16:46:24', NULL),
('4020602', 'Advertisement and Publicity', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:51:44', '', '0000-00-00 00:00:00', NULL),
('1010410', 'Air Cooler', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-05-23 12:13:55', '', '0000-00-00 00:00:00', NULL),
('4020603', 'AIT Against Advertisement', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:52:09', '', '0000-00-00 00:00:00', NULL),
('1', 'Assets', 'Chart Of Accounts', 0, 1, 0, 0, 'A', 0, 0, 0.00, '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', NULL),
('1010204', 'Attendance Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:49:31', '', '0000-00-00 00:00:00', NULL),
('40216', 'Audit Fee', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, 0.00, 'admin', '2017-07-18 12:54:30', '', '0000-00-00 00:00:00', NULL),
('102010202', 'Bank Asia', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 08:50:05', '', '0000-00-00 00:00:00', NULL),
('4021002', 'Bank Charge', 'Financial Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:21:03', '', '0000-00-00 00:00:00', NULL),
('30203', 'Bank Interest', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, 0.00, 'Obaidul', '2015-01-03 14:49:54', 'admin', '2016-09-25 11:04:19', NULL),
('1010104', 'Book Shelf', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:46:11', '', '0000-00-00 00:00:00', NULL),
('1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:45:37', '', '0000-00-00 00:00:00', NULL),
('4020604', 'Business Development Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:52:29', '', '0000-00-00 00:00:00', NULL),
('4020606', 'Campaign Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:52:57', 'admin', '2016-09-19 14:52:48', NULL),
('4020502', 'Campus Rent', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:46:53', 'admin', '2017-04-27 17:02:39', NULL),
('40212', 'Car Running Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:28:43', '', '0000-00-00 00:00:00', NULL),
('10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, 0.00, '', '0000-00-00 00:00:00', 'admin', '2015-10-15 15:57:55', NULL),
('1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 0, 'A', 0, 0, 0.00, '2', '2018-07-19 13:43:59', 'admin', '2015-10-15 15:32:42', NULL),
('1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 1, 'A', 0, 0, 0.00, '2', '2018-07-31 12:56:28', 'admin', '2016-05-23 12:05:43', NULL),
('30101', 'Cash Sale', 'Store Income', 1, 1, 1, 1, 'I', 0, 0, 0.00, '2', '2018-07-08 07:51:26', '', '0000-00-00 00:00:00', NULL),
('1010207', 'CCTV', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:51:24', '', '0000-00-00 00:00:00', NULL),
('102020102', 'CEO Current A/C', 'Advance', 4, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-09-25 11:54:54', '', '0000-00-00 00:00:00', NULL),
('1010101', 'Class Room Chair', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:45:29', '', '0000-00-00 00:00:00', NULL),
('4021407', 'Close Circuit Cemera', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:35:35', '', '0000-00-00 00:00:00', NULL),
('4020601', 'Commision on Admission', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:51:21', 'admin', '2016-09-19 14:42:54', NULL),
('1010206', 'Computer', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:51:09', '', '0000-00-00 00:00:00', NULL),
('4021410', 'Computer (R)', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'Zoherul', '2016-03-24 12:38:52', 'Zoherul', '2016-03-24 12:41:40', NULL),
('1010102', 'Computer Table', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:45:44', '', '0000-00-00 00:00:00', NULL),
('301020401', 'Continuing Registration fee - UoL (Income)', 'Registration Fee (UOL) Income', 4, 1, 1, 0, 'I', 0, 0, 0.00, 'admin', '2015-10-15 17:40:40', '', '0000-00-00 00:00:00', NULL),
('4020904', 'Contratuall Staff Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:12:34', '', '0000-00-00 00:00:00', NULL),
('403', 'Cost of Sale', 'Expence', 0, 1, 1, 0, 'E', 0, 0, 0.00, '2', '2018-07-08 10:37:16', '', '0000-00-00 00:00:00', NULL),
('30102', 'Credit Sale', 'Store Income', 1, 1, 1, 1, 'I', 0, 0, 0.00, '2', '2018-07-08 07:51:34', '', '0000-00-00 00:00:00', NULL),
('4020709', 'Cultural Expense', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'nasmud', '2017-04-29 12:45:10', '', '0000-00-00 00:00:00', NULL),
('102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, 0.00, '', '0000-00-00 00:00:00', 'admin', '2018-07-07 11:23:00', NULL),
('502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, 0.00, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21', NULL),
('40100002', 'cw-Chichawatni', 'Store Expenses', 2, 1, 1, 0, 'E', 0, 0, 0.00, '2', '2018-08-02 16:30:41', '', '0000-00-00 00:00:00', NULL),
('102010204', 'Default Bank', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-20 08:15:42', '', '0000-00-00 00:00:00', NULL),
('1020202', 'Deposit', 'Advance, Deposit And Pre-payments', 3, 1, 0, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:40:42', '', '0000-00-00 00:00:00', NULL),
('4020605', 'Design & Printing Expense', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:55:00', '', '0000-00-00 00:00:00', NULL),
('4020404', 'Dish Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:58:21', '', '0000-00-00 00:00:00', NULL),
('40215', 'Dividend', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, 0.00, 'admin', '2016-09-25 14:07:55', '', '0000-00-00 00:00:00', NULL),
('4020403', 'Drinking Water Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:58:10', '', '0000-00-00 00:00:00', NULL),
('1010211', 'DSLR Camera', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:53:17', 'admin', '2016-01-02 16:23:25', NULL),
('4020908', 'Earned Leave', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:13:38', '', '0000-00-00 00:00:00', NULL),
('4020607', 'Education Fair Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:53:42', '', '0000-00-00 00:00:00', NULL),
('1010602', 'Electric Equipment', 'Electrical Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:44:51', '', '0000-00-00 00:00:00', NULL),
('1010203', 'Electric Kettle', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:49:07', '', '0000-00-00 00:00:00', NULL),
('10106', 'Electrical Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:43:44', '', '0000-00-00 00:00:00', NULL),
('4020407', 'Electricity Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:59:31', '', '0000-00-00 00:00:00', NULL),
('10202010501', 'employ', 'Salary', 5, 1, 0, 0, 'A', 0, 0, 0.00, 'admin', '2018-07-05 11:47:10', '', '0000-00-00 00:00:00', NULL),
('5020201', 'Employee Payable', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, 0.00, '2', '2019-01-07 10:16:12', '', '0000-00-00 00:00:00', NULL),
('1020301', 'Employee Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, 0.00, '2', '2018-10-17 11:13:45', 'admin', '2018-07-07 12:31:42', NULL),
('40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, 0.00, 'admin', '2013-07-08 16:21:26', 'anwarul', '2013-07-17 14:21:47', NULL),
('2', 'Equity', 'Chart Of Accounts', 0, 1, 0, 0, 'L', 0, 0, 0.00, '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', NULL),
('4', 'Expence', 'Chart Of Accounts', 0, 1, 0, 0, 'E', 0, 0, 0.00, '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', NULL),
('4020903', 'Faculty,Staff Salary & Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:12:21', '', '0000-00-00 00:00:00', NULL),
('4021404', 'Fax Machine', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:34:15', '', '0000-00-00 00:00:00', NULL),
('4020905', 'Festival & Incentive Bonus', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:12:48', '', '0000-00-00 00:00:00', NULL),
('1010103', 'File Cabinet', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:46:02', '', '0000-00-00 00:00:00', NULL),
('40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36', NULL),
('1010403', 'Fire Extingushier', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:39:32', '', '0000-00-00 00:00:00', NULL),
('4021408', 'Furniture', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:35:47', '', '0000-00-00 00:00:00', NULL),
('10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, 0.00, 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40', NULL),
('4020406', 'Gas Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:59:20', '', '0000-00-00 00:00:00', NULL),
('20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, 0.00, 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49', NULL),
('10105', 'Generator', 'Non Current Assets', 2, 1, 1, 1, 'A', 0, 0, 0.00, 'Zoherul', '2016-02-27 16:02:35', 'admin', '2016-05-23 12:05:18', NULL),
('4021414', 'Generator Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'Zoherul', '2016-06-16 10:21:05', '', '0000-00-00 00:00:00', NULL),
('40213', 'Generator Running Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:29:29', '', '0000-00-00 00:00:00', NULL),
('10103', 'Groceries and Cutleries', 'Non Current Assets', 2, 1, 1, 1, 'A', 0, 0, 0.00, '2', '2018-07-12 10:02:55', '', '0000-00-00 00:00:00', NULL),
('1010408', 'Gym Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:46:03', '', '0000-00-00 00:00:00', NULL),
('4020907', 'Honorarium', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:13:26', '', '0000-00-00 00:00:00', NULL),
('40205', 'House Rent', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'anwarul', '2013-08-24 10:26:56', '', '0000-00-00 00:00:00', NULL),
('40100001', 'HP-Hasilpur', 'Academic Expenses', 2, 1, 1, 0, 'E', 0, 0, 0.00, '2', '2018-07-29 03:44:23', '', '0000-00-00 00:00:00', NULL),
('4020702', 'HR Recruitment Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2016-09-25 12:55:49', '', '0000-00-00 00:00:00', NULL),
('4020703', 'Incentive on Admission', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2016-09-25 12:56:09', '', '0000-00-00 00:00:00', NULL),
('3', 'Income', 'Chart Of Accounts', 0, 1, 0, 0, 'I', 0, 0, 0.00, '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', NULL),
('30204', 'Income from Photocopy & Printing', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, 0.00, 'Zoherul', '2015-07-14 10:29:54', 'admin', '2016-09-25 11:04:28', NULL),
('5020302', 'Income Tax Payable', 'Liabilities for Expenses', 3, 1, 0, 1, 'L', 0, 0, 0.00, 'admin', '2016-09-19 11:18:17', 'admin', '2016-09-28 13:18:35', NULL),
('102020302', 'Insurance Premium', 'Prepayment', 4, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-09-19 13:10:57', '', '0000-00-00 00:00:00', NULL),
('4021001', 'Interest on Loan', 'Financial Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:20:53', 'admin', '2016-09-19 14:53:34', NULL),
('4020401', 'Internet Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:56:55', 'admin', '2015-10-15 18:57:32', NULL),
('10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, 0.00, '2', '2018-07-07 15:21:58', '', '0000-00-00 00:00:00', NULL),
('10205010101', 'Jahangir', 'Hasan', 1, 1, 0, 0, 'A', 0, 0, 0.00, '2', '2018-07-07 10:40:56', '', '0000-00-00 00:00:00', NULL),
('1010210', 'LCD TV', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:52:27', '', '0000-00-00 00:00:00', NULL),
('30103', 'Lease Sale', 'Store Income', 1, 1, 1, 1, 'I', 0, 0, 0.00, '2', '2018-07-08 07:51:52', '', '0000-00-00 00:00:00', NULL),
('5', 'Liabilities', 'Chart Of Accounts', 0, 1, 0, 0, 'L', 0, 0, 0.00, 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54', NULL),
('50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, 0.00, 'admin', '2015-10-15 19:50:59', '', '0000-00-00 00:00:00', NULL),
('4020707', 'Library Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2017-01-10 15:34:54', '', '0000-00-00 00:00:00', NULL),
('4021409', 'Lift', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:36:12', '', '0000-00-00 00:00:00', NULL),
('50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, 0.00, 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40', NULL),
('5020202', 'Manufacturer Payable', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, 0.00, '2', '2019-08-26 14:53:57', '', '0000-00-00 00:00:00', NULL),
('4020608', 'Marketing & Promotion Exp.', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:53:59', '', '0000-00-00 00:00:00', NULL),
('4020901', 'Medical Allowance', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:11:33', '', '0000-00-00 00:00:00', NULL),
('1010411', 'Metal Ditector', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'Zoherul', '2016-08-22 10:55:22', '', '0000-00-00 00:00:00', NULL),
('4021413', 'Micro Oven', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'Zoherul', '2016-05-12 14:53:51', '', '0000-00-00 00:00:00', NULL),
('30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, 0.00, 'anwarul', '2014-02-06 15:26:31', 'admin', '2016-09-25 11:04:35', NULL),
('4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:13:53', '', '0000-00-00 00:00:00', NULL),
('4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2016-09-25 12:54:39', '', '0000-00-00 00:00:00', NULL),
('40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19', NULL),
('1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-01-29 10:43:30', '', '0000-00-00 00:00:00', NULL),
('1010212', 'Network Accessories', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-01-02 16:23:32', '', '0000-00-00 00:00:00', NULL),
('4020408', 'News Paper Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2016-01-02 15:55:57', '', '0000-00-00 00:00:00', NULL),
('101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, 0.00, '', '0000-00-00 00:00:00', 'admin', '2015-10-15 15:29:11', NULL),
('501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, 0.00, 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21', NULL),
('1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:40:02', '', '0000-00-00 00:00:00', NULL),
('10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, 0.00, 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21', NULL),
('4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:33:15', '', '0000-00-00 00:00:00', NULL),
('30201', 'Office Stationary (Income)', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, 0.00, 'anwarul', '2013-07-17 15:21:06', 'admin', '2016-09-25 11:04:50', NULL),
('402', 'Other Expenses', 'Expence', 1, 1, 0, 0, 'E', 0, 0, 0.00, '2', '2018-07-07 14:00:16', 'admin', '2015-10-15 18:37:42', NULL),
('302', 'Other Income', 'Income', 1, 1, 0, 0, 'I', 0, 0, 0.00, '2', '2018-07-07 13:40:57', 'admin', '2016-09-25 11:04:09', NULL),
('40211', 'Others (Non Academic Expenses)', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'Obaidul', '2014-12-03 16:05:42', 'admin', '2015-10-15 19:22:09', NULL),
('30205', 'Others (Non-Academic Income)', 'Other Income', 2, 1, 0, 1, 'I', 0, 0, 0.00, 'admin', '2015-10-15 17:23:49', 'admin', '2015-10-15 17:57:52', NULL),
('10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, 0.00, 'admin', '2016-01-29 10:43:16', '', '0000-00-00 00:00:00', NULL),
('4020910', 'Outstanding Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'Zoherul', '2016-04-24 11:56:50', '', '0000-00-00 00:00:00', NULL),
('4021405', 'Oven', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:34:31', '', '0000-00-00 00:00:00', NULL),
('102030200004', 'P08MP6WQ-Roger -Haywood', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 10:08:23', '', '0000-00-00 00:00:00', NULL),
('102030200034', 'P0IK5GYT-Kelly-A. Bodine', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 04:18:47', '', '0000-00-00 00:00:00', NULL),
('102030200014', 'P0PMGAQE-Rosie-A. Kelly', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:34:26', '', '0000-00-00 00:00:00', NULL),
('102030200063', 'P1AKIU0O-sss-sfdsf', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 11:53:08', '', '0000-00-00 00:00:00', 2),
('102030200024', 'P1WY4YCU-Maurice-M. Atchison', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:45:50', '', '0000-00-00 00:00:00', NULL),
('102030200013', 'P25LF5A8-Howard-M. McCoy', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:32:35', '', '0000-00-00 00:00:00', NULL),
('102030200071', 'P2QWC5UN-Tanzil-bdtask', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '82', '2019-09-11 08:55:32', '', '0000-00-00 00:00:00', 0),
('102030200025', 'P45E9QXP-Michael-N. Hayes', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:47:17', '', '0000-00-00 00:00:00', NULL),
('102030200066', 'P4M5A5XZ-kh-golam', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 12:50:24', '', '0000-00-00 00:00:00', 2),
('102030200035', 'P4MHVMZT-Geraldine-R. Flores', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 04:19:37', '', '0000-00-00 00:00:00', NULL),
('102030200007', 'P4PCCAGG-Rick-Thompson', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 10:14:36', '', '0000-00-00 00:00:00', NULL),
('102030200036', 'P5J949EY-Wanda-R. Flores', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 04:20:31', '', '0000-00-00 00:00:00', NULL),
('102030200051', 'P60FTN40-js-Prince', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-08-22 12:14:46', '', '0000-00-00 00:00:00', NULL),
('102030200005', 'P69YN5VJ-Ashraf -Rahman', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 10:10:02', '', '0000-00-00 00:00:00', NULL),
('102030200028', 'P6J76U0K-Daniel-C. Haskins', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:52:31', '', '0000-00-00 00:00:00', NULL),
('102030200033', 'P6WFT0SK-Otis-I. Levine', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 04:01:20', '', '0000-00-00 00:00:00', NULL),
('102030200048', 'P736D262-Wemimo-Aina', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-07-24 13:14:56', '', '0000-00-00 00:00:00', NULL),
('102030200058', 'P73WX7YS-Js-Rafiq', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 11:02:56', '', '0000-00-00 00:00:00', NULL),
('102030200021', 'P7YM5ZSG-Colleen-R. Matos', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-27 03:34:14', '', '0000-00-00 00:00:00', NULL),
('102030200027', 'P86DHOFT-Celeste-G. Brown', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:50:09', '', '0000-00-00 00:00:00', NULL),
('102030200056', 'P8F1PZHH-Sm-Mosiur', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-09-04 13:55:29', '', '0000-00-00 00:00:00', 1),
('102030200067', 'P8OMUDK3-Md-Babul', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-09-07 13:28:28', '', '0000-00-00 00:00:00', 1),
('102030200070', 'P8ZCQMXI-adding -patient', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '39', '2019-09-09 13:00:29', '', '0000-00-00 00:00:00', 1),
('102030200038', 'P8ZQ54X8-vxc-xcv', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-02-26 07:49:19', '', '0000-00-00 00:00:00', NULL),
('102030200012', 'P90KREP7-Howard-M. McCoy', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:32:34', '', '0000-00-00 00:00:00', NULL),
('4021412', 'PABX-Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'Zoherul', '2016-04-24 14:40:18', '', '0000-00-00 00:00:00', NULL),
('4020902', 'Part-time Staff Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:12:06', '', '0000-00-00 00:00:00', NULL),
('102030200069', 'PAT1LAGN-Touhidul -Islam', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-08 09:47:06', '', '0000-00-00 00:00:00', 2),
('1020302', 'Patient Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, 0.00, '2', '2019-01-07 10:00:42', '', '0000-00-00 00:00:00', NULL),
('102030200052', 'PBQFL5ZD-Md-Su', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-08-22 14:27:52', '', '0000-00-00 00:00:00', NULL),
('102030200062', 'PC7EK6QR-Karim-Mubarak', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 11:48:42', '', '0000-00-00 00:00:00', 2),
('102030200064', 'PCLY6LSX-kkk-234234', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 11:58:03', '', '0000-00-00 00:00:00', 2),
('102030200068', 'PCQ2JONU-Km-Habib', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '78', '2019-09-08 05:41:14', '', '0000-00-00 00:00:00', 2),
('102030200019', 'PDDLXVWI-Peter-S. Leu', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:43:55', '', '0000-00-00 00:00:00', NULL),
('102030200053', 'PDMCKTYV-js-malek', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-08-25 07:58:17', '', '0000-00-00 00:00:00', NULL),
('102030200022', 'PDQQ75PP-George -A. White', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-27 03:35:20', '', '0000-00-00 00:00:00', NULL),
('102030200046', 'PECW0N53-Md-Tuhin', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-03-13 07:13:11', '', '0000-00-00 00:00:00', NULL),
('102030200010', 'PEMTVO2A-Derrick-M. Rigney', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:25:58', '', '0000-00-00 00:00:00', NULL),
('102030200029', 'PEPWILVC-Richard-A. Hamel', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:57:00', '', '0000-00-00 00:00:00', NULL),
('102030200037', 'PFKH0JGR-vxcv-cxvx', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-02-26 07:15:08', '', '0000-00-00 00:00:00', NULL),
('102030200073', 'PFR7U0FN-Rahim-Khan', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '84', '2019-09-12 08:48:20', '', '0000-00-00 00:00:00', 4),
('102030200016', 'PFTBOJTF-Donald-M. Smith', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:37:03', '', '0000-00-00 00:00:00', NULL),
('102030200017', 'PFZ6JDKE-James-B. Roberts', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:39:54', '', '0000-00-00 00:00:00', NULL),
('102030200075', 'PG0VW841-2nd patient-kaz', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '86', '2019-09-15 10:42:47', '', '0000-00-00 00:00:00', 3),
('102030200001', 'PG1E53IT-Marco-S. Scott', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 10:02:54', '', '0000-00-00 00:00:00', NULL),
('102030200050', 'PGQG6XSK-Oladele-Shede', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-07-24 13:35:47', '', '0000-00-00 00:00:00', NULL),
('1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40', NULL),
('4021411', 'Photocopy Machine Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'Zoherul', '2016-04-24 12:40:02', 'admin', '2017-04-27 17:03:17', NULL),
('102030200055', 'PHSMK844-Kobir -Ahmad', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '76', '2019-09-04 08:21:54', '', '0000-00-00 00:00:00', 0),
('102030200078', 'PI27P99N-new-patient', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-09-16 13:11:02', '', '0000-00-00 00:00:00', 3),
('102030200049', 'PI62NRWQ-Wemimo-Aina', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-07-24 13:29:27', '', '0000-00-00 00:00:00', NULL),
('102030200030', 'PJ916RXG-Thomas-A. Hamel', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:57:46', '', '0000-00-00 00:00:00', NULL),
('102030200061', 'PJDW95W3-sm-Shahabsfduddin', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 11:36:26', '', '0000-00-00 00:00:00', NULL),
('102030200032', 'PK5L3G3G-Michael- R. Vega', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:59:59', '', '0000-00-00 00:00:00', NULL),
('102030200002', 'PK9QNQ5E-John-Glasper', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 10:05:04', '', '0000-00-00 00:00:00', NULL),
('102030200003', 'PKK4W6QT-Lourdes -Eubanks', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 10:06:06', '', '0000-00-00 00:00:00', NULL),
('102030200059', 'PMK5OHQF-Md-Su', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 11:33:32', '', '0000-00-00 00:00:00', NULL),
('102030200026', 'PMTGGP7A-Jose-S. Low', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:48:21', '', '0000-00-00 00:00:00', NULL),
('102030200011', 'PN8DKMJG-Otto-J. Voigt', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:31:11', '', '0000-00-00 00:00:00', NULL),
('102030200047', 'PO6P4DWW-Walking -Patient', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-07-23 08:01:30', '', '0000-00-00 00:00:00', NULL),
('102030200054', 'PP37NOT8-Suvo-sm', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '76', '2019-08-31 13:37:54', '', '0000-00-00 00:00:00', 0),
('3020503', 'Practical Fee', 'Others (Non-Academic Income)', 3, 1, 1, 1, 'I', 0, 0, 0.00, 'admin', '2017-07-22 18:00:37', '', '0000-00-00 00:00:00', NULL),
('1020203', 'Prepayment', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:40:51', 'admin', '2015-12-31 16:49:58', NULL),
('1010201', 'Printer', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:47:15', '', '0000-00-00 00:00:00', NULL),
('40202', 'Printing and Stationary', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, 0.00, 'admin', '2013-07-08 16:21:45', 'admin', '2016-09-19 14:39:32', NULL),
('3020502', 'Professional Training Course(Oracal-1)', 'Others (Non-Academic Income)', 3, 1, 1, 0, 'I', 0, 0, 0.00, 'nasim', '2017-06-22 13:28:05', '', '0000-00-00 00:00:00', NULL),
('30207', 'Professional Training Course(Oracal)', 'Other Income', 2, 1, 0, 1, 'I', 0, 0, 0.00, 'nasim', '2017-06-22 13:24:16', 'nasim', '2017-06-22 13:25:56', NULL),
('1010208', 'Projector', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:51:44', '', '0000-00-00 00:00:00', NULL),
('40206', 'Promonational Expence', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'anwarul', '2013-07-11 13:48:57', 'anwarul', '2013-07-17 14:23:03', NULL),
('102030200023', 'PRQXXD9Q-Solomon-B. Boston', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:44:12', '', '0000-00-00 00:00:00', NULL),
('102030200074', 'PS5U4FVL-patient-referred', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '86', '2019-09-15 10:37:34', '', '0000-00-00 00:00:00', 3),
('102030200057', 'PSYTJFD7-js-Jahangir', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-09-04 13:57:52', '', '0000-00-00 00:00:00', 1),
('102030200006', 'PTHITYJC-Linda -Logan', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 10:11:59', '', '0000-00-00 00:00:00', NULL),
('102030200020', 'PUUVC1CY-Hilda-P. Lambert', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-27 03:31:52', '', '0000-00-00 00:00:00', NULL),
('102030200065', 'PUW40T0G-jj-Rawling', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 12:03:55', '', '0000-00-00 00:00:00', 2),
('102030200031', 'PV5KRZJ1-Troy-A. Hamel', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-27 03:58:46', '', '0000-00-00 00:00:00', NULL),
('102030200077', 'PWKOEB8U-dfgdf-dgfhdfgh', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '96', '2019-09-16 08:05:12', '', '0000-00-00 00:00:00', 3),
('102030200008', 'PX4HE1WM-Billy -Diehl', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 10:16:24', '', '0000-00-00 00:00:00', NULL),
('102030200072', 'PXL3B2DJ-patientref-bdtask', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '82', '2019-09-11 09:29:25', '', '0000-00-00 00:00:00', 0),
('102030200018', 'PY55TSNJ-Mark-N. Jeske', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:40:58', '', '0000-00-00 00:00:00', NULL),
('102030200015', 'PY5G7FNA-Richard-V. Welch', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-01-26 13:35:40', '', '0000-00-00 00:00:00', NULL),
('102030200076', 'PZ1A14D7-Rafiq-hossain', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '88', '2019-09-16 07:23:03', '', '0000-00-00 00:00:00', 3),
('102030200060', 'PZ685B4I-sm-Shahabuddin', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-09-07 11:34:45', '', '0000-00-00 00:00:00', NULL),
('102030200009', 'PZRIMPJ4-Christopher-C. Salmon', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '0', '2019-01-26 10:53:20', '', '0000-00-00 00:00:00', NULL),
('5020203', 'Referrer Ledger', 'Account Payable', 3, 1, 0, 1, 'L', 0, 0, 0.00, '2', '2019-08-29 07:08:18', '', '0000-00-00 00:00:00', NULL),
('40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:32:46', '', '0000-00-00 00:00:00', NULL),
('202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, 0.00, 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57', NULL),
('20102', 'Retained Earnings', 'Share Holders Equity', 2, 1, 1, 1, 'L', 0, 0, 0.00, 'admin', '2016-05-23 11:20:40', 'admin', '2016-09-25 14:05:06', NULL),
('4020708', 'River Cruse', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2017-04-24 15:35:25', '', '0000-00-00 00:00:00', NULL),
('102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, 0.00, 'admin', '2018-07-05 11:46:44', '', '0000-00-00 00:00:00', NULL),
('40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'anwarul', '2013-12-12 11:22:58', '', '0000-00-00 00:00:00', NULL),
('404', 'Sale Discount', 'Expence', 1, 1, 1, 0, 'E', 0, 0, 0.00, '2', '2018-07-19 10:15:11', '', '0000-00-00 00:00:00', NULL),
('1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:41:30', '', '0000-00-00 00:00:00', NULL),
('20101', 'Share Capital', 'Share Holders Equity', 2, 1, 0, 1, 'L', 0, 0, 0.00, 'anwarul', '2013-12-08 19:37:32', 'admin', '2015-10-15 19:45:35', NULL),
('201', 'Share Holders Equity', 'Equity', 1, 1, 0, 0, 'L', 0, 0, 0.00, '', '0000-00-00 00:00:00', 'admin', '2015-10-15 19:43:51', NULL),
('50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, 0.00, 'admin', '2015-10-15 19:50:30', '', '0000-00-00 00:00:00', NULL),
('40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'anwarul', '2013-11-21 14:13:01', 'admin', '2015-10-15 19:02:51', NULL),
('4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:13:13', '', '0000-00-00 00:00:00', NULL),
('50102', 'Sponsors Loan', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, 0.00, 'admin', '2015-10-15 19:48:02', '', '0000-00-00 00:00:00', NULL),
('4020706', 'Sports Expense', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'nasmud', '2016-11-09 13:16:53', '', '0000-00-00 00:00:00', NULL),
('401', 'Store Expenses', 'Expence', 1, 1, 0, 0, 'E', 0, 0, 0.00, '2', '2018-07-07 13:38:59', 'admin', '2015-10-15 17:58:46', NULL),
('301', 'Store Income', 'Income', 1, 1, 0, 0, 'I', 0, 0, 0.00, '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02', NULL),
('3020501', 'Students Info. Correction Fee', 'Others (Non-Academic Income)', 3, 1, 1, 0, 'I', 0, 0, 0.00, 'admin', '2015-10-15 17:24:45', '', '0000-00-00 00:00:00', NULL),
('1010601', 'Sub Station', 'Electrical Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:44:11', '', '0000-00-00 00:00:00', NULL),
('4020704', 'TB Care Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2016-10-08 13:03:04', '', '0000-00-00 00:00:00', NULL),
('30206', 'TB Care Income', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, 0.00, 'admin', '2016-10-08 13:00:56', '', '0000-00-00 00:00:00', NULL),
('4020501', 'TDS on House Rent', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:44:07', 'admin', '2016-09-19 14:40:16', NULL),
('502030201', 'TDS Payable House Rent', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, 'admin', '2016-09-19 11:19:42', 'admin', '2016-09-28 13:19:37', NULL),
('502030203', 'TDS Payable on Advertisement Bill', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, 'admin', '2016-09-28 13:20:51', '', '0000-00-00 00:00:00', NULL),
('502030202', 'TDS Payable on Salary', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, 0.00, 'admin', '2016-09-28 13:20:17', '', '0000-00-00 00:00:00', NULL),
('4021402', 'Tea Kettle', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:33:45', '', '0000-00-00 00:00:00', NULL),
('4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:57:59', '', '0000-00-00 00:00:00', NULL),
('1010209', 'Telephone Set & PABX', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:51:57', 'admin', '2016-10-02 17:10:40', NULL),
('102020104', 'Test', 'Advance', 4, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2018-07-05 11:42:48', '', '0000-00-00 00:00:00', NULL),
('40203', 'Travelling & Conveyance', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, 0.00, 'admin', '2013-07-08 16:22:06', 'admin', '2015-10-15 18:45:13', NULL),
('4021406', 'TV', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 19:35:07', '', '0000-00-00 00:00:00', NULL),
('1010205', 'UPS', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:50:38', '', '0000-00-00 00:00:00', NULL),
('40204', 'Utility Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, 0.00, 'anwarul', '2013-07-11 16:20:24', 'admin', '2016-01-02 15:55:22', NULL),
('4020503', 'VAT on House Rent Exp', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:49:22', 'admin', '2016-09-25 14:00:52', NULL),
('5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 0, 1, 'L', 0, 0, 0.00, 'admin', '2015-10-15 19:51:11', 'admin', '2016-09-28 13:23:53', NULL),
('1010409', 'Vehicle A/C', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'Zoherul', '2016-05-12 12:13:21', '', '0000-00-00 00:00:00', NULL),
('1010405', 'Voltage Stablizer', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-03-27 10:40:59', '', '0000-00-00 00:00:00', NULL),
('1010105', 'Waiting Sofa - Steel', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2015-10-15 15:46:29', '', '0000-00-00 00:00:00', NULL),
('4020405', 'WASA Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2015-10-15 18:58:51', '', '0000-00-00 00:00:00', NULL),
('1010402', 'Water Purifier', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, 0.00, 'admin', '2016-01-29 11:14:11', '', '0000-00-00 00:00:00', NULL),
('4020705', 'Website Development Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, 0.00, 'admin', '2016-10-15 12:42:47', '', '0000-00-00 00:00:00', NULL),
('102030200045', 'xccsdgfd-Md-Tuhin', 'Patient Receivable', 4, 1, 1, 0, 'A', 0, 0, 0.00, '2', '2019-03-13 06:36:25', '', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `acc_transaction`
--

CREATE TABLE IF NOT EXISTS `acc_transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VDate` date DEFAULT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `StoreID` int(11) NOT NULL,
  `IsPosted` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `IsAppove` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_transaction`
--

INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `StoreID`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`, `branch_id`) VALUES
(1, 'ADGFDP38', 'Doctor Fee', '2019-01-26', '102030200009', 'Patient Credit For Doctor Fee', 0.00, 700.00, 2, '1', '38', '2019-01-26 10:58:46', NULL, NULL, '1', NULL),
(2, 'ADGFDP38', 'Doctor Fee', '2019-01-26', '1020302', 'Cash In Hand Debit For Doctor Fee', 700.00, 0.00, 2, '1', '38', '2019-01-26 10:58:46', NULL, NULL, '1', NULL),
(3, '1', 'Patient Advance', '2019-01-26', '102030200009', 'Advance For Patient Id PZRIMPJ4', 0.00, 1000.00, 2, '1', '2', '2019-01-26 11:02:43', NULL, NULL, '1', NULL),
(4, '1', 'Patient Advance', '2019-01-26', '102010204', 'Advance Payment For Patient Id PZRIMPJ4', 1000.00, 0.00, 2, '1', '2', '2019-01-26 11:02:43', NULL, NULL, '1', NULL),
(5, 'DV-1', 'DV', '2019-01-26', '102010204', 'add form denote', 0.00, 6200.00, 0, '1', '2', '2019-01-26 12:23:54', NULL, NULL, '1', NULL),
(6, 'DV-1', 'DV', '2019-01-26', '102030200009', 'add form denote', 1200.00, 0.00, 0, '1', '2', '2019-01-26 12:23:54', NULL, NULL, '1', NULL),
(7, 'DV-1', 'DV', '2019-01-26', '102010202', 'add form denote', 5000.00, 0.00, 0, '1', '2', '2019-01-26 12:23:54', NULL, NULL, '1', NULL),
(8, 'CV-1', 'CV', '2019-01-26', '102010204', 'for extra computer', 40000.00, 0.00, 0, '1', '2', '2019-01-26 12:25:08', NULL, NULL, '1', NULL),
(9, 'CV-1', 'CV', '2019-01-26', '1010206', 'for extra computer', 0.00, 40000.00, 0, '1', '2', '2019-01-26 12:25:08', NULL, NULL, '1', NULL),
(10, 'CV-2', 'CV', '2019-01-26', '1020101', 'salary', 73500.00, 0.00, 0, '1', '2', '2019-01-26 12:26:21', NULL, NULL, '1', NULL),
(11, 'CV-2', 'CV', '2019-01-26', '502020100002', 'salary', 0.00, 50000.00, 0, '1', '2', '2019-01-26 12:26:21', NULL, NULL, '1', NULL),
(12, 'CV-2', 'CV', '2019-01-26', '502020100003', 'salary', 0.00, 23500.00, 0, '1', '2', '2019-01-26 12:26:21', NULL, NULL, '1', NULL),
(13, 'AXFCTU8A', 'Doctor Fee', '2019-01-27', '102030200035', 'Patient Credit For Doctor Fee', 0.00, 1200.00, 2, '1', '70', '2019-01-27 04:30:09', NULL, NULL, '1', NULL),
(14, 'AXFCTU8A', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 1200.00, 0.00, 2, '1', '70', '2019-01-27 04:30:09', NULL, NULL, '1', NULL),
(15, 'ATPQI5A4', 'Doctor Fee', '2019-01-27', '102030200034', 'Patient Credit For Doctor Fee', 0.00, 100.00, 2, '1', '70', '2019-01-27 04:31:51', NULL, NULL, '1', NULL),
(16, 'ATPQI5A4', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 100.00, 0.00, 2, '1', '70', '2019-01-27 04:31:51', NULL, NULL, '1', NULL),
(17, 'AF9H71XW', 'Doctor Fee', '2019-01-27', '102030200032', 'Patient Credit For Doctor Fee', 0.00, 800.00, 2, '1', '50', '2019-01-27 04:35:16', NULL, NULL, '1', NULL),
(18, 'AF9H71XW', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 800.00, 0.00, 2, '1', '50', '2019-01-27 04:35:16', NULL, NULL, '1', NULL),
(19, 'A4ZQ1IVP', 'Doctor Fee', '2019-01-27', '102030200026', 'Patient Credit For Doctor Fee', 0.00, 800.00, 2, '1', '50', '2019-01-27 04:36:32', NULL, NULL, '1', NULL),
(20, 'A4ZQ1IVP', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 800.00, 0.00, 2, '1', '50', '2019-01-27 04:36:32', NULL, NULL, '1', NULL),
(21, 'AR0BPBC9', 'Doctor Fee', '2019-01-27', '102030200025', 'Patient Credit For Doctor Fee', 0.00, 800.00, 2, '1', '50', '2019-01-27 04:37:41', NULL, NULL, '1', NULL),
(22, 'AR0BPBC9', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 800.00, 0.00, 2, '1', '50', '2019-01-27 04:37:41', NULL, NULL, '1', NULL),
(23, 'AT9AIOHO', 'Doctor Fee', '2019-01-27', '102030200036', 'Patient Credit For Doctor Fee', 0.00, 900.00, 2, '1', '40', '2019-01-27 04:38:54', NULL, NULL, '1', NULL),
(24, 'AT9AIOHO', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 900.00, 0.00, 2, '1', '40', '2019-01-27 04:38:54', NULL, NULL, '1', NULL),
(25, 'AEP0ASA0', 'Doctor Fee', '2019-01-27', '102030200028', 'Patient Credit For Doctor Fee', 0.00, 900.00, 2, '1', '40', '2019-01-27 04:39:28', NULL, NULL, '1', NULL),
(26, 'AEP0ASA0', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 900.00, 0.00, 2, '1', '40', '2019-01-27 04:39:28', NULL, NULL, '1', NULL),
(27, 'AB5A1607', 'Doctor Fee', '2019-01-27', '102030200024', 'Patient Credit For Doctor Fee', 0.00, 900.00, 2, '1', '40', '2019-01-27 04:39:53', NULL, NULL, '1', NULL),
(28, 'AB5A1607', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 900.00, 0.00, 2, '1', '40', '2019-01-27 04:39:53', NULL, NULL, '1', NULL),
(29, 'AFZ28KX1', 'Doctor Fee', '2019-01-27', '102030200031', 'Patient Credit For Doctor Fee', 0.00, 1000.00, 2, '1', '39', '2019-01-27 04:41:05', NULL, NULL, '1', NULL),
(30, 'AFZ28KX1', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 1000.00, 0.00, 2, '1', '39', '2019-01-27 04:41:05', NULL, NULL, '1', NULL),
(31, 'ANMZOY5D', 'Doctor Fee', '2019-01-27', '102030200027', 'Patient Credit For Doctor Fee', 0.00, 100.00, 2, '1', '39', '2019-01-27 04:41:29', NULL, NULL, '1', NULL),
(32, 'ANMZOY5D', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 100.00, 0.00, 2, '1', '39', '2019-01-27 04:41:29', NULL, NULL, '1', NULL),
(33, 'AVWTBVQW', 'Doctor Fee', '2019-01-27', '102030200023', 'Patient Credit For Doctor Fee', 0.00, 700.00, 2, '1', '38', '2019-01-27 04:43:15', NULL, NULL, '1', NULL),
(34, 'AVWTBVQW', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 700.00, 0.00, 2, '1', '38', '2019-01-27 04:43:15', NULL, NULL, '1', NULL),
(35, 'ADGFDP38', 'Doctor Fee', '2019-01-27', '102030200009', 'Patient Credit For Doctor Fee', 0.00, 700.00, 2, '1', '38', '2019-01-27 04:44:13', NULL, NULL, '1', NULL),
(36, 'ADGFDP38', 'Doctor Fee', '2019-01-27', '1020302', 'Cash In Hand Debit For Doctor Fee', 700.00, 0.00, 2, '1', '38', '2019-01-27 04:44:13', NULL, NULL, '1', NULL),
(37, 'A3Z19LAS2', 'Doctor Fee', '2019-07-24', '102030200050', 'Patient Credit For Doctor Fee', 0.00, 30000.00, 2, '1', '2', '2019-07-24 15:58:18', NULL, NULL, '1', NULL),
(38, 'A3Z19LAS2', 'Doctor Fee', '2019-07-24', '1020302', 'Cash In Hand Debit For Doctor Fee', 30000.00, 0.00, 2, '1', '2', '2019-07-24 15:58:18', NULL, NULL, '1', NULL),
(39, 'A198OODZ2', 'Doctor Fee', '2019-08-24', '102030200051', 'Patient Credit For Doctor Fee', 0.00, 500.00, 2, '1', '2', '2019-08-24 15:05:49', NULL, NULL, '1', NULL),
(40, 'A198OODZ2', 'Doctor Fee', '2019-08-24', '1020302', 'Cash In Hand Debit For Doctor Fee', 500.00, 0.00, 2, '1', '2', '2019-08-24 15:05:49', NULL, NULL, '1', NULL),
(41, '20190828122841', 'Manufacaturer credit', '2019-08-28', '502020202', 'Medicine Purchase', 0.00, 640.00, 2, '1', '2', '2019-08-28 12:27:48', NULL, NULL, '1', 1),
(42, 'AT0UZDY62', 'Doctor Fee', '2019-09-01', '102030200054', 'Patient Credit For Doctor Fee', 0.00, 500.00, 2, '1', '2', '2019-09-01 06:31:13', NULL, NULL, '1', 0),
(43, 'AT0UZDY62', 'Doctor Fee', '2019-09-01', '1020302', 'Cash In Hand Debit For Doctor Fee', 500.00, 0.00, 2, '1', '2', '2019-09-01 06:31:13', NULL, NULL, '1', 0),
(44, 'AZAZXH6Q76', 'Doctor Fee', '2019-09-01', '102030200054', 'Patient Credit For Doctor Fee', 0.00, 300.00, 2, '1', '76', '2019-09-01 06:38:51', NULL, NULL, '1', 0),
(45, 'AZAZXH6Q76', 'Doctor Fee', '2019-09-01', '1020302', 'Cash In Hand Debit For Doctor Fee', 300.00, 0.00, 2, '1', '76', '2019-09-01 06:38:51', NULL, NULL, '1', 0),
(46, '2', 'Patient Advance', '2019-09-08', '102030200009', 'Advance For Patient Id PZRIMPJ4', 0.00, 25000.00, 2, '1', '2', '2019-09-08 10:31:28', NULL, NULL, '1', NULL),
(47, '2', 'Patient Advance', '2019-09-08', '102010204', 'Advance Payment For Patient Id PZRIMPJ4', 25000.00, 0.00, 2, '1', '2', '2019-09-08 10:31:28', NULL, NULL, '1', NULL),
(48, '3', 'Patient Advance', '2019-09-08', '102030200009', 'Advance For Patient Id PZRIMPJ4', 0.00, 25000.00, 2, '1', '2', '2019-09-08 10:32:20', NULL, NULL, '1', NULL),
(49, '3', 'Patient Advance', '2019-09-08', '102010204', 'Advance Payment For Patient Id PZRIMPJ4', 25000.00, 0.00, 2, '1', '2', '2019-09-08 10:32:20', NULL, NULL, '1', NULL),
(50, 'AVORCH8A81', 'Doctor Fee', '2019-09-15', '102030200075', 'Patient Credit For Doctor Fee', 0.00, 0.00, 2, '1', '81', '2019-09-15 11:21:08', NULL, NULL, '1', 3),
(51, 'AVORCH8A81', 'Doctor Fee', '2019-09-15', '1020302', 'Cash In Hand Debit For Doctor Fee', 0.00, 0.00, 2, '1', '81', '2019-09-15 11:21:08', NULL, NULL, '1', 3);

-- --------------------------------------------------------

--
-- Table structure for table `acm_account`
--

CREATE TABLE IF NOT EXISTS `acm_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acm_invoice`
--

CREATE TABLE IF NOT EXISTS `acm_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) NOT NULL,
  `total` float NOT NULL,
  `vat` float NOT NULL,
  `discount` float NOT NULL,
  `grand_total` float NOT NULL,
  `paid` float NOT NULL,
  `due` float NOT NULL,
  `created_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acm_invoice_details`
--

CREATE TABLE IF NOT EXISTS `acm_invoice_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `quantity` float NOT NULL,
  `price` float NOT NULL,
  `subtotal` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acm_payment`
--

CREATE TABLE IF NOT EXISTS `acm_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `pay_to` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `amount` float NOT NULL,
  `created_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acn_account_transaction`
--

CREATE TABLE IF NOT EXISTS `acn_account_transaction` (
  `account_tran_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `transaction_description` varchar(255) NOT NULL,
  `amount` varchar(25) NOT NULL,
  `tran_date` date NOT NULL,
  `payment_id` int(11) NOT NULL,
  `create_by_id` varchar(25) NOT NULL,
  PRIMARY KEY (`account_tran_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE IF NOT EXISTS `appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` varchar(20) DEFAULT NULL,
  `patient_id` varchar(20) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `serial_no` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `problem` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `refer_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `appointment_id`, `patient_id`, `department_id`, `doctor_id`, `schedule_id`, `serial_no`, `date`, `problem`, `created_by`, `create_date`, `branch_id`, `status`, `refer_by`) VALUES
(23, 'ADGFDP38', 'PZRIMPJ4', 16, 38, 6, '1', '2019-01-03', 'Skin Problem', 0, '2019-01-28', 0, 0, NULL),
(24, 'AVWTBVQW', 'PRQXXD9Q', 16, 38, 6, '2', '2019-01-03', 'spine', 0, '2019-01-28', 0, 0, NULL),
(25, 'AB5A1607', 'P1WY4YCU', 26, 40, 8, '1', '2019-01-01', 'Kidney problem', 0, '2019-01-28', 0, 0, NULL),
(26, 'AR0BPBC9', 'P45E9QXP', 30, 50, 7, '1', '2019-01-04', 'Leg broken', 0, '2018-06-26', 0, 0, NULL),
(27, 'A4ZQ1IVP', 'PMTGGP7A', 30, 50, 5, '1', '2019-01-07', 'Leg pain', 0, '2018-06-26', 0, 0, NULL),
(28, 'ANMZOY5D', 'P86DHOFT', 31, 39, 1, '1', '2019-01-06', 'Heart Diseases', 0, '2018-07-26', 0, 0, NULL),
(29, 'AEP0ASA0', 'P6J76U0K', 26, 40, 8, '2', '2019-01-01', 'Baby problem', 0, '2018-07-26', 0, 0, NULL),
(30, 'ALELQRTQ', 'PEPWILVC', 31, 42, 4, '1', '2019-01-04', 'heart', 0, '2018-09-26', 0, 0, NULL),
(31, 'AKW65ZSK', 'PJ916RXG', 31, 42, 4, '2', '2019-01-04', 'pain', 0, '2018-11-26', 0, 0, NULL),
(32, 'AFZ28KX1', 'PV5KRZJ1', 31, 39, 1, '5', '2019-01-06', 'heart', 0, '2018-11-26', 0, 0, NULL),
(33, 'AF9H71XW', 'PK5L3G3G', 30, 50, 5, '2', '2019-01-07', 'tre', 0, '2018-11-26', 0, 0, NULL),
(34, 'ASA9APQ6', 'P6WFT0SK', 31, 42, 4, '4', '2019-01-04', 'ytres', 0, '2018-12-26', 0, 0, NULL),
(35, 'ATPQI5A4', 'P0IK5GYT', 16, 70, 9, '1', '2019-01-01', 'ytres', 0, '2018-03-26', 0, 0, NULL),
(36, 'AXFCTU8A', 'P4MHVMZT', 16, 70, 3, '1', '2019-01-05', 'ytres', 0, '2018-03-26', 0, 0, NULL),
(37, 'AT9AIOHO', 'P5J949EY', 26, 40, 8, '6', '2019-01-01', 'pain ', 0, '2018-04-26', 0, 0, NULL),
(39, 'ALVZDXGQ', 'P60FTN40', 21, 39, 1, '1', '2019-08-25', 'Many problem', 2, '2019-08-24', 1, 1, NULL),
(40, 'ADI7T6BW', 'PP37NOT8', 27, 38, 14, '1', '2019-09-11', 'back Pain', 2, '2019-09-03', 1, 1, NULL),
(41, 'A3XH2R7V', 'P60FTN40', 16, 38, 12, '1', '2019-09-15', 'some problems', 2, '2019-09-03', 1, 1, NULL),
(42, 'ARZRPYMD', 'P60FTN40', 16, 39, 1, '1', '2019-09-15', 'sssssss', 2, '2019-09-03', 1, 1, NULL),
(43, 'AKFTIP7R', 'PHSMK844', 16, 39, 10, '1', '2019-09-06', 'Nasal drop out', 39, '2019-09-04', 1, 1, 76),
(44, 'ABQC487B', 'P73WX7YS', 32, 40, 14, '1', '2019-09-11', 'some problem', 0, '2019-09-07', 1, 1, NULL),
(45, 'AFR8HK3E', 'PMK5OHQF', 32, 77, 18, '1', '2019-09-19', 'sdfasf', 0, '2019-09-07', 2, 1, NULL),
(46, 'A6ZRC2ZF', 'PZ685B4I', 32, 77, 18, '1', '2019-09-12', 'sadfasdfasdf', 0, '2019-09-07', 2, 1, NULL),
(47, 'AXWQUR51', 'PJDW95W3', 32, 77, 18, '2', '2019-09-12', 'sadfasdfasdf', 0, '2019-09-07', 2, 1, NULL),
(48, 'AR86XNXO', 'P73WX7YS', 32, 77, 18, '3', '2019-09-12', 'dsfsdaf', 0, '2019-09-07', 0, 1, NULL),
(49, 'AEHIELWH', 'P73WX7YS', 32, 77, 18, '2', '2019-09-19', 'sdfasdf', 0, '2019-09-07', 2, 1, NULL),
(50, 'A4334VUA', 'PC7EK6QR', 32, 77, 18, '4', '2019-09-19', 'sadfsadf', 0, '2019-09-07', 2, 1, NULL),
(51, 'AXF5L9UX', 'P1AKIU0O', 32, 77, 18, '12:00 - 12:30', '2019-09-12', 'sdfasdfsadf', 0, '2019-09-07', 2, 1, NULL),
(52, 'AS1NTP9I', 'PCLY6LSX', 32, 77, 18, '08:30 - 09:00', '2019-09-26', 'sdfsdf', 0, '2019-09-07', 2, 1, NULL),
(53, 'A2SP7FR0', 'PUW40T0G', 32, 77, 18, '08:30 - 09:00', '2019-09-26', 'asdfasd', 0, '2019-09-07', 2, 1, NULL),
(54, 'ABZILQG6', 'P4M5A5XZ', 32, 77, 18, '09:30 - 10:00', '2019-09-19', 'sdfsad', 0, '2019-09-07', 2, 1, NULL),
(55, 'AQPSN8EZ', 'P8OMUDK3', 32, 77, 18, '10:00 - 10:30', '2019-09-12', 'sdfsadfsdf', 0, '2019-09-07', 2, 1, NULL),
(56, 'AUTOQB9L', 'P8OMUDK3', 32, 77, 18, '08:30 - 09:00', '2019-09-26', 'ssdfasdf', 0, '2019-09-07', 2, 1, NULL),
(57, 'AIURWMKN', 'PCQ2JONU', 32, 77, 18, '09:30 - 10:00', '2019-09-19', 'Gass problem', 78, '2019-09-08', 2, 1, 0),
(58, 'AY5AKLZ1', 'PCQ2JONU', 32, 77, 18, '10:00 - 10:30', '2019-09-12', 'some problem', 0, '2019-09-08', 2, 1, NULL),
(59, 'AI1EJ8QO', 'PAT1LAGN', 32, 77, 18, '10:00 - 10:30', '2019-09-12', 'cold ', 0, '2019-09-08', 2, 1, NULL),
(60, 'AIU2A2AS', 'P8ZCQMXI', 31, 39, 20, '10:30 - 11:00', '2019-09-10', 'Testing', 39, '2019-09-09', 1, 1, NULL),
(61, 'AJ3LBC47', 'PECW0N53', 16, 39, 1, '07:00 - 07:20', '2019-09-08', 'kkkk problem', 2, '2019-09-09', 2, 1, 76),
(64, 'A0YQ1WI1', 'PXL3B2DJ', 16, 81, 26, '17:00 - 17:30', '2019-09-16', 'asfsf ad af asfd asdfdsf ', 81, '2019-09-11', 3, 1, 82),
(65, 'ABOF92LI', 'PSKNIKJ2', 33, 83, 29, '06:28 - 06:48', '2019-09-12', 'demo problem', 83, '2019-09-12', 4, 1, 84),
(67, 'AZVQSVW3', 'PG0VW841', 16, 81, 26, '13:30 - 14:00', '2019-09-16', 'asdfsdf', 2, '2019-09-15', 3, 1, 86),
(68, 'AB8DPNM8', 'PZ1A14D7', 31, 39, 25, '09:00 - 09:30', '2019-09-24', 'some body', 2, '2019-09-17', 3, 1, 0),
(69, 'ANG42GQO', 'PI27P99N', 16, 97, 38, '09:20 - 09:40', '2019-09-23', 'sdfsd asfaf', 2, '2019-09-17', 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE IF NOT EXISTS `bill` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_id` varchar(20) DEFAULT NULL,
  `bill_type` varchar(20) DEFAULT NULL,
  `bill_date` date DEFAULT NULL,
  `admission_id` varchar(20) DEFAULT NULL,
  `discount` float DEFAULT 0,
  `tax` float DEFAULT 0,
  `total` float DEFAULT 0,
  `payment_method` varchar(10) DEFAULT NULL,
  `card_cheque_no` varchar(100) DEFAULT NULL,
  `receipt_no` varchar(100) DEFAULT NULL,
  `note` text DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 3` (`bill_id`),
  KEY `Index 2` (`admission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bill_admission`
--

CREATE TABLE IF NOT EXISTS `bill_admission` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admission_id` varchar(20) DEFAULT NULL,
  `patient_id` varchar(20) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `discharge_date` date DEFAULT NULL,
  `insurance_id` int(11) DEFAULT NULL,
  `policy_no` varchar(100) DEFAULT NULL,
  `agent_name` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `guardian_relation` varchar(255) DEFAULT NULL,
  `guardian_contact` varchar(255) DEFAULT NULL,
  `guardian_address` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `isComplete` tinyint(4) NOT NULL COMMENT '1=Complete and 0=Not Complete',
  `branch_id` int(11) NOT NULL,
  `assign_by` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`admission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_admission`
--

INSERT INTO `bill_admission` (`id`, `admission_id`, `patient_id`, `doctor_id`, `package_id`, `admission_date`, `discharge_date`, `insurance_id`, `policy_no`, `agent_name`, `guardian_name`, `guardian_relation`, `guardian_contact`, `guardian_address`, `status`, `isComplete`, `branch_id`, `assign_by`) VALUES
(1, 'UXQWE286', 'PZRIMPJ4', 38, 0, '2019-01-26', '0000-00-00', 0, '', '', 'John', 'Brother', '098765456', '', 1, 0, 0, 2),
(2, 'UTU3N48P', 'PECW0N53', 39, NULL, '2019-03-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 2),
(3, 'UL8RH8GI', 'PGQG6XSK', 39, 0, '2019-07-24', '0000-00-00', 1, '', '', '', '', '', '', 1, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bill_advanced`
--

CREATE TABLE IF NOT EXISTS `bill_advanced` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `admission_id` varchar(20) DEFAULT NULL,
  `patient_id` varchar(30) DEFAULT NULL,
  `amount` float DEFAULT 0,
  `payment_method` varchar(255) DEFAULT NULL,
  `cash_card_or_cheque` varchar(10) DEFAULT NULL COMMENT '1 cash, 2 card, 3 cheque',
  `receipt_no` varchar(100) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ipd_bill_advanced_ipd_admission` (`admission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_advanced`
--

INSERT INTO `bill_advanced` (`id`, `admission_id`, `patient_id`, `amount`, `payment_method`, `cash_card_or_cheque`, `receipt_no`, `branch_id`, `date`) VALUES
(1, 'UXQWE286', 'PZRIMPJ4', 1000, 'online', 'Card', '1211', 0, '2019-01-26 11:02:43'),
(2, 'UXQWE286', 'PZRIMPJ4', 25000, 'ADVANCE', 'Cash', '', 2, '2019-09-08 10:31:28'),
(3, 'UXQWE286', 'PZRIMPJ4', 25000, 'ADVANCE', 'Cash', '62', 1, '2019-09-08 10:32:20');

-- --------------------------------------------------------

--
-- Table structure for table `bill_details`
--

CREATE TABLE IF NOT EXISTS `bill_details` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_id` varchar(20) DEFAULT NULL,
  `admission_id` varchar(20) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `quantity` float DEFAULT 0,
  `amount` float DEFAULT 0,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Key As Bill_ID` (`bill_id`),
  KEY `Admission ID` (`admission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bill_package`
--

CREATE TABLE IF NOT EXISTS `bill_package` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `services` text DEFAULT NULL,
  `discount` float DEFAULT 0,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_package`
--

INSERT INTO `bill_package` (`id`, `name`, `description`, `services`, `discount`, `branch_id`, `status`) VALUES
(1, 'New Package', 'sdfsdf', '[{\"id\":\"1\",\"name\":\"Test Service\",\"quantity\":\"10\",\"amount\":\"1000\"}]', 5, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bill_service`
--

CREATE TABLE IF NOT EXISTS `bill_service` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `quantity` float DEFAULT 0,
  `amount` float DEFAULT 0,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bill_service`
--

INSERT INTO `bill_service` (`id`, `name`, `description`, `quantity`, `amount`, `branch_id`, `status`) VALUES
(1, 'Test Service', 'Some text here', 10, 1000, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bm_bed`
--

CREATE TABLE IF NOT EXISTS `bm_bed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `bed_number` varchar(20) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bm_bed`
--

INSERT INTO `bm_bed` (`id`, `room_id`, `description`, `bed_number`, `branch_id`, `status`) VALUES
(1, 1, 'fh', 'CB-001', 0, 1),
(2, 1, 'b', 'CB-002', 0, 1),
(3, 4, 'sdfsdf', 'A1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bm_bed_assign`
--

CREATE TABLE IF NOT EXISTS `bm_bed_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial` varchar(20) DEFAULT NULL,
  `patient_id` varchar(20) NOT NULL,
  `room_id` int(11) NOT NULL,
  `bed_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `assign_date` date NOT NULL,
  `discharge_date` date DEFAULT NULL,
  `assign_by` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bm_bed_assign`
--

INSERT INTO `bm_bed_assign` (`id`, `serial`, `patient_id`, `room_id`, `bed_id`, `description`, `assign_date`, `discharge_date`, `assign_by`, `branch_id`, `status`, `update_by`) VALUES
(1, 'KW2KCQ', 'PCN2GABV', 1, 26, '', '2019-01-12', NULL, 2, 0, 1, 0),
(2, '8MJDWL', 'PBKVP9IH', 2, 25, '', '2019-01-12', '2019-01-13', 2, 0, 0, 2),
(3, 'TH2H6N', 'P3YRCI9J', 0, 0, '', '2019-01-13', '2019-01-17', 2, 0, 0, 2),
(4, 'LDRR1N', 'PUXULY35', 1, 4, '', '2019-01-13', '2019-01-17', 2, 0, 0, 2),
(5, 'FUML7U', 'P5J5PQIL', 3, 29, '', '2019-01-20', '2019-01-21', 2, 0, 0, 2),
(6, '1JKU17', 'PV3W61HC', 1, 1, '', '2019-01-21', NULL, 2, 0, 1, 0),
(7, 'ZTYNQO', 'PZRIMPJ4', 1, 3, '', '2019-01-26', '0000-00-00', 2, 0, 0, 0),
(8, 'M2J7NX', 'PECW0N53', 1, 1, '', '2019-03-13', NULL, 2, 0, 1, 0),
(9, 'GCPIZC', 'PGQG6XSK', 1, 2, '', '2019-07-24', '0000-00-00', 2, 0, 0, 0),
(10, 'TS6BKO', 'PDMCKTYV', 4, 3, 'sdafasd', '2019-08-25', '0000-00-00', 2, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bm_bed_transfer`
--

CREATE TABLE IF NOT EXISTS `bm_bed_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(11) NOT NULL,
  `form_bed_id` int(11) NOT NULL,
  `to_bed_id` int(11) NOT NULL,
  `assign_date` date NOT NULL,
  `discharge_date` date DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assign_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bm_room`
--

CREATE TABLE IF NOT EXISTS `bm_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `limit` int(3) NOT NULL,
  `charge` float NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bm_room`
--

INSERT INTO `bm_room` (`id`, `room_name`, `description`, `limit`, `charge`, `branch_id`, `status`) VALUES
(1, 'Cabin', 'dsf', 6, 800, 0, 1),
(2, 'Ward', 'dfsdf', 18, 300, 0, 1),
(3, 'ICO', 'dfs', 6, 3000, 0, 1),
(4, 'X-ray', 'sdfdf', 4, 1000, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `branch_info`
--

CREATE TABLE IF NOT EXISTS `branch_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branch_info`
--

INSERT INTO `branch_info` (`id`, `branch_name`, `address`, `phone`, `status`) VALUES
(3, 'Queens', 'abc 1243', '243242343', 1),
(5, 'Texas', 'lskjfdso98098fs9', '32423432432', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cm_patient`
--

CREATE TABLE IF NOT EXISTS `cm_patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) NOT NULL,
  `case_manager_id` int(11) NOT NULL,
  `ref_doctor_id` int(11) DEFAULT NULL,
  `hospital_name` varchar(255) DEFAULT NULL,
  `hospital_address` text DEFAULT NULL,
  `doctor_name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cm_patient`
--

INSERT INTO `cm_patient` (`id`, `patient_id`, `case_manager_id`, `ref_doctor_id`, `hospital_name`, `hospital_address`, `doctor_name`, `created_by`, `date`, `status`) VALUES
(1, 'PZ1A14D7', 30, 81, 'deemo', 'dfgsdfgsdfg', 'hmmm', 88, '2019-09-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cm_status`
--

CREATE TABLE IF NOT EXISTS `cm_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `critical_status` varchar(255) NOT NULL DEFAULT '1',
  `cm_patient_id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_sms_info`
--

CREATE TABLE IF NOT EXISTS `custom_sms_info` (
  `custom_sms_id` int(11) NOT NULL AUTO_INCREMENT,
  `reciver` varchar(100) NOT NULL,
  `gateway` text NOT NULL,
  `message` text NOT NULL,
  `sms_date_time` datetime NOT NULL,
  PRIMARY KEY (`custom_sms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `custom_sms_info`
--

INSERT INTO `custom_sms_info` (`custom_sms_id`, `reciver`, `gateway`, `message`, `sms_date_time`) VALUES
(56, '012455', 'nexmo', 'Hello, al hasan. \r\nYour ID: PNQL9ONJ,  \r\nThank you for the registration.', '2018-08-07 12:05:03'),
(57, '35463', 'nexmo', 'Hello, al hassan. \r\nYour ID: PH2VIFS9,  \r\nThank you for the registration.', '2018-08-19 03:03:16'),
(58, '768678', 'nexmo', 'Hello, fcgbdxfgb fdxgbdfx. \r\nYour ID: PS2H4ADR,  \r\nThank you for the registration.', '2018-08-19 03:06:55'),
(59, 'xcvx', 'nexmo', 'Hello, bvcb vbcvb. \r\nYour ID: P6CK827E,  \r\nThank you for the registration.', '2018-11-08 07:30:31'),
(60, '5475476', 'nexmo', 'Hello, Ashraf vbcvb. \r\nYour ID: P5Z5KVEJ,  \r\nThank you for the registration.', '2018-11-08 08:02:08'),
(61, '645645', 'nexmo', 'Hello, rte fd. \r\nYour ID: P6QVEOSW,  \r\nThank you for the registration.', '2018-11-08 08:17:53'),
(62, '757', 'nexmo', 'Hello, vvxcv cxvxcv. \r\nYour ID: PDZZRROF,  \r\nThank you for the registration.', '2018-11-08 08:29:34'),
(63, '64', 'nexmo', 'Hello, Devid  vxc. \r\nYour ID: PXB9O97E,  \r\nThank you for the registration.', '2018-11-08 08:48:04'),
(64, '5345', 'nexmo', 'Hello, Ashraf vbcvb. \r\nYour ID: PYNL6ZEK,  \r\nThank you for the registration.', '2018-11-08 09:31:40'),
(65, '4654', 'nexmo', 'Hello, Ashraf vbcvb. \r\nYour ID: P4T45TWY,  \r\nThank you for the registration.', '2018-11-08 11:08:39'),
(66, '454634', 'nexmo', 'Hello, bvcb Mullar. \r\nYour ID: PXPEZJUG,  \r\nThank you for the registration.', '2018-11-10 07:44:39'),
(67, '4543', 'nexmo', 'Hello, rtrt rtert. \r\nYour ID: PXKPH1FD,  \r\nThank you for the registration.', '2018-11-10 07:47:44'),
(68, '54643534', 'nexmo', 'Hello, fg fgfdg. \r\nYour ID: P4RGX3YZ,  \r\nThank you for the registration.', '2018-11-10 07:50:03'),
(69, '567456', 'nexmo', 'Hello, fgdf fdgd. \r\nYour ID: PXQ4KS0P,  \r\nThank you for the registration.', '2018-11-10 07:56:52'),
(70, '017238338869', 'nexmo', 'Hello, James Marry. \r\nYour ID: PLUB5ZUT,  \r\nThank you for the registration.', '2018-11-11 05:42:52'),
(71, '017238338869', 'nexmo', 'Hello, James Marry. \r\nYour ID: PCFJG1H3,  \r\nThank you for the registration.', '2018-11-11 05:43:27'),
(72, '564564', 'nexmo', 'Hello, James  Marry. \r\nYour ID: PRXFNZ43,  \r\nThank you for the registration.', '2018-11-11 05:51:29'),
(73, '5646456456', 'nexmo', 'Hello, aaaa aaaa. \r\nYour ID: PTNDNJK8,  \r\nThank you for the registration.', '2019-01-07 12:24:16'),
(74, '57689', 'nexmo', 'Hello, Ashraf Khan Rana. \r\nYour ID: P7B5WMAE,  \r\nThank you for the registration.', '2019-01-07 12:25:21'),
(75, '546456456', 'nexmo', 'Hello, adsasd asdas. \r\nYour ID: PFNYDIN3,  \r\nThank you for the registration.', '2019-01-07 12:45:17'),
(76, '876786', 'nexmo', 'Hello, asd gd. \r\nYour ID: P5W98FYZ,  \r\nThank you for the registration.', '2019-01-07 12:48:42'),
(77, '98777', 'nexmo', 'Hello, Azad Ahmed. \r\nYour ID: PK1DKERF,  \r\nThank you for the registration.', '2019-01-07 12:50:14'),
(78, '57567', 'nexmo', 'Hello, nnn mmmm. \r\nYour ID: PCN2GABV,  \r\nThank you for the registration.', '2019-01-12 12:54:46'),
(79, '123456', 'nexmo', 'Hello, add ray. \r\nYour ID: PBKVP9IH,  \r\nThank you for the registration.', '2019-01-12 02:17:26'),
(80, 'fgdf', 'nexmo', 'Hello, gdfg fgdf. \r\nYour ID: P3YRCI9J,  \r\nThank you for the registration.', '2019-01-13 02:02:58'),
(81, '7567', 'nexmo', 'Hello, fgfd gfdg. \r\nYour ID: PUXULY35,  \r\nThank you for the registration.', '2019-01-13 02:05:13'),
(82, '54545', 'nexmo', 'Hello, ffgf ggff. \r\nYour ID: PLGB55X5,  \r\nThank you for the registration.', '2019-01-16 07:25:41'),
(83, '017346743568', 'nexmo', 'Hello, ttt tt. \r\nYour ID: PWHGBWJ7,  \r\nThank you for the registration.', '2019-01-16 07:36:00'),
(84, '017346743568', 'nexmo', 'Hello, tt tt. \r\nYour ID: PYM97T2G,  \r\nThank you for the registration.', '2019-01-16 07:38:10'),
(85, '017346743568', 'nexmo', 'Hello, tt tt. \r\nYour ID: PNW3SBSX,  \r\nThank you for the registration.', '2019-01-16 07:39:19'),
(86, '017346743568', 'nexmo', 'Hello, dfgh243 dfsdf. \r\nYour ID: P5J5PQIL,  \r\nThank you for the registration.', '2019-01-17 04:49:51'),
(87, '017346743568', 'nexmo', 'Doctor, Sumon Ahmed. \r\nHello, tt tt. \r\nYour ID: PNW3SBSX, Appointment ID: A4B8MAEU, Serial: 1 and Appointment Date: 20 January 2019. \r\nThank you for the Appointment.', '2019-01-19 11:10:25'),
(88, '7567', 'nexmo', 'Doctor, Sumon Ahmed. \r\nHello, fgfd gfdg. \r\nYour ID: PUXULY35, Appointment ID: AS842LSV, Serial: 1 and Appointment Date: 20 January 2019. \r\nThank you for the Appointment.', '2019-01-19 11:42:07'),
(89, '01723833869', 'nexmo', 'zxczx', '2019-01-19 05:31:35'),
(90, '456', 'nexmo', 'Hello, hello hello123. \r\nYour ID: PKZ1AX3E,  \r\nThank you for the registration.', '2019-01-19 01:42:48'),
(91, '463443564', 'nexmo', 'Hello, Shah Alam. \r\nYour ID: PHJHMNTH,  \r\nThank you for the registration.', '2019-01-21 08:26:20'),
(92, '34543', 'nexmo', 'Hello, vcv xcvx. \r\nYour ID: PV3W61HC,  \r\nThank you for the registration.', '2019-01-21 09:42:36'),
(93, '017346743568', 'nexmo', 'Doctor, Sumon Ahmed. \r\nHello, dfgh243 dfsdf. \r\nYour ID: P5J5PQIL, Appointment ID: ASQMJOLK, Serial: 1 and Appointment Date: 06 January 2019. \r\nThank you for the Appointment.', '2019-01-22 05:54:37'),
(94, '435345', 'nexmo', 'Doctor, Sumon Ahmed. \r\nHello, xcxv cxvxc. \r\nYour ID: P7MFNN2D, Appointment ID: AB6JKNLP, Serial: 2 and Appointment Date: 06 January 2019. \r\nThank you for the Appointment.', '2019-01-22 11:55:36'),
(95, '01725436866', 'nexmo', 'Doctor, Joe Keller. \r\nHello, sfsd dfdf. \r\nYour ID: P0QTQ0LG, Appointment ID: AZN2R8HK, Serial: 1 and Appointment Date: 06 January 2019. \r\nThank you for the Appointment.', '2019-01-22 12:10:31'),
(96, '4454543545343', 'bdtask', 'Hello, Marco S. Scott. \r\nYour ID: PG1E53IT,  \r\nThank you for the registration.', '2019-01-26 10:02:54'),
(97, '45346457', 'bdtask', 'Hello, John Glasper. \r\nYour ID: PK9QNQ5E,  \r\nThank you for the registration.', '2019-01-26 10:05:05'),
(98, '098765445345', 'bdtask', 'Hello, Lourdes  Eubanks. \r\nYour ID: PKK4W6QT,  \r\nThank you for the registration.', '2019-01-26 10:06:06'),
(99, '765433456789', 'bdtask', 'Hello, Roger  Haywood. \r\nYour ID: P08MP6WQ,  \r\nThank you for the registration.', '2019-01-26 10:08:23'),
(100, '0987654456776', 'bdtask', 'Hello, Ashraf  Rahman. \r\nYour ID: P69YN5VJ,  \r\nThank you for the registration.', '2019-01-26 10:10:02'),
(101, '7978675757645', 'bdtask', 'Hello, Linda  Logan. \r\nYour ID: PTHITYJC,  \r\nThank you for the registration.', '2019-01-26 10:12:00'),
(102, '896489455453', 'bdtask', 'Hello, Rick Thompson. \r\nYour ID: P4PCCAGG,  \r\nThank you for the registration.', '2019-01-26 10:14:36'),
(103, '7865897655345', 'bdtask', 'Hello, Billy  Diehl. \r\nYour ID: PX4HE1WM,  \r\nThank you for the registration.', '2019-01-26 10:16:24'),
(104, '87443226543', 'bdtask', 'Doctor, Sharon Wooten. \r\nHello, Christopher C. Salmon. \r\nYour ID: PZRIMPJ4, Appointment ID: ADGFDP38, Serial: 1 and Appointment Date: 03 January 2019. \r\nThank you for the Appointment.', '2019-01-26 10:53:21'),
(105, '34567897890', 'bdtask', 'Hello, Derrick M. Rigney. \r\nYour ID: PEMTVO2A,  \r\nThank you for the registration.', '2019-01-26 01:25:58'),
(106, '8675675675675', 'bdtask', 'Hello, Otto J. Voigt. \r\nYour ID: PN8DKMJG,  \r\nThank you for the registration.', '2019-01-26 01:31:11'),
(107, '345678987654', 'bdtask', 'Hello, Howard M. McCoy. \r\nYour ID: P90KREP7,  \r\nThank you for the registration.', '2019-01-26 01:32:35'),
(108, '345678987654', 'bdtask', 'Hello, Howard M. McCoy. \r\nYour ID: P25LF5A8,  \r\nThank you for the registration.', '2019-01-26 01:32:36'),
(109, '45678997654', 'bdtask', 'Hello, Rosie A. Kelly. \r\nYour ID: P0PMGAQE,  \r\nThank you for the registration.', '2019-01-26 01:34:26'),
(110, '87698765487', 'bdtask', 'Hello, Richard V. Welch. \r\nYour ID: PY5G7FNA,  \r\nThank you for the registration.', '2019-01-26 01:35:41'),
(111, '09867540077', 'bdtask', 'Hello, Donald M. Smith. \r\nYour ID: PFTBOJTF,  \r\nThank you for the registration.', '2019-01-26 01:37:03'),
(112, '0123456787', 'bdtask', 'Hello, James B. Roberts. \r\nYour ID: PFZ6JDKE,  \r\nThank you for the registration.', '2019-01-26 01:39:54'),
(113, '012570587554', 'bdtask', 'Hello, Mark N. Jeske. \r\nYour ID: PY55TSNJ,  \r\nThank you for the registration.', '2019-01-26 01:40:58'),
(114, '85675753445', 'bdtask', 'Hello, Peter S. Leu. \r\nYour ID: PDDLXVWI,  \r\nThank you for the registration.', '2019-01-26 01:43:56'),
(115, '01745653955', 'bdtask', 'Hello, Hilda P. Lambert. \r\nYour ID: PUUVC1CY,  \r\nThank you for the registration.', '2019-01-27 03:31:53'),
(116, '98765438906', 'bdtask', 'Hello, Colleen R. Matos. \r\nYour ID: P7YM5ZSG,  \r\nThank you for the registration.', '2019-01-27 03:34:14'),
(117, '9876543234', 'bdtask', 'Hello, George  A. White. \r\nYour ID: PDQQ75PP,  \r\nThank you for the registration.', '2019-01-27 03:35:20'),
(118, '98763453243', 'bdtask', 'Doctor, Sharon Wooten. \r\nHello, Solomon B. Boston. \r\nYour ID: PRQXXD9Q, Appointment ID: AVWTBVQW, Serial: 2 and Appointment Date: 03 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:44:12'),
(119, '809567897654', 'bdtask', 'Doctor, Kevin Wright. \r\nHello, Maurice M. Atchison. \r\nYour ID: P1WY4YCU, Appointment ID: AB5A1607, Serial: 1 and Appointment Date: 01 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:45:50'),
(120, '09876543456', 'bdtask', 'Doctor, James  Smith. \r\nHello, Michael N. Hayes. \r\nYour ID: P45E9QXP, Appointment ID: AR0BPBC9, Serial: 1 and Appointment Date: 04 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:47:18'),
(121, '012485673768', 'bdtask', 'Doctor, James  Smith. \r\nHello, Jose S. Low. \r\nYour ID: PMTGGP7A, Appointment ID: A4ZQ1IVP, Serial: 1 and Appointment Date: 07 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:48:21'),
(122, '28789784534', 'bdtask', 'Doctor, Joe Keller. \r\nHello, Celeste G. Brown. \r\nYour ID: P86DHOFT, Appointment ID: ANMZOY5D, Serial: 1 and Appointment Date: 06 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:50:09'),
(123, '90876543457', 'bdtask', 'Doctor, Kevin Wright. \r\nHello, Daniel C. Haskins. \r\nYour ID: P6J76U0K, Appointment ID: AEP0ASA0, Serial: 2 and Appointment Date: 01 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:52:31'),
(124, '486889435', 'bdtask', 'Doctor, Kaitlyn Campion. \r\nHello, Richard A. Hamel. \r\nYour ID: PEPWILVC, Appointment ID: ALELQRTQ, Serial: 1 and Appointment Date: 04 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:57:01'),
(125, '8743232556', 'bdtask', 'Doctor, Kaitlyn Campion. \r\nHello, Thomas A. Hamel. \r\nYour ID: PJ916RXG, Appointment ID: AKW65ZSK, Serial: 2 and Appointment Date: 04 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:57:46'),
(126, '0557348785', 'bdtask', 'Doctor, Joe Keller. \r\nHello, Troy A. Hamel. \r\nYour ID: PV5KRZJ1, Appointment ID: AFZ28KX1, Serial: 5 and Appointment Date: 06 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:58:46'),
(127, '0546675456', 'bdtask', 'Doctor, James  Smith. \r\nHello, Michael  R. Vega. \r\nYour ID: PK5L3G3G, Appointment ID: AF9H71XW, Serial: 2 and Appointment Date: 07 January 2019. \r\nThank you for the Appointment.', '2019-01-27 03:59:59'),
(128, '04378538765', 'bdtask', 'Doctor, Kaitlyn Campion. \r\nHello, Otis I. Levine. \r\nYour ID: P6WFT0SK, Appointment ID: ASA9APQ6, Serial: 4 and Appointment Date: 04 January 2019. \r\nThank you for the Appointment.', '2019-01-27 04:01:21'),
(129, '987654568', 'bdtask', 'Doctor, Sumon Ahmed. \r\nHello, Kelly A. Bodine. \r\nYour ID: P0IK5GYT, Appointment ID: ATPQI5A4, Serial: 1 and Appointment Date: 01 January 2019. \r\nThank you for the Appointment.', '2019-01-27 04:18:47'),
(130, '98765434567', 'bdtask', 'Doctor, Sumon Ahmed. \r\nHello, Geraldine R. Flores. \r\nYour ID: P4MHVMZT, Appointment ID: AXFCTU8A, Serial: 1 and Appointment Date: 05 January 2019. \r\nThank you for the Appointment.', '2019-01-27 04:19:38'),
(131, '09876543234', 'bdtask', 'Doctor, Kevin Wright. \r\nHello, Wanda R. Flores. \r\nYour ID: P5J949EY, Appointment ID: AT9AIOHO, Serial: 6 and Appointment Date: 01 January 2019. \r\nThank you for the Appointment.', '2019-01-27 04:20:31'),
(132, '01723833869', 'bdtask', 'Hello, Md Babul. \r\nYour ID: P8OMUDK3,  \r\nThank you for the registration.', '2019-09-07 01:28:28'),
(133, '01723833869', 'bdtask', 'Doctor, Tanzil Ahmad. \r\nHello, Md Babul. \r\nYour ID: P8OMUDK3, Appointment ID: AQPSN8EZ, Serial: 10:00 - 10:30 and Appointment Date: 12 September 2019. \r\nThank you for the Appointment.', '2019-09-07 01:29:09'),
(134, '01723833869', 'bdtask', 'Doctor, Tanzil Ahmad. \r\nHello, Md Babul. \r\nYour ID: P8OMUDK3, Appointment ID: AUTOQB9L, Serial: 08:30 - 09:00 and Appointment Date: 26 September 2019. \r\nThank you for the Appointment.', '2019-09-07 01:35:43'),
(135, '01852376598', 'bdtask', 'Hello, Km Habib. \r\nYour ID: PCQ2JONU,  \r\nThank you for the registration.', '2019-09-08 05:41:15'),
(136, '01852376598', 'bdtask', 'Doctor, Tanzil Ahmad. \r\nHello, Km Habib. \r\nYour ID: PCQ2JONU, Appointment ID: AIURWMKN, Serial: 09:30 - 10:00 and Appointment Date: 19 September 2019. \r\nThank you for the Appointment.', '2019-09-08 05:42:11'),
(137, '01852376598', 'bdtask', 'Doctor, Tanzil Ahmad. \r\nHello, Km Habib. \r\nYour ID: PCQ2JONU, Appointment ID: AY5AKLZ1, Serial: 10:00 - 10:30 and Appointment Date: 12 September 2019. \r\nThank you for the Appointment.', '2019-09-08 05:50:45'),
(138, '01955110016', 'bdtask', 'Doctor, Tanzil Ahmad. \r\nHello, Touhidul  Islam. \r\nYour ID: PAT1LAGN, Appointment ID: AI1EJ8QO, Serial: 10:00 - 10:30 and Appointment Date: 12 September 2019. \r\nThank you for the Appointment.', '2019-09-08 09:47:07'),
(139, '07484684826', 'bdtask', 'Hello, adding  patient. \r\nYour ID: P8ZCQMXI,  \r\nThank you for the registration.', '2019-09-09 01:00:31'),
(140, '07484684826', 'bdtask', 'Doctor, Joe Keller. \r\nHello, adding  patient. \r\nYour ID: P8ZCQMXI, Appointment ID: AIU2A2AS, Serial: 10:30 - 11:00 and Appointment Date: 10 September 2019. \r\nThank you for the Appointment.', '2019-09-09 01:07:56'),
(141, '1751194212', 'bdtask', 'Doctor, Joe Keller. \r\nHello, Md Tuhin. \r\nYour ID: PECW0N53, Appointment ID: AJ3LBC47, Serial: 07:00 - 07:20 and Appointment Date: 08 September 2019. \r\nThank you for the Appointment.', '2019-09-09 01:11:25'),
(142, '234324324', 'bdtask', 'Hello, Tanzil bdtask. \r\nYour ID: P2QWC5UN,  \r\nThank you for the registration.', '2019-09-11 08:55:33'),
(143, '432432423', 'bdtask', 'Hello, Tanzil bdtask. \r\nYour ID: P6BYBIP9,  \r\nThank you for the registration.', '2019-09-11 08:56:55'),
(144, '3432432423', 'bdtask', 'Hello, tanzil2 bdtask. \r\nYour ID: PI1XJ3FW,  \r\nThank you for the registration.', '2019-09-11 09:09:32'),
(145, '324343', 'bdtask', 'Hello, tanzil3 bdtask. \r\nYour ID: PPS3AFJB,  \r\nThank you for the registration.', '2019-09-11 09:13:32'),
(146, '3432432423', 'bdtask', 'Doctor, waj kaz. \r\nHello, tanzil2 bdtask. \r\nYour ID: PI1XJ3FW, Appointment ID: ASMH37XM, Serial: 13:00 - 13:30 and Appointment Date: 16 September 2019. \r\nThank you for the Appointment.', '2019-09-11 09:16:33'),
(147, '324343', 'bdtask', 'Doctor, waj kaz. \r\nHello, tanzil3 bdtask. \r\nYour ID: PPS3AFJB, Appointment ID: AK2JWVFO, Serial: 15:30 - 16:00 and Appointment Date: 16 September 2019. \r\nThank you for the Appointment.', '2019-09-11 09:23:24'),
(148, '23423432', 'bdtask', 'Hello, patientref bdtask. \r\nYour ID: PXL3B2DJ,  \r\nThank you for the registration.', '2019-09-11 09:29:26'),
(149, '23423432', 'bdtask', 'Doctor, waj kaz. \r\nHello, patientref bdtask. \r\nYour ID: PXL3B2DJ, Appointment ID: A0YQ1WI1, Serial: 17:00 - 17:30 and Appointment Date: 16 September 2019. \r\nThank you for the Appointment.', '2019-09-11 09:31:09'),
(150, '01876565432', 'bdtask', 'Hello, Rahim Khan. \r\nYour ID: PFR7U0FN,  \r\nThank you for the registration.', '2019-09-12 08:48:21'),
(151, '32452345', 'bdtask', 'Hello, Karim khan. \r\nYour ID: PSKNIKJ2,  \r\nThank you for the registration.', '2019-09-12 08:52:21'),
(152, '32452345', 'bdtask', 'Doctor, Sumch Mohammad Tarek. \r\nHello, Karim khan. \r\nYour ID: PSKNIKJ2, Appointment ID: ABOF92LI, Serial: 06:28 - 06:48 and Appointment Date: 12 September 2019. \r\nThank you for the Appointment.', '2019-09-12 09:00:13'),
(153, '01876565432', 'bdtask', 'Doctor, waj kaz. \r\nHello, Rahim Khan. \r\nYour ID: PFR7U0FN, Appointment ID: AYQVVKD1, Serial: 11:00 - 11:30 and Appointment Date: 16 September 2019. \r\nThank you for the Appointment.', '2019-09-15 10:29:27'),
(154, '32432423432', 'bdtask', 'Hello, patient referred. \r\nYour ID: PS5U4FVL,  \r\nThank you for the registration.', '2019-09-15 10:37:34'),
(155, '324324324', 'bdtask', 'Hello, 2nd patient kaz. \r\nYour ID: PG0VW841,  \r\nThank you for the registration.', '2019-09-15 10:42:48'),
(156, '324324324', 'bdtask', 'Doctor, waj kaz. \r\nHello, 2nd patient kaz. \r\nYour ID: PG0VW841, Appointment ID: AZVQSVW3, Serial: 13:30 - 14:00 and Appointment Date: 16 September 2019. \r\nThank you for the Appointment.', '2019-09-15 10:46:03'),
(157, '01857675727', 'bdtask', 'Hello, Rafiq hossain. \r\nYour ID: PZ1A14D7,  \r\nThank you for the registration.', '2019-09-16 07:23:05'),
(158, '45345345', 'bdtask', 'Hello, dfgdf dgfhdfgh. \r\nYour ID: PWKOEB8U,  \r\nThank you for the registration.', '2019-09-16 08:05:13'),
(159, '7484684826', 'bdtask', 'Hello, new patient. \r\nYour ID: PI27P99N,  \r\nThank you for the registration.', '2019-09-16 01:11:03'),
(160, '01857675727', 'bdtask', 'Doctor, Joe Keller. \r\nHello, Rafiq hossain. \r\nYour ID: PZ1A14D7, Appointment ID: AB8DPNM8, Serial: 09:00 - 09:30 and Appointment Date: 24 September 2019. \r\nThank you for the Appointment.', '2019-09-17 01:06:17'),
(161, '7484684826', 'bdtask', 'Hello, Wajeh ul Kazmi. \r\nYour ID: P8GSSZSF,  \r\nThank you for the registration.', '2019-09-17 10:56:02'),
(162, '7484684826', 'bdtask', 'Doctor, waj kaz. \r\nHello, new patient. \r\nYour ID: PI27P99N, Appointment ID: ANG42GQO, Serial: 09:20 - 09:40 and Appointment Date: 23 September 2019. \r\nThank you for the Appointment.', '2019-09-17 11:43:10');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `dprt_id` int(11) NOT NULL AUTO_INCREMENT,
  `main_id` int(6) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `flaticon` varchar(30) NOT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(200) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`dprt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`dprt_id`, `main_id`, `name`, `flaticon`, `description`, `image`, `branch_id`, `status`) VALUES
(16, 3, 'Oncology', 'uterus', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ipsum magna, gravida nec erat ac, malesuada pharetra felis. Phasellus eu dolor orci. Duis et dictum sem, sit amet sagittis dolor. Curabitur scelerisque, nunc eget viverra malesuada, nunc ligula tincidunt nisi, eget elementum urna magna at dui. Praesent eu tincidunt arcu. Ut imperdiet a ligula nec dapibus. Aliquam erat volutpat. Donec auctor elementum accumsan. Vestibulum velit augue, feugiat ac nisl in, pharetra accumsan ligula. Proin nunc mauris, ultrices quis tristique vitae, ornare quis nunc. Aenean ut tincidunt lorem. Maecenas consectetur faucibus velit, nec tincidunt nulla fermentum sed.\r\n\r\n', '', 0, 1),
(18, 3, 'Pharmacy', 'drug', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ipsum magna, gravida nec erat ac, malesuada pharetra felis. Phasellus eu dolor orci. Duis et dictum sem, sit amet sagittis dolor. Curabitur scelerisque, nunc eget viverra malesuada, nunc ligula tincidunt nisi, eget elementum urna magna at dui. Praesent eu tincidunt arcu. Ut imperdiet a ligula nec dapibus. Aliquam erat volutpat. Donec auctor elementum accumsan. Vestibulum velit augue, feugiat ac nisl in, pharetra accumsan ligula. Proin nunc mauris, ultrices quis tristique vitae, ornare quis nunc. Aenean ut tincidunt lorem. Maecenas consectetur faucibus velit, nec tincidunt nulla fermentum sed.\r\n\r\n', '', 0, 1),
(19, 1, 'Radiotherapy', 'herbal', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ipsum magna, gravida nec erat ac, malesuada pharetra felis. Phasellus eu dolor orci. Duis et dictum sem, sit amet sagittis dolor. Curabitur scelerisque, nunc eget viverra malesuada, nunc ligula tincidunt nisi, eget elementum urna magna at dui. Praesent eu tincidunt arcu. Ut imperdiet a ligula nec dapibus. Aliquam erat volutpat. Donec auctor elementum accumsan. Vestibulum velit augue, feugiat ac nisl in, pharetra accumsan ligula. Proin nunc mauris, ultrices quis tristique vitae, ornare quis nunc. Aenean ut tincidunt lorem. Maecenas consectetur faucibus velit, nec tincidunt nulla fermentum sed.\r\n\r\n', '', 0, 1),
(21, 3, 'Rheumatology', 'kidney-1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ipsum magna, gravida nec erat ac, malesuada pharetra felis. Phasellus eu dolor orci. Duis et dictum sem, sit amet sagittis dolor. Curabitur scelerisque, nunc eget viverra malesuada, nunc ligula tincidunt nisi, eget elementum urna magna at dui. Praesent eu tincidunt arcu. Ut imperdiet a ligula nec dapibus. Aliquam erat volutpat. Donec auctor elementum accumsan. Vestibulum velit augue, feugiat ac nisl in, pharetra accumsan ligula. Proin nunc mauris, ultrices quis tristique vitae, ornare quis nunc. Aenean ut tincidunt lorem. Maecenas consectetur faucibus velit, nec tincidunt nulla fermentum sed.\r\n\r\n', '', 0, 1),
(22, 1, 'Gynaecology', 'kidney-1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ipsum magna, gravida nec erat ac, malesuada pharetra felis. Phasellus eu dolor orci. Duis et dictum sem, sit amet sagittis dolor. Curabitur scelerisque, nunc eget viverra malesuada, nunc ligula tincidunt nisi, eget elementum urna magna at dui. Praesent eu tincidunt arcu. Ut imperdiet a ligula nec dapibus. Aliquam erat volutpat. Donec auctor elementum accumsan. Vestibulum velit augue, feugiat ac nisl in, pharetra accumsan ligula. Proin nunc mauris, ultrices quis tristique vitae, ornare quis nunc. Aenean ut tincidunt lorem. Maecenas consectetur faucibus velit, nec tincidunt nulla fermentum sed.\r\n\r\n', '', 0, 1),
(23, 3, 'Obstetrics', 'vitamins', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ipsum magna, gravida nec erat ac, malesuada pharetra felis. Phasellus eu dolor orci. Duis et dictum sem, sit amet sagittis dolor. Curabitur scelerisque, nunc eget viverra malesuada, nunc ligula tincidunt nisi, eget elementum urna magna at dui. Praesent eu tincidunt arcu. Ut imperdiet a ligula nec dapibus. Aliquam erat volutpat. Donec auctor elementum accumsan. Vestibulum velit augue, feugiat ac nisl in, pharetra accumsan ligula. Proin nunc mauris, ultrices quis tristique vitae, ornare quis nunc. Aenean ut tincidunt lorem. Maecenas consectetur faucibus velit, nec tincidunt nulla fermentum sed.\r\n\r\n', '', 0, 1),
(25, 4, 'General Surgery', 'tooth', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ipsum magna, gravida nec erat ac, malesuada pharetra felis. Phasellus eu dolor orci. Duis et dictum sem, sit amet sagittis dolor. Curabitur scelerisque, nunc eget viverra malesuada, nunc ligula tincidunt nisi, eget elementum urna magna at dui. Praesent eu tincidunt arcu. Ut imperdiet a ligula nec dapibus. Aliquam erat volutpat. Donec auctor elementum accumsan. Vestibulum velit augue, feugiat ac nisl in, pharetra accumsan ligula.', '', 0, 1),
(26, 3, 'Pregnancy', 'sperm-2', 'fgdfg ghfgh fghftg fhrtg dgfgdfgd tfds', '', 0, 1),
(27, 3, 'Surgery', 'surgery', 'Quis autem vel eum iriure dolor in…', '', 0, 1),
(28, 2, 'Psychology', 'focus', 'Quis autem vel eum iriure dolor in…', '', 0, 1),
(29, 1, 'Therapy', 'herbal', 'Quis autem vel eum iriure dolor in…', '', 0, 1),
(30, 2, 'Nursing', 'feeder', 'Quis autem vel eum iriure dolor in…', '', 0, 1),
(31, 2, 'Cardiology', 'heart', 'Quis autem vel eum iriure dolor in…', '', 0, 1),
(32, 2, 'X-rey', 'x-ray', 'Quis autem vel eum iriure dolor in…', 'assets_web/img/department/233437b73b6885be87fade77f3a93029.jpg', 1, 1),
(33, 3, 'geotherapy', 'neurology', 'demo testing', 'assets_web/img/department/933b8d1666490ae7f3727118bb56fa82.png', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `department_lang`
--

CREATE TABLE IF NOT EXISTS `department_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `language` varchar(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department_lang`
--

INSERT INTO `department_lang` (`id`, `department_id`, `language`, `name`, `description`, `status`) VALUES
(1, 32, 'english', 'X-rey', 'Extend point of care with new services to increase revenues', 1),
(2, 32, 'french', 'X-rey', 'Extension des points de service avec de nouveaux services pour augmenter les revenus', 1),
(3, 32, 'bangla', 'এক্স-রে', 'রাজস্ব বৃদ্ধি নতুন সেবা সঙ্গে যত্ন বিন্দু প্রসারিত', 1),
(4, 32, 'arabic', 'اكس ري', 'توسيع نقطة الرعاية مع الخدمات الجديدة لزيادة الإيرادات', 1),
(5, 31, 'french', 'Cardiologie', 'Un cardiologue est un médecin spécialement formé et habile à détecter, traiter et prévenir les maladies du cœur et des vaisseaux sanguins.', 1),
(6, 31, 'arabic', 'طب القلب', 'طبيب القلب هو طبيب ذو تدريب خاص ومهارة في اكتشاف وعلاج ومنع أمراض القلب والأوعية الدموية.', 1),
(7, 31, 'bangla', 'হৃদ্বিজ্ঞান', 'কার্ডিওলজিস্ট হ\'ল হৃদরোগ ও রক্তবাহী শাবকদের রোগ সনাক্ত, চিকিৎসা ও প্রতিরোধে বিশেষ প্রশিক্ষণ ও দক্ষতার সঙ্গে একটি ডাক্তার।', 1),
(8, 31, 'english', 'Cardiology', 'A cardiologist is a doctor with special training and skill in finding, treating and preventing diseases of the heart and blood vessels.', 1),
(9, 30, 'french', 'allaitement', 'Les infirmières sont l’un des groupes de professionnels les plus fiables aux États-Unis.', 1),
(10, 30, 'arabic', 'تمريض', 'الممرضات هي واحدة من أكثر مجموعات المهنيين الموثوق بهم في الولايات المتحدة.', 1),
(11, 30, 'english', 'Nursing', 'Nurses are one of the most trusted groups of professionals in the United States.', 1),
(12, 30, 'bangla', 'নার্সিং', 'নার্স মার্কিন যুক্তরাষ্ট্রে পেশাদারদের সবচেয়ে নির্ভরযোগ্য গ্রুপগুলির মধ্যে একটি।', 1),
(13, 29, 'english', 'Therapy', 'Psychotherapy is the practice of spending time with a trained professional—usually a psychologist, a social worker, or a licensed counselor', 1),
(14, 29, 'bangla', 'থেরাপি', 'সাইকোথেরাপি একজন প্রশিক্ষিত পেশাদারের সাথে সময় কাটাবার অভ্যাস - সাধারণত একজন মনোবিজ্ঞানী, একজন সামাজিক কর্মী, অথবা একটি লাইসেন্সপ্রাপ্ত কাউন্সিলর', 1),
(15, 29, 'arabic', 'علاج', 'العلاج النفسي هو ممارسة قضاء الوقت مع محترف مدرب - عادة ما يكون طبيب نفساني أو عامل اجتماعي أو مستشار مرخص', 1),
(16, 29, 'french', 'Thérapie', 'La psychothérapie consiste à passer du temps avec un professionnel qualifié - généralement un psychologue, un travailleur social ou un conseiller agréé.', 1),
(17, 28, 'bangla', 'মনোবিজ্ঞান', 'মনোবিজ্ঞান মন এবং আচরণ বৈজ্ঞানিক গবেষণা।', 1),
(18, 28, 'arabic', 'علم النفس', 'علم النفس هو الدراسة العلمية للعقل والسلوك.', 1),
(19, 28, 'french', 'Psychologie', 'La psychologie est l\'étude scientifique de l\'esprit et du comportement.', 1),
(20, 28, 'english', 'Psychology', 'Psychology is the scientific study of the mind and behavior.', 1),
(21, 27, 'bangla', 'সার্জারি', 'সার্জারি করার অনেক কারণ আছে। কিছু অপারেশন ব্যথা উপশম বা প্রতিরোধ করতে পারেন।', 1),
(22, 27, 'arabic', 'العملية الجراحية', 'هناك العديد من الأسباب لإجراء عملية جراحية. بعض العمليات يمكن أن تخفف الألم أو تمنعه.', 1),
(23, 27, 'french', 'Chirurgie', 'Il y a plusieurs raisons de subir une intervention chirurgicale. Certaines opérations peuvent soulager ou prévenir la douleur.', 1),
(24, 27, 'english', 'Surgery', 'There are many reasons to have surgery. Some operations can relieve or prevent pain.', 1),
(25, 26, 'english', 'Pregnancy', 'Congratulations, and welcome to your pregnancy! Here’s what to expect each week and how to have a healthy, happy pregnancy.', 1),
(26, 26, 'bangla', 'গর্ভাবস্থা', 'অভিনন্দন, এবং আপনার গর্ভাবস্থায় স্বাগতম! এখানে প্রতি সপ্তাহে কী আশা করা যায় এবং কীভাবে সুস্থ, সুখী গর্ভধারণ করা যায়।', 1),
(27, 26, 'arabic', 'حمل', 'مبروك ، ومرحبا بكم في الحمل! إليك ما يمكن توقعه كل أسبوع وكيفية الحصول على حمل صحي وسعيد.', 1),
(28, 26, 'french', 'Grossesse', 'Félicitations et bienvenue dans votre grossesse! Voici à quoi s\'attendre chaque semaine et comment avoir une grossesse saine et heureuse.', 1),
(29, 22, 'english', 'Gynaecology', 'The gynaecology service at UCLH, in association with The Institute for Women\'s Health', 1),
(30, 22, 'french', 'Gynécologie', 'Le service de gynécologie de l\'UCLH, en association avec l\'Institut pour la santé des femmes', 1),
(31, 22, 'arabic', 'طب النساء', 'خدمة أمراض النساء في UCLH ، بالتعاون مع معهد صحة المرأة', 1),
(32, 22, 'bangla', 'স্ত্রীরোগবিদ্যা', 'ইউসিএলএইচ-তে গাইনোকোলজি সেবা, ইনস্টিটিউট ফর উইমেনস হেলথের সহযোগিতায়', 1),
(33, 18, 'bangla', 'ফার্মেসী', 'এটি একটি ঔষধের দোকান, আপনি এখানে সব ধরনের ওষুধ কিনতে পারেন। কিছু বিদেশী ঔষধ এখানেও পাওয়া যায়। দ্রুত সেবা. শুধুমাত্র নগদ. কর্মীরা তাই ভাল।', 1),
(34, 18, 'english', 'Pharmacy', 'Hospital pharmacy is a specialised field of pharmacy which forms an integrated part of patient health care in a health facility.', 1),
(35, 18, 'french', 'Les pharmacies', 'Pharmacie hospitalière 2 Un domaine spécialisé de la pharmacie Hoch forme une partie intégrante des soins de santé dispensés aux patients dans un établissement de santé.', 1),
(36, 18, 'arabic', 'الصيدليات', 'صيدلية المستشفيات 2 حقل متخصص من الصيدلة أشكال Hoch غير متكاملة جزء من الرعاية الصحية للمرضى في المرفق الصحي.', 1),
(37, 19, 'english', 'Radiotherapy', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content her', 1),
(38, 19, 'bangla', 'রঁজনরশ্মি দ্বারা চিকিত্সা', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে। লোরম ইপসাম ব্যবহার করার বিষয়টি হল \'এখানে সামগ্রী, এখানে সামগ্রী\' ব্যবহার করার বিরোধিতা করে এটির অক্ষরগুলির কম বা কম সাধারণ বন্টন রয়েছ', 1),
(39, 19, 'arabic', 'المعالجة بالإشعاع', 'من الحقائق الثابتة منذ زمن بعيد أن القارئ سوف يصرفه المحتوى القابل للقراءة لصفحة عند النظر إلى تصميمه. إن الهدف من استخدام لوريم إيبسوم هو أن لديه توزيعًا عاديًا أو أكثر للرسائل ، بدلاً من استخدام \"المحتوى هنا ، المحتوى هنا\" ، مما يجعله يبدو وكأنه قا', 1),
(40, 19, 'french', 'Radiothérapie', 'C\'est un fait établi de longue date qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page. Lorem Ipsum a l\'avantage de présenter une distribution de lettres plus ou moins normale, par opposition à l\'utilisa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `hidden_attach_file` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `upload_by` int(11) DEFAULT NULL,
  `refer_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id`, `patient_id`, `doctor_id`, `description`, `hidden_attach_file`, `date`, `branch_id`, `upload_by`, `refer_by`) VALUES
(1, '543654', 1, '<p>DG DRFG DRGR</p>', './assets/attachments/c2f7ddb6bd23517b5a476eea84e2a2b2.jpg', '2018-08-08', 0, 1, NULL),
(2, 'P1XWEV6W', 50, 'test update', 'assets/attachments/8aa2d385defb4ee47a10ac686af0804e.jpg', '2018-12-24', 0, 0, NULL),
(6, 'P1XWEV6W', 50, 'test insert', 'assets/attachments/8aa2d385defb4ee47a10ac686af0804e.jpg', '2018-12-24', 0, 0, NULL),
(7, 'P5J5PQIL', 39, 'Signature', './assets/attachments/21a0b494bde86f9ec5042c41ccee6870.png', '2019-01-22', 0, 39, NULL),
(8, 'P5J5PQIL', 39, 'Patient will be doctor appointment', './assets/attachments/1480bcc5d9b435ee7ecbb62f47ea6b27.jpg', '2019-01-22', 0, 39, NULL),
(9, 'P1XWEV6W', 39, 'hello testing', './assets/attachments/b8261bb1878f6b1353e2435fa9a71dd1.jpg', '2019-01-22', 0, 0, NULL),
(10, 'P1XWEV6W', 39, 'ghfghfgh', './assets/attachments/c145c0ed27b84c463f591fc10dd00cde.jpg', '2019-01-22', 0, 0, NULL),
(11, 'P736D262', 38, 'scan 001', './assets/attachments/2175e3be294ccdc1529eb8f607378e3b.jpg', '2019-07-24', 0, 2, NULL),
(12, 'PGQG6XSK', 38, 'scan 001', './assets/attachments/8299038d0bae6474fbbc7a0f0a88b956.jpg', '2019-07-24', 0, 2, NULL),
(13, 'PXL3B2DJ', 81, 'Tests', './assets/attachments/1a0ca68204553a8fcaffba3da4805f5d.png', '2019-09-11', 3, 81, NULL),
(14, 'PSKNIKJ2', 83, 'demo', './assets/attachments/2ef62e27a21302c9fe11f590a4664b7c.png', '2019-09-12', 4, 83, NULL),
(15, 'PG0VW841', 81, 'Patient docs', './assets/attachments/d114798781877cc652973a999863e846.jpg', '2019-09-15', 3, 81, NULL),
(16, 'PZ1A14D7', 39, 'gfhgdgfh', './assets/attachments/959f048807e4ed09e340bfde983f7975.png', '2019-09-16', 3, 2, 90),
(17, 'PWKOEB8U', 81, 'sdfsd', './assets/attachments/f80c75b983181c2fdae31fa0d81018fb.png', '2019-09-16', 3, 96, 96),
(18, 'PG0VW841', 81, 'Blood test', './assets/attachments/a130a9a6943ebb6315ae1e4dfad0d6e6.jpg', '2019-09-16', 3, 86, 86),
(19, 'PI27P99N', 97, 'Blood group', './assets/attachments/5a4fea1ad80c265a8c311d63688e6a5e.png', '2019-09-17', 3, 97, NULL),
(20, 'PIX208Q2', 97, 'Blood test report', './assets/attachments/3a154cdae35d43db275bff1e93d90007.pdf', '2019-09-17', 3, 2, 0),
(21, 'PZ1A14D7', 97, 'Blood test report 2', './assets/attachments/711da301ae13bd53a4b55b1548788953.jpg', '2019-09-19', 3, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE IF NOT EXISTS `enquiry` (
  `enquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `subject` varchar(100) NOT NULL,
  `enquiry` text DEFAULT NULL,
  `checked` tinyint(1) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `checked_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`enquiry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`enquiry_id`, `email`, `phone`, `name`, `subject`, `enquiry`, `checked`, `ip_address`, `user_agent`, `checked_by`, `created_date`, `status`) VALUES
(1, 'alexa321@gmail.com', NULL, 'Alexa', 'Inquiry of Hospital Management SMS System ', 'Hi, \nMy name is Alexa. I am from USA and I want to buy your system. Please submit to me', 1, '162.158.165.122', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', 2, '2019-01-24 11:08:17', 1),
(2, 'james321@gmail.com', NULL, 'james', 'Report Generate Info', 'I want to all report information', NULL, '172.69.134.235', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', NULL, '2019-01-26 06:04:07', 1),
(3, 'asrafrahman@gmail.com', NULL, 'Ashraf Rahman Babul', 'Annual Prices', 'Hello,\nHere is a basic information for you system. Please tall me about this.', NULL, '162.158.165.122', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', NULL, '2019-01-26 06:05:50', 1),
(4, 'wajsh234@yahoo.com', NULL, 'Williamson', 'How can i install your system in my domain', 'How can i install your system in my domain, Please instruction me.', NULL, '172.68.144.239', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', NULL, '2019-01-26 06:07:53', 1),
(5, 'Linda1231@yahoo.com', NULL, 'Linda', 'Support for Account Manager', 'Hi,\nI am from USA & support for accounts.\nThank you.', NULL, '162.158.165.140', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', NULL, '2019-01-26 13:08:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ha_birth`
--

CREATE TABLE IF NOT EXISTS `ha_birth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ha_birth`
--

INSERT INTO `ha_birth` (`id`, `title`, `description`, `patient_id`, `doctor_id`, `date`, `branch_id`, `status`) VALUES
(1, 'Bdtask Hospital Limited1', 'tyrthtry', 'PK1DKERF', 48, '2019-01-20', 0, 1),
(2, 'test title', 'sdfdsf', 'PECW0N53', 38, '2019-08-27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ha_category`
--

CREATE TABLE IF NOT EXISTS `ha_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ha_category`
--

INSERT INTO `ha_category` (`id`, `name`, `description`, `status`) VALUES
(1, 'Gel/Ointment', '<p>all Gel/Ointment stores</p>', 1),
(2, 'Capsule', '<p>All Capsule stores</p>', 1),
(3, 'Syrup ', '<p>All syrup</p>', 1),
(4, 'Vaccine ', '<p>All Vaccine</p>', 1),
(5, 'Pills/Tablets', '<p>All tablets</p>', 1),
(6, 'Vasculsar', '<p>for heart gesculation</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ha_death`
--

CREATE TABLE IF NOT EXISTS `ha_death` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ha_death`
--

INSERT INTO `ha_death` (`id`, `title`, `description`, `patient_id`, `doctor_id`, `date`, `branch_id`, `status`) VALUES
(1, 'Bdtask Hospital Limited1', 'xcvvvxcvfdsff', 'PBKVP9IH', 48, '2019-01-20', 0, 1),
(2, 'Normal death', 'sfdfdsf', 'PECW0N53', 38, '2019-09-28', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ha_investigation`
--

CREATE TABLE IF NOT EXISTS `ha_investigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ha_investigation`
--

INSERT INTO `ha_investigation` (`id`, `title`, `description`, `patient_id`, `doctor_id`, `picture`, `date`, `branch_id`, `status`) VALUES
(1, 'Test Investigataion', 'sdfsadf', 'PECW0N53', 40, '', '2019-08-27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ha_medicine`
--

CREATE TABLE IF NOT EXISTS `ha_medicine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `generic_name` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `quantity` int(6) NOT NULL,
  `price` float NOT NULL,
  `manufactured_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `strength` varchar(30) NOT NULL,
  `shelf` varchar(20) NOT NULL,
  `pack_size` float NOT NULL,
  `box_quantity` float NOT NULL,
  `total_unit` float NOT NULL,
  `total_purchase_p` float NOT NULL,
  `total_saleprice` float NOT NULL,
  `unit_pur_price` float NOT NULL,
  `unit_sale_p` float NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ha_medicine`
--

INSERT INTO `ha_medicine` (`id`, `name`, `generic_name`, `category_id`, `type_id`, `description`, `quantity`, `price`, `manufactured_by`, `create_date`, `strength`, `shelf`, `pack_size`, `box_quantity`, `total_unit`, `total_purchase_p`, `total_saleprice`, `unit_pur_price`, `unit_sale_p`, `branch_id`, `status`) VALUES
(1, 'Napa', '', 5, 0, '<p>fdfdg</p>', 9, 2, 'Square', '2019-01-12', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 1),
(2, 'Cal-DS', 'Calcium', 2, 2, '<p>fsd</p>', 1, 8, '2', '2019-07-24', '400 mg', 'C', 20, 1, 20, 320, 360, 16, 18, 0, 1),
(3, 'Napa Extra', 'Paracitamol', 5, 1, '<p>Some text</p>', -21, 0, '1', '2019-07-21', '500mm', 'M', 12, 10, 120, 100, 120, 0.83, 1, 0, 1),
(4, 'Tuska', 'Paracitamol', 3, 1, '<p>sadfasdf</p>', 0, 0, '1', '2019-07-22', '200ml', 'N', 10, 1, 10, 800, 1000, 80, 100, 0, 1),
(5, 'Ace 500mg', 'Paracetamol BP', 5, 2, '<p>testing this</p>', 0, 0, '2', '2019-07-24', '500 mg', 'A', 100, 10, 1000, 800, 1000, 0.8, 1, 0, 1),
(6, 'parastemolw', 'yht ohi', 6, 5, '<p>medcin1</p>', 0, 0, '2', '2019-07-25', '', '', 3, 3, 9, 1, 5, 0.11, 0.56, 0, 1),
(7, 'Metrox', 'Mtx', 2, 2, '<p>dsfafd</p>', 0, 0, '3', '2019-08-24', '500mm', 'M', 10, 10, 100, 150, 200, 1.5, 2, 1, 1),
(8, 'Amodin', 'amodis', 5, 3, '<p>sdfsdf</p>', 0, 0, '6', '2019-08-28', '500mm', 'S', 10, 12, 120, 130, 150, 1.08, 1.25, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ha_operation`
--

CREATE TABLE IF NOT EXISTS `ha_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ha_operation`
--

INSERT INTO `ha_operation` (`id`, `title`, `description`, `patient_id`, `doctor_id`, `date`, `branch_id`, `status`) VALUES
(1, 'Test Operation', 'some text here', 'PECW0N53', 38, '2019-08-27', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inc_insurance`
--

CREATE TABLE IF NOT EXISTS `inc_insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insurance_name` varchar(255) DEFAULT NULL,
  `service_tax` varchar(50) DEFAULT NULL,
  `discount` varchar(50) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `insurance_no` varchar(50) DEFAULT NULL,
  `insurance_code` varchar(50) DEFAULT NULL,
  `disease_charge` text DEFAULT NULL COMMENT 'array(name, charge)',
  `hospital_rate` varchar(50) DEFAULT NULL,
  `insurance_rate` varchar(50) DEFAULT NULL,
  `total` varchar(50) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inc_insurance`
--

INSERT INTO `inc_insurance` (`id`, `insurance_name`, `service_tax`, `discount`, `remark`, `insurance_no`, `insurance_code`, `disease_charge`, `hospital_rate`, `insurance_rate`, `total`, `branch_id`, `status`) VALUES
(1, 'Alico', '10', '5', 'test', '212', '2323', '{\"Heart pain\":\"500\",\"Heart \":\"600\"}', '600', '500', '1000', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inc_limit_approval`
--

CREATE TABLE IF NOT EXISTS `inc_limit_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) DEFAULT NULL,
  `room_no` varchar(100) DEFAULT NULL,
  `disease_details` text DEFAULT NULL COMMENT 'name, description',
  `consultant_id` int(11) DEFAULT NULL COMMENT 'doctor list',
  `policy_name` varchar(255) DEFAULT NULL,
  `policy_no` varchar(100) DEFAULT NULL,
  `policy_holder_name` varchar(255) DEFAULT NULL,
  `insurance_id` int(11) DEFAULT NULL,
  `approval_breakup` text DEFAULT NULL COMMENT 'name, charge',
  `total` varchar(50) DEFAULT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inputoutput`
--

CREATE TABLE IF NOT EXISTS `inputoutput` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `type_of_input_and_amount` text NOT NULL,
  `input_time` time NOT NULL,
  `type_of_output_and_amount` text DEFAULT NULL,
  `output_time` time DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inputoutput`
--

INSERT INTO `inputoutput` (`id`, `patient_id`, `date`, `type_of_input_and_amount`, `input_time`, `type_of_output_and_amount`, `output_time`, `remarks`, `branch_id`, `assign_by`) VALUES
(2, 'PECW0N53', '2019-07-20', '1', '05:25:30', '2', '14:45:30', 'hhh', 0, '2'),
(3, 'PZRIMPJ4', '2019-07-20', 'Second', '03:30:30', 'dsfdsdf', '07:40:45', 'dsfasdf', 0, '2'),
(4, 'PECW0N53', '2019-08-24', '23424', '04:15:30', 'ttttt', '17:45:30', 'sdfsdf', 1, '2');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details_p`
--

CREATE TABLE IF NOT EXISTS `invoice_details_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `total` decimal(12,0) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `batch_id` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_details_p`
--

INSERT INTO `invoice_details_p` (`id`, `invoice_id`, `medicine_id`, `qty`, `rate`, `total`, `branch_id`, `batch_id`) VALUES
(3, 1, 3, 5, 1.00, 5, 0, '213423'),
(4, 1, 4, 2, 100.00, 200, 0, '3242342'),
(7, 2, 2, 10, 18.00, 180, 0, '112'),
(8, 2, 5, 20, 1.00, 20, 0, '112'),
(9, 3, 7, 10, 2.00, 20, 0, '3245234'),
(10, 3, 5, 20, 1.00, 20, 0, '112');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_p`
--

CREATE TABLE IF NOT EXISTS `invoice_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `patient_id` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `grandtotal` decimal(12,2) NOT NULL,
  `total_discount` decimal(8,0) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_p`
--

INSERT INTO `invoice_p` (`id`, `voucher_no`, `date`, `patient_id`, `total`, `grandtotal`, `total_discount`, `branch_id`, `assign_by`) VALUES
(1, '1000', '2019-07-23', 48, 205.00, 203.00, 2, 0, '2'),
(2, '1001', '2019-07-24', 48, 200.00, 195.00, 5, 0, '2'),
(3, '1002', '2019-08-24', 48, 40.00, 40.00, 0, 1, '2');

-- --------------------------------------------------------

--
-- Table structure for table `issue_details`
--

CREATE TABLE IF NOT EXISTS `issue_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `patient_id` text NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `batch_id` varchar(30) NOT NULL,
  `qty` float NOT NULL,
  `up` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issue_details`
--

INSERT INTO `issue_details` (`id`, `issue_id`, `date`, `patient_id`, `medicine_id`, `batch_id`, `qty`, `up`, `total`, `branch_id`) VALUES
(2, 1, '2019-07-31', 'PKK4W6QT', 5, '', 1, 0.00, 0.00, 0),
(3, 1, '2019-07-31', 'PK9QNQ5E', 5, '', 2, 0.00, 0.00, 0),
(4, 2, '2019-08-06', 'PK9QNQ5E', 4, '', 2, 0.00, 0.00, 0),
(7, 3, '2019-08-21', 'PG1E53IT', 5, '', 10, 1.00, 10.00, 1),
(8, 3, '2019-08-21', 'PK9QNQ5E', 5, '', 5, 1.00, 5.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `issue_p`
--

CREATE TABLE IF NOT EXISTS `issue_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` varchar(30) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `batch_id` varchar(30) NOT NULL,
  `expire_date` date NOT NULL,
  `assign_by` varchar(20) NOT NULL,
  `grandtotal` decimal(10,2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issue_p`
--

INSERT INTO `issue_p` (`id`, `voucher_no`, `medicine_id`, `batch_id`, `expire_date`, `assign_by`, `grandtotal`, `branch_id`) VALUES
(1, '1000', 5, '112', '2020-10-28', '2', 0.00, 0),
(2, '1001', 4, '3242342', '2019-10-31', '2', 0.00, 0),
(3, '1002', 5, '112', '2020-10-28', '2', 15.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `phrase` text NOT NULL,
  `english` text DEFAULT NULL,
  `arabic` text DEFAULT NULL,
  `bangla` text DEFAULT NULL,
  `french` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=867 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `phrase`, `english`, `arabic`, `bangla`, `french`) VALUES
(1, 'email', 'Email Address', 'البريد الإلكتروني', 'ই-মেইল', NULL),
(2, 'password', 'Password', 'كلمه السر', 'পাসওয়ার্ড', NULL),
(3, 'login', 'Log In', 'تسجيل الدخول', 'লগইন', NULL),
(4, 'incorrect_email_password', 'Incorrect Email/Password!', 'كلمة مرور غير صحيحة', 'ভুল ইমেইল পাসওয়ার্ড', NULL),
(5, 'user_role', 'User Role', 'دور المستخدم', 'ব্যবহারকারীর নিয়ম', NULL),
(6, 'please_login', 'Please Log In', 'الرجاء تسجيل الدخول', 'দয়া করে লগইন করুন', NULL),
(7, 'setting', 'Setting', 'ضبط', 'সেটিং', NULL),
(8, 'profile', 'Profile', 'الملف الشخصي', 'প্রোফাইল', NULL),
(9, 'logout', 'Log Out', 'الخروج', 'প্রস্থান', NULL),
(10, 'please_try_again', 'Please Try Again', 'حاول مرة اخرى', 'অনুগ্রহপূর্বক আবার চেষ্টা করুন', NULL),
(11, 'admin', 'Admin', 'مشرف', 'অ্যাডমিন', 'admin'),
(12, 'doctor', 'Doctor', 'طبيب', 'ডাক্তার', NULL),
(13, 'representative', 'Representative', 'وكيل', 'প্রতিনিধি', NULL),
(14, 'dashboard', 'Dashboard', 'لوحة القيادة', 'ড্যাশবোর্ড', NULL),
(15, 'department', 'Department', ' قسم، أقسام', 'বিভাগ', NULL),
(16, 'add_department', 'Add Department', 'أضف إلى القسم', 'বিভাগ যোগ করুন', 'Ajouter un département'),
(17, 'department_list', 'Department List', 'قائمة القسم', 'বিভাগ তালিকা', NULL),
(18, 'add_doctor', 'Add Doctor', 'اضافة طبيب', 'ডাক্তার যোগ করুন', 'Ajouter un docteur'),
(19, 'doctor_list', 'Doctor List', 'قائمة الطبيب', 'ডাক্তার তালিকা', NULL),
(20, 'add_representative', 'Add Representative', 'إضافة ممثل', 'প্রতিনিধি যোগ করুন', 'Ajouter un représentant'),
(21, 'representative_list', 'Representative List', 'قائمة تمثيلية', 'প্রতিনিধি তালিকা', NULL),
(22, 'patient', 'Patient', 'صبور', 'রোগী', NULL),
(23, 'add_patient', 'Add Patient', 'يضيف المريض', 'রোগী যোগ করুন', 'Ajouter un patient'),
(24, 'patient_list', 'Patient List', 'قائمة المريض', 'রোগীর তালিকা', NULL),
(25, 'schedule', 'Schedule', 'جدول', 'সময়সূচী ', NULL),
(26, 'add_schedule', 'Add Schedule', 'إضافة جدول', 'সময়সূচী যোগ করুন', 'ajouter un horaire'),
(27, 'schedule_list', 'Schedule List', 'قائمة الجدول الزمني', 'সময়সূচীর তালিকা', NULL),
(28, 'appointment', 'Appointment', 'موعد', 'এপয়েন্টমেন্ট', 'rendez-vous'),
(29, 'add_appointment', 'Add Appointment', 'إضافة موعد', 'অ্যাপয়েন্টমেন্ট যোগ করুন', 'Ajouter un rendez-vous'),
(30, 'appointment_list', 'Appointment List', 'قائمة التعيين', 'অ্যাপয়েন্টমেন্ট তালিকা', 'liste de rendez-vous'),
(31, 'enquiry', 'Enquiry', 'تحقيق', 'অনুসন্ধান', NULL),
(32, 'language_setting', 'Language Setting', 'إعدادات اللغة', 'ভাষা সেটিং', NULL),
(33, 'appointment_report', 'Appointment Report', 'تقرير موعد', 'অ্যাপয়েন্টমেন্ট রিপোর্ট', 'rapport de rendez-vous'),
(34, 'assign_by_all', 'Assign by All', 'تعيين من قبل الجميع', 'সবার দ্বারা  বরাদ্দ', 'assigner par tous'),
(35, 'assign_by_doctor', 'Assign by Doctor', 'تعيين من قبل الطبيب', 'ডাক্তার দ্বারা বরাদ্দ', 'assigner par le docteur'),
(36, 'assign_to_doctor', 'Assign to Doctor', 'تخصيص للطبيب', 'ডাক্তারকে  বরাদ্দ করা', ''),
(37, 'assign_by_representative', 'Assign by Representative', 'يعين من قبل ممثل', 'প্রতিনিধি দ্বারা বরাদ্দ', 'assigner par représentant'),
(38, 'report', 'Report', 'أبلغ عن', 'প্রতিবেদন', NULL),
(39, 'assign_by_me', 'Assign by Me', 'يعينني', 'আমার দ্বারা বরাদ্দ', 'assigner par moi'),
(40, 'assign_to_me', 'Assign to Me', 'يعين لي', 'আমাকে বরাদ্দ করুন', ''),
(41, 'website', 'Website', 'موقع الكتروني', 'ওয়েবসাইট', NULL),
(42, 'slider', 'Slider', 'المنزلق', 'স্লাইডার', NULL),
(43, 'section', 'Section', 'الجزء', 'অধ্যায়', NULL),
(44, 'section_item', 'Section Item', 'بند القسم', 'অধ্যায় আইটেম', NULL),
(45, 'comments', 'Comment', 'تعليقات', 'মন্তব্য', NULL),
(46, 'latest_enquiry', 'Latest Enquiry', 'آخر استفسار', 'সর্বশেষ তদন্ত', NULL),
(47, 'total_progress', 'Total Progress', 'التقدم الكلي', 'মোট অগ্রগতি', NULL),
(48, 'last_year_status', 'Showing status from the last year', 'حالة العام الماضي', 'গত বছরের অবস্থা', NULL),
(49, 'department_name', 'Department Name', 'اسم القسم', 'বিভাগের নাম', NULL),
(50, 'description', 'Description', 'وصف', 'বিবরণ', NULL),
(51, 'status', 'Status', 'الحالة', 'অবস্থা', NULL),
(52, 'active', 'Active', 'نشيط', 'সক্রিয়', 'Actif'),
(53, 'inactive', 'Inactive', 'غير نشط', 'নিষ্ক্রিয়', NULL),
(54, 'cancel', 'Cancel', 'إلغاء', 'বাতিল', ''),
(55, 'save', 'Save', 'حفظ', 'সংরক্ষণ করুন ', NULL),
(56, 'serial', 'SL', 'مسلسل', 'ক্রমিক', NULL),
(57, 'action', 'Action', 'عمل', 'কার্যকলাপ', 'Action'),
(58, 'edit', 'Edit ', 'تصحيح', 'সম্পাদন করা', NULL),
(59, 'delete', 'Delete', 'حذف', 'মুছে ফেলা', NULL),
(60, 'save_successfully', 'Save Successfully!', 'حفظ بنجاح', 'সফলভাবে সংরক্ষণ হয়েছে ', NULL),
(61, 'update_successfully', 'Update Successfully!', 'التحديث بنجاح', 'সফলভাবে আপডেট হয়েছে ', NULL),
(62, 'department_edit', 'Department Edit', 'تحرير القسم', 'বিভাগ সম্পাদনা', NULL),
(63, 'delete_successfully', 'Delete successfully!', 'حذف بنجاح', 'সফলভাবে মুছে ফেলা হয়েছে', NULL),
(64, 'are_you_sure', 'Are You Sure ? ', 'هل أنت واثق؟', 'তুমি কি নিশ্চিত?', 'êtes-vous sûr?'),
(65, 'first_name', 'First Name', 'الاسم الاول', 'নামের প্রথম অংশ', NULL),
(66, 'last_name', 'Last Name', 'الكنية', 'নামের শেষাংশ', NULL),
(67, 'phone', 'Phone No', 'هاتف', 'ফোন', NULL),
(68, 'mobile', 'Mobile No', 'التليفون المحمول', 'মোবাইল', NULL),
(69, 'blood_group', 'Blood Group', 'فصيلة الدم', 'রক্তের গ্রুপ', ''),
(70, 'gender', 'Gender', 'جنس', 'লিঙ্গ', NULL),
(71, 'date_of_birth', 'Date of Birth', 'تاريخ الولادة', 'জন্ম তারিখ', NULL),
(72, 'address', 'Address', 'عنوان', 'ঠিকানা', 'Adresse'),
(73, 'invalid_picture', 'Invalid Picture', 'الصورة غير صالحة', 'অবৈধ ছবি', NULL),
(74, 'doctor_profile', 'Doctor Profile', 'الملف الشخصي الطبيب', 'ডাক্তার প্রোফাইল', NULL),
(75, 'edit_profile', 'Edit Profile', 'تعديل الملف الشخصي', 'জীবন বৃত্তান্ত সম্পাদনা', NULL),
(76, 'edit_doctor', 'Edit Doctor', 'تحرير الطبيب', 'ডাক্তার সম্পাদনা করুন', NULL),
(77, 'designation', 'Designation', 'تعيين', 'উপাধি', NULL),
(78, 'short_biography', 'Short Biography', 'سيرة ذاتية قصيرة', 'সংক্ষিপ্ত জীবনী', NULL),
(79, 'picture', 'Picture', 'صورة', 'ছবি', NULL),
(80, 'specialist', 'Specialist', 'متخصص', 'বিশেষজ্ঞ', NULL),
(81, 'male', 'Male', 'الذكر', 'পুরুষ', NULL),
(82, 'female', 'Female', 'إناثا', 'মহিলা ', NULL),
(83, 'education_degree', 'Education/Degree', 'درجة التعليم', 'শীক্ষাগত ডিগ্রি', NULL),
(84, 'create_date', 'Create Date', 'تاريخ الإنشاء', 'তারিখ তৈরি করুন', NULL),
(85, 'view', 'View', 'رأي', 'দেখা', NULL),
(86, 'doctor_information', 'Doctor Information', 'معلومات الطبيب', 'ডাক্তার তথ্য', NULL),
(87, 'update_date', 'Update Date', 'تاريخ التحديث', 'আপডেট তারিখ', NULL),
(88, 'print', 'Print', 'طباعة', 'ছাপা', NULL),
(89, 'representative_edit', 'Representative Edit', 'تحرير ممثل', 'প্রতিনিধি সম্পাদনা', NULL),
(90, 'patient_information', 'Patient Information', 'معلومات المريض', 'রোগীর তথ্য', NULL),
(91, 'other', 'Other', 'آخر', 'অন্যান্য', NULL),
(92, 'patient_id', 'Patient ID', 'رقم المريض', 'রোগী আইডি', NULL),
(93, 'age', 'Age', 'عمر', 'বয়স', 'âge'),
(94, 'patient_edit', 'Patient Edit', 'تعديل المريض', 'রোগী সম্পাদনা', NULL),
(95, 'id_no', 'ID No.', 'رقم بطاقة الهوية', 'আইডি নাম্বার', NULL),
(96, 'select_option', 'Select Option', 'حدد الخيار', 'বিকল্প নির্বাচন', NULL),
(97, 'doctor_name', 'Doctor Name', 'اسم الطبيب', 'ডাক্তার নাম', NULL),
(98, 'day', 'Day', 'يوم', 'দিন', NULL),
(99, 'start_time', 'Start Time', 'وقت البدء', 'সময় শুরু', NULL),
(100, 'end_time', 'End Time', 'وقت النهاية', 'শেষ সময়', NULL),
(101, 'per_patient_time', 'Per Patient Time', 'لكل مريض', 'প্রতি রোগীর সময়', NULL),
(102, 'serial_visibility_type', 'Serial Visibility', 'نوع الرؤية التسلسلية', 'সিরিয়াল দৃশ্যমানতা টাইপ', NULL),
(103, 'sequential', 'Sequential', 'تسلسلي', 'ক্রমানুসারে', NULL),
(104, 'timestamp', 'Timestamp', 'الطابع الزمني', 'টাইমস্ট্যাম্প', NULL),
(105, 'available_days', 'Available Days', 'أيام المتاحة', 'সহজলভ্য দিন', ''),
(106, 'schedule_edit', 'Schedule Edit', 'تحرير الجدول الزمني', 'সময়সূচী সম্পাদনা', NULL),
(107, 'available_time', 'Available Time', 'الوقت المتاح', 'সহজলভ্য সময়', ''),
(108, 'serial_no', 'Serial No', 'الرقم التسلسلي', 'ক্রমিক নং', NULL),
(109, 'problem', 'Problem', 'مشكلة', 'সমস্যা', NULL),
(110, 'appointment_date', 'Appointment Date', 'موعد الموعد', 'সাক্ষাৎকারের তারিখ', 'date de rendez-vous'),
(111, 'you_are_already_registered', 'You are already registered!', 'انت مسجل مسبقا', 'আপনি ইতোমধ্যে নিবন্ধিত', NULL),
(112, 'invalid_patient_id', 'Invalid patient ID', 'معرف المريض غير صالح', 'অবৈধ রোগী আইডি', NULL),
(113, 'invalid_input', 'Invalid Input', 'مدخل غير صالح', 'ভুল ইনপুট', NULL),
(114, 'no_doctor_available', 'No Doctor Available', 'لا يوجد طبيب متاح', 'কোন ডাক্তার পাওয়া যায় না', NULL),
(115, 'invalid_department', 'Invalid Department!', 'قسم غير صالح', 'অবৈধ বিভাগ', NULL),
(116, 'no_schedule_available', 'No Schedule Available', 'لا يوجد جدول متاح', 'কোন সময়সূচী পাওয়া যায় নাই ', NULL),
(117, 'please_fillup_all_required_fields', 'Please fillup all required filelds', 'رجاءا، إملأ جميع الحقول المطلوبة', 'সব প্রয়োজনীয় ফিল্ডগুলি পূরণ করুন', NULL),
(118, 'appointment_id', 'Appointment ID', 'معرف الموعد', 'অ্যাপয়েন্টমেন্ট আইডি', 'identifiant de rendez-vous'),
(119, 'schedule_time', 'Schedule Time', 'جدول الوقت', 'তফসিল সময়', NULL),
(120, 'appointment_information', 'Appointment Information', 'معلومات التعيين', 'অ্যাপয়েন্টমেন্ট তথ্য', 'informations de rendez-vous'),
(121, 'full_name', 'Full Name', 'الاسم الكامل', 'পুরো নাম', NULL),
(122, 'read_unread', 'Read / Unread', 'قراءة غير مقروءة', 'অপঠিত পড়া', NULL),
(123, 'date', 'Date', 'تاريخ', 'তারিখ', NULL),
(124, 'ip_address', 'IP Address', 'عنوان IP', 'আইপি ঠিকানা', NULL),
(125, 'user_agent', 'User Agent', 'وكيل المستخدم', 'ব্যবহারকারী এজেন্ট ', NULL),
(126, 'checked_by', 'Checked By', 'فحص بواسطة', 'চেক', NULL),
(127, 'enquiry_date', 'Enquirey Date', 'تاريخ الاستفسار', 'তদন্ত তারিখ', NULL),
(128, 'enquiry_list', 'Enquiry List', 'قائمة استفسار', 'তদন্ত তালিকা', NULL),
(129, 'filter', 'Filter', 'منقي', 'ফিল্টার', NULL),
(130, 'start_date', 'Start Date', 'تاريخ البدء', 'শুরুর তারিখ', NULL),
(131, 'end_date', 'End Date', 'تاريخ الانتهاء', 'শেষ তারিখ', NULL),
(132, 'application_title', 'Application Title', 'عنوان التطبيق', 'আবেদনের শিরোনাম', 'Titre de l\'application'),
(133, 'favicon', 'Favicon', 'فافيكون', 'ফেভিকন', NULL),
(134, 'logo', 'Logo', 'شعار', 'লোগো', NULL),
(135, 'footer_text', 'Footer Text', 'نص التذييل', 'ফুটার টেক্সট ', NULL),
(136, 'language', 'Language', 'لغة', 'ভাষা', NULL),
(137, 'appointment_assign_by_all', 'Appointment Assign by All', 'تعيين موعد من قبل الجميع', 'অ্যাপয়েন্টমেন্ট সকলের দ্বারা বরাদ্দ', 'rendez-vous assigner par tous'),
(138, 'appointment_assign_by_doctor', 'Appointment Assign by Doctor', 'تعيين موعد من قبل الطبيب', 'অ্যাপয়েন্টমেন্ট ডাক্তার দ্বারা বরাদ্দ', 'rendez-vous assigner par le docteur'),
(139, 'appointment_assign_by_representative', 'Appointment Assign by Representative', 'تعيين موعد من قبل ممثل', 'অ্যাপয়েন্টমেন্ট প্রতিনিধি দ্বারা বরাদ্দ', 'rendez-vous assigner par le représentant'),
(140, 'appointment_assign_to_all_doctor', 'Appointment Assign to All Doctor', 'تعيين موعد لجميع الأطباء', 'ডাক্তারদের সকল বরাদ্দ অ্যাপয়েন্টমেন্ট  ', 'rendez-vous assigner à tout le docteur'),
(141, 'appointment_assign_to_me', 'Appointment Assign to Me', 'تعيين موعد لي', 'আমার বরাদ্দ অ্যাপয়েন্টমেন্ট ', 'rendez-vous me assigner'),
(142, 'appointment_assign_by_me', 'Appointment Assign by Me', 'تعيين موعد من قبلي', 'অ্যাপয়েন্টমেন্ট আমার দ্বারা বরাদ্দ', 'rendez-vous assigner par moi'),
(143, 'type', 'Type', 'نوع', 'টাইপ', NULL),
(144, 'website_title', 'Website Title', 'عنوان الموقع', 'ওয়েবসাইট শিরোনাম', NULL),
(145, 'invalid_logo', 'Invalid Logo', 'شعار غير صالح', 'অবৈধ লোগো', NULL),
(146, 'details', 'Details', 'تفاصيل', 'বিস্তারিত', NULL),
(147, 'website_setting', 'Website Setting', 'إعداد موقع الويب', 'ওয়েবসাইট সেটিং', NULL),
(148, 'submit_successfully', 'Submit Successfully!', 'تقديم بنجاح', 'সফলভাবে জমা হয়েছে ', NULL),
(149, 'application_setting', 'Application Setting', 'اعدادات التطبيق', 'অ্যাপ্লিকেশন সেটিং', 'réglage de l\'application'),
(150, 'invalid_favicon', 'Invalid Favicon', 'رمز غير صالح', 'অবৈধ ফেভিকন', NULL),
(151, 'new_enquiry', 'New Enquiry', 'تحقيق جديد', 'নতুন তদন্ত', NULL),
(152, 'information', 'Information', 'معلومات', 'তথ্য', NULL),
(153, 'home', 'Home', 'الصفحة الرئيسية', 'হোম', NULL),
(154, 'select_department', 'Select Department', 'اختر القسم', 'বিভাগ নির্বাচন করুন', NULL),
(155, 'select_doctor', 'Select Doctor', 'اختر الطبيب', 'ডাক্তার নির্বাচন করুন', NULL),
(156, 'select_representative', 'Select Representative', 'اختر ممثل', 'প্রতিনিধি নির্বাচন করুন', NULL),
(157, 'post_id', 'Post ID', 'بعد معرف', 'পোস্ট আইডি', NULL),
(158, 'thank_you_for_your_comment', 'Thank you for your comment!', 'شكرا لك على تعليقك', 'আপনার মন্তব্যের জন্য ধন্যবাদ', NULL),
(159, 'comment_id', 'Comment ID', 'معرف التعليق', 'মন্তব্য আইডি', NULL),
(160, 'comment_status', 'Comment Status', 'حالة التعليق', 'মন্তব্যের অবস্থা', NULL),
(161, 'comment_added_successfully', 'Comment Added Successfully', 'تمت إضافة تعليق بنجاح', 'মন্তব্য সফলভাবে যোগ করা হয়েছে', NULL),
(162, 'comment_remove_successfully', 'Comment Remove Successfully', 'تعليق إزالة بنجاح', 'মন্তব্য সফলভাবে মুছে ফেলা হয়েছে', NULL),
(163, 'select_item', 'Section Item', 'اختر البند', 'আইটেম নির্বাচন করুন', NULL),
(164, 'add_item', 'Add Item', 'اضافة عنصر', 'আইটেম যোগ করুন', 'Ajouter des objets'),
(165, 'menu_name', 'Menu Name', 'اسم القائمة', 'মেনু নাম', NULL),
(166, 'title', 'Title', 'عنوان', 'খেতাব', NULL),
(167, 'position', 'Position', 'موضع', 'অবস্থান', NULL),
(168, 'invalid_icon_image', 'Invalid Icon Image!', 'صورة رمز غير صالح', 'অবৈধ আইকন ছবি', NULL),
(169, 'about', 'About', 'حول', 'সম্পর্কে', 'À propos de'),
(170, 'blog', 'Blog', 'مدونة', 'ব্লগ', ''),
(171, 'service', 'Service', 'الخدمات', 'সেবা', NULL),
(172, 'item_edit', 'Item Edit', 'تحرير العنصر', 'আইটেম সম্পাদনা', NULL),
(173, 'registration_successfully', 'Registration Successfully', 'التسجيل بنجاح', 'নিবন্ধন সফল', NULL),
(174, 'add_section', 'Add Section', 'إضافة قسم', 'বিভাগ যোগ করুন', 'ajouter une section'),
(175, 'section_name', 'Section Name', 'اسم القسم', 'বিভাগের নাম', NULL),
(176, 'invalid_featured_image', 'Invalid Featured Image!', 'صورة مميزة غير صالحة', 'অবৈধ বৈশিষ্ট্যযুক্ত ইমেজ', NULL),
(177, 'section_edit', 'Section Edit', 'تحرير القسم', 'অধ্যায় সম্পাদনা ', NULL),
(178, 'meta_description', 'Meta Description', 'ميتا الوصف', 'মেটা বর্ণনা', NULL),
(179, 'twitter_api', 'Twitter Api', 'تويتر api', 'টুইটার এপিই ', NULL),
(180, 'google_map', 'Google Map', 'خرائط جوجل', 'গুগল মানচিত্র', NULL),
(181, 'copyright_text', 'Copyright Text', 'نص حقوق النشر', 'কপিরাইট টেক্সট', NULL),
(182, 'facebook_url', 'Facebook URL', 'URL الفيسبوك', 'ফেসবুক ইউআরএল', NULL),
(183, 'twitter_url', 'Twitter URL', 'تغريد رابط', 'টুইটার ইউআরএল', NULL),
(184, 'vimeo_url', NULL, 'Vimeo url', 'Vimeo ইউআরএল', NULL),
(185, 'instagram_url', 'Instagram Url', 'انستغرام رابط', 'Instagram ইউআরএল', NULL),
(186, 'dribbble_url', 'Dribbble URL', 'رابط dribbble', 'ড্রিবল ইউআরএল', NULL),
(187, 'skype_url', 'Skype URL', 'سكايب رابط', 'স্কাইপ ইউআরএল', NULL),
(188, 'add_slider', 'Add Slider', 'أضف شريط التمرير', 'স্লাইডার যোগ করুন', 'ajouter un curseur'),
(189, 'subtitle', 'Sub Title', 'عنوان فرعي', 'বিকল্প নাম', NULL),
(190, 'slide_position', 'Slide Position', 'موقف المنزلق', 'স্লাইড অবস্থান', NULL),
(191, 'invalid_image', 'Invalid Image', 'صورة غير صالحة', 'অবৈধ ছবি', NULL),
(192, 'image_is_required', 'Image is required', 'الصورة مطلوبة', 'ছবি প্রয়োজন', NULL),
(193, 'slider_edit', 'Slider Edit', 'تحرير المنزلق', 'স্লাইডার সম্পাদনা', NULL),
(194, 'meta_keyword', 'Meta Keyword', 'الكلمة الفوقية', 'মেটা শব্দ', NULL),
(195, 'year', 'Year', 'عام', 'বছর', NULL),
(196, 'month', 'Month', 'شهر', 'মাস', NULL),
(197, 'recent_post', 'Recent Post', 'المنشور الاخير', 'সাম্প্রতিক পোস্ট', NULL),
(198, 'leave_a_comment', 'Leave a Comment', 'اترك تعليقا', 'মতামত দিন', NULL),
(199, 'submit', 'Submit', 'خضع', 'জমা দিন', NULL),
(200, 'google_plus_url', 'Google Plus URL', 'جوجل بلس رابط', 'গুগল প্লাস ইউআরএল', NULL),
(201, 'website_status', 'Website Status', 'حالة موقع الويب', 'ওয়েবসাইট অবস্থা', NULL),
(202, 'new_slide', 'New Slide', 'شريحة جديدة', 'নতুন স্লাইড', NULL),
(203, 'new_section', 'New Section', 'قسم جديد', 'নতুন অধ্যায়', NULL),
(204, 'subtitle_description', 'Sub Title / Description', 'وصف العنوان الفرعي', 'সাবটাইটেল বিবরণ', NULL),
(205, 'featured_image', 'Featured Image', 'صورة مميزة', 'বৈশিষ্ট্যযুক্ত ইমেজ', NULL),
(206, 'new_item', 'New Item', 'عنصر جديد', 'নতুন আইটেম ', NULL),
(207, 'item_position', 'Item Position', 'موقف البند', 'position', NULL),
(208, 'icon_image', 'Icon Image', 'صورة الرمز', 'আইকন ছবি', NULL),
(209, 'post_title', 'Post Title', 'عنوان المشاركة', 'পোস্ট শিরোনাম', NULL),
(210, 'add_to_website', 'Add to Website', 'أضف إلى الموقع', 'ওয়েবসাইট এ যোগ করুন', 'ajouter au site'),
(211, 'read_more', 'Read More', 'قراءة المزيد', 'আরো পড়ুন', NULL),
(212, 'registration', 'Registration', 'التسجيل', 'নিবন্ধন', NULL),
(213, 'appointment_form', 'Appointment Form', 'نموذج التعيين', 'অ্যাপয়েন্টমেন্ট ফর্ম', 'formulaire de rendez-vous'),
(214, 'contact', 'Contact', 'اتصل', 'যোগাযোগ', NULL),
(215, 'optional', 'Optional', 'اختياري', 'ঐচ্ছিক', NULL),
(216, 'customer_comments', 'Customer Comments', 'تعليقات العملاء', 'গ্রাহক মন্তব্য', NULL),
(217, 'need_a_doctor_for_checkup', 'Need a Doctor for Check-up?', 'بحاجة الى طبيب للفحص', 'চেকআপ জন্য একটি ডাক্তার প্রয়োজন', NULL),
(218, 'just_make_an_appointment_and_you_are_done', 'JUST MAKE AN APPOINTMENT & YOU\'RE DONE ! ', 'مجرد تحديد موعد وانتهيت', 'শুধু একটি অ্যাপয়েন্টমেন্ট করুন এবং আপনার সম্পন্ন হবে ', NULL),
(219, 'get_an_appointment', 'Get an appointment', 'الحصول على موعد', 'একটি অ্যাপয়েন্টমেন্ট পেতে', NULL),
(220, 'latest_news', 'Latest News', 'أحدث الأخبار', 'সর্বশেষ সংবাদ', NULL),
(221, 'latest_tweet', 'Latest Tweet', 'أحدث تغريدة', 'সর্বশেষ টুইট', NULL),
(222, 'menu', 'Menu', 'قائمة طعام', 'মেনু', NULL),
(223, 'administrative_user', 'Administrative User', 'مستخدم إداري', 'প্রশাসনিক ব্যবহারকারী', 'utilisateur administratif'),
(224, 'site_align', 'Website Align', 'محاذاة الموقع', 'সাইট সারিবদ্ধ', NULL),
(225, 'right_to_left', 'Right to Left', 'من اليمين الى اليسار', 'ডান থেকে বাম', NULL),
(226, 'left_to_right', 'Left to Right', 'من اليسار إلى اليمين', 'বাম থেকে ডান', NULL),
(227, 'account_manager', 'Account Manager', 'إدارة حساب المستخدم', 'একাউন্ট ম্যানেজার', 'Directeur de compte'),
(228, 'add_invoice', 'Add Invoice', 'أضف فاتورة', 'চালান যোগ করুন', 'Ajouter une facture'),
(229, 'invoice_list', 'Invoice List', 'قائمة الفاتورة', 'চালান তালিকা', NULL),
(230, 'account_list', 'Account List', 'قائمة الحسابات', 'অ্যাকাউন্ট তালিকা', 'Liste de compte'),
(231, 'add_account', 'Add Account', 'إضافة حساب', 'অ্যাকাউন্ট যোগ করা', 'Ajouter un compte'),
(232, 'account_name', 'Account Name', 'أسم الحساب', 'অ্যাকাউন্ট নাম', 'Nom du compte'),
(233, 'credit', 'Credit', 'ائتمان', 'ক্রেডিট', NULL),
(234, 'debit', 'Debit', 'مدين', 'ডেবিট', NULL),
(235, 'account_edit', 'Account Edit', 'تحرير الحساب', 'অ্যাকাউন্ট সম্পাদনা', 'Compte modifier'),
(236, 'quantity', 'Quantity', 'كمية', 'পরিমাণ', NULL),
(237, 'price', 'Price', 'السعر', 'মূল্য', NULL),
(238, 'total', 'Total', 'مجموع', 'মোট', NULL),
(239, 'remove', 'Remove', 'إزالة', 'অপসারণ', NULL),
(240, 'add', 'Add', 'إضافة', 'যোগ করা', 'Ajouter'),
(241, 'subtotal', 'Sub Total', 'حاصل الجمع', 'উপসমষ্টি', NULL),
(242, 'vat', 'Vat', 'برميل', 'ভ্যাট', NULL),
(243, 'grand_total', 'Grand Total', 'المجموع الكلي', 'সর্বমোট', NULL),
(244, 'discount', 'Discount', 'خصم', 'ডিসকাউন্ট', NULL),
(245, 'paid', 'Paid', 'دفع', 'অর্থ প্রদান', NULL),
(246, 'due', 'Due', 'بسبب', 'বাকি', NULL),
(247, 'reset', 'Reset', 'إعادة تعيين', 'রিসেট', NULL),
(248, 'add_or_remove', 'Add / Remove', 'أضف أو أزل', 'যোগ অথবা অপসারণ', 'Ajouter votre suppression'),
(249, 'invoice', 'Invoice', 'فاتورة', 'চালান', NULL),
(250, 'invoice_information', 'Invoice Information', 'معلومات الفاتورة', 'চালান তথ্য', NULL),
(251, 'invoice_edit', 'Invoice Edit', 'تحرير الفاتورة', 'চালান সম্পাদনা', NULL),
(252, 'update', 'Update', 'تحديث', 'হালনাগাদ', NULL),
(253, 'all', 'All', 'الكل', 'সব', 'toutes'),
(254, 'patient_wise', 'Patient Wise', 'مريض حكيم', 'রোগী অনুযায়ী', NULL),
(255, 'account_wise', 'Account Wise', 'حساب حكيم', 'অ্যাকাউন্ট অনুযায়ী', 'Âge du compte'),
(256, 'debit_report', 'Debit Report', 'تقرير الخصم', 'ডেবিট রিপোর্ট', NULL),
(257, 'credit_report', 'Credit Report', 'تقرير الائتمان', 'ক্রেডিট রিপোর্ট', NULL),
(258, 'payment_list', 'Payment List', 'قائمة الدفع', 'পেমেন্ট তালিকা', NULL),
(259, 'add_payment', 'Add Payment', 'إضافة الدفع', 'পেমেন্ট যোগ করুন', 'Ajouter un paiement'),
(260, 'payment_edit', 'Payment Edit', 'تحرير الدفع', 'পেমেন্ট সম্পাদনা', NULL),
(261, 'pay_to', 'Pay To', 'دفع ل', 'পরিশোধ', NULL),
(262, 'amount', 'Amount', 'كمية', 'পরিমাণ', 'montante'),
(263, 'bed_type', 'Bed Type', 'نوع السرير', 'বিছানা টাইপ', ''),
(264, 'bed_limit', 'Bed Capacity', 'الحد من السرير', 'বিছানা সীমা', ''),
(265, 'charge', 'Charge', 'الشحنة', 'মূল্য', NULL),
(266, 'bed_list', 'Bed List', 'قائمة السرير', 'বিছানা তালিকা', ''),
(267, 'add_bed', 'Add Bed', 'أضف السرير', 'বিছানা যোগ করুন', 'Ajouter un lit'),
(268, 'bed_manager', 'Bed Manager', 'مدير السرير', 'বিছানা ম্যানেজার', ''),
(269, 'bed_edit', 'Bed Edit', 'تحرير السرير', 'বিছানা সম্পাদনা', ''),
(270, 'bed_assign', 'Bed Assign', 'تخصيص السرير', 'বিছানা বরাদ্দ', ''),
(271, 'assign_date', 'Assign Date', 'تاريخ التعيين', 'তারিখ নির্ধারণ করুন', ''),
(272, 'discharge_date', 'Discharge Date', 'تاريخ التفريغ', 'নির্গমন তারিখ', NULL),
(273, 'bed_assign_list', 'Bed Assign List', 'قائمة تخصيص السرير', 'বিছানা বরাদ্দ তালিকা', ''),
(274, 'assign_by', 'Assign By', 'تعيين بواسطة', 'দ্বারা বরাদ্দ করা', 'assigner par'),
(275, 'bed_available', 'Bed is Available', 'سرير المتاحة', 'বিছানা সহজলভ্য ', ''),
(276, 'bed_not_available', 'Bed is Not Available', 'السرير غير متوفر', 'বিছানা খালি নাই ', ''),
(277, 'invlid_input', 'Invalid Input', 'مدخل غير صالح', 'ভুল ইনপুট', NULL),
(278, 'allocated', 'Allocated', 'تخصيص', 'বরাদ্দ', 'alloué'),
(279, 'free_now', 'Free', 'حر الآن', 'বিনামূল্যে এখন', NULL),
(280, 'select_only_avaiable_days', 'Select Only Avaiable Days', 'اختر الأيام المتاحة فقط', 'শুধুমাত্র ফ্রী  দিন নির্বাচন করুন', NULL),
(281, 'human_resources', 'Human Resources', 'الموارد البشرية', 'মানব সম্পদ', NULL),
(282, 'nurse_list', 'Nurse List', 'قائمة الممرضات', 'নার্স তালিকা', NULL),
(283, 'add_employee', 'Add Employee', 'إضافة موظف', 'কর্মচারী যোগ করুন', 'Ajouter un employé'),
(284, 'user_type', 'User Type', 'نوع المستخدم', 'ব্যবহারকারীর ধরন', NULL),
(285, 'employee_information', 'Employee Information', 'معلومات الموظف', 'কর্মীদের তথ্য', NULL),
(286, 'employee_edit', 'Edit Employee', 'تعديل الموظف', 'কর্মচারী সম্পাদনা', NULL),
(287, 'laboratorist_list', 'Laboratorist List', 'قائمة المختبرين', 'পরীক্ষাগার তালিকা', NULL),
(288, 'employee_list', 'Employee List', 'قائمة موظف', 'কর্মচারী তালিকা', NULL),
(289, 'receptionist_list', 'Receptionist List', 'قائمة استقبال', 'অভ্যর্থনাকারী তালিকা', NULL),
(290, 'pharmacist_list', 'Pharmacist List', 'قائمة الصيدلي', 'ফার্মাসিস্ট তালিকা', NULL),
(291, 'nurse', 'Nurse', 'ممرضة', 'নার্স', NULL),
(292, 'laboratorist', 'Laboratorist', 'قائمة', 'পরীক্ষাগার', NULL),
(293, 'pharmacist', 'Pharmacist', 'صيدلاني', 'কম্পউণ্ডার', NULL),
(294, 'accountant', 'Accountant', 'محاسب', 'হিসাবরক্ষক', 'Comptable'),
(295, 'receptionist', 'Receptionist', 'موظف الإستقبال', 'অভ্যর্থনাকারী ', NULL),
(296, 'noticeboard', 'Noticeboard', 'لوح الإعلانات', 'নোটিশ বোর্ড', NULL),
(297, 'notice_list', 'Notice List', 'قائمة إشعار', 'নোটিশ তালিকা', NULL),
(298, 'add_notice', 'Add Notice', 'إضافة إشعار', 'নোটিশ যোগ করুন', 'Ajouter un avis'),
(299, 'hospital_activities', 'Hospital Activities', 'أنشطة المستشفى', 'হাসপাতাল কার্যক্রম', NULL),
(300, 'death_report', 'Death Report', 'تقرير الموت', 'মৃত্যুর রিপোর্ট', NULL),
(301, 'add_death_report', 'Add Death Report', 'إضافة تقرير الموت', 'মৃত্যু রিপোর্ট যোগ করুন', 'Ajouter un rapport de décès'),
(302, 'death_report_edit', 'Death Report Edit', 'تقرير الموت تحرير', 'মৃত্যু রিপোর্ট সম্পাদনা', NULL),
(303, 'birth_report', 'Birth Report', 'تقرير الولادة', 'জন্ম রিপোর্ট', ''),
(304, 'birth_report_edit', 'Birth Report Edit', 'تحرير تقرير الميلاد', 'জন্ম রিপোর্ট সম্পাদনা', ''),
(305, 'add_birth_report', 'Add Birth Report', 'أضف تقرير الولادة', 'জন্ম রিপোর্ট যোগ করুন', 'Ajouter un rapport d\'ami'),
(306, 'add_operation_report', 'Add Operation Report', 'إضافة تقرير العملية', 'অপারেশন রিপোর্ট যুক্ত করুন', 'Ajouter un rapport d\'opération'),
(307, 'operation_report', 'Operation Report', 'تقرير التشغيل', 'অপারেশন রিপোর্ট', NULL),
(308, 'investigation_report', 'Investigation Report', 'تقرير التحقيق', 'তদন্ত রিপোর্ট', NULL),
(309, 'add_investigation_report', 'Add Investigation Report', 'إضافة تقرير التحقيق', 'তদন্ত রিপোর্ট যোগ করুন', 'Ajouter un rapport d\'enquête'),
(310, 'add_medicine_category', 'Add Medicine Category', 'إضافة فئة الدواء', 'ঔষধ বিভাগ যোগ করুন', 'Ajouter une catégorie de médicament'),
(311, 'medicine_category_list', 'Medicine Category List', 'قائمة فئة الطب', 'ঔষধ বিভাগ তালিকা', NULL),
(312, 'category_name', 'Category Name', 'اسم التصنيف', 'বিভাগ নাম', NULL),
(313, 'medicine_category_edit', 'Medicine Category Edit', 'تحرير فئة الطب', 'ঔষধ বিভাগ সম্পাদনা', NULL),
(314, 'add_medicine', 'Add Medicine', 'إضافة دواء', 'ঔষধ যোগ করুন', 'Ajouter un médicament'),
(315, 'medicine_list', 'Medicine List', 'قائمة الأدوية', 'ঔষধ তালিকা', NULL),
(316, 'medicine_edit', 'Medicine Edit', 'تحرير الدواء', 'ঔষধ সম্পাদনা', NULL),
(317, 'manufactured_by', 'Manufactured By', 'صنع بواسطة', 'নির্মিত', NULL),
(318, 'medicine_name', 'Medicine Name', 'اسم الدواء', 'ঔষধ নাম', NULL),
(319, 'messages', 'Messages', 'رسائل', 'বার্তা', NULL),
(320, 'inbox', 'Inbox', 'صندوق الوارد', 'ইনবক্স', NULL),
(321, 'sent', 'Sent', 'أرسلت', 'প্রেরিত', NULL),
(322, 'new_message', 'New Message', 'رسالة جديدة', 'নতুন বার্তা', NULL),
(323, 'sender', 'Sender Name', 'مرسل', 'প্রেরক', NULL),
(324, 'message', 'Message', 'رسالة', 'বার্তা', NULL),
(325, 'subject', 'Subject', 'موضوع', 'বিষয়', NULL),
(326, 'receiver_name', 'Send To', 'اسم المتلقي', 'প্রাপকের নাম', NULL),
(327, 'select_user', 'Select User', 'اختر المستخدم', 'ব্যবহারকারী নির্বাচন করুন', NULL),
(328, 'message_sent', 'Messages Sent', 'تم الارسال', 'বার্তা পাঠানো', NULL),
(329, 'mail', 'Mail', 'بريد', 'মেইল', NULL),
(330, 'send_mail', 'Send Mail', 'ارسل بريد', 'মেইল পাঠাও', NULL),
(331, 'mail_setting', 'Mail Setting', 'إعداد البريد', 'মেইল সেটিং', NULL),
(332, 'protocol', 'Protocol', 'بروتوكول', 'প্রোটোকল', NULL),
(333, 'mailpath', 'Mail Path', 'مسار البريد', 'মেইল পথ', NULL),
(334, 'mailtype', 'Mail Type', 'نوع البريد', 'মেইল টাইপ', NULL),
(335, 'validate_email', NULL, 'التحقق من صحة البريد الإلكتروني', 'ইমেল যাচাই', NULL),
(336, 'true', 'True', 'صحيح', 'সত্য', NULL),
(337, 'false', 'False', 'خاطئة', 'ভুল', NULL),
(338, 'attach_file', 'Attach File', 'أرفق ملف', 'ফাইল সংযুক্ত', ''),
(339, 'wordwrap', '', 'رجوع الى السطر', 'শব্দ মোড়ানো', NULL),
(340, 'send', 'Send', 'إرسال', 'পাঠান', NULL),
(341, 'upload_successfully', 'Upload Successfully!', 'تحميل بنجاح', 'সফলভাবে আপলোড করা হয়েছে', NULL),
(342, 'app_setting', 'App Setting', 'إعداد التطبيق', 'অ্যাপ্লিকেশন সেটিং', 'réglage de l\'application'),
(343, 'case_manager', 'Case Manager', 'مدير الحالة', 'মামলা ব্যাবস্থাপক', ''),
(344, 'patient_status', 'Patient Status', 'حالة المريض', 'রোগীর অবস্থা', NULL),
(345, 'patient_status_edit', 'Edit Patient Status', 'تعديل حالة المريض', 'রোগীর অবস্থা সম্পাদনা', NULL),
(346, 'add_patient_status', 'Add Patient Status', 'إضافة حالة المريض', 'রোগীর অবস্থা যোগ করুন', 'Ajouter le statut du patient'),
(347, 'add_new_status', 'Add New Status', 'إضافة حالة جديدة', 'খবরের  অবস্থা যোগ করুন', 'Ajouter un nouveau statut'),
(348, 'case_manager_list', 'Case Manager List', 'قائمة مدير الحالة', 'কেস ম্যানেজার তালিকা', ''),
(349, 'hospital_address', 'Hospital Address', 'عنوان المستشفى', 'হাসপাতাল ঠিকানা', NULL),
(350, 'ref_doctor_name', 'Ref. Doctor Name', 'اسم الطبيب المرجعي', 'রেফারেন্স ডাক্তারের নাম', NULL),
(351, 'hospital_name', 'Hospital Name', 'اسم المستشفى', 'হাসপাতালের  নাম', NULL),
(352, 'patient_name', 'Patient  Name', 'اسم المريض', 'রোগীর নাম', NULL),
(353, 'document_list', 'Document List', 'قائمة المستندات', 'নথি তালিকা', NULL),
(354, 'add_document', 'Add Document', 'إضافة وثيقة', 'নথি যোগ করুন', 'Ajouter un document'),
(355, 'upload_by', 'Update by', 'تحميل بواسطة', 'হালনাগাদ', NULL),
(356, 'documents', 'Documents', 'مستندات', 'কাগজপত্র', NULL),
(357, 'prescription', 'Prescription', 'وصفة طبية', 'প্রেসক্রিপশন', NULL),
(358, 'add_prescription', 'Add Prescription', 'إضافة وصفة طبية', 'প্রেসক্রিপশন যোগ করুন', 'Ajouter une ordonnance'),
(359, 'prescription_list', 'Prescription List', 'قائمة وصفة طبية', 'প্রেসক্রিপশন তালিকা', NULL),
(360, 'add_insurance', 'Add Insurance', 'إضافة التأمين', 'বীমা যোগ করুন', 'Ajouter une assurance'),
(361, 'insurance_list', 'Insurance List', 'قائمة التأمين', 'বীমা তালিকা', NULL),
(362, 'insurance_name', 'Insurance Name', 'اسم التأمين', 'বীমা নাম', NULL),
(366, 'add_patient_case_study', 'Add Patient Case Study', 'إضافة دراسة حالة المريض', 'রোগীর ক্ষেত্রে অধ্যয়ন যোগ করুন', 'Ajouter une étude de cas à un patient'),
(367, 'patient_case_study_list', 'Patient Case Study List', 'قائمة دراسة حالة المريض', 'রোগীর ক্ষেত্রে অধ্যয়ন তালিকা', NULL),
(368, 'food_allergies', 'Food Allergies', 'حساسية الطعام', 'খাবারে এ্যালার্জী', NULL),
(369, 'tendency_bleed', 'Tendency Bleed', 'نزف الميل', 'প্রবণতা রক্তপাত', NULL),
(370, 'heart_disease', 'Heart Disease', 'مرض القلب', 'হৃদরোগ', NULL),
(371, 'high_blood_pressure', 'High Blood Pressure', 'ضغط دم مرتفع', 'উচ্চ রক্তচাপ', NULL),
(372, 'diabetic', 'Diabetic', 'مريض بالسكر', 'বহুমূত্ররোগগ্রস্ত', NULL),
(373, 'surgery', 'Surgery', 'العملية الجراحية', 'সার্জারি', NULL),
(374, 'accident', 'Accident', 'حادث', 'দুর্ঘটনা', 'Accident'),
(375, 'others', 'Others', 'الآخرين', 'অন্যান্য', NULL),
(376, 'family_medical_history', 'Family Medical History', 'التاريخ الطبي للعائلة', 'পারিবারিক চিকিৎসা ইতিহাস', NULL),
(377, 'current_medication', 'Current Medication', 'الدواء الحالي', 'বর্তমান ঔষধ', NULL),
(378, 'female_pregnancy', 'Female Pregnancy', 'الحمل الأنثوي', 'মহিলা গর্ভাবস্থা', NULL),
(379, 'breast_feeding', 'Breast Feeding', 'الرضاعة الطبيعية', 'স্তন খাওয়ানো', ''),
(380, 'health_insurance', 'Health Insurance', 'تأمين صحي', 'স্বাস্থ্য বীমা', NULL),
(381, 'low_income', 'Low Income', 'دخل منخفض', 'কম আয়', NULL),
(382, 'reference', 'Reference', 'مرجع', 'রেফারেন্স ', NULL),
(385, 'instruction', 'Instruction', 'تعليمات', 'নির্দেশ', NULL),
(386, 'medicine_type', 'Medicine Type', 'نوع الدواء', 'ঔষধ টাইপ', NULL),
(387, 'days', 'Days', 'أيام', 'দিন', NULL),
(388, 'weight', 'Weight', 'وزن', 'ওজন', NULL),
(389, 'blood_pressure', 'Blood Pressure', 'ضغط الدم', 'রক্তচাপ', ''),
(390, 'old', 'Old', 'قديم', 'পুরাতন', NULL),
(391, 'new', 'New', 'الجديد', 'নতুন', NULL),
(392, 'case_study', 'Case Study', 'دراسة الحالة', 'কেস স্টাডি', NULL),
(393, 'chief_complain', 'Chief Complain', 'الشكوى الرئيسية', 'প্রধান অভিযোগ', NULL),
(394, 'patient_notes', 'Patient Notes', 'ملاحظات المريض', 'রোগীর নোট', NULL),
(395, 'visiting_fees', 'Visiting Fees', 'رسوم الزيارة', 'ভিসিটিং ফি', 'frais de visite'),
(396, 'diagnosis', 'Diagnosis', 'التشخيص', 'রোগ নির্ণয়', NULL),
(397, 'prescription_id', 'Prescription ID', 'معرف وصفة طبية', 'প্রেসক্রিপশন আইডি', NULL),
(398, 'name', 'Name', 'اسم', 'নাম', NULL),
(399, 'prescription_information', 'Prescription Information', 'معلومات وصفة طبية', 'প্রেসক্রিপশন তথ্য', NULL),
(400, 'sms', 'SMS', 'رسالة قصيرة', 'এসএমএস ', NULL),
(401, 'gateway_setting', 'Gateway Setting', 'إعداد البوابة', 'গেটওয়ে সেটিং', NULL),
(402, 'time_zone', 'Time Zone', 'وحدة زمنية', 'সময় জোন ', NULL),
(403, 'username', 'User Name', 'اسم المستخدم', 'ব্যবহারকারীর নাম', NULL),
(404, 'provider', 'Provider', 'مزود', 'প্রদানকারী', NULL),
(405, 'sms_template', 'SMS Template', 'قالب الرسائل القصيرة', 'এসএমএস টেমপ্লেট', NULL),
(406, 'template_name', 'Template Name', 'اسم القالب', 'টেম্পলেট নাম', NULL),
(407, 'sms_schedule', 'SMS Schedule', 'جدول الرسائل القصيرة', 'এসএমএস সময়সূচী', NULL),
(408, 'schedule_name', 'Schedule Name', 'اسم الجدول الزمني', 'সময়সূচী নাম', NULL),
(409, 'time', 'Time', 'زمن', 'সময়', NULL),
(410, 'already_exists', 'Already Exists', 'موجود مسبقا', 'আগে থেকেই আছে', 'existe déjà'),
(411, 'send_custom_sms', 'Send Custom SMS', 'ارسال الرسائل القصيرة المخصصة', 'কাস্টম এসএমএস পাঠান', NULL),
(412, 'sms_sent', 'SMS Sent!', 'أرسلت الرسائل القصيرة', 'এসএমএস পাঠানো হয়েছে', NULL),
(413, 'custom_sms_list', 'Custom SMS List', 'قائمة الرسائل القصيرة المخصصة', 'কাস্টম এসএমএস তালিকা', NULL),
(414, 'reciver', 'Reciver', 'المتلقي', 'গ্রাহক', NULL),
(415, 'auto_sms_report', 'Auto SMS Report', 'تقرير السيارات sms', 'অটো এসএমএস রিপোর্ট', ''),
(417, 'user_sms_list', 'User SMS List', 'قائمة الرسائل القصيرة للمستخدم', 'ব্যবহারকারী এসএমএস তালিকা', NULL),
(418, 'send_sms', 'Send SMS', 'أرسل رسالة نصية قصيرة', 'বার্তা পাঠান', NULL),
(419, 'new_sms', 'New SMS', 'الرسائل القصيرة الجديدة', 'নতুন এসএমএস', NULL),
(420, 'sms_list', 'SMS List', 'قائمة الرسائل القصيرة', 'এসএমএস তালিকা', NULL),
(421, 'insurance', 'Insurance', 'تأمين', 'বীমা', NULL),
(422, 'add_limit_approval', 'Add Limit Approval', 'إضافة موافقة الحد', 'সীমা অনুমোদন যোগ করুন', 'Ajouter une approbation de limite'),
(423, 'limit_approval_list', 'Limit Approval List', 'قائمة القبول', 'অনুমোদন তালিকা সীমাবদ্ধ', NULL),
(424, 'service_tax', 'Service Tax', 'ضريبة الخدمة', 'সেবা কর', NULL),
(425, 'remark', 'Remark', 'تعليق', 'মন্তব্য', NULL),
(426, 'insurance_no', 'Insurance No.', 'التأمين لا', 'বীমা নম্বর', NULL),
(427, 'insurance_code', 'Insurance Code', 'رمز التأمين', 'বীমা কোড', NULL),
(428, 'hospital_rate', 'Hospital Rate', 'معدل المستشفى', 'হাসপাতালের  হার', NULL),
(429, 'insurance_rate', 'Insurance Rate', 'سعر التأمين', 'বীমা হার', NULL),
(430, 'disease_charge', 'Disease Charge', 'تهمة المرض', 'রোগ চার্জ', NULL),
(431, 'disease_name', 'Disease Name', 'اسم المرض', 'রোগ নাম', NULL),
(432, 'room_no', 'Room No', 'غرفة لا', 'কক্ষ নম্বর', NULL),
(433, 'disease_details', 'Disease Details', 'تفاصيل المرض', 'রোগ বিবরণ', NULL),
(434, 'consultant_name', 'Consultant Name', 'اسم استشاري', 'পরামর্শদাতা নাম', NULL),
(435, 'policy_name', 'Policy Name', 'اسم السياسة', 'পলিসি নাম', NULL),
(436, 'policy_no', 'Policy No.', 'لا توجد سياسة', 'পলিসি নাম্বার', NULL),
(437, 'policy_holder_name', 'Policy Holder Name', 'إسم صاحب الوثيقة', 'নীতি ধারক নাম', NULL),
(438, 'approval_breakup', ' Approval Charge Break up', 'تفكك الموافقة', 'অনুমোদন বিরতি', 'rupture d\'approbation'),
(439, 'limit_approval', 'Limit Approval', 'موافقة الحد', 'অনুমোদন সীমিত', NULL),
(440, 'insurance_limit_approval', 'Insurance Limit Approval', 'موافقة حد التأمين', 'বীমা সীমা অনুমোদন', NULL),
(441, 'billing', 'Billing', 'الفواتير', 'বিলিং', ''),
(442, 'add_admission', 'Add Patient Admission', 'اضف القبول', 'ভর্তি যোগ করুন', 'Ajouter des admissions'),
(443, 'add_service', 'Add Service', 'أضف الخدمة', 'সেবা যোগ করুন', 'ajouter un service'),
(444, 'service_list', 'Service List', 'قائمة الخدمات', 'সেবা তালিকা', NULL),
(445, 'service_name', 'Service Name', 'اسم الخدمة', 'সেবার নাম', NULL),
(446, 'add_package', 'Add Package', 'إضافة حزمة', 'প্যাকেজ যোগ করুন', 'Ajouter un paquet'),
(447, 'package_list', 'Package List', 'قائمة الحزمة', 'প্যাকেজ তালিকা', NULL),
(448, 'package_name', 'Package Name', 'اسم الحزمة', 'প্যাকেজ নাম', NULL),
(449, 'package_details', 'Package Details', 'حزمة من التفاصيل', 'প্যাকেজ বিবরণ', NULL),
(450, 'edit_package', 'Edit Package', 'تحرير الحزمة', 'প্যাকেজ সম্পাদনা করুন', NULL),
(451, 'admission_date', 'Admission Date', 'تاريخ القبول', 'ভর্তির তারিখ', 'date d\'admission'),
(452, 'guardian_name', 'Guardian Name', 'اسم الوصي', 'অভিভাবকের নাম', NULL),
(453, 'agent_name', 'Agent Name', 'اسم العميل', 'এজেন্ট নাম', 'nom d\'agent'),
(454, 'guardian_relation', 'Guardian Relation', 'علاقة الولي', 'অভিভাবক সম্পর্ক', NULL),
(455, 'guardian_contact', 'Guardian Contact', 'الاتصال الوصي', 'অভিভাবক যোগাযোগ', NULL),
(456, 'guardian_address', 'Guardian Address', 'عنوان الوصي', 'অভিভাবক ঠিকানা', NULL),
(457, 'admission_list', 'Patient Admission List', 'قائمة القبول', 'ভর্তি তালিকা', 'list'),
(458, 'admission_id', 'Admission ID', 'معرف القبول', 'ভর্তি আইডি', 'identifiant d\'admission'),
(459, 'edit_admission', 'Edit Admission', 'تحرير القبول', 'ভর্তি সম্পাদনা করুন', NULL),
(460, 'add_advance', 'Add Advance Payment', 'إضافة مسبقا', 'আগাম যোগ করুন', 'Ajouter une avance'),
(461, 'advance_list', 'Advance Payment List', 'قائمة مسبقا', 'অগ্রিম তালিকা', 'liste avance'),
(462, 'receipt_no', 'Receipt No', 'رقم الإيصال', 'রশীদ নং', NULL),
(463, 'cash_card_or_cheque', 'Cash / Card / Cheque', 'بطاقة نقدية أو شيك', 'নগদ কার্ড বা চেক', NULL),
(464, 'payment_method', 'Payment Method', 'طريقة الدفع او السداد', 'মূল্যপরিশোধ পদ্ধতি', NULL),
(465, 'add_bill', 'Add Bill', 'إضافة فاتورة', 'বিল যোগ করুন', 'Ajouter une facture'),
(466, 'bill_list', 'Bill List', 'قائمة الفاتورة', 'বিল তালিকা', ''),
(467, 'bill_date', 'Bill Date', 'تاريخ الفاتورة', 'বিলের তারিখ', ''),
(468, 'total_days', 'Total Days', 'مجموع الأيام', 'মোট দিন', NULL),
(469, 'advance_payment', 'Advance Payment', 'دفعه مقدمه', 'অগ্রিম পেমেন্ট', 'acompte'),
(470, 'cash', 'Cash', 'السيولة النقدية', 'নগদ', NULL),
(471, 'card', 'Card', 'بطاقة', 'কার্ড', ''),
(472, 'cheque', 'Cheque', 'التحقق من', 'চেক', NULL),
(473, 'card_cheque_no', 'Card / Cheque No.', 'بطاقة لا تحقق', 'কার্ড চেক সংখ্যা', ''),
(474, 'receipt', 'Receipt', 'إيصال', 'প্রাপ্তি', NULL),
(475, 'tax', 'Tax', 'ضريبة', 'কর', NULL),
(476, 'pay_advance', 'Advance Paid', 'دفع مقدما', 'অ্যাডভান্স  পরিশোধ', NULL),
(477, 'payable', 'Payable', 'تدفع', 'প্রদেয়', NULL),
(478, 'notes', 'Notes', 'ملاحظات', 'নোট', NULL),
(479, 'rate', 'Rate', 'معدل', 'দর', NULL),
(480, 'bill_id', 'Bill ID.', 'معرف الفاتورة', 'বিল আইডি', ''),
(482, 'unpaid', 'Unpaid', 'غير مدفوع', 'অপরিশোধিত', NULL),
(483, 'bill_details', 'Bill Details', 'تفاصيل الفاتورة', 'বিল বিবরণ', ''),
(484, 'signature', 'Signature', 'التوقيع', 'স্বাক্ষর', NULL),
(485, 'update_bill', 'Update Bill', 'فاتورة التحديث', 'বিল আপডেট করুন', NULL),
(486, 'role_permission', 'Role Permission', 'إذن الدور', 'ভূমিকা অনুমতি', NULL),
(487, 'add_role', 'Add Role', 'إضافة دور', 'ভূমিকা যোগ করুন', 'ajouter une règle'),
(488, 'role_list', 'Role List', 'قائمة الأدوار', 'ভূমিকা তালিকা', NULL),
(489, 'role_name', 'Role Name', 'اسم الدور', 'ভূমিকা নাম', NULL),
(490, 'assign_role_to_user', 'Assign Role To User', 'تعيين دور للمستخدم', 'ব্যবহারকারী বিধি বরাদ্দ', ''),
(491, 'you_do_not_have_permission_to_access_please_contact_with_administrator', 'You do not have permission to access please contact with administrator', 'ليس لديك إذن للوصول يرجى الاتصال بالمسؤول', 'আপনার অ্যাক্সেস করার অনুমতি নেই প্রশাসকের সাথে যোগাযোগ করুন', NULL),
(492, 'language_list', 'Language List', 'قائمة اللغات', 'ভাষা তালিকা', NULL),
(493, 'add_phrase', 'Add Phrase', 'إضافة عبارة', 'বাক্য যোগ করুন', 'Ajouter Fraser'),
(494, 'sms_setting', 'SMS Setting', 'إعداد الرسائل القصيرة', 'এসএমএস সেটিং', NULL),
(495, 'create', 'Create', 'خلق', 'সৃষ্টি করা', NULL),
(496, 'read', 'Read', 'اقرأ', 'পড়া', NULL),
(497, 'system_user', 'System User', 'مستخدم النظام', 'সিস্টেম ব্যবহারকারী', NULL),
(498, 'permission', 'Permission', 'الإذن', 'অনুমতি', NULL),
(499, 'employees_list', 'Employees List', 'قائمة الموظفين', 'কর্মচারী তালিকা', NULL),
(500, 'not_role_select', 'Not Role Select', 'لا اختيار دور', 'আইন নিৰ্বাচন করা না ', NULL),
(504, 'email_password', 'Email & Password', 'كلمة مرور البريد الإلكتروني', 'ই - মেইলের পাসওয়ার্ড', NULL),
(505, 'user', 'User', 'المستعمل', 'ব্যবহারকারী', NULL),
(506, 'add_room', 'Add Room', 'إضافة غرفة', 'রুম যোগ করুন', 'ajouter de la place'),
(507, 'room_name', 'Room Name', 'اسم الغرفة', 'রুমের নাম', NULL),
(508, 'room_list', 'Room List', 'قائمة غرفة', 'রুম তালিকা', NULL),
(509, 'room_edit', 'Room Edit', 'تحرير الغرفة', 'রুম সম্পাদনা', NULL),
(510, 'bed_number', 'Bed Number', 'رقم السرير', 'বিছানা সংখ্যা', ''),
(511, 'bed_transfer', 'Bed Transfer', 'نقل السرير', 'বিছানা স্থানান্তর', ''),
(512, 'bed_transfer_list', 'Bed Transfer List', 'قائمة نقل السرير', 'বিছানা স্থানান্তর তালিকা', ''),
(513, 'booked', 'Booked', 'حجز', 'বরাদ্দ ', ''),
(514, 'available', 'Available ', 'متاح', 'সহজলভ্য', ''),
(515, 'sex', 'Sex', 'جنس', 'লিঙ্গ', NULL),
(516, 'medications_and_visits', 'Medications and Visits', 'الأدوية والزيارات', 'ঔষধ এবং ভিজিট', NULL),
(517, 'add_medication', 'Add Patient Medication', 'إضافة الدواء', 'ঔষধ যোগ করুন', 'Ajouter un médicament'),
(518, 'medication_list', 'Patient Medication List', 'قائمة الأدوية', 'ঔষধ তালিকা', NULL),
(519, 'add_visit', 'Add Patient Visit', 'أضف زيارة', 'পরিদর্শন যোগ করুন', 'ajouter visite'),
(520, 'visit_list', 'Patient Visit List', 'قائمة الزيارة', 'ভিসিট তালিকা', NULL),
(521, 'dosage', 'Dosage', 'جرعة', 'ডোজ', NULL),
(522, 'per_day_intake', 'Per Day Intake', 'في اليوم الواحد', 'প্রতি দিন ভোজন', NULL),
(523, 'full_stomach', 'Full Stomach', 'معدة ممتلئة', 'ভরা পেটে', NULL),
(524, 'other_instruction', 'Others Instruction', 'تعليمات أخرى', 'অন্যান্য নির্দেশনা', NULL),
(525, 'from_date', 'From Date', 'من التاريخ', 'তারিখ হইতে', NULL),
(526, 'to_date', 'To Date', 'حتى الآن', 'তারিখ ', NULL),
(527, 'prescribe_by', 'Prescribe By', 'يصف', 'প্রেসক্রিব ', NULL),
(528, 'intake_time', 'Intake Time', 'وقت الاستلام', 'ভোজনের সময়', NULL),
(529, 'medication_edit', 'Patient Medication Edit', 'تحرير الدواء', 'ঔষধ সম্পাদনা', NULL),
(530, 'visit_date', 'Visit Date', 'تاريخ الزيارة', 'ভিসিট তারিখ', NULL),
(531, 'visit_time', 'Visit Time', 'وقت الزيارة', 'ভিসিট টাইম ', NULL),
(532, 'finding', 'Finding', 'العثور على', 'খোঁজা ', NULL),
(533, 'instructions', 'Instructions', 'تعليمات', 'নির্দেশাবলী', NULL),
(534, 'fees', 'Fees', 'رسوم', 'ফি', NULL),
(535, 'visit_by', 'Visit By', 'زيارة من قبل', 'ভিসিট', NULL),
(536, 'visit_edit', 'Visit Edit', 'زيارة تحرير', 'ভিসিট সম্পাদনা', NULL),
(537, 'visit_type', 'Visit Type', 'نوع الزيارة', 'ভিসিট ধরণ ', NULL),
(538, 'visit_report', 'Visiting Reports', 'تقرير زيارة', 'ভিসিট রিপোর্ট', NULL),
(539, 'medication_report', 'Medication Reports', 'تقرير الدواء', 'ঔষধ রিপোর্ট', NULL),
(540, 'url', 'Url', 'رابط', 'ইউআরএল', NULL),
(541, 'image_dimension_require', 'Image Dimension Required', 'البعد الصورة تتطلب', 'ইমেজ মাত্রা প্রয়োজন', NULL),
(542, 'download_now', 'Download Now', 'التحميل الان', 'এখন ডাউনলোড করুন', NULL),
(543, 'start_now', 'Start Now', 'ابدأ الآن', 'এখুনি শুরু করুন', NULL),
(544, 'author_name', 'Author Name', 'اسم المؤلف', 'লেখকের নাম', ''),
(545, 'quotation', 'Quotation', 'اقتباس', 'উদ্ধৃতি', NULL),
(546, 'about_edit', 'About Edit', 'حول تحرير', 'সম্পাদনা সম্পর্কে', 'A propos de Edit'),
(547, 'testimonial_list', 'Testimonial List', 'قائمة الشهادات', 'প্রশংসাপত্র তালিকা', NULL),
(548, 'add_testimonial', 'Add Testimonial', 'أضف شهادة', 'প্রশংসাপত্র যোগ করুন', 'ajouter un témoignage'),
(549, 'testimonial', 'Testimonial', 'شهادة', 'প্রশংসাপত্র', NULL),
(550, 'testimonial_edit', 'Testimonial Edit', 'تحرير الشهادة', 'প্রশংসাপত্র সম্পাদনা', NULL),
(551, 'write_us', 'Write Us', 'اكتب لنا', 'আমাদের লিখুন', NULL),
(552, 'call_us', 'Call Us', 'اتصل بنا', 'আমাদের কল করুন', ''),
(553, 'add_instruction', 'Add Instruction', 'اضف التعليمات', 'নির্দেশ যোগ করুন', 'Instruction publicitaire'),
(554, 'instruction_edit', 'Instruction Edit', 'تحرير التعليمات', 'নির্দেশ সম্পাদনা', NULL),
(555, 'short_instruction', 'Short Instruction', 'تعليمات قصيرة', 'সংক্ষিপ্ত নির্দেশনা', NULL),
(556, 'add_partner', 'Add Partner', 'أضف شريك', 'অংশীদার যোগ করুন', 'Partenaire complémentaire'),
(557, 'partner_list', 'Partner List', 'قائمة الشركاء', 'অংশীদার তালিকা', NULL),
(558, 'partner_edit', 'Partner Edit', 'تعديل الشريك', 'অংশীদার সম্পাদনা', NULL),
(559, 'instruction_list', 'Instruction List', 'قائمة التعليمات', 'নির্দেশাবলী তালিকা', NULL),
(560, 'appoint_instruction', 'Appointment Instruction', 'يعين التعليمات', 'অ্যাপয়েন্টমেন্ট নির্দেশনা', 'instruction de rendez-vous'),
(561, 'institute_name', 'Institute Name', 'اسم المعهد', 'প্রতিষ্ঠানের নাম', NULL),
(564, 'add_portfolio', 'Add Portfolio', 'إضافة محفظة', 'পোর্টফোলিও যোগ করুন', 'Ajouter des portefeuilles'),
(565, 'portfolio_list', 'Portfolio List', 'قائمة المحافظ', 'পোর্টফোলিও তালিকা', NULL),
(566, 'portfolio_edit', 'Portfolio Edit', 'تحرير المحفظة', 'পোর্টফোলিও সম্পাদনা', NULL),
(567, 'flaticon', 'Flaticon', 'رمز مسطح', 'ফ্লাটআইকন ', NULL),
(568, 'add_time_slot', 'Add Time Slot', 'إضافة فتحة الوقت', 'সময় স্লট যোগ করুন', 'ajouter un créneau horaire'),
(569, 'slot', 'Slot', 'فتحة', 'স্লট', NULL),
(570, 'slot_name', 'Slot Name', 'اسم الفتحة', 'স্লট নাম', NULL),
(571, 'slot_list', 'Slot List', 'قائمة الفتحة', 'স্লট তালিকা', NULL),
(572, 'slot_edit', 'Slot Edit', 'تحرير فتحة', 'স্লট সম্পাদনা', NULL),
(573, 'add_main_department', 'Add Main Department', 'إضافة القسم الرئيسي', 'প্রধান বিভাগ যোগ করুন', 'Ajouter une section de menu'),
(574, 'main_department_list', 'Main Department List', 'قائمة الإدارة الرئيسية', 'প্রধান বিভাগ তালিকা', NULL),
(575, 'main_department', 'Main Department', 'القسم الرئيسي', 'প্রধান বিভাগ', NULL),
(576, 'about_us', 'About Us', 'معلومات عنا', 'আমাদের সম্পর্কে', 'À propos de nous'),
(577, 'doctors', 'Doctor\'s', 'الأطباء', 'ডাক্তার', NULL),
(578, 'nurses', 'Nurse\'s', 'الممرضات', 'নার্স', NULL),
(579, 'working_hours', 'Working Hours', 'ساعات العمل', 'কর্মঘন্টা', NULL),
(580, 'closed', 'Closed', 'مغلق', 'বন্ধ', NULL),
(581, 'are_you_human', 'Are you human?', 'هل انت انسان؟', 'তুমি কি মানুষ?', 'es-tu humain?'),
(582, 'add_news', 'Add News', 'أضف الأخبار', 'খবর যোগ করুন', 'Ajouter des nouvelles'),
(583, 'news', 'News', 'أخبار', 'খবর', NULL),
(584, 'news_list', 'News List', 'قائمة الأخبار', 'খবর তালিকা', NULL),
(585, 'added_to_website_successfully', 'Added to website successfully', 'تمت إضافته إلى موقع الويب بنجاح', 'সফলভাবে ওয়েবসাইট এ যোগ করা হয়েছে', 'Site Web d\'Aadhaa avec succès'),
(586, 'removed_form_website_successfully', 'Removed form website successfully', 'إزالة نموذج الموقع بنجاح', 'সফলভাবে ওয়েবসাইট হতে মুছে ফেলা হয়েছে', NULL),
(587, 'related_article', 'Related article', 'مقالات لها صلة', 'সম্পর্কিত নিবন্ধ', NULL),
(588, 'share_this', 'Share this', 'شارك هذا', 'এটি শেয়ার করুন', NULL),
(593, 'menu_list', 'Menu List', 'قائمة القائمة', 'মেনু তালিকা', NULL),
(596, 'add_menu', 'Add Menu', 'إضافة القائمة', 'মেনু যোগ করুন', 'Ajouter un menu'),
(597, 'parent_menu', 'Parent Menu', 'القائمة الأم', 'অভিভাবক মেনু', NULL),
(598, 'menu_edit', 'Menu Edit', 'تحرير القائمة', 'মেনু সম্পাদনা', NULL),
(599, 'template_assign', 'Template Assign', 'تعيين قالب', 'টেম্পলেট বরাদ্দ', NULL),
(600, 'add_template', 'Add Template', 'إضافة قالب', 'টেমপ্লেট যোগ করুন', 'ajouter un modèle'),
(601, 'contents', 'Contents', 'محتويات', 'বিষয়বস্তু', NULL),
(602, 'make_an_appointment', 'Make an appointment!', 'إحجز موعد', 'একটি অ্যাপয়েন্টমেন্ট করা', NULL),
(603, 'go_there', 'Go there', 'اذهب الى هناك', 'সেখানে যাও', NULL),
(604, 'view_our_team_of_surgeons', 'View our team of surgeons', 'عرض فريقنا من الجراحين', 'আমাদের টীম সার্জনদের দেখুন', NULL),
(605, 'timetable', 'Timetable', 'الجدول الزمني', 'সময়সূচি', NULL),
(606, 'benefits', 'Benefits', 'فوائد', 'সুবিধা', ''),
(607, 'common_template', 'Common Template', 'قالب مشترك', 'সাধারণ টেমপ্লেট', NULL),
(608, 'you_need_help', 'You need help?', 'أنت بحاجة للمساعدة؟', 'তোমার সাহায্য দরকার?', NULL),
(609, 'our_team', 'Our Team', 'فريقنا', 'আমাদের টিম', NULL),
(610, 'what_client_say', 'What Client Say', 'ما يقوله العميل', 'ক্লায়েন্ট কি বলে?', NULL),
(611, 'contact_us', 'Contact Us', 'اتصل بنا', 'আমাদের সাথে যোগাযোগ করুন', NULL),
(612, 'departments', 'Departments', 'الإدارات', 'বিভাগ', NULL),
(613, 'quick_links', 'Quick Links', 'روابط سريعة', 'দ্রুত লিঙ্ক', NULL),
(614, 'contact_details', 'Contact Details', 'بيانات المتصل', 'যোগাযোগের ঠিকানা', NULL),
(615, 'get_directions', 'Get Directions', 'احصل على الاتجاهات', 'দিকনির্দেশ পেতে', NULL),
(616, 'data_not_available', 'Data not available!', 'البيانات غير متوفرة', 'তথ্য পাওয়া যায় নাই ', NULL),
(617, 'lets_talk', 'Let\'s Talk!', 'يتيح لك التحدث', 'চল কথা বলি', NULL),
(618, 'my_schedule_for_this_week', 'My schedule for this week', 'جدول أعمالي لهذا الأسبوع', 'এই সপ্তাহের জন্য আমার সময়সূচী', NULL),
(619, 'old_patient', 'Old Patient', 'مريض قديم', 'পুরাতন রোগী', NULL),
(620, 'new_patient', 'New Patient', 'مريض جديد', 'নতুন রোগী', NULL),
(621, '1', '1', '1', '১', '1'),
(622, '2', '2', '2', '২ ', '2'),
(623, 'book_appointment', 'Book Appointment', 'موعد الكتاب', 'অ্যাপয়েন্টমেন্ট বরাদ্দ করুন', ''),
(624, 'notes_submitted_to_the_attendance_office_must_include_following', 'Notes submitted to the Attendance Office must include following', 'يجب أن تتضمن الملاحظات المقدمة إلى مكتب الحضور ما يلي', 'উপস্থিতি অফিসে নিম্নলিখিত নোট অন্তর্ভুক্ত করা আবশ্যক', NULL),
(625, 'provide_your_primary_information_about_the_following_details', 'Provide your primary information about the following details.', 'تقديم معلوماتك الأساسية حول التفاصيل التالية', 'নিম্নলিখিত বিবরণ সম্পর্কে আপনার প্রাথমিক তথ্য প্রদান করুন', NULL),
(626, 'please_provide_a_valid_email', 'Please provide a valid email.', 'يرجى تقديم عنوان بريد إلكتروني صالح', 'বৈধ ইমেইল প্রদান করুন', NULL),
(627, 'help_us_with_accurate_information_about_the_following_details', 'Help us with accurate information about the following details', 'ساعدنا بمعلومات دقيقة حول التفاصيل التالية', 'নিম্নলিখিত বিবরণ সম্পর্কে সঠিক তথ্য দিয়ে  আমাদের সাহায্য করুন', NULL),
(628, 'i_consent_to_having_this_website_store_my_submitted_information_so_they_can_respond_to_my_inquiry', 'I consent to having this website store my submitted information so they can respond to my inquiry.', 'أوافق على تخزين موقع الويب هذا المعلومات التي أرسلتها حتى يتمكنوا من الرد على استفساري', 'আমি এই ওয়েবসাইট এ আমার তথ্য জমা থাকার অনুমতি দিচ্ছি যাতে তারা আমার তদন্তের সাড়া দিতে পারেন', NULL),
(629, 'please_provide_a_valid_id', 'Please provide a valid ID.', 'يرجى تقديم معرف صالح', 'বৈধ আইডি প্রদান করুন', NULL),
(630, 'if_forgot_patient_id_please_selected_the_checkbox', 'If Forgot Patient ID Please Selected The CheckBox', 'إذا نسيت   معرف المريض يرجى تحديد خانة الاختيار', 'রোগীর আইডি ভুলে গেলে চেকবক্স নির্বাচন করুন', NULL),
(631, 'fax', 'Fax', 'الفاكس', 'ফ্যাক্স', NULL),
(632, 'text', 'Text', 'نص', 'পাঠ', NULL),
(633, 'map_longitude', 'Map Longitude', 'خريطة الطول', 'মানচিত্র দ্রাঘিমাংশ', NULL),
(634, 'map_latitude', 'Map Latitude', 'خريطة خط العرض', 'মানচিত্র অক্ষাংশ', NULL),
(635, 'personal_information', 'Personal information', 'معلومات شخصية', 'ব্যক্তিগত তথ্য', NULL),
(636, 'practicing', 'Practicing', 'ممارسة', 'অনুশীলন', NULL),
(637, 'vacation', 'Vacation', 'عطلة', 'ছুটি', NULL),
(638, 'languages', 'languages', 'اللغات', 'ভাষা', NULL),
(639, 'portfolio', 'Portfolio', 'محفظة', 'পোর্টফোলিও', NULL),
(640, 'career_title', 'Career Title', 'اللقب الوظيفي', 'পেশা শিরোনাম', ''),
(641, 'behance_url', 'Behance Url', 'behance url', 'বেহ্যান্স ইউআরএল', ''),
(642, 'sign_up_with_linkedin', 'Sign up with LinkedIn', 'قم بالتسجيل مع linkedin', 'লিংকডইন দিয়ে সাইন আপ করুন', NULL),
(643, 'sign_up_with_google', 'Sign up with Google', 'اشترك مع جوجل', 'গুগল দিয়ে সাইন আপ করুন', NULL);
INSERT INTO `language` (`id`, `phrase`, `english`, `arabic`, `bangla`, `french`) VALUES
(644, 'related_departments', 'Related Departments', 'الإدارات ذات الصلة', 'সম্পর্কিত বিভাগ', NULL),
(645, 'sign_in', 'Sign In', 'تسجيل الدخول', 'প্রবেশ কর', NULL),
(646, 'sign_up', 'Sign Up', 'سجل', 'নিবন্ধন করুন', NULL),
(647, 'remind', 'Remind', 'تذكير', 'মনে করা ', NULL),
(648, 'do_not_have_an_account', 'Don\'t have an account?', 'لا تملك حساب', 'একাউন্ট আছে কিনা?', NULL),
(649, 'remember_me_next_time', 'Remember me next time', 'تذكرني في المرة القادمة', 'আমার পরবর্তী সময় মনে রাখবেন', NULL),
(650, 'or', 'Or', 'أو', 'অথবা', NULL),
(651, 'thank_you_for_registration', 'Thank you for registration', 'شكرا للتسجيل', 'নিবন্ধনের জন্য আপনাকে ধন্যবাদ', NULL),
(652, 'all_addresses_and_support_hotlines', 'All addresses and Support Hotlines', 'جميع العناوين ودعم الخطوط الساخنة', 'সব ঠিকানা এবং সাপোর্ট হটলাইন', 'toutes les adresses et lignes d\'assistance'),
(653, 'thank_you_for_contacting_with_us', 'Thank you for contacting with us.', 'شكرا على اتصالك بنا', 'আমাদের সাথে যোগাযোগ করার জন্য আপনাকে ধন্যবাদ', NULL),
(654, 'invalid_captcha', 'Invalid Captcha', 'كلمة التحقق غير صالحة', 'অবৈধ ছাপা', NULL),
(655, 'service_position', 'Service Position', 'موقف الخدمة', 'সেবা অবস্থান', NULL),
(656, 'view_all', 'View all', 'عرض الكل', 'সব দেখা', NULL),
(657, 'contact_us_for_help', 'Contact Us For Help', 'اتصل بنا للحصول على المساعدة', 'সাহায্যের জন্য আমাদের সাথে যোগাযোগ করুন', NULL),
(658, 'rating', 'Rating', 'تقييم', 'গুণমান নির্দেশ', 'évaluation'),
(659, 'native', 'Native', 'محلي', 'স্থানীয়', 'originaire de'),
(660, 'beginner', 'Beginner', 'مبتدئ', 'শিক্ষানবিস', 'débutante'),
(661, 'fluent', 'Fluent', 'بطلاقة', 'অনর্গল', 'Courant'),
(662, 'rating_out_of_10', 'Rating Out Of 10', 'تصنيف من 10', 'রেটিং আউট অফ ১০', 'note sur 10'),
(663, 'years', 'Years', 'سنوات', 'বছর', 'années'),
(664, 'template_edit', 'Template edit', 'تحرير قالب', 'টেমপ্লেট সম্পাদনা', 'modification du modèle'),
(665, 'map_directions', 'Map Directions', 'خريطة الاتجاهات', 'মানচিত্র নির্দেশাবলী', 'Plan d\'accès'),
(666, 'role_aready_exists', 'Role already exists', 'الدور موجود بالفعل', 'ভূমিকা ইতিমধ্যে বিদ্যমান', 'Le rôle existe déjà'),
(667, 'support', 'Support?', 'الدعم؟', 'সাপোর্ট?', 'Soutien?'),
(668, 'language_data_not_inserted_yet', 'Language data not inserted yet!', 'بيانات اللغة لم يتم إدخالها بعد!', 'ভাষা তথ্য এখনো সন্নিবেশ করা হয় নি!', 'les données linguistiques ne sont pas encore insérées!'),
(669, 'youtube_url', 'Youtube URL', 'يوتيوب رابط', 'ইউটিউব ইউআরএল', 'URL youtube'),
(670, 'document_title', 'Document Title', 'عنوان الوثيقة', 'নথির শিরোনাম', 'Titre du document'),
(671, 'free_bed_list', 'Free bed list', 'قائمة سرير مجاني', 'মুক্ত বিছানা তালিকা', 'Liste de lit gratuit'),
(672, 'discharged', 'Discharged', 'تفريغها', 'কর্মচ্যুত', 'déchargée'),
(673, 'welcome_back', 'Welcome back!', '!مرحبا بعودتك', 'ফিরে আসার জন্য স্বাগতম!', 'Nous saluons le retour!'),
(674, 'today_patient_list', 'Today patient list', 'اليوم قائمة المرضى', 'আজ রোগীর তালিকা', 'Liste de patients aujourd\'hui'),
(675, 'chart_of_account', 'Chart Of Account', NULL, NULL, NULL),
(676, 'debit_voucher', 'Debit Voucher', NULL, NULL, NULL),
(677, 'credit_voucher', 'Credit Voucher', NULL, NULL, NULL),
(678, 'contra_voucher', 'Contra Voucher', NULL, NULL, NULL),
(679, 'journal_voucher', 'Journal Voucher', NULL, NULL, NULL),
(680, 'voucher_approval', 'Voucher Approval', NULL, NULL, NULL),
(681, 'account_report', 'Account Report', NULL, NULL, NULL),
(682, 'voucher_report', 'Voucher Report', NULL, NULL, NULL),
(683, 'cash_book', 'Cash Book', NULL, NULL, NULL),
(684, 'bank_book', 'Bank Book', NULL, NULL, NULL),
(685, 'general_ledger', 'General Ledger', NULL, NULL, NULL),
(686, 'trial_balance', 'Trial Balance', NULL, NULL, NULL),
(687, 'profit_loss', 'Profit Loss', NULL, NULL, NULL),
(688, 'voucher_no', 'Voucher No', NULL, NULL, NULL),
(689, 'credit_account_head', 'Credit Account Head', NULL, NULL, NULL),
(690, 'cash_in_hand', 'Cash In Hand', NULL, NULL, NULL),
(691, 'add_more', 'Add More', NULL, NULL, NULL),
(692, 'code', 'Code', NULL, NULL, NULL),
(693, 'debit_account_head', 'Debit Account Head', NULL, NULL, NULL),
(694, 'approved', 'Approved', NULL, NULL, NULL),
(695, 'successfully_approved', 'Successfully Approved', NULL, NULL, NULL),
(696, 'update_debit_voucher', 'Update Debit Voucher', NULL, NULL, NULL),
(697, 'update_credit_voucher', 'Update Credit Voucher', NULL, NULL, NULL),
(698, 'update_contra_voucher', 'Update Contra Voucher', NULL, NULL, NULL),
(699, 'find', 'Find', NULL, NULL, NULL),
(700, 'balance', 'Balance', NULL, NULL, NULL),
(701, 'particulars', 'Particulars', NULL, NULL, NULL),
(702, 'voucher_type', 'Voucher Type', NULL, NULL, NULL),
(703, 'transaction_date', 'Transaction Date', NULL, NULL, NULL),
(704, 'opening_balance', 'Opening Balance', NULL, NULL, NULL),
(705, 'cash_book_report_on', 'Cash Book Report On', NULL, NULL, NULL),
(706, 'cash_book_voucher', 'Cash Book Voucher', NULL, NULL, NULL),
(707, 'to', 'To', NULL, NULL, NULL),
(708, 'head_of_account', 'Head Of Account', NULL, NULL, NULL),
(709, 'bank_book_report_of', 'Bank Book Report Of', NULL, NULL, NULL),
(710, 'on', 'On', NULL, NULL, NULL),
(711, 'bank_book_voucher', 'Bank Book Voucher', NULL, NULL, NULL),
(712, 'account_code', 'Account Code', NULL, NULL, NULL),
(713, 'gl_head', 'GL Head', NULL, NULL, NULL),
(714, 'transaction_head', 'Transaction Head', NULL, NULL, NULL),
(715, 'with_details', 'With Details', NULL, NULL, NULL),
(716, 'pre_balance', 'Pre Balance', NULL, NULL, NULL),
(717, 'current_balance', 'Current Balance', NULL, NULL, NULL),
(718, 'general_ledger_report', 'General Ledger Report', NULL, NULL, NULL),
(719, 'trial_balance_report', 'Trail Balance Report', NULL, NULL, NULL),
(720, 'prepared_by', 'Prepared By', NULL, NULL, NULL),
(721, 'accounts', 'Accounts', NULL, NULL, NULL),
(722, 'chairman', 'Chairman', NULL, NULL, NULL),
(723, 'authorized_signature', 'Authorized Signature', NULL, NULL, NULL),
(724, 'cash_flow', 'Cash Flow', NULL, NULL, NULL),
(725, 'cash_flow_statement', 'Cash Flow Statement', NULL, NULL, NULL),
(726, 'amount_in_dollar', 'Amount In Dollar', NULL, NULL, NULL),
(727, 'opening_cash_and_equivalent', 'Opening Cash && Equivalent', NULL, NULL, NULL),
(728, 'profit_loss_report', 'Profit Loss Report', NULL, NULL, NULL),
(729, 'statement_of_comprehensive_income', 'Statement Of Comprehensive Income', 'كشف الحساب الشامل', 'ব্যাপক আয় বিবৃতি', 'État du résultat étendu'),
(730, 'bed_bill', 'Bed Bill', 'بيل السرير', 'বিছানা বিল', 'Bed Bill'),
(731, 'bed_payment', 'Bed Payment', 'دفع السرير', 'বিছানা পেমেন্ট', 'Paiement du lit'),
(732, 'pharmacy', 'Pharmacy', 'مقابل', 'ঔষধালয়', 'Pharmacie'),
(733, 'stock_quantity', 'Stock Quantity', 'كمية المخزون', 'স্টক পরিমাণ', 'Quantité en stock'),
(734, 'add_stock', 'Add Stock', 'أضف سهم', 'স্টক যোগ করুন', 'Ajouter un stock'),
(735, 'app_qr_code', 'App QR Code', 'التطبيق QR Code', 'অ্যাপ QR কোড', 'Code QR de l\'application'),
(736, 'show_qr_code', 'Show QR Code', 'إظهار رمز الاستجابة السريعة', 'QR কোড দেখান', 'Afficher le code QR'),
(737, 'language_proficiency', 'Language Proficiency', NULL, NULL, NULL),
(738, 'your_appointment_already_taken', 'Your appointment already taken!', NULL, NULL, NULL),
(739, 'content_language', 'Content Language', NULL, NULL, NULL),
(740, 'admission', 'Admission', NULL, NULL, NULL),
(741, 'yes', 'Yes', NULL, NULL, NULL),
(742, 'no', 'No', NULL, NULL, NULL),
(743, 'patient_visit', 'Patient Visit', NULL, NULL, NULL),
(744, 'complete_bill_list', 'Complete Bill List', NULL, NULL, NULL),
(745, 'trial_balance_with_opening', 'Trail Balance With Opening', NULL, NULL, NULL),
(746, 'as_on', 'As On', NULL, NULL, NULL),
(747, 'add_banner_slider', 'Add Banner Slider', NULL, NULL, NULL),
(748, 'banner_list', 'Banner List', NULL, NULL, NULL),
(749, 'edit_banner', 'Edit Banner', NULL, NULL, NULL),
(750, 'search', 'Search', NULL, NULL, NULL),
(751, 'common_settings', 'Common settings', NULL, NULL, NULL),
(752, 'map_show_by', 'Map Show By', NULL, NULL, NULL),
(753, 'embed', 'Embed', NULL, NULL, NULL),
(754, 'api', 'API', NULL, NULL, NULL),
(755, 'google_map_api', 'Google Map API', NULL, NULL, NULL),
(756, 'google_map_embed', 'Google Map Embed', NULL, NULL, NULL),
(757, 'import_csv_data', 'Import CSV Data', NULL, NULL, NULL),
(758, 'sample_csv', 'Sample CSV', NULL, NULL, NULL),
(759, 'edit_prescription', 'Edit Prescription', NULL, NULL, NULL),
(760, 'add_new_patient', 'Add New Patient', NULL, NULL, NULL),
(761, 'create_setting', 'Create Setting', NULL, NULL, NULL),
(762, 'common_setting', 'Common Settings', NULL, NULL, NULL),
(763, 'auto_update', 'Auto Update', NULL, NULL, NULL),
(764, 'in_patient_observation', 'In Patient Observation', NULL, NULL, NULL),
(765, 'vital_sign', 'Vital Sign', NULL, NULL, NULL),
(766, 'input_output_chart', 'Input Output Chart', NULL, NULL, NULL),
(767, 'medication_management', 'Medication Management', NULL, NULL, NULL),
(768, 'patient_observation', 'Patient Observation', NULL, NULL, NULL),
(769, 'add_vital_sign', 'Add Vital Sign', NULL, NULL, NULL),
(770, 'vital_sign_edit', 'Vital Sign Edit', NULL, NULL, NULL),
(771, 'fetal_heart_rate', 'Fetal Heart Rate', NULL, NULL, NULL),
(772, 'respiration_rate', 'Respiration Rate', NULL, NULL, NULL),
(773, 'pulse_rate', 'Pulse Rate', NULL, NULL, NULL),
(774, 'temperature', 'Temperature', NULL, NULL, NULL),
(775, 'vital_sign_list', 'Vital Sign List', NULL, NULL, NULL),
(776, 'vital_sing_list', 'Vital Sign List', NULL, NULL, NULL),
(777, 'add_input_output_chart', 'Add Input Output Chart', NULL, NULL, NULL),
(778, 'type_of_input', 'Type Of Input And Amount in ML', NULL, NULL, NULL),
(779, 'input_amount', 'Input Amount', NULL, NULL, NULL),
(780, 'input_time', 'Input Time', NULL, NULL, NULL),
(781, 'type_of_output', 'Type Of Output And Amount in ML', NULL, NULL, NULL),
(782, 'output_amount', 'Output Amount', NULL, NULL, NULL),
(783, 'output_time', 'Output Time', NULL, NULL, NULL),
(784, 'remarks', 'Remarks', NULL, NULL, NULL),
(785, 'inputoutput_edit', 'Edit Input Output Chart', NULL, NULL, NULL),
(786, 'input_output_list', 'Input Output List', NULL, NULL, NULL),
(787, 'print_patient_observation', 'Patient Observation Print', NULL, NULL, NULL),
(788, 'print_inputoutput_chart', 'Print Input Output Chart', NULL, NULL, NULL),
(789, 'medication_details', 'Medication Details', NULL, NULL, NULL),
(790, 'generic_name', 'Generic Name', NULL, NULL, NULL),
(791, 'strength', 'Strength', NULL, NULL, NULL),
(792, 'manufacturer', 'Receive From', NULL, NULL, NULL),
(793, 'medicine_shelf', 'Medicine Shelf', NULL, NULL, NULL),
(794, 'pack_size', 'Pack Size', NULL, NULL, NULL),
(795, 'box_qty', 'Box Qty', NULL, NULL, NULL),
(796, 'total_unit', 'Total Unit', NULL, NULL, NULL),
(797, 'purchase_price_withvat', 'Purchase Price With Vat', NULL, NULL, NULL),
(798, 'sale_price', 'Sale Price', NULL, NULL, NULL),
(799, 'unit_pp', 'Unit Purchase Price', NULL, NULL, NULL),
(800, 'unit_sp', 'Unit Sale Price', NULL, NULL, NULL),
(801, 'add_manufacturer', 'Add Receive From', NULL, NULL, NULL),
(802, 'manufacturer_name', 'Manufacturer Name', NULL, NULL, NULL),
(803, 'manufacturer_list', ' Receive From List', NULL, NULL, NULL),
(804, 'manufacturer_edit', 'Receiver Edit', NULL, NULL, NULL),
(805, 'add_medicine_type', 'Add Medicine Type', NULL, NULL, NULL),
(806, 'medicine_type_list', 'Medicine Type List', NULL, NULL, NULL),
(807, 'type_name', 'Type Name', NULL, NULL, NULL),
(808, 'medicine_type_edit', 'Medicine Type Edit', NULL, NULL, NULL),
(809, 'select_type', 'Select Type', NULL, NULL, NULL),
(810, 'select_manufacturer', 'Select Manufacturer', NULL, NULL, NULL),
(811, 'purchase', 'Purchase', NULL, NULL, NULL),
(812, 'add_purchase', 'Add Purchase', NULL, NULL, NULL),
(813, 'purchase_list', 'Purchase List', NULL, NULL, NULL),
(814, 'avail_qty', 'stock Qty', NULL, NULL, NULL),
(815, 'unit_price', 'Unit Price', NULL, NULL, NULL),
(816, 'batch_id', 'Batch ID', NULL, NULL, NULL),
(817, 'expiry', 'Expiry', NULL, NULL, NULL),
(818, 'purchase_details', 'Purchase Details', NULL, NULL, NULL),
(819, 'select_one', 'Select One', NULL, NULL, NULL),
(820, 'select_batch', 'Select Batch', NULL, NULL, NULL),
(821, 'stock_management', 'Stock Management', NULL, NULL, NULL),
(822, 'stock_list', 'Stock List', NULL, NULL, NULL),
(823, 'in_qty', 'In Qty', NULL, NULL, NULL),
(824, 'out_qty', 'Out Qty', NULL, NULL, NULL),
(825, 'stock', 'Stock', NULL, NULL, NULL),
(826, 'medicine_category', 'Medicine Category', NULL, NULL, NULL),
(827, 'list', 'List', NULL, NULL, NULL),
(828, 'medicine', 'Medicine', NULL, NULL, NULL),
(829, 'box_qty', 'Box Qty', NULL, NULL, NULL),
(830, 'medicine_issue', 'Medicine Issue', NULL, NULL, NULL),
(831, 'add_issue', 'Add Issue', NULL, NULL, NULL),
(832, 'issue_list', 'Issue List', NULL, NULL, NULL),
(833, 'issue_details', 'Issue Details', NULL, NULL, NULL),
(834, 'issue_date', 'Issue Date ', NULL, NULL, NULL),
(835, 'print_vital_sign', 'Print Vital Sign', NULL, NULL, NULL),
(836, 'branch', 'Branch ', NULL, NULL, NULL),
(837, 'add_branch', 'Add Branch', NULL, NULL, NULL),
(838, 'branch_list', 'Branch List', NULL, NULL, NULL),
(839, 'select_branch', 'Select Branch', NULL, NULL, NULL),
(840, 'referrer', 'Referrer', NULL, NULL, NULL),
(841, 'add_referrer', 'Add Referrer', NULL, NULL, NULL),
(842, 'referrer_list', 'Referrer List', NULL, NULL, NULL),
(843, 'confirm_password', 'Confirm Password', NULL, NULL, NULL),
(844, 'zip_code', 'Zip Code', NULL, NULL, NULL),
(845, 'state', 'State', NULL, NULL, NULL),
(846, 'city', 'City', NULL, NULL, NULL),
(847, 'address1', 'Address 1', NULL, NULL, NULL),
(848, 'address2', 'Address 2', NULL, NULL, NULL),
(849, 'speciality', 'Speciality', NULL, NULL, NULL),
(850, 'company_name', 'Company Name', NULL, NULL, NULL),
(851, 'office_phone', 'Office Phone', NULL, NULL, NULL),
(852, 'select_your_branch', 'Select Your Branch', NULL, NULL, NULL),
(853, 'referrer_appointment', 'Referrer Appointment', NULL, NULL, NULL),
(854, 'decline_successfully', 'Decline Successful', NULL, NULL, NULL),
(855, 'approved_appointment', 'Today\'s Approved Appointment', NULL, NULL, NULL),
(856, 'declined_appointment', 'Today\'s Declined Appointment', NULL, NULL, NULL),
(857, 'refer_by', 'referred by', NULL, NULL, NULL),
(858, 'ref_company', 'Referred Company ', NULL, NULL, NULL),
(859, 'branch_name', 'Branch Name', NULL, NULL, NULL),
(860, 'request_appointment_list', 'Requested Appointment List', NULL, NULL, NULL),
(861, 'approve_appointment_list', 'Approved Appointment List', NULL, NULL, NULL),
(862, 'decline_appointment_list', 'Decline Appointment List', NULL, NULL, NULL),
(863, 'insurance_company', 'Insurance Company Name', NULL, NULL, NULL),
(864, 'insurance_policy', 'Insurance Policy', NULL, NULL, NULL),
(865, 'edit_appointment', 'Edit Appointment', NULL, NULL, NULL),
(866, 'successfully_updated', 'Successfully Updated', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mail_setting`
--

CREATE TABLE IF NOT EXISTS `mail_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protocol` varchar(20) NOT NULL,
  `mailpath` varchar(255) NOT NULL,
  `mailtype` varchar(20) NOT NULL,
  `validate_email` varchar(20) NOT NULL,
  `wordwrap` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mail_setting`
--

INSERT INTO `mail_setting` (`id`, `protocol`, `mailpath`, `mailtype`, `validate_email`, `wordwrap`) VALUES
(5, 'mail', '/usr/sbin/sendmail', 'text', 'true', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `main_department`
--

CREATE TABLE IF NOT EXISTS `main_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_department`
--

INSERT INTO `main_department` (`id`, `name`, `description`, `branch_id`, `status`) VALUES
(1, 'Science Departments', 'Science Departments Science Departments', 0, 1),
(2, 'Clinical Departments', 'testing', 0, 1),
(3, 'Surgery', 'surgery', 0, 1),
(4, 'Medicine', 'Medicine Section', 0, 1),
(5, 'Main department', 'Description', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `main_department_lang`
--

CREATE TABLE IF NOT EXISTS `main_department_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_id` int(11) NOT NULL,
  `language` varchar(15) CHARACTER SET utf8 NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_department_lang`
--

INSERT INTO `main_department_lang` (`id`, `main_id`, `language`, `name`, `description`, `status`) VALUES
(1, 4, 'bangla', 'ঔষধ', 'ঔষধ', 1),
(2, 4, 'arabic', 'دواء', 'دواء', 1),
(3, 4, 'french', 'Médicament', 'Médicament', 1),
(4, 4, 'english', 'Medicine', 'Medicine', 1),
(5, 3, 'english', 'Surgery', 'Surgery', 1),
(6, 3, 'bangla', 'সার্জারি', 'সার্জারি', 1),
(7, 3, 'french', 'chirurgie', 'chirurgie', 1),
(8, 3, 'arabic', 'العملية الجراحية', 'العملية الجراحية', 1),
(9, 2, 'english', 'Clinical Departments', 'Clinical Departments', 1),
(10, 2, 'arabic', 'الأقسام السريرية', 'الأقسام السريرية', 1),
(11, 2, 'bangla', 'ক্লিনিকাল বিভাগ', 'ক্লিনিকাল বিভাগ', 1),
(12, 2, 'french', 'Départements Cliniques', 'Départements Cliniques', 1),
(13, 1, 'english', 'Science Departments', 'Science Departments', 1),
(14, 1, 'french', 'Départements scientifiques', 'Départements scientifiques', 1),
(15, 1, 'arabic', 'أقسام العلوم', 'أقسام العلوم', 1),
(16, 1, 'bangla', 'বিজ্ঞান বিভাগ', 'বিজ্ঞান বিভাগ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer_information`
--

CREATE TABLE IF NOT EXISTS `manufacturer_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_name` varchar(150) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manufacturer_information`
--

INSERT INTO `manufacturer_information` (`id`, `manufacturer_name`, `phone`, `email`, `address`, `branch_id`, `status`) VALUES
(1, 'Reckit & Benkeizer Ltd.', '', 'rebenzer@gmail.com', 'Nigeria', 0, 1),
(2, 'Glaxo Pharmaceuticals Ltd.', '', '', 'Nigeria', 0, 1),
(3, 'Bdtask', '3214234', 'bdtask@gmail.com', 'sdfsdf', 1, 1),
(4, 'H.M Shahriar', '234234', 'shahriar@gmail.com', 'Khilkhete', 1, 1),
(5, 'Mdsu', '23423', 'md@su.com', 'Khilkhete', 1, 1),
(6, 'saiful Islam', '345345', 'saiful@gmail.com', '234', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `medication`
--

CREATE TABLE IF NOT EXISTS `medication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `medicine_id` int(11) NOT NULL,
  `dosage` int(3) NOT NULL,
  `per_day_intake` int(1) NOT NULL,
  `full_stomach` tinyint(1) DEFAULT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `other_instruction` text COLLATE utf8_unicode_ci NOT NULL,
  `prescribe_by` int(11) NOT NULL,
  `intake_time` time NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `medication`
--

INSERT INTO `medication` (`id`, `patient_id`, `medicine_id`, `dosage`, `per_day_intake`, `full_stomach`, `from_date`, `to_date`, `other_instruction`, `prescribe_by`, `intake_time`, `branch_id`) VALUES
(1, 'PZRIMPJ4', 2, 500, 2, 1, '2019-01-26', '2019-02-26', 'bn', 38, '08:25:15', 0),
(2, 'PECW0N53', 3, 0, 3, 0, '2019-08-25', '2019-08-31', 'sdfsdf', 39, '04:25:30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `medication_details`
--

CREATE TABLE IF NOT EXISTS `medication_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medication_id` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `given_time` time NOT NULL,
  `next_due_time` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medication_details`
--

INSERT INTO `medication_details` (`id`, `medication_id`, `medicine_id`, `given_time`, `next_due_time`) VALUES
(1, 2, 4, '06:48:02', '18:45:30'),
(2, 2, 4, '06:48:02', '17:35:30');

-- --------------------------------------------------------

--
-- Table structure for table `medication_p`
--

CREATE TABLE IF NOT EXISTS `medication_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `remarks` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medication_p`
--

INSERT INTO `medication_p` (`id`, `patient_id`, `date`, `remarks`, `branch_id`, `assign_by`) VALUES
(1, 'PZRIMPJ4', '2019-07-20', 'Master', 0, '2'),
(2, 'PZRIMPJ4', '2019-08-24', 'ggg', 1, '2');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_type`
--

CREATE TABLE IF NOT EXISTS `medicine_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medicine_type`
--

INSERT INTO `medicine_type` (`id`, `type_name`) VALUES
(2, 'Inflamatory'),
(3, 'Antipsychotic'),
(4, 'Antidepressant '),
(5, 'Mood stabilizers'),
(6, 'Inhalants'),
(7, 'Cannabis');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `datetime` datetime NOT NULL,
  `sender_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=unseen, 1=seen, 2=delete',
  `receiver_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=unseen, 1=seen, 2=delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `sender_id`, `receiver_id`, `subject`, `message`, `datetime`, `sender_status`, `receiver_status`) VALUES
(1, 39, 37, ' nmb nm', '<p>nmbnm</p>', '2019-01-19 12:32:13', 1, 2),
(2, 39, 70, 'nm,', '<p>nbmbnmb</p>', '2019-01-19 12:34:55', 1, 1),
(3, 2, 37, 'Hello test', '<p>bbvjnhj</p>', '2019-01-20 05:17:49', 1, 0),
(4, 39, 73, 'What about Hospital patient', '<p>i want to know the status of my patients</p>', '2019-01-24 11:02:22', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `directory` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES
(1, 'Dashboard', 'Dashboard', '', 'dashboard', 1),
(3, 'Department', 'department System', 'application/modules/store/assets/images/thumbnail.jpg', 'department', 1),
(4, 'Doctor', 'Doctor', 'application/modules/user_role/assets/images/thumbnail.jpg', 'doctor', 1),
(5, 'Patient', 'Patient', 'application/modules/employee/assets/images/thumbnail.jpg', 'patient', 1),
(6, 'Schedule', 'Schedule', 'application/modules/customer/assets/images/thumbnail.jpg', 'schedule', 1),
(7, 'Appointment', 'Appointment', 'application/modules/product/assets/images/thumbnail.jpg', 'appointment', 1),
(8, 'Prescription', 'Prescription', 'application/modules/category/assets/images/thumbnail.jpg', 'prescription', 1),
(9, 'Account Manager', 'Account Manager', 'application/modules/supplier/assets/images/thumbnail.jpg', 'account_manager', 1),
(11, 'Insurance', 'Insurance', 'application/modules/warehouse/assets/images/thumbnail.jpg', 'insurance', 1),
(12, 'Billing', 'Billing', 'application/modules/sale/assets/images/thumbnail.jpg', 'billing', 1),
(13, 'Human Resources', 'Human Resources', 'application/modules/lease/assets/images/thumbnail.jpg', 'human_resources', 1),
(14, 'Bed Manager', 'Bed Manager', 'application/modules/payment/assets/images/thumbnail.jpg', 'bed_manager', 1),
(15, 'Noticeboard', 'Noticeboard', 'application/modules/stockmovment/assets/images/thumbnail.jpg', 'noticeboard', 1),
(16, 'Case Manager', 'Case Manager', 'application/modules/store/assets/images/thumbnail.jpg', 'case_manager', 1),
(17, 'Hospital Activities', 'Hospital Activities', 'application/modules/store/assets/images/thumbnail.jpg', 'hospital_activities', 1),
(18, 'Enquiry', 'Enquiry', 'application/modules/store/assets/images/thumbnail.jpg', 'enquiry', 1),
(19, 'Setting', 'Setting', 'application/modules/store/assets/images/thumbnail.jpg', 'setting', 1),
(20, 'SMS', 'SMS', 'application/modules/store/assets/images/thumbnail.jpg', 'sms', 1),
(21, 'Messages', 'Messages', 'application/modules/store/assets/images/thumbnail.jpg', 'messages', 1),
(22, 'Mail', 'Mail', 'application/modules/store/assets/images/thumbnail.jpg', 'mail', 1),
(23, 'Website', 'Website', 'application/modules/store/assets/images/thumbnail.jpg', 'website', 1),
(24, 'Permission', 'Permission', 'application/modules/store/assets/images/thumbnail.jpg', 'permission', 1),
(25, 'Medications and Visits', 'Medications and Visits', '', 'medication_visit', 1),
(26, 'Pharmacy', 'Hospital Pharmacy', '', 'pharmacy', 1),
(27, 'In Patient Observation', 'In Patient Observation', '', 'in_patient_observation', 1),
(28, 'Vital Sign', NULL, '', 'vital_sign', 1),
(29, 'Branch', NULL, '', 'branch', 1),
(30, 'Referrer', NULL, '', 'referrer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE IF NOT EXISTS `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `assign_by` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notice`
--

INSERT INTO `notice` (`id`, `title`, `description`, `start_date`, `end_date`, `assign_by`, `branch_id`, `status`) VALUES
(1, 'xzcZX', '<p>zx</p>', '2019-01-11', '2019-01-20', 2, 0, 1),
(2, 'Test NOtice', '<p>Some notice text here</p>', '2019-08-27', '2019-08-31', 2, 1, 1),
(3, 'Holiday', '<p>It is holiday on friday 23rd December</p>', '2019-09-10', '2019-09-24', 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE IF NOT EXISTS `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `blood_group` varchar(10) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `affliate` varchar(50) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `refer_by` int(11) DEFAULT NULL,
  `is_insurance` tinyint(4) NOT NULL,
  `insurance_company` varchar(200) DEFAULT NULL,
  `insurance_policy` varchar(200) DEFAULT NULL,
  `user_role` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `patient_id`, `firstname`, `lastname`, `email`, `password`, `phone`, `mobile`, `address`, `sex`, `blood_group`, `date_of_birth`, `affliate`, `picture`, `created_by`, `create_date`, `branch_id`, `status`, `refer_by`, `is_insurance`, `insurance_company`, `insurance_policy`, `user_role`) VALUES
(1, 'PG1E53IT', 'Marco', 'S. Scott', 'Marcosco@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '5345345', '4454543545343', '411 Cunningham Court\r\nSouthfield, MI 48235', 'Male', 'B-', '1984-01-13', NULL, '', 2, '2019-01-28', 0, 1, NULL, 0, NULL, NULL, ''),
(2, 'PK9QNQ5E', 'John', 'Glasper', 'glasper@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '54456456', '45346457', '4401 Golden Ridge Road\r\nAlbany, NY 12207', 'Male', 'A-', '1983-01-07', NULL, '', 2, '2019-01-28', 0, 1, NULL, 0, NULL, NULL, ''),
(3, 'PKK4W6QT', 'Lourdes ', 'Eubanks', 'lourdes@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '45646', '098765445345', '3857 Ben Street\r\nLansing, MI 48933', 'Male', 'B+', '1986-01-02', NULL, '', 2, '2019-01-28', 0, 1, NULL, 0, NULL, NULL, ''),
(4, 'P08MP6WQ', 'Roger ', 'Haywood', 'haywood@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '4587654', '765433456789', '83 Pine Street\r\nNew Kensington, PA 15068', 'Male', 'AB-', '2000-01-12', NULL, '', 2, '2019-01-28', 0, 1, NULL, 0, NULL, NULL, ''),
(5, 'P69YN5VJ', 'Ashraf ', 'Rahman', 'ashraf@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '347543756', '0987654456776', 'Dhaka, Bangladesh', 'Male', 'O-', '1996-01-18', NULL, '', 2, '2019-01-28', 0, 1, NULL, 0, NULL, NULL, ''),
(6, 'PTHITYJC', 'Linda ', 'Logan', 'lindalo@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '76543234', '7978675757645', '104 Peck Street\r\nLisbon, NH 03585', 'Male', 'B-', '2001-01-26', NULL, '', 2, '2019-01-28', 0, 1, NULL, 0, NULL, NULL, ''),
(7, 'P4PCCAGG', 'Rick', 'Thompson', 'thompson@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '3183361495', '896489455453', '3340 Roguski Road\r\nVidalia, LA 71373', 'Male', 'A-', '1978-01-04', NULL, '', 2, '2019-01-28', 0, 1, NULL, 0, NULL, NULL, ''),
(8, 'PX4HE1WM', 'Billy ', 'Diehl', 'billyk@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '654345990', '7865897655345', '281 Patterson Street\r\nHouston, TX 77092', 'Male', 'A+', '1982-01-07', NULL, '', 2, '2018-11-26', 0, 1, NULL, 0, NULL, NULL, ''),
(9, 'PZRIMPJ4', 'Christopher', 'C. Salmon', 'christopher21@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '87443226543', 'London', 'Female', 'B+', '2001-01-17', NULL, '', 2, '2018-11-26', 0, 1, NULL, 0, NULL, NULL, ''),
(10, 'PEMTVO2A', 'Derrick', 'M. Rigney', 'derrikr@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '7654', '34567897890', '1770 Carriage Lane\r\nToledo, OH 43609', 'Male', 'B+', '1979-01-11', NULL, '', 2, '2018-11-26', 0, 1, NULL, 0, NULL, NULL, ''),
(11, 'PN8DKMJG', 'Otto', 'J. Voigt', 'ottojv@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '8675675675675', '1843 Cedar Lane\r\nSomerville, MA 02143', 'Male', 'B+', '1986-01-09', NULL, '', 2, '2018-11-26', 0, 1, NULL, 0, NULL, NULL, ''),
(12, 'P90KREP7', 'Howard', 'M. McCoy', 'howardm@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '345678987654', '3393 Capitol Avenue\r\nLafayette, IN 47095', 'Male', 'B-', '1994-01-06', NULL, '', 2, '2018-09-27', 0, 1, NULL, 0, NULL, NULL, ''),
(14, 'P0PMGAQE', 'Rosie', 'A. Kelly', 'rosea@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '45678997654', '372 Blair Court\r\nBethany, MO 64424', 'Female', 'O+', '2004-01-22', NULL, '', 2, '2018-08-27', 0, 1, NULL, 0, NULL, NULL, ''),
(15, 'PY5G7FNA', 'Richard', 'V. Welch', 'richardv@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '87698765487', '2331 Cemetery Street\r\nSalinas, CA 93901', 'Male', 'O-', '1989-01-05', NULL, '', 2, '2018-08-27', 0, 1, NULL, 0, NULL, NULL, ''),
(16, 'PFTBOJTF', 'Donald', 'M. Smith', 'donaldm@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '09867540077', '4079 Blair Court\r\nKansas City, MO 64106', 'Male', 'AB+', '1989-01-11', NULL, '', 2, '2018-07-27', 0, 1, NULL, 0, NULL, NULL, ''),
(17, 'PFZ6JDKE', 'James', 'B. Roberts', 'jamesr@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '0123456787', '4494 Freed Drive\r\nStockton, CA 95204', 'Male', 'B-', '1976-01-08', NULL, '', 2, '2018-04-26', 0, 1, NULL, 0, NULL, NULL, ''),
(18, 'PY55TSNJ', 'Mark', 'N. Jeske', 'markn@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '012570587554', '4535 Martha Ellen Drive\r\nReno, NV 89501', 'Male', 'B+', '1998-01-14', NULL, '', 2, '2018-04-27', 0, 1, NULL, 0, NULL, NULL, ''),
(19, 'PDDLXVWI', 'Peter', 'S. Leu', 'peterj@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '85675753445', '4459 Park Avenue\r\nSacramento, CA 95814', 'Male', 'B-', '2000-01-26', NULL, '', 2, '2018-04-27', 0, 1, NULL, 0, NULL, NULL, ''),
(20, 'PUUVC1CY', 'Hilda', 'P. Lambert', 'hildapl@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '01745653955', '610 Commerce Boulevard\r\nColumbus, NE 68601', 'Male', 'B+', '1990-01-04', NULL, '', 2, '2018-04-27', 0, 1, NULL, 0, NULL, NULL, ''),
(21, 'P7YM5ZSG', 'Colleen', 'R. Matos', 'colleenr@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '98765438906', '1811 Hartway Street\r\nBradley, SD 57217', 'Male', 'A-', '1987-01-15', NULL, '', 2, '2018-03-27', 0, 1, NULL, 0, NULL, NULL, ''),
(22, 'PDQQ75PP', 'George ', 'A. White', 'georgea@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '9876543234', '325 Westwood Avenue\r\nHuntington, NY 11743', 'Male', 'O+', '1999-01-28', NULL, '', 2, '2018-03-27', 0, 1, NULL, 0, NULL, NULL, ''),
(23, 'PRQXXD9Q', 'Solomon', 'B. Boston', 'solomanb@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '98763453243', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2018-11-27', 0, 1, NULL, 0, NULL, NULL, ''),
(24, 'P1WY4YCU', 'Maurice', 'M. Atchison', 'mauricem@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '809567897654', 'usa', 'Male', 'A-', '1997-01-21', NULL, '', 2, '2018-12-27', 0, 1, NULL, 0, NULL, NULL, ''),
(25, 'P45E9QXP', 'Michael', 'N. Hayes', 'michaeln@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '09876543456', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2018-07-27', 0, 1, NULL, 0, NULL, NULL, ''),
(26, 'PMTGGP7A', 'Jose', 'S. Low', 'Jose321@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '012485673768', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-27', 0, 1, 1, 0, NULL, NULL, ''),
(27, 'P86DHOFT', 'Celeste', 'G. Brown', 'Celeste@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '28789784534', 'usa', 'Female', '', '2003-01-08', NULL, '', 2, '2019-02-26', 0, 1, NULL, 0, NULL, NULL, ''),
(28, 'P6J76U0K', 'Daniel', 'C. Haskins', 'Daniel23@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '90876543457', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-02-26', 0, 1, NULL, 0, NULL, NULL, ''),
(29, 'PEPWILVC', 'Richard', 'A. Hamel', 'Richard34@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '486889435', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(30, 'PJ916RXG', 'Thomas', 'A. Hamel', 'Thomas131@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '8743232556', 'usa', 'Male', 'O+', '2005-01-05', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(31, 'PV5KRZJ1', 'Troy', 'A. Hamel', 'Troy647@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '0557348785', 'usa', 'Male', 'O-', '1973-01-03', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(32, 'PK5L3G3G', 'Michael', ' R. Vega', 'Michaelrv@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '0546675456', 'usa', 'Male', 'B-', '2005-01-20', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(33, 'P6WFT0SK', 'Otis', 'I. Levine', 'Otisi@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '04378538765', 'usa', 'Male', 'B-', '2002-01-28', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(34, 'P0IK5GYT', 'Kelly', 'A. Bodine', 'Kellya@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '987654568', 'usa', 'Female', 'B+', '2000-01-19', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(35, 'P4MHVMZT', 'Geraldine', 'R. Flores', 'Geraldine21@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '98765434567', '12412, usa', 'Female', 'A-', '1988-01-21', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(36, 'P5J949EY', 'Wanda', 'R. Flores', 'Wanda23@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '09876543234', 'usa', 'Female', 'AB+', '2000-01-24', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(37, 'PFKH0JGR', 'vxcv', 'cxvx', 'root@g.b', '827ccb0eea8a706c4c34a16891f84e7b', '01751194212', '01751194212', '98 Green Road', 'Male', 'B+', '2019-02-26', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(38, 'P8ZQ54X8', 'vxc', 'xcv', 'root@ghfg.gyh', '827ccb0eea8a706c4c34a16891f84e7b', '01751194212', '01751194212', '98 Green Road', 'Male', 'O-', '2019-02-26', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(39, '121233043131426', 'Merry', 'Trumb', 'merryt@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '1751194212', '1751194212', 'test\r\ntest', 'Male', 'O+', '2019-03-13', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(46, NULL, 'Md', 'Tuhin', 'terter@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '1751194212', '98 Green Road2', 'Male', 'B-', '2019-03-13', NULL, '', 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(47, 'PECW0N53', 'Md', 'Tuhin', NULL, '827ccb0eea8a706c4c34a16891f84e7b', NULL, '1751194212', NULL, 'Male', 'B+', NULL, NULL, NULL, 2, '2019-03-13', 0, 1, NULL, 0, NULL, NULL, ''),
(48, NULL, 'Walk In', 'Patient', 'walking@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '32424234', 'Walking  Patient', 'Male', '', '2019-07-23', NULL, '', 2, '2019-07-23', 0, 1, NULL, 0, NULL, NULL, ''),
(51, NULL, 'Oladele ss', 'Shede', 'wemiman@gokj.com', '827ccb0eea8a706c4c34a16891f84e7b', '9031166980', '9031166980', 'casddsf', 'Male', 'O+', '1979-07-24', NULL, '', 2, '2019-07-24', 0, 1, NULL, 0, NULL, NULL, ''),
(52, 'P60FTN40', 'js', 'Prince', 'prince@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '0124574878', '34543534', 'fdgdsfg', 'Male', 'O+', '1990-08-09', NULL, '', 2, '2019-08-22', 0, 1, NULL, 0, NULL, NULL, ''),
(53, NULL, 'Md', 'Su', 'mdsu@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '34535', '345345', 'sdfasdfasdf', 'Male', 'B+', '1990-08-22', NULL, '', 2, '2019-08-22', 1, 1, NULL, 0, NULL, NULL, ''),
(54, NULL, 'js', 'malek', 'malek@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '234234', 'sdfsd', 'Male', 'O+', '1990-08-23', NULL, '', 2, '2019-08-25', 1, 1, NULL, 0, NULL, NULL, ''),
(55, 'PP37NOT8', 'Suvo', 'sm', 'suvo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '62111309244', '234234', 'sdfasdf', 'Male', 'A+', '2019-08-31', NULL, '', 76, '2019-08-31', 1, 1, 76, 0, NULL, NULL, ''),
(56, 'PHSMK844', 'Kobir ', 'Ahmad', 'kobir@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '01852376598', '01852376598', 'fdsfsdf', 'Male', 'O+', '1990-10-08', NULL, '', 76, '2019-09-04', 0, 1, 76, 0, NULL, NULL, ''),
(57, 'P8F1PZHH', 'Sm', 'Mosiur', 'mosiur@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '23442442', '3456345234', 'dsfasdf', 'Male', 'O+', '2019-09-04', NULL, '', 2, '2019-09-04', 1, 1, 76, 0, NULL, NULL, ''),
(58, 'PSYTJFD7', 'js', 'Jahangir', 'jahangir@gmail.com', 'a1a3e1728ecc9bfe4ca64599f0e661df', '2134213', '1234123', 'sdfsdf', 'Male', 'B+', '2019-09-04', NULL, '', 2, '2019-09-04', 1, 1, 76, 0, NULL, NULL, ''),
(59, NULL, 'Js', 'Rafiq', 'hmisahaq01@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '01852376598', '01852376598', 'Khilkhete', 'Male', 'O+', '1990-10-08', NULL, '', 2, '2019-09-07', 1, 1, NULL, 0, NULL, NULL, ''),
(60, 'PMK5OHQF', 'Md', 'Su', 'mdsu1@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '234234', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-07', 2, 1, NULL, 0, NULL, NULL, ''),
(61, 'PZ685B4I', 'sm', 'Shahabuddin', 'shabuddin@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '234234234', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-07', 2, 1, NULL, 0, NULL, NULL, ''),
(62, 'PJDW95W3', 'sm', 'Shahabsfduddin', 'shabuddinsdfsd@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '234234234', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-07', 2, 1, NULL, 0, NULL, NULL, ''),
(63, 'PC7EK6QR', 'Karim', 'Mubarak', 'mubarakkarim@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '234234', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-07', 2, 1, NULL, 0, NULL, NULL, ''),
(64, 'P1AKIU0O', 'sss', 'sfdsf', 'sdfasd@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '234234234234', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-07', 2, 1, NULL, 0, NULL, NULL, ''),
(65, 'PCLY6LSX', 'kkk', '234234', 'hmissdfsdfahaq01@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '23423423423', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-07', 2, 1, NULL, 0, NULL, NULL, NULL),
(66, 'PUW40T0G', 'jj', 'Rawling', 'rawling@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '234234', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-07', 2, 1, NULL, 0, NULL, NULL, NULL),
(67, 'P4M5A5XZ', 'kh', 'golam', 'golam@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '01730164623', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-07', 2, 1, NULL, 0, NULL, NULL, NULL),
(68, 'P8OMUDK3', 'Md', 'Babul', 'babul@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '01723833869', 'khilkhet', 'Male', 'O+', '2019-09-07', NULL, '', 2, '2019-09-07', 1, 1, 76, 0, NULL, NULL, NULL),
(69, 'PCQ2JONU', 'Km', 'Habib', 'hmisahaq@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', '01852376598', '01852376598', 'some thing here', 'Male', 'O+', '1990-10-08', NULL, '', 78, '2019-09-08', 2, 1, NULL, 0, NULL, NULL, NULL),
(70, 'PAT1LAGN', 'Touhidul ', 'Islam', 'touchid@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', NULL, '01955110016', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2019-09-08', 2, 1, NULL, 0, NULL, NULL, NULL),
(71, 'P8ZCQMXI', 'adding ', 'patient', 'adding@patient.com', '827ccb0eea8a706c4c34a16891f84e7b', '07484684826', '07484684826', '02 Biro House\r\n110 Stanley Road', 'Male', '', '2019-09-09', NULL, '', 39, '2019-09-09', 1, 1, NULL, 0, NULL, NULL, NULL),
(72, 'P2QWC5UN', 'Tanzil', 'bdtask', 'business@bdtask.com', '827ccb0eea8a706c4c34a16891f84e7b', '423432423', '234324324', '234 downtown abby', 'Male', 'A-', '2019-09-11', NULL, '', 82, '2019-09-11', 0, 1, 82, 0, NULL, NULL, NULL),
(73, 'P6BYBIP9', 'Tanzil', 'bdtask', 'business@bdtask.com', 'd41d8cd98f00b204e9800998ecf8427e', NULL, '432432423', '34 lkadjf', 'Male', 'O-', '2019-09-19', NULL, NULL, 82, '2019-09-11', NULL, 1, NULL, 0, NULL, NULL, NULL),
(74, 'PI1XJ3FW', 'tanzil2', 'bdtask', 'tanzil@tanzil.com', 'd41d8cd98f00b204e9800998ecf8427e', NULL, '3432432423', 'were 2343', 'Male', 'B+', '2019-09-18', NULL, NULL, 82, '2019-09-11', NULL, 1, NULL, 0, NULL, NULL, NULL),
(75, 'PPS3AFJB', 'tanzil3', 'bdtask', 'tanzil@tanzil.com', 'd41d8cd98f00b204e9800998ecf8427e', NULL, '324343', '34 dgfdgfd', 'Male', 'A-', '2019-09-18', NULL, NULL, 82, '2019-09-11', NULL, 1, NULL, 0, NULL, NULL, NULL),
(76, 'PXL3B2DJ', 'patientref', 'bdtask', 'patient@patient.com', '827ccb0eea8a706c4c34a16891f84e7b', '234324234', '23423432', '234234c fsfs sdf s', 'Male', 'B+', '2019-09-18', NULL, '', 82, '2019-09-11', 0, 1, 82, 0, NULL, NULL, NULL),
(77, 'PFR7U0FN', 'Rahim', 'Khan', 'rahim@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '01922297673', '01876565432', 'demo address', 'Male', 'O+', '2009-09-16', NULL, '', 84, '2019-09-12', 4, 1, 84, 0, NULL, NULL, NULL),
(78, 'PSKNIKJ2', 'Karim', 'khan', 'karim@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', NULL, '32452345', 'sdks', 'Male', '', '2019-09-02', NULL, NULL, 84, '2019-09-12', 4, 1, 84, 0, NULL, NULL, NULL),
(79, 'PS5U4FVL', 'patient', 'referred', 'patient@xyc.org', '827ccb0eea8a706c4c34a16891f84e7b', '', '32432423432', '105-46 Cross Bay Blvd', 'Male', '', '2019-09-17', NULL, '', 86, '2019-09-15', 3, 1, 86, 0, NULL, NULL, NULL),
(80, 'PG0VW841', '2nd patient', 'kaz', 'kaz@patient.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '324324324', '234324324', 'Male', '', '2019-09-15', NULL, '', 86, '2019-09-15', 3, 1, 86, 0, NULL, NULL, NULL),
(81, 'PZ1A14D7', 'Rafiq', 'hossain', 'rafi@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '01857675727', 'sdfsdfs', 'Male', 'A-', '2007-09-12', NULL, '', 88, '2019-09-16', 3, 1, 88, 1, 'Bdtask', '234232345DFRT', NULL),
(82, 'PWKOEB8U', 'dfgdf', 'dgfhdfgh', 'dfhg@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '45345345', 'dfgdfg', 'Male', 'A-', '2011-09-05', NULL, '', 96, '2019-09-16', 3, 1, 96, 1, 'gdfgh', '5345345fgdfg', NULL),
(83, 'PI27P99N', 'new', 'patient', 'new@patient.com', '827ccb0eea8a706c4c34a16891f84e7b', '7484684826', '7484684826', '02 Biro House\r\n110 Stanley Road', 'Male', 'A-', '2019-09-16', NULL, '', 2, '2019-09-16', 3, 1, 86, 1, 'Alpha Insurance company', '23423432WA', NULL),
(84, 'P8GSSZSF', 'Wajeh ul', 'Kazmi', 'wajeeh.kaz@gmail.com', 'd41d8cd98f00b204e9800998ecf8427e', NULL, '7484684826', '02 Biro House\r\n110 Stanley Road', 'Male', '', '2019-09-18', NULL, NULL, 82, '2019-09-17', 3, 1, 82, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pa_visit`
--

CREATE TABLE IF NOT EXISTS `pa_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(2) NOT NULL COMMENT '2=doctor and 5=nurse',
  `user_id` int(11) NOT NULL,
  `visit_date` date NOT NULL,
  `visit_time` time NOT NULL,
  `finding` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `instructions` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `fees` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pa_visit`
--

INSERT INTO `pa_visit` (`id`, `patient_id`, `user_type`, `user_id`, `visit_date`, `visit_time`, `finding`, `instructions`, `branch_id`, `fees`) VALUES
(1, 'PZRIMPJ4', 2, 0, '2019-01-26', '12:00:00', 'Improved', 'N/A', 0, 400);

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `language` varchar(15) NOT NULL,
  `title` varchar(60) NOT NULL,
  `institute` varchar(100) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`id`, `user_id`, `language`, `title`, `institute`, `from_date`, `to_date`, `description`, `status`) VALUES
(3, 42, 'english', 'Register Incharge', 'Apollo Hospital Limited', '2016-12-07', '2018-08-16', 'Responsibilities: Follow all Start of Day procedures, Maintain a high level of customer service at all times, Work as part of a team with emphasis on communication Follow all Start of Day procedures, Maintain a high level of customer service at all times,', 1),
(4, 42, 'french', 'Head Of Department', 'Cumbrian International  ', '2012-11-20', '2015-11-20', 'Websites are essential for bloggers and businesses. They provide a way for people to gather information, to interact, and to foster purchases that facilitate e-commerce. They provide a way for people to gather information', 1),
(6, 39, 'english', 'Assistant Professor', 'Shahabuddin Medical College', '2010-11-20', '2014-11-20', 'Responsibilities: Follow all Start of Day procedures, Maintain a high level of customer service at all times, Work as part of a team with emphasis on communication , Work as part of a team with emphas', 1),
(7, 38, 'english', 'Bdtask Hospital Limited 123', 'Shahbuddin Medical', '2015-11-22', '2018-11-22', 'Responsibilities: Follow all Start of Day procedures, Maintain a high level of customer service at all times, Work as part of a team with emphasis on communication especially a program, system, or inquiry)', 1),
(12, 40, 'english', 'Register', 'California', '2015-12-09', '2018-12-12', 'Responsibilities: Follow all Start of Day procedures, Maintain a high level of customer service at all times, Work as par customer service at all times, Work as par Day procedures, Maintain a high lev', 1),
(13, 39, 'french', 'Maître assistant', 'Collège médical Shahabuddin34', '2014-11-08', '2016-12-07', 'Une société ou une organisation ayant un objet ou un facteur commun, en particulier au collège médical de Shahbuddin Donc je suis heureux.  Il y a beaucoup de gens pour conduire une voiture. Les patients sont très à me trouver à l\'hôpital.', 1),
(14, 39, 'arabic', 'استاذ مساعد', 'كلية شهاب الدين الطبية', '2014-11-08', '2016-12-07', 'مجتمع أو منظمة لها هدف معين أو عامل مشترك ، لا سيما في كلية شاهبد الدين الطبية. لذلك أنا سعيد. هناك الكثير من الناس لقيادة السيارة. المريض جدا لي في المستشفى. الناس لقيادة السيارة. المريض جدا لي في المستشفى.', 1),
(15, 39, 'bangla', 'সহকারী অধ্যাপক', 'শাহাবুদ্দিন মেডিকেল কলেজ', '2014-11-08', '2016-12-07', 'একটি সমাজ বা সংগঠন বিশেষ বস্তু বা সাধারণ ফ্যাক্টর, বিশেষ করে শাহবুদ্দীন মেডিকেল কলেজেও। তাই আমি খুশি। ড্রাইভিং গাড়ী জন্য অনেক মানুষ আছে। রোগী আমাকে খুব হাসপাতালে খুঁজে বের করতে হয়। গাড়ী জন্য অনেক মানুষ আছে। রোগী আমাকে খুব হাসপাতালে খুঁজে বের করতে হয়।', 1),
(17, 50, 'english', 'Practical Intern Ship', 'Dhaka Medical College', '2016-11-15', '2017-01-12', 'Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. El punto de usar Lorem Ipsum es que tiene una distribución más o menos normal de las letras, al contrario de us', 1),
(18, 39, 'english', 'Duty Incharge', 'Dhaka Medical College', '2017-01-26', '2019-01-26', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, yo', 1),
(19, 39, 'bangla', 'দায়িত্ব ইনচার্জ', 'Dhaka Medical College', '2017-01-18', '2019-01-26', 'লোরেম ইপ্সামের প্যাসেজের অনেকগুলি বৈচিত্র রয়েছে তবে বেশির ভাগ ক্ষেত্রেই কিছুটা রূপান্তরিত হয়েছে, ইনজেক্টেড হাস্যরস বা র্যান্ডমাইজড শব্দগুলি যা সামান্য বিশ্বাসযোগ্য নয়। আপনি Lorem Ipsum এর একটি উত্তরণ ব্যবহার করতে যাচ্ছেন, তবে আপনার অবশ্যই নিশ্চিত হওয়া', 1),
(20, 39, 'arabic', 'واجب الرسوم', 'كلية دكا الطبية', '2017-01-26', '2019-01-26', 'هناك العديد من الاختلافات في مقاطع لوريم إيبسوم المتاحة ، ولكن الغالبية عانت من تغيير في شكل ما ، عن طريق الفكاهة المحقنة ، أو الكلمات العشوائية التي لا تبدو قابلة للتصديق بشكل طفيف. إذا كنت ستستخدم مقطع لوريم إيبسوم ، فأنت بحاجة إلى التأكد من عدم وجود أي', 1),
(21, 77, 'english', 'Chairman', ' Dhaka', '2017-01-26', '2019-11-30', 'Il existe de nombreuses variantes de passages de Lorem Ipsum disponibles, mais la plupart ont subi une altération sous une forme ou une autre, par injection d’humour, ou par des mots aléatoires qui ne semblent même pas très crédibles. Si vous allez utilis', 1),
(22, 70, 'english', 'Assistant Professor', 'Dhaka Medical College', '2010-01-26', '2015-01-26', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, yo', 1),
(23, 77, 'english', 'Maître assistant', 'Collège médical de Dhaka', '2010-01-26', '2015-01-26', 'Il existe de nombreuses variantes de passages de Lorem Ipsum disponibles, mais la plupart ont subi une altération sous une forme ou une autre, par injection d’humour, ou par des mots aléatoires qui ne semblent même pas très crédibles. Si vous allez utilis', 1),
(24, 70, 'bangla', 'সহকারী অধ্যাপক', 'ঢাকা মেডিকেল কলেজ', '2010-01-26', '2015-01-26', 'লোরেম ইপ্সামের প্যাসেজের অনেকগুলি বৈচিত্র রয়েছে তবে বেশির ভাগ ক্ষেত্রেই কিছুটা রূপান্তরিত হয়েছে, ইনজেক্টেড হাস্যরস বা র্যান্ডমাইজড শব্দগুলি যা সামান্য বিশ্বাসযোগ্য নয়। আপনি Lorem Ipsum এর একটি উত্তরণ ব্যবহার করতে যাচ্ছেন, তবে আপনার অবশ্যই নিশ্চিত হওয়া', 1),
(25, 70, 'arabic', 'استاذ مساعد', 'كلية دكا الطبية', '2010-01-26', '2015-01-26', 'هناك العديد من الاختلافات في مقاطع لوريم إيبسوم المتاحة ، ولكن الغالبية عانت من تغيير في شكل ما ، عن طريق الفكاهة المحقنة ، أو الكلمات العشوائية التي لا تبدو قابلة للتصديق بشكل طفيف. إذا كنت ستستخدم مقطع لوريم إيبسوم ، فأنت بحاجة إلى التأكد من عدم وجود أي', 1),
(26, 70, 'english', 'Assistant Professor', 'Apollo Hospital', '2016-01-26', '2019-01-26', 'All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to gene', 1),
(27, 70, 'arabic', 'استاذ مساعد', 'مستشفى أبولو', '2016-01-26', '2019-01-26', 'تميل جميع مولدات Lorem Ipsum على الإنترنت إلى تكرار قطع محددة مسبقًا حسب الضرورة ، مما يجعل هذا أول مولد حقيقي على الإنترنت. وهو يستخدم قاموسًا يضم أكثر من 200 كلمة لاتينية ، بالإضافة إلى حفنة من هياكل الجملة النموذجية ، لتوليد لوريم إيبسوم الذي يبدو معقو', 1),
(28, 70, 'bangla', 'সহকারী অধ্যাপক', 'অ্যাপোলো হাসপাতাল', '2016-01-26', '2019-01-26', 'ইন্টারনেটে সমস্ত লোরেম ইপ্সাম জেনারেটর প্রয়োজনীয় হিসাবে পূর্বনির্ধারিত অংশ পুনরাবৃত্তি ঝোঁক, এটি ইন্টারনেটে প্রথম সত্য জেনারেটর তৈরি। এটি 200 টিরও বেশি ল্যাটিন শব্দের একটি অভিধান ব্যবহার করে, যা বেশ কয়েকটি মডেল বাক্য কাঠামোর সাথে মিলিত হয়, যা লোরেম ইপ', 1),
(29, 70, 'french', 'Maître assistant', 'Hôpital Apollo', '2016-01-26', '2019-01-26', 'Tous les générateurs Lorem Ipsum sur Internet ont tendance à répéter autant que nécessaire les blocs prédéfinis, ce qui en fait le premier véritable générateur sur Internet. Il utilise un dictionnaire de plus de 200 mots latins, associé à une poignée de s', 1),
(30, 75, 'english', 'sfsdf', 'sdfsdf', '2019-08-22', '2019-08-22', 'sdf sdfasdfasdf sadfasdfasd aser t vetewrtwer tbrtwervewtb bet weverertvwetew rtvberter gerwterfgwe rtewvert rujhyjhtyumntr uyrtertqwerterwtvwetvfetvwee t dsadfasdfasdf asasdwer twertvrtretertertertretertwertewrtewrtewrtertertewrv rtwertwerthfdghrtyewrtew', 1),
(31, 77, 'english', 'Profesor', 'Dm', '2019-09-07', '2019-09-07', 'sdfasf asdsdafasdfasdf sadfasdfasdfasd sadfas dfasdf asdf asdfasd fasdfasdfasdfasdfasadfasdf asfasdfas dfasdfasdfasdfasdf sdfasdfasdfasdfasdfa sdfasdf asdfasdfasdfasdfsdfsadffasdf afsdasdfasdfsdsafasdfadf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pr_case_study`
--

CREATE TABLE IF NOT EXISTS `pr_case_study` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) DEFAULT NULL,
  `food_allergies` varchar(255) DEFAULT NULL,
  `tendency_bleed` varchar(255) DEFAULT NULL,
  `heart_disease` varchar(255) DEFAULT NULL,
  `high_blood_pressure` varchar(255) DEFAULT NULL,
  `diabetic` varchar(255) DEFAULT NULL,
  `surgery` varchar(255) DEFAULT NULL,
  `accident` varchar(255) DEFAULT NULL,
  `others` varchar(255) DEFAULT NULL,
  `family_medical_history` varchar(255) DEFAULT NULL,
  `current_medication` varchar(255) DEFAULT NULL,
  `female_pregnancy` varchar(255) DEFAULT NULL,
  `breast_feeding` varchar(255) DEFAULT NULL,
  `health_insurance` varchar(255) DEFAULT NULL,
  `low_income` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pr_case_study`
--

INSERT INTO `pr_case_study` (`id`, `patient_id`, `food_allergies`, `tendency_bleed`, `heart_disease`, `high_blood_pressure`, `diabetic`, `surgery`, `accident`, `others`, `family_medical_history`, `current_medication`, `female_pregnancy`, `breast_feeding`, `health_insurance`, `low_income`, `reference`, `branch_id`, `status`) VALUES
(1, 'PGQG6XSK', 'dsa', 'ads', 'ads', 'sd', '', '', '', 'gdfdgfdgbd', '', '', '', '', '', '', '', 1, 1),
(2, 'PG0VW841', 'nuts', 'mostly', 'heart disease', '', '', '', '', '', '', '', '', '', '', '', '', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pr_prescription`
--

CREATE TABLE IF NOT EXISTS `pr_prescription` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `appointment_id` varchar(30) DEFAULT NULL,
  `patient_id` varchar(30) NOT NULL,
  `patient_type` varchar(50) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `chief_complain` text DEFAULT NULL,
  `insurance_id` int(11) DEFAULT NULL,
  `policy_no` varchar(255) DEFAULT NULL,
  `weight` varchar(50) DEFAULT NULL,
  `blood_pressure` varchar(255) DEFAULT NULL,
  `medicine` text DEFAULT NULL,
  `diagnosis` text DEFAULT NULL,
  `visiting_fees` float DEFAULT NULL,
  `patient_notes` text DEFAULT NULL,
  `reference_by` varchar(50) DEFAULT NULL,
  `reference_to` varchar(50) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `refer_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pr_prescription`
--

INSERT INTO `pr_prescription` (`id`, `appointment_id`, `patient_id`, `patient_type`, `doctor_id`, `chief_complain`, `insurance_id`, `policy_no`, `weight`, `blood_pressure`, `medicine`, `diagnosis`, `visiting_fees`, `patient_notes`, `reference_by`, `reference_to`, `branch_id`, `date`, `refer_by`) VALUES
(1, 'ADGFDP38', 'PZRIMPJ4', 'New', 38, 'Skin ', 1, '1231', '', '', '[{\"name\":\"Napa\",\"type\":\"tbt\",\"instruction\":\"full stromach\",\"days\":\"3\"}]', '[{\"name\":\"Blood\",\"instruction\":\"Very good\"}]', 700, 'test', '', NULL, 0, '2019-01-28', NULL),
(2, 'AXFCTU8A', 'P4MHVMZT', 'New', 70, 'not good', 1, '1231', '', '', '[{\"name\":\"Napa\",\"type\":\"tbt\",\"instruction\":\"test\",\"days\":\"4\"}]', '[{\"name\":\"X-ray\",\"instruction\":\"hh\"}]', 1200, 'follow all', '', NULL, 0, '2019-01-28', NULL),
(3, 'ATPQI5A4', 'P0IK5GYT', 'New', 70, 'test', 0, '', '', '', '[{\"name\":\"Napa\",\"type\":\"tbt\",\"instruction\":\"good\",\"days\":\"5\"}]', '[{\"name\":\"Bloos\",\"instruction\":\"problem\"}]', 100, 'te', '', NULL, 0, '2018-06-26', NULL),
(4, 'AF9H71XW', 'PK5L3G3G', 'New', 50, 'back pain', 0, '', '', '', '[{\"name\":\"Cal-DS\",\"type\":\"cap\",\"instruction\":\"trt\",\"days\":\"30\"}]', '[{\"name\":\"blood\",\"instruction\":\"good\"}]', 800, 'follow', '', NULL, 0, '2018-06-27', NULL),
(5, 'A4ZQ1IVP', 'PMTGGP7A', 'New', 50, 'test', 0, '', '', '', '[{\"name\":\"Napa\",\"type\":\"tbt\",\"instruction\":\"tr\",\"days\":\"\"}]', '[{\"name\":\"bloos\",\"instruction\":\"good\"}]', 800, 'tr', '', NULL, 0, '2018-08-27', NULL),
(6, 'AR0BPBC9', 'P45E9QXP', 'New', 50, 'block pain', 0, '', '', '', '[{\"name\":\"Cal-DS\",\"type\":\"cap\",\"instruction\":\"rt\",\"days\":\"10\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 800, 'trw', '', NULL, 0, '2018-08-27', NULL),
(7, 'AT9AIOHO', 'P5J949EY', 'New', 40, 'tee', 0, '', '', '', '[{\"name\":\"Napa\",\"type\":\"tbt\",\"instruction\":\"hf\",\"days\":\"10\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 900, 'jg', '', NULL, 0, '2018-10-27', NULL),
(8, 'AEP0ASA0', 'P6J76U0K', 'New', 40, 'dfds', 0, '', '', '', '[{\"name\":\"Napa\",\"type\":\"tbt\",\"instruction\":\"jhj\",\"days\":\"5\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 900, 'fd', '', NULL, 0, '2018-10-27', NULL),
(9, 'AB5A1607', 'P1WY4YCU', 'New', 40, 'xd', 0, '', '', '', '[{\"name\":\"Napa\",\"type\":\"tbt\",\"instruction\":\"fgdf\",\"days\":\"4\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 900, 'dds', '', NULL, 0, '2018-11-27', NULL),
(10, 'AFZ28KX1', 'PV5KRZJ1', 'New', 39, 'xfd', 0, '', '', '', '[{\"name\":\"Napa\",\"type\":\"tbt\",\"instruction\":\"iyui\",\"days\":\"2\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 1000, 'fsd', '', NULL, 0, '2018-12-27', NULL),
(11, 'ANMZOY5D', 'P86DHOFT', 'New', 39, 'dasd', 0, '', '', '', '[{\"name\":\"Cal-DS\",\"type\":\"cap\",\"instruction\":\"klk\",\"days\":\"5\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 100, 'iou', '', NULL, 0, '2018-12-27', NULL),
(12, 'AVWTBVQW', 'PRQXXD9Q', 'New', 38, 'cxx', 0, '', '', '', '[{\"name\":\"Napa\",\"type\":\"tbi\",\"instruction\":\"uyt\",\"days\":\"3\"},{\"name\":\"Cal-DS\",\"type\":\"cap\",\"instruction\":\"follow all\",\"days\":\"30\"}]', '[{\"name\":\"Blood\",\"instruction\":\"gig\"}]', 700, 'ouio', '', NULL, 0, '2018-04-26', NULL),
(13, 'ADGFDP38', 'PZRIMPJ4', 'New', 38, 'xcz', 0, '', '', '', '[{\"name\":\"Napa\",\"type\":\"aS\",\"instruction\":\"sad\",\"days\":\"\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 700, 'opi', '', NULL, 0, '2018-03-26', NULL),
(14, 'A3Z19LAS2', 'PGQG6XSK', 'New', 2, 'fghgfhg', 0, '', '', '', '[{\"name\":\"ghh\",\"type\":\"hghgfh\",\"instruction\":\"vbvnbvnb\",\"days\":\"\"},{\"name\":\"\",\"type\":\"\",\"instruction\":\"\",\"days\":\"\"}]', '[{\"name\":\"gfhg\",\"instruction\":\"bvnbnbn\"},{\"name\":\"\",\"instruction\":\"\"}]', 30000, 'vcvcb', '', NULL, 0, '2019-07-24', NULL),
(15, 'A198OODZ2', 'P60FTN40', 'New', 2, 'Test Problem', 0, '', '62', '80', '[{\"name\":\"Metrox\",\"type\":\"capsul\",\"instruction\":\"sdfdsf\",\"days\":\"20\"},{\"name\":\"Napa Extra\",\"type\":\"tablet\",\"instruction\":\"sdfsdf\",\"days\":\"20\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 500, 'dfgds', '', NULL, 1, '2019-08-24', NULL),
(16, 'AT0UZDY62', 'PP37NOT8', 'New', 2, 'Back Pain', 0, '', '55', '', '[{\"name\":\"Napa Extra\",\"type\":\"Tablet\",\"instruction\":\"1+0+1\",\"days\":\"20\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 500, 'sdff', 'shofiqul', NULL, 1, '2019-09-01', 2),
(17, 'AZAZXH6Q76', 'PP37NOT8', 'New', 76, 'Pain', 0, '', '55', '', '[{\"name\":\"Napa\",\"type\":\"tablet\",\"instruction\":\"1+0+1\",\"days\":\"10\"}]', '[{\"name\":\"\",\"instruction\":\"\"}]', 300, 'sdfsdf', 'sss', NULL, 1, '2019-09-01', 76),
(18, 'AVORCH8A81', 'PG0VW841', 'New', 81, 'uyiuyiuyiuy', 0, '98798798', '', '', '[{\"name\":\"kjhkjhk kjh kjh \",\"type\":\"hgfghf \",\"instruction\":\"jg jhg jhg jhgjhg j hjg \",\"days\":\"7\"}]', '[{\"name\":\"jh kjh kh \",\"instruction\":\"kjh kjh kh kjh kkh \"}]', 0, '', '', NULL, 3, '2019-09-15', 81);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_details`
--

CREATE TABLE IF NOT EXISTS `purchase_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `batch_id` varchar(30) NOT NULL,
  `expiry` date NOT NULL,
  `qty` float NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_details`
--

INSERT INTO `purchase_details` (`id`, `purchase_id`, `medicine_id`, `batch_id`, `expiry`, `qty`, `rate`, `total`, `branch_id`) VALUES
(4, 1, 3, '', '0000-00-00', 10, 0.83, 8.30, 0),
(5, 1, 4, '', '0000-00-00', 2, 80.00, 160.00, 0),
(12, 2, 3, '213423', '2019-12-28', 4, 0.83, 3.32, 0),
(13, 2, 4, '3242342', '2019-10-31', 4, 80.00, 320.00, 0),
(14, 3, 3, '4542', '2019-10-26', 20, 0.83, 16.60, 0),
(17, 4, 2, '112', '2020-10-28', 400, 16.00, 6400.00, 0),
(18, 4, 5, '112', '2019-12-25', 200, 0.80, 160.00, 0),
(19, 5, 7, '3245234', '2019-11-30', 500, 1.50, 750.00, 0),
(27, 6, 8, '234234', '2019-11-30', 600, 1.08, 648.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `purchase_p`
--

CREATE TABLE IF NOT EXISTS `purchase_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `grandtotal` decimal(12,2) NOT NULL,
  `total_discount` decimal(5,2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` int(11) NOT NULL,
  `acc_vaoucher` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_p`
--

INSERT INTO `purchase_p` (`id`, `voucher_no`, `manufacturer_id`, `date`, `total`, `grandtotal`, `total_discount`, `branch_id`, `assign_by`, `acc_vaoucher`) VALUES
(1, 1000, 1, '2019-07-22', 168.30, 158.30, 10.00, 0, 2, NULL),
(2, 1001, 1, '2019-07-22', 323.32, 303.32, 20.00, 0, 2, NULL),
(3, 1002, 1, '2019-07-23', 16.60, 16.60, 0.00, 0, 2, NULL),
(4, 1003, 2, '2019-07-24', 6560.00, 6560.00, 0.00, 0, 2, NULL),
(5, 34234, 3, '2019-08-24', 750.00, 740.00, 10.00, 1, 2, NULL),
(6, 23423, 6, '2019-08-28', 648.00, 640.00, 8.00, 1, 2, '20190828122841');

-- --------------------------------------------------------

--
-- Table structure for table `referrer_appointment`
--

CREATE TABLE IF NOT EXISTS `referrer_appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` varchar(20) DEFAULT NULL,
  `patient_id` varchar(20) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `serial_no` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `problem` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `refer_by` int(11) DEFAULT NULL,
  `is_approve` int(11) DEFAULT 0,
  `doctor_comments` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referrer_appointment`
--

INSERT INTO `referrer_appointment` (`id`, `appointment_id`, `patient_id`, `department_id`, `doctor_id`, `schedule_id`, `serial_no`, `date`, `problem`, `created_by`, `create_date`, `branch_id`, `status`, `refer_by`, `is_approve`, `doctor_comments`) VALUES
(1, 'A5Z8IBUP', 'P60FTN40', 16, 39, 13, '1', '2019-09-04', 'test Problems', 76, '2019-09-04', 1, 1, 76, 2, NULL),
(2, 'AKFTIP7R', 'PHSMK844', 16, 39, 10, '1', '2019-09-04', 'Nasal drop out', 76, '2019-09-04', 1, 1, 76, 1, NULL),
(3, 'AEHAJW82', 'PHSMK844', 16, 39, 1, '1', '2019-09-04', 'js problems', 76, '2019-09-04', 1, 1, 76, 2, NULL),
(4, 'ANCUNEWZ', 'PHSMK844', 16, 39, 1, '1', '2019-09-04', 'ttt', 76, '2019-09-04', 2, 1, 76, 1, NULL),
(5, 'AJ3LBC47', 'PECW0N53', 16, 39, 1, '07:00 - 07:20', '2019-09-08', 'kkkk problem', 76, '2019-09-04', 2, 1, 76, 1, NULL),
(6, 'AN8SKTWW', 'PSYTJFD7', 16, 39, 11, '1', '2019-09-22', 'sdfsdf', 2, '2019-09-04', 1, 1, 76, 1, NULL),
(7, 'AD4OANXN', 'P6BYBIP9', 16, 81, 26, '16:00 - 16:30', '2019-09-16', 'gdgdfgfdg dgdfg dgdfg', 82, '2019-09-11', 3, 1, 82, 2, NULL),
(8, 'ASMH37XM', 'PI1XJ3FW', 16, 81, 26, '13:00 - 13:30', '2019-09-16', 'asfsafs', 82, '2019-09-11', 3, 1, 82, 1, NULL),
(9, 'AK2JWVFO', 'PPS3AFJB', 16, 81, 26, '15:30 - 16:00', '2019-09-16', 'sfdsfsd', 82, '2019-09-11', 3, 1, 82, 1, NULL),
(10, 'A0YQ1WI1', 'PXL3B2DJ', 16, 81, 26, '17:00 - 17:30', '2019-09-16', 'asfsf ad af asfd asdfdsf ', 82, '2019-09-11', 3, 1, 82, 1, NULL),
(11, 'ABOF92LI', 'PSKNIKJ2', 33, 83, 29, '06:28 - 06:48', '2019-09-12', 'demo problem', 84, '2019-09-12', 4, 1, 84, 1, 'Your appointment has approved'),
(12, 'A8CNQAKE', 'PSKNIKJ2', 33, 83, 29, '07:08 - 07:28', '2019-09-12', 'cg', 84, '2019-09-12', 4, 1, 84, 2, NULL),
(13, 'ATHXFUAL', 'PS5U4FVL', 16, 81, 26, '15:30 - 16:00', '2019-09-16', 'Referring test', 86, '2019-09-15', 3, 1, 86, 2, 'cant take patient right now.'),
(14, 'AZVQSVW3', 'PG0VW841', 16, 81, 26, '13:30 - 14:00', '2019-09-16', 'asdfsdf', 86, '2019-09-15', 3, 1, 86, 1, NULL),
(15, 'AGTXTWXF', 'P8GSSZSF', 16, 97, 40, '09:00 - 09:20', '2019-09-18', '', 82, '2019-09-17', 3, 1, 82, 0, NULL),
(16, 'AOR5UIY3', 'P8GSSZSF', 16, 97, 40, '09:20 - 09:40', '2019-09-25', 'sdfsdfsdf', 82, '2019-09-18', 3, 1, 82, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `referrer_information`
--

CREATE TABLE IF NOT EXISTS `referrer_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `speciality` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address1` text COLLATE utf8_unicode_ci NOT NULL,
  `address2` text COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `confirm_password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `referrer_information`
--

INSERT INTO `referrer_information` (`id`, `first_name`, `last_name`, `company_name`, `speciality`, `office_phone`, `mobile_phone`, `email`, `address1`, `address2`, `city`, `state`, `zip_code`, `password`, `confirm_password`, `status`) VALUES
(13, 'wajeeh', 'kazmi', 'LDGL', 'therapist', '97987987', '987987987987', 'waj@gmail.com', '343 down town', '110 bup rodad', 'london', 'New York', '987987', '827ccb0eea8a706c4c34a16891f84e7b', '827ccb0eea8a706c4c34a16891f84e7b', 1),
(16, 'tan', 'zil', 'bdt', '', '', '', 'tan@zil.com', '', '', '', '', '', '827ccb0eea8a706c4c34a16891f84e7b', '827ccb0eea8a706c4c34a16891f84e7b', 1),
(23, 'tuhhin', 'Mia', 'sdfasdf', '', '', '', 'tuhhin@gmail.com', '', '', '', '', '', 'e10adc3949ba59abbe56e057f20f883e', 'e10adc3949ba59abbe56e057f20f883e', 1),
(24, 'kkk', '', 'sdfasdf', '', '', '', 'kkkk@gmail.com', '', '', '', '', '', '827ccb0eea8a706c4c34a16891f84e7b', '827ccb0eea8a706c4c34a16891f84e7b', 1),
(25, 'ooo', '', 'sadfasdf', '', '', '', 'oooo@gmail.com', '', '', '', '', '', '827ccb0eea8a706c4c34a16891f84e7b', '827ccb0eea8a706c4c34a16891f84e7b', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE IF NOT EXISTS `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_module_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create` tinyint(1) DEFAULT NULL,
  `read` tinyint(1) DEFAULT NULL,
  `update` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_module_id` (`fk_module_id`),
  KEY `fk_user_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4733 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES
(1052, 127, 4, 0, 0, 0, 0),
(1053, 128, 4, 0, 0, 0, 0),
(1054, 129, 4, 0, 1, 0, 0),
(1055, 130, 4, 0, 1, 0, 0),
(1056, 3, 4, 0, 0, 0, 0),
(1057, 4, 4, 0, 0, 0, 0),
(1058, 121, 4, 0, 0, 0, 0),
(1059, 122, 4, 0, 0, 0, 0),
(1060, 6, 4, 0, 0, 0, 0),
(1061, 7, 4, 0, 0, 0, 0),
(1062, 8, 4, 0, 0, 0, 0),
(1063, 9, 4, 0, 0, 0, 0),
(1064, 10, 4, 0, 0, 0, 0),
(1065, 11, 4, 0, 0, 0, 0),
(1066, 12, 4, 0, 0, 0, 0),
(1067, 13, 4, 0, 0, 0, 0),
(1068, 126, 4, 0, 0, 0, 0),
(1069, 14, 4, 0, 0, 0, 0),
(1070, 15, 4, 0, 0, 0, 0),
(1071, 16, 4, 0, 0, 0, 0),
(1072, 17, 4, 0, 0, 0, 0),
(1073, 18, 4, 0, 0, 0, 0),
(1074, 19, 4, 0, 0, 0, 0),
(1075, 96, 4, 0, 0, 0, 0),
(1076, 97, 4, 0, 0, 0, 0),
(1077, 20, 4, 0, 0, 0, 0),
(1078, 21, 4, 0, 0, 0, 0),
(1079, 22, 4, 0, 0, 0, 0),
(1080, 95, 4, 0, 0, 0, 0),
(1081, 23, 4, 0, 0, 0, 0),
(1082, 24, 4, 0, 0, 0, 0),
(1083, 25, 4, 0, 0, 0, 0),
(1084, 26, 4, 0, 0, 0, 0),
(1085, 27, 4, 0, 0, 0, 0),
(1086, 28, 4, 0, 0, 0, 0),
(1087, 29, 4, 0, 0, 0, 0),
(1088, 30, 4, 0, 0, 0, 0),
(1089, 31, 4, 0, 0, 0, 0),
(1090, 123, 4, 0, 0, 0, 0),
(1091, 124, 4, 0, 0, 0, 0),
(1092, 125, 4, 0, 0, 0, 0),
(1093, 32, 4, 0, 0, 0, 0),
(1094, 33, 4, 0, 0, 0, 0),
(1095, 34, 4, 0, 0, 0, 0),
(1096, 35, 4, 0, 0, 0, 0),
(1097, 36, 4, 0, 0, 0, 0),
(1098, 37, 4, 0, 0, 0, 0),
(1099, 38, 4, 0, 0, 0, 0),
(1100, 39, 4, 0, 0, 0, 0),
(1101, 40, 4, 0, 0, 0, 0),
(1102, 41, 4, 0, 0, 0, 0),
(1103, 42, 4, 0, 0, 0, 0),
(1104, 43, 4, 0, 0, 0, 0),
(1105, 44, 4, 0, 0, 0, 0),
(1106, 45, 4, 0, 0, 0, 0),
(1107, 46, 4, 0, 0, 0, 0),
(1108, 47, 4, 0, 0, 0, 0),
(1109, 54, 4, 0, 0, 0, 0),
(1110, 55, 4, 0, 0, 0, 0),
(1111, 56, 4, 0, 0, 0, 0),
(1112, 57, 4, 0, 0, 0, 0),
(1113, 58, 4, 0, 0, 0, 0),
(1114, 59, 4, 0, 0, 0, 0),
(1115, 60, 4, 0, 1, 0, 0),
(1116, 61, 4, 0, 0, 0, 0),
(1117, 62, 4, 0, 0, 0, 0),
(1118, 63, 4, 0, 0, 0, 0),
(1119, 64, 4, 0, 0, 0, 0),
(1120, 65, 4, 0, 0, 0, 0),
(1121, 66, 4, 0, 0, 0, 0),
(1122, 67, 4, 0, 0, 0, 0),
(1123, 68, 4, 0, 0, 0, 0),
(1124, 69, 4, 0, 0, 0, 0),
(1125, 70, 4, 0, 0, 0, 0),
(1126, 75, 4, 0, 0, 0, 0),
(1127, 76, 4, 0, 0, 0, 0),
(1128, 77, 4, 0, 0, 0, 0),
(1129, 98, 4, 0, 0, 0, 0),
(1130, 103, 4, 0, 0, 0, 0),
(1131, 104, 4, 0, 0, 0, 0),
(1132, 78, 4, 0, 0, 0, 0),
(1133, 79, 4, 0, 0, 0, 0),
(1134, 80, 4, 0, 0, 0, 0),
(1135, 81, 4, 0, 0, 0, 0),
(1136, 82, 4, 0, 0, 0, 0),
(1137, 83, 4, 0, 0, 0, 0),
(1138, 84, 4, 0, 0, 0, 0),
(1139, 85, 4, 1, 1, 1, 0),
(1140, 86, 4, 1, 1, 1, 0),
(1141, 87, 4, 1, 1, 1, 0),
(1142, 88, 4, 0, 0, 0, 0),
(1143, 89, 4, 0, 0, 0, 0),
(1144, 90, 4, 0, 0, 0, 0),
(1145, 91, 4, 0, 0, 0, 0),
(1146, 92, 4, 0, 0, 0, 0),
(1147, 93, 4, 0, 0, 0, 0),
(1148, 94, 4, 0, 0, 0, 0),
(1149, 111, 4, 0, 0, 0, 0),
(1150, 112, 4, 0, 0, 0, 0),
(1151, 113, 4, 0, 0, 0, 0),
(1152, 114, 4, 0, 0, 0, 0),
(1153, 115, 4, 0, 0, 0, 0),
(1154, 116, 4, 0, 0, 0, 0),
(1155, 117, 4, 0, 0, 0, 0),
(1156, 118, 4, 0, 0, 0, 0),
(1157, 119, 4, 0, 0, 0, 0),
(1158, 120, 4, 0, 0, 0, 0),
(1159, 99, 4, 0, 0, 0, 0),
(1160, 101, 4, 0, 0, 0, 0),
(1161, 102, 4, 0, 0, 0, 0),
(1162, 105, 4, 0, 0, 0, 0),
(1163, 106, 4, 0, 0, 0, 0),
(1164, 107, 4, 0, 0, 0, 0),
(1165, 108, 4, 0, 0, 0, 0),
(1166, 109, 4, 0, 0, 0, 0),
(1167, 110, 4, 0, 0, 0, 0),
(1168, 71, 4, 0, 0, 0, 0),
(1169, 72, 4, 0, 0, 0, 0),
(1170, 73, 4, 0, 0, 0, 0),
(1171, 74, 4, 0, 0, 0, 0),
(1772, 127, 6, 0, 0, 0, 0),
(1773, 128, 6, 0, 0, 0, 0),
(1774, 129, 6, 0, 0, 0, 0),
(1775, 130, 6, 0, 0, 0, 0),
(1776, 3, 6, 0, 0, 0, 0),
(1777, 4, 6, 0, 0, 0, 0),
(1778, 121, 6, 0, 0, 0, 0),
(1779, 122, 6, 0, 0, 0, 0),
(1780, 6, 6, 0, 0, 0, 0),
(1781, 7, 6, 0, 0, 0, 0),
(1782, 8, 6, 0, 0, 0, 0),
(1783, 9, 6, 0, 0, 0, 0),
(1784, 10, 6, 0, 0, 0, 0),
(1785, 11, 6, 0, 0, 0, 0),
(1786, 12, 6, 0, 0, 0, 0),
(1787, 13, 6, 0, 0, 0, 0),
(1788, 126, 6, 0, 0, 0, 0),
(1789, 14, 6, 0, 0, 0, 0),
(1790, 15, 6, 0, 0, 0, 0),
(1791, 16, 6, 0, 0, 0, 0),
(1792, 17, 6, 0, 0, 0, 0),
(1793, 18, 6, 0, 0, 0, 0),
(1794, 19, 6, 0, 0, 0, 0),
(1795, 96, 6, 0, 0, 0, 0),
(1796, 97, 6, 0, 0, 0, 0),
(1797, 20, 6, 1, 0, 0, 0),
(1798, 21, 6, 0, 1, 0, 0),
(1799, 22, 6, 0, 1, 0, 0),
(1800, 95, 6, 1, 0, 0, 0),
(1801, 23, 6, 0, 0, 0, 0),
(1802, 24, 6, 0, 0, 0, 0),
(1803, 25, 6, 0, 0, 0, 0),
(1804, 26, 6, 0, 0, 0, 0),
(1805, 27, 6, 0, 0, 0, 0),
(1806, 28, 6, 0, 0, 0, 0),
(1807, 29, 6, 0, 0, 0, 0),
(1808, 30, 6, 0, 0, 0, 0),
(1809, 31, 6, 0, 0, 0, 0),
(1810, 123, 6, 0, 0, 0, 0),
(1811, 124, 6, 0, 0, 0, 0),
(1812, 125, 6, 0, 0, 0, 0),
(1813, 32, 6, 0, 0, 0, 0),
(1814, 33, 6, 0, 0, 0, 0),
(1815, 34, 6, 0, 0, 0, 0),
(1816, 35, 6, 0, 0, 0, 0),
(1817, 36, 6, 0, 0, 0, 0),
(1818, 37, 6, 0, 0, 0, 0),
(1819, 38, 6, 0, 0, 0, 0),
(1820, 39, 6, 0, 0, 0, 0),
(1821, 40, 6, 0, 0, 0, 0),
(1822, 41, 6, 0, 0, 0, 0),
(1823, 42, 6, 0, 0, 0, 0),
(1824, 43, 6, 0, 0, 0, 0),
(1825, 44, 6, 0, 0, 0, 0),
(1826, 45, 6, 0, 0, 0, 0),
(1827, 46, 6, 0, 0, 0, 0),
(1828, 47, 6, 0, 0, 0, 0),
(1829, 54, 6, 0, 0, 0, 0),
(1830, 55, 6, 0, 0, 0, 0),
(1831, 56, 6, 0, 0, 0, 0),
(1832, 57, 6, 0, 0, 0, 0),
(1833, 58, 6, 0, 0, 0, 0),
(1834, 59, 6, 0, 0, 0, 0),
(1835, 60, 6, 0, 0, 0, 0),
(1836, 61, 6, 0, 0, 0, 0),
(1837, 62, 6, 0, 0, 0, 0),
(1838, 63, 6, 0, 0, 0, 0),
(1839, 64, 6, 0, 0, 0, 0),
(1840, 65, 6, 0, 0, 0, 0),
(1841, 66, 6, 0, 0, 0, 0),
(1842, 67, 6, 0, 0, 0, 0),
(1843, 68, 6, 0, 0, 0, 0),
(1844, 69, 6, 0, 0, 0, 0),
(1845, 70, 6, 0, 0, 0, 0),
(1846, 75, 6, 0, 0, 0, 0),
(1847, 76, 6, 0, 0, 0, 0),
(1848, 77, 6, 0, 0, 0, 0),
(1849, 98, 6, 0, 0, 0, 0),
(1850, 103, 6, 0, 0, 0, 0),
(1851, 104, 6, 0, 0, 0, 0),
(1852, 78, 6, 0, 0, 0, 0),
(1853, 79, 6, 0, 0, 0, 0),
(1854, 80, 6, 0, 0, 0, 0),
(1855, 81, 6, 0, 0, 0, 0),
(1856, 82, 6, 0, 0, 0, 0),
(1857, 83, 6, 0, 0, 0, 0),
(1858, 84, 6, 0, 0, 0, 0),
(1859, 85, 6, 0, 0, 0, 0),
(1860, 86, 6, 0, 0, 0, 0),
(1861, 87, 6, 0, 0, 0, 0),
(1862, 88, 6, 0, 0, 0, 0),
(1863, 89, 6, 0, 0, 0, 0),
(1864, 90, 6, 0, 0, 0, 0),
(1865, 91, 6, 0, 0, 0, 0),
(1866, 92, 6, 0, 0, 0, 0),
(1867, 93, 6, 0, 0, 0, 0),
(1868, 94, 6, 0, 0, 0, 0),
(1869, 111, 6, 0, 0, 0, 0),
(1870, 112, 6, 0, 0, 0, 0),
(1871, 113, 6, 0, 0, 0, 0),
(1872, 114, 6, 0, 0, 0, 0),
(1873, 115, 6, 0, 0, 0, 0),
(1874, 116, 6, 0, 0, 0, 0),
(1875, 117, 6, 0, 0, 0, 0),
(1876, 118, 6, 0, 0, 0, 0),
(1877, 119, 6, 0, 0, 0, 0),
(1878, 120, 6, 0, 0, 0, 0),
(1879, 99, 6, 0, 0, 0, 0),
(1880, 101, 6, 0, 0, 0, 0),
(1881, 102, 6, 0, 0, 0, 0),
(1882, 105, 6, 0, 0, 0, 0),
(1883, 106, 6, 0, 0, 0, 0),
(1884, 107, 6, 0, 0, 0, 0),
(1885, 108, 6, 0, 0, 0, 0),
(1886, 109, 6, 0, 0, 0, 0),
(1887, 110, 6, 0, 0, 0, 0),
(1888, 71, 6, 0, 0, 0, 0),
(1889, 72, 6, 0, 0, 0, 0),
(1890, 73, 6, 0, 0, 0, 0),
(1891, 74, 6, 0, 0, 0, 0),
(1892, 137, 6, 0, 0, 0, 0),
(1893, 138, 6, 0, 0, 0, 0),
(1894, 139, 6, 0, 0, 0, 0),
(1895, 140, 6, 0, 0, 0, 0),
(1896, 141, 6, 0, 0, 0, 0),
(1897, 142, 6, 0, 0, 0, 0),
(1898, 143, 6, 0, 0, 0, 0),
(1899, 144, 6, 0, 0, 0, 0),
(1900, 145, 6, 0, 0, 0, 0),
(1901, 146, 6, 0, 0, 0, 0),
(1902, 147, 6, 0, 0, 0, 0),
(1903, 133, 6, 0, 0, 0, 0),
(1904, 134, 6, 0, 0, 0, 0),
(1905, 135, 6, 0, 0, 0, 0),
(1906, 136, 6, 0, 0, 0, 0),
(1907, 131, 6, 0, 0, 0, 0),
(1908, 132, 6, 0, 0, 0, 0),
(4449, 127, 11, 0, 0, 0, 0),
(4450, 128, 11, 0, 0, 0, 0),
(4451, 129, 11, 0, 0, 0, 0),
(4452, 130, 11, 0, 0, 0, 0),
(4453, 3, 11, 0, 0, 0, 0),
(4454, 4, 11, 0, 0, 0, 0),
(4455, 121, 11, 0, 0, 0, 0),
(4456, 122, 11, 0, 0, 0, 0),
(4457, 6, 11, 0, 0, 0, 0),
(4458, 7, 11, 0, 0, 0, 0),
(4459, 8, 11, 1, 0, 0, 0),
(4460, 9, 11, 0, 1, 1, 0),
(4461, 10, 11, 1, 1, 1, 0),
(4462, 11, 11, 1, 1, 1, 0),
(4463, 12, 11, 0, 0, 0, 0),
(4464, 13, 11, 0, 0, 0, 0),
(4465, 126, 11, 0, 0, 0, 0),
(4466, 14, 11, 1, 0, 0, 0),
(4467, 15, 11, 0, 0, 0, 0),
(4468, 16, 11, 0, 0, 0, 0),
(4469, 17, 11, 0, 0, 0, 0),
(4470, 18, 11, 0, 0, 0, 0),
(4471, 19, 11, 0, 0, 0, 0),
(4472, 96, 11, 0, 0, 0, 0),
(4473, 97, 11, 0, 0, 0, 0),
(4474, 152, 11, 0, 1, 1, 1),
(4475, 20, 11, 1, 1, 1, 0),
(4476, 21, 11, 1, 1, 1, 0),
(4477, 22, 11, 0, 1, 0, 0),
(4478, 95, 11, 0, 0, 0, 0),
(4479, 23, 11, 0, 0, 0, 0),
(4480, 24, 11, 0, 0, 0, 0),
(4481, 25, 11, 0, 0, 0, 0),
(4482, 26, 11, 0, 0, 0, 0),
(4483, 27, 11, 0, 0, 0, 0),
(4484, 28, 11, 0, 0, 0, 0),
(4485, 29, 11, 0, 0, 0, 0),
(4486, 30, 11, 0, 0, 0, 0),
(4487, 31, 11, 0, 0, 0, 0),
(4488, 123, 11, 0, 0, 0, 0),
(4489, 124, 11, 0, 0, 0, 0),
(4490, 125, 11, 0, 0, 0, 0),
(4491, 32, 11, 0, 0, 0, 0),
(4492, 33, 11, 0, 0, 0, 0),
(4493, 34, 11, 0, 0, 0, 0),
(4494, 35, 11, 0, 0, 0, 0),
(4495, 36, 11, 0, 0, 0, 0),
(4496, 37, 11, 0, 0, 0, 0),
(4497, 38, 11, 0, 0, 0, 0),
(4498, 39, 11, 0, 0, 0, 0),
(4499, 40, 11, 0, 0, 0, 0),
(4500, 41, 11, 0, 0, 0, 0),
(4501, 42, 11, 0, 0, 0, 0),
(4502, 43, 11, 0, 0, 0, 0),
(4503, 44, 11, 0, 0, 0, 0),
(4504, 45, 11, 0, 0, 0, 0),
(4505, 46, 11, 0, 0, 0, 0),
(4506, 47, 11, 0, 0, 0, 0),
(4507, 54, 11, 0, 0, 0, 0),
(4508, 55, 11, 0, 0, 0, 0),
(4509, 56, 11, 0, 0, 0, 0),
(4510, 57, 11, 0, 0, 0, 0),
(4511, 58, 11, 0, 0, 0, 0),
(4512, 59, 11, 0, 0, 0, 0),
(4513, 60, 11, 0, 0, 0, 0),
(4514, 61, 11, 1, 0, 0, 0),
(4515, 62, 11, 0, 1, 0, 0),
(4516, 63, 11, 0, 0, 0, 0),
(4517, 64, 11, 0, 0, 0, 0),
(4518, 65, 11, 0, 0, 0, 0),
(4519, 66, 11, 0, 0, 0, 0),
(4520, 67, 11, 0, 0, 0, 0),
(4521, 68, 11, 0, 0, 0, 0),
(4522, 69, 11, 0, 0, 0, 0),
(4523, 70, 11, 0, 0, 0, 0),
(4524, 75, 11, 0, 0, 0, 0),
(4525, 76, 11, 0, 0, 0, 0),
(4526, 77, 11, 0, 0, 0, 0),
(4527, 98, 11, 0, 0, 0, 0),
(4528, 103, 11, 0, 0, 0, 0),
(4529, 104, 11, 0, 0, 0, 0),
(4530, 78, 11, 0, 0, 0, 0),
(4531, 79, 11, 0, 0, 0, 0),
(4532, 80, 11, 0, 0, 0, 0),
(4533, 81, 11, 0, 0, 0, 0),
(4534, 82, 11, 0, 0, 0, 0),
(4535, 83, 11, 0, 0, 0, 0),
(4536, 84, 11, 0, 0, 0, 0),
(4537, 85, 11, 0, 0, 0, 0),
(4538, 86, 11, 0, 0, 0, 0),
(4539, 87, 11, 0, 0, 0, 0),
(4540, 88, 11, 0, 0, 0, 0),
(4541, 89, 11, 0, 0, 0, 0),
(4542, 90, 11, 0, 0, 0, 0),
(4543, 91, 11, 0, 0, 0, 0),
(4544, 92, 11, 0, 0, 0, 0),
(4545, 93, 11, 0, 0, 0, 0),
(4546, 94, 11, 0, 0, 0, 0),
(4547, 111, 11, 0, 0, 0, 0),
(4548, 112, 11, 0, 0, 0, 0),
(4549, 113, 11, 0, 0, 0, 0),
(4550, 114, 11, 0, 0, 0, 0),
(4551, 115, 11, 0, 0, 0, 0),
(4552, 116, 11, 0, 0, 0, 0),
(4553, 117, 11, 0, 0, 0, 0),
(4554, 118, 11, 0, 0, 0, 0),
(4555, 119, 11, 0, 0, 0, 0),
(4556, 120, 11, 0, 0, 0, 0),
(4557, 99, 11, 0, 0, 0, 0),
(4558, 101, 11, 0, 0, 0, 0),
(4559, 102, 11, 0, 0, 0, 0),
(4560, 105, 11, 0, 0, 0, 0),
(4561, 106, 11, 0, 0, 0, 0),
(4562, 107, 11, 0, 0, 0, 0),
(4563, 108, 11, 0, 0, 0, 0),
(4564, 109, 11, 0, 0, 0, 0),
(4565, 110, 11, 0, 0, 0, 0),
(4566, 71, 11, 0, 0, 0, 0),
(4567, 72, 11, 0, 0, 0, 0),
(4568, 73, 11, 0, 0, 0, 0),
(4569, 74, 11, 0, 0, 0, 0),
(4570, 137, 11, 0, 0, 0, 0),
(4571, 138, 11, 0, 0, 0, 0),
(4572, 139, 11, 0, 0, 0, 0),
(4573, 140, 11, 0, 0, 0, 0),
(4574, 141, 11, 0, 0, 0, 0),
(4575, 142, 11, 0, 0, 0, 0),
(4576, 143, 11, 0, 0, 0, 0),
(4577, 144, 11, 0, 0, 0, 0),
(4578, 145, 11, 0, 0, 0, 0),
(4579, 146, 11, 0, 0, 0, 0),
(4580, 147, 11, 0, 0, 0, 0),
(4581, 133, 11, 0, 0, 0, 0),
(4582, 134, 11, 0, 0, 0, 0),
(4583, 135, 11, 0, 0, 0, 0),
(4584, 136, 11, 0, 0, 0, 0),
(4585, 131, 11, 0, 0, 0, 0),
(4586, 132, 11, 0, 0, 0, 0),
(4587, 148, 11, 0, 0, 0, 0),
(4588, 149, 11, 0, 0, 0, 0),
(4589, 150, 11, 0, 0, 0, 0),
(4590, 151, 11, 0, 0, 0, 0),
(4591, 127, 2, 1, 1, 1, 0),
(4592, 128, 2, 1, 1, 1, 0),
(4593, 129, 2, 1, 1, 1, 1),
(4594, 130, 2, 0, 0, 0, 0),
(4595, 3, 2, 0, 0, 0, 0),
(4596, 4, 2, 0, 0, 0, 0),
(4597, 121, 2, 0, 0, 0, 0),
(4598, 122, 2, 0, 0, 0, 0),
(4599, 6, 2, 0, 0, 0, 0),
(4600, 7, 2, 0, 0, 0, 0),
(4601, 8, 2, 1, 1, 1, 1),
(4602, 9, 2, 0, 1, 0, 0),
(4603, 10, 2, 1, 1, 1, 1),
(4604, 11, 2, 1, 1, 1, 1),
(4605, 12, 2, 1, 1, 1, 1),
(4606, 13, 2, 0, 1, 1, 1),
(4607, 126, 2, 1, 1, 1, 1),
(4608, 14, 2, 1, 1, 1, 1),
(4609, 15, 2, 1, 1, 1, 1),
(4610, 16, 2, 0, 0, 0, 0),
(4611, 17, 2, 0, 0, 0, 0),
(4612, 18, 2, 0, 0, 0, 0),
(4613, 19, 2, 0, 0, 0, 0),
(4614, 96, 2, 0, 0, 0, 0),
(4615, 97, 2, 0, 0, 0, 0),
(4616, 152, 2, 1, 1, 1, 1),
(4617, 20, 2, 0, 0, 0, 0),
(4618, 21, 2, 0, 0, 0, 0),
(4619, 22, 2, 0, 0, 0, 0),
(4620, 95, 2, 0, 0, 0, 0),
(4621, 23, 2, 0, 0, 0, 0),
(4622, 24, 2, 0, 0, 0, 0),
(4623, 25, 2, 0, 0, 0, 0),
(4624, 26, 2, 0, 0, 0, 0),
(4625, 27, 2, 0, 0, 0, 0),
(4626, 28, 2, 0, 0, 0, 0),
(4627, 29, 2, 0, 0, 0, 0),
(4628, 30, 2, 0, 0, 0, 0),
(4629, 31, 2, 0, 0, 0, 0),
(4630, 123, 2, 0, 0, 0, 0),
(4631, 124, 2, 0, 0, 0, 0),
(4632, 125, 2, 0, 0, 0, 0),
(4633, 32, 2, 0, 0, 0, 0),
(4634, 33, 2, 0, 0, 0, 0),
(4635, 34, 2, 0, 0, 0, 0),
(4636, 35, 2, 0, 0, 0, 0),
(4637, 36, 2, 0, 0, 0, 0),
(4638, 37, 2, 0, 0, 0, 0),
(4639, 38, 2, 0, 0, 0, 0),
(4640, 39, 2, 0, 0, 0, 0),
(4641, 40, 2, 0, 0, 0, 0),
(4642, 41, 2, 0, 0, 0, 0),
(4643, 42, 2, 0, 0, 0, 0),
(4644, 43, 2, 0, 0, 0, 0),
(4645, 44, 2, 0, 0, 0, 0),
(4646, 45, 2, 0, 0, 0, 0),
(4647, 46, 2, 0, 0, 0, 0),
(4648, 47, 2, 0, 0, 0, 0),
(4649, 54, 2, 0, 0, 0, 0),
(4650, 55, 2, 0, 0, 0, 0),
(4651, 56, 2, 0, 0, 0, 0),
(4652, 57, 2, 0, 0, 0, 0),
(4653, 58, 2, 0, 0, 0, 0),
(4654, 59, 2, 0, 1, 0, 0),
(4655, 60, 2, 0, 1, 0, 0),
(4656, 61, 2, 0, 0, 0, 0),
(4657, 62, 2, 0, 0, 0, 0),
(4658, 63, 2, 0, 0, 0, 0),
(4659, 64, 2, 0, 0, 0, 0),
(4660, 65, 2, 0, 0, 0, 0),
(4661, 66, 2, 0, 0, 0, 0),
(4662, 67, 2, 0, 0, 0, 0),
(4663, 68, 2, 0, 0, 0, 0),
(4664, 69, 2, 0, 0, 0, 0),
(4665, 70, 2, 0, 0, 0, 0),
(4666, 75, 2, 0, 0, 0, 0),
(4667, 76, 2, 0, 0, 0, 0),
(4668, 77, 2, 0, 0, 0, 0),
(4669, 98, 2, 0, 0, 0, 0),
(4670, 103, 2, 0, 0, 0, 0),
(4671, 104, 2, 0, 0, 0, 0),
(4672, 78, 2, 0, 0, 0, 0),
(4673, 79, 2, 0, 0, 0, 0),
(4674, 80, 2, 0, 0, 0, 0),
(4675, 81, 2, 0, 0, 0, 0),
(4676, 82, 2, 0, 0, 0, 0),
(4677, 83, 2, 0, 0, 0, 0),
(4678, 84, 2, 0, 0, 0, 0),
(4679, 85, 2, 0, 0, 0, 0),
(4680, 86, 2, 0, 0, 0, 0),
(4681, 87, 2, 0, 0, 0, 0),
(4682, 88, 2, 0, 0, 0, 0),
(4683, 89, 2, 0, 0, 0, 0),
(4684, 90, 2, 0, 0, 0, 0),
(4685, 91, 2, 0, 0, 0, 0),
(4686, 92, 2, 0, 0, 0, 0),
(4687, 93, 2, 0, 0, 0, 0),
(4688, 94, 2, 0, 0, 0, 0),
(4689, 111, 2, 0, 0, 0, 0),
(4690, 112, 2, 0, 0, 0, 0),
(4691, 113, 2, 0, 0, 0, 0),
(4692, 114, 2, 0, 0, 0, 0),
(4693, 115, 2, 0, 0, 0, 0),
(4694, 116, 2, 0, 0, 0, 0),
(4695, 117, 2, 0, 0, 0, 0),
(4696, 118, 2, 0, 0, 0, 0),
(4697, 119, 2, 0, 0, 0, 0),
(4698, 120, 2, 0, 0, 0, 0),
(4699, 99, 2, 0, 0, 0, 0),
(4700, 101, 2, 0, 0, 0, 0),
(4701, 102, 2, 0, 0, 0, 0),
(4702, 105, 2, 0, 0, 0, 0),
(4703, 106, 2, 0, 0, 0, 0),
(4704, 107, 2, 0, 0, 0, 0),
(4705, 108, 2, 0, 0, 0, 0),
(4706, 109, 2, 0, 0, 0, 0),
(4707, 110, 2, 0, 0, 0, 0),
(4708, 71, 2, 0, 0, 0, 0),
(4709, 72, 2, 0, 0, 0, 0),
(4710, 73, 2, 0, 0, 0, 0),
(4711, 74, 2, 0, 0, 0, 0),
(4712, 137, 2, 0, 0, 0, 0),
(4713, 138, 2, 0, 0, 0, 0),
(4714, 139, 2, 0, 0, 0, 0),
(4715, 140, 2, 0, 0, 0, 0),
(4716, 141, 2, 0, 0, 0, 0),
(4717, 142, 2, 0, 0, 0, 0),
(4718, 143, 2, 0, 0, 0, 0),
(4719, 144, 2, 0, 0, 0, 0),
(4720, 145, 2, 0, 0, 0, 0),
(4721, 146, 2, 0, 0, 0, 0),
(4722, 147, 2, 0, 0, 0, 0),
(4723, 133, 2, 0, 0, 0, 0),
(4724, 134, 2, 0, 0, 0, 0),
(4725, 135, 2, 0, 0, 0, 0),
(4726, 136, 2, 0, 0, 0, 0),
(4727, 131, 2, 0, 0, 0, 0),
(4728, 132, 2, 0, 0, 0, 0),
(4729, 148, 2, 0, 0, 0, 0),
(4730, 149, 2, 0, 0, 0, 0),
(4731, 150, 2, 0, 0, 0, 0),
(4732, 151, 2, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE IF NOT EXISTS `schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `slot_id` int(11) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `available_days` varchar(50) DEFAULT NULL,
  `per_patient_time` time DEFAULT NULL,
  `serial_visibility_type` tinyint(4) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`schedule_id`, `slot_id`, `doctor_id`, `start_time`, `end_time`, `available_days`, `per_patient_time`, `serial_visibility_type`, `branch_id`, `status`) VALUES
(1, 1, 39, '07:00:00', '10:00:00', 'Sunday', '00:20:00', 2, 0, 1),
(2, 1, 70, '05:00:00', '11:00:00', 'Sunday', '00:30:00', 2, 0, 1),
(3, 2, 70, '09:00:00', '14:00:00', 'Saturday', '00:30:00', 1, 0, 1),
(4, 1, 42, '10:00:00', '13:00:00', 'Friday', '00:30:00', 1, 0, 1),
(5, 1, 50, '07:00:00', '12:00:00', 'Monday', '00:25:00', 2, 0, 1),
(6, 2, 38, '10:00:00', '17:00:00', 'Thursday', '00:45:00', 1, 0, 1),
(7, 1, 50, '12:00:00', '19:00:00', 'Friday', '00:25:00', 2, 0, 1),
(8, 1, 40, '09:00:00', '12:00:00', 'Tuesday', '00:30:00', 2, 0, 1),
(9, 1, 70, '10:00:00', '13:00:00', 'Tuesday', '00:25:00', 1, 0, 1),
(10, 1, 39, '09:00:00', '11:00:00', 'Friday', '00:30:00', 2, 2, 1),
(11, 1, 39, '09:35:30', '14:35:30', 'Sunday', '01:05:00', 1, 1, 1),
(12, 1, 38, '05:25:30', '07:30:30', 'Sunday', '00:30:00', 1, 1, 1),
(13, 1, 38, '05:25:30', '07:30:30', 'Tuesday', '00:30:00', 1, 1, 1),
(14, 1, 40, '05:25:30', '08:30:30', 'Wednesday', '00:40:00', 1, 1, 1),
(15, 3, 77, '08:30:00', '18:30:00', 'Sunday', '00:30:00', 1, 2, 1),
(16, 3, 77, '08:30:00', '18:30:00', 'Monday', '00:30:00', 1, 2, 1),
(17, 3, 77, '08:30:00', '18:30:00', 'Wednesday', '00:30:00', 1, 2, 1),
(18, 2, 77, '08:30:00', '12:30:00', 'Thursday', '00:30:00', 2, 2, 1),
(19, 3, 39, '09:00:00', '18:00:00', 'Monday', '00:30:00', 2, 1, 1),
(20, 3, 39, '09:00:00', '18:00:00', 'Tuesday', '00:30:00', 2, 1, 1),
(21, 3, 39, '09:00:00', '18:00:00', 'Wednesday', '00:30:00', 2, 1, 1),
(22, 3, 39, '09:00:00', '18:00:00', 'Thursday', '00:30:00', 2, 1, 1),
(23, 3, 39, '09:00:00', '18:00:00', 'Friday', '00:30:00', 2, 1, 1),
(24, 1, 39, '09:00:00', '00:00:00', 'Monday', '00:30:00', 2, 3, 1),
(25, 1, 39, '09:00:00', '14:30:00', 'Tuesday', '00:30:00', 2, 3, 1),
(26, 1, 81, '09:00:00', '18:00:00', 'Monday', '00:30:00', 2, 3, 1),
(30, 1, 81, '09:00:00', '05:00:00', 'Monday', '00:30:00', 2, 3, 1),
(31, 1, 81, '08:00:00', '06:00:00', 'Monday', '00:20:00', 1, 3, 1),
(32, 1, 81, '08:00:00', '06:00:00', 'Tuesday', '00:20:00', 1, 3, 1),
(33, 1, 81, '08:00:00', '06:00:00', 'Wednesday', '00:20:00', 1, 3, 1),
(34, 1, 81, '08:00:00', '06:00:00', 'Thursday', '00:20:00', 1, 3, 1),
(35, 1, 81, '08:00:00', '06:00:00', 'Friday', '00:20:00', 1, 3, 1),
(36, 1, 81, '08:00:00', '06:00:00', 'Saturday', '00:20:00', 1, 3, 1),
(37, 6, 97, '08:00:00', '10:00:00', 'Sunday', '00:20:00', 2, 3, 1),
(38, 6, 97, '08:00:00', '10:00:00', 'Monday', '00:20:00', 2, 3, 1),
(39, 6, 97, '08:00:00', '10:00:00', 'Tuesday', '00:20:00', 2, 3, 1),
(40, 6, 97, '08:00:00', '10:00:00', 'Wednesday', '00:20:00', 2, 3, 1),
(41, 6, 97, '08:00:00', '10:00:00', 'Thursday', '00:20:00', 2, 3, 1),
(42, 6, 97, '08:00:00', '10:00:00', 'Friday', '00:20:00', 2, 3, 1),
(43, 7, 97, '08:00:00', '15:00:00', 'Sunday', '00:25:00', 2, 3, 1),
(44, 7, 97, '08:00:00', '15:00:00', 'Monday', '00:25:00', 2, 3, 1),
(45, 7, 97, '08:00:00', '15:00:00', 'Tuesday', '00:25:00', 2, 3, 1),
(46, 7, 97, '08:00:00', '15:00:00', 'Wednesday', '00:25:00', 2, 3, 1),
(47, 2, 97, '12:00:00', '15:00:00', 'Monday', '00:20:00', 2, 5, 1),
(48, 2, 97, '12:00:00', '15:00:00', 'Tuesday', '00:20:00', 2, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sec_role`
--

CREATE TABLE IF NOT EXISTS `sec_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sec_role`
--

INSERT INTO `sec_role` (`id`, `type`) VALUES
(1, 'Admin'),
(2, 'Doctor'),
(3, 'Accountant'),
(4, 'Laboratorist'),
(5, 'Nurse'),
(6, 'Pharmacist'),
(7, 'Receptionist'),
(8, 'Representative'),
(9, 'Case Manager'),
(10, 'Duty Office'),
(11, 'Referrer'),
(12, 'OutBound Referrers');

-- --------------------------------------------------------

--
-- Table structure for table `sec_userrole`
--

CREATE TABLE IF NOT EXISTS `sec_userrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `createby` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `createdate` datetime NOT NULL,
  UNIQUE KEY `ID` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sec_userrole`
--

INSERT INTO `sec_userrole` (`id`, `user_id`, `roleid`, `createby`, `createdate`) VALUES
(2, '41', 3, '2', '2018-08-29 11:31:07'),
(3, '37', 4, '2', '2018-08-29 11:33:18'),
(4, '29', 5, '2', '2018-08-29 11:36:32'),
(5, '12', 6, '2', '2018-08-29 11:39:33'),
(6, '7', 7, '2', '2018-08-29 11:42:13'),
(7, '8', 8, '2', '2018-08-29 11:44:26'),
(8, '30', 9, '2', '2018-08-29 11:46:29'),
(9, '19', 9, '2', '2018-11-05 09:10:33'),
(10, '12', 4, '2', '2018-11-07 08:22:10'),
(12, '44', 5, '2', '2018-11-15 00:00:00'),
(13, '47', 10, '2', '2018-12-03 07:43:01'),
(14, '46', 1, '2', '2018-12-08 12:04:51'),
(16, '43', 3, '2', '2018-12-09 10:17:20'),
(17, '43', 10, '2', '2018-12-09 10:19:56'),
(18, '2', 4, '2', '2018-12-10 05:25:03'),
(20, '48', 5, '2', '2018-12-10 06:13:47'),
(21, '39', 2, '2', '2018-12-19 02:01:13'),
(22, '51', 1, '2', '2018-12-20 00:00:00'),
(23, '51', 2, '2', '2019-01-07 00:00:00'),
(24, '52', 1, '2', '2019-01-07 00:00:00'),
(25, '53', 10, '2', '2019-01-07 00:00:00'),
(26, '54', 5, '2', '2019-01-07 00:00:00'),
(27, '55', 2, '2', '2019-01-07 00:00:00'),
(28, '56', 5, '2', '2019-01-07 00:00:00'),
(29, '57', 2, '2', '2019-01-07 00:00:00'),
(30, '58', 2, '2', '2019-01-07 00:00:00'),
(31, '59', 2, '2', '2019-01-07 00:00:00'),
(32, '60', 5, '2', '2019-01-07 00:00:00'),
(33, '61', 10, '2', '2019-01-07 00:00:00'),
(34, '62', 1, '2', '2019-01-07 00:00:00'),
(35, '63', 2, '2', '2019-01-07 00:00:00'),
(36, '64', 6, '2', '2019-01-07 00:00:00'),
(37, '65', 2, '2', '2019-01-07 00:00:00'),
(38, '66', 6, '2', '2019-01-07 00:00:00'),
(39, '67', 9, '2', '2019-01-07 00:00:00'),
(40, '42', 2, '2', '2019-01-17 12:33:24'),
(41, '70', 2, '2', '2019-01-19 10:57:31'),
(42, '71', 1, '2', '2019-01-21 00:00:00'),
(43, '72', 3, '71', '2019-01-21 00:00:00'),
(44, '38', 2, '2', '2019-01-26 10:56:45'),
(45, '50', 2, '2', '2019-01-27 04:33:18'),
(46, '40', 2, '2', '2019-01-27 04:33:46'),
(47, '74', 5, '2', '2019-08-22 00:00:00'),
(48, '76', 11, '2', '2019-08-31 00:00:00'),
(49, '78', 1, '2', '2019-09-07 00:00:00'),
(50, '8', 11, '2', '2019-09-09 00:00:00'),
(51, '9', 11, '2', '2019-09-09 00:00:00'),
(52, '10', 11, '2', '2019-09-09 00:00:00'),
(53, '11', 11, '2', '2019-09-09 00:00:00'),
(54, '80', 11, '2', '2019-09-11 00:00:00'),
(55, '82', 11, '2', '2019-09-11 00:00:00'),
(56, '81', 2, '2', '2019-09-11 09:20:07'),
(57, '83', 2, '2', '2019-09-12 08:43:33'),
(58, '84', 11, '2', '2019-09-12 00:00:00'),
(59, '85', 11, '2', '2019-09-12 00:00:00'),
(60, '86', 11, '2', '2019-09-15 00:00:00'),
(61, '87', 11, '2', '2019-09-15 00:00:00'),
(62, '88', 11, '2', '2019-09-16 00:00:00'),
(63, '89', 11, '2', '2019-09-16 00:00:00'),
(64, '90', 11, '2', '2019-09-16 00:00:00'),
(65, '91', 11, '2', '2019-09-16 00:00:00'),
(66, '92', 11, '2', '2019-09-16 00:00:00'),
(67, '93', 11, '2', '2019-09-16 00:00:00'),
(68, '94', 11, '2', '2019-09-16 00:00:00'),
(69, '95', 11, '2', '2019-09-16 00:00:00'),
(70, '96', 11, '2', '2019-09-16 00:00:00'),
(71, '97', 2, '2', '2019-09-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `site_align` varchar(50) DEFAULT NULL,
  `footer_text` varchar(255) DEFAULT NULL,
  `time_zone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`setting_id`, `title`, `description`, `email`, `phone`, `logo`, `favicon`, `language`, `site_align`, `footer_text`, `time_zone`) VALUES
(2, 'Demo Hospital Limited', 'Mannan Plaza, 4th Floor, Khilkhet Dhaka-1229, Bangladesh', 'bdtask@gmail.com', '1922296392', 'assets/images/apps/198a67e91b5321eb77cad21692dbd179.png', 'assets/images/icons/6a735b1e711e6bd8cd016d6845cad6c8.png', 'english', 'LTR', '2017Â©Copyright', 'Asia/Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `sms_delivery`
--

CREATE TABLE IF NOT EXISTS `sms_delivery` (
  `sms_delivery_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_id` int(11) NOT NULL,
  `delivery_date_time` datetime NOT NULL,
  `sms_info_id` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`sms_delivery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sms_gateway`
--

CREATE TABLE IF NOT EXISTS `sms_gateway` (
  `gateway_id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_name` text NOT NULL,
  `user` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `authentication` text NOT NULL,
  `link` text NOT NULL,
  `default_status` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`gateway_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_gateway`
--

INSERT INTO `sms_gateway` (`gateway_id`, `provider_name`, `user`, `password`, `authentication`, `link`, `default_status`, `status`) VALUES
(1, 'nexmo', '1d286ff1', '11a8b67955d4482f', 'Hospital', 'https://www.nexmo.com/', 0, 1),
(2, 'clickatell', 'clickatell', '9d2e2d3aa558ddcb', 'Hospital', 'https://www.clickatell.com/', 0, 1),
(3, 'bdtask', 'C20029865c42c504afc711.77492546', '161QLtkk1I', '8801847169884', 'ms.bdtask.com', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sms_info`
--

CREATE TABLE IF NOT EXISTS `sms_info` (
  `sms_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `appointment_id` varchar(30) NOT NULL,
  `appointment_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `sms_counter` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`sms_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_info`
--

INSERT INTO `sms_info` (`sms_info_id`, `doctor_id`, `patient_id`, `phone_no`, `appointment_id`, `appointment_date`, `status`, `sms_counter`) VALUES
(1, 70, 'PNW3SBSX', '017346743568', 'A4B8MAEU', '2019-01-20 00:00:00', 0, 0),
(2, 70, 'PUXULY35', '7567', 'AS842LSV', '2019-01-20 00:00:00', 0, 0),
(3, 70, 'P5J5PQIL', '017346743568', 'ASQMJOLK', '2019-01-06 00:00:00', 0, 0),
(4, 70, 'P7MFNN2D', '435345', 'AB6JKNLP', '2019-01-06 00:00:00', 0, 0),
(5, 39, 'P0QTQ0LG', '01725436866', 'AZN2R8HK', '2019-01-06 00:00:00', 0, 0),
(6, 38, 'PZRIMPJ4', '87443226543', 'ADGFDP38', '2019-01-03 00:00:00', 0, 0),
(7, 38, 'PRQXXD9Q', '98763453243', 'AVWTBVQW', '2019-01-03 00:00:00', 0, 0),
(8, 40, 'P1WY4YCU', '809567897654', 'AB5A1607', '2019-01-01 00:00:00', 0, 0),
(9, 50, 'P45E9QXP', '09876543456', 'AR0BPBC9', '2019-01-04 00:00:00', 0, 0),
(10, 50, 'PMTGGP7A', '012485673768', 'A4ZQ1IVP', '2019-01-07 00:00:00', 0, 0),
(11, 39, 'P86DHOFT', '28789784534', 'ANMZOY5D', '2019-01-06 00:00:00', 0, 0),
(12, 40, 'P6J76U0K', '90876543457', 'AEP0ASA0', '2019-01-01 00:00:00', 0, 0),
(13, 42, 'PEPWILVC', '486889435', 'ALELQRTQ', '2019-01-04 00:00:00', 0, 0),
(14, 42, 'PJ916RXG', '8743232556', 'AKW65ZSK', '2019-01-04 00:00:00', 0, 0),
(15, 39, 'PV5KRZJ1', '0557348785', 'AFZ28KX1', '2019-01-06 00:00:00', 0, 0),
(16, 50, 'PK5L3G3G', '0546675456', 'AF9H71XW', '2019-01-07 00:00:00', 0, 0),
(17, 42, 'P6WFT0SK', '04378538765', 'ASA9APQ6', '2019-01-04 00:00:00', 0, 0),
(18, 70, 'P0IK5GYT', '987654568', 'ATPQI5A4', '2019-01-01 00:00:00', 0, 0),
(19, 70, 'P4MHVMZT', '98765434567', 'AXFCTU8A', '2019-01-05 00:00:00', 0, 0),
(20, 40, 'P5J949EY', '09876543234', 'AT9AIOHO', '2019-01-01 00:00:00', 0, 0),
(21, 77, 'P8OMUDK3', '01723833869', 'AQPSN8EZ', '2019-09-12 00:00:00', 0, 0),
(22, 77, 'P8OMUDK3', '01723833869', 'AUTOQB9L', '2019-09-26 00:00:00', 0, 0),
(23, 77, 'PCQ2JONU', '01852376598', 'AIURWMKN', '2019-09-19 00:00:00', 0, 0),
(24, 77, 'PCQ2JONU', '01852376598', 'AY5AKLZ1', '2019-09-12 00:00:00', 0, 0),
(25, 77, 'PAT1LAGN', '01955110016', 'AI1EJ8QO', '2019-09-12 00:00:00', 0, 0),
(26, 39, 'P8ZCQMXI', '07484684826', 'AIU2A2AS', '2019-09-10 00:00:00', 0, 0),
(27, 39, 'PECW0N53', '1751194212', 'AJ3LBC47', '2019-09-08 00:00:00', 0, 0),
(28, 81, 'PI1XJ3FW', '3432432423', 'ASMH37XM', '2019-09-16 00:00:00', 0, 0),
(29, 81, 'PPS3AFJB', '324343', 'AK2JWVFO', '2019-09-16 00:00:00', 0, 0),
(30, 81, 'PXL3B2DJ', '23423432', 'A0YQ1WI1', '2019-09-16 00:00:00', 0, 0),
(31, 83, 'PSKNIKJ2', '32452345', 'ABOF92LI', '2019-09-12 00:00:00', 0, 0),
(32, 81, 'PFR7U0FN', '01876565432', 'AYQVVKD1', '2019-09-16 00:00:00', 0, 0),
(33, 81, 'PG0VW841', '324324324', 'AZVQSVW3', '2019-09-16 00:00:00', 0, 0),
(34, 39, 'PZ1A14D7', '01857675727', 'AB8DPNM8', '2019-09-24 00:00:00', 0, 0),
(35, 97, 'PI27P99N', '7484684826', 'ANG42GQO', '2019-09-23 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sms_schedule`
--

CREATE TABLE IF NOT EXISTS `sms_schedule` (
  `ss_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_teamplate_id` int(11) NOT NULL,
  `ss_name` text NOT NULL,
  `ss_schedule` varchar(100) NOT NULL,
  `ss_status` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ss_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_schedule`
--

INSERT INTO `sms_schedule` (`ss_id`, `ss_teamplate_id`, `ss_name`, `ss_schedule`, `ss_status`) VALUES
(1, 2, 'One', '1:1:1', 1),
(2, 9, 'Summer offer', '10:3:0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms_setting`
--

CREATE TABLE IF NOT EXISTS `sms_setting` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `appointment` tinyint(1) DEFAULT NULL,
  `registration` tinyint(1) DEFAULT NULL,
  `schedule` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_setting`
--

INSERT INTO `sms_setting` (`id`, `appointment`, `registration`, `schedule`) VALUES
(2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms_teamplate`
--

CREATE TABLE IF NOT EXISTS `sms_teamplate` (
  `teamplate_id` int(11) NOT NULL AUTO_INCREMENT,
  `teamplate_name` text DEFAULT NULL,
  `teamplate` text DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `default_status` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`teamplate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_teamplate`
--

INSERT INTO `sms_teamplate` (`teamplate_id`, `teamplate_name`, `teamplate`, `type`, `status`, `default_status`) VALUES
(1, 'Appointment Template', 'Doctor, %doctor_name%. \r\nHello, %patient_name%. \r\nYour ID: %patient_id%, Appointment ID: %appointment_id%, Serial: %sequence% and Appointment Date: %appointment_date%. \r\nThank you for the Appointment.', 'Appointment', 1, 1),
(2, 'Schedule', 'Doctor, %doctor_name%. \r\nHello, %patient_name%. \r\nYour ID: %patient_id%, Appointment ID: %appointment_id%, Serial: %sequence% and Appointment Date: %appointment_date%. \r\nThank you for the Appointment.', 'Schedule', 1, 1),
(3, 'Registration', 'Hello, %patient_name%. \r\nYour ID: %patient_id%,  \r\nThank you for the registration.', 'Registration', 1, 1),
(4, 'Summer Offer', 'Hello, %patient_name%. Your ID: %patient_id%,\r\nYour promo code is 1010101.\r\nContact with us.\r\nThanks', 'Schedule', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sms_users`
--

CREATE TABLE IF NOT EXISTS `sms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `receiver` varchar(20) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `date` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sub_module`
--

CREATE TABLE IF NOT EXISTS `sub_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `directory` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_module`
--

INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES
(3, 3, 'Add Department', 'Add Department', 'application/modules/store/assets/images/thumbnail.jpg', 'add_department', 1),
(4, 3, 'Department List', 'Department List', 'application/modules/store/assets/images/thumbnail.jpg', 'department_list', 1),
(6, 4, 'Add Doctor', 'Add Doctor', 'application/modules/store/assets/images/thumbnail.jpg', 'add_doctor', 1),
(7, 4, 'Doctor List', 'Doctor List', 'application/modules/store/assets/images/thumbnail.jpg', 'doctor_list', 1),
(8, 5, 'Add Patient', 'Add Patient', 'application/modules/store/assets/images/thumbnail.jpg', 'add_patient', 1),
(9, 5, 'Patient List', 'Patient List', 'application/modules/store/assets/images/thumbnail.jpg', 'patient_list', 1),
(10, 5, 'Add Document', 'Add Document', 'application/modules/store/assets/images/thumbnail.jpg', 'add_document', 1),
(11, 5, 'Document List', 'Document List', 'application/modules/store/assets/images/thumbnail.jpg', 'document_list', 1),
(12, 6, 'Add Schedule', 'Manage Customer', 'application/modules/store/assets/images/thumbnail.jpg', 'add_schedule', 1),
(13, 6, 'Schedule List', 'Credit Customer', 'application/modules/store/assets/images/thumbnail.jpg', 'schedule_list', 1),
(14, 7, 'Add Appointment', 'Add Appointment', 'application/modules/store/assets/images/thumbnail.jpg', 'add_appointment', 1),
(15, 7, 'Appointment List', 'Appointment List', 'application/modules/store/assets/images/thumbnail.jpg', 'appointment_list', 1),
(16, 7, 'Assign By All', 'Assign By All', 'application/modules/store/assets/images/thumbnail.jpg', 'assign_by_all', 1),
(17, 7, 'Assign By Doctor', 'Assign By Doctor', 'application/modules/store/assets/images/thumbnail.jpg', 'assign_by_doctor', 1),
(18, 7, 'Assign By Representative', 'Assign By Representative', 'application/modules/store/assets/images/thumbnail.jpg', 'assign_by_representative', 1),
(19, 7, 'Assign To Doctor', 'Assign To Doctor', 'application/modules/store/assets/images/thumbnail.jpg', 'assign_to_doctor', 1),
(20, 8, 'Add Patient Case Study', 'Add Patient Case Study', 'application/modules/store/assets/images/thumbnail.jpg', 'add_patient_case_study', 1),
(21, 8, 'Patient Case Study List', 'Patient Case Study List', 'application/modules/store/assets/images/thumbnail.jpg', 'patient_case_study_list', 1),
(22, 8, 'Prescription List', '	\r\nPrescription List', 'application/modules/store/assets/images/thumbnail.jpg', 'prescription_list', 1),
(23, 9, 'Debit Voucher', 'Debit Vouche', 'application/modules/store/assets/images/thumbnail.jpg', 'debit_voucher', 1),
(24, 9, 'Account List', 'Account List', 'application/modules/store/assets/images/thumbnail.jpg', 'account_list', 1),
(25, 9, 'Credit Voucher', 'Credit Voucher', 'application/modules/store/assets/images/thumbnail.jpg', 'credit_voucher', 1),
(26, 9, 'Contra Voucher', 'Contra Voucher', 'application/modules/store/assets/images/thumbnail.jpg', 'contra_voucher', 1),
(27, 9, 'Journal Voucher', 'Journal Voucher', 'application/modules/store/assets/images/thumbnail.jpg', 'journal_voucher', 1),
(28, 9, 'Voucher Approval', 'Voucher Approval', 'application/modules/store/assets/images/thumbnail.jpg', 'voucher_approval', 1),
(29, 9, 'Account Report', 'Account Report', 'application/modules/store/assets/images/thumbnail.jpg', 'account_report', 1),
(30, 9, 'Voucher Report', 'Voucher Report', 'application/modules/store/assets/images/thumbnail.jpg', 'voucher_report', 1),
(31, 9, 'Cash Book', 'Cash Book', 'application/modules/store/assets/images/thumbnail.jpg', 'cash_book', 1),
(32, 11, 'Add Insurance', 'Add Insurance', 'application/modules/store/assets/images/thumbnail.jpg', 'add_insurance', 1),
(33, 11, 'Insurance List', 'Insurance List', 'application/modules/store/assets/images/thumbnail.jpg', 'insurance_list', 1),
(34, 11, 'Add Limit Approval', 'Add Limit Approval', 'application/modules/store/assets/images/thumbnail.jpg', 'add_limit_approval', 1),
(35, 11, 'Limit Approval List', 'Limit Approval List', 'application/modules/store/assets/images/thumbnail.jpg', 'limit_approval_list', 1),
(36, 12, 'Add Service', 'Add Service', 'application/modules/store/assets/images/thumbnail.jpg', 'add_service', 1),
(37, 12, 'Service List', 'Service List', 'application/modules/store/assets/images/thumbnail.jpg', 'service_list', 1),
(38, 12, 'Add Package', 'Add Package', 'application/modules/store/assets/images/thumbnail.jpg', 'add_package', 1),
(39, 12, 'Package List', 'Package List', 'application/modules/store/assets/images/thumbnail.jpg', 'package_list', 1),
(40, 12, 'Add Admission', 'Add Admission', 'application/modules/store/assets/images/thumbnail.jpg', 'add_admission', 1),
(41, 12, 'Admission List', 'Admission List', 'application/modules/store/assets/images/thumbnail.jpg', 'admission_list', 1),
(42, 12, 'Add Advance', 'Add Advance', 'application/modules/store/assets/images/thumbnail.jpg', 'add_advance', 1),
(43, 12, 'Advance List', 'Advance List', 'application/modules/store/assets/images/thumbnail.jpg', 'advance_list', 1),
(44, 12, 'Add Bill', 'Add Bill', 'application/modules/store/assets/images/thumbnail.jpg', 'add_bill', 1),
(45, 12, 'Bill List', 'Bill List', 'application/modules/store/assets/images/thumbnail.jpg', 'bill_list', 1),
(46, 13, 'Add Employee', 'Add Employee', 'application/modules/store/assets/images/thumbnail.jpg', 'add_employee', 1),
(47, 13, 'Employee List', 'Employee List', 'application/modules/store/assets/images/thumbnail.jpg', 'employee_list', 1),
(54, 14, 'Add Bed', 'Add Bed', 'application/modules/store/assets/images/thumbnail.jpg', 'add_bed', 1),
(55, 14, 'Bed List', 'Bed List', 'application/modules/store/assets/images/thumbnail.jpg', 'bed_list', 1),
(56, 14, 'Bed Assign', 'Bed Assign', 'application/modules/store/assets/images/thumbnail.jpg', 'bed_assign', 1),
(57, 14, 'Bed Assign List', 'Bed Assign List', 'application/modules/store/assets/images/thumbnail.jpg', 'bed_assign_list', 1),
(58, 14, 'Report', 'Report', 'application/modules/store/assets/images/thumbnail.jpg', 'bed_report', 1),
(59, 15, 'Add Notice', 'Add Notice', 'application/modules/stockmovment/assets/images/thumbnail.jpg', 'add_notice', 1),
(60, 15, 'Notice List', 'Notice List', 'application/modules/stockmovment/assets/images/thumbnail.jpg', 'notice_list', 1),
(61, 16, 'Add Patient', 'Add Patient', 'application/modules/store/assets/images/thumbnail.jpg', 'case_add_patient', 1),
(62, 16, 'Patient List', 'Patient List', 'application/modules/store/assets/images/thumbnail.jpg', 'case_patient_list', 1),
(63, 17, 'Add Birth Report', 'Add Birth Report', 'application/modules/store/assets/images/thumbnail.jpg', 'add_birth_report', 1),
(64, 17, 'Birth Report', 'Birth Report', 'application/modules/store/assets/images/thumbnail.jpg', 'birth_report', 1),
(65, 17, 'Add Death Report', 'Add Death Report', 'application/modules/store/assets/images/thumbnail.jpg', 'add_death_report', 1),
(66, 17, 'Death Report', 'Death Report', 'application/modules/store/assets/images/thumbnail.jpg', 'death_report', 1),
(67, 17, 'Add Operation Report', 'Add Operation Report', 'application/modules/store/assets/images/thumbnail.jpg', 'add_operation_report', 1),
(68, 17, 'Operation Report', 'Operation Report', 'application/modules/store/assets/images/thumbnail.jpg', 'operation_report', 1),
(69, 17, 'Add Investigation Report', 'Add Investigation Report', 'application/modules/store/assets/images/thumbnail.jpg', 'add_investigation_report', 1),
(70, 17, 'Investigation Report', 'Investigation Report', 'application/modules/store/assets/images/thumbnail.jpg', 'investigation_report', 1),
(71, 26, 'Add Medicine Category', 'Add Medicine Category', 'application/modules/store/assets/images/thumbnail.jpg', 'add_medicine_category', 1),
(72, 26, 'Medicine Category List', 'Medicine Category List', 'application/modules/store/assets/images/thumbnail.jpg', 'medicine_category_list', 1),
(73, 26, 'Add Medicine Type', 'Add Medicine Type', 'application/modules/store/assets/images/thumbnail.jpg', 'add_medicine_type', 1),
(74, 26, 'Medicine Type List', 'Medicine type List', 'application/modules/store/assets/images/thumbnail.jpg', 'medicine_type_list', 1),
(75, 18, 'Enquiry', 'Enquiry', 'application/modules/store/assets/images/thumbnail.jpg', 'enquiry', 1),
(76, 19, 'App Setting', 'App Setting', 'application/modules/store/assets/images/thumbnail.jpg', 'app_setting', 1),
(77, 19, 'Language Setting', 'Language Setting', 'application/modules/store/assets/images/thumbnail.jpg', 'language_setting', 1),
(78, 20, 'Gateway Setting', 'Gateway Setting', 'application/modules/store/assets/images/thumbnail.jpg', 'gateway_setting', 1),
(79, 20, 'SMS Template', 'SMS Template', 'application/modules/store/assets/images/thumbnail.jpg', 'sms_template', 1),
(80, 20, 'SMS Schedule', 'SMS Schedule', 'application/modules/store/assets/images/thumbnail.jpg', 'sms_schedule', 1),
(81, 20, 'send_custom_sms', 'Send Custom SMS', 'application/modules/store/assets/images/thumbnail.jpg', 'send_custom_sms', 1),
(82, 20, 'Custom SMS List', 'Custom SMS List', 'application/modules/store/assets/images/thumbnail.jpg', 'custom_sms_list', 1),
(83, 20, 'Auto SMS Report', 'Auto SMS Report', 'application/modules/store/assets/images/thumbnail.jpg', 'auto_sms_report', 1),
(84, 20, 'User SMS List', 'User SMS List', 'application/modules/store/assets/images/thumbnail.jpg', 'user_sms_list', 1),
(85, 21, 'New Message', 'New Message', 'application/modules/store/assets/images/thumbnail.jpg', 'new_message', 1),
(86, 21, 'Inbox', 'Inbox', 'application/modules/store/assets/images/thumbnail.jpg', 'inbox', 1),
(87, 21, 'Sent', 'Sent', 'application/modules/store/assets/images/thumbnail.jpg', 'sent', 1),
(88, 22, 'Send Mail', 'Send Mail', 'application/modules/store/assets/images/thumbnail.jpg', 'send_mail', 1),
(89, 22, 'Mail Setting', 'Mail Setting', 'application/modules/store/assets/images/thumbnail.jpg', 'mail_setting', 1),
(90, 23, 'Setting', 'Setting', 'application/modules/store/assets/images/thumbnail.jpg', 'web_setting', 1),
(91, 23, 'Slider', 'Slider', 'application/modules/store/assets/images/thumbnail.jpg', 'slider', 1),
(92, 23, 'Section', 'Section', 'application/modules/store/assets/images/thumbnail.jpg', 'section', 1),
(93, 23, 'Section Item', 'Section Item', 'application/modules/store/assets/images/thumbnail.jpg', 'section_item', 1),
(94, 23, 'Comments', 'Comments', 'application/modules/store/assets/images/thumbnail.jpg', 'comments', 1),
(95, 8, 'Create Prescription', 'Create Prescription', 'application/modules/store/assets/images/thumbnail.jpg', 'create_prescription', 1),
(96, 7, 'Assign To Me', 'Assign To Me', 'application/modules/store/assets/images/thumbnail.jpg', 'assign_to_me', 1),
(97, 7, 'Assign By Me', 'Assign By Me', 'application/modules/store/assets/images/thumbnail.jpg', 'assign_by_me', 1),
(98, 19, 'Add Phrase', 'Add Phrase', 'application/modules/store/assets/images/thumbnail.jpg', 'add_phrase', 1),
(99, 24, 'Add Role', 'Add Role', 'application/modules/store/assets/images/thumbnail.jpg', 'add_role', 1),
(101, 24, 'Assign Role To USer', 'Assign Role To USer', 'application/modules/store/assets/images/thumbnail.jpg', 'assign_role_to_user', 1),
(102, 24, 'Role Permission', 'Role Permission', 'application/modules/store/assets/images/thumbnail.jpg', 'role_permission', 1),
(103, 19, 'Profile', 'Profile', 'application/modules/store/assets/images/thumbnail.jpg', 'profile', 1),
(104, 19, 'Edit Profile', 'Edit Profile', 'application/modules/store/assets/images/thumbnail.jpg', 'edit_profile', 1),
(105, 25, 'Add Medication', 'Add Medication', '', 'add_medication', 1),
(106, 25, 'Medication List', 'Medication List', '', 'medication_list', 1),
(107, 25, 'Add Visit', 'Add Visit', '', 'add_visit', 1),
(108, 25, 'Visit List', 'Visit List', '', 'visit_list', 1),
(109, 25, 'Medication Report', 'Medication Report', '', 'medication_report', 1),
(110, 25, 'Patient Visit Report', 'Patient Visit Report', '', 'visit_report', 1),
(111, 23, 'Add Menu', 'Add Menu', '', 'add_menu', 1),
(112, 23, 'Menu List', 'Menu List', '', 'menu', 1),
(113, 23, 'Add Template', 'Add Template', '', 'add_template', 1),
(114, 23, 'Template List', 'Template List, Edit and Delete', '', 'template', 1),
(115, 23, 'About Us', 'About Us', '', 'about', 1),
(116, 23, 'Testimonial', 'Testimonial, Edit, Add and Delete', '', 'testimonial', 1),
(117, 23, 'Appointment Instructions', 'Appointment Instructions add, edit and delete', '', 'appoint_instruction', 1),
(118, 23, 'Partner', 'partner add, edit and delete', '', 'partner', 1),
(119, 23, 'News', 'News Add, Edit and Delete', '', 'news', 1),
(120, 23, 'Services', 'Service Add, Edit and Delete', '', 'service', 1),
(121, 3, 'Add Main Department', 'Add Main Department', '', 'add_main_department', 1),
(122, 3, 'Main Department List', 'Main Department edit and delete', '', 'main_department', 1),
(123, 9, 'Bank Book', 'Bank Book', '', 'bank_book', 1),
(124, 9, 'General Ledger', 'General Ledger', '', 'general_ledger', 1),
(125, 9, 'Profit Loss', 'Profit Loss', '', 'profit_loss', 1),
(126, 6, 'Add Time Slot', 'Doctor time slot', '', 'add_time_slot', 1),
(127, 1, 'Graph', 'Graph', '', 'graph', 1),
(128, 1, 'Quick Menu', 'Quick Menu', '', 'quick_menu', 1),
(129, 1, 'Noticeboard', 'Noticeboard', '', 'noticeboard', 1),
(130, 1, 'Messages', 'Messages', '', 'messages', 1),
(131, 28, 'Add Vital Sign', '', '', 'add_vital_sign', 1),
(132, 28, 'Vital Sign List', '', '', 'vital_sign_list', 1),
(133, 27, 'Input Output Chart', '', '', 'inputoutput_form', 1),
(134, 27, 'Input Output List', '', '', 'inputoutpur_list', 1),
(135, 27, 'Medication Management', '', '', 'inputout_medication_management', 1),
(136, 27, 'Medication List', '', '', 'patient_medication_management', 1),
(137, 26, 'Add Receive From', '', '', 'manufacturer_add', 1),
(138, 26, 'Receive From List', '', '', 'manufacturer_list', 1),
(139, 26, 'Add Medicine', '', '', 'add_medicine', 1),
(140, 26, 'Medicine List', '', '', 'medicine_list', 1),
(141, 26, 'Add Purchase', '', '', 'purchase_add', 1),
(142, 26, 'Purchase List', '', '', 'purchase_list', 1),
(143, 26, 'Add Invoice', '', '', 'invoice_add', 1),
(144, 26, 'Invoice List', '', '', 'invoice_list', 1),
(145, 26, 'Stock Management', '', '', 'stock_management', 1),
(146, 26, 'Add Issue', '', '', 'add_issue', 1),
(147, 26, 'Issue List', '', '', 'issue_list', 1),
(148, 29, 'Add Branch', '', '', 'add_branch', 1),
(149, 29, 'Branch List', '', '', 'branch_list', 1),
(150, 30, 'Add Referrer', '', '', 'add_referrer', 1),
(151, 30, 'Referrer List', '', '', 'referrer_list', 1),
(152, 7, 'Referrer Appointment', '', '', 'referrer_appointment', 1);

-- --------------------------------------------------------

--
-- Table structure for table `time_slot`
--

CREATE TABLE IF NOT EXISTS `time_slot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slot_name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `slot` varchar(15) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_slot`
--

INSERT INTO `time_slot` (`id`, `slot_name`, `slot`, `status`) VALUES
(1, 'Morning', '08:00 - 12:00', 1),
(2, 'Evening', '12:00 - 15:00', 1),
(3, 'new Slot', '08:30-10:30', 1),
(4, 'Morning', '08:00-02:00', 1),
(5, 'Morning', '08:00 - 12:00', 1),
(6, 'Morning to Evening', '08:00 - 06:00', 1),
(7, 'Morning shift', '08:00 - 15:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `user_role` tinyint(1) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `blood_group` varchar(10) DEFAULT NULL,
  `vacation` varchar(40) NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `youtube` varchar(150) NOT NULL,
  `dribbble` varchar(150) NOT NULL,
  `behance` varchar(150) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `branch_id` int(11) DEFAULT 0,
  `rf_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `password`, `user_role`, `department_id`, `picture`, `date_of_birth`, `sex`, `blood_group`, `vacation`, `facebook`, `twitter`, `youtube`, `dribbble`, `behance`, `created_by`, `create_date`, `update_date`, `branch_id`, `rf_id`, `status`) VALUES
(1, 'Hasan', 'Khan', 'hasan@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 1, 12, '', '2016-10-12', 'Male', 'A+', '', '', '', '', '', '', 2, '2018-08-28', NULL, 0, NULL, 1),
(2, 'Jhon', 'Doei', 'admin@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 1, NULL, '', '1970-01-01', 'Male', NULL, '', '', '', '', '', '', 2, '2019-01-15', NULL, 0, NULL, 1),
(7, 'Hasan', 'Khan', 'receptionist@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 7, NULL, '', '1970-01-01', 'Male', NULL, '', '', '', '', '', '', 2, '2018-08-29', NULL, 0, NULL, 1),
(8, 'Ashik', 'Islam', 'representative@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 8, NULL, '', '1970-01-01', 'Male', NULL, '', '', '', '', '', '', 2, '2018-08-29', NULL, 0, NULL, 1),
(12, 'Elvera ', 'Lewis', 'pharmacist@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 6, 12, '', '0000-00-00', 'Male', 'A+', '', '', '', '', '', '', 2, '2018-08-29', NULL, 0, NULL, 1),
(19, 'Ahmed', 'Ziniya', 'laboratorist@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 4, NULL, '', '1970-01-01', 'Male', NULL, '', '', '', '', '', '', 19, '2017-03-16', NULL, 0, NULL, 1),
(24, 'Meshu', 'Munawar', 'pharmacist@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 6, NULL, '', '1970-01-01', 'Male', NULL, '', '', '', '', '', '', 24, '2017-03-16', NULL, 0, NULL, 1),
(29, 'Bay', 'Smith', 'nurse@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 5, NULL, '', '1970-01-01', 'Male', NULL, '', '', '', '', '', '', 2, '2018-08-29', NULL, 0, NULL, 1),
(30, 'Tuhin', 'Abdullah', 'case@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 9, NULL, '', '1970-01-01', 'Male', NULL, '', '', '', '', '', '', 2, '2018-08-29', NULL, 0, NULL, 1),
(31, 'John', 'Doe', 'case2@demo.com', '827ccb0eea8a706c4c34a16891f84e7b', 9, NULL, '', NULL, 'Male', NULL, '', '', '', '', '', '', 2, '2017-10-28', NULL, 0, NULL, 1),
(37, 'Al', 'Hassan', 'alhassan@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 4, NULL, '', NULL, 'Male', NULL, '', '', '', '', '', '', 2, '2018-08-29', NULL, 0, NULL, 1),
(38, 'Sharon', 'Wooten', 'sharonrwooten@dayrep.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 16, '', '2018-08-28', 'Male', 'A-', '', '', '', '', '', '', 2, '2019-01-26', NULL, 0, NULL, 1),
(39, 'Joe', 'Keller', 'joekeller@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 31, '', '1970-01-01', 'Male', '', '12 January, 2019 - 23 June, 2019', 'http://facebook.com', 'http://twitter.com', 'http://youtube.com', 'http://dribble.com', 'http://behance.com', 2, '2019-01-26', NULL, 0, NULL, 1),
(40, 'Kevin', 'Wright', 'kevinwright@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 32, '', '2018-08-28', 'Male', 'O+', '', '', '', '', '', '', 2, '2019-01-26', NULL, 0, NULL, 1),
(41, 'Janina', 'Bach', 'bach@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 3, 28, '', '2018-08-28', 'Male', 'O-', '', '', '', '', '', '', 2, '2018-08-29', NULL, 0, NULL, 1),
(42, 'Kaitlyn', 'Campion', 'kaitlyncampion@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 31, '', '2018-08-28', 'Male', 'B-', '', '', '', '', '', '', 2, '2019-01-26', NULL, 0, NULL, 1),
(43, 'Devid ', 'Mullar', 'devid@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 10, NULL, NULL, NULL, 'Male', NULL, '', '', '', '', '', '', 2, '2018-11-07', NULL, 0, NULL, 1),
(44, 'Ashraf', 'Rahman', 'asrafahmanb@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 5, NULL, '', NULL, 'Male', NULL, '', '', '', '', '', '', 2, '2018-12-25', NULL, 0, NULL, 1),
(48, 'John ', 'Carry', 'john@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 22, '', '1990-12-12', 'Male', '', '', '', '', '', '', '', 2, '2019-01-26', NULL, 0, NULL, 1),
(49, 'Fahana ', 'Sultana', 'farhana@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 5, NULL, NULL, NULL, 'Female', NULL, '', '', '', '', '', '', 2, '2018-12-09', NULL, 0, NULL, 1),
(50, 'James ', 'Smith', 'jamessmith@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 30, '', '1988-12-18', 'Male', 'O-', '', '', '', '', '', '', 2, '2019-01-26', NULL, 0, NULL, 1),
(70, 'Sumon', 'Ahmed', 'sumon@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 16, '', '2019-01-15', 'Male', 'B+', '', '', '', '', '', '', 2, '2019-01-26', NULL, 0, NULL, 1),
(71, 'Shahriar', 'Kabir', 'shahriar@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 1, NULL, NULL, NULL, 'Male', NULL, '', '', '', '', '', '', 2, '2019-01-21', NULL, 0, NULL, 1),
(72, 'Sayan', 'Rahman', 'sayan@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 3, NULL, NULL, NULL, 'Male', NULL, '', '', '', '', '', '', 71, '2019-01-21', NULL, 0, NULL, 1),
(73, 'Tanzil', 'Ahmad', 'bdtask@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, '', '1970-01-01', 'Male', NULL, '', '', '', '', '', '', 73, '2019-01-24', NULL, 0, NULL, 1),
(74, 'Md', 'Kuddus', 'kuddus@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 5, NULL, 'assets/images/human_resources/f624fe85cb060d8c03b27917aeb030ef.jpg', NULL, 'Male', NULL, '', '', '', '', '', '', 2, '2019-08-22', NULL, 1, NULL, 1),
(75, 'Hm', 'Isahaq', 'hmisahaq012@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 27, 'assets/images/doctor/28661b99ec76556e7d1d2cb98e8e0b43.png', '2019-08-22', 'Male', 'O+', '', '', '', '', '', '', 2, '2019-08-22', NULL, 1, NULL, 1),
(76, 'Md', 'shofiqul', 'referrer@gmail.com', 'fcea920f7412b5da7be0cf42b8c93759', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-08-31', NULL, 0, 5, 1),
(78, 'kawser', 'Ahmad', 'hmisahaq00121@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, NULL, NULL, 'Male', NULL, '', '', '', '', '', '', 2, '2019-09-07', NULL, 1, NULL, 1),
(79, 'drwaj', 'drkaz', 'waj@waj.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 18, '', '2019-09-09', 'Male', 'A+', '', '', '', '', '', '', 2, '2019-09-09', NULL, 0, NULL, 1),
(80, 'Al', 'Mahmud', 'almahmud@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-11', NULL, 0, 12, 1),
(82, 'wajeeh', 'kazmi', 'waj@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-11', NULL, 0, 13, 1),
(84, 'Ishaq', 'sorkar', 'ishaq@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-12', NULL, 0, 14, 1),
(85, 'S.M', 'Faruk', 'faruk@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-12', NULL, 0, 15, 1),
(86, 'tan', 'zil', 'tan@zil.com', '827ccb0eea8a706c4c34a16891f84e7b', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-15', NULL, 0, 16, 1),
(87, 'Sm', 'Isahaq', 'hmisahaq01@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-15', NULL, 0, 17, 1),
(90, 'Sumch Mohammad', 'Tarek', 'tanzil4091@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-16', NULL, 0, 20, 1),
(91, 'jakir', 'mia', 'jk@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-16', NULL, 0, 21, 1),
(92, 'hhh', 'sdfsdf', 'hhh@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-16', NULL, 0, 22, 1),
(93, 'tuhhin', 'Mia', 'tuhhin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-16', NULL, 0, 23, 1),
(94, 'kkk', '', 'kkkk@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-16', NULL, NULL, 24, 1),
(95, 'ooo', '', 'oooo@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-16', NULL, 0, 25, 1),
(96, 'ishaq', 'hossain', 'bdtask1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', 2, '2019-09-16', NULL, 0, 26, 1),
(97, 'waj', 'kaz', 'waj@kaz.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 16, '', '2019-09-17', 'Male', '', '', '', '', '', '', '', 2, '2019-09-17', NULL, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_lang`
--

CREATE TABLE IF NOT EXISTS `user_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `language` varchar(15) NOT NULL,
  `designation` varchar(120) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `mobile` varchar(25) NOT NULL,
  `career_title` varchar(200) NOT NULL,
  `short_biography` text NOT NULL,
  `specialist` varchar(200) NOT NULL,
  `degree` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_lang`
--

INSERT INTO `user_lang` (`id`, `user_id`, `firstname`, `lastname`, `language`, `designation`, `address`, `phone`, `mobile`, `career_title`, `short_biography`, `specialist`, `degree`) VALUES
(1, 50, 'James ', 'Smith', 'english', 'Assistant Professor', 'thgjgfj', '56777777', '56456456456', 'I am 30 years old and 6 years experience of Radiotherapy in California', '<p>With a Computer Science & Engineering, I have a full understanding of the full life cycle of a software development project. I also have experience in learning and excelling at new technologies as needed.</p>', 'Spine', '<p>With a Computer Science & Engineering, I have a full understanding of the full life cycle of a software development project. I also have experience in learning and excelling at new technologies as needed.</p>'),
(2, 50, 'James', 'Smiths', 'french', 'maître assistant', 'dgfdgdfg', '56777777', '344534543564', 'fgdf', '<p>dfg</p>', 'colonne vertébrale', '<p>dfgfd</p>'),
(3, 50, 'جوامع', 'حداد', 'arabic', 'استاذ مساعد', '7655 سبرينج لين\r\nLincolnton، NC 28092', '56777777', '344534543564', 'أنا عمري 30 سنة و 6 سنوات خبرة في العلاج الإشعاعي في كاليفورنيا', '<p>باستخدام علوم الكمبيوتر والهندسة ، لدي فهم كامل لدورة الحياة الكاملة لمشروع تطوير البرمجيات. لدي أيضًا خبرة في التعلم والتميز في التقنيات الجديدة حسب الحاجة.</p>', 'العمود الفقري', '<p>باستخدام علوم الكمبيوتر والهندسة ، لدي فهم كامل لدورة الحياة الكاملة لمشروع تطوير البرمجيات. لدي أيضًا خبرة في التعلم والتميز في التقنيات الجديدة حسب الحاجة.</p>'),
(4, 50, 'জেমস ', 'স্মিথ', 'bangla', 'সহকারী অধ্যাপক', '৭৬৫৫ স্প্রিং লেন\r\nলিঙ্কনটন, এনসি ২৮০৯৬', '৭৬৫৫৫৬৭৫', '২৮০৯৬৫৭৫৭৫৬', 'আমি ক্যালিফোর্নিয়ায় 30 বছর বয়সী এবং রেডিওথেরাপি 6 বছর অভিজ্ঞতা', '<p>একটি কম্পিউটার বিজ্ঞান ও প্রকৌশল দিয়ে, আমার একটি সফ্টওয়্যার ডেভেলপমেন্ট প্রকল্পের পুরো জীবনচক্রের সম্পূর্ণ বোঝার আছে। আমি প্রয়োজন হিসাবে নতুন প্রযুক্তি শেখার এবং excelling অভিজ্ঞতা আছে।</p>', 'কণ্টক', '<p>একটি কম্পিউটার বিজ্ঞান ও প্রকৌশল দিয়ে, আমার একটি সফ্টওয়্যার ডেভেলপমেন্ট প্রকল্পের পুরো জীবনচক্রের সম্পূর্ণ বোঝার আছে। আমি প্রয়োজন হিসাবে নতুন প্রযুক্তি শেখার এবং excelling অভিজ্ঞতা আছে।</p>'),
(5, 42, 'কৈটলিন ', 'ক্যাম্পিয়ন', 'bangla', 'পরিচালন অধিকর্তা', '9 ফাওয়ান রা।\r\nহার্ডন, ভিএ 20170', '৪৬৪৩৬৪৫', '৫৪৫৩৪৫৩৪৪৫', 'আমি মার্কিন যুক্তরাষ্ট্র থেকে 14 বছরের অভিজ্ঞতা সহ 40 বছর বয়সী কার্ডিওলোজিস্ট।', '<p>এমন একটি সংস্থা বা প্রতিষ্ঠানের সাথে চাকরি পেতে যা আমাকে নতুন প্রযুক্তিগুলি শিখতে এবং ব্যবসার উন্নতির জন্য তাদের বাস্তবায়নের জন্য একটি ধারাবাহিকভাবে ইতিবাচক পরিবেশ সরবরাহ করে।</p>', 'অণুজীব বিজ্ঞানী', '<p>এমন একটি সংস্থা বা প্রতিষ্ঠানের সাথে চাকরি পেতে যা আমাকে নতুন প্রযুক্তিগুলি শিখতে এবং ব্যবসার উন্নতির জন্য তাদের বাস্তবায়নের জন্য একটি ধারাবাহিকভাবে ইতিবাচক পরিবেশ সরবরাহ করে।</p>'),
(6, 42, 'Kaitlyn ', 'Campion', 'french', 'Directeur général', '9 Fawn Rd. \r\nHerndon, VA 20170', '546456450', '5464564564560', 'J\'ai 40 ans, cardiologue et 14 ans d’expérience aux États-Unis.', '<p>Obtenir un emploi dans une entreprise ou une institution qui m\'offre une atmosphère toujours positive pour apprendre de nouvelles technologies et les mettre en œuvre pour améliorer l\'entreprise.</p>', 'Microbiologiste', '<p>Obtenir un emploi dans une entreprise ou une institution qui m\'offre une atmosphère toujours positive pour apprendre de nouvelles technologies et les mettre en œuvre pour améliorer l\'entreprise.</p>'),
(7, 42, 'كايتلين ', 'كامبيون', 'arabic', 'المدير العام', '9 افتون الطريق.\r\nهيرندون ، فيرجينيا 20170', '564564353', '43534534534', 'أنا طبيب قلب يبلغ من العمر 40 عامًا ولدي 14 عامًا من الخبرة من الولايات المتحدة الأمريكية.', '<p>للحصول على وظيفة مع شركة أو مؤسسة توفر لي جوا إيجابيا باستمرار لتعلم التقنيات الجديدة وتنفيذها من أجل تحسين الأعمال.</p>', 'الميكروبيولوجي', '<p>للحصول على وظيفة مع شركة أو مؤسسة توفر لي جوا إيجابيا باستمرار لتعلم التقنيات الجديدة وتنفيذها من أجل تحسين الأعمال.</p>'),
(8, 42, 'Kaitlyn ', 'Campion', 'english', 'Managing Director', '9 Fawn Rd. \r\nHerndon, VA 20170', '74564564', '456346346', 'I am 40 years old Cardiologist with 14 years of experience from USA.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'Microbiologist', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>'),
(9, 39, 'Joe ', 'Keller', 'english', 'Office Incharge', 'USA', '654645', '453453453', 'Use no more than 5 lines, including: Addressee\'s name. Street address or P.O. box number.', '<p>Use no more than 5 lines, including: Addressee\'s name. Street address or P.O. box number.</p>', 'Adolescent medicine specialist.', '<p>Use no more than 5 lines, including: Addressee\'s name. Street address or P.O. box number.</p>'),
(10, 39, 'জো ', 'কেলার', 'bangla', 'অফিস ইনচার্জ', 'আমেরিকা', '৪৬৪৩৬৪৫', '৫৪৫৩৪৫৩৪৪৫', '5 টির বেশি লাইন ব্যবহার করুন, এড্রেসেসের নাম সহ: রাস্তার ঠিকানা বা P.O. বক্স নাম্বার.', '<p>5 টির বেশি লাইন ব্যবহার করুন, এড্রেসেসের নাম সহ: রাস্তার ঠিকানা বা P.O. বক্স নাম্বার.</p>', 'কিশোরী ওষুধ বিশেষজ্ঞ।', '<p>5 টির বেশি লাইন ব্যবহার করুন, এড্রেসেসের নাম সহ: রাস্তার ঠিকানা বা P.O. বক্স নাম্বার.</p>'),
(11, 39, 'جو ', 'كيلر', 'arabic', 'مكتب شحنة', 'الولايات المتحدة الأمريكية', '456456', '56463556756', 'لا تستخدم أكثر من 5 أسطر ، بما في ذلك: اسم Addressee. عنوان الشارع أو P.O. رقم الصندوق.', '<p>لا تستخدم أكثر من 5 أسطر ، بما في ذلك: اسم Addressee. عنوان الشارع أو P.O. رقم الصندوق.</p>', 'اختصاصي طب المراهقين.', '<p>لا تستخدم أكثر من 5 أسطر ، بما في ذلك: اسم Addressee. عنوان الشارع أو P.O. رقم الصندوق.</p>'),
(12, 39, 'Joe ', 'Keller', 'french', 'Charge de bureau', 'Etats-Unis', '546456', '5645456456', 'N\'utilisez pas plus de 5 lignes, y compris: Nom du destinataire. Adresse de rue ou P.O. numéro de boîte.', '<p>N\'utilisez pas plus de 5 lignes, y compris: Nom du destinataire. Adresse de rue ou P.O. numéro de boîte.</p>', 'Spécialiste en médecine adolescente.', '<p>N\'utilisez pas plus de 5 lignes, y compris: Nom du destinataire. Adresse de rue ou P.O. numéro de boîte.</p>'),
(13, 1, 'Hasan ', 'Khan', 'english', 'Officer', '98, Green Road, Farmgate, Dhaka -1215', '45745756756', '0123456789', 'fghghh', '<p>gfhfgh</p>', 'Spine', '<p>vbcvb</p>'),
(15, 29, 'Bay', 'Smith', 'english', 'Office Incharge', 'hhjh', '56777777', '787678678', 'tytyrtyrty', '<p>hghg</p>', 'fgfdg', '<p>ghjghj</p>'),
(17, 2, 'Jhon', 'Doei', 'english', 'Professor', 'USA', '0123456879', '01725436866', 'dfgdfg', '<p>fgg</p>', 'Neorulogist', '<p>xcvcx</p>'),
(18, 38, 'Sharon ', 'Wooten', 'english', 'Department Head', 'cvdfg', '56456456', '4534534534534', 'testing', '<p>tt</p>', 'Specialist Of Oncology', '<p>gfg</p>'),
(19, 70, 'Sumon', 'Ahmed', 'english', 'Professor', 'dsfsad', '56456456', '12345', 'I am 48 years old Cardiologist with 25 years of experience from Warsaw.', '<p>Through my 5 years of my career I have been working for over 500 clients from all over the World. Thanks to my experience I am able to provide high quality services.</p>', 'Neorulogist', '<p>Through my 5 years of my career I have been working for over 500 clients from all over the World. Thanks to my experience I am able to provide high quality services.</p>'),
(20, 40, 'Kevin ', 'Wright', 'english', 'Assistant Professor', 'USA', '456789', '2335436643', 'I am Kevin  Wright and 20 years experience.', '<p>fsdf</p>', 'Oncologist', '<p>gdf</p>'),
(21, 38, 'শ্যারন ', 'Wooten', 'bangla', 'বিভাগিও প্রধান', 'Norway', '89976543', '2347789096', 'ক্যান্সার বিশেষজ্ঞ দশ বছর', '<p>fdsfsdf</p>', 'অনকোলজি বিশেষজ্ঞ', '<p>dfsd</p>'),
(22, 38, 'شارون ', 'ووتين', 'arabic', 'رئيس القسم', 'Norway', '4567890768', '34567890098', 'عشر سنوات من تجارب الاورام', '<p>عشر سنوات من تجارب الاورام</p>', 'متخصص في علم الأورام', '<p>متخصص في علم الأورام</p>'),
(23, 38, 'Sharon ', 'Wooten', 'french', 'Chef de département', 'Norway', '45678900', '89765438904', 'Dix ans d\'expérience d\'oncologue', '<p>Spécialiste en oncologie</p>', 'Spécialiste en oncologie', '<p>Spécialiste en oncologie</p>'),
(24, 70, 'Sumon ', 'Ahmed', 'french', 'Maître assistant', 'Norway', '25345343', '9078456456785', 'Dix ans d\'expérience d\'oncologue', '<p>dfg</p>', 'Spécialiste en oncologie', '<p>fdf</p>'),
(25, 77, 'Tanzil', 'Ahmad', 'english', 'সহকারী অধ্যাপক', 'USA', '453543534', '34534234234', 'ক্যান্সার বিশেষজ্ঞ দশ বছর', '<p>অপারেটিং বিভাগের অনুশীলনকারী সকল বয়সের রোগীদের সাথে কাজ করে এবং একজন ব্যক্তির অপারেশনের প্রতিটি পর্যায়ে জড়িত থাকে।</p>', 'অনকোলজি বিশেষজ্ঞ', '<p>অপারেটিং বিভাগের অনুশীলনকারী সকল বয়সের রোগীদের সাথে কাজ করে এবং একজন ব্যক্তির অপারেশনের প্রতিটি পর্যায়ে জড়িত থাকে।</p>'),
(26, 81, 'waj', 'kaz', 'english', '', '', '', '', '', '', '', ''),
(27, 83, 'Sumch Mohammad', 'Tarek', 'english', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_language`
--

CREATE TABLE IF NOT EXISTS `user_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `rating` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_language`
--

INSERT INTO `user_language` (`id`, `user_id`, `name`, `type`, `rating`) VALUES
(3, 39, 'English', 'Fluent', 7),
(6, 40, 'English', 'Fluent', 7),
(7, 40, 'Bangla', 'Beginner', 4),
(9, 39, 'Bangla', 'Native', 9),
(10, 50, 'Bangla', 'Native', 8),
(11, 68, 'Bangla', 'Beginner', 4),
(12, 69, 'Bangla', 'Beginner', 4),
(13, 70, 'Bangla', 'Native', 7),
(14, 70, 'English', 'Fluent', 6),
(15, 75, 'Bangla', 'Native', 8),
(16, 75, 'Bangla', 'Native', 8),
(17, 77, 'English', 'Fluent', 8),
(20, 79, 'English', 'Native', 10);

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1=active and 0=inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_log`
--

INSERT INTO `user_log` (`id`, `user_id`, `in_time`, `out_time`, `date`, `branch_id`, `status`) VALUES
(1, 38, '10:34:50', '00:00:00', '2019-01-28', 0, 1),
(2, 39, '10:46:54', '00:00:00', '2019-01-28', 0, 1),
(3, 40, '10:05:17', '00:00:00', '2019-01-28', 0, 1),
(4, 42, '10:05:30', '00:00:00', '2019-01-28', 0, 1),
(5, 48, '10:05:38', '00:00:00', '2019-01-28', 0, 1),
(6, 50, '05:55:00', '00:00:00', '2019-01-28', 0, 1),
(7, 70, '05:23:59', '00:00:00', '2019-01-28', 0, 1),
(53, 2, '14:01:37', '00:00:00', '2019-01-28', 0, 1),
(54, 2, '06:59:44', '00:00:00', '2019-02-02', 0, 1),
(55, 2, '07:18:03', '07:22:46', '2019-02-11', 0, 0),
(56, 2, '10:33:45', '14:27:48', '2019-02-24', 0, 0),
(57, 2, '07:13:34', '00:00:00', '2019-02-26', 0, 1),
(58, 2, '07:58:44', '00:00:00', '2019-02-27', 0, 1),
(59, 2, '13:00:16', '00:00:00', '2019-03-02', 0, 1),
(60, 2, '06:34:54', '00:00:00', '2019-03-03', 0, 1),
(61, 2, '07:00:30', '00:00:00', '2019-03-04', 0, 1),
(62, 2, '11:26:22', '00:00:00', '2019-03-05', 0, 1),
(63, 2, '12:48:35', '00:00:00', '2019-03-10', 0, 1),
(64, 2, '13:38:14', '13:41:56', '2019-03-12', 0, 0),
(65, 2, '05:05:46', '00:00:00', '2019-03-13', 0, 1),
(66, 2, '10:12:17', '00:00:00', '2019-03-19', 0, 1),
(67, 2, '10:47:45', '00:00:00', '2019-03-27', 0, 1),
(68, 2, '07:53:11', '00:00:00', '2019-04-07', 0, 1),
(69, 2, '07:51:58', '08:28:01', '2019-07-01', 0, 0),
(70, 2, '08:09:29', '11:48:51', '2019-07-18', 0, 1),
(71, 29, '08:18:46', '08:18:55', '2019-07-18', 0, 0),
(72, 2, '06:11:03', '00:00:00', '2019-07-20', 0, 1),
(73, 2, '06:39:32', '00:00:00', '2019-07-21', 0, 1),
(74, 2, '06:17:28', '00:00:00', '2019-07-22', 0, 1),
(75, 2, '06:10:59', '00:00:00', '2019-07-23', 0, 1),
(76, 2, '05:16:31', '12:54:54', '2019-07-24', 0, 1),
(77, 29, '12:55:06', '12:55:20', '2019-07-24', 0, 0),
(78, 39, '12:58:10', '12:58:20', '2019-07-24', 0, 1),
(79, 12, '12:58:30', '12:58:43', '2019-07-24', 0, 0),
(80, 2, '12:25:50', '13:20:34', '2019-07-25', 0, 1),
(81, 12, '13:20:46', '13:21:02', '2019-07-25', 0, 0),
(82, 2, '06:28:47', '07:14:57', '2019-07-30', 0, 1),
(83, 2, '06:32:27', '00:00:00', '2019-07-31', 0, 1),
(84, 2, '11:49:31', '00:00:00', '2019-08-01', 0, 1),
(85, 2, '06:23:04', '00:00:00', '2019-08-03', 0, 1),
(86, 2, '12:41:16', '00:00:00', '2019-08-06', 0, 1),
(87, 2, '08:08:58', '12:26:40', '2019-08-21', 0, 1),
(88, 12, '10:28:35', '12:27:34', '2019-08-21', 0, 0),
(89, 12, '07:55:26', '07:56:01', '2019-08-22', 0, 0),
(90, 2, '07:56:13', '13:40:17', '2019-08-22', 0, 1),
(91, 39, '13:40:20', '13:40:37', '2019-08-22', 0, 0),
(92, 41, '13:40:45', '13:40:49', '2019-08-22', 0, 0),
(93, 2, '06:12:06', '00:00:00', '2019-08-24', 0, 1),
(94, 2, '06:45:10', '00:00:00', '2019-08-25', 0, 1),
(95, 2, '08:31:35', '00:00:00', '2019-08-26', 0, 1),
(96, 2, '06:25:18', '00:00:00', '2019-08-27', 0, 1),
(97, 2, '07:04:46', '14:44:40', '2019-08-28', 0, 1),
(98, 2, '07:05:59', '00:00:00', '2019-08-29', 0, 1),
(99, 2, '06:25:02', '15:06:56', '2019-08-31', 0, 1),
(100, 76, '08:59:50', '15:06:49', '2019-08-31', 0, 0),
(101, 2, '06:18:09', '11:22:00', '2019-09-01', 0, 0),
(102, 76, '06:32:46', '13:25:46', '2019-09-01', 0, 0),
(103, 39, '08:33:58', '10:52:17', '2019-09-01', 0, 1),
(104, 2, '08:17:45', '13:50:07', '2019-09-02', 0, 0),
(105, 76, '09:28:31', '13:54:10', '2019-09-02', 0, 1),
(106, 2, '05:19:36', '15:56:01', '2019-09-03', 0, 1),
(107, 76, '05:31:06', '05:50:11', '2019-09-03', 0, 1),
(108, 39, '05:36:02', '16:03:22', '2019-09-03', 0, 0),
(109, 41, '05:45:15', '05:45:19', '2019-09-03', 0, 0),
(110, 12, '05:45:22', '05:45:44', '2019-09-03', 0, 0),
(111, 2, '06:10:34', '14:57:31', '2019-09-04', 0, 0),
(112, 76, '06:30:49', '14:05:37', '2019-09-04', 0, 1),
(113, 39, '07:06:27', '15:02:01', '2019-09-04', 0, 0),
(114, 41, '11:33:22', '11:33:29', '2019-09-04', 0, 0),
(115, 2, '06:13:18', '13:45:22', '2019-09-05', 0, 1),
(116, 39, '09:01:52', '14:15:44', '2019-09-05', 0, 0),
(117, 2, '06:05:56', '13:43:59', '2019-09-07', 0, 1),
(118, 39, '12:23:28', '12:23:37', '2019-09-07', 0, 0),
(119, 78, '13:44:09', '00:00:00', '2019-09-07', 0, 1),
(120, 78, '05:38:45', '00:00:00', '2019-09-08', 0, 1),
(121, 2, '06:53:16', '00:00:00', '2019-09-08', 0, 1),
(122, 2, '12:43:41', '12:59:09', '2019-09-09', 0, 1),
(123, 79, '12:52:39', '00:00:00', '2019-09-09', 0, 1),
(124, 39, '12:59:12', '00:00:00', '2019-09-09', 0, 1),
(125, 2, '05:17:13', '06:16:21', '2019-09-11', 0, 1),
(126, 80, '06:03:35', '06:18:09', '2019-09-11', 0, 0),
(127, 82, '08:51:46', '00:00:00', '2019-09-11', 0, 1),
(128, 76, '08:56:56', '00:00:00', '2019-09-11', 0, 1),
(129, 81, '09:18:16', '09:20:14', '2019-09-11', 0, 1),
(130, 39, '13:11:09', '00:00:00', '2019-09-11', 0, 1),
(131, 2, '08:27:14', '11:24:12', '2019-09-12', 0, 1),
(132, 83, '08:40:51', '08:43:43', '2019-09-12', 0, 1),
(133, 84, '08:47:05', '00:00:00', '2019-09-12', 0, 1),
(134, 76, '11:18:29', '15:14:18', '2019-09-12', 0, 0),
(135, 85, '11:24:20', '11:58:57', '2019-09-12', 0, 0),
(136, 2, '10:24:54', '13:31:08', '2019-09-15', 0, 0),
(137, 81, '10:30:42', '11:10:32', '2019-09-15', 0, 1),
(138, 86, '10:35:17', '11:25:34', '2019-09-15', 0, 1),
(139, 87, '13:31:18', '13:31:28', '2019-09-15', 0, 0),
(140, 76, '05:14:02', '07:49:09', '2019-09-16', 0, 0),
(141, 2, '05:16:07', '08:03:44', '2019-09-16', 0, 1),
(142, 83, '07:19:11', '07:21:30', '2019-09-16', 0, 0),
(143, 88, '07:21:47', '08:02:46', '2019-09-16', 0, 0),
(144, 90, '07:48:39', '07:48:50', '2019-09-16', 0, 0),
(145, 91, '07:50:19', '07:52:19', '2019-09-16', 0, 0),
(146, 92, '07:53:06', '07:54:39', '2019-09-16', 0, 0),
(147, 93, '07:55:34', '07:56:43', '2019-09-16', 0, 0),
(148, 94, '07:57:33', '07:59:10', '2019-09-16', 0, 0),
(149, 95, '08:01:00', '00:00:00', '2019-09-16', 0, 1),
(150, 96, '08:03:55', '00:00:00', '2019-09-16', 0, 1),
(151, 81, '12:58:49', '13:00:04', '2019-09-16', 0, 1),
(152, 86, '13:04:08', '00:00:00', '2019-09-16', 0, 1),
(153, 2, '06:15:04', '23:53:42', '2019-09-17', 0, 1),
(154, 81, '20:04:47', '22:48:09', '2019-09-17', 0, 0),
(155, 97, '22:48:55', '23:59:17', '2019-09-17', 0, 0),
(156, 82, '22:55:06', '00:00:00', '2019-09-17', 0, 1),
(157, 2, '00:00:15', '00:00:39', '2019-09-18', 0, 1),
(158, 82, '00:00:45', '00:11:04', '2019-09-18', 0, 0),
(159, 2, '11:03:38', '11:15:32', '2019-09-19', 0, 1),
(160, 82, '11:13:12', '11:22:10', '2019-09-19', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vital_sign`
--

CREATE TABLE IF NOT EXISTS `vital_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) NOT NULL,
  `f_heart_rate` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `temprature` varchar(30) NOT NULL,
  `pulse_rate` varchar(30) NOT NULL,
  `respiration_rate` varchar(30) NOT NULL,
  `blood_pressure` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vital_sign`
--

INSERT INTO `vital_sign` (`id`, `patient_id`, `f_heart_rate`, `date`, `time`, `temprature`, `pulse_rate`, `respiration_rate`, `blood_pressure`, `description`, `branch_id`, `assign_by`) VALUES
(2, 'PZRIMPJ4', '62', '2019-07-20', '05:30:30', '95', '76', '80', '120/80', 'dfgsdfg', 0, '2'),
(3, 'PGQG6XSK', '6', '2019-07-24', '02:10:15', '4', '5', '6', '6', 'gsffg', 0, '2'),
(4, 'P69YN5VJ', '70', '2019-08-24', '08:25:30', '99', '78', '65', '120', '54', 1, '2');

-- --------------------------------------------------------

--
-- Table structure for table `ws_about`
--

CREATE TABLE IF NOT EXISTS `ws_about` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `quotation` varchar(150) CHARACTER SET utf8 NOT NULL,
  `author_name` varchar(35) CHARACTER SET utf8 NOT NULL,
  `signature` varchar(200) CHARACTER SET utf8 NOT NULL,
  `image` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ws_about`
--

INSERT INTO `ws_about` (`id`, `language`, `title`, `description`, `quotation`, `author_name`, `signature`, `image`, `status`) VALUES
(1, 'english', 'Summary Of Hospital', '<p>The simplest method of installation is through the Sublime Text conso</p>\r\n<p>The simplest method of installation is through the Sublime Text conso</p>\r\n<p>The simplest method of installation is through the Sublime Text cons</p>\r\n<p>The simplest method of installation is through the Sublime Text cons</p>\r\n<p>The simplest method of installation is through the Sublime Text cons</p>', 'Once open, paste the appropriate Python code for your version of Sublime Text into the console.', 'Michael Smith', '', '', 1),
(2, 'bangla', 'হাসপাতাল সংক্ষিপ্তসার', '<p>ইনস্টলেশনের সবচেয়ে সরল পদ্ধতি হলো স্লাইবমে টেক্সট কনসো</p>\r\n<p>ইনস্টলেশনের সবচেয়ে সরল পদ্ধতি হলো স্লাইবমে টেক্সট কনসো</p>\r\n<p>ইনস্টলেশনের সবচেয়ে সরল পদ্ধতিটি সুবাইল টেক্সট কনস</p>\r\n<p>ইনস্টলেশনের সবচেয়ে সরল পদ্ধতিটি সুবাইল টেক্সট কনস</p>', 'একবার খোলা হলে কনসোলের আপনার সংস্করণের সংস্করণটির জন্য যথাযথ পাইথন কোডটি আটকান।', 'মাইকেল স্মিথ', '', '', 1),
(3, 'arabic', 'ملخص المستشفى', '<p> أبسط طريقة للتثبيت هي من خلال النص السامي</p>\r\n<p>أبسط طريقة للتثبيت هي من خلال النص السامي</p>\r\n<p>أبسط طريقة للتثبيت هي من خلال النص السامي</p>\r\n<p>أبسط طريقة للتثبيت هي من خلال النص السامي</p>', 'بمجرد فتح ، قم بلصق رمز  المناسب لإصدارك من  في وحدة التحكم.', 'مايكل سميث', '', '', 1),
(4, 'french', 'Résumé de l\'hôpital', '<p>La méthode d’installation la plus simple consiste à utiliser Conso Text Sublime</p>\r\n<p>La méthode d’installation la plus simple consiste à utiliser Conso Text Sublime</p>\r\n<p>La méthode d\'installation la plus simple consiste à utiliser les consoles Sublime Text.</p>\r\n<p>La méthode d\'installation la plus simple consiste à utiliser les consoles Sublime Text.</p>\r\n<p> </p>\r\n<p> </p>', 'Une fois ouvert, collez le code Python approprié à votre version de Sublime Text dans la console.', 'Michael Smith', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_appoint_instruction`
--

CREATE TABLE IF NOT EXISTS `ws_appoint_instruction` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `title` varchar(30) NOT NULL,
  `short_instruction` varchar(150) NOT NULL,
  `instruction` text NOT NULL,
  `note` varchar(150) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_appoint_instruction`
--

INSERT INTO `ws_appoint_instruction` (`id`, `language`, `title`, `short_instruction`, `instruction`, `note`, `status`) VALUES
(1, 'english', 'Book with your doctor', 'Some up and coming trends are healthcare consolidation for independent healthcare centers that see a cut in unforeseen payouts.', 'Praesent pellentesque nunc vel velit varius feugiat.\r\nSuspendisse vel ex vitae velit dignissim faucibus.\r\nInteger congue erat vel bibendum volutpat.\r\nNunc nec quam dapibus, placerat est in, tincidunt nibh.\r\nSed facilisis velit sit amet purus mattis, id rutrum leo scelerisque.\r\ntesting......', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 1),
(2, 'arabic', 'احجز مع طبيبك', 'بعض الاتجاهات القادمة والقادمة هي تعزيز الرعاية الصحية لمراكز الرعاية الصحية المستقلة التي ترى خفضا في المدفوعات غير المتوقعة.', 'وأفضل أنواع المستشفيات المعروفة هو المستشفى العام \r\n، الذي يوجد به عادة قسم للطوارئ لمعالجة المشاكل \r\nالصحية العاجلة التي تتراوح بين\r\n ضحايا الحريق والحوادث إلى نوبة قلبية.', 'إنها حقيقة راسخة أن القارئ سوف يصرفه محتوى مقروء للصفحة عندما ينظر إلى تخطيطه.', 1),
(3, 'bangla', 'আপনার ডাক্তারকে  বুক করুন', 'কিছু আপ এবং আসন্ন প্রবণতা স্বাধীন স্বাস্থ্যসেবা কেন্দ্রগুলির জন্য স্বাস্থ্যসেবা একীকরণ যা অপ্রত্যাশিত অর্থ প্রদানের মধ্যে একটি কাট দেখতে পায়।', 'একটি হাসপাতাল একটি স্বাস্থ্য সেবা প্রতিষ্ঠান যা বিশেষ চিকিৎসা ও নার্সিং কর্মীদের এবং চিকিৎসা সরঞ্জামগুলির সাথে রোগীর চিকিৎসা প্রদান করে। \r\nহাসপাতালের সবচেয়ে সুপরিচিত প্রকার হল সাধারণ হাসপাতাল,\r\n যা সাধারণত আগুন এবং দুর্ঘটনার শিকার হওয়া হৃদরোগে ক্ষতিকারক স্বাস্থ্য \r\nসমস্যাগুলির মোকাবিলা করার জন্য একটি জরুরি বিভাগ রয়েছে।', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে।', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_banner`
--

CREATE TABLE IF NOT EXISTS `ws_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ws_banner`
--

INSERT INTO `ws_banner` (`id`, `image`, `status`) VALUES
(1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_basic`
--

CREATE TABLE IF NOT EXISTS `ws_basic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `twitter_api` text DEFAULT NULL,
  `google_map` text DEFAULT NULL,
  `google_map_api` text NOT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `vimeo` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `dribbble` varchar(100) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `google_plus` varchar(100) DEFAULT NULL,
  `direction` varchar(255) NOT NULL,
  `latitude` float(10,8) NOT NULL,
  `longitude` float(10,8) NOT NULL,
  `map_active` tinyint(4) NOT NULL COMMENT '1=embed, 0=api',
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_basic`
--

INSERT INTO `ws_basic` (`id`, `logo`, `favicon`, `email`, `twitter_api`, `google_map`, `google_map_api`, `facebook`, `twitter`, `vimeo`, `instagram`, `dribbble`, `skype`, `google_plus`, `direction`, `latitude`, `longitude`, `map_active`, `status`) VALUES
(1, 'assets_web/images/icons/e983e0e7e78cf5d5b6cd0a9d75d8547c.png', 'assets_web/images/icons/cea89bb004a4ceb09ff61af3e6ac8d54.png', 'demo@hospital.com, info@hospital.com, support@hospital.com', '<a class=\"twitter-timeline\" data-lang=\"en\" data-dnt=\"true\" data-link-color=\"#207FDD\" href=\"https://twitter.com/taylorswift13\">Tweets by taylorswift13</a> <script async src=\"//platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>', '<div class=\"mapouter\"><div class=\"gmap_canvas\"><iframe width=\"1350\" height=\"500\" id=\"gmap_canvas\" src=\"https://maps.google.com/maps?q=House%2025%2C%20mannan%20plaza%2C%20khilkhet%2C%20dhaka%2C%20bangladesh&t=&z=13&ie=UTF8&iwloc=&output=embed\" frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>Werbung: <a href=\"https://www.jetzt-drucken-lassen.de\">jetzt-drucken-lassen.de</a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:1350px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:1350px;}</style></div>', 'AIzaSyBDHeh9zEbXo-YCWJcicXH2VRwVwAf_tq0', 'http://facebook.com/', 'http://', 'http://', 'http://', 'http://', 'http://', 'http://', 'https://www.google.com/maps/dir/23.744512,90.39872/united+hospital+location/@23.7745642,90.3753058,13z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x3755c7abd941ed15:0xf151df4e4e9c047c!2m2!1d90.4154716!2d23.8046243', 10.92543983, 79.83800507, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_comment`
--

CREATE TABLE IF NOT EXISTS `ws_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `date` datetime NOT NULL,
  `add_to_website` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_comment`
--

INSERT INTO `ws_comment` (`id`, `item_id`, `name`, `email`, `comment`, `date`, `add_to_website`) VALUES
(64, 25, 'John Doe', 'doe@example.com', 'Hello World!', '2017-01-15 11:42:47', 1),
(65, 24, 'Tanzil Ahmad', 'tanzil4091@gmail.com', 'I highly recommend this application for hospital management', '2017-01-16 01:50:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_menu`
--

CREATE TABLE IF NOT EXISTS `ws_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `parent_id` int(5) NOT NULL,
  `position` int(2) NOT NULL,
  `fixed` tinyint(1) NOT NULL COMMENT '1=fixed and 0=Not fixed',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=active and 0=inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_menu`
--

INSERT INTO `ws_menu` (`id`, `name`, `parent_id`, `position`, `fixed`, `status`) VALUES
(1, 'Home', 0, 1, 1, 1),
(2, 'Service', 0, 2, 1, 1),
(3, 'About', 0, 3, 1, 1),
(4, 'Doctor', 0, 4, 1, 1),
(5, 'Department', 0, 5, 1, 1),
(6, 'Contact', 0, 6, 1, 1),
(7, 'Help', 2, 3, 0, 0),
(8, 'Condition & Policy', 2, 2, 0, 1),
(9, 'Patient Login', 0, 7, 1, 1),
(12, 'Support', 14, 1, 0, 1),
(13, 'Patient Role', 2, 1, 0, 1),
(14, 'Others', 0, 7, 0, 1),
(15, 'kjojk', 0, 9, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_menu_lang`
--

CREATE TABLE IF NOT EXISTS `ws_menu_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `language` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_menu_lang`
--

INSERT INTO `ws_menu_lang` (`id`, `menu_id`, `title`, `language`) VALUES
(1, 1, 'হোম', 'bangla'),
(2, 1, 'Home', 'english'),
(3, 1, 'الصفحة الرئيسية', 'arabic'),
(4, 1, 'Accueil', 'french'),
(5, 2, 'Services', 'english'),
(6, 2, 'Prestations de service', 'french'),
(7, 2, 'সার্ভিসেস', 'bangla'),
(8, 2, 'خدمات', 'arabic'),
(9, 3, 'About Us', 'english'),
(10, 3, 'معلومات عنا', 'arabic'),
(11, 3, 'À propos de nous', 'french'),
(12, 3, 'আমাদের সম্পর্কে', 'bangla'),
(13, 4, 'Doctors', 'english'),
(14, 4, 'ডাক্তার', 'bangla'),
(15, 4, 'الأطباء', 'arabic'),
(16, 4, 'Médecins', 'french'),
(17, 5, 'Departments', 'english'),
(18, 5, 'Départements', 'french'),
(19, 5, 'الإدارات', 'arabic'),
(20, 5, 'বিভাগ', 'bangla'),
(21, 6, 'Contact Us', 'english'),
(22, 6, 'যোগাযোগ', 'bangla'),
(23, 6, 'اتصل بنا', 'arabic'),
(24, 6, 'Contactez nous', 'french'),
(25, 7, 'Help Center', 'english'),
(26, 7, 'সাহায্য কেন্দ্র', 'bangla'),
(27, 7, 'مركز المساعدة', 'arabic'),
(28, 7, 'Centre d\'aide', 'french'),
(29, 8, 'Condition & Policy', 'english'),
(30, 8, 'Condition et politique', 'french'),
(31, 8, 'الشرط والسياسة', 'arabic'),
(32, 8, 'শর্ত ও নীতি', 'bangla'),
(33, 9, 'Patient Login', 'english'),
(34, 9, 'ইউজার লগইন', 'bangla'),
(35, 9, 'تسجيل دخول المريض', 'arabic'),
(36, 9, 'Connexion du patient', 'french'),
(37, 12, 'Supports', 'english'),
(38, 12, 'Les soutiens', 'french'),
(39, 12, 'الدعم', 'arabic'),
(40, 12, 'সাপোর্ট সেবা', 'bangla'),
(41, 14, 'Others', 'english'),
(42, 14, 'অন্যান্য', 'bangla'),
(43, 14, 'الآخرين', 'arabic'),
(44, 14, 'autres', 'french');

-- --------------------------------------------------------

--
-- Table structure for table `ws_news`
--

CREATE TABLE IF NOT EXISTS `ws_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 NOT NULL,
  `featured_image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_date` date NOT NULL,
  `create_by` int(5) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1=active and 0=inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ws_news`
--

INSERT INTO `ws_news` (`id`, `dept_id`, `title`, `featured_image`, `create_date`, `create_by`, `status`) VALUES
(6, 32, 'Pie Chart Example', '', '2019-01-23', 2, 1),
(7, 26, 'Pregnancy, also known as gestation', '', '2019-01-24', 2, 1),
(8, 31, 'What is cardiology?', '', '2019-01-24', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_news_language`
--

CREATE TABLE IF NOT EXISTS `ws_news_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `language` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_news_language`
--

INSERT INTO `ws_news_language` (`id`, `news_id`, `title`, `description`, `language`) VALUES
(5, 6, 'Pie Chart Example', '<h2>Where does it come from?</h2>\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 'english'),
(6, 6, 'Le morceau standard de Lorem Ipsum utilisé depuis', '<p>D\'où est ce que ça vient?<br>Contrairement à la croyance populaire, Lorem Ipsum n’est pas simplement un texte aléatoire. Il a ses racines dans un morceau de littérature latine classique datant de 45 ans av. Richard McClintock, professeur de latin au Hampden-Sydney College, en Virginie, a examiné l\'un des mots latins les plus obscurs, consectetur, tiré d\'un passage de Lorem Ipsum. Lorem Ipsum provient des sections 1.10.32 et 1.10.33 de \"De Finibus Bonorum et Malorum\" de Cicéron, écrit en 45 av. Ce livre est un traité sur la théorie de l\'éthique, très populaire à la Renaissance. La première ligne de Lorem Ipsum, \"Lorem ipsum dolor sit amet ..\", provient d\'une ligne de la section 1.10.32.</p>\r\n<p>Le morceau standard de Lorem Ipsum utilisé depuis les années 1500 est reproduit ci-dessous pour les personnes intéressées. Les sections 1.10.32 et 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicéron sont également reproduites dans leur forme originale, accompagnées des versions anglaises de la traduction de 1914 par H. Rackham.</p>', 'french'),
(7, 6, 'مستشفى Bdtask المحدودة', '<p>حيث أنها لا تأتي من؟<br>خلافا للاعتقاد الشائع ، لوريم إيبسوم ليس مجرد نص عشوائي. له جذور في قطعة من الأدب اللاتيني الكلاسيكي من 45 قبل الميلاد ، مما يجعلها أكثر من 2000 سنة. ريتشارد مكلينتوك ، أستاذ اللغة اللاتينية في كلية هامبدن سيدني في ولاية فرجينيا ، بحث عن واحدة من أكثر الكلمات اللاتينية الغامضة ، consectetur ، من ممر لوريم إيبسوم ، وتمر عبر أقوال الكلمة في الأدب الكلاسيكي ، اكتشف المصدر الذي لا يمكن نفيه. يأتي لوريم إيبسوم من القسمين 1.10.32 و 1.10.33 من \"دي فينيبس بونوروم إيه مالوروم\" (The Extremes of Good and Evil) بواسطة شيشرون Cicero ، وكتب في عام 45 قبل الميلاد. هذا الكتاب هو أطروحة عن نظرية الأخلاق ، تحظى بشعبية كبيرة خلال عصر النهضة. السطر الأول من Lorem Ipsum ، \"Lorem ipsum dolor sit amet ..\" ، يأتي من سطر في القسم 1.10.32.</p>\r\n<p>الجزء المعياري من لوريم إيبسوم المستخدم منذ القرن السادس عشر مذكور أدناه للمهتمين. كما يتم إعادة إنتاج القسمين 1.10.32 و 1.10.33 من \"de Finibus Bonorum et Malorum\" بواسطة Cicero في شكلها الأصلي الدقيق ، مصحوبًا بنسخ إنجليزية من ترجمة 1945 التي كتبها H. Rackham.</p>', 'arabic'),
(8, 6, 'জনপ্রিয় বিশ্বাসের বিপরীতে', '<p>এটা কোথা থেকে এসেছে?<br>জনপ্রিয় বিশ্বাসের বিপরীতে, Lorem Ipsum কেবল র্যান্ডম পাঠ্য নয়। এটি 45 বিসি থেকে শাস্ত্রীয় ল্যাটিন সাহিত্যের একটি অংশে শিকড় রয়েছে, এটি 2000 বছরেরও বেশি সময় ধরে তৈরি করেছে। ভার্জিনিয়া হ্যাম্পডেন-সিডনি কলেজের ল্যাটিন প্রফেসর রিচার্ড ম্যাক ক্লিনটক লোরেম ইপ্সামের উত্তরণ থেকে লক্ষণীয় ল্যাটিন শব্দের একটিকে দেখেছিলেন এবং শাস্ত্রীয় সাহিত্যে শব্দটির পাতায় গিয়েছিলেন। লোরম ইম্পসাম 45 খ্রিস্টপূর্বাব্দে লিখিত লিখিত সিসেরোর \"দ্য ফিনিবাস বনোরুম এট মালোরুম\" (দ্য এক্সট্রিমস অব গুড অ্যান্ড ইভিল) এর 1.10.3২ এবং 1.10.33 বিভাগ থেকে এসেছে। এই বইটি নৈতিকতা তত্ত্বের উপর একটি গ্রন্থ, যা রেনেসাঁর সময় খুব জনপ্রিয়। লোরম ইম্পসামের প্রথম লাইন, \"লোরেম ipsum dolor sit amet ..\", ধারা 1.10.32 এর একটি লাইন থেকে এসেছে।</p>\r\n<p>1500 সাল থেকে ব্যবহৃত লোরেম ইম্পসামের স্ট্যান্ডার্ড খণ্ডটি আগ্রহী ব্যক্তিদের জন্য নীচের পুনরুত্পাদন করা হয়। সিসেরোর \"ডি ফিনিবাস বনোরাম এ মালোররম\" থেকে 1.10.3২ এবং 1.10.33 অনুচ্ছেদগুলিকে তাদের সঠিক মূল রূপে পুনঃপ্রবর্তিত করা হয়েছে, যার সাহায্যে 1914 সালের অনুবাদ থেকে এইচ।</p>', 'bangla'),
(9, 7, 'Pregnancy, also known as gestation, is the time during which one or more offspring develops inside a woman.', '<p>professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 'english'),
(10, 7, 'গর্ভধারণ, গর্ভধারণের নামেও পরিচিত, সেই সময়ের মধ্যে একটি বা একাধিক বাচ্চা একজন মহিলার ভিতরে বিকাশ হয়।', '<p>ভার্জিনিয়ায় হ্যাম্পডেন-সিডনি কলেজের অধ্যাপক, লোরেম ইপ্সামের উত্তরণ থেকে ল্যাটিন শব্দগুলির আরো একটি অস্পষ্ট ল্যাটিন শব্দের দিকে তাকালেন, এবং শাস্ত্রীয় সাহিত্যে শব্দটির কিতাবগুলির মধ্য দিয়ে যাচ্ছিলেন, সন্দেহভাজন সূত্র আবিষ্কার করেছিলেন। লোরম ইম্পসাম 45 খ্রিস্টপূর্বাব্দে লিখিত লিখিত সিসেরোর \"দ্য ফিনিবাস বনোরুম এট মালোরুম\" (দ্য এক্সট্রিমস অব গুড অ্যান্ড ইভিল) এর 1.10.3২ এবং 1.10.33 বিভাগ থেকে এসেছে। এই বইটি নৈতিকতা তত্ত্বের উপর একটি গ্রন্থ, যা রেনেসাঁর সময় খুব জনপ্রিয়। লোরম ইম্পসামের প্রথম লাইন, \"লোরেম ipsum dolor sit amet ..\", ধারা 1.10.32 এর একটি লাইন থেকে এসেছে।</p>\r\n<p>1500 সাল থেকে ব্যবহৃত লোরেম ইম্পসামের স্ট্যান্ডার্ড খণ্ডটি আগ্রহী ব্যক্তিদের জন্য নীচের পুনরুত্পাদন করা হয়। সিসেরোর \"ডি ফিনিবাস বনোরাম এ মালোররম\" থেকে 1.10.3২ এবং 1.10.33 অনুচ্ছেদগুলিকে তাদের সঠিক মূল রূপে পুনঃপ্রবর্তিত করা হয়েছে, যার সাহায্যে 1914 সালের অনুবাদ থেকে এইচ।</p>', 'bangla'),
(11, 7, 'La grossesse, également appelée gestation, est la période au cours de laquelle un ou plusieurs enfants se développent à l\'intérieur d\'une femme.', '<p>Un professeur du Hampden-Sydney College, en Virginie, a recherché l\'un des mots latins les plus obscurs, consectetur, tiré d\'un passage de Lorem Ipsum. Lorem Ipsum provient des sections 1.10.32 et 1.10.33 de \"De Finibus Bonorum et Malorum\" de Cicéron, écrit en 45 av. Ce livre est un traité sur la théorie de l\'éthique, très populaire à la Renaissance. La première ligne de Lorem Ipsum, \"Lorem ipsum dolor sit amet ..\", provient d\'une ligne de la section 1.10.32.</p>\r\n<p>Le morceau standard de Lorem Ipsum utilisé depuis les années 1500 est reproduit ci-dessous pour les personnes intéressées. Les sections 1.10.32 et 1.10.33 de \"de Finibus Bonorum et Malorum\" de Cicero sont également reproduites dans leur forme originale, accompagnées des versions anglaises de la traduction de 1914 par H. Rackham.</p>', 'french'),
(12, 7, 'الحمل ، المعروف أيضا باسم الحمل ، هو الوقت الذي يتطور فيه ذرية أو أكثر داخل المرأة.', '<p><br>أستاذ في كلية هامبدن سيدني في ولاية فرجينيا ، نظرت واحدة من الكلمات اللاتينية أكثر غموضا ، consectetur ، من مرور لوريم إيبسوم ، والذهاب من خلال الاستشهادات من الكلمة في الأدب الكلاسيكي ، واكتشفت مصدر لا يمكن الاستغناء عنه. يأتي لوريم إيبسوم من القسمين 1.10.32 و 1.10.33 من \"دي فينيبس بونوروم إيه مالوروم\" (The Extremes of Good and Evil) بواسطة شيشرون Cicero ، وكتب في عام 45 قبل الميلاد. هذا الكتاب هو أطروحة عن نظرية الأخلاق ، تحظى بشعبية كبيرة خلال عصر النهضة. السطر الأول من Lorem Ipsum ، \"Lorem ipsum dolor sit amet ..\" ، يأتي من سطر في القسم 1.10.32.</p>\r\n<p>الجزء المعياري من لوريم إيبسوم المستخدم منذ القرن السادس عشر مذكور أدناه للمهتمين. كما يتم إعادة إنتاج القسمين 1.10.32 و 1.10.33 من \"de Finibus Bonorum et Malorum\" بواسطة Cicero في شكلها الأصلي الدقيق ، مصحوبًا بنسخ إنجليزية من ترجمة 1945 التي كتبها H. Rackham.</p>', 'arabic'),
(13, 8, 'What is cardiology?', '<p>Cardiologists are doctors who specialize in diagnosing and treating diseases or conditions of the heart and blood vessels—the cardiovascular system. You might also visit a cardiologist so you can learn about your risk factors for heart disease and find out what measures you can take for better heart health. Texas Heart Institute cardiologists are listed in the professional staff directory.</p>\r\n<p>When you are dealing with a complex health condition like heart disease, it is important that you find the right match between you and your specialist. A diagnosis of heart or vascular disease often begins with your primary care doctor, who then refers you to a cardiologist. The cardiologist evaluates your symptoms and your medical history and may recommend tests for a more definite diagnosis. Then, your cardiologist decides if your condition can be managed under his or her care using medicines or other available treatments. If your cardiologist decides that you need surgery, he or she refers you to a cardiovascular surgeon, who specializes in operations on the heart, lungs, and blood vessels. You remain under the care of your cardiologist even when you are referred to other specialists.</p>', 'english'),
(14, 8, 'ما هو أمراض القلب؟', '<p>أطباء القلب هم أطباء متخصصون في تشخيص ومعالجة الأمراض أو أمراض القلب والأوعية الدموية - نظام القلب والأوعية الدموية. يمكنك أيضا زيارة طبيب القلب حتى تتمكن من معرفة عوامل الخطر الخاصة بك لأمراض القلب ومعرفة التدابير التي يمكن اتخاذها لتحسين صحة القلب. يتم سرد أمراض القلب معهد القلب تكساس في دليل الموظفين الفنيين.</p>\r\n<p>عندما تتعامل مع حالة صحية معقدة مثل أمراض القلب ، من المهم أن تجد المطابقة الصحيحة بينك وبين أخصائيك. غالبًا ما يبدأ تشخيص أمراض القلب أو الأوعية الدموية بطبيب الرعاية الأولية الذي يحيلك إلى طبيب القلب. يقوم طبيب القلب بتقييم الأعراض والتاريخ الطبي الخاص بك وقد يوصي بإجراء اختبارات للتشخيص أكثر تحديدًا. بعد ذلك ، يقرر طبيب القلب إذا كان من الممكن إدارة حالتك تحت رعايته باستخدام الأدوية أو العلاجات الأخرى المتاحة. إذا قرر طبيب القلب الخاص بك أنك بحاجة لعملية جراحية ، فإنه يحيلك إلى جراح القلب والأوعية الدموية ، الذي يتخصص في العمليات على القلب والرئتين والأوعية الدموية. تظل تحت رعاية طبيب القلب حتى عندما تتم إحالتك إلى أخصائيين آخرين.</p>', 'arabic'),
(15, 8, 'কার্ডিওলজি কি?', '<p>কার্ডিওলোজিস্টরা হ\'ল হৃদরোগ ও রক্তবাহী জাহাজের রোগ বা অবস্থার নির্ণয় ও চিকিত্সার ক্ষেত্রে বিশেষজ্ঞ যারা কার্ডিওভাসকুলার সিস্টেম। আপনি কার্ডিওলজিস্টেরও পরিদর্শন করতে পারেন যাতে হৃদরোগের জন্য আপনার ঝুঁকির কারণগুলি জানতে পারেন এবং হৃদরোগের জন্য আপনি কোন পদক্ষেপ গ্রহণ করতে পারেন তা জানতে পারেন। টেক্সাস হার্ট ইনস্টিটিউট কার্ডিওলজিস্ট পেশাদার কর্মীদের ডিরেক্টরি তালিকাভুক্ত করা হয়।</p>\r\n<p>আপনি যখন হৃদরোগের মতো জটিল স্বাস্থ্যের অবস্থা নিয়ে কাজ করছেন তখন আপনার এবং আপনার বিশেষজ্ঞের মধ্যে সঠিক মিল খুঁজে পাওয়া গুরুত্বপূর্ণ। হার্ট বা ভাস্কুলার রোগের নির্ণয় প্রায়ই আপনার প্রাথমিক যত্ন ডাক্তারের সাথে শুরু হয়, যিনি তখন আপনাকে কার্ডিওলোজিস্ট বলে উল্লেখ করেন। কার্ডিওলজিস্ট আপনার লক্ষণগুলি এবং আপনার চিকিৎসা ইতিহাস মূল্যায়ন করে এবং আরও নির্দিষ্ট নির্ণয়ের জন্য পরীক্ষাগুলি সুপারিশ করতে পারে। তারপরে, আপনার কার্ডিওলোজিস্ট সিদ্ধান্ত নেয় যে ওষুধগুলি বা অন্যান্য উপলব্ধ চিকিত্সাগুলি ব্যবহার করে আপনার অবস্থা তার যত্নের অধীনে পরিচালিত হতে পারে কিনা। আপনার হৃদরোগ বিশেষজ্ঞ যদি সিদ্ধান্ত নেয় যে অস্ত্রোপচারের প্রয়োজন হয় তবে সে আপনাকে কার্ডিওভাসকুলার সার্জনকে নির্দেশ করে, যিনি হৃদয়, ফুসফুস এবং রক্তবাহী পদার্থের অপারেশনের জন্য বিশেষজ্ঞ। আপনি অন্যান্য বিশেষজ্ঞদের উল্লেখ করা হয়, এমনকি যখন আপনি আপনার হৃদরোগ বিশেষজ্ঞের তত্ত্বাবধানে থাকা।</p>', 'bangla'),
(16, 8, 'Qu\'est ce que la cardiologie?', '<p>Les cardiologues sont des médecins spécialisés dans le diagnostic et le traitement de maladies ou d\'affections du cœur et des vaisseaux sanguins - le système cardiovasculaire. Vous pouvez également consulter un cardiologue afin de connaître vos facteurs de risque de maladie cardiaque et de savoir quelles mesures vous pouvez prendre pour améliorer votre santé cardiaque. Les cardiologues du Texas Heart Institute figurent dans le répertoire du personnel professionnel.</p>\r\n<p>Lorsque vous traitez avec un problème de santé complexe comme une maladie cardiaque, il est important que vous trouviez le partenaire idéal entre vous et votre spécialiste. Un diagnostic de maladie cardiaque ou vasculaire commence souvent par votre médecin de famille, qui vous oriente ensuite vers un cardiologue. Le cardiologue évalue vos symptômes et vos antécédents médicaux et peut vous recommander des tests pour un diagnostic plus précis. Ensuite, votre cardiologue décide si votre état peut être soigné sous ses soins à l\'aide de médicaments ou d\'autres traitements disponibles. Si votre cardiologue décide que vous avez besoin d\'une intervention chirurgicale, il vous dirigera vers un chirurgien cardiovasculaire spécialisé dans les opérations du cœur, des poumons et des vaisseaux sanguins. Vous restez sous les soins de votre cardiologue même lorsque vous êtes dirigé vers d\'autres spécialistes.</p>', 'french');

-- --------------------------------------------------------

--
-- Table structure for table `ws_page_content`
--

CREATE TABLE IF NOT EXISTS `ws_page_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `url` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `featured_image` varchar(250) NOT NULL,
  `create_date` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_page_content`
--

INSERT INTO `ws_page_content` (`id`, `menu_id`, `url`, `title`, `description`, `featured_image`, `create_date`, `status`) VALUES
(1, 1, 'home', '', '', '', '2018-11-27', 1),
(2, 2, 'services', '', '', '', '2018-11-27', 1),
(3, 3, 'about', '', '', '', '2018-11-27', 1),
(4, 4, 'doctors', '', '', '', '2018-11-27', 1),
(5, 5, 'departments', '', '', '', '2018-11-27', 1),
(6, 6, 'contact', '', '', '', '2018-11-27', 1),
(8, 9, 'patient_login', 'Patient Login', '<p>fdgdg</p>', '', '2018-11-28', 1),
(34, 14, 'page', '', '', '', '2018-12-20', 1),
(35, 12, 'page', 'We are supported our clients 24 Hours', '<p>We are supported our clients 24 Hours</p>\r\n<p>We are supported our clients 24 Hours</p>\r\n<p>We are supported our clients 24 Hours</p>\r\n<p>We are supported our clients 24 Hours</p>\r\n<p>We are supported our clients 24 Hours</p>\r\n<p> </p>', '', '2018-12-20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_partner`
--

CREATE TABLE IF NOT EXISTS `ws_partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `url` varchar(120) CHARACTER SET utf8 NOT NULL,
  `image` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ws_partner`
--

INSERT INTO `ws_partner` (`id`, `name`, `url`, `image`, `status`) VALUES
(1, 'Apollo Hospital', 'https://apollo.com/', '', 1),
(2, 'BDTask Limited', 'https://bdtask.com/', '', 1),
(3, 'Dhaka Medical College', 'www.dhakamedical.com', '', 1),
(4, 'Test', 'https://testing.com/', '', 1),
(5, 'Medicine', 'www.Medicine.com', '', 1),
(6, 'Pharmacy', 'www.Pharmacy.com', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_section`
--

CREATE TABLE IF NOT EXISTS `ws_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `featured_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_section`
--

INSERT INTO `ws_section` (`id`, `name`, `title`, `description`, `featured_image`) VALUES
(1, 'about', 'About Us', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', ''),
(2, 'contact', 'Contact Us', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', ''),
(3, 'department', 'Department List', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', ''),
(4, 'doctor', 'Doctor List', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', ''),
(5, 'timetable', 'Doctors Timetable', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley', ''),
(6, 'news', 'Latest News', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', ''),
(7, 'benefits', 'Our Benefits', 'Qualified Staff of Doctors\r\nSave Your Money and Time with us\r\n24x7 Emergency service\r\nFeel Like you are at Home Services\r\nFeel Like you are at Home Services', ''),
(8, 'service', 'Our Services', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, ', ''),
(9, 'team', 'Meet our (awesome) team', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', ''),
(10, 'portfolio', 'Jobs & Education', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', NULL),
(11, 'working_hours', 'Our Working Hours', 'It is a long established fact that a reader will.', NULL),
(12, 'signin', 'Try Hospital+ for free', 'We won\'t post anything without your permission and your personal details are kept private', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ws_section_lang`
--

CREATE TABLE IF NOT EXISTS `ws_section_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `section_id` int(6) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_section_lang`
--

INSERT INTO `ws_section_lang` (`id`, `language`, `section_id`, `title`, `description`) VALUES
(1, 'english', 1, 'About Us', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(2, 'bangla', 1, 'আমাদের সম্পর্কে', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে।'),
(3, 'arabic', 1, 'معلومات عنا', 'إنها حقيقة راسخة أن القارئ سوف يصرفه محتوى مقروء للصفحة عندما ينظر إلى تخطيطه.'),
(4, 'french', 1, 'À propos de nous', 'C\'est un fait établi depuis longtemps qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page.'),
(5, 'english', 7, 'Our Benefits', 'Qualified Staff of Doctors\r\nSave Your Money and Time with us\r\n24x7 Emergency service\r\nFeel Like you are at Home Services\r\nFeel Like you are at Home Services'),
(6, 'bangla', 7, 'আমাদের উপকারিতা', 'ডাক্তারের যোগ্য স্টাফ\r\nআমাদের সাথে আপনার টাকা এবং সময় সংরক্ষণ করুন\r\n24x7 জরুরী সেবা\r\nমনে হচ্ছে আপনি হোম সার্ভিসেসে আছেন\r\nমনে হচ্ছে আপনি হোম সার্ভিসেসে আছেন'),
(7, 'arabic', 7, 'فوائدنا', 'طاقم مؤهل من الأطباء\r\nحفظ أموالك والوقت معنا\r\nخدمة الطوارئ على مدار الساعة\r\nأشعر وكأنك في الخدمات المنزلية\r\nأشعر وكأنك في الخدمات المنزلية'),
(8, 'french', 7, 'Nos avantages', 'Personnel qualifié de médecins\r\nÉconomisez votre argent et votre temps avec nous\r\nService d\'urgence 24x7\r\nSentez-vous comme chez vous\r\nSentez-vous comme chez vous'),
(9, 'english', 2, 'Contact Us', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(10, 'french', 2, 'Contactez nous', 'C\'est un fait établi depuis longtemps qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page.'),
(11, 'arabic', 2, 'اتصل بنا', 'إنها حقيقة راسخة أن القارئ سوف يصرفه محتوى مقروء للصفحة عندما ينظر إلى تخطيطه.'),
(12, 'bangla', 2, 'আমাদের সাথে যোগাযোগ করুন', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে।'),
(13, 'french', 3, 'Liste des départements', 'C\'est un fait établi depuis longtemps qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page.'),
(14, 'arabic', 3, 'قائمة الإدارات', 'إنها حقيقة راسخة أن القارئ سوف يصرفه محتوى مقروء للصفحة عندما ينظر إلى تخطيطه.'),
(15, 'bangla', 3, 'বিভাগ তালিকা', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে।'),
(16, 'english', 3, 'Department List', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(17, 'english', 4, 'Doctor List', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(18, 'french', 4, 'liste de docteur', 'C\'est un fait établi depuis longtemps qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page.'),
(19, 'bangla', 4, 'ডাক্তার তালিকা', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে।'),
(20, 'arabic', 4, 'قائمة الطبيب', 'إنها حقيقة راسخة أن القارئ سوف يصرفه محتوى مقروء للصفحة عندما ينظر إلى تخطيطه.'),
(21, 'english', 6, 'Latest News', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(22, 'bangla', 6, 'সর্বশেষ সংবাদ', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে।'),
(23, 'arabic', 6, 'أحدث الأخبار', 'إنها حقيقة راسخة أن القارئ سوف يصرفه محتوى مقروء للصفحة عندما ينظر إلى تخطيطه.'),
(24, 'french', 6, 'Dernières nouvelles', 'C\'est un fait établi depuis longtemps qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page.'),
(25, 'english', 10, 'Jobs & Education', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(26, 'bangla', 10, 'চাকরি ও শিক্ষা', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে।'),
(27, 'arabic', 10, 'الوظائف والتعليم', 'إنها حقيقة راسخة أن القارئ سوف يصرفه محتوى مقروء للصفحة عندما ينظر إلى تخطيطه.'),
(28, 'french', 10, 'Emplois & Education', 'C\'est un fait établi depuis longtemps qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page.'),
(29, 'english', 8, 'Our Services', 'The NWT Health Care Plan covers the cost of medically necessary hospital services, provided at a hospital, on an inpatient or outpatient basis within Canada. '),
(30, 'bangla', 8, 'আমাদের সেবাসমূহ', 'এনডব্লিউটিএ হেলথ কেয়ার প্ল্যানটি কানাডার অভ্যন্তরে একটি ইনসেন্টেন্ট বা আউটপেইটিভ ভিত্তিতে হাসপাতালে দেওয়া ঔষধগত প্রয়োজনীয় হাসপাতালের খরচগুলি জুড়ে দেয়।'),
(31, 'arabic', 8, 'خدماتنا', 'تغطي خطة الرعاية الصحية بشبكة NWT تكلفة خدمات المستشفيات الضرورية طبياً ، والتي يتم توفيرها في المستشفى ، على أساس المرضى الداخليين أو خارج المستشفى داخل كندا.'),
(32, 'french', 8, 'Nos services', 'Le régime de soins de santé des Territoires du Nord-Ouest couvre le coût des services hospitaliers médicalement nécessaires, en milieu hospitalier ou en consultation externe au Canada.'),
(33, 'english', 9, 'Meet our (awesome) team', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'),
(34, 'french', 9, 'Rencontrez notre (génial) équipe', 'C\'est un fait établi depuis longtemps qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page.'),
(35, 'arabic', 9, 'تعرف على فريقنا (الرائع)', 'إنها حقيقة راسخة أن القارئ سوف يصرفه محتوى مقروء للصفحة عندما ينظر إلى تخطيطه.'),
(36, 'bangla', 9, 'আমাদের এক্সপার্টদের সাথে দেখা করুন ', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে।'),
(37, 'french', 5, 'Horaires des médecins', 'Vous trouverez ci-dessous des conseils qui vous aideront à planifier votre rendez-vous avec un médecin ou une infirmière de votre choix.'),
(38, 'english', 5, 'Doctors Timetable', 'The following is for guidance only to help you plan your appointment with a preferred doctor or nurse.'),
(39, 'arabic', 5, 'جدول الأطباء', 'ما يلي هو للإرشاد فقط لمساعدتك في تخطيط موعدك مع طبيب أو ممرضة مفضلة.'),
(40, 'bangla', 5, 'ডাক্তারের সময়সীমা', 'নিচের দিক নির্দেশনা শুধুমাত্র আপনাকে পছন্দসই ডাক্তার বা নার্সের সাথে আপনার অ্যাপয়েন্টমেন্ট পরিকল্পনা করতে সহায়তা করার জন্য।'),
(41, 'english', 11, 'Our Working Hours', 'It is a long established fact that a reader will.'),
(42, 'french', 11, 'Nos heures de travail', 'C\'est un fait établi depuis longtemps qu\'un lecteur le fera.'),
(43, 'arabic', 11, 'ساعات العمل لدينا', 'إنها حقيقة ثابتة منذ زمن طويل أن القارئ سوف.'),
(44, 'bangla', 11, 'আমাদের কাজের ঘন্টা', 'এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যে একটি পাঠক হবে।');

-- --------------------------------------------------------

--
-- Table structure for table `ws_service`
--

CREATE TABLE IF NOT EXISTS `ws_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `name` varchar(20) NOT NULL,
  `icon_image` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text DEFAULT NULL,
  `position` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `date` date DEFAULT NULL,
  `is_parent` int(11) NOT NULL COMMENT '0=Parent',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_service`
--

INSERT INTO `ws_service` (`id`, `language`, `name`, `icon_image`, `title`, `description`, `position`, `status`, `date`, `is_parent`) VALUES
(1, 'english', 'Bdtask ', '', 'Bdtask Hospital Limited', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1, 1, '2019-01-26', 0),
(3, 'english', 'Emergency  ', '', '24 hours Emergency services health', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 2, 1, '2019-01-26', 0),
(4, 'french', 'Emergency  ', '', 'services d\'urgence santé', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 2, 1, '2019-01-26', 3),
(5, 'arabic', 'حالة طوارئ', '', 'خدمات الطوارئ الصحية', '<div>\r\n<h2>What is Lorem Ipsum?</h2>\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n</div>\r\n<div>\r\n<h2>Why do we use it?</h2>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n</div>', 2, 1, '2019-01-26', 3),
(6, 'bangla', 'জরুরি অবস্থা', '', 'জরুরী সেবা স্বাস্থ্য', '<p>Lorem ipsum কি?<br>Lorem Ipsum কেবল মুদ্রণ এবং টাইপসটিং শিল্পের ডামি টেক্সট। Lorem Ipsum 1500 সাল থেকে শিল্পের আদর্শ ডামি পাঠ্যক্রম হয়েছে, যখন একটি অজানা প্রিন্টার একটি গলি টাইপ নেয় এবং এটি একটি টাইপ নমুনা বই তৈরি করতে স্ক্যাম্বল করে। এটি কেবল পাঁচ শতাব্দীই বেঁচে নেই, তবে ইলেকট্রনিক টাইপসেটিংয়েও লাফ, অপরিহার্যভাবে অপরিবর্তিত থাকবে। লরেম ইপ্সাম প্যাসেজ সম্বলিত লেটারাসেট শীট প্রকাশের সাথে 1960 এর দশকে এটি জনপ্রিয় হয়ে ওঠে এবং সম্প্রতি লরেম ইপসুমের সংস্করণ সহ অ্যালডাস পেজমেকারের মতো ডেস্কটপ প্রকাশনা সফটওয়্যারের সাথে এটি জনপ্রিয় হয়।</p>\r\n<p>আমরা কেন এটা ব্যবহার করি?<br>এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে। লোরম ইপসাম ব্যবহার করার বিষয়টি হল \'এখানে সামগ্রী, এখানে সামগ্রী\' ব্যবহার করার বিরোধিতা করে এটির অক্ষরগুলির কম বা কম সাধারণ বন্টন রয়েছে, যা এটি পাঠযোগ্য ইংরাজী রূপে পরিণত হয়। অনেক ডেস্কটপ পাবলিশিং প্যাকেজ এবং ওয়েব পেইজ এডিটরগুলি এখন তাদের ডিফল্ট মডেল পাঠ্য হিসাবে লোরম ইম্পসাম ব্যবহার করে এবং \'লোরেম ইপুসুম\' এর অনুসন্ধানের জন্য তাদের অনুসন্ধানগুলি এখনও তাদের বাচ্চাদের মধ্যে অনেকগুলি ওয়েব সাইট উন্মোচিত করবে। বিভিন্ন সংস্করণ বছর ধরে, কখনও কখনও দুর্ঘটনা দ্বারা, কখনও কখনও উদ্দেশ্য (হাস্যরস এবং অনুরূপ ইনজেকশন) উপর বিকাশ হয়েছে।</p>', 2, 1, '2019-01-26', 3),
(7, 'bangla', 'Bdtask ', '', 'বিডকাস হাসপাতাল লি', '<p>আমরা কেন এটা ব্যবহার করি?<br>এটি একটি দীর্ঘ প্রতিষ্ঠিত সত্য যা একটি পাঠক তার লেআউটটি দেখতে যখন পৃষ্ঠাটির পাঠযোগ্য সামগ্রী দ্বারা বিভ্রান্ত হবে। লোরম ইপসাম ব্যবহার করার বিষয়টি হল \'এখানে সামগ্রী, এখানে সামগ্রী\' ব্যবহার করার বিরোধিতা করে এটির অক্ষরগুলির কম বা কম সাধারণ বন্টন রয়েছে, যা এটি পাঠযোগ্য ইংরাজী রূপে পরিণত হয়। অনেক ডেস্কটপ পাবলিশিং প্যাকেজ এবং ওয়েব পেইজ এডিটরগুলি এখন তাদের ডিফল্ট মডেল পাঠ্য হিসাবে লোরম ইম্পসাম ব্যবহার করে এবং \'লোরেম ইপুসুম\' এর অনুসন্ধানের জন্য তাদের অনুসন্ধানগুলি এখনও তাদের বাচ্চাদের মধ্যে অনেকগুলি ওয়েব সাইট উন্মোচিত করবে। বিভিন্ন সংস্করণ বছর ধরে, কখনও কখনও দুর্ঘটনা দ্বারা, কখনও কখনও উদ্দেশ্য (হাস্যরস এবং অনুরূপ ইনজেকশন) উপর বিকাশ হয়েছে।</p>', 1, 1, '2019-01-26', 1),
(8, 'arabic', 'Bdtask ', '', 'مستشفى Bdtask المحدودة', '<p>لماذا نستخدمه؟</p>\r\n<p>من الحقائق الثابتة منذ زمن بعيد أن القارئ سوف يصرفه المحتوى القابل للقراءة لصفحة عند النظر إلى تصميمه. إن الهدف من استخدام لوريم إيبسوم هو أن لديه توزيعًا عاديًا أو أكثر للرسائل ، بدلاً من استخدام \"المحتوى هنا ، المحتوى هنا\" ، مما يجعله يبدو وكأنه قابل للقراءة باللغة الإنجليزية. تستخدم الآن العديد من حزم النشر المكتبي ومحرري صفحات الويب Lorem Ipsum كنص نموذج افتراضي ، وسيكشف البحث عن \'lorem ipsum\' عن العديد من مواقع الويب التي لا تزال في مهدها. تطورت الإصدارات المختلفة على مر السنين ، وأحيانًا عن طريق الصدفة ، وأحيانًا عن قصد (الفكاهة المحقونة وما شابه).</p>', 1, 1, '2019-01-26', 1),
(9, 'french', 'Bdtask ', '', 'Bdtask Hospital Limited', '<p>Pourquoi l\'utilisons-nous?<br>C\'est un fait établi de longue date qu\'un lecteur sera distrait par le contenu lisible d\'une page lorsqu\'il examinera sa mise en page. Lorem Ipsum a l\'avantage de présenter une distribution de lettres plus ou moins normale, par opposition à l\'utilisation de \'Contenu ici, contenu ici\', ce qui donne un aspect lisible en anglais. De nombreux packages de publication assistée par ordinateur et éditeurs de pages Web utilisent maintenant Lorem Ipsum comme texte de modèle par défaut, et une recherche sur \'lorem ipsum\' permet de découvrir de nombreux sites Web encore à leurs balbutiements. Diverses versions ont évolué au fil des ans, parfois par accident, parfois exprès (humour injecté, etc.).</p>', 1, 1, '2019-01-26', 1),
(10, 'english', 'ODP', '', 'Operating department practitioner', '<h3><strong><a id=\"surgical phase\" name=\"surgical phase\"></a>The surgical phase</strong></h3>\r\n<p>You’ll play an important part of the surgery team and will:</p>\r\n<ul>\r\n<li>prepare all the necessary instruments and equipment for the operations, including microscopes, lasers and endoscopes</li>\r\n<li>provide the correct surgical instruments and materials to the surgeon</li>\r\n<li>be responsible for surgical instruments, equipment and swabs during the operation</li>\r\n<li>act as a link between the surgical team and other parts of the theatre and hospital</li>\r\n<li>anticipate the requirements of the surgical team and respond effectively</li>\r\n</ul>\r\n<h3><a id=\"recovery phase\" name=\"recovery phase\"></a><strong>The recovery phase</strong></h3>\r\n<p>You’ll:</p>\r\n<ul>\r\n<li>support the patient on their arrival into the recovery unit</li>\r\n<li>monitor a patient’s physiological parameters</li>\r\n<li>provide appropriate treatment until the patient has recovered from the effects of the anaesthesia and/or surgery</li>\r\n<li>assess the patient in order to ensure they can be discharged back to a ward</li>\r\n<li>evaluate the care given during each phase</li>\r\n</ul>\r\n<h2>Where do ODPs work?</h2>\r\n<p>You’ll be primarily employed within operating theatres but increasingly in other critical care areas of a hospital.</p>\r\n<p>ODPs also manage the preparation of the environment, equipment and act as the link between the surgical team and other parts of the operating theatre and hospital. They must be able to anticipate the requirements of the surgical team and respond effectively.</p>', 3, 1, '2019-01-26', 0),
(11, 'french', 'ODP', '', 'Praticien de service d\'exploitation', '<p>La phase chirurgicale<br>Vous jouerez un rôle important dans l\'équipe de chirurgie et:</p>\r\n<p>préparer tous les instruments et l\'équipement nécessaires aux opérations, y compris les microscopes, les lasers et les endoscopes<br>fournir les instruments et le matériel chirurgicaux appropriés au chirurgien<br>être responsable des instruments chirurgicaux, du matériel et des écouvillons pendant l\'opération<br>servir de lien entre l\'équipe chirurgicale et d\'autres parties du théâtre et de l\'hôpital<br>anticiper les besoins de l\'équipe chirurgicale et y répondre efficacement<br>La phase de récupération<br>Vous allez:</p>\r\n<p>soutenir le patient à son arrivée dans l\'unité de récupération<br>surveiller les paramètres physiologiques du patient<br>fournir un traitement approprié jusqu\'à la guérison du patient des effets de l\'anesthésie et / ou de la chirurgie<br>évaluer le patient afin de s\'assurer qu\'il peut être renvoyé dans un service<br>évaluer les soins donnés à chaque phase<br>Où travaillent les ODP?<br>Vous travaillerez principalement dans les salles d\'opération, mais de plus en plus dans d\'autres secteurs de soins critiques d\'un hôpital.</p>\r\n<p>Les ODP gèrent également la préparation de l\'environnement et des équipements et servent de lien entre l\'équipe chirurgicale et d\'autres parties du bloc opératoire et de l\'hôpital. Ils doivent pouvoir anticiper les besoins de l\'équipe chirurgicale et réagir efficacement.</p>', 3, 1, '2019-01-26', 10),
(12, 'arabic', 'ODP', '', 'ممارس قسم التشغيل', '<p>المرحلة الجراحية</p>\r\n<p>سيلعب جزءًا مهمًا من فريق الجراحة وسيقوم بما يلي:</p>\r\n<p>إعداد جميع الأدوات والمعدات اللازمة للعمليات ، بما في ذلك المجاهر والليزر والمناظير<br>توفير الأدوات الجراحية والمواد الصحيحة للجراح<br>تكون مسؤولة عن الأدوات الجراحية والمعدات والمسحات أثناء العملية<br>بمثابة صلة بين الفريق الجراحي وأجزاء أخرى من المسرح والمستشفى<br>توقع متطلبات الفريق الجراحي والاستجابة بفعالية<br>مرحلة الانتعاش<br>عليك:</p>\r\n<p>دعم المريض عند وصوله إلى وحدة الاسترداد<br>مراقبة المعلمات الفسيولوجية للمريض<br>توفير العلاج المناسب حتى يسترد المريض من آثار التخدير و / أو الجراحة<br>تقييم المريض من أجل ضمان إعادته إلى الجناح<br>تقييم الرعاية المقدمة خلال كل مرحلة<br>أين تعمل ODP؟<br>ستعمل بشكل أساسي في غرف العمليات ، ولكن بشكل متزايد في مناطق الرعاية الحرجة الأخرى في المستشفى.</p>\r\n<p>كما تقوم إدارة ODPs بإعداد البيئة والمعدات والعمل كحلقة وصل بين الفريق الجراحي وأجزاء أخرى من غرفة العمليات والمستشفى. يجب أن يكونوا قادرين على توقع متطلبات فريق الجراحة والاستجابة بفعالية.</p>', 3, 1, '2019-01-26', 10),
(13, 'bangla', 'ODP', '', 'অপারেটিং বিভাগের অনুশীলনকারী', '<p>অস্ত্রোপচার ফেজ<br>আপনি অস্ত্রোপচার দলের একটি গুরুত্বপূর্ণ অংশ খেলতে হবে এবং হবে:</p>\r\n<p>মাইক্রোস্কোপ, লেজার এবং এন্ডোসকোপ সহ অপারেশনগুলির জন্য প্রয়োজনীয় সমস্ত সরঞ্জাম এবং সরঞ্জাম প্রস্তুত করুন<br>সার্জন সঠিক অস্ত্রোপচার যন্ত্র এবং উপকরণ প্রদান<br>অপারেশন সময় অস্ত্রোপচার যন্ত্র, সরঞ্জাম এবং swabs জন্য দায়ী হতে হবে<br>অস্ত্রোপচার দল এবং থিয়েটার এবং হাসপাতালে অন্যান্য অংশ মধ্যে একটি লিঙ্ক হিসাবে কাজ<br>অস্ত্রোপচার দলের প্রয়োজনীয়তা আশা এবং কার্যকরভাবে সাড়া<br>পুনরুদ্ধারের ফেজ<br>আপনি যা করবেন:</p>\r\n<p>পুনরুদ্ধার ইউনিট তাদের আগমনের উপর রোগীর সমর্থন<br>রোগীর শারীরিক পরামিতি নিরীক্ষণ<br>রোগী অ্যানেস্থেশিয়া এবং / অথবা অস্ত্রোপচারের প্রভাব থেকে পুনরুদ্ধার না হওয়া পর্যন্ত যথাযথ চিকিৎসা প্রদান করে<br>তারা একটি ওয়ার্ড ফিরে discharged করা যাবে তা নিশ্চিত করার জন্য রোগীর মূল্যায়ন<br>প্রতিটি পর্যায়ে দেওয়া যত্ন মূল্যায়ন<br>কোথায় ওডিপি কাজ করে?<br>আপনি প্রাথমিকভাবে অপারেটিং থিয়েটার মধ্যে নিযুক্ত করা হবে কিন্তু একটি হাসপাতালে অন্যান্য গুরুত্বপূর্ণ যত্ন এলাকায় ক্রমবর্ধমান।</p>\r\n<p>ওডিপিগুলি অস্ত্রোপচার দল এবং অপারেটিং থিয়েটার ও হাসপাতালের অন্যান্য অংশগুলির মধ্যে সংযোগ হিসাবে পরিবেশ, সরঞ্জাম এবং কর্মের প্রস্তুতি পরিচালনা করে। তারা অস্ত্রোপচার দলের প্রয়োজনীয়তা পূর্বাভাস এবং কার্যকরভাবে সাড়া দিতে সক্ষম হতে হবে।</p>', 3, 1, '2019-01-26', 10);

-- --------------------------------------------------------

--
-- Table structure for table `ws_setting`
--

CREATE TABLE IF NOT EXISTS `ws_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `meta_tag` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `text` varchar(25) NOT NULL,
  `fax` varchar(25) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `closed_day` varchar(30) NOT NULL,
  `open_day` varchar(30) NOT NULL,
  `copyright_text` varchar(255) DEFAULT NULL,
  `working_hour` text NOT NULL,
  `isQRCode` tinyint(1) NOT NULL COMMENT '1=active and 0=inactive',
  `status` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_setting`
--

INSERT INTO `ws_setting` (`id`, `language`, `title`, `description`, `meta_tag`, `meta_keyword`, `phone`, `text`, `fax`, `address`, `closed_day`, `open_day`, `copyright_text`, `working_hour`, `isQRCode`, `status`) VALUES
(1, 'english', 'Bdtask Hospital Limited', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. ', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. ', '                              Hospital, Appointment, System, \r\nHospital Appointment,Demo, Appointment', '(012)-345-6788, (022)-143-545-45, (011)-122-244-45', '(123) 456-7890', ' (123) 456-7890', '123/A, Street, State-12345, Demo', 'Saturday and Sunday', 'MON - FRI: 08:00AM - 20:00PM', '<p>Copyright &copy; 2016- <a title=\"BDtask\" href=\"http://www.bdtask.com\" target=\"_blank\">BDtask</a>&nbsp;.&nbsp;All rights reserved.</p>', '<table class=\"table\">\r\n                        <tbody>\r\n                            <tr>\r\n                                <td>Monday-Wednesday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Thursday -Friday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Saturday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Sunday</td>\r\n                                <td class=\"text-right\">Closed</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>', 1, 1),
(2, 'bangla', 'বিডি টাস্ক হাসপাতাল লি :', '', '', '      ', '৫৭৪৫৭৪৫৭, ৫৬৭৫৬৭৫৬,  ৫৭৬৬৭৫৬৫', '৫৭৬৬৭৫৬৫', '৫৬৭৫৬৭৫৬', 'Uttara, Dhaka-1230', '', '', '<p>কপিরাইট &copy; ২০১৬ - <a href=\"http://www.bdtask.com\" target=\"_blank\">বিডিস্ক</a>। সর্বস্বত্ব সংরক্ষিত.</p>', '<table class=\"table\">\r\n                        <tbody>\r\n                            <tr>\r\n                                <td>সোমবার - বুধবার</td>\r\n                                <td class=\"text-right\">০৮.০০-১৮.০০</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>বৃহস্পতিবার - শুক্রবার</td>\r\n                                <td class=\"text-right\">০৮.০০-১৮.০০</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>শনিবার</td>\r\n                                <td class=\"text-right\">০৮.০০-১৮.০০</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>রবিবার</td>\r\n                                <td class=\"text-right\">বন্ধ</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>', 1, 1),
(3, 'arabic', 'مستشفى Bdtask المحدودة', 'xc', 'vx', '  xcz', '(012)-345-6788, (022)-143-545-45, (011)-122-244-45', '(123) 456-7890', '(123) 456-7890', '123 / A، Street، State-12345، Demo', '', '', '<p>Copyright &copy; 2016-&nbsp;<a title=\"BDtask\" href=\"http://www.bdtask.com\" target=\"_blank\">BDtask</a>&nbsp;.&nbsp;All rights reserved.</p>', '<table class=\"table\">\r\n                        <tbody>\r\n                            <tr>\r\n                                <td>Monday-Wednesday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Thursday -Friday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Saturday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Sunday</td>\r\n                                <td class=\"text-right\">Closed</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>', 1, 1),
(4, 'french', 'Bdtask Hospital Limited', 'b', 'bvc', '   vbc', '(012)-345-6788, (022)-143-545-45, (011)-122-244-45', '(123) 456-7890', '(123) 456-7890', 'vxcv', '', '', '<p>Copyright &copy; 2016-&nbsp;<a title=\"BDtask\" href=\"http://www.bdtask.com\" target=\"_blank\">BDtask</a>&nbsp;.&nbsp;All rights reserved.</p>', '<table class=\"table\">\r\n                        <tbody>\r\n                            <tr>\r\n                                <td>Monday-Wednesday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Thursday -Friday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Saturday</td>\r\n                                <td class=\"text-right\">08.00-18.00</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td>Sunday</td>\r\n                                <td class=\"text-right\">Closed</td>\r\n                            </tr>\r\n                        </tbody>\r\n                    </table>', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_slider`
--

CREATE TABLE IF NOT EXISTS `ws_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(120) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_slider`
--

INSERT INTO `ws_slider` (`id`, `title`, `url`, `image`, `position`, `status`) VALUES
(1, 'A Hospital Information System &#40;HIS&#41; basically is a synonym ', 'https://stackoverflow.com/', '', 3, 1),
(2, 'Generally, the system should be safe and secure from a data management', 'https://stackoverflow.com/', '', 2, 1),
(5, 'Introducing a new information system', 'https://www.usahealthsystem.com/usacwh', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ws_slider_lang`
--

CREATE TABLE IF NOT EXISTS `ws_slider_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(5) NOT NULL,
  `language` varchar(15) NOT NULL,
  `title` varchar(120) NOT NULL,
  `subtitle` varchar(150) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_slider_lang`
--

INSERT INTO `ws_slider_lang` (`id`, `slider_id`, `language`, `title`, `subtitle`, `description`) VALUES
(1, 1, 'english', 'A Hospital Information System &#40;HIS&#41; basically  is a synonym for information management ', 'A Hospital Information System &#40;HIS&#41; basically  is a synonym for information management', '<p>A Hospital Information System (HIS) basically&nbsp; is a synonym for information management system at use in hospitals. Hospitals generate a wealth of data round the clock, 365 days a year, all of which needs to be well managed to ensure efficient functioning. Patients visit such establishments for outpatient care, in an emergency, or get admitted for either a short stay (a few hours) or long in duration (that may sometimes be indefinite). While in the past, the important modules of an HIS tended to be those that dealt with revenue management aspects, the recent trend sees a growing emphasis on improving overall efficiency and clinical management. It must be noted here that while some modules are required by all clinical establishments, requirement for others depend on the size of and specialities covered by a particular establishment.</p>'),
(2, 1, 'bangla', 'একটি হাসপাতাল ইনফরমেশন সিস্টেম (এইচআইএস) মূলত হাসপাতালের ব্যবহারের তথ্য তথ্য সিস্টেমের সমার্থক। ', 'একটি হাসপাতাল ইনফরমেশন সিস্টেম (এইচআইএস) মূলত হাসপাতালের ব্যবহারের তথ্য তথ্য সিস্টেমের সমার্থক। ', '<p>একটি হাসপাতাল ইনফরমেশন সিস্টেম (এইচআইএস) মূলত হাসপাতালের ব্যবহারের তথ্য তথ্য সিস্টেমের সমার্থক। হাসপাতাল ঘড়ি ঘন ঘন তথ্য, একটি বছরে 365 দিন উত্পাদন করে, যা সব দক্ষ কার্যকারিতা নিশ্চিত করতে ভাল পরিচালিত করা প্রয়োজন। রোগীরা আউটসেন্টেন্টের যত্নের জন্য জরুরি অবস্থার জন্য এই ধরনের সংস্থান পরিদর্শন করে, অথবা একটি সংক্ষিপ্ত থাকার (কয়েক ঘন্টা) বা দীর্ঘ সময়কালের জন্য ভর্তি হয় (যা কখনও কখনও অনির্দিষ্ট হতে পারে)। অতীতের সময়ে, একটি এইচআইএসের গুরুত্বপূর্ণ মডিউলগুলি তাদের উপার্জন পরিচালনার দিকগুলির সাথে মোকাবিলা করেছিল বলে মনে করা হয়, সাম্প্রতিক প্রবণতা সামগ্রিক দক্ষতা এবং ক্লিনিকাল পরিচালনার উন্নতিতে ক্রমবর্ধমান জোর দেয়। এখানে অবশ্যই উল্লেখ করা উচিত যে কিছু ক্লিনিকাল সংস্থার দ্বারা কিছু মডিউল প্রয়োজন হলে, অন্যের জন্য প্রয়োজনীয় একটি বিশেষ সংস্থার আওতায় থাকা এবং বিশেষত্বের উপর নির্ভর করে।</p>'),
(3, 1, 'arabic', ' هو في الأساس مرادف لنظام إدارة المعلومات المستخدم في المستشفيات. تولد المستشفيات ثروة من البيانات ع', ' هو في الأساس مرادف لنظام إدارة المعلومات المستخدم في المستشفيات. تولد المستشفيات ثروة من البيانات ع', '<p>نظام معلومات المستشفى (HIS) هو في الأساس مرادف لنظام إدارة المعلومات المستخدم في المستشفيات. تولد المستشفيات ثروة من البيانات على مدار الساعة ، 365 يومًا في السنة ، وكلها بحاجة إلى إدارة جيدة لضمان الأداء الفعال. ﯾزور اﻟﻣرﺿﯽ ھذه اﻟﻣؤﺳﺳﺎت ﻟﻟرﻋﺎﯾﺔ اﻟﺧﺎرﺟﯾﺔ ، أو ﻓﻲ ﺣﺎﻟﺔ اﻟطوارئ ، أو ﯾﺟب أن ﯾﺣﺻﻟوا ﻋﻟﯽ إﻗﺎﻣﺔ ﻗﺻﯾرة (ﺳﺎﻋﺎت ﻗﻟﯾﻟﺔ) أو طوﯾﻟﺔ اﻟﻣدة (اﻟﺗﻲ ﻗد ﺗﮐون ﻓﻲ ﺑﻌض اﻷﺣﯾﺎن ﻏﯾر ﻣﺣددة). بينما في الماضي ، كانت الوحدات الهامة في نظام HIS هي تلك التي تعاملت مع جوانب إدارة الإيرادات ، إلا أن الاتجاه الأخير يرى تركيزًا متزايدًا على تحسين الكفاءة الكلية والإدارة السريرية. وتجدر الإشارة هنا إلى أنه في حين أن بعض الوحدات الطبية مطلوبة من قبل جميع المؤسسات ، فإن متطلبات الآخرين تعتمد على حجم وتخصصات مؤسسة معينة.</p>'),
(4, 1, 'french', 'Un système d\'information hospitalier (HIS) est fondamentalement synonyme de système de', 'Un système d\'information hospitalier (HIS) est fondamentalement synonyme de système de', '<p>Un syst&egrave;me d\'information hospitalier (HIS) est fondamentalement synonyme de syst&egrave;me de gestion de l\'information utilis&eacute; dans les h&ocirc;pitaux. Les h&ocirc;pitaux g&eacute;n&egrave;rent une multitude de donn&eacute;es 24 heures sur 24, 365 jours par an, qui doivent tous &ecirc;tre bien g&eacute;r&eacute;s pour assurer un fonctionnement efficace. Les patients se rendent dans ces &eacute;tablissements pour des soins ambulatoires, en cas d\'urgence ou sont admis pour un s&eacute;jour de courte dur&eacute;e (quelques heures) ou de longue dur&eacute;e (parfois avec une dur&eacute;e ind&eacute;termin&eacute;e). Alors que par le pass&eacute;, les modules importants d\'un syst&egrave;me d\'information de sant&eacute; &eacute;taient g&eacute;n&eacute;ralement ceux qui traitaient de la gestion des revenus, la tendance r&eacute;cente met de plus en plus l\'accent sur l\'am&eacute;lioration de l\'efficacit&eacute; globale et de la gestion clinique. Il convient de noter ici que, si certains modules sont requis par tous les &eacute;tablissements cliniques, ceux-ci d&eacute;pendent de la taille et des sp&eacute;cialit&eacute;s couvertes par un &eacute;tablissement donn&eacute;.</p>'),
(5, 2, 'english', 'General Requirements– An Overview', 'Generally, the system should be safe and secure from a data management point-of-view.', '<p>General Requirements&ndash; An Overview<br />Generally, the system should be safe and secure from a data management point-of-view. Highly sensitive data is handled by such systems and hence the comfort-level related to privacy and safety issues need to be addressed aggressively. The system should ensure efficient flow of information that provides interdepartmental support to the establishment, functional and process integration, be adaptable and flexible from a user perspective, and last, but not the least, be standards-based to ensure interoperability in terms of syntactic, semantic and process.</p>'),
(6, 2, 'french', 'Exigences générales - Un aperçu', 'En règle générale, le système doit être sûr et sécurisé du point de vue de la gestion des données.', '<p>Exigences g&eacute;n&eacute;rales - Un aper&ccedil;u<br />En r&egrave;gle g&eacute;n&eacute;rale, le syst&egrave;me doit &ecirc;tre s&ucirc;r et s&eacute;curis&eacute; du point de vue de la gestion des donn&eacute;es. Les donn&eacute;es hautement sensibles sont trait&eacute;es par de tels syst&egrave;mes et par cons&eacute;quent, le niveau de confort li&eacute; aux questions de confidentialit&eacute; et de s&eacute;curit&eacute; doit &ecirc;tre trait&eacute; de mani&egrave;re agressive. Le syst&egrave;me doit assurer une circulation efficace de l&rsquo;information qui assure un soutien interminist&eacute;riel &agrave; l&rsquo;&eacute;tablissement, une int&eacute;gration fonctionnelle et des processus, &ecirc;tre adaptable et flexible du point de vue de l&rsquo;utilisateur, et enfin et surtout, &ecirc;tre fond&eacute; sur des normes garantissant l&rsquo;interop&eacute;rabilit&eacute; en termes s&eacute;mantique et processus.</p>'),
(7, 2, 'arabic', 'المتطلبات العامة - نظرة عامة', 'بشكل عام ، يجب أن يكون النظام آمنًا ومأمونًا من وجهة نظر إدارة البيانات.', '<p>المتطلبات العامة - نظرة عامة<br />بشكل عام ، يجب أن يكون النظام آمنًا ومأمونًا من وجهة نظر إدارة البيانات. يتم التعامل مع البيانات الحساسة للغاية من قبل هذه الأنظمة ومن ثم يجب التعامل مع مستوى الراحة المتعلق بقضايا الخصوصية والسلامة بشكل مكثف. ينبغي أن يضمن النظام التدفق الفعال للمعلومات التي توفر الدعم المشترك بين الإدارات للتأسيس ، والتكامل الوظيفي والعملي ، وتكون قابلة للتكيف ومرنة من وجهة نظر المستخدم ، وأخيراً ، وليس أقلها ، تعتمد على المعايير لضمان قابلية التشغيل البيني من حيث التركيب النحوي ، الدلالي وعملية.</p>'),
(8, 2, 'bangla', 'সাধারণ প্রয়োজন - একটি সংক্ষিপ্ত বিবরণ', 'সাধারণত, সিস্টেমটি ডেটা ম্যানেজমেন্ট পয়েন্ট অফ ভিউ থেকে নিরাপদ এবং সুরক্ষিত হওয়া উচিত।', '<p>সাধারণ প্রয়োজন - একটি সংক্ষিপ্ত বিবরণ<br />সাধারণত, সিস্টেমটি ডেটা ম্যানেজমেন্ট পয়েন্ট অফ ভিউ থেকে নিরাপদ এবং সুরক্ষিত হওয়া উচিত। অত্যন্ত সংবেদনশীল তথ্য যেমন সিস্টেম দ্বারা পরিচালিত হয় এবং অতএব গোপনীয়তা এবং নিরাপত্তা বিষয়গুলির সাথে সান্ত্বনা স্তরের আক্রমণাত্মকভাবে মোকাবেলা করা প্রয়োজন। সিস্টেমটি কার্যকর দক্ষতা প্রবাহ নিশ্চিত করবে যা সংস্থান, কার্যকরী এবং প্রক্রিয়া ইন্টিগ্রেশনকে আন্তঃবিভাগীয় সহায়তা প্রদান করে, ব্যবহারকারীর দৃষ্টিকোণ থেকে অভিযোজিত এবং নমনীয় হতে পারে এবং অন্তত নয়, অন্তত, সিন্ট্যাকটিকের ক্ষেত্রে আন্তঃব্যবস্থা নিশ্চিত করার জন্য মান-ভিত্তিক হতে পারে। শব্দার্থিক এবং প্রক্রিয়া।</p>'),
(9, 5, 'bangla', 'একটি নতুন তথ্য সিস্টেম চালু করা হচ্ছে', 'যেখানে একটি ইতিমধ্যেই রয়েছে এবং সক্রিয়ভাবে ব্যবহৃত হচ্ছে', '<p>একটি নতুন তথ্য সিস্টেম চালু করা হচ্ছে, যেখানে একটি ইতিমধ্যেই রয়েছে এবং সক্রিয়ভাবে ব্যবহৃত হচ্ছে, পরিবর্তন ব্যবস্থার একটি নির্দিষ্ট ডিগ্রী প্রয়োজন কারণ এই নতুন সিস্টেমটি কাজ করার একটি \'নতুন উপায়\' প্রতিফলিত করে।</p>'),
(10, 5, 'english', 'Introducing a new information system', 'where one is already in place and is actively used', '<p>Introducing a new information system, where one is already in place and is actively used, requires a certain degree of change management as this new system reflects a &lsquo;new way&rsquo; of working.</p>'),
(11, 5, 'arabic', 'يتطلب تقديم نظام معلومات جديد ', 'حيث يوجد بالفعل موضع التطبيق ويستخدم بنشاط', '<p>يتطلب تقديم نظام معلومات جديد ، حيث يوجد بالفعل موضع التطبيق ويستخدم بنشاط ، درجة معينة من إدارة التغيير حيث يعكس هذا النظام الجديد \"طريقة جديدة\" للعمل.</p>'),
(12, 5, 'french', 'L’introduction d’un nouveau système d’information', ' là où un système existe déjà et est activement utilisé', '<p>L&rsquo;introduction d&rsquo;un nouveau syst&egrave;me d&rsquo;information, l&agrave; o&ugrave; un syst&egrave;me existe d&eacute;j&agrave; et est activement utilis&eacute;, n&eacute;cessite un certain degr&eacute; de gestion du changement, car ce nouveau syst&egrave;me refl&egrave;te une &laquo;nouvelle fa&ccedil;on de travailler&raquo;.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `ws_testimonial`
--

CREATE TABLE IF NOT EXISTS `ws_testimonial` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(35) NOT NULL,
  `url` varchar(120) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_testimonial`
--

INSERT INTO `ws_testimonial` (`id`, `author_name`, `url`, `image`, `status`) VALUES
(1, 'William John', 'www.bdtask.com', '', 1),
(2, 'Ashraf Rahman', 'www.bdtask.com', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ws_testimonial_lang`
--

CREATE TABLE IF NOT EXISTS `ws_testimonial_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tstml_id` int(11) NOT NULL,
  `language` varchar(15) NOT NULL,
  `title` varchar(50) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `quotation` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ws_testimonial_lang`
--

INSERT INTO `ws_testimonial_lang` (`id`, `tstml_id`, `language`, `title`, `author_name`, `quotation`) VALUES
(1, 2, 'english', '`BDtask@CEO`', 'Ashraf Rahman', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever'),
(2, 2, 'arabic', 'BDtask@CEO', 'Ashraf Rahman', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s'),
(3, 1, 'english', 'Hospital@FCS', 'William John', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bill`
--
ALTER TABLE `bill`
  ADD CONSTRAINT `FK_ipd_bill_ipd_admission` FOREIGN KEY (`admission_id`) REFERENCES `bill_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `bill_advanced`
--
ALTER TABLE `bill_advanced`
  ADD CONSTRAINT `FK_ipd_bill_advanced_ipd_admission` FOREIGN KEY (`admission_id`) REFERENCES `bill_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `bill_details`
--
ALTER TABLE `bill_details`
  ADD CONSTRAINT `fk_ipd_bill_details_ipd_admission` FOREIGN KEY (`admission_id`) REFERENCES `bill_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ipd_bill_details_ipd_bill` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`bill_id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
