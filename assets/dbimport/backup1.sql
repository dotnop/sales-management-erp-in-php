#
# TABLE STRUCTURE FOR: acc_account_name
#

DROP TABLE IF EXISTS `acc_account_name`;

CREATE TABLE `acc_account_name` (
  `account_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) NOT NULL,
  `account_type` int(11) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: acc_coa
#

DROP TABLE IF EXISTS `acc_coa`;

CREATE TABLE `acc_coa` (
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UpdateDate` datetime NOT NULL,
  PRIMARY KEY (`HeadName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5020200004', '2T6QHG45AP4M11FG6TPT-shahabuddin', 'Account Payable', '3', '1', '1', '0', 'L', '0', '0', '0.00', '1', '2019-09-05 05:43:01', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5020200003', '6167UCENYO5JJITKRH21-hhhkkk', 'Account Payable', '3', '1', '1', '0', 'L', '0', '0', '0.00', '1', '2019-09-05 05:40:40', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030134', '6FGVC6K7OX5L9Z1-Walking customer', 'Customer Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-04-20 10:47:37', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021403', 'AC', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:33:55', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('50202', 'Account Payable', 'Current Liabilities', '2', '1', '0', '1', 'L', '0', '0', '0.00', 'admin', '2015-10-15 19:50:43', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10203', 'Account Receivable', 'Current Asset', '2', '1', '0', '0', 'A', '0', '0', '0.00', '', '0000-00-00 00:00:00', 'admin', '2013-09-18 15:29:35');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1020201', 'Advance', 'Advance, Deposit And Pre-payments', '3', '1', '0', '1', 'A', '0', '0', '0.00', 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102020103', 'Advance House Rent', 'Advance', '4', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-10-02 16:55:38', 'admin', '2016-10-02 16:57:32');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10202', 'Advance, Deposit And Pre-payments', 'Current Asset', '2', '1', '0', '0', 'A', '0', '0', '0.00', '', '0000-00-00 00:00:00', 'admin', '2015-12-31 16:46:24');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020602', 'Advertisement and Publicity', 'Promonational Expence', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:51:44', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010410', 'Air Cooler', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-05-23 12:13:55', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020603', 'AIT Against Advertisement', 'Promonational Expence', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:52:09', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1', 'Assets', 'COA', '0', '1', '0', '0', 'A', '0', '0', '0.00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010204', 'Attendance Machine', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:49:31', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40216', 'Audit Fee', 'Other Expenses', '2', '1', '1', '1', 'E', '0', '0', '0.00', 'admin', '2017-07-18 12:54:30', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021002', 'Bank Charge', 'Financial Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:21:03', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('30203', 'Bank Interest', 'Other Income', '2', '1', '1', '1', 'I', '0', '0', '0.00', 'Obaidul', '2015-01-03 14:49:54', 'admin', '2016-09-25 11:04:19');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030140', 'BIXQAYJ3XDFBYZN-Hm Isahaq', 'Customer Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-08-27 08:33:11', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010104', 'Book Shelf', 'Furniture & Fixturers', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:46:11', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010407', 'Books and Journal', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:45:37', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020604', 'Business Development Expenses', 'Promonational Expence', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:52:29', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('404', 'C Bill', 'Expence', '1', '1', '1', '0', 'E', '1', '1', '1.00', '1', '2019-06-17 11:51:58', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020606', 'Campaign Expenses', 'Promonational Expence', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:52:57', 'admin', '2016-09-19 14:52:48');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020502', 'Campus Rent', 'House Rent', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:46:53', 'admin', '2017-04-27 17:02:39');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40212', 'Car Running Expenses', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:28:43', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10201', 'Cash & Cash Equivalent', 'Current Asset', '2', '1', '0', '1', 'A', '0', '0', '0.00', '1', '2019-06-25 13:47:29', 'admin', '2015-10-15 15:57:55');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1020102', 'Cash At Bank', 'Cash & Cash Equivalent', '3', '1', '0', '1', 'A', '0', '0', '0.00', '1', '2019-03-18 06:08:18', 'admin', '2015-10-15 15:32:42');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1020101', 'Cash In Hand', 'Cash & Cash Equivalent', '3', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-01-26 07:38:48', 'admin', '2016-05-23 12:05:43');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010207', 'CCTV', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:51:24', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102020102', 'CEO Current A/C', 'Advance', '4', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-09-25 11:54:54', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010101', 'Class Room Chair', 'Furniture & Fixturers', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:45:29', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021407', 'Close Circuit Cemera', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:35:35', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5020200006', 'CMBQPCTZMZ3ALHEYT8BG-Ovi', 'Account Payable', '3', '1', '1', '0', 'L', '0', '0', '0.00', '1', '2019-09-05 13:44:59', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020601', 'Commision on Admission', 'Promonational Expence', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:51:21', 'admin', '2016-09-19 14:42:54');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010206', 'Computer', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:51:09', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021410', 'Computer (R)', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'Zoherul', '2016-03-24 12:38:52', 'Zoherul', '2016-03-24 12:41:40');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010102', 'Computer Table', 'Furniture & Fixturers', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:45:44', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('301020401', 'Continuing Registration fee - UoL (Income)', 'Registration Fee (UOL) Income', '4', '1', '1', '0', 'I', '0', '0', '0.00', 'admin', '2015-10-15 17:40:40', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020904', 'Contratuall Staff Salary', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:12:34', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020709', 'Cultural Expense', 'Miscellaneous Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'nasmud', '2017-04-29 12:45:10', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102', 'Current Asset', 'Assets', '1', '1', '0', '0', 'A', '0', '0', '0.00', '', '0000-00-00 00:00:00', 'admin', '2018-07-07 11:23:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('502', 'Current Liabilities', 'Liabilities', '1', '1', '0', '0', 'L', '0', '0', '0.00', 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1020301', 'Customer Receivable', 'Account Receivable', '3', '1', '0', '1', 'A', '0', '0', '0.00', '1', '2019-01-24 12:10:05', 'admin', '2018-07-07 12:31:42');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1020202', 'Deposit', 'Advance, Deposit And Pre-payments', '3', '1', '0', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:40:42', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020605', 'Design & Printing Expense', 'Promonational Expence', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:55:00', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102010201', 'Dhaka Bank ', 'Cash At Bank', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-06-28 05:44:41', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020404', 'Dish Bill', 'Utility Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:58:21', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40215', 'Dividend', 'Other Expenses', '2', '1', '1', '1', 'E', '0', '0', '0.00', 'admin', '2016-09-25 14:07:55', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020403', 'Drinking Water Bill', 'Utility Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:58:10', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010211', 'DSLR Camera', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:53:17', 'admin', '2016-01-02 16:23:25');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020908', 'Earned Leave', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:13:38', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020607', 'Education Fair Expenses', 'Promonational Expence', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:53:42', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030205', 'EIWA3A4HT1-jisan', 'Loan Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-02-01 11:28:12', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010602', 'Electric Equipment', 'Electrical Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:44:51', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010203', 'Electric Kettle', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:49:07', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10106', 'Electrical Equipment', 'Non Current Assets', '2', '1', '0', '1', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:43:44', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020407', 'Electricity Bill', 'Utility Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:59:31', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('50204', 'Employee Ledger', 'Current Liabilities', '2', '1', '0', '1', 'L', '0', '0', '0.00', '1', '2019-04-08 10:36:32', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('403', 'Employee Salary', 'Expence', '1', '1', '1', '0', 'E', '0', '1', '1.00', '1', '2019-06-17 11:44:52', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40201', 'Entertainment', 'Other Expenses', '2', '1', '1', '1', 'E', '0', '0', '0.00', 'admin', '2013-07-08 16:21:26', 'anwarul', '2013-07-17 14:21:47');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('2', 'Equity', 'COA', '0', '1', '0', '0', 'L', '0', '0', '0.00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4', 'Expence', 'COA', '0', '1', '0', '0', 'E', '0', '0', '0.00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020903', 'Faculty,Staff Salary & Allowances', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:12:21', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021404', 'Fax Machine', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:34:15', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020905', 'Festival & Incentive Bonus', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:12:48', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010103', 'File Cabinet', 'Furniture & Fixturers', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:46:02', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40210', 'Financial Expenses', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010403', 'Fire Extingushier', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:39:32', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021408', 'Furniture', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:35:47', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10101', 'Furniture & Fixturers', 'Non Current Assets', '2', '1', '0', '1', 'A', '0', '0', '0.00', 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030136', 'FYJ4EEBMOGTW25T-md abdullah', 'Customer Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-06-26 10:17:16', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('20201', 'General Reserve', 'Reserve & Surplus', '2', '1', '1', '0', 'L', '0', '0', '0.00', 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10105', 'Generator', 'Non Current Assets', '2', '1', '1', '1', 'A', '0', '0', '0.00', 'Zoherul', '2016-02-27 16:02:35', 'admin', '2016-05-23 12:05:18');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021414', 'Generator Repair', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'Zoherul', '2016-06-16 10:21:05', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40213', 'Generator Running Expenses', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:29:29', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10103', 'Groceries and Cutleries', 'Non Current Assets', '2', '1', '1', '1', 'A', '0', '0', '0.00', '2', '2018-07-12 10:02:55', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010408', 'Gym Equipment', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:46:03', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('407', 'honda-22552500', 'Expence', '1', '1', '1', '0', 'E', '1', '1', '1.00', '1', '2019-07-01 06:22:04', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020907', 'Honorarium', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:13:26', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40205', 'House Rent', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'anwarul', '2013-08-24 10:26:56', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('3', 'Income', 'COA', '0', '1', '0', '0', 'I', '0', '0', '0.00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('30204', 'Income from Photocopy & Printing', 'Other Income', '2', '1', '1', '1', 'I', '0', '0', '0.00', 'Zoherul', '2015-07-14 10:29:54', 'admin', '2016-09-25 11:04:28');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5020302', 'Income Tax Payable', 'Liabilities for Expenses', '3', '1', '0', '1', 'L', '0', '0', '0.00', 'admin', '2016-09-19 11:18:17', 'admin', '2016-09-28 13:18:35');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10107', 'Inventory', 'Non Current Assets', '1', '1', '0', '0', 'A', '0', '0', '0.00', '2', '2018-07-07 15:21:58', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030206', 'JMGCK5MVCN-dhaka electronics', 'Loan Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-06-27 13:07:33', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030208', 'KDN4CJGVRK-abul hardware', 'Loan Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-06-27 13:08:11', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030139', 'KFTUKN7RY1O2W6R-samil khan', 'Customer Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-06-28 05:47:49', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5020200002', 'KRQGUNN4ZMS3WT8I2RK9-global band ', 'Account Payable', '3', '1', '1', '0', 'L', '0', '0', '0.00', '1', '2019-06-28 05:56:23', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010210', 'LCD TV', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:52:27', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('30103', 'Lease Sale', 'Store Income', '1', '1', '1', '1', 'I', '0', '0', '0.00', '2', '2018-07-08 07:51:52', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5', 'Liabilities', 'COA', '0', '1', '0', '0', 'L', '0', '0', '0.00', 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('50203', 'Liabilities for Expenses', 'Current Liabilities', '2', '1', '0', '0', 'L', '0', '0', '0.00', 'admin', '2015-10-15 19:50:59', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020707', 'Library Expenses', 'Miscellaneous Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2017-01-10 15:34:54', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021409', 'Lift', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:36:12', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1020302', 'Loan Receivable', 'Account Receivable', '3', '1', '0', '1', 'A', '0', '0', '0.00', '1', '2019-01-26 07:37:20', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('50101', 'Long Term Borrowing', 'Non Current Liabilities', '2', '1', '0', '1', 'L', '0', '0', '0.00', 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020608', 'Marketing & Promotion Exp.', 'Promonational Expence', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:53:59', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020901', 'Medical Allowance', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:11:33', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010411', 'Metal Ditector', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'Zoherul', '2016-08-22 10:55:22', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021413', 'Micro Oven', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'Zoherul', '2016-05-12 14:53:51', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('30202', 'Miscellaneous (Income)', 'Other Income', '2', '1', '1', '1', 'I', '0', '0', '0.00', 'anwarul', '2014-02-06 15:26:31', 'admin', '2016-09-25 11:04:35');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020909', 'Miscellaneous Benifit', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:13:53', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2016-09-25 12:54:39', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40207', 'Miscellaneous Expenses', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010401', 'Mobile Phone', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-01-29 10:43:30', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102020101', 'Mr Ashiqur Rahman', 'Advance', '4', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-12-31 16:47:23', 'admin', '2016-09-25 11:55:13');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010212', 'Network Accessories', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-01-02 16:23:32', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020408', 'News Paper Bill', 'Utility Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2016-01-02 15:55:57', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('101', 'Non Current Assets', 'Assets', '1', '1', '0', '0', 'A', '0', '0', '0.00', '', '0000-00-00 00:00:00', 'admin', '2015-10-15 15:29:11');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('501', 'Non Current Liabilities', 'Liabilities', '1', '1', '0', '0', 'L', '0', '0', '0.00', 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010404', 'Office Decoration', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:40:02', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10102', 'Office Equipment', 'Non Current Assets', '2', '1', '0', '1', 'A', '0', '0', '0.00', 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:33:15', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('30201', 'Office Stationary (Income)', 'Other Income', '2', '1', '1', '1', 'I', '0', '0', '0.00', 'anwarul', '2013-07-17 15:21:06', 'admin', '2016-09-25 11:04:50');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('302', 'Other Income', 'Income', '1', '1', '0', '0', 'I', '0', '0', '0.00', '2', '2018-07-07 13:40:57', 'admin', '2016-09-25 11:04:09');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40211', 'Others (Non Academic Expenses)', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'Obaidul', '2014-12-03 16:05:42', 'admin', '2015-10-15 19:22:09');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('30205', 'Others (Non-Academic Income)', 'Other Income', '2', '1', '0', '1', 'I', '0', '0', '0.00', 'admin', '2015-10-15 17:23:49', 'admin', '2015-10-15 17:57:52');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10104', 'Others Assets', 'Non Current Assets', '2', '1', '0', '1', 'A', '0', '0', '0.00', 'admin', '2016-01-29 10:43:16', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020910', 'Outstanding Salary', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'Zoherul', '2016-04-24 11:56:50', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021405', 'Oven', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:34:31', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021412', 'PABX-Repair', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'Zoherul', '2016-04-24 14:40:18', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020902', 'Part-time Staff Salary', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:12:06', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010202', 'Photocopy & Fax Machine', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021411', 'Photocopy Machine Repair', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'Zoherul', '2016-04-24 12:40:02', 'admin', '2017-04-27 17:03:17');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('3020503', 'Practical Fee', 'Others (Non-Academic Income)', '3', '1', '1', '1', 'I', '0', '0', '0.00', 'admin', '2017-07-22 18:00:37', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1020203', 'Prepayment', 'Advance, Deposit And Pre-payments', '3', '1', '0', '1', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:40:51', 'admin', '2015-12-31 16:49:58');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010201', 'Printer', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:47:15', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40202', 'Printing and Stationary', 'Other Expenses', '2', '1', '1', '1', 'E', '0', '0', '0.00', 'admin', '2013-07-08 16:21:45', 'admin', '2016-09-19 14:39:32');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('402', 'Product Purchase', 'Expence', '1', '1', '0', '0', 'E', '0', '0', '0.00', '2', '2018-07-07 14:00:16', 'admin', '2015-10-15 18:37:42');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('303', 'Product Sale', 'Income', '1', '1', '1', '0', 'I', '0', '0', '0.00', '1', '2019-06-17 08:22:42', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('3020502', 'Professional Training Course(Oracal-1)', 'Others (Non-Academic Income)', '3', '1', '1', '0', 'I', '0', '0', '0.00', 'nasim', '2017-06-22 13:28:05', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('30207', 'Professional Training Course(Oracal)', 'Other Income', '2', '1', '0', '1', 'I', '0', '0', '0.00', 'nasim', '2017-06-22 13:24:16', 'nasim', '2017-06-22 13:25:56');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010208', 'Projector', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:51:44', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40206', 'Promonational Expence', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'anwarul', '2013-07-11 13:48:57', 'anwarul', '2013-07-17 14:23:03');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030137', 'PVGGNNSWOESP45P-alexix', 'Customer Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-06-26 10:52:38', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40214', 'Repair and Maintenance', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:32:46', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('202', 'Reserve & Surplus', 'Equity', '1', '1', '0', '1', 'L', '0', '0', '0.00', 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('20102', 'Retained Earnings', 'Share Holders Equity', '2', '1', '1', '1', 'L', '0', '0', '0.00', 'admin', '2016-05-23 11:20:40', 'admin', '2016-09-25 14:05:06');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020708', 'River Cruse', 'Miscellaneous Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2017-04-24 15:35:25', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102020105', 'Salary', 'Advance', '4', '1', '0', '0', 'A', '0', '0', '0.00', 'admin', '2018-07-05 11:46:44', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40209', 'Salary & Allowances', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'anwarul', '2013-12-12 11:22:58', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010406', 'Security Equipment', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:41:30', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('304', 'Service Income', 'Income', '1', '1', '1', '0', 'I', '0', '0', '0.00', '1', '2019-06-17 11:31:11', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('20101', 'Share Capital', 'Share Holders Equity', '2', '1', '0', '1', 'L', '0', '0', '0.00', 'anwarul', '2013-12-08 19:37:32', 'admin', '2015-10-15 19:45:35');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('201', 'Share Holders Equity', 'Equity', '1', '1', '0', '0', 'L', '0', '0', '0.00', '', '0000-00-00 00:00:00', 'admin', '2015-10-15 19:43:51');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('405', 'shop rent ', 'Expence', '1', '1', '1', '0', 'E', '1', '1', '1.00', '1', '2019-06-28 05:40:09', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('50201', 'Short Term Borrowing', 'Current Liabilities', '2', '1', '0', '1', 'L', '0', '0', '0.00', 'admin', '2015-10-15 19:50:30', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40208', 'Software Development Expenses', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'anwarul', '2013-11-21 14:13:01', 'admin', '2015-10-15 19:02:51');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020906', 'Special Allowances', 'Salary & Allowances', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:13:13', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('50102', 'Sponsors Loan', 'Non Current Liabilities', '2', '1', '0', '1', 'L', '0', '0', '0.00', 'admin', '2015-10-15 19:48:02', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020706', 'Sports Expense', 'Miscellaneous Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'nasmud', '2016-11-09 13:16:53', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('301', 'Store Income', 'Income', '1', '1', '0', '0', 'I', '0', '0', '0.00', '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('3020501', 'Students Info. Correction Fee', 'Others (Non-Academic Income)', '3', '1', '1', '0', 'I', '0', '0', '0.00', 'admin', '2015-10-15 17:24:45', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010601', 'Sub Station', 'Electrical Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:44:11', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020704', 'TB Care Expenses', 'Miscellaneous Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2016-10-08 13:03:04', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('30206', 'TB Care Income', 'Other Income', '2', '1', '1', '1', 'I', '0', '0', '0.00', 'admin', '2016-10-08 13:00:56', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020501', 'TDS on House Rent', 'House Rent', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:44:07', 'admin', '2016-09-19 14:40:16');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('502030201', 'TDS Payable House Rent', 'Income Tax Payable', '4', '1', '1', '0', 'L', '0', '0', '0.00', 'admin', '2016-09-19 11:19:42', 'admin', '2016-09-28 13:19:37');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('502030203', 'TDS Payable on Advertisement Bill', 'Income Tax Payable', '4', '1', '1', '0', 'L', '0', '0', '0.00', 'admin', '2016-09-28 13:20:51', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('502030202', 'TDS Payable on Salary', 'Income Tax Payable', '4', '1', '1', '0', 'L', '0', '0', '0.00', 'admin', '2016-09-28 13:20:17', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('406', 'Tea biscut', 'Expence', '1', '1', '1', '0', 'E', '1', '1', '1.00', '1', '2019-06-28 05:40:26', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021402', 'Tea Kettle', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:33:45', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020402', 'Telephone Bill', 'Utility Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:57:59', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010209', 'Telephone Set & PABX', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:51:57', 'admin', '2016-10-02 17:10:40');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('10108', 'Test', 'Non Current Assets', '2', '1', '0', '0', 'A', '0', '0', '0.00', '1', '2019-09-19 06:03:07', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40203', 'Travelling & Conveyance', 'Other Expenses', '2', '1', '1', '1', 'E', '0', '0', '0.00', 'admin', '2013-07-08 16:22:06', 'admin', '2015-10-15 18:45:13');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4021406', 'TV', 'Repair and Maintenance', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 19:35:07', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010205', 'UPS', 'Office Equipment', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:50:38', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('40204', 'Utility Expenses', 'Other Expenses', '2', '1', '0', '1', 'E', '0', '0', '0.00', 'anwarul', '2013-07-11 16:20:24', 'admin', '2016-01-02 15:55:22');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020503', 'VAT on House Rent Exp', 'House Rent', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:49:22', 'admin', '2016-09-25 14:00:52');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5020301', 'VAT Payable', 'Liabilities for Expenses', '3', '1', '0', '1', 'L', '0', '0', '0.00', 'admin', '2015-10-15 19:51:11', 'admin', '2016-09-28 13:23:53');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010409', 'Vehicle A/C', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'Zoherul', '2016-05-12 12:13:21', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030207', 'VJY67W3YTE-tesco ', 'Loan Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-06-27 13:07:51', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010405', 'Voltage Stablizer', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-03-27 10:40:59', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010105', 'Waiting Sofa - Steel', 'Furniture & Fixturers', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2015-10-15 15:46:29', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020405', 'WASA Bill', 'Utility Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2015-10-15 18:58:51', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('1010402', 'Water Purifier', 'Others Assets', '3', '1', '1', '0', 'A', '0', '0', '0.00', 'admin', '2016-01-29 11:14:11', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('4020705', 'Website Development Expenses', 'Miscellaneous Expenses', '3', '1', '1', '0', 'E', '0', '0', '0.00', 'admin', '2016-10-15 12:42:47', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5020200001', 'WPXXU7UC1Z4ID1HUVU53-Test Supplier', 'Account Payable', '3', '1', '1', '0', 'L', '0', '0', '0.00', '1', '2019-06-25 09:07:26', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('5020200005', 'XUQB7JFFJSFXKGOZ9VD3-isahaq', 'Account Payable', '3', '1', '1', '0', 'L', '0', '0', '0.00', '1', '2019-09-05 06:01:45', '', '0000-00-00 00:00:00');
INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES ('102030135', 'XWCEM9UVV5NAT5R-Test customer', 'Customer Receivable', '4', '1', '1', '0', 'A', '0', '0', '0.00', '1', '2019-06-25 09:09:05', '', '0000-00-00 00:00:00');


#
# TABLE STRUCTURE FOR: acc_income_expence
#

DROP TABLE IF EXISTS `acc_income_expence`;

CREATE TABLE `acc_income_expence` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VNo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Student_Id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `Paymode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Perpose` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci NOT NULL,
  `StoreID` int(11) NOT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `IsApprove` tinyint(4) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: acc_transaction
#

DROP TABLE IF EXISTS `acc_transaction`;

CREATE TABLE `acc_transaction` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VDate` date DEFAULT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `IsPosted` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `IsAppove` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=947 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('1', '20190625090838', 'Purchase', '2019-06-25', '10107', 'Inventory Debit For Supplier Test Supplier', '7500.00', '0.00', '1', '1', '2019-06-25 09:08:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('2', '20190625090838', 'Purchase', '2019-06-25', '5020200001', 'Supplier .Test Supplier', '0.00', '7500.00', '1', '1', '2019-06-25 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('3', '20190625090838', 'Purchase', '2019-06-25', '402', 'Company Credit For  Test Supplier', '7500.00', '0.00', '1', '1', '2019-06-25 09:08:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('4', '20190625090838', 'Purchase', '2019-06-25', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '7500.00', '1', '1', '2019-06-25 09:08:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('5', '20190625090838', 'Purchase', '2019-06-25', '5020200001', 'Supplier .Test Supplier', '7500.00', '0.00', '1', '1', '2019-06-25 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('6', '3498244447', 'INV', '2019-06-25', '10107', 'Inventory credit For Invoice No3498244447', '0.00', '150.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('7', '3498244447', 'INV', '2019-06-25', '102030135', 'Customer debit For  Test customer', '200.00', '0.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('8', '3498244447', 'INVOICE', '2019-06-25', '303', 'Sale Income For Test customer', '0.00', '200.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('9', '3498244447', 'INV', '2019-06-25', '102030135', 'Customer credit for Paid Amount For Customer Test customer', '0.00', '200.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('10', '3498244447', 'INV', '2019-06-25', '1020101', 'Cash in Hand in Sale for Test customer', '200.00', '0.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('11', '4477997231', 'INV', '2019-06-25', '10107', 'Inventory credit For Invoice No4477997231', '0.00', '150.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('12', '4477997231', 'INV', '2019-06-25', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('13', '4477997231', 'INVOICE', '2019-06-25', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('14', '4477997231', 'INV', '2019-06-25', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('15', '4477997231', 'INV', '2019-06-25', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('16', '20190625132953', 'Purchase', '2019-06-25', '10107', 'Inventory Debit For Supplier Test Supplier', '4000.00', '0.00', '1', '1', '2019-06-25 13:29:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('17', '20190625132953', 'Purchase', '2019-06-25', '5020200001', 'Supplier .Test Supplier', '0.00', '4000.00', '1', '1', '2019-06-25 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('18', '20190625132953', 'Purchase', '2019-06-25', '402', 'Company Credit For  Test Supplier', '4000.00', '0.00', '1', '1', '2019-06-25 13:29:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('19', '20190625132953', 'Purchase', '2019-06-25', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '4000.00', '1', '1', '2019-06-25 13:29:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('20', '20190625132953', 'Purchase', '2019-06-25', '5020200001', 'Supplier .Test Supplier', '4000.00', '0.00', '1', '1', '2019-06-25 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('21', '3973611118', 'INV', '2019-06-25', '10107', 'Inventory credit For Invoice No3973611118', '0.00', '400.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('22', '3973611118', 'INV', '2019-06-25', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('23', '3973611118', 'INVOICE', '2019-06-25', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('24', '3973611118', 'INV', '2019-06-25', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('25', '3973611118', 'INV', '2019-06-25', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('26', 'PM-1', 'PM', '2019-06-25', '5020200001', '', '250.00', '0.00', '1', '1', '2019-06-25 13:49:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('27', 'PM-1', 'PM', '2019-06-25', '1020101', 'Cash in Hand For Voucher NoPM-1', '0.00', '250.00', '1', '1', '2019-06-25 13:49:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('28', '3923586588', 'INV', '2019-06-25', '10107', 'Inventory credit For Invoice No3923586588', '0.00', '950.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('29', '3923586588', 'INV', '2019-06-25', '102030134', 'Customer debit For  Walking customer', '900.00', '0.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('30', '3923586588', 'INVOICE', '2019-06-25', '303', 'Sale Income For Walking customer', '0.00', '900.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('31', '3923586588', 'INV', '2019-06-25', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '600.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('32', '3923586588', 'INV', '2019-06-25', '1020101', 'Cash in Hand in Sale for Walking customer', '600.00', '0.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('33', '6575276926', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No6575276926', '0.00', '150.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('34', '6575276926', 'INV', '2019-06-26', '102030135', 'Customer debit For  Test customer', '200.00', '0.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('35', '6575276926', 'INVOICE', '2019-06-26', '303', 'Sale Income For Test customer', '0.00', '200.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('36', '6575276926', 'INV', '2019-06-26', '102030135', 'Customer credit for Paid Amount For Customer Test customer', '0.00', '200.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('37', '6575276926', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Test customer', '200.00', '0.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('38', '6643494915', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No6643494915', '0.00', '150.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('39', '6643494915', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('40', '6643494915', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('41', '6643494915', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('42', '6643494915', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('43', '1969912464', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No1969912464', '0.00', '400.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('44', '1969912464', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('45', '1969912464', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('46', '1969912464', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('47', '1969912464', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('48', '6691959977', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No6691959977', '0.00', '400.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('49', '6691959977', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('50', '6691959977', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('51', '6691959977', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('52', '6691959977', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('53', '2258111339', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No2258111339', '0.00', '150.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('54', '2258111339', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('55', '2258111339', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('56', '2258111339', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('57', '2258111339', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('58', '9624435359', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No9624435359', '0.00', '400.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('59', '9624435359', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('60', '9624435359', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('61', '9624435359', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('62', '9624435359', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('63', '8668653724', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No8668653724', '0.00', '400.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('64', '8668653724', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('65', '8668653724', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('66', '8668653724', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('67', '8668653724', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('68', '8558184696', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No8558184696', '0.00', '400.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('69', '8558184696', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('70', '8558184696', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('71', '8558184696', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('72', '8558184696', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('73', '20190626100758', 'Purchase', '2019-06-26', '10107', 'Inventory Debit For Supplier Test Supplier', '3500.00', '0.00', '1', '1', '2019-06-26 10:07:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('74', '20190626100758', 'Purchase', '2019-06-26', '5020200001', 'Supplier .Test Supplier', '0.00', '3500.00', '1', '1', '2019-06-26 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('75', '20190626100758', 'Purchase', '2019-06-26', '402', 'Company Credit For  Test Supplier', '3500.00', '0.00', '1', '1', '2019-06-26 10:07:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('76', '20190626100758', 'Purchase', '2019-06-26', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '3500.00', '1', '1', '2019-06-26 10:07:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('77', '20190626100758', 'Purchase', '2019-06-26', '5020200001', 'Supplier .Test Supplier', '3500.00', '0.00', '1', '1', '2019-06-26 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('78', '9147771963', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No9147771963', '0.00', '350.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('79', '9147771963', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '450.00', '0.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('80', '9147771963', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '450.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('81', '9147771963', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '450.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('82', '9147771963', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '450.00', '0.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('83', '20190626101452', 'Purchase', '2019-06-26', '10107', 'Inventory Debit For Supplier Test Supplier', '3900.00', '0.00', '1', '1', '2019-06-26 10:14:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('84', '20190626101452', 'Purchase', '2019-06-26', '5020200001', 'Supplier .Test Supplier', '0.00', '3900.00', '1', '1', '2019-06-26 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('85', '20190626101452', 'Purchase', '2019-06-26', '402', 'Company Credit For  Test Supplier', '3900.00', '0.00', '1', '1', '2019-06-26 10:14:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('86', '20190626101452', 'Purchase', '2019-06-26', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '3900.00', '1', '1', '2019-06-26 10:14:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('87', '20190626101452', 'Purchase', '2019-06-26', '5020200001', 'Supplier .Test Supplier', '3900.00', '0.00', '1', '1', '2019-06-26 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('88', '3169824785', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No3169824785', '0.00', '390.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('89', '3169824785', 'INV', '2019-06-26', '102030136', 'Customer debit For  md abdullah', '800.00', '0.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('90', '3169824785', 'INVOICE', '2019-06-26', '303', 'Sale Income For md abdullah', '0.00', '800.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('91', '3169824785', 'INV', '2019-06-26', '102030136', 'Customer credit for Paid Amount For Customer md abdullah', '0.00', '800.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('92', '3169824785', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for md abdullah', '800.00', '0.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('93', '3918198168', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No3918198168', '0.00', '890.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('94', '3918198168', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '1450.00', '0.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('95', '3918198168', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '1450.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('96', '3918198168', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '1450.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('97', '3918198168', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '1450.00', '0.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('98', '5219125479', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No5219125479', '0.00', '350.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('99', '5219125479', 'INV', '2019-06-26', '102030137', 'Customer debit For  alexix', '450.00', '0.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('100', '5219125479', 'INVOICE', '2019-06-26', '303', 'Sale Income For alexix', '0.00', '450.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('101', '5219125479', 'INV', '2019-06-26', '102030137', 'Customer credit for Paid Amount For Customer alexix', '0.00', '450.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('102', '5219125479', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for alexix', '450.00', '0.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('103', '8394724961', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No8394724961', '0.00', '890.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('104', '8394724961', 'INV', '2019-06-26', '102030137', 'Customer debit For  alexix', '1450.00', '0.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('105', '8394724961', 'INVOICE', '2019-06-26', '303', 'Sale Income For alexix', '0.00', '1450.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('106', '8394724961', 'INV', '2019-06-26', '102030137', 'Customer credit for Paid Amount For Customer alexix', '0.00', '1450.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('107', '8394724961', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for alexix', '1450.00', '0.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('108', '7986446844', 'INV', '2019-06-27', '10107', 'Inventory credit For Invoice No7986446844', '0.00', '390.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('109', '7986446844', 'INV', '2019-06-27', '102030134', 'Customer debit For  Walking customer', '800.00', '0.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('110', '7986446844', 'INVOICE', '2019-06-27', '303', 'Sale Income For Walking customer', '0.00', '800.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('111', '7986446844', 'INV', '2019-06-27', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '800.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('112', '7986446844', 'INV', '2019-06-27', '1020101', 'Cash in Hand in Sale for Walking customer', '800.00', '0.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('113', '6789217583', 'INV', '2019-06-27', '10107', 'Inventory credit For Invoice No6789217583', '0.00', '400.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('114', '6789217583', 'INV', '2019-06-27', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('115', '6789217583', 'INVOICE', '2019-06-27', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('116', '6789217583', 'INV', '2019-06-27', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('117', '6789217583', 'INV', '2019-06-27', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('118', 'KT84M3I2O7', 'LNR', '2019-06-27', '102030206', 'Loan for .dhaka electronics', '150.00', '0.00', '1', '1', '2019-06-27 13:08:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('119', 'KT84M3I2O7', 'LNR', '2019-06-27', '1020101', 'Cash in Hand Credit For dhaka electronics', '0.00', '150.00', '1', '1', '2019-06-27 13:08:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('120', 'F6E1J7D1DX', 'LNR', '2019-06-27', '102030207', 'Loan for .tesco ', '500.00', '0.00', '1', '1', '2019-06-27 13:09:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('121', 'F6E1J7D1DX', 'LNR', '2019-06-27', '1020101', 'Cash in Hand Credit For tesco ', '0.00', '500.00', '1', '1', '2019-06-27 13:09:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('122', 'XJ4B8N4TWO', 'LNR', '2019-06-27', '102030208', 'Loan for .abul hardware', '650.00', '0.00', '1', '1', '2019-06-27 13:09:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('123', 'XJ4B8N4TWO', 'LNR', '2019-06-27', '1020101', 'Cash in Hand Credit For abul hardware', '0.00', '650.00', '1', '1', '2019-06-27 13:09:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('124', '4978495577', 'INV', '2019-06-27', '10107', 'Inventory credit For Invoice No4978495577', '0.00', '750.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('125', '4978495577', 'INV', '2019-06-27', '102030138', 'Customer debit For  kamal', '950.00', '0.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('126', '4978495577', 'INVOICE', '2019-06-27', '303', 'Sale Income For kamal', '0.00', '950.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('127', '4978495577', 'INV', '2019-06-27', '102030138', 'Customer credit for Paid Amount For Customer kamal', '0.00', '500.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('128', '4978495577', 'INV', '2019-06-27', '1020101', 'Cash in Hand in Sale for kamal', '500.00', '0.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('129', '20190628054055', 'Expense', '2019-06-28', '406', 'Tea biscut Expense 20190628054055', '350.00', '0.00', '1', '1', '2019-06-28 05:40:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('130', '20190628054055', 'Expense', '2019-06-28', '1020101', 'Tea biscut Expense20190628054055', '0.00', '350.00', '1', '1', '2019-06-28 05:40:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('131', '20190628054109', 'Expense', '2019-06-28', '405', 'shop rent  Expense 20190628054109', '450.00', '0.00', '1', '1', '2019-06-28 05:41:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('132', '20190628054109', 'Expense', '2019-06-28', '1020101', 'shop rent  Expense20190628054109', '0.00', '450.00', '1', '1', '2019-06-28 05:41:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('133', 'VS6VVPYSTP', 'LNR', '2019-06-28', '102030206', 'Loan for .dhaka electronics', '555.00', '0.00', '1', '1', '2019-06-28 05:41:28', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('134', 'VS6VVPYSTP', 'LNR', '2019-06-28', '1020101', 'Cash in Hand Credit For dhaka electronics', '0.00', '555.00', '1', '1', '2019-06-28 05:41:28', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('135', 'QV9JT8IFJE', 'LNP', '2019-06-28', '102030207', 'Loan Payment from .tesco ', '0.00', '333.00', '1', '1', '2019-06-28 05:41:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('136', 'QV9JT8IFJE', 'LNR', '2019-06-28', '1020101', 'Cash in Hand Debit For tesco ', '333.00', '0.00', '1', '1', '2019-06-28 05:41:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('137', '20190628054650', 'Purchase', '2019-06-28', '10107', 'Inventory Debit For Supplier Test Supplier', '400000.00', '0.00', '1', '1', '2019-06-28 05:46:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('138', '20190628054650', 'Purchase', '2019-06-28', '5020200001', 'Supplier .Test Supplier', '0.00', '400000.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('139', '20190628054650', 'Purchase', '2019-06-28', '402', 'Company Credit For  Test Supplier', '400000.00', '0.00', '1', '1', '2019-06-28 05:46:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('140', '2687213589', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No2687213589', '0.00', '40000.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('141', '2687213589', 'INV', '2019-06-28', '102030139', 'Customer debit For  samil khan', '50000.00', '0.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('142', '2687213589', 'INVOICE', '2019-06-28', '303', 'Sale Income For samil khan', '0.00', '50000.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('143', '2687213589', 'INV', '2019-06-28', '102030139', 'Customer credit for Paid Amount For Customer samil khan', '0.00', '30000.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('144', '2687213589', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for samil khan', '30000.00', '0.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('145', 'G21MAEQN2P', 'Advance', '2019-06-28', '102030139', 'Customer Advance Forsamil khan', '111.00', '0.00', '1', '1', '2019-06-28 05:51:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('146', 'G21MAEQN2P', 'Advance', '2019-06-28', '1020101', 'Cash in Hand  For samil khan Advance', '111.00', '0.00', '1', '1', '2019-06-28 05:51:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('147', 'HJK5D3PFUW', 'Advance', '2019-06-28', '102030138', 'Customer Advance Forkamal', '0.00', '200.00', '1', '1', '2019-06-28 05:51:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('148', 'HJK5D3PFUW', 'Advance', '2019-06-28', '1020101', 'Cash in Hand  For kamal Advance', '0.00', '200.00', '1', '1', '2019-06-28 05:51:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('149', 'V2WGALHBZ5', 'Advance', '2019-06-28', '5020200001', 'supplier Advance For Test Supplier', '2500.00', '0.00', '1', '1', '2019-06-28 05:54:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('150', 'V2WGALHBZ5', 'Advance', '2019-06-28', '1020101', 'Cash in Hand  For Test Supplier Advance', '2500.00', '0.00', '1', '1', '2019-06-28 05:54:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('151', '20190628055844', 'Purchase', '2019-06-28', '10107', 'Inventory Debit For Supplier global band ', '150000.00', '0.00', '1', '1', '2019-06-28 05:58:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('152', '20190628055844', 'Purchase', '2019-06-28', '5020200002', 'Supplier .global band ', '0.00', '150000.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('153', '20190628055844', 'Purchase', '2019-06-28', '402', 'Company Credit For  global band ', '150000.00', '0.00', '1', '1', '2019-06-28 05:58:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('154', '20190628055844', 'Purchase', '2019-06-28', '1020101', 'Cash in Hand For Supplier global band ', '0.00', '150000.00', '1', '1', '2019-06-28 05:58:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('155', '20190628055844', 'Purchase', '2019-06-28', '5020200002', 'Supplier .global band ', '150000.00', '0.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('156', '20190628062109', 'Purchase', '2019-06-28', '10107', 'Inventory Debit For Supplier Test Supplier', '1900.00', '0.00', '1', '1', '2019-06-28 06:21:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('157', '20190628062109', 'Purchase', '2019-06-28', '5020200001', 'Supplier .Test Supplier', '0.00', '1900.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('158', '20190628062109', 'Purchase', '2019-06-28', '402', 'Company Credit For  Test Supplier', '1900.00', '0.00', '1', '1', '2019-06-28 06:21:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('159', '20190628062109', 'Purchase', '2019-06-28', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '1900.00', '1', '1', '2019-06-28 06:21:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('160', '20190628062109', 'Purchase', '2019-06-28', '5020200001', 'Supplier .Test Supplier', '1900.00', '0.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('161', '3378453241', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No3378453241', '0.00', '390.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('162', '3378453241', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('163', '3378453241', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('164', '3378453241', 'INV', '2019-06-28', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('165', '3378453241', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('166', '4458233386', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No4458233386', '0.00', '390.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('167', '4458233386', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('168', '4458233386', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('169', '4458233386', 'INV', '2019-06-28', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('170', '4458233386', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('171', '1114135497', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No1114135497', '0.00', '1520.00', '1', '1', '2019-06-28 10:57:32', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('172', '1114135497', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '2210.00', '0.00', '1', '1', '2019-06-28 10:57:32', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('173', '1114135497', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '2210.00', '1', '1', '2019-06-28 10:57:32', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('174', '4441511736', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No4441511736', '0.00', '1520.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('175', '4441511736', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '2210.00', '0.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('176', '4441511736', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '2210.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('177', '4441511736', 'INV', '2019-06-28', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '2210.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('178', '4441511736', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for Walking customer', '2210.00', '0.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('179', '8754679919', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No8754679919', '0.00', '40000.00', '1', '1', '2019-06-28 10:58:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('180', '8754679919', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '50000.00', '0.00', '1', '1', '2019-06-28 10:58:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('181', '8754679919', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '50000.00', '1', '1', '2019-06-28 10:58:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('182', '3378767423', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No3378767423', '0.00', '40000.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('183', '3378767423', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '50000.00', '0.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('184', '3378767423', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '50000.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('185', '3378767423', 'INV', '2019-06-28', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50000.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('186', '3378767423', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for Walking customer', '50000.00', '0.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('187', '9317933378', 'INV', '2019-06-29', '10107', 'Inventory credit For Invoice No9317933378', '0.00', '40000.00', '1', '1', '2019-06-29 05:30:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('188', '9317933378', 'INV', '2019-06-29', '102030134', 'Customer debit For  Walking customer', '50000.00', '0.00', '1', '1', '2019-06-29 05:30:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('189', '9317933378', 'INVOICE', '2019-06-29', '303', 'Sale Income For Walking customer', '0.00', '50000.00', '1', '1', '2019-06-29 05:30:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('190', '1797561487', 'INV', '2019-06-29', '10107', 'Inventory credit For Invoice No1797561487', '0.00', '30780.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('191', '1797561487', 'INV', '2019-06-29', '102030134', 'Customer debit For  Walking customer', '36000.00', '0.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('192', '1797561487', 'INVOICE', '2019-06-29', '303', 'Sale Income For Walking customer', '0.00', '36000.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('193', '1797561487', 'INV', '2019-06-29', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '36000.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('194', '1797561487', 'INV', '2019-06-29', '1020101', 'Cash in Hand in Sale for Walking customer', '36000.00', '0.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('195', '20190629115435', 'Purchase', '2019-06-29', '10107', 'Inventory Debit For Supplier Test Supplier', '20000.00', '0.00', '1', '1', '2019-06-29 11:54:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('196', '20190629115435', 'Purchase', '2019-06-29', '5020200001', 'Supplier .Test Supplier', '0.00', '20000.00', '1', '1', '2019-06-29 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('197', '20190629115435', 'Purchase', '2019-06-29', '402', 'Company Credit For  Test Supplier', '20000.00', '0.00', '1', '1', '2019-06-29 11:54:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('198', '20190629115435', 'Purchase', '2019-06-29', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '20000.00', '1', '1', '2019-06-29 11:54:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('199', '20190629115435', 'Purchase', '2019-06-29', '5020200001', 'Supplier .Test Supplier', '20000.00', '0.00', '1', '1', '2019-06-29 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('200', '9438755655', 'INV', '2019-06-30', '10107', 'Inventory credit For Invoice No9438755655', '0.00', '30000.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('201', '9438755655', 'INV', '2019-06-30', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('202', '9438755655', 'INVOICE', '2019-06-30', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('203', '9438755655', 'INV', '2019-06-30', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('204', '9438755655', 'INV', '2019-06-30', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('205', '5919433886', 'INV', '2019-06-30', '10107', 'Inventory credit For Invoice No5919433886', '0.00', '30000.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('206', '5919433886', 'INV', '2019-06-30', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('207', '5919433886', 'INVOICE', '2019-06-30', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('208', '5919433886', 'INV', '2019-06-30', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('209', '5919433886', 'INV', '2019-06-30', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('210', '2875718763', 'INV', '2019-07-01', '10107', 'Inventory credit For Invoice No2875718763', '0.00', '150.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('211', '2875718763', 'INV', '2019-07-01', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('212', '2875718763', 'INVOICE', '2019-07-01', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('213', '2875718763', 'INV', '2019-07-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('214', '2875718763', 'INV', '2019-07-01', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('215', '20190701062243', 'Expense', '2019-07-01', '407', 'honda-22552500 Expense 20190701062243', '350.00', '0.00', '1', '1', '2019-07-01 06:22:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('216', '20190701062243', 'Expense', '2019-07-01', '1020101', 'honda-22552500 Expense20190701062243', '0.00', '350.00', '1', '1', '2019-07-01 06:22:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('217', '20190701062405', 'Expense', '2019-07-01', '407', 'honda-22552500 Expense 20190701062405', '500.00', '0.00', '1', '1', '2019-07-01 06:24:05', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('218', '20190701062405', 'Expense', '2019-07-01', '1020101', 'honda-22552500 Expense20190701062405', '0.00', '500.00', '1', '1', '2019-07-01 06:24:05', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('219', '1749144575', 'INV', '2019-07-02', '10107', 'Inventory credit For Invoice No1749144575', '0.00', '150.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('220', '1749144575', 'INV', '2019-07-02', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('221', '1749144575', 'INVOICE', '2019-07-02', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('222', '1749144575', 'INV', '2019-07-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('223', '1749144575', 'INV', '2019-07-02', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('224', '9815582964', 'INV', '2019-07-02', '10107', 'Inventory credit For Invoice No9815582964', '0.00', '30000.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('225', '9815582964', 'INV', '2019-07-02', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('226', '9815582964', 'INVOICE', '2019-07-02', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('227', '9815582964', 'INV', '2019-07-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('228', '9815582964', 'INV', '2019-07-02', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('229', '8493678614', 'INV', '2019-07-02', '10107', 'Inventory credit For Invoice No8493678614', '0.00', '350.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('230', '8493678614', 'INV', '2019-07-02', '102030134', 'Customer debit For  Walking customer', '455.00', '0.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('231', '8493678614', 'INVOICE', '2019-07-02', '303', 'Sale Income For Walking customer', '0.00', '455.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('232', '8493678614', 'INV', '2019-07-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '455.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('233', '8493678614', 'INV', '2019-07-02', '1020101', 'Cash in Hand in Sale for Walking customer', '455.00', '0.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('234', '9691144514', 'INV', '2019-07-31', '10107', 'Inventory credit For Invoice No9691144514', '0.00', '390.00', '1', '1', '2019-07-31 11:38:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('235', '9691144514', 'INV', '2019-07-31', '102030134', 'Customer debit For  Walking customer', '800.00', '0.00', '1', '1', '2019-07-31 11:38:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('236', '9691144514', 'INVOICE', '2019-07-31', '303', 'Sale Income For Walking customer', '0.00', '800.00', '1', '1', '2019-07-31 11:38:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('237', '20190731114458', 'Purchase', '2019-07-31', '10107', 'Inventory Debit For Supplier global band ', '0.00', '0.00', '1', '1', '2019-07-31 11:44:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('238', '20190731114458', 'Purchase', '2019-07-31', '5020200002', 'Supplier .global band ', '0.00', '0.00', '1', '1', '2019-07-31 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('239', '20190731114458', 'Purchase', '2019-07-31', '402', 'Company Credit For  global band ', '0.00', '0.00', '1', '1', '2019-07-31 11:44:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('240', '20190731114458', 'Purchase', '2019-07-31', '1020101', 'Cash in Hand For Supplier global band ', '0.00', '0.00', '1', '1', '2019-07-31 11:44:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('241', '20190731114458', 'Purchase', '2019-07-31', '5020200002', 'Supplier .global band ', '0.00', '0.00', '1', '1', '2019-07-31 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('242', '20190731114746', 'Purchase', '2019-07-31', '10107', 'Inventory Debit For Supplier Test Supplier', '2039100.00', '0.00', '1', '1', '2019-07-31 11:47:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('243', '20190731114746', 'Purchase', '2019-07-31', '5020200001', 'Supplier .Test Supplier', '0.00', '2039100.00', '1', '1', '2019-07-31 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('244', '20190731114746', 'Purchase', '2019-07-31', '402', 'Company Credit For  Test Supplier', '2039100.00', '0.00', '1', '1', '2019-07-31 11:47:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('245', '20190731114746', 'Purchase', '2019-07-31', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '2039100.00', '1', '1', '2019-07-31 11:47:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('246', '20190731114746', 'Purchase', '2019-07-31', '5020200001', 'Supplier .Test Supplier', '2039100.00', '0.00', '1', '1', '2019-07-31 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('247', '5457199982', 'INV', '2019-07-31', '10107', 'Inventory credit For Invoice No5457199982', '0.00', '73997.50', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('248', '5457199982', 'INV', '2019-07-31', '102030134', 'Customer debit For  Walking customer', '195465.00', '0.00', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('249', '5457199982', 'INVOICE', '2019-07-31', '303', 'Sale Income For Walking customer', '0.00', '195465.00', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('250', '5457199982', 'INV', '2019-07-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '195465.00', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('251', '5457199982', 'INV', '2019-07-31', '1020101', 'Cash in Hand in Sale for Walking customer', '195465.00', '0.00', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('252', '2362693843', 'INV', '2019-07-31', '10107', 'Inventory credit For Invoice No2362693843', '0.00', '30000.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('253', '2362693843', 'INV', '2019-07-31', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('254', '2362693843', 'INVOICE', '2019-07-31', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('255', '2362693843', 'INV', '2019-07-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('256', '2362693843', 'INV', '2019-07-31', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('262', '5763389422', 'INV', '2019-07-31', '10107', 'Inventory credit For Invoice No5763389422', '0.00', '352.50', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('263', '5763389422', 'INVOICE', '2019-07-31', '303', 'Sale Income From Walking customer', '0.00', '455.00', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('264', '5763389422', 'INV', '2019-07-31', '102030134', 'Customer debit For Walking customer', '455.00', '0.00', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('265', '5763389422', 'INV', '2019-07-31', '102030134', 'Customer credit for Paid Amount For Walking customer', '0.00', '455.00', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('266', '5763389422', 'INV', '2019-07-31', '1020101', 'Cash in Hand for sale for Walking customer', '455.00', '0.00', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('267', '6921223925', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No6921223925', '0.00', '40352.50', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('268', '6921223925', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '50455.00', '0.00', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('269', '6921223925', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '50455.00', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('270', '6921223925', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50455.00', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('271', '6921223925', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '50455.00', '0.00', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('272', '2992478698', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No2992478698', '0.00', '40502.50', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('273', '2992478698', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '50655.00', '0.00', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('274', '2992478698', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '50655.00', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('275', '2992478698', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50655.00', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('276', '2992478698', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '50655.00', '0.00', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('277', '2429117215', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No2429117215', '0.00', '41502.50', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('278', '2429117215', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '51855.00', '0.00', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('279', '2429117215', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '51855.00', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('280', '2429117215', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '51855.00', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('281', '2429117215', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '51855.00', '0.00', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('282', '8636821614', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No8636821614', '0.00', '70000.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('283', '8636821614', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '85000.00', '0.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('284', '8636821614', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '85000.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('285', '8636821614', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '85000.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('286', '8636821614', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '85000.00', '0.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('287', '2274125738', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No2274125738', '0.00', '40000.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('288', '2274125738', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '50000.00', '0.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('289', '2274125738', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '50000.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('290', '2274125738', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50000.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('291', '2274125738', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '50000.00', '0.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('292', 'PM-2', 'PM', '2019-08-01', '5020200001', 'Paid to Test Supplier', '200.00', '0.00', '1', '1', '2019-08-01 07:25:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('293', 'PM-2', 'PM', '2019-08-01', '1020101', 'Paid to Test Supplier', '0.00', '200.00', '1', '1', '2019-08-01 07:25:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('294', '6881727828', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No6881727828', '0.00', '352.50', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('295', '6881727828', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '455.00', '0.00', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('296', '6881727828', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '455.00', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('297', '6881727828', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '455.00', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('298', '6881727828', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '455.00', '0.00', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('299', 'WCAX81XD3R', 'Advance', '2019-08-03', '5020200001', 'supplier Advance For Test Supplier', '1000.00', '0.00', '1', NULL, '2019-08-03 06:08:24', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('300', 'WCAX81XD3R', 'Advance', '2019-08-03', '1020101', 'Cash in Hand  For Test Supplier Advance', '1000.00', '0.00', '1', NULL, '2019-08-03 06:08:24', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('301', 'LRRQ4L5FO1', 'PR Balance', '2019-08-03', '10107', 'Inventory credit For  ovi', '10.00', '0.00', '1', NULL, '2019-08-03 10:53:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('302', 'Z9IYRG1MRI', 'PR Balance', '2019-08-03', '10107', 'Inventory credit For  rttt', '1.00', '0.00', '1', NULL, '2019-08-03 10:54:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('303', '6942233633', 'INV', '2019-08-03', '10107', 'Inventory credit For Invoice No6942233633', '0.00', '150.00', '1', '1', '2019-08-03 11:23:56', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('304', '6942233633', 'INV', '2019-08-03', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-08-03 11:23:56', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('305', '6942233633', 'INVOICE', '2019-08-03', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-08-03 11:23:56', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('306', '7167684116', 'INV', '2019-08-03', '10107', 'Inventory credit For Invoice No7167684116', '0.00', '2000.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('307', '7167684116', 'INV', '2019-08-03', '102030134', 'Customer debit For  Walking customer', '2600.00', '0.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('308', '7167684116', 'INVOICE', '2019-08-03', '303', 'Sale Income For Walking customer', '0.00', '2600.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('309', '7167684116', 'INV', '2019-08-03', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '2600.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('310', '7167684116', 'INV', '2019-08-03', '1020101', 'Cash in Hand in Sale for Walking customer', '2600.00', '0.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('311', '20190804092551', 'Purchase', '2019-08-04', '10107', 'Inventory Debit For Supplier test', '400.00', '0.00', '1', '1', '2019-08-04 09:25:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('312', '20190804092551', 'Purchase', '2019-08-04', '5020200003', 'Supplier .test', '0.00', '400.00', '1', '1', '2019-08-04 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('313', '20190804092551', 'Purchase', '2019-08-04', '402', 'Company Credit For  test', '400.00', '0.00', '1', '1', '2019-08-04 09:25:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('314', '20190804092551', 'Purchase', '2019-08-04', '1020101', 'Cash in Hand For Supplier test', '0.00', '400.00', '1', '1', '2019-08-04 09:25:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('315', '20190804092551', 'Purchase', '2019-08-04', '5020200003', 'Supplier .test', '400.00', '0.00', '1', '1', '2019-08-04 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('316', 'Z243GXXO9F', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For sdasds Advance', '0.00', '0.00', '1', NULL, '2019-08-06 07:05:42', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('317', 'PNW2N33NCX', 'Advance', '2019-08-06', '5020200003', 'supplier Advance For test', '232.00', '0.00', '1', NULL, '2019-08-06 07:06:45', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('318', 'PNW2N33NCX', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For test Advance', '232.00', '0.00', '1', NULL, '2019-08-06 07:06:45', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('319', 'IF2K2MG62S', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For  Advance', '232.00', '0.00', '1', NULL, '2019-08-06 10:45:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('320', '5OMZ3G1GUN', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For  Advance', '232.00', '0.00', '1', NULL, '2019-08-06 10:46:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('321', 'L26HEBCZJM', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For  Advance', '232.00', '0.00', '1', NULL, '2019-08-06 10:46:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('322', '20190807091741', 'Purchase', '2019-08-07', '10107', 'Inventory Debit For Supplier fghfg', '3000.00', '0.00', '1', '1', '2019-08-07 09:17:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('323', '20190807091741', 'Purchase', '2019-08-07', '402', 'Company Credit For  fghfg', '3000.00', '0.00', '1', '1', '2019-08-07 09:17:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('324', '20190807091741', 'Purchase', '2019-08-07', '1020101', 'Cash in Hand For Supplier fghfg', '0.00', '3000.00', '1', '1', '2019-08-07 09:17:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('325', '20190807104826', 'Purchase', '2019-08-07', '10107', 'Inventory Debit For Supplier fghfg', '9000.00', '0.00', '1', '1', '2019-08-07 10:48:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('326', '20190807104826', 'Purchase', '2019-08-07', '402', 'Company Credit For  fghfg', '9000.00', '0.00', '1', '1', '2019-08-07 10:48:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('327', '20190807104826', 'Purchase', '2019-08-07', '1020101', 'Cash in Hand For Supplier fghfg', '0.00', '9000.00', '1', '1', '2019-08-07 10:48:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('328', '2PSD6PZRDY', 'Advance', '2019-08-07', '1020101', 'Cash in Hand  For fghfg Advance', '5099.00', '0.00', '1', NULL, '2019-08-07 15:41:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('329', 'MBJEQBLEDT', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoMBJEQBLEDT', '0.00', '150.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('330', 'MBJEQBLEDT', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('331', 'MBJEQBLEDT', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('332', 'MBJEQBLEDT', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('333', 'MBJEQBLEDT', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('334', '3OXFJU9FLE', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice No3OXFJU9FLE', '0.00', '150.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('335', '3OXFJU9FLE', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('336', '3OXFJU9FLE', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('337', '3OXFJU9FLE', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('338', '3OXFJU9FLE', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('339', 'AEYS0XT6FB', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoAEYS0XT6FB', '0.00', '150.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('340', 'AEYS0XT6FB', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '150.00', '0.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('341', 'AEYS0XT6FB', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '150.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('342', 'AEYS0XT6FB', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('343', 'AEYS0XT6FB', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('344', 'IFU2EJUMSR', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoIFU2EJUMSR', '0.00', '150.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('345', 'IFU2EJUMSR', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('346', 'IFU2EJUMSR', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('347', 'IFU2EJUMSR', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('348', 'IFU2EJUMSR', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('349', 'TD6SQ1IDPR', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoTD6SQ1IDPR', '0.00', '150.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('350', 'TD6SQ1IDPR', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('351', 'TD6SQ1IDPR', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('352', 'TD6SQ1IDPR', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('353', 'TD6SQ1IDPR', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('354', 'B8P4EGGRAY', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoB8P4EGGRAY', '0.00', '150.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('355', 'B8P4EGGRAY', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('356', 'B8P4EGGRAY', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('357', 'B8P4EGGRAY', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('358', 'B8P4EGGRAY', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('359', 'R8OMFVYJXQ', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoR8OMFVYJXQ', '0.00', '150.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('360', 'R8OMFVYJXQ', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('361', 'R8OMFVYJXQ', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('362', 'R8OMFVYJXQ', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('363', 'R8OMFVYJXQ', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('364', 'VYMH47RI7Y', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoVYMH47RI7Y', '0.00', '150.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('365', 'VYMH47RI7Y', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '150.00', '0.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('366', 'VYMH47RI7Y', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '150.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('367', 'VYMH47RI7Y', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('368', 'VYMH47RI7Y', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('369', '3FQSMSSQDT', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice No3FQSMSSQDT', '0.00', '150.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('370', '3FQSMSSQDT', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('371', '3FQSMSSQDT', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('372', '3FQSMSSQDT', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('373', '3FQSMSSQDT', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('374', 'JTXNMIW0DT', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoJTXNMIW0DT', '0.00', '150.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('375', 'JTXNMIW0DT', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('376', 'JTXNMIW0DT', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('377', 'JTXNMIW0DT', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('378', 'JTXNMIW0DT', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('379', 'TJSXGFD1F6', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoTJSXGFD1F6', '0.00', '150.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('380', 'TJSXGFD1F6', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('381', 'TJSXGFD1F6', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('382', 'TJSXGFD1F6', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('383', 'TJSXGFD1F6', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('384', '1AGGWW2DR6', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice No1AGGWW2DR6', '0.00', '150.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('385', '1AGGWW2DR6', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('386', '1AGGWW2DR6', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('387', '1AGGWW2DR6', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('388', '1AGGWW2DR6', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('389', 'FO9Q0CZBAH', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoFO9Q0CZBAH', '0.00', '150.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('390', 'FO9Q0CZBAH', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('391', 'FO9Q0CZBAH', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('392', 'FO9Q0CZBAH', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('393', 'FO9Q0CZBAH', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('394', 'CYN1QFPGZE', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoCYN1QFPGZE', '0.00', '150.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('395', 'CYN1QFPGZE', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('396', 'CYN1QFPGZE', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('397', 'CYN1QFPGZE', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('398', 'CYN1QFPGZE', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('399', 'QE2YQFXLDA', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoQE2YQFXLDA', '0.00', '0.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('400', 'QE2YQFXLDA', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('401', 'QE2YQFXLDA', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('402', 'QE2YQFXLDA', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('403', 'QE2YQFXLDA', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('404', 'JJ75B2XUKG', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoJJ75B2XUKG', '0.00', '150.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('405', 'JJ75B2XUKG', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('406', 'JJ75B2XUKG', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('407', 'JJ75B2XUKG', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('408', 'JJ75B2XUKG', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('409', 'ZNOOWPHIKK', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoZNOOWPHIKK', '0.00', '300.00', '1', '1', '2019-08-31 05:39:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('410', 'ZNOOWPHIKK', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:39:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('411', 'ZNOOWPHIKK', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:39:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('412', 'KYNKY2F1EI', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoKYNKY2F1EI', '0.00', '300.00', '1', '1', '2019-08-31 05:39:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('413', 'KYNKY2F1EI', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:39:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('414', 'KYNKY2F1EI', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:39:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('415', 'R6BFIRTLMP', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoR6BFIRTLMP', '0.00', '0.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('416', 'R6BFIRTLMP', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('417', 'R6BFIRTLMP', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('418', 'R6BFIRTLMP', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '150.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('419', 'R6BFIRTLMP', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '150.00', '0.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('420', 'HUJOSOYQPV', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoHUJOSOYQPV', '0.00', '0.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('421', 'HUJOSOYQPV', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('422', 'HUJOSOYQPV', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('423', 'HUJOSOYQPV', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '150.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('424', 'HUJOSOYQPV', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '150.00', '0.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('425', 'SOQYFD5DVR', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoSOQYFD5DVR', '0.00', '0.00', '1', '1', '2019-08-31 05:48:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('426', 'SOQYFD5DVR', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:48:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('427', 'SOQYFD5DVR', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:48:59', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('428', 'HBW2WPWYHE', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoHBW2WPWYHE', '0.00', '0.00', '1', '1', '2019-08-31 05:49:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('429', 'HBW2WPWYHE', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:49:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('430', 'HBW2WPWYHE', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:49:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('431', 'EMRLGS5W57', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoEMRLGS5W57', '0.00', '0.00', '1', '1', '2019-08-31 05:51:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('432', 'EMRLGS5W57', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:51:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('433', 'EMRLGS5W57', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:51:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('434', 'CEZC44VKAW', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoCEZC44VKAW', '0.00', '0.00', '1', '1', '2019-08-31 05:52:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('435', 'CEZC44VKAW', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:52:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('436', 'CEZC44VKAW', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:52:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('437', 'RWKKAW90BR', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoRWKKAW90BR', '0.00', '0.00', '1', '1', '2019-08-31 05:53:24', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('438', 'RWKKAW90BR', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:53:24', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('439', 'RWKKAW90BR', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:53:24', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('440', 'NDQSPVYGQR', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoNDQSPVYGQR', '0.00', '0.00', '1', '1', '2019-08-31 05:53:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('441', 'NDQSPVYGQR', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:53:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('442', 'NDQSPVYGQR', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:53:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('443', 'WPOGYFR2EJ', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoWPOGYFR2EJ', '0.00', '0.00', '1', '1', '2019-08-31 07:11:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('444', 'WPOGYFR2EJ', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 07:11:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('445', 'WPOGYFR2EJ', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 07:11:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('446', 'GHKCY7BNB8', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoGHKCY7BNB8', '0.00', '0.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('447', 'GHKCY7BNB8', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '1000.00', '0.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('448', 'GHKCY7BNB8', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '1000.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('449', 'GHKCY7BNB8', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('450', 'GHKCY7BNB8', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('451', '7FE92ZSJVO', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No7FE92ZSJVO', '0.00', '0.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('452', '7FE92ZSJVO', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('453', '7FE92ZSJVO', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('454', '7FE92ZSJVO', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '150.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('455', '7FE92ZSJVO', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '150.00', '0.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('456', 'ETAJFQYI61', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoETAJFQYI61', '0.00', '0.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('457', 'ETAJFQYI61', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('458', 'ETAJFQYI61', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('459', 'ETAJFQYI61', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('460', 'ETAJFQYI61', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('461', '4GGW3BKYYX', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No4GGW3BKYYX', '0.00', '0.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('462', '4GGW3BKYYX', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('463', '4GGW3BKYYX', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('464', '4GGW3BKYYX', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('465', '4GGW3BKYYX', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('466', 'IMXKKV3CWH', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoIMXKKV3CWH', '0.00', '0.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('467', 'IMXKKV3CWH', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('468', 'IMXKKV3CWH', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('469', 'IMXKKV3CWH', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('470', 'IMXKKV3CWH', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('471', 'OBRAR2MS1B', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoOBRAR2MS1B', '0.00', '0.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('472', 'OBRAR2MS1B', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('473', 'OBRAR2MS1B', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('474', 'OBRAR2MS1B', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('475', 'OBRAR2MS1B', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('476', 'LKHINKOQEZ', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoLKHINKOQEZ', '0.00', '0.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('477', 'LKHINKOQEZ', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('478', 'LKHINKOQEZ', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('479', 'LKHINKOQEZ', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('480', 'LKHINKOQEZ', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('481', 'K5ZUAPRFM3', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoK5ZUAPRFM3', '0.00', '0.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('482', 'K5ZUAPRFM3', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('483', 'K5ZUAPRFM3', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('484', 'K5ZUAPRFM3', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('485', 'K5ZUAPRFM3', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('486', 'ZVEQPUB12A', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoZVEQPUB12A', '0.00', '780.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('487', 'ZVEQPUB12A', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('488', 'ZVEQPUB12A', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('489', 'ZVEQPUB12A', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('490', 'ZVEQPUB12A', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('491', '7NOYHECZKX', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No7NOYHECZKX', '0.00', '0.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('492', '7NOYHECZKX', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('493', '7NOYHECZKX', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('494', '7NOYHECZKX', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('495', '7NOYHECZKX', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('496', 'LULH5EJTKL', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoLULH5EJTKL', '0.00', '0.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('497', 'LULH5EJTKL', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('498', 'LULH5EJTKL', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('499', 'LULH5EJTKL', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('500', 'LULH5EJTKL', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('501', 'PGJOTFJMBS', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoPGJOTFJMBS', '0.00', '780.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('502', 'PGJOTFJMBS', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('503', 'PGJOTFJMBS', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('504', 'PGJOTFJMBS', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('505', 'PGJOTFJMBS', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('506', 'G52D4TNFF9', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoG52D4TNFF9', '0.00', '0.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('507', 'G52D4TNFF9', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '1000.00', '0.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('508', 'G52D4TNFF9', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '1000.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('509', 'G52D4TNFF9', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('510', 'G52D4TNFF9', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('511', '4CP6Q9TXQE', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No4CP6Q9TXQE', '0.00', '0.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('512', '4CP6Q9TXQE', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('513', '4CP6Q9TXQE', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('514', '4CP6Q9TXQE', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('515', '4CP6Q9TXQE', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('516', 'LRYVFDX4LA', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoLRYVFDX4LA', '0.00', '780.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('517', 'LRYVFDX4LA', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('518', 'LRYVFDX4LA', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('519', 'LRYVFDX4LA', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('520', 'LRYVFDX4LA', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('521', 'VHASYKN18J', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoVHASYKN18J', '0.00', '780.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('522', 'VHASYKN18J', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('523', 'VHASYKN18J', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('524', 'VHASYKN18J', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('525', 'VHASYKN18J', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('526', 'QV7GKHPOAT', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoQV7GKHPOAT', '0.00', '780.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('527', 'QV7GKHPOAT', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('528', 'QV7GKHPOAT', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('529', 'QV7GKHPOAT', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('530', 'QV7GKHPOAT', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('531', 'TTBSFBYWQS', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoTTBSFBYWQS', '0.00', '0.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('532', 'TTBSFBYWQS', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('533', 'TTBSFBYWQS', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('534', 'TTBSFBYWQS', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '15.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('535', 'TTBSFBYWQS', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '15.00', '0.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('536', 'D3LGRQ7BOJ', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoD3LGRQ7BOJ', '0.00', '68015.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('537', 'D3LGRQ7BOJ', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('538', 'D3LGRQ7BOJ', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('539', 'D3LGRQ7BOJ', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('540', 'D3LGRQ7BOJ', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('541', '9EIQL1SSY3', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No9EIQL1SSY3', '0.00', '67515.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('542', '9EIQL1SSY3', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('543', '9EIQL1SSY3', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('544', '9EIQL1SSY3', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('545', '9EIQL1SSY3', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('546', 'YIBCDHM1DF', 'PR Balance', '2019-08-31', '10107', 'Inventory credit For  ovi', '100.00', '0.00', '1', NULL, '2019-08-31 12:16:20', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('547', 'AHLH2FBCER', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoAHLH2FBCER', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('548', 'AHLH2FBCER', 'INV', '2019-09-02', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('549', 'AHLH2FBCER', 'INVOICE', '2019-09-02', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('550', 'AHLH2FBCER', 'INV', '2019-09-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('551', 'AHLH2FBCER', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('552', 'L48BYLQOYA', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoL48BYLQOYA', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('553', 'L48BYLQOYA', 'INV', '2019-09-02', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('554', 'L48BYLQOYA', 'INVOICE', '2019-09-02', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('555', 'L48BYLQOYA', 'INV', '2019-09-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('556', 'L48BYLQOYA', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('557', 'TKVN9MS0S8', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoTKVN9MS0S8', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('558', 'TKVN9MS0S8', 'INV', '2019-09-02', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('559', 'TKVN9MS0S8', 'INVOICE', '2019-09-02', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('560', 'TKVN9MS0S8', 'INV', '2019-09-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('561', 'TKVN9MS0S8', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('562', 'XN5CZSOQT1', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoXN5CZSOQT1', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:40:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('563', 'XN5CZSOQT1', 'INVOICE', '2019-09-02', '303', 'Sale Income For kobir', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:40:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('564', 'XN5CZSOQT1', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for kobir', '35080.00', '0.00', '1', 'erterte', '2019-09-02 10:40:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('565', 'WIJXKFBHB2', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoWIJXKFBHB2', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:40:57', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('566', 'WIJXKFBHB2', 'INVOICE', '2019-09-02', '303', 'Sale Income For kobir', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:40:57', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('567', 'WIJXKFBHB2', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for kobir', '35080.00', '0.00', '1', 'erterte', '2019-09-02 10:40:57', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('568', 'VSFEVF9CKV', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoVSFEVF9CKV', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:41:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('569', 'VSFEVF9CKV', 'INVOICE', '2019-09-02', '303', 'Sale Income For kobir', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:41:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('570', 'VSFEVF9CKV', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for kobir', '35080.00', '0.00', '1', 'erterte', '2019-09-02 10:41:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('571', 'JPPFABLKAT', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoJPPFABLKAT', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:41:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('572', 'JPPFABLKAT', 'INVOICE', '2019-09-02', '303', 'Sale Income For kobir', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:41:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('573', 'JPPFABLKAT', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for kobir', '35080.00', '0.00', '1', 'erterte', '2019-09-02 10:41:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('610', '20190903053506', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:35:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('611', '20190903053506', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:35:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('612', '20190903053506', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:35:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('613', '20190903053622', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:36:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('614', '20190903053622', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:36:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('615', '20190903053622', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:36:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('616', '20190903053809', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:38:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('617', '20190903053809', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:38:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('618', '20190903053809', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:38:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('619', '20190903053848', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:38:48', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('620', '20190903053848', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:38:48', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('621', '20190903053848', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:38:48', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('622', '20190903053939', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:39:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('623', '20190903053939', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:39:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('624', '20190903053939', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:39:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('631', '20190903054203', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:42:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('632', '20190903054203', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:42:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('633', '20190903054203', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:42:03', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('634', '20190903054240', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:42:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('635', '20190903054240', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:42:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('636', '20190903054240', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:42:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('637', '20190903054128', 'Purchase', '2019-09-03', '10107', 'Inventory Devit Supplier ovi', '68.00', '0.00', '1', '1', '2019-09-03 05:45:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('638', '20190903054128', 'Purchase', '2019-09-03', '402', 'Company Credit For Supplierovi', '68.00', '0.00', '1', '1', '2019-09-03 05:45:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('639', '20190903054128', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '68.00', '1', '1', '2019-09-03 05:45:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('640', '20190903054749', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:47:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('641', '20190903054749', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:47:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('642', '20190903054749', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:47:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('643', '20190903055022', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:50:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('644', '20190903055022', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:50:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('645', '20190903055022', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:50:22', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('646', '20190903055114', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:51:14', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('647', '20190903055114', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:51:14', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('648', '20190903055114', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:51:14', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('649', '20190903055139', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:51:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('650', '20190903055139', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:51:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('651', '20190903055139', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:51:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('670', '20190903061451', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '960.00', '0.00', '1', NULL, '2019-09-03 06:14:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('671', '20190903061451', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '960.00', '0.00', '1', NULL, '2019-09-03 06:14:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('672', '20190903061451', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '960.00', '1', NULL, '2019-09-03 06:14:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('676', '20190903094416', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '27303.00', '0.00', '1', NULL, '2019-09-03 09:44:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('677', '20190903094416', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '27303.00', '0.00', '1', NULL, '2019-09-03 09:44:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('678', '20190903094416', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '27303.00', '1', NULL, '2019-09-03 09:44:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('679', '20190904070737', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '30.00', '0.00', '1', NULL, '2019-09-04 07:07:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('680', '20190904070737', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '30.00', '0.00', '1', NULL, '2019-09-04 07:07:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('681', '20190904070737', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '30.00', '1', NULL, '2019-09-04 07:07:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('682', '20190904071000', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '254.00', '0.00', '1', NULL, '2019-09-04 07:10:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('683', '20190904071000', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '254.00', '0.00', '1', NULL, '2019-09-04 07:10:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('684', '20190904071000', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '254.00', '1', NULL, '2019-09-04 07:10:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('685', '20190904094916', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '60.00', '0.00', '1', NULL, '2019-09-04 09:49:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('686', '20190904094916', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '60.00', '0.00', '1', NULL, '2019-09-04 09:49:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('687', '20190904094916', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '60.00', '1', NULL, '2019-09-04 09:49:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('688', '20190904103141', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '472.00', '0.00', '1', NULL, '2019-09-04 10:31:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('689', '20190904103141', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '472.00', '0.00', '1', NULL, '2019-09-04 10:31:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('690', '20190904103141', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '472.00', '1', NULL, '2019-09-04 10:31:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('691', '20190904103318', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '60.00', '0.00', '1', NULL, '2019-09-04 10:33:18', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('692', '20190904103318', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '60.00', '0.00', '1', NULL, '2019-09-04 10:33:18', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('693', '20190904103318', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '60.00', '1', NULL, '2019-09-04 10:33:18', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('724', '20190904112702', 'Purchase', '2019-09-05', '10107', 'Inventory Debit For Supplier ovi', '1089.00', '0.00', '1', NULL, '2019-09-05 04:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('725', '20190904112702', 'Purchase', '2019-09-05', '402', 'Company Credit For  ovi', '1089.00', '0.00', '1', NULL, '2019-09-05 04:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('726', '20190904112702', 'Purchase', '2019-09-05', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '1089.00', '1', NULL, '2019-09-05 04:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('727', 'TA1UKNEC4K', 'INV', '2019-09-05', '10107', 'Inventory credit For Invoice NoTA1UKNEC4K', '0.00', '0.00', '1', '', '2019-09-05 05:35:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('728', 'TA1UKNEC4K', 'INVOICE', '2019-09-05', '303', 'Sale Income For ', '0.00', '0.00', '1', '', '2019-09-05 05:35:13', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('729', 'PM-3', 'PM', '2019-08-05', '5', 'test remarks', '1.00', '0.00', '1', '1', '2019-09-05 06:03:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('730', 'PM-3', 'PM', '2019-08-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '1.00', '1', '1', '2019-09-05 06:03:36', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('731', 'PM-3', 'PM', '2019-08-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:05:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('732', 'PM-3', 'PM', '2019-08-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:05:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('733', 'PM-3', 'PM', '2019-09-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:08:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('734', 'PM-3', 'PM', '2019-09-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:08:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('735', 'PM-3', 'PM', '2019-09-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:10:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('736', 'PM-3', 'PM', '2019-09-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:10:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('737', 'PM-3', 'PM', '2019-09-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:17:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('738', 'PM-3', 'PM', '2019-09-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:17:35', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('739', 'PM-3', 'PM', '2019-09-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:21:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('740', 'PM-3', 'PM', '2019-09-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:21:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('741', 'PM-4', 'PM', '2019-09-05', '5020200005', 'Paid to isahaq', '200.00', '0.00', '1', '1', '2019-09-05 06:59:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('742', 'PM-4', 'PM', '2019-09-05', '1020101', 'Paid to isahaq', '0.00', '200.00', '1', '1', '2019-09-05 06:59:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('743', 'PM-5', 'PM', '2019-09-05', '5020200004', 'Paid to shahabuddin', '324234.00', '0.00', '1', '1', '2019-09-05 13:43:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('744', 'PM-6', 'PM', '2019-09-05', '1020101', 'Paid to shahabuddin', '0.00', '324234.00', '1', '1', '2019-09-05 13:43:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('745', '', 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 09:55:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('746', '', 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 09:55:54', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('747', '', 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 09:59:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('748', '', 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 09:59:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('749', '', 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 10:30:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('750', '', 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 10:30:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('751', '', 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 10:36:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('752', '', 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 10:36:12', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('753', NULL, 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 10:39:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('754', NULL, 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 10:39:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('755', 'PM-7', 'PM', '2019-09-15', '', '', '0.00', '0.00', '1', '1', '2019-09-15 10:42:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('756', 'PM-7', 'PM', '2019-09-15', '1020101', 'Cash in Hand For Voucher NoPM-7', '0.00', '0.00', '1', '1', '2019-09-15 10:42:02', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('757', 'KFHXF3UIWU', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoKFHXF3UIWU', '0.00', '0.00', '1', '', '2019-09-18 05:39:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('758', 'KFHXF3UIWU', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:39:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('759', 'KFHXF3UIWU', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:39:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('760', 'K4GNPYU4A6', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoK4GNPYU4A6', '0.00', '0.00', '1', '', '2019-09-18 05:40:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('761', 'K4GNPYU4A6', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:40:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('762', 'K4GNPYU4A6', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:40:01', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('763', 'ETZ0PPT6LS', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoETZ0PPT6LS', '0.00', '0.00', '1', '', '2019-09-18 05:50:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('764', 'ETZ0PPT6LS', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:50:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('765', 'ETZ0PPT6LS', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:50:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('766', 'RH1VWKB0F4', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoRH1VWKB0F4', '0.00', '0.00', '1', '', '2019-09-18 06:38:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('767', 'RH1VWKB0F4', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 06:38:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('768', 'RH1VWKB0F4', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 06:38:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('769', 'KHMCMEOOXT', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoKHMCMEOOXT', '0.00', '0.00', '1', '', '2019-09-18 07:03:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('770', 'KHMCMEOOXT', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:03:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('771', 'KHMCMEOOXT', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:03:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('772', '1PGIEIHMX8', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice No1PGIEIHMX8', '0.00', '0.00', '1', '', '2019-09-18 07:25:05', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('773', '1PGIEIHMX8', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:25:05', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('774', '1PGIEIHMX8', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:25:05', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('775', 'PFYCGTPW4B', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoPFYCGTPW4B', '0.00', '0.00', '1', '', '2019-09-18 07:27:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('776', 'PFYCGTPW4B', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:27:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('777', 'PFYCGTPW4B', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:27:44', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('778', 'YNDSZTUPBE', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoYNDSZTUPBE', '0.00', '0.00', '1', '', '2019-09-18 07:31:18', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('779', 'YNDSZTUPBE', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:31:18', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('780', 'YNDSZTUPBE', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:31:18', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('781', '3LDBLLXXXC', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice No3LDBLLXXXC', '0.00', '0.00', '1', '', '2019-09-18 07:42:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('782', '3LDBLLXXXC', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:42:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('783', '3LDBLLXXXC', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:42:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('784', 'P0HMDJWIUP', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoP0HMDJWIUP', '0.00', '0.00', '1', '', '2019-09-18 07:42:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('785', 'P0HMDJWIUP', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:42:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('786', 'P0HMDJWIUP', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:42:46', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('787', 'KMWPIXOENF', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoKMWPIXOENF', '0.00', '0.00', '1', '', '2019-09-18 07:47:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('788', 'KMWPIXOENF', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:47:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('789', 'KMWPIXOENF', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:47:16', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('790', 'FSRL6Y46ZE', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoFSRL6Y46ZE', '0.00', '0.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('791', 'FSRL6Y46ZE', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('792', 'FSRL6Y46ZE', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('793', 'FSRL6Y46ZE', 'INV', '2019-09-18', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '1200.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('794', 'FSRL6Y46ZE', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Walking customer', '1200.00', '0.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('795', 'A2YLFCEYN0', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoA2YLFCEYN0', '0.00', '0.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('796', 'A2YLFCEYN0', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('797', 'A2YLFCEYN0', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('798', 'A2YLFCEYN0', 'INV', '2019-09-18', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '210500.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('799', 'A2YLFCEYN0', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Walking customer', '210500.00', '0.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('800', 'LFREZ3GFUG', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoLFREZ3GFUG', '0.00', '0.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('801', 'LFREZ3GFUG', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('802', 'LFREZ3GFUG', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('803', 'LFREZ3GFUG', 'INV', '2019-09-18', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '71500.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('804', 'LFREZ3GFUG', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Walking customer', '71500.00', '0.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('805', 'RNOAKIZTU1', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoRNOAKIZTU1', '0.00', '0.00', '1', '', '2019-09-18 13:15:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('806', 'RNOAKIZTU1', 'INVOICE', '2019-09-18', '303', 'Sale Income For Isahaq', '0.00', '0.00', '1', '', '2019-09-18 13:15:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('807', 'RNOAKIZTU1', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Isahaq', '200.00', '0.00', '1', '', '2019-09-18 13:15:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('808', 'MVLCTPNENW', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoMVLCTPNENW', '0.00', '0.00', '1', '1', '2019-09-18 13:22:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('809', 'MVLCTPNENW', 'INVOICE', '2019-09-18', '303', 'Sale Income For Isahaq', '0.00', '0.00', '1', '1', '2019-09-18 13:22:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('810', 'MVLCTPNENW', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Isahaq', '1000.00', '0.00', '1', '1', '2019-09-18 13:22:19', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('811', 'A0XMF4HKBH', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoA0XMF4HKBH', '0.00', '0.00', '1', '1', '2019-09-18 13:26:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('812', 'A0XMF4HKBH', 'INVOICE', '2019-09-18', '303', 'Sale Income For Isahaq', '0.00', '0.00', '1', '1', '2019-09-18 13:26:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('813', 'A0XMF4HKBH', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Isahaq', '1500.00', '0.00', '1', '1', '2019-09-18 13:26:41', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('814', 'NO2EV8MWL4', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoNO2EV8MWL4', '0.00', '0.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('815', 'NO2EV8MWL4', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('816', 'NO2EV8MWL4', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('817', 'NO2EV8MWL4', 'INV', '2019-09-18', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '1500.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('818', 'NO2EV8MWL4', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Walking customer', '1500.00', '0.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('819', 'KAKERT7VJI', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice NoKAKERT7VJI', '0.00', '0.00', '1', '', '2019-09-19 04:20:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('820', 'KAKERT7VJI', 'INVOICE', '2019-09-19', '303', 'Sale Income For Isahaq', '0.00', '0.00', '1', '', '2019-09-19 04:20:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('821', 'KAKERT7VJI', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Isahaq', '400.00', '0.00', '1', '', '2019-09-19 04:20:47', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('822', 'MSSLK5UBVB', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice NoMSSLK5UBVB', '0.00', '34.15', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('823', 'MSSLK5UBVB', 'INV', '2019-09-19', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('824', 'MSSLK5UBVB', 'INVOICE', '2019-09-19', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('825', 'MSSLK5UBVB', 'INV', '2019-09-19', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '355.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('826', 'MSSLK5UBVB', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Walking customer', '355.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('827', 'EEAK1E4KLQ', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice NoEEAK1E4KLQ', '0.00', '159.33', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('828', 'EEAK1E4KLQ', 'INV', '2019-09-19', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('829', 'EEAK1E4KLQ', 'INVOICE', '2019-09-19', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('830', 'EEAK1E4KLQ', 'INV', '2019-09-19', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('831', 'EEAK1E4KLQ', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('832', 'KMS4ECHBZW', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice NoKMS4ECHBZW', '0.00', '17.08', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('833', 'KMS4ECHBZW', 'INV', '2019-09-19', '102030134', 'Customer debit For  Walking customer', '70000.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('834', 'KMS4ECHBZW', 'INVOICE', '2019-09-19', '303', 'Sale Income For Walking customer', '0.00', '70000.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('835', 'KMS4ECHBZW', 'INV', '2019-09-19', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '580.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('836', 'KMS4ECHBZW', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Walking customer', '580.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('837', '5147385423', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice No5147385423', '0.00', '42.69', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('838', '5147385423', 'INV', '2019-09-19', '102030140', 'Customer debit For  Hm Isahaq', '175000.00', '0.00', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('839', '5147385423', 'INVOICE', '2019-09-19', '303', 'Sale Income For Hm Isahaq', '0.00', '175000.00', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('840', '5147385423', 'INV', '2019-09-19', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '175900.00', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('841', '5147385423', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '175900.00', '0.00', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('842', '5147385423', 'Return', '2019-09-19', '102030140', 'Customer debit For  Hm Isahaq', '70000.00', '0.00', '1', '1', '2019-09-19 07:22:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('843', 'CR-1', 'CR', '2019-09-19', '1', '', '0.00', '8.00', '1', '1', '2019-09-19 08:58:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('844', 'CR-1', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '8.00', '0.00', '1', '1', '2019-09-19 08:58:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('845', 'CR-2', 'CR', '2019-09-19', '1', '', '0.00', '7.00', '1', '1', '2019-09-19 08:59:31', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('846', 'CR-2', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '7.00', '0.00', '1', '1', '2019-09-19 08:59:31', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('847', 'DV-1', 'DV', '2019-09-19', '102010201', 'sdfasdf', '0.00', '123.00', '1', NULL, '2019-09-19 09:03:23', NULL, NULL, '0');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('848', 'DV-1', 'DV', '2019-09-19', '5020200004', 'sdfasdf', '123.00', '0.00', '1', NULL, '2019-09-19 09:03:23', NULL, NULL, '0');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('849', 'CV-1', 'CV', '2019-09-19', '102010201', 'sdfasdf', '23.00', '0.00', '1', NULL, '2019-09-19 09:04:34', NULL, NULL, '0');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('850', 'CV-1', 'CV', '2019-09-19', '102030134', 'sdfasdf', '0.00', '23.00', '1', NULL, '2019-09-19 09:04:34', NULL, NULL, '0');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('851', 'CR-2', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 09:24:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('852', 'CR-2', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 09:24:55', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('853', 'CR-3', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 09:58:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('854', 'CR-3', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 09:58:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('855', 'CR-3', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:13:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('856', 'CR-3', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 10:13:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('857', 'CR-3', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:20:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('858', 'CR-4', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:20:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('859', 'CR-4', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:21:50', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('860', 'CR-5', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:22:34', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('861', 'CR-6', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:23:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('862', 'CR-6', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 10:23:40', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('863', 'CR-7', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:25:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('864', 'CR-7', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 10:25:43', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('865', 'CR-8', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:26:18', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('866', 'CR-8', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 10:26:18', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('867', '3QHCHGI5BM', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No3QHCHGI5BM', '0.00', '0.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('868', '3QHCHGI5BM', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('869', '3QHCHGI5BM', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('870', '3QHCHGI5BM', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('871', '3QHCHGI5BM', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('872', '5928235575', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No5928235575', '0.00', '8.54', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('873', '5928235575', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('874', '5928235575', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('875', '5928235575', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('876', '5928235575', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('877', '6852499599', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No6852499599', '0.00', '8.54', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('878', '6852499599', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('879', '6852499599', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('880', '6852499599', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('881', '6852499599', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('882', '7348638525', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No7348638525', '0.00', '8.54', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('883', '7348638525', 'INV', '2019-09-21', '102030140', 'Customer debit For  Hm Isahaq', '105000.00', '0.00', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('884', '7348638525', 'INVOICE', '2019-09-21', '303', 'Sale Income For Hm Isahaq', '0.00', '105000.00', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('885', '7348638525', 'INV', '2019-09-21', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '35000.00', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('886', '7348638525', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '35000.00', '0.00', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('887', '6979694511', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No6979694511', '0.00', '39.83', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('888', '6979694511', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('889', '6979694511', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('890', '6979694511', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('891', '6979694511', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('892', '7675268361', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No7675268361', '0.00', '39.83', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('893', '7675268361', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('894', '7675268361', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('895', '7675268361', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('896', '7675268361', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('897', '1699845521', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No1699845521', '0.00', '8.54', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('898', '1699845521', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('899', '1699845521', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('900', '1699845521', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('901', '1699845521', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('902', '3291158273', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No3291158273', '0.00', '8.54', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('903', '3291158273', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('904', '3291158273', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('905', '3291158273', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('906', '3291158273', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('907', '7884756971', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No7884756971', '0.00', '8.54', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('908', '7884756971', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('909', '7884756971', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('910', '7884756971', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('911', '7884756971', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('912', '9836928819', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No9836928819', '0.00', '8.54', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('913', '9836928819', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('914', '9836928819', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('915', '9836928819', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('916', '9836928819', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('917', '2466279899', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No2466279899', '0.00', '8.54', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('918', '2466279899', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('919', '2466279899', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('920', '2466279899', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('921', '2466279899', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('922', '20190924143851', 'Purchase', '2019-09-24', '10107', 'Inventory Debit For Supplier isahaq', '4500.00', '0.00', '1', '1', '2019-09-24 14:38:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('923', '20190924143851', 'Purchase', '2019-09-24', '5020200005', 'Supplier .isahaq', '0.00', '4500.00', '1', '1', '2019-09-24 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('924', '20190924143851', 'Purchase', '2019-09-24', '402', 'Company Credit For  isahaq', '4500.00', '0.00', '1', '1', '2019-09-24 14:38:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('925', '20190924143851', 'Purchase', '2019-09-24', '1020101', 'Cash in Hand For Supplier isahaq', '0.00', '4500.00', '1', '1', '2019-09-24 14:38:51', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('926', '20190924143851', 'Purchase', '2019-09-24', '5020200005', 'Supplier .isahaq', '4500.00', '0.00', '1', '1', '2019-09-24 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('927', '7126396398', 'INV', '2019-09-24', '10107', 'Inventory credit For Invoice No7126396398', '0.00', '450.00', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('928', '7126396398', 'INV', '2019-09-24', '102030134', 'Customer debit For  Walking customer', '514.90', '0.00', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('929', '7126396398', 'INVOICE', '2019-09-24', '303', 'Sale Income For Walking customer', '0.00', '514.90', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('930', '7126396398', 'INV', '2019-09-24', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '514.90', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('931', '7126396398', 'INV', '2019-09-24', '1020101', 'Cash in Hand in Sale for Walking customer', '514.90', '0.00', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('932', '20190925103121', 'Purchase', '2019-09-25', '10107', 'Inventory Debit For Supplier isahaq', '3300.00', '0.00', '1', '1', '2019-09-25 10:31:21', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('933', '20190925103121', 'Purchase', '2019-09-25', '5020200005', 'Supplier .isahaq', '0.00', '3300.00', '1', '1', '2019-09-25 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('934', '20190925103121', 'Purchase', '2019-09-25', '402', 'Company Credit For  isahaq', '3300.00', '0.00', '1', '1', '2019-09-25 10:31:21', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('935', '20190925103121', 'Purchase', '2019-09-25', '1020101', 'Cash in Hand For Supplier isahaq', '0.00', '3300.00', '1', '1', '2019-09-25 10:31:21', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('936', '20190925103121', 'Purchase', '2019-09-25', '5020200005', 'Supplier .isahaq', '3300.00', '0.00', '1', '1', '2019-09-25 00:00:00', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('937', '3496522264', 'INV', '2019-09-26', '10107', 'Inventory credit For Invoice No3496522264', '0.00', '90.00', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('938', '3496522264', 'INV', '2019-09-26', '102030134', 'Customer debit For  Walking customer', '102.98', '0.00', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('939', '3496522264', 'INVOICE', '2019-09-26', '303', 'Sale Income For Walking customer', '0.00', '102.98', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('940', '3496522264', 'INV', '2019-09-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '102.98', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('941', '3496522264', 'INV', '2019-09-26', '1020101', 'Cash in Hand in Sale for Walking customer', '102.98', '0.00', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('942', '1962163959', 'INV', '2019-09-26', '10107', 'Inventory credit For Invoice No1962163959', '0.00', '90.00', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('943', '1962163959', 'INV', '2019-09-26', '102030134', 'Customer debit For  Walking customer', '102.98', '0.00', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('944', '1962163959', 'INVOICE', '2019-09-26', '303', 'Sale Income For Walking customer', '0.00', '102.98', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('945', '1962163959', 'INV', '2019-09-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '102.98', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES ('946', '1962163959', 'INV', '2019-09-26', '1020101', 'Cash in Hand in Sale for Walking customer', '102.98', '0.00', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1');


#
# TABLE STRUCTURE FOR: account_2
#

DROP TABLE IF EXISTS `account_2`;

CREATE TABLE `account_2` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(40) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_id` int(1) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: acm_account
#

DROP TABLE IF EXISTS `acm_account`;

CREATE TABLE `acm_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: acm_invoice
#

DROP TABLE IF EXISTS `acm_invoice`;

CREATE TABLE `acm_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) NOT NULL,
  `total` float NOT NULL,
  `vat` float NOT NULL,
  `discount` float NOT NULL,
  `grand_total` float NOT NULL,
  `paid` float NOT NULL,
  `due` float NOT NULL,
  `created_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: acm_invoice_details
#

DROP TABLE IF EXISTS `acm_invoice_details`;

CREATE TABLE `acm_invoice_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `quantity` float NOT NULL,
  `price` float NOT NULL,
  `subtotal` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: acm_payment
#

DROP TABLE IF EXISTS `acm_payment`;

CREATE TABLE `acm_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `pay_to` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `amount` float NOT NULL,
  `created_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: acn_account_transaction
#

DROP TABLE IF EXISTS `acn_account_transaction`;

CREATE TABLE `acn_account_transaction` (
  `account_tran_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `transaction_description` varchar(255) NOT NULL,
  `amount` varchar(25) NOT NULL,
  `tran_date` date NOT NULL,
  `payment_id` int(11) NOT NULL,
  `create_by_id` varchar(25) NOT NULL,
  PRIMARY KEY (`account_tran_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: appointment
#

DROP TABLE IF EXISTS `appointment`;

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` varchar(20) DEFAULT NULL,
  `patient_id` varchar(20) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `serial_no` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `problem` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `refer_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: attendance
#

DROP TABLE IF EXISTS `attendance`;

CREATE TABLE `attendance` (
  `att_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `sign_in` varchar(30) NOT NULL,
  `sign_out` varchar(30) NOT NULL,
  `staytime` varchar(30) NOT NULL,
  PRIMARY KEY (`att_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bank_add
#

DROP TABLE IF EXISTS `bank_add`;

CREATE TABLE `bank_add` (
  `bank_id` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `ac_name` varchar(250) DEFAULT NULL,
  `ac_number` varchar(250) DEFAULT NULL,
  `branch` varchar(250) DEFAULT NULL,
  `signature_pic` varchar(250) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `bank_add` (`bank_id`, `bank_name`, `ac_name`, `ac_number`, `branch`, `signature_pic`, `status`) VALUES ('5AYHFKX8PU', 'Dhaka Bank ', 'Mr Md Nur', '3554553', 'chowmuhani ', NULL, '1');


#
# TABLE STRUCTURE FOR: bank_summary
#

DROP TABLE IF EXISTS `bank_summary`;

CREATE TABLE `bank_summary` (
  `bank_id` varchar(250) DEFAULT NULL,
  `description` text,
  `deposite_id` varchar(250) DEFAULT NULL,
  `date` varchar(250) DEFAULT NULL,
  `ac_type` varchar(50) DEFAULT NULL,
  `dr` float DEFAULT NULL,
  `cr` float DEFAULT NULL,
  `ammount` float DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bill
#

DROP TABLE IF EXISTS `bill`;

CREATE TABLE `bill` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bill_id` varchar(20) DEFAULT NULL,
  `bill_type` varchar(20) DEFAULT NULL,
  `bill_date` date DEFAULT NULL,
  `admission_id` varchar(20) DEFAULT NULL,
  `discount` float DEFAULT '0',
  `tax` float DEFAULT '0',
  `total` float DEFAULT '0',
  `payment_method` varchar(10) DEFAULT NULL,
  `card_cheque_no` varchar(100) DEFAULT NULL,
  `receipt_no` varchar(100) DEFAULT NULL,
  `note` text,
  `date` datetime DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 3` (`bill_id`),
  KEY `Index 2` (`admission_id`),
  CONSTRAINT `FK_ipd_bill_ipd_admission` FOREIGN KEY (`admission_id`) REFERENCES `bill_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bill_admission
#

DROP TABLE IF EXISTS `bill_admission`;

CREATE TABLE `bill_admission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admission_id` varchar(20) DEFAULT NULL,
  `patient_id` varchar(20) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `discharge_date` date DEFAULT NULL,
  `insurance_id` int(11) DEFAULT NULL,
  `policy_no` varchar(100) DEFAULT NULL,
  `agent_name` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `guardian_relation` varchar(255) DEFAULT NULL,
  `guardian_contact` varchar(255) DEFAULT NULL,
  `guardian_address` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `isComplete` tinyint(4) NOT NULL COMMENT '1=Complete and 0=Not Complete',
  `branch_id` int(11) NOT NULL,
  `assign_by` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`admission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `bill_admission` (`id`, `admission_id`, `patient_id`, `doctor_id`, `package_id`, `admission_date`, `discharge_date`, `insurance_id`, `policy_no`, `agent_name`, `guardian_name`, `guardian_relation`, `guardian_contact`, `guardian_address`, `status`, `isComplete`, `branch_id`, `assign_by`) VALUES ('1', 'UXQWE286', 'PZRIMPJ4', '38', '0', '2019-01-26', '0000-00-00', '0', '', '', 'John', 'Brother', '098765456', '', '1', '0', '0', '2');
INSERT INTO `bill_admission` (`id`, `admission_id`, `patient_id`, `doctor_id`, `package_id`, `admission_date`, `discharge_date`, `insurance_id`, `policy_no`, `agent_name`, `guardian_name`, `guardian_relation`, `guardian_contact`, `guardian_address`, `status`, `isComplete`, `branch_id`, `assign_by`) VALUES ('2', 'UTU3N48P', 'PECW0N53', '39', NULL, '2019-03-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', '0', '2');
INSERT INTO `bill_admission` (`id`, `admission_id`, `patient_id`, `doctor_id`, `package_id`, `admission_date`, `discharge_date`, `insurance_id`, `policy_no`, `agent_name`, `guardian_name`, `guardian_relation`, `guardian_contact`, `guardian_address`, `status`, `isComplete`, `branch_id`, `assign_by`) VALUES ('3', 'UL8RH8GI', 'PGQG6XSK', '39', '0', '2019-07-24', '0000-00-00', '1', '', '', '', '', '', '', '1', '0', '0', '2');


#
# TABLE STRUCTURE FOR: bill_advanced
#

DROP TABLE IF EXISTS `bill_advanced`;

CREATE TABLE `bill_advanced` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admission_id` varchar(20) DEFAULT NULL,
  `patient_id` varchar(30) DEFAULT NULL,
  `amount` float DEFAULT '0',
  `payment_method` varchar(255) DEFAULT NULL,
  `cash_card_or_cheque` varchar(10) DEFAULT NULL COMMENT '1 cash, 2 card, 3 cheque',
  `receipt_no` varchar(100) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ipd_bill_advanced_ipd_admission` (`admission_id`),
  CONSTRAINT `FK_ipd_bill_advanced_ipd_admission` FOREIGN KEY (`admission_id`) REFERENCES `bill_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bill_details
#

DROP TABLE IF EXISTS `bill_details`;

CREATE TABLE `bill_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bill_id` varchar(20) DEFAULT NULL,
  `admission_id` varchar(20) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `quantity` float DEFAULT '0',
  `amount` float DEFAULT '0',
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Key As Bill_ID` (`bill_id`),
  KEY `Admission ID` (`admission_id`),
  CONSTRAINT `fk_ipd_bill_details_ipd_admission` FOREIGN KEY (`admission_id`) REFERENCES `bill_admission` (`admission_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_ipd_bill_details_ipd_bill` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`bill_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bill_package
#

DROP TABLE IF EXISTS `bill_package`;

CREATE TABLE `bill_package` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `services` text,
  `discount` float DEFAULT '0',
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bill_service
#

DROP TABLE IF EXISTS `bill_service`;

CREATE TABLE `bill_service` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `quantity` float DEFAULT '0',
  `amount` float DEFAULT '0',
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bm_bed
#

DROP TABLE IF EXISTS `bm_bed`;

CREATE TABLE `bm_bed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `bed_number` varchar(20) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bm_bed_assign
#

DROP TABLE IF EXISTS `bm_bed_assign`;

CREATE TABLE `bm_bed_assign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial` varchar(20) DEFAULT NULL,
  `patient_id` varchar(20) NOT NULL,
  `room_id` int(11) NOT NULL,
  `bed_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `assign_date` date NOT NULL,
  `discharge_date` date DEFAULT NULL,
  `assign_by` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: bm_bed_transfer
#

DROP TABLE IF EXISTS `bm_bed_transfer`;

CREATE TABLE `bm_bed_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(6) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `room_id` int(11) NOT NULL,
  `form_bed_id` int(11) NOT NULL,
  `to_bed_id` int(11) NOT NULL,
  `assign_date` date NOT NULL,
  `discharge_date` date DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `assign_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `update_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: bm_room
#

DROP TABLE IF EXISTS `bm_room`;

CREATE TABLE `bm_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `limit` int(3) NOT NULL,
  `charge` float NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: branch_info
#

DROP TABLE IF EXISTS `branch_info`;

CREATE TABLE `branch_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: cheque_manger
#

DROP TABLE IF EXISTS `cheque_manger`;

CREATE TABLE `cheque_manger` (
  `cheque_id` varchar(100) NOT NULL,
  `transection_id` varchar(100) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `bank_id` varchar(100) NOT NULL,
  `cheque_no` varchar(100) NOT NULL,
  `date` varchar(250) DEFAULT NULL,
  `transection_type` varchar(100) NOT NULL,
  `cheque_status` int(10) NOT NULL,
  `amount` float NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: cm_patient
#

DROP TABLE IF EXISTS `cm_patient`;

CREATE TABLE `cm_patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) NOT NULL,
  `case_manager_id` int(11) NOT NULL,
  `ref_doctor_id` int(11) DEFAULT NULL,
  `hospital_name` varchar(255) DEFAULT NULL,
  `hospital_address` text,
  `doctor_name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: cm_status
#

DROP TABLE IF EXISTS `cm_status`;

CREATE TABLE `cm_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `critical_status` varchar(255) NOT NULL DEFAULT '1',
  `cm_patient_id` int(11) NOT NULL,
  `description` text,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: company_information
#

DROP TABLE IF EXISTS `company_information`;

CREATE TABLE `company_information` (
  `company_id` varchar(250) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `company_information` (`company_id`, `company_name`, `email`, `address`, `mobile`, `website`, `status`) VALUES ('1', 'coderit.net', 'coderit.net@gmail.com', 'Korim Pur Road  , Chowmuhani , Noakhali ', '01985121212 ,01730164612 015445441', 'https://www.coderit.net/', '1');


#
# TABLE STRUCTURE FOR: currency_tbl
#

DROP TABLE IF EXISTS `currency_tbl`;

CREATE TABLE `currency_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(50) NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `currency_tbl` (`id`, `currency_name`, `icon`) VALUES ('1', 'Taka', '৳');
INSERT INTO `currency_tbl` (`id`, `currency_name`, `icon`) VALUES ('2', 'Azerbaijan Manat', '₼');


#
# TABLE STRUCTURE FOR: custom_sms_info
#

DROP TABLE IF EXISTS `custom_sms_info`;

CREATE TABLE `custom_sms_info` (
  `custom_sms_id` int(11) NOT NULL AUTO_INCREMENT,
  `reciver` varchar(100) NOT NULL,
  `gateway` text NOT NULL,
  `message` text NOT NULL,
  `sms_date_time` datetime NOT NULL,
  PRIMARY KEY (`custom_sms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: customer_information
#

DROP TABLE IF EXISTS `customer_information`;

CREATE TABLE `customer_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(250) NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_mobile` varchar(100) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `status` int(2) NOT NULL COMMENT '1=paid,2=credit',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('1', '6FGVC6K7OX5L9Z1', 'Walking customer', '', '', '', '1', '2019-04-20 02:47:38');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('45', 'XWCEM9UVV5NAT5R', 'Test customer', 'Dhaka', '01550', 'testE@gmail.com2', '2', '2019-06-25 09:09:05');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('46', 'FYJ4EEBMOGTW25T', 'md abdullah', 'cumilla', '01900000000', '', '1', '2019-06-26 10:17:16');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('47', 'PVGGNNSWOESP45P', 'alexix', 'ukrain', '01200 000000', '', '1', '2019-06-26 10:52:38');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('49', 'KFTUKN7RY1O2W6R', 'samil khan', '', '', '', '1', '2019-06-28 05:47:49');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('56', 'MQ49MPE6YAYN9QN', 'kobir', 'dhaka', '123456', 'kobirdo@gmail. com', '2', '2019-09-02 10:38:33');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('51', '1IVRRX4VOCVYHOW', 'Ovi', 'fdsfsd', '02234', 'dgd@gmail.com', '2', '2019-08-01 05:58:15');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('52', '93K7WTVUD86LBTC', 'Isahaq', '', '', '', '2', '2019-08-06 05:57:28');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('53', 'TJV9RCZST65TC4B', 'demo', 'gghh', '66544333', '', '2', '2019-08-07 15:37:49');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('54', 'BIXQAYJ3XDFBYZN', 'Hm Isahaq', 'sdfadsf', '01955110016', 'hmisahaq@yahoo.com', '1', '2019-08-27 08:33:11');
INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES ('55', '7DNRCVSN8GE2S6W', 'ggfsdgh', '', '', '', '2', '2019-08-27 10:29:08');


#
# TABLE STRUCTURE FOR: customer_ledger
#

DROP TABLE IF EXISTS `customer_ledger`;

CREATE TABLE `customer_ledger` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `receipt_no` varchar(50) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT '0.00',
  `description` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `cheque_no` varchar(255) NOT NULL,
  `date` varchar(250) DEFAULT NULL,
  `receipt_from` varchar(50) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `d_c` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=284 DEFAULT CHARSET=utf8;

INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('1', 'SUBNIWB5ZF', 'XWCEM9UVV5NAT5R', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-25', NULL, '1', 'd', '2019-06-25 09:09:05');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('2', 'FS93V8F75ICGLDK', 'XWCEM9UVV5NAT5R', '3498244447', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-25', NULL, '1', 'd', '2019-06-25 09:09:28');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('3', 'FS93V8F75ICGLDK', 'XWCEM9UVV5NAT5R', '3498244447', 'EJFJS7V65M', '200.00', 'Paid by customer', '1', '', '2019-06-25', NULL, '1', 'c', '2019-06-25 09:09:28');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('4', 'Q3IZ9DRCOSZ768G', '6FGVC6K7OX5L9Z1', '4477997231', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-25', NULL, '1', 'd', '2019-06-25 09:19:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('5', 'Q3IZ9DRCOSZ768G', '6FGVC6K7OX5L9Z1', '4477997231', 'NB6KEBYU3M', '200.00', 'Paid by customer', '1', '', '2019-06-25', NULL, '1', 'c', '2019-06-25 09:19:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('6', 'QO6WH5CIWCPJ6Q1', '6FGVC6K7OX5L9Z1', '3973611118', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-25', NULL, '1', 'd', '2019-06-25 13:30:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('7', 'QO6WH5CIWCPJ6Q1', '6FGVC6K7OX5L9Z1', '3973611118', '7VOZQV8OLI', '500.00', 'Paid by customer', '1', '', '2019-06-25', NULL, '1', 'c', '2019-06-25 13:30:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('8', 'S67BGX3BCKB5VDV', '6FGVC6K7OX5L9Z1', '3923586588', NULL, '900.00', 'Purchase by customer', '', '', '2019-06-25', NULL, '1', 'd', '2019-06-25 13:52:59');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('9', 'S67BGX3BCKB5VDV', '6FGVC6K7OX5L9Z1', '3923586588', 'K2R4OPEMTM', '600.00', 'Paid by customer', '1', '', '2019-06-25', NULL, '1', 'c', '2019-06-25 13:52:59');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('10', 'F1QPDTOHWZ16P9P', 'XWCEM9UVV5NAT5R', '6575276926', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 05:34:09');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('11', 'F1QPDTOHWZ16P9P', 'XWCEM9UVV5NAT5R', '6575276926', 'UX74RVQAI8', '200.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 05:34:09');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('12', 'YYMQYK93V1ZM516', '6FGVC6K7OX5L9Z1', '6643494915', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 05:35:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('13', 'YYMQYK93V1ZM516', '6FGVC6K7OX5L9Z1', '6643494915', 'MNO2ZAMKMZ', '200.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 05:35:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('14', 'FE7CRWNMHEY38MD', '6FGVC6K7OX5L9Z1', '1969912464', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 05:36:30');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('15', 'FE7CRWNMHEY38MD', '6FGVC6K7OX5L9Z1', '1969912464', '62Y51O5X95', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 05:36:30');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('16', 'AMUTK48CY2VYX8L', '6FGVC6K7OX5L9Z1', '6691959977', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 05:37:50');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('17', 'AMUTK48CY2VYX8L', '6FGVC6K7OX5L9Z1', '6691959977', 'JBOUFCF2BI', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 05:37:50');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('18', 'HVFX6UG8VG2NR6K', '6FGVC6K7OX5L9Z1', '2258111339', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 05:38:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('19', 'HVFX6UG8VG2NR6K', '6FGVC6K7OX5L9Z1', '2258111339', '9K9F3MK86F', '200.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 05:38:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('20', 'KWTMIGM4DFV4MMN', '6FGVC6K7OX5L9Z1', '9624435359', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 05:38:35');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('21', 'KWTMIGM4DFV4MMN', '6FGVC6K7OX5L9Z1', '9624435359', '66K2LR19XD', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 05:38:35');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('22', 'O6D1W158OH1A4SO', '6FGVC6K7OX5L9Z1', '8668653724', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 05:41:41');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('23', 'O6D1W158OH1A4SO', '6FGVC6K7OX5L9Z1', '8668653724', 'XN72D3TZMP', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 05:41:41');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('24', 'ZZGD9XYMVFUFHET', '6FGVC6K7OX5L9Z1', '8558184696', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 05:55:57');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('25', 'ZZGD9XYMVFUFHET', '6FGVC6K7OX5L9Z1', '8558184696', '14F5NOZ37S', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 05:55:57');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('26', 'XPE32XHWMR1IIOD', '6FGVC6K7OX5L9Z1', '9147771963', NULL, '450.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 10:11:20');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('27', 'XPE32XHWMR1IIOD', '6FGVC6K7OX5L9Z1', '9147771963', '7232TK1WXR', '450.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 10:11:20');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('28', 'DZIV6866V4', 'FYJ4EEBMOGTW25T', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-26', NULL, '1', 'd', '2019-06-26 10:17:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('29', 'Y2GQTYTL9QPDS3S', 'FYJ4EEBMOGTW25T', '3169824785', NULL, '800.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 10:17:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('30', 'Y2GQTYTL9QPDS3S', 'FYJ4EEBMOGTW25T', '3169824785', 'YOFRJ2TAAG', '800.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 10:17:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('31', 'DIAUX4GUIHBKXM2', '6FGVC6K7OX5L9Z1', '3918198168', NULL, '1450.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 10:50:05');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('32', 'DIAUX4GUIHBKXM2', '6FGVC6K7OX5L9Z1', '3918198168', 'SIMXB999D4', '1450.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 10:50:05');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('33', 'BBDN1X3N59', 'PVGGNNSWOESP45P', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-26', NULL, '1', 'd', '2019-06-26 10:52:38');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('34', '5ES9981RI25GKC4', 'PVGGNNSWOESP45P', '5219125479', NULL, '450.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 10:52:46');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('35', '5ES9981RI25GKC4', 'PVGGNNSWOESP45P', '5219125479', 'CW6AK3SP6Q', '450.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 10:52:46');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('36', 'DI85CJ5RMXQD7HB', 'PVGGNNSWOESP45P', '8394724961', NULL, '1450.00', 'Purchase by customer', '', '', '2019-06-26', NULL, '1', 'd', '2019-06-26 10:55:06');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('37', 'DI85CJ5RMXQD7HB', 'PVGGNNSWOESP45P', '8394724961', '854LM2HXYW', '1450.00', 'Paid by customer', '1', '', '2019-06-26', NULL, '1', 'c', '2019-06-26 10:55:06');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('38', 'GUYXF9TXVLAJJEQ', '6FGVC6K7OX5L9Z1', '7986446844', NULL, '800.00', 'Purchase by customer', '', '', '2019-06-27', NULL, '1', 'd', '2019-06-27 10:58:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('39', 'GUYXF9TXVLAJJEQ', '6FGVC6K7OX5L9Z1', '7986446844', 'ZSU3NSRJMC', '800.00', 'Paid by customer', '1', '', '2019-06-27', NULL, '1', 'c', '2019-06-27 10:58:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('40', '9P35ZCUMLIEJFWO', '6FGVC6K7OX5L9Z1', '6789217583', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-27', NULL, '1', 'd', '2019-06-27 12:28:36');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('41', '9P35ZCUMLIEJFWO', '6FGVC6K7OX5L9Z1', '6789217583', 'NA14OJQGCP', '500.00', 'Paid by customer', '1', '', '2019-06-27', NULL, '1', 'c', '2019-06-27 12:28:36');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('45', 'FPBKH6VMRF', 'KFTUKN7RY1O2W6R', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-28', NULL, '1', 'd', '2019-06-28 05:47:49');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('46', 'H2INB5JLI83B2JZ', 'KFTUKN7RY1O2W6R', '2687213589', NULL, '50000.00', 'Purchase by customer', '', '', '2019-06-28', NULL, '1', 'd', '2019-06-28 05:48:12');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('47', 'H2INB5JLI83B2JZ', 'KFTUKN7RY1O2W6R', '2687213589', '9KNGG4M1XF', '30000.00', 'Paid by customer', '1', '', '2019-06-28', NULL, '1', 'c', '2019-06-28 05:48:12');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('48', 'G21MAEQN2P', 'KFTUKN7RY1O2W6R', 'NA', NULL, '111.00', 'Customer Advance', 'NA', 'NA', '2019-06-28', NULL, '1', 'd', '2019-06-28 05:51:19');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('50', 'MSAGGFLPOJDKQII', '6FGVC6K7OX5L9Z1', '3378453241', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-28', NULL, '1', 'd', '2019-06-28 06:24:01');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('51', 'MSAGGFLPOJDKQII', '6FGVC6K7OX5L9Z1', '3378453241', 'Z5FJ4HSG6M', '500.00', 'Paid by customer', '1', '', '2019-06-28', NULL, '1', 'c', '2019-06-28 06:24:01');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('52', 'STD9RZ3H1JBXP5R', '6FGVC6K7OX5L9Z1', '4458233386', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-28', NULL, '1', 'd', '2019-06-28 06:24:10');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('53', 'STD9RZ3H1JBXP5R', '6FGVC6K7OX5L9Z1', '4458233386', 'D9LQ9H5LZ1', '500.00', 'Paid by customer', '1', '', '2019-06-28', NULL, '1', 'c', '2019-06-28 06:24:10');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('54', 'UNSJVY7KRG63XFU', '6FGVC6K7OX5L9Z1', '1114135497', NULL, '2210.00', 'Purchase by customer', '', '', '2019-06-28', NULL, '1', 'd', '2019-06-28 10:57:32');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('55', '2FA13JON5GQ6T9K', '6FGVC6K7OX5L9Z1', '4441511736', NULL, '2210.00', 'Purchase by customer', '', '', '2019-06-28', NULL, '1', 'd', '2019-06-28 10:57:34');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('56', '2FA13JON5GQ6T9K', '6FGVC6K7OX5L9Z1', '4441511736', 'LMEIB4VOGV', '2210.00', 'Paid by customer', '1', '', '2019-06-28', NULL, '1', 'c', '2019-06-28 10:57:34');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('57', 'VP9EK1NHG955C8E', '6FGVC6K7OX5L9Z1', '8754679919', NULL, '50000.00', 'Purchase by customer', '', '', '2019-06-28', NULL, '1', 'd', '2019-06-28 10:58:11');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('58', 'EFZMHVZOZIHL9X9', '6FGVC6K7OX5L9Z1', '3378767423', NULL, '50000.00', 'Purchase by customer', '', '', '2019-06-28', NULL, '1', 'd', '2019-06-28 10:58:19');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('59', 'EFZMHVZOZIHL9X9', '6FGVC6K7OX5L9Z1', '3378767423', 'B42F8EMH4I', '50000.00', 'Paid by customer', '1', '', '2019-06-28', NULL, '1', 'c', '2019-06-28 10:58:19');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('60', 'FUFHSCJATY2LHY4', '6FGVC6K7OX5L9Z1', '9317933378', NULL, '50000.00', 'Purchase by customer', '', '', '2019-06-29', NULL, '1', 'd', '2019-06-29 05:30:07');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('61', 'OFTJDX79S6JKXT5', '6FGVC6K7OX5L9Z1', '1797561487', NULL, '36000.00', 'Purchase by customer', '', '', '2019-06-29', NULL, '1', 'd', '2019-06-29 11:45:22');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('62', 'OFTJDX79S6JKXT5', '6FGVC6K7OX5L9Z1', '1797561487', 'G5A4ND6CAT', '36000.00', 'Paid by customer', '1', '', '2019-06-29', NULL, '1', 'c', '2019-06-29 11:45:22');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('63', 'YKFQ14NNAGXFATU', '6FGVC6K7OX5L9Z1', '9438755655', NULL, '35000.00', 'Purchase by customer', '', '', '2019-06-30', NULL, '1', 'd', '2019-06-30 12:00:12');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('64', 'YKFQ14NNAGXFATU', '6FGVC6K7OX5L9Z1', '9438755655', '99W8I36UB1', '35000.00', 'Paid by customer', '1', '', '2019-06-30', NULL, '1', 'c', '2019-06-30 12:00:12');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('65', '126OOJOQE2XEQ26', '6FGVC6K7OX5L9Z1', '5919433886', NULL, '35000.00', 'Purchase by customer', '', '', '2019-06-30', NULL, '1', 'd', '2019-06-30 12:00:44');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('66', '126OOJOQE2XEQ26', '6FGVC6K7OX5L9Z1', '5919433886', '4T836JN4JJ', '35000.00', 'Paid by customer', '1', '', '2019-06-30', NULL, '1', 'c', '2019-06-30 12:00:44');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('67', 'B1CY534MOESSW5C', '6FGVC6K7OX5L9Z1', '2875718763', NULL, '200.00', 'Purchase by customer', '', '', '2019-07-01', NULL, '1', 'd', '2019-07-01 06:14:50');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('68', 'B1CY534MOESSW5C', '6FGVC6K7OX5L9Z1', '2875718763', '82GDNBKG11', '200.00', 'Paid by customer', '1', '', '2019-07-01', NULL, '1', 'c', '2019-07-01 06:14:50');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('69', '2V7IRO1C13I2MDW', '6FGVC6K7OX5L9Z1', '1749144575', NULL, '200.00', 'Purchase by customer', '', '', '2019-07-02', NULL, '1', 'd', '2019-07-02 05:31:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('70', '2V7IRO1C13I2MDW', '6FGVC6K7OX5L9Z1', '1749144575', 'CGN9PJCHT8', '200.00', 'Paid by customer', '1', '', '2019-07-02', NULL, '1', 'c', '2019-07-02 05:31:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('71', 'GFVMOPDBU2EB87U', '6FGVC6K7OX5L9Z1', '9815582964', NULL, '35000.00', 'Purchase by customer', '', '', '2019-07-02', NULL, '1', 'd', '2019-07-02 05:32:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('72', 'GFVMOPDBU2EB87U', '6FGVC6K7OX5L9Z1', '9815582964', '38IWAMW5QC', '35000.00', 'Paid by customer', '1', '', '2019-07-02', NULL, '1', 'c', '2019-07-02 05:32:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('73', 'SBW99OEQ3LR73IG', '6FGVC6K7OX5L9Z1', '8493678614', NULL, '455.00', 'Purchase by customer', '', '', '2019-07-02', NULL, '1', 'd', '2019-07-02 05:38:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('74', 'SBW99OEQ3LR73IG', '6FGVC6K7OX5L9Z1', '8493678614', 'ZZLJCKFRHQ', '455.00', 'Paid by customer', '1', '', '2019-07-02', NULL, '1', 'c', '2019-07-02 05:38:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('76', 'PZBG4F2CP4ZVSP8', '6FGVC6K7OX5L9Z1', '9691144514', NULL, '800.00', 'Purchase by customer', '', '', '2019-07-31', NULL, '1', 'd', '2019-07-31 11:38:51');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('77', '8SA4NBY3SH62KMW', '6FGVC6K7OX5L9Z1', '5457199982', NULL, '195465.00', 'Purchase by customer', '', '', '2019-07-31', NULL, '1', 'd', '2019-07-31 11:50:38');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('78', '8SA4NBY3SH62KMW', '6FGVC6K7OX5L9Z1', '5457199982', 'OJFJJQCNNW', '195465.00', 'Paid by customer', '1', '', '2019-07-31', NULL, '1', 'c', '2019-07-31 11:50:38');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('79', 'E64ODCUPGHL57QG', '6FGVC6K7OX5L9Z1', '2362693843', NULL, '35000.00', 'Purchase by customer', '', '', '2019-07-31', NULL, '1', 'd', '2019-07-31 13:24:11');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('80', 'E64ODCUPGHL57QG', '6FGVC6K7OX5L9Z1', '2362693843', 'XCCLA5L58X', '35000.00', 'Paid by customer', '1', '', '2019-07-31', NULL, '1', 'c', '2019-07-31 13:24:11');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('84', 'RZVZUC2CK73MFHK', '6FGVC6K7OX5L9Z1', '5763389422', NULL, '455.00', 'Purchase by customer', '1', '', '2019-07-31', NULL, '1', 'd', '2019-07-31 13:31:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('83', 'RZVZUC2CK73MFHK', '6FGVC6K7OX5L9Z1', '5763389422', '1K2EJSWXP1', '455.00', 'Paid by customer', '1', '', '2019-07-31', NULL, '1', 'c', '2019-07-31 13:31:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('85', 'UH22TNP1RNBWZMU', '6FGVC6K7OX5L9Z1', '6921223925', NULL, '50455.00', 'Purchase by customer', '', '', '2019-08-01', NULL, '1', 'd', '2019-08-01 04:46:47');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('86', 'UH22TNP1RNBWZMU', '6FGVC6K7OX5L9Z1', '6921223925', 'HEYT5JXWVX', '50455.00', 'Paid by customer', '1', '', '2019-08-01', NULL, '1', 'c', '2019-08-01 04:46:47');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('87', '2FPNZL5FLJRFEY4', '6FGVC6K7OX5L9Z1', '2992478698', NULL, '50655.00', 'Purchase by customer', '', '', '2019-08-01', NULL, '1', 'd', '2019-08-01 04:51:22');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('88', '2FPNZL5FLJRFEY4', '6FGVC6K7OX5L9Z1', '2992478698', 'GPMG1LGY4U', '50655.00', 'Paid by customer', '1', '', '2019-08-01', NULL, '1', 'c', '2019-08-01 04:51:22');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('89', 'YP2TDHXDFT7PQ56', '6FGVC6K7OX5L9Z1', '2429117215', NULL, '51855.00', 'Purchase by customer', '', '', '2019-08-01', NULL, '1', 'd', '2019-08-01 04:58:34');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('90', 'YP2TDHXDFT7PQ56', '6FGVC6K7OX5L9Z1', '2429117215', 'SSTBYE5JAM', '51855.00', 'Paid by customer', '1', '', '2019-08-01', NULL, '1', 'c', '2019-08-01 04:58:34');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('91', 'YCJUPGV3B5MBKQX', '6FGVC6K7OX5L9Z1', '8636821614', NULL, '85000.00', 'Purchase by customer', '', '', '2019-08-01', NULL, '1', 'd', '2019-08-01 05:05:19');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('92', 'YCJUPGV3B5MBKQX', '6FGVC6K7OX5L9Z1', '8636821614', '1BO21D4KYM', '85000.00', 'Paid by customer', '1', '', '2019-08-01', NULL, '1', 'c', '2019-08-01 05:05:19');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('93', 'WBUNPUTYW5', '1IVRRX4VOCVYHOW', 'NA', NULL, NULL, 'Previous adjustment with software', 'NA', 'NA', '2019-08-01', NULL, '1', 'd', '2019-08-01 05:58:15');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('94', 'DO95YM46XX8JQRL', '6FGVC6K7OX5L9Z1', '2274125738', NULL, '50000.00', 'Purchase by customer', '', '', '2019-08-01', NULL, '1', 'd', '2019-08-01 06:12:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('95', 'DO95YM46XX8JQRL', '6FGVC6K7OX5L9Z1', '2274125738', '9XYYNV7UR6', '50000.00', 'Paid by customer', '1', '', '2019-08-01', NULL, '1', 'c', '2019-08-01 06:12:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('96', 'Z1PT7RD61Z4MZ3I', '6FGVC6K7OX5L9Z1', '6881727828', NULL, '455.00', 'Purchase by customer', '', '', '2019-08-01', NULL, '1', 'd', '2019-08-01 11:46:11');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('97', 'Z1PT7RD61Z4MZ3I', '6FGVC6K7OX5L9Z1', '6881727828', '86X1REVPAW', '455.00', 'Paid by customer', '1', '', '2019-08-01', NULL, '1', 'c', '2019-08-01 11:46:11');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('98', '37P7PM8L72D6ORF', '6FGVC6K7OX5L9Z1', '6942233633', NULL, '200.00', 'Purchase by customer', '', '', '2019-08-03', NULL, '1', 'd', '2019-08-03 11:23:56');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('99', '4D6FVY1AIMSJDKW', '6FGVC6K7OX5L9Z1', '7167684116', NULL, '2600.00', 'Purchase by customer', '', '', '2019-08-03', NULL, '1', 'd', '2019-08-03 11:30:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('100', '4D6FVY1AIMSJDKW', '6FGVC6K7OX5L9Z1', '7167684116', '4N5NMFBDXU', '2600.00', 'Paid by customer', '1', '', '2019-08-03', NULL, '1', 'c', '2019-08-03 11:30:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('101', 'UIHYIVQC7V', '93K7WTVUD86LBTC', 'NA', NULL, NULL, 'Previous adjustment with software', 'NA', 'NA', '2019-08-06', NULL, '1', 'd', '2019-08-06 05:57:28');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('102', 'URRZ8DNDBW', 'TJV9RCZST65TC4B', 'NA', NULL, NULL, 'Previous adjustment with software', 'NA', 'NA', '2019-08-07', NULL, '1', 'd', '2019-08-07 15:37:49');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('103', '3K9OT4P4GQ', 'BIXQAYJ3XDFBYZN', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-08-27', NULL, '1', 'd', '2019-08-27 08:33:11');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('104', '9SQL4HDK78GTX79', 'BIXQAYJ3XDFBYZN', 'MBJEQBLEDT', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 08:35:31');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('105', '9SQL4HDK78GTX79', 'BIXQAYJ3XDFBYZN', 'MBJEQBLEDT', 'P9P3G6PXBM', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 08:35:31');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('106', 'P14K3GNFMSCCLP3', 'BIXQAYJ3XDFBYZN', '3OXFJU9FLE', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 08:38:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('107', 'P14K3GNFMSCCLP3', 'BIXQAYJ3XDFBYZN', '3OXFJU9FLE', 'EM9OWEYUFW', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 08:38:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('108', 'OE3P77I88Q91OA4', 'BIXQAYJ3XDFBYZN', 'AEYS0XT6FB', NULL, '150.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 08:57:53');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('109', 'OE3P77I88Q91OA4', 'BIXQAYJ3XDFBYZN', 'AEYS0XT6FB', 'NSIHB2WVVN', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 08:57:53');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('110', 'S4TZR8MS4HYJO1C', 'BIXQAYJ3XDFBYZN', 'IFU2EJUMSR', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:01:43');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('111', 'S4TZR8MS4HYJO1C', 'BIXQAYJ3XDFBYZN', 'IFU2EJUMSR', '3ZYUJYODR2', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:01:43');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('112', 'KMW431HG76855QH', 'BIXQAYJ3XDFBYZN', 'TD6SQ1IDPR', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:02:54');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('113', 'KMW431HG76855QH', 'BIXQAYJ3XDFBYZN', 'TD6SQ1IDPR', 'L97W4Y5T65', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:02:54');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('114', 'FQHWRZQDA26OVKK', 'BIXQAYJ3XDFBYZN', 'B8P4EGGRAY', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:04:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('115', 'FQHWRZQDA26OVKK', 'BIXQAYJ3XDFBYZN', 'B8P4EGGRAY', 'TCV44HQL19', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:04:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('116', '91CDZX4GY87FT9R', 'BIXQAYJ3XDFBYZN', 'R8OMFVYJXQ', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:05:03');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('117', '91CDZX4GY87FT9R', 'BIXQAYJ3XDFBYZN', 'R8OMFVYJXQ', 'D5A2LWYGI5', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:05:03');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('118', 'ZVMINW2RAKCCJ9K', 'BIXQAYJ3XDFBYZN', 'VYMH47RI7Y', NULL, '150.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:05:34');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('119', 'ZVMINW2RAKCCJ9K', 'BIXQAYJ3XDFBYZN', 'VYMH47RI7Y', 'EXED7HNSX1', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:05:34');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('120', 'FMSLB6U1LFMD8R8', 'BIXQAYJ3XDFBYZN', '3FQSMSSQDT', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:08:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('121', 'FMSLB6U1LFMD8R8', 'BIXQAYJ3XDFBYZN', '3FQSMSSQDT', 'AWLQDZO3ZV', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:08:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('122', '35EZAQVD9ORALB2', 'BIXQAYJ3XDFBYZN', 'JTXNMIW0DT', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:09:20');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('123', '35EZAQVD9ORALB2', 'BIXQAYJ3XDFBYZN', 'JTXNMIW0DT', '84ESG1Z1FR', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:09:20');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('124', 'NGQ3TQ5IZN6X2EB', 'BIXQAYJ3XDFBYZN', 'TJSXGFD1F6', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:11:01');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('125', 'NGQ3TQ5IZN6X2EB', 'BIXQAYJ3XDFBYZN', 'TJSXGFD1F6', '9S1YK67XMN', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:11:01');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('126', 'HVJJM7B256Y5T6B', 'BIXQAYJ3XDFBYZN', '1AGGWW2DR6', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:11:40');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('127', 'HVJJM7B256Y5T6B', 'BIXQAYJ3XDFBYZN', '1AGGWW2DR6', 'OR2FACM5KI', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:11:40');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('128', 'MFLDBMSJWC3QXK2', 'BIXQAYJ3XDFBYZN', 'FO9Q0CZBAH', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:18:23');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('129', 'MFLDBMSJWC3QXK2', 'BIXQAYJ3XDFBYZN', 'FO9Q0CZBAH', 'TH5C8D8MDM', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:18:23');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('130', '455YPJW2E7OEVEE', 'BIXQAYJ3XDFBYZN', 'CYN1QFPGZE', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:22:27');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('131', '455YPJW2E7OEVEE', 'BIXQAYJ3XDFBYZN', 'CYN1QFPGZE', 'WVC31I46Q3', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:22:27');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('132', 'J3OSS89CCGIWBQQ', 'BIXQAYJ3XDFBYZN', 'QE2YQFXLDA', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 09:57:40');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('133', 'J3OSS89CCGIWBQQ', 'BIXQAYJ3XDFBYZN', 'QE2YQFXLDA', 'R427RNN91P', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 09:57:40');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('134', 'CWIGLTXWUW', '7DNRCVSN8GE2S6W', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-08-27', NULL, '1', 'd', '2019-08-27 10:29:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('135', 'NC7539IIO4UVMEN', 'BIXQAYJ3XDFBYZN', 'JJ75B2XUKG', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, '1', 'd', '2019-08-27 13:52:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('136', 'NC7539IIO4UVMEN', 'BIXQAYJ3XDFBYZN', 'JJ75B2XUKG', '7CH87X1RYS', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, '1', 'c', '2019-08-27 13:52:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('137', 'P3QEHEBLYFL8L71', 'fgdfdsf', 'ZNOOWPHIKK', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, '1', 'd', '2019-08-31 05:39:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('138', 'P3QEHEBLYFL8L71', 'fgdfdsf', 'ZNOOWPHIKK', '1J6WO57KSM', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, '1', 'c', '2019-08-31 05:39:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('139', '762KZOSIU1LAXZ8', 'fgdfdsf', 'KYNKY2F1EI', NULL, '35457568.00', 'Purchase by customer', '', '', '2019/08/22', NULL, '1', 'd', '2019-08-31 05:39:26');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('140', '762KZOSIU1LAXZ8', 'fgdfdsf', 'KYNKY2F1EI', 'KAM48G8DCQ', '345345.00', 'Paid by customer', '1', '', '2019/08/22', NULL, '1', 'c', '2019-08-31 05:39:26');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('141', '939OSHPFYWOOOYU', '6FGVC6K7OX5L9Z1', 'R6BFIRTLMP', NULL, '0.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 05:41:40');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('142', '939OSHPFYWOOOYU', '6FGVC6K7OX5L9Z1', 'R6BFIRTLMP', 'ZV62YVZTTN', '150.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 05:41:40');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('143', 'QSQ7K44IRXQQ2T1', '6FGVC6K7OX5L9Z1', 'HUJOSOYQPV', NULL, '0.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 05:43:00');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('144', 'QSQ7K44IRXQQ2T1', '6FGVC6K7OX5L9Z1', 'HUJOSOYQPV', '5PKCX2MXH9', '150.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 05:43:00');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('145', 'KMNV6LBX9S255A1', 'fgdfdsf', 'SOQYFD5DVR', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, '1', 'd', '2019-08-31 05:48:59');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('146', 'KMNV6LBX9S255A1', 'fgdfdsf', 'SOQYFD5DVR', 'F8NWP6V4W8', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, '1', 'c', '2019-08-31 05:48:59');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('147', '4B6ABYSTPNG8TXC', 'fgdfdsf', 'HBW2WPWYHE', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, '1', 'd', '2019-08-31 05:49:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('148', '4B6ABYSTPNG8TXC', 'fgdfdsf', 'HBW2WPWYHE', 'FQH6NXF6FO', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, '1', 'c', '2019-08-31 05:49:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('149', 'A855CI6XUZJQAKE', 'fgdfdsf', 'EMRLGS5W57', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, '1', 'd', '2019-08-31 05:51:40');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('150', 'A855CI6XUZJQAKE', 'fgdfdsf', 'EMRLGS5W57', '7AJ4K3J54W', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, '1', 'c', '2019-08-31 05:51:40');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('151', '43FXE59FSEPPIWW', 'fgdfdsf', 'CEZC44VKAW', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, '1', 'd', '2019-08-31 05:52:07');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('152', '43FXE59FSEPPIWW', 'fgdfdsf', 'CEZC44VKAW', 'SGDEHX3PEA', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, '1', 'c', '2019-08-31 05:52:07');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('153', 'NMMF9U8AKQD1QJE', 'fgdfdsf', 'RWKKAW90BR', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, '1', 'd', '2019-08-31 05:53:24');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('154', 'NMMF9U8AKQD1QJE', 'fgdfdsf', 'RWKKAW90BR', '5MIXYN3EIA', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, '1', 'c', '2019-08-31 05:53:24');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('155', 'CXYRSY7DS9WZN72', 'fgdfdsf', 'NDQSPVYGQR', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, '1', 'd', '2019-08-31 05:53:36');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('156', 'CXYRSY7DS9WZN72', 'fgdfdsf', 'NDQSPVYGQR', 'ASVFLWMIXP', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, '1', 'c', '2019-08-31 05:53:36');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('157', 'O6D1QLW2P1FMOW6', 'fgdfdsf', 'WPOGYFR2EJ', NULL, '35457568.00', 'Purchase by customer', '', '', '2019/08/22', NULL, '1', 'd', '2019-08-31 07:11:01');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('158', 'O6D1QLW2P1FMOW6', 'fgdfdsf', 'WPOGYFR2EJ', 'XW4BUABWET', '345345.00', 'Paid by customer', '1', '', '2019/08/22', NULL, '1', 'c', '2019-08-31 07:11:01');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('159', 'LUMEF2QHGTNEIO7', '6FGVC6K7OX5L9Z1', 'GHKCY7BNB8', NULL, '1000.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 07:30:13');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('160', 'LUMEF2QHGTNEIO7', '6FGVC6K7OX5L9Z1', 'GHKCY7BNB8', '62IST1Y5BI', '500.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 07:30:14');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('161', 'ZI5UU3G5W8IE7GQ', '6FGVC6K7OX5L9Z1', '7FE92ZSJVO', NULL, '0.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 08:30:51');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('162', 'ZI5UU3G5W8IE7GQ', '6FGVC6K7OX5L9Z1', '7FE92ZSJVO', 'LF23FNEKO3', '150.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 08:30:51');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('163', '6FJ8HHODBNTISOQ', '6FGVC6K7OX5L9Z1', 'ETAJFQYI61', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 08:36:52');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('164', '6FJ8HHODBNTISOQ', '6FGVC6K7OX5L9Z1', 'ETAJFQYI61', '9CYA9VNNJV', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 08:36:52');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('165', 'VOXTI35ACPWGQBP', '6FGVC6K7OX5L9Z1', '4GGW3BKYYX', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 08:37:42');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('166', 'VOXTI35ACPWGQBP', '6FGVC6K7OX5L9Z1', '4GGW3BKYYX', 'N73LWEQVLX', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 08:37:42');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('167', 'HH6GBLCIJJ9ZQM2', '6FGVC6K7OX5L9Z1', 'IMXKKV3CWH', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 08:39:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('168', 'HH6GBLCIJJ9ZQM2', '6FGVC6K7OX5L9Z1', 'IMXKKV3CWH', '3LVMRF19JX', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 08:39:16');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('169', 'WZF1YKA1WQ2S3SU', '6FGVC6K7OX5L9Z1', 'OBRAR2MS1B', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 08:57:39');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('170', 'WZF1YKA1WQ2S3SU', '6FGVC6K7OX5L9Z1', 'OBRAR2MS1B', '98XGG3HZJK', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 08:57:39');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('171', 'ETDT9FUGDVM8O14', '6FGVC6K7OX5L9Z1', 'LKHINKOQEZ', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 08:58:07');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('172', 'ETDT9FUGDVM8O14', '6FGVC6K7OX5L9Z1', 'LKHINKOQEZ', 'GZG2ZUN8OV', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 08:58:07');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('173', 'K5MFK1FTDELQEFC', '6FGVC6K7OX5L9Z1', 'K5ZUAPRFM3', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 08:59:01');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('174', 'K5MFK1FTDELQEFC', '6FGVC6K7OX5L9Z1', 'K5ZUAPRFM3', '5NYXOQZKWM', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 08:59:01');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('175', '1R8DOQDR74PEWMF', '6FGVC6K7OX5L9Z1', 'ZVEQPUB12A', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 09:04:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('176', '1R8DOQDR74PEWMF', '6FGVC6K7OX5L9Z1', 'ZVEQPUB12A', 'GGPXUWQOWM', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 09:04:17');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('259', 'SEY7BE7IMACFKIZ', '6FGVC6K7OX5L9Z1', '5928235575', '7IWEKC33VJ', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 09:41:26');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('241', '4FC3T1CMFY65B2T', '6FGVC6K7OX5L9Z1', 'MSSLK5UBVB', NULL, '0.00', 'Purchase by customer', '', '', '2019-09-19', NULL, '1', 'd', '2019-09-19 04:24:51');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('239', 'UG4ONQNHFXFZJH9', '93K7WTVUD86LBTC', 'KAKERT7VJI', NULL, '0.00', 'Purchase by customer', '', '', '2019-09-19', NULL, '1', 'd', '2019-09-19 04:20:47');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('240', 'UG4ONQNHFXFZJH9', '93K7WTVUD86LBTC', 'KAKERT7VJI', 'UWSNV21VZC', '400.00', 'Paid by customer', '1', '', '2019-09-19', NULL, '1', 'c', '2019-09-19 04:20:47');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('181', 'SI7T2A7UM5A5ZZW', '6FGVC6K7OX5L9Z1', 'PGJOTFJMBS', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, '1', 'd', '2019-08-31 09:15:55');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('182', 'SI7T2A7UM5A5ZZW', '6FGVC6K7OX5L9Z1', 'PGJOTFJMBS', 'IWW255IV5Y', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, '1', 'c', '2019-08-31 09:15:55');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('242', '4FC3T1CMFY65B2T', '6FGVC6K7OX5L9Z1', 'MSSLK5UBVB', 'XNXKUH8UW8', '355.00', 'Paid by customer', '1', '', '2019-09-19', NULL, '1', 'c', '2019-09-19 04:24:51');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('243', '3BWOZ61PSP6P3R6', '6FGVC6K7OX5L9Z1', 'EEAK1E4KLQ', NULL, '0.00', 'Purchase by customer', '', '', '2019-09-19', NULL, '1', 'd', '2019-09-19 04:26:09');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('244', '3BWOZ61PSP6P3R6', '6FGVC6K7OX5L9Z1', 'EEAK1E4KLQ', 'G4U6U2LRYB', '500.00', 'Paid by customer', '1', '', '2019-09-19', NULL, '1', 'c', '2019-09-19 04:26:09');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('245', '6F3CVEXN8F6L7OJ', '6FGVC6K7OX5L9Z1', 'KMS4ECHBZW', NULL, '70000.00', 'Purchase by customer', '', '', '2019-09-19', NULL, '1', 'd', '2019-09-19 04:48:52');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('246', '6F3CVEXN8F6L7OJ', '6FGVC6K7OX5L9Z1', 'KMS4ECHBZW', '8QJAPIKHOA', '580.00', 'Paid by customer', '1', '', '2019-09-19', NULL, '1', 'c', '2019-09-19 04:48:52');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('247', '6XMHQKVX3LDM22E', 'BIXQAYJ3XDFBYZN', '5147385423', NULL, '175000.00', 'Purchase by customer', '', '', '2019-09-19', NULL, '1', 'd', '2019-09-19 07:20:32');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('248', '6XMHQKVX3LDM22E', 'BIXQAYJ3XDFBYZN', '5147385423', 'O823EJ3IXV', '175900.00', 'Paid by customer', '1', '', '2019-09-19', NULL, '1', 'c', '2019-09-19 07:20:32');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('249', '6XMHQKVX3LDM22E', 'BIXQAYJ3XDFBYZN', '5147385423', NULL, '70000.00', 'Return', '', '', '2019-09-19', NULL, '1', 'c', '2019-09-19 07:22:43');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('250', 'CR-1', '6', NULL, 'CR-1', '8.00', '', '1', '', '2019-09-19', 'receipt', '1', 'c', '2019-09-19 08:58:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('251', 'CR-2', '6', NULL, 'CR-2', '7.00', '', '1', '', '2019-09-19', 'receipt', '1', 'c', '2019-09-19 08:59:31');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('252', 'CR-2', '', NULL, 'CR-2', '0.00', 'fsdfsdf', '1', '', '2019-09-19', 'receipt', '1', 'c', '2019-09-19 09:24:55');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('253', 'CR-3', '', NULL, 'CR-3', '0.00', 'fsdfsdf', '1', '', '2019-09-19', 'receipt', '1', 'c', '2019-09-19 09:58:34');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('254', 'CR-3', '', NULL, 'CR-3', '0.00', 'fsdfsdf', '1', '', '2019-09-19', 'receipt', '1', 'c', '2019-09-19 10:13:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('255', 'CR-8', '', NULL, 'CR-8', '0.00', 'fsdfsdf', '1', '', '2019-09-19', 'receipt', '1', 'c', '2019-09-19 10:26:18');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('256', 'F8ZQIVM5WVILDF2', '6FGVC6K7OX5L9Z1', '3QHCHGI5BM', NULL, '0.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 05:43:10');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('257', 'F8ZQIVM5WVILDF2', '6FGVC6K7OX5L9Z1', '3QHCHGI5BM', 'CXQM5ACDZG', '500.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 05:43:10');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('258', 'SEY7BE7IMACFKIZ', '6FGVC6K7OX5L9Z1', '5928235575', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 09:41:26');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('205', '9HIGVVA6YY', 'MQ49MPE6YAYN9QN', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-09-02', NULL, '1', 'd', '2019-09-02 10:38:33');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('260', 'YQK988EWI6LVZL5', '6FGVC6K7OX5L9Z1', '6852499599', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 16:25:52');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('261', 'YQK988EWI6LVZL5', '6FGVC6K7OX5L9Z1', '6852499599', 'AXEY8QCF6O', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 16:25:52');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('262', 'CZXTX9S9BARD9J4', 'BIXQAYJ3XDFBYZN', '7348638525', NULL, '105000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 16:29:00');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('263', 'CZXTX9S9BARD9J4', 'BIXQAYJ3XDFBYZN', '7348638525', 'L7DSUBE7I6', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 16:29:00');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('264', 'U6DCWDWEU75XH7P', '6FGVC6K7OX5L9Z1', '6979694511', NULL, '500.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 16:56:58');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('265', 'U6DCWDWEU75XH7P', '6FGVC6K7OX5L9Z1', '6979694511', 'RDC6HZ5IGT', '500.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 16:56:58');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('266', 'R915QIBP9J2SPZF', '6FGVC6K7OX5L9Z1', '7675268361', NULL, '500.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 16:58:31');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('267', 'R915QIBP9J2SPZF', '6FGVC6K7OX5L9Z1', '7675268361', 'CD6T69LNB4', '500.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 16:58:31');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('268', 'VHWMMJ6IPKFR2NP', '6FGVC6K7OX5L9Z1', '1699845521', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 17:07:56');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('269', 'VHWMMJ6IPKFR2NP', '6FGVC6K7OX5L9Z1', '1699845521', 'E1M26ROIVJ', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 17:07:56');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('270', 'SHQV7VGBJM88D52', '6FGVC6K7OX5L9Z1', '3291158273', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 17:14:07');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('271', 'SHQV7VGBJM88D52', '6FGVC6K7OX5L9Z1', '3291158273', '3AJC7HLCX5', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 17:14:07');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('272', 'SC6U7J6V3H4S8SC', '6FGVC6K7OX5L9Z1', '7884756971', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 17:15:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('273', 'SC6U7J6V3H4S8SC', '6FGVC6K7OX5L9Z1', '7884756971', 'BOEHGJ4ZTW', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 17:15:08');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('274', '2L1ZNYLEB91PUP5', '6FGVC6K7OX5L9Z1', '9836928819', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 17:17:09');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('275', '2L1ZNYLEB91PUP5', '6FGVC6K7OX5L9Z1', '9836928819', 'RA36UP6J2P', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 17:17:10');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('276', 'CVWYE5KJL3D22O4', '6FGVC6K7OX5L9Z1', '2466279899', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, '1', 'd', '2019-09-21 17:18:26');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('277', 'CVWYE5KJL3D22O4', '6FGVC6K7OX5L9Z1', '2466279899', '2E1DA16MIY', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, '1', 'c', '2019-09-21 17:18:26');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('278', 'AGMXPFZLB5B3EO7', '6FGVC6K7OX5L9Z1', '7126396398', NULL, '514.90', 'Purchase by customer', '', '', '2019-09-24', NULL, '1', 'd', '2019-09-24 18:42:49');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('279', 'AGMXPFZLB5B3EO7', '6FGVC6K7OX5L9Z1', '7126396398', 'H2U2DFHQBO', '514.90', 'Paid by customer', '1', '', '2019-09-24', NULL, '1', 'c', '2019-09-24 18:42:50');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('280', 'TIPZHE21WSRO5NF', '6FGVC6K7OX5L9Z1', '3496522264', NULL, '102.98', 'Purchase by customer', '', '', '2019-09-26', NULL, '1', 'd', '2019-09-26 13:02:26');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('281', 'TIPZHE21WSRO5NF', '6FGVC6K7OX5L9Z1', '3496522264', 'BE1FEKEYQR', '102.98', 'Paid by customer', '1', '', '2019-09-26', NULL, '1', 'c', '2019-09-26 13:02:27');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('282', 'C8RMSIZMIGUMKGZ', '6FGVC6K7OX5L9Z1', '1962163959', NULL, '102.98', 'Purchase by customer', '', '', '2019-09-26', NULL, '1', 'd', '2019-09-26 13:03:38');
INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES ('283', 'C8RMSIZMIGUMKGZ', '6FGVC6K7OX5L9Z1', '1962163959', 'DTUYA1W6Z1', '102.98', 'Paid by customer', '1', '', '2019-09-26', NULL, '1', 'c', '2019-09-26 13:03:38');


#
# TABLE STRUCTURE FOR: daily_banking_add
#

DROP TABLE IF EXISTS `daily_banking_add`;

CREATE TABLE `daily_banking_add` (
  `banking_id` varchar(255) NOT NULL,
  `date` datetime DEFAULT NULL,
  `bank_id` varchar(100) DEFAULT NULL,
  `deposit_type` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `description` text,
  `amount` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: daily_closing
#

DROP TABLE IF EXISTS `daily_closing`;

CREATE TABLE `daily_closing` (
  `closing_id` varchar(255) NOT NULL,
  `last_day_closing` float NOT NULL,
  `cash_in` float NOT NULL,
  `cash_out` float NOT NULL,
  `date` varchar(250) NOT NULL,
  `amount` float NOT NULL,
  `adjustment` float DEFAULT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `daily_closing` (`closing_id`, `last_day_closing`, `cash_in`, `cash_out`, `date`, `amount`, `adjustment`, `status`) VALUES ('pNrtAYboRxG4JiA', '0', '0', '0', '2019-08-18', '0', NULL, '1');


#
# TABLE STRUCTURE FOR: department
#

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `dprt_id` int(11) NOT NULL AUTO_INCREMENT,
  `main_id` int(6) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `flaticon` varchar(30) NOT NULL,
  `description` text,
  `image` varchar(200) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`dprt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: department_lang
#

DROP TABLE IF EXISTS `department_lang`;

CREATE TABLE `department_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `language` varchar(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: designation
#

DROP TABLE IF EXISTS `designation`;

CREATE TABLE `designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(150) NOT NULL,
  `details` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: document
#

DROP TABLE IF EXISTS `document`;

CREATE TABLE `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `hidden_attach_file` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `upload_by` int(11) DEFAULT NULL,
  `refer_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: employee_history
#

DROP TABLE IF EXISTS `employee_history`;

CREATE TABLE `employee_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `rate_type` int(11) NOT NULL,
  `hrate` float NOT NULL,
  `email` varchar(50) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `address_line_1` text NOT NULL,
  `address_line_2` text NOT NULL,
  `image` text,
  `country` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: employee_salary_payment
#

DROP TABLE IF EXISTS `employee_salary_payment`;

CREATE TABLE `employee_salary_payment` (
  `emp_sal_pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `generate_id` int(11) NOT NULL,
  `employee_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `total_salary` decimal(18,2) NOT NULL DEFAULT '0.00',
  `total_working_minutes` varchar(50) CHARACTER SET latin1 NOT NULL,
  `working_period` varchar(50) CHARACTER SET latin1 NOT NULL,
  `payment_due` varchar(50) CHARACTER SET latin1 NOT NULL,
  `payment_date` varchar(50) CHARACTER SET latin1 NOT NULL,
  `paid_by` varchar(50) CHARACTER SET latin1 NOT NULL,
  `salary_month` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`emp_sal_pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: employee_salary_setup
#

DROP TABLE IF EXISTS `employee_salary_setup`;

CREATE TABLE `employee_salary_setup` (
  `e_s_s_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `sal_type` varchar(30) NOT NULL,
  `salary_type_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `create_date` date DEFAULT NULL,
  `update_date` datetime(6) DEFAULT NULL,
  `update_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `gross_salary` float NOT NULL,
  PRIMARY KEY (`e_s_s_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: enquiry
#

DROP TABLE IF EXISTS `enquiry`;

CREATE TABLE `enquiry` (
  `enquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `subject` varchar(100) NOT NULL,
  `enquiry` text,
  `checked` tinyint(1) DEFAULT NULL,
  `ip_address` varchar(20) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `checked_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`enquiry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: expense
#

DROP TABLE IF EXISTS `expense`;

CREATE TABLE `expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `type` varchar(100) NOT NULL,
  `voucher_no` varchar(50) NOT NULL,
  `amount` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `expense` (`id`, `date`, `type`, `voucher_no`, `amount`) VALUES ('1', '2019-06-28', 'Tea biscut', '20190628054055', '350');
INSERT INTO `expense` (`id`, `date`, `type`, `voucher_no`, `amount`) VALUES ('2', '2019-06-28', 'shop rent ', '20190628054109', '450');
INSERT INTO `expense` (`id`, `date`, `type`, `voucher_no`, `amount`) VALUES ('3', '2019-07-01', 'honda-22552500', '20190701062243', '350');
INSERT INTO `expense` (`id`, `date`, `type`, `voucher_no`, `amount`) VALUES ('4', '2019-07-01', 'honda-22552500', '20190701062405', '500');


#
# TABLE STRUCTURE FOR: expense_item
#

DROP TABLE IF EXISTS `expense_item`;

CREATE TABLE `expense_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_item_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `expense_item` (`id`, `expense_item_name`) VALUES ('1', 'shop rent ');
INSERT INTO `expense_item` (`id`, `expense_item_name`) VALUES ('2', 'Tea biscut');
INSERT INTO `expense_item` (`id`, `expense_item_name`) VALUES ('3', 'honda-22552500');


#
# TABLE STRUCTURE FOR: ha_birth
#

DROP TABLE IF EXISTS `ha_birth`;

CREATE TABLE `ha_birth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ha_category
#

DROP TABLE IF EXISTS `ha_category`;

CREATE TABLE `ha_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ha_death
#

DROP TABLE IF EXISTS `ha_death`;

CREATE TABLE `ha_death` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ha_investigation
#

DROP TABLE IF EXISTS `ha_investigation`;

CREATE TABLE `ha_investigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ha_medicine
#

DROP TABLE IF EXISTS `ha_medicine`;

CREATE TABLE `ha_medicine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `generic_name` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `quantity` int(6) NOT NULL,
  `price` float NOT NULL,
  `manufactured_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `strength` varchar(30) NOT NULL,
  `shelf` varchar(20) NOT NULL,
  `pack_size` float NOT NULL,
  `box_quantity` float NOT NULL,
  `total_unit` float NOT NULL,
  `total_purchase_p` float NOT NULL,
  `total_saleprice` float NOT NULL,
  `unit_pur_price` float NOT NULL,
  `unit_sale_p` float NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ha_operation
#

DROP TABLE IF EXISTS `ha_operation`;

CREATE TABLE `ha_operation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: inc_insurance
#

DROP TABLE IF EXISTS `inc_insurance`;

CREATE TABLE `inc_insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insurance_name` varchar(255) DEFAULT NULL,
  `service_tax` varchar(50) DEFAULT NULL,
  `discount` varchar(50) DEFAULT NULL,
  `remark` text,
  `insurance_no` varchar(50) DEFAULT NULL,
  `insurance_code` varchar(50) DEFAULT NULL,
  `disease_charge` text COMMENT 'array(name, charge)',
  `hospital_rate` varchar(50) DEFAULT NULL,
  `insurance_rate` varchar(50) DEFAULT NULL,
  `total` varchar(50) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: inc_limit_approval
#

DROP TABLE IF EXISTS `inc_limit_approval`;

CREATE TABLE `inc_limit_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) DEFAULT NULL,
  `room_no` varchar(100) DEFAULT NULL,
  `disease_details` text COMMENT 'name, description',
  `consultant_id` int(11) DEFAULT NULL COMMENT 'doctor list',
  `policy_name` varchar(255) DEFAULT NULL,
  `policy_no` varchar(100) DEFAULT NULL,
  `policy_holder_name` varchar(255) DEFAULT NULL,
  `insurance_id` int(11) DEFAULT NULL,
  `approval_breakup` text COMMENT 'name, charge',
  `total` varchar(50) DEFAULT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: inputoutput
#

DROP TABLE IF EXISTS `inputoutput`;

CREATE TABLE `inputoutput` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `type_of_input_and_amount` text NOT NULL,
  `input_time` time NOT NULL,
  `type_of_output_and_amount` text,
  `output_time` time DEFAULT NULL,
  `remarks` text,
  `branch_id` int(11) NOT NULL,
  `assign_by` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: invoice
#

DROP TABLE IF EXISTS `invoice`;

CREATE TABLE `invoice` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `invoice_id` varchar(100) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `date` varchar(50) DEFAULT NULL,
  `total_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `prevous_due` decimal(20,2) NOT NULL DEFAULT '0.00',
  `shipping_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invoice` varchar(255) NOT NULL,
  `invoice_discount` decimal(10,2) DEFAULT '0.00' COMMENT 'invoice discount',
  `total_discount` decimal(10,2) DEFAULT '0.00' COMMENT 'total invoice discount',
  `total_tax` decimal(10,2) DEFAULT '0.00',
  `sales_by` varchar(50) NOT NULL,
  `invoice_details` text NOT NULL,
  `status` int(2) NOT NULL,
  `bank_id` varchar(30) DEFAULT NULL,
  `payment_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('1', '3498244447', 'XWCEM9UVV5NAT5R', '2019-06-25', '200.00', '0.00', '0.00', '1000', '0.00', '0.00', '0.00', '1', 'sdfsdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('2', '4477997231', '6FGVC6K7OX5L9Z1', '2019-06-25', '200.00', '0.00', '0.00', '1001', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('3', '3973611118', '6FGVC6K7OX5L9Z1', '2019-06-25', '500.00', '0.00', '0.00', '1002', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('4', '3923586588', '6FGVC6K7OX5L9Z1', '2019-06-25', '900.00', '0.00', '0.00', '1003', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('5', '6575276926', 'XWCEM9UVV5NAT5R', '2019-06-26', '200.00', '0.00', '0.00', '1004', '0.00', '0.00', '0.00', '1', 'sdfsdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('6', '6643494915', '6FGVC6K7OX5L9Z1', '2019-06-26', '200.00', '0.00', '0.00', '1005', '0.00', '0.00', '0.00', '1', 'sdfsd', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('7', '1969912464', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1006', '0.00', '0.00', '0.00', '1', 'dfgdfg', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('8', '6691959977', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1007', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('9', '2258111339', '6FGVC6K7OX5L9Z1', '2019-06-26', '200.00', '0.00', '0.00', '1008', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('10', '9624435359', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1009', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('11', '8668653724', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1010', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('12', '8558184696', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1011', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('13', '9147771963', '6FGVC6K7OX5L9Z1', '2019-06-26', '450.00', '0.00', '0.00', '1012', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('14', '3169824785', 'FYJ4EEBMOGTW25T', '2019-06-26', '800.00', '0.00', '0.00', '1013', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('15', '3918198168', '6FGVC6K7OX5L9Z1', '2019-06-26', '1450.00', '0.00', '0.00', '1014', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('16', '5219125479', 'PVGGNNSWOESP45P', '2019-06-26', '450.00', '0.00', '0.00', '1015', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('17', '8394724961', 'PVGGNNSWOESP45P', '2019-06-26', '1450.00', '0.00', '0.00', '1016', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('18', '7986446844', '6FGVC6K7OX5L9Z1', '2019-06-27', '800.00', '0.00', '0.00', '1017', '0.00', '0.00', '0.00', '1', 'sdfds', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('19', '6789217583', '6FGVC6K7OX5L9Z1', '2019-06-27', '500.00', '0.00', '0.00', '1018', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('20', '4978495577', '6BPL9FJR54PBZJE', '2019-06-27', '950.00', '0.00', '0.00', '1019', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('21', '2687213589', 'KFTUKN7RY1O2W6R', '2019-06-28', '50000.00', '0.00', '0.00', '1020', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('22', '3378453241', '6FGVC6K7OX5L9Z1', '2019-06-28', '500.00', '0.00', '0.00', '1021', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('23', '4458233386', '6FGVC6K7OX5L9Z1', '2019-06-28', '500.00', '0.00', '0.00', '1022', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('24', '1114135497', '6FGVC6K7OX5L9Z1', '2019-06-28', '2210.00', '0.00', '0.00', '1023', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('25', '4441511736', '6FGVC6K7OX5L9Z1', '2019-06-28', '2210.00', '0.00', '0.00', '1024', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('26', '8754679919', '6FGVC6K7OX5L9Z1', '2019-06-28', '50000.00', '0.00', '0.00', '1025', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('27', '3378767423', '6FGVC6K7OX5L9Z1', '2019-06-28', '50000.00', '0.00', '0.00', '1026', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('28', '9317933378', '6FGVC6K7OX5L9Z1', '2019-06-29', '50000.00', '0.00', '0.00', '1027', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('29', '1797561487', '6FGVC6K7OX5L9Z1', '2019-06-29', '36000.00', '0.00', '0.00', '1028', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('30', '9438755655', '6FGVC6K7OX5L9Z1', '2019-06-30', '35000.00', '0.00', '0.00', '1029', '0.00', '0.00', '0.00', '1', 'sdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('31', '5919433886', '6FGVC6K7OX5L9Z1', '2019-06-30', '35000.00', '0.00', '0.00', '1030', '0.00', '0.00', '0.00', '1', 'sdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('32', '2875718763', '6FGVC6K7OX5L9Z1', '2019-07-01', '200.00', '0.00', '0.00', '1031', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('33', '1749144575', '6FGVC6K7OX5L9Z1', '2019-07-02', '200.00', '0.00', '0.00', '1032', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('34', '9815582964', '6FGVC6K7OX5L9Z1', '2019-07-02', '35000.00', '0.00', '0.00', '1033', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('35', '8493678614', '6FGVC6K7OX5L9Z1', '2019-07-02', '455.00', '0.00', '0.00', '1034', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('36', '9691144514', '6FGVC6K7OX5L9Z1', '2019-07-31', '800.00', '0.00', '0.00', '1035', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('37', '5457199982', '6FGVC6K7OX5L9Z1', '2019-07-31', '195465.00', '0.00', '0.00', '1036', '0.00', '0.00', '0.00', '1', 'sdfasdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('38', '2362693843', '6FGVC6K7OX5L9Z1', '2019-07-31', '35000.00', '0.00', '0.00', '1037', '0.00', '0.00', '0.00', '1', 'sdfsdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('39', '5763389422', '6FGVC6K7OX5L9Z1', '2019-07-31', '455.00', '0.00', '0.00', '1038', '0.00', '0.00', '0.00', '1', 'dsfsd', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('40', '6921223925', '6FGVC6K7OX5L9Z1', '2019-08-01', '50455.00', '0.00', '0.00', '1039', '0.00', '0.00', '0.00', '1', 'dsff', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('41', '2992478698', '6FGVC6K7OX5L9Z1', '2019-08-01', '50655.00', '0.00', '0.00', '1040', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('42', '2429117215', '6FGVC6K7OX5L9Z1', '2019-08-01', '51855.00', '0.00', '0.00', '1041', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('43', '8636821614', '6FGVC6K7OX5L9Z1', '2019-08-01', '85000.00', '0.00', '0.00', '1042', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('44', '2274125738', '6FGVC6K7OX5L9Z1', '2019-08-01', '50000.00', '0.00', '0.00', '1043', '0.00', '0.00', '0.00', '1', 'gdsfasdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('45', '6881727828', '6FGVC6K7OX5L9Z1', '2019-08-01', '455.00', '0.00', '0.00', '1044', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('46', '6942233633', '6FGVC6K7OX5L9Z1', '2019-08-03', '200.00', '0.00', '0.00', '1045', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('47', '7167684116', '6FGVC6K7OX5L9Z1', '2019-08-03', '2600.00', '0.00', '0.00', '1046', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('48', 'MBJEQBLEDT', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1047', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('49', '3OXFJU9FLE', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1048', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('50', 'AEYS0XT6FB', 'BIXQAYJ3XDFBYZN', '2019-08-27', '150.00', '0.00', '0.00', '1049', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('51', 'IFU2EJUMSR', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1050', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('52', 'TD6SQ1IDPR', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1051', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('53', 'B8P4EGGRAY', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1052', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('54', 'R8OMFVYJXQ', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1053', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('55', 'VYMH47RI7Y', 'BIXQAYJ3XDFBYZN', '2019-08-27', '150.00', '0.00', '0.00', '1054', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('56', '3FQSMSSQDT', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1055', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('57', 'JTXNMIW0DT', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1056', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('58', 'TJSXGFD1F6', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1057', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('59', '1AGGWW2DR6', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1058', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('60', 'FO9Q0CZBAH', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1059', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('61', 'CYN1QFPGZE', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1060', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('62', 'ZNOOWPHIKK', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1061', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('63', 'KYNKY2F1EI', 'fgdfdsf', '2019/08/22', '35457568.00', '0.00', '0.00', '1062', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('64', 'R6BFIRTLMP', '6FGVC6K7OX5L9Z1', '2019-08-31', '0.00', '0.00', '0.00', '1063', '0.00', '100.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('65', 'HUJOSOYQPV', '6FGVC6K7OX5L9Z1', '2019-08-31', '0.00', '0.00', '0.00', '1064', '0.00', '100.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('66', 'SOQYFD5DVR', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1065', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('67', 'HBW2WPWYHE', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1066', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('68', 'EMRLGS5W57', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1067', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('69', 'CEZC44VKAW', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1068', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('70', 'RWKKAW90BR', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1069', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('71', 'NDQSPVYGQR', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1070', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('72', 'WPOGYFR2EJ', 'fgdfdsf', '2019/08/22', '35457568.00', '0.00', '0.00', '1071', '0.00', '8768.00', '456.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('73', 'GHKCY7BNB8', '6FGVC6K7OX5L9Z1', '2019-08-31', '1000.00', '0.00', '0.00', '1072', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('74', '7FE92ZSJVO', '6FGVC6K7OX5L9Z1', '2019-08-31', '0.00', '0.00', '0.00', '1073', '0.00', '10.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('75', 'ETAJFQYI61', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1074', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('76', '4GGW3BKYYX', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1075', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('77', 'IMXKKV3CWH', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1076', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('78', 'OBRAR2MS1B', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1077', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('79', 'LKHINKOQEZ', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1078', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('80', 'K5ZUAPRFM3', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1079', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('81', 'ZVEQPUB12A', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1080', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('118', 'KAKERT7VJI', '93K7WTVUD86LBTC', '2019-09-19', '0.00', '0.00', '0.00', '1084', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('119', 'MSSLK5UBVB', '6FGVC6K7OX5L9Z1', '2019-09-19', '0.00', '0.00', '0.00', '1085', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('84', 'PGJOTFJMBS', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1083', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('120', 'EEAK1E4KLQ', '6FGVC6K7OX5L9Z1', '2019-09-19', '0.00', '0.00', '0.00', '1086', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('121', 'KMS4ECHBZW', '6FGVC6K7OX5L9Z1', '2019-09-19', '70000.00', '0.00', '0.00', '1087', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('122', '5147385423', 'BIXQAYJ3XDFBYZN', '2019-09-19', '175000.00', '900.00', '0.00', '1088', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('123', '3QHCHGI5BM', '6FGVC6K7OX5L9Z1', '2019-09-21', '0.00', '0.00', '0.00', '1089', '0.00', '0.00', '0.00', '1', '', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('124', '5928235575', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1090', '0.00', '0.00', '0.00', '1', 'sdfsdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('125', '6852499599', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1091', '0.00', '0.00', '0.00', '1', 'sadfasdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('126', '7348638525', 'BIXQAYJ3XDFBYZN', '2019-09-21', '35000.00', '-70000.00', '0.00', '1092', '0.00', '0.00', '0.00', '1', 'sdfsdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('127', '6979694511', '6FGVC6K7OX5L9Z1', '2019-09-21', '500.00', '0.00', '0.00', '1093', '0.00', '0.00', '0.00', '1', 'dsff', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('128', '7675268361', '6FGVC6K7OX5L9Z1', '2019-09-21', '500.00', '0.00', '0.00', '1094', '0.00', '0.00', '0.00', '1', 'sdfsadf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('129', '1699845521', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1095', '0.00', '0.00', '0.00', '1', 'Gui Pos', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('130', '3291158273', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1096', '0.00', '0.00', '0.00', '1', 'asdfasdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('131', '7884756971', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1097', '0.00', '0.00', '0.00', '1', 'sadsadfasdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('132', '9836928819', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1098', '0.00', '0.00', '0.00', '1', 'sdfasdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('133', '2466279899', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1099', '0.00', '0.00', '0.00', '1', 'sadfasdf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('134', '7126396398', '6FGVC6K7OX5L9Z1', '2019-09-24', '514.90', '0.00', '0.00', '1100', '0.00', '0.00', '14.90', '1', 'sdfsad', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('135', '3496522264', '6FGVC6K7OX5L9Z1', '2019-09-26', '102.98', '0.00', '0.00', '1101', '0.00', '0.00', '2.98', '1', 'sdfsadf', '1', NULL, '1');
INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES ('136', '1962163959', '6FGVC6K7OX5L9Z1', '2019-09-26', '102.98', '0.00', '0.00', '1102', '0.00', '0.00', '2.98', '1', 'sdfsadff', '1', NULL, '1');


#
# TABLE STRUCTURE FOR: invoice_details
#

DROP TABLE IF EXISTS `invoice_details`;

CREATE TABLE `invoice_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_details_id` varchar(100) NOT NULL,
  `invoice_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `serial_no` varchar(30) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `quantity` decimal(10,2) DEFAULT NULL,
  `rate` decimal(10,2) DEFAULT NULL,
  `supplier_rate` float DEFAULT NULL,
  `total_price` decimal(12,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `discount_per` varchar(15) DEFAULT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `paid_amount` decimal(12,0) DEFAULT NULL,
  `due_amount` decimal(10,2) DEFAULT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('1', '137575625474673', '3498244447', '15419452', '23', 'sdf', '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '200', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('2', '581712284985499', '4477997231', '15419452', NULL, '', '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '200', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('3', '143252691329446', '3973611118', '7898940742035', NULL, '', '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('4', '851349838845756', '3923586588', '7898940742035', NULL, NULL, '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '600', '300.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('5', '954661845789826', '3923586588', '15419452', NULL, NULL, '2.00', '200.00', '150', '400.00', '0.00', '', NULL, '600', '300.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('6', '295222397713239', '6575276926', '15419452', '23', 'sdfsdf', '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '200', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('7', '641545319772768', '6643494915', '15419452', NULL, 'dfdfg', '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '200', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('8', '611174384339762', '1969912464', '7898940742035', NULL, 'dfgdf', '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('9', '762739765493419', '6691959977', '7898940742035', NULL, NULL, '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('10', '193332584279475', '2258111339', '15419452', NULL, NULL, '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '200', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('11', '496655922344734', '9624435359', '7898940742035', NULL, '', '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('12', '487111492258153', '8668653724', '7898940742035', NULL, '', '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('13', '889576258355181', '8558184696', '7898940742035', NULL, '', '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('14', '445417159836199', '9147771963', '4895180728259', NULL, NULL, '1.00', '450.00', '350', '450.00', '0.00', '', NULL, '450', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('15', '991859532998638', '3169824785', '76358837', NULL, '', '1.00', '800.00', '390', '800.00', '0.00', '', NULL, '800', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('16', '116265385854116', '3918198168', '76358837', NULL, NULL, '1.00', '800.00', '390', '800.00', '0.00', '', NULL, '1450', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('17', '329957471171172', '3918198168', '4895180728259', NULL, NULL, '1.00', '450.00', '350', '450.00', '0.00', '', NULL, '1450', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('18', '982618147622923', '3918198168', '15419452', NULL, NULL, '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '1450', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('19', '519889794784446', '5219125479', '4895180728259', NULL, NULL, '1.00', '450.00', '350', '450.00', '0.00', '', NULL, '450', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('20', '481442571474237', '8394724961', '76358837', NULL, NULL, '1.00', '800.00', '390', '800.00', '0.00', '', NULL, '1450', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('21', '978899887225483', '8394724961', '4895180728259', NULL, NULL, '1.00', '450.00', '350', '450.00', '0.00', '', NULL, '1450', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('22', '932166213359762', '8394724961', '15419452', NULL, NULL, '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '1450', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('23', '777521777487153', '7986446844', '76358837', NULL, 'sdfsd', '1.00', '800.00', '390', '800.00', '0.00', '', NULL, '800', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('24', '311894743623256', '6789217583', '7898940742035', NULL, '', '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('25', '952438843275488', '4978495577', '7898940742035', NULL, NULL, '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '450.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('26', '513259341797653', '4978495577', '4895180728259', NULL, NULL, '1.00', '450.00', '350', '450.00', '0.00', '', NULL, '500', '450.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('27', '974464646945377', '2687213589', '19276692', NULL, NULL, '1.00', '50000.00', '40000', '50000.00', '0.00', '', NULL, '30000', '20000.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('28', '646777237458793', '3378453241', '7898940742035', NULL, '', '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('29', '591787248295113', '4458233386', '7898940742035', NULL, '', '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('30', '787175611967892', '1114135497', '7898940742035', NULL, NULL, '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '0', '2210.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('31', '181795372137992', '1114135497', '4895180728259', NULL, NULL, '2.00', '455.00', '355', '910.00', '0.00', '', NULL, '0', '2210.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('32', '361977447434223', '1114135497', '76358837', NULL, NULL, '1.00', '800.00', '390', '800.00', '0.00', '', NULL, '0', '2210.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('33', '937664474256695', '4441511736', '7898940742035', NULL, NULL, '1.00', '500.00', '400', '500.00', '0.00', '', NULL, '2210', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('34', '744447778986528', '4441511736', '4895180728259', NULL, NULL, '2.00', '455.00', '355', '910.00', '0.00', '', NULL, '2210', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('35', '221886224549298', '4441511736', '76358837', NULL, NULL, '1.00', '800.00', '390', '800.00', '0.00', '', NULL, '2210', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('36', '246145685723245', '8754679919', '19276692', NULL, NULL, '1.00', '50000.00', '40000', '50000.00', '0.00', '', NULL, '0', '50000.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('37', '849363479795543', '3378767423', '19276692', NULL, NULL, '1.00', '50000.00', '40000', '50000.00', '0.00', '', NULL, '50000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('38', '446624776936233', '9317933378', '19276692', 'fgadgaf123213', NULL, '1.00', '50000.00', '40000', '50000.00', '0.00', '', NULL, '0', '50000.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('39', '566944469776672', '1797561487', '38296235', NULL, NULL, '1.00', '35000.00', '30000', '35000.00', '0.00', '', NULL, '36000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('40', '595263453284194', '1797561487', '7898940742035', NULL, NULL, '2.00', '500.00', '400', '1000.00', '0.00', '', NULL, '36000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('41', '363928641128468', '9438755655', '38296235', NULL, 'dsf', '1.00', '35000.00', '30000', '35000.00', '0.00', '', NULL, '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('42', '365142821263614', '5919433886', '38296235', NULL, 'sdf', '1.00', '35000.00', '30000', '35000.00', '0.00', '', NULL, '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('43', '975648728417341', '2875718763', '15419452', NULL, NULL, '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '200', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('44', '394779538783591', '1749144575', '15419452', NULL, NULL, '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '200', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('45', '333396413857953', '9815582964', '38296235', NULL, NULL, '1.00', '35000.00', '30000', '35000.00', '0.00', '', NULL, '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('46', '293897394397456', '8493678614', '4895180728259', NULL, NULL, '1.00', '455.00', '355', '455.00', '0.00', '', NULL, '455', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('47', '739137696854459', '9691144514', '76358837', NULL, 'sdfsdf', '1.00', '800.00', '390', '800.00', '0.00', '', NULL, '0', '800.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('48', '448379374788561', '5457199982', '4895180728259', NULL, '', '2.00', '455.00', '355', '910.00', '0.00', '', '0.00', '195465', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('49', '717685695658682', '5457199982', '76358837', NULL, '', '1.00', '800.00', '390', '800.00', '0.00', '', '0.00', '195465', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('50', '865235676939658', '5457199982', '4895180728259', NULL, '', '1.00', '455.00', '355', '455.00', '0.00', '', '0.00', '195465', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('51', '662695419219481', '5457199982', '34113672', '123', '', '1.00', '300.00', '250', '300.00', '0.00', '', '0.00', '195465', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('52', '878665989248834', '5457199982', '19276692', 'tgart4tt34523423', '', '3.00', '50000.00', '40000', '150000.00', '0.00', '', '0.00', '195465', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('53', '994655353945159', '5457199982', '15419452', '23', '', '1.00', '200.00', '150', '200.00', '0.00', '', '0.00', '195465', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('54', '741823432513397', '5457199982', '38296235', '645645661411fdf', '', '1.00', '35000.00', '30000', '35000.00', '0.00', '', '0.00', '195465', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('55', '625671587752579', '5457199982', 'SN191508964287', 'gp-gstfs31120gntd', '', '3.00', '2600.00', '2000', '7800.00', '0.00', '', '0.00', '195465', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('56', '796788549382232', '2362693843', '38296235', '645645661411fdf', '', '1.00', '35000.00', '30000', '35000.00', '0.00', '', NULL, '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('58', '315432755456982', '5763389422', '4895180728259', NULL, 'test one', '1.00', '455.00', '355', '455.00', '0.00', '', '0.00', '455', '0.00', '0');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('59', '715713361844324', '6921223925', '4895180728259', NULL, 'sdfsdf', '1.00', '455.00', '355', '455.00', '0.00', '', '0.00', '50455', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('60', '513417716724662', '6921223925', '19276692', 'fgadgaf123213', '', '1.00', '50000.00', '40000', '50000.00', '0.00', '', '0.00', '50455', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('61', '294424414463898', '2992478698', '15419452', '23', '', '1.00', '200.00', '150', '200.00', '0.00', '', '0.00', '50655', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('62', '968484347995985', '2992478698', '4895180728259', NULL, '', '1.00', '455.00', '355', '455.00', '0.00', '', '0.00', '50655', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('63', '183163522981473', '2992478698', '19276692', 'tgart4tt34523423', '', '1.00', '50000.00', '40000', '50000.00', '0.00', '', '0.00', '50655', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('64', '554872484972156', '2429117215', '15419452', '23', '', '1.00', '200.00', '150', '200.00', '0.00', '', '0.00', '51855', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('65', '897194938679971', '2429117215', '19276692', 'tgart4tt34523423', '', '1.00', '50000.00', '40000', '50000.00', '0.00', '', '0.00', '51855', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('66', '163854359763219', '2429117215', '34113672', 'werq', '', '4.00', '300.00', '250', '1200.00', '0.00', '', '0.00', '51855', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('67', '119869541418933', '2429117215', '4895180728259', NULL, '', '1.00', '455.00', '355', '455.00', '0.00', '', '0.00', '51855', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('68', '352582949319253', '8636821614', '38296235', 'reteytregf25436456546', '', '1.00', '35000.00', '30000', '35000.00', '0.00', '', NULL, '85000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('69', '243882731263446', '8636821614', '19276692', 'fgadgaf123213', '', '1.00', '50000.00', '40000', '50000.00', '0.00', '', NULL, '85000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('70', '924532871886139', '2274125738', '19276692', 'fgadgaf123213', '', '1.00', '50000.00', '40000', '50000.00', '0.00', '', NULL, '50000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('71', '372741867629755', '6881727828', '4895180728259', NULL, '', '1.00', '455.00', '355', '455.00', '0.00', '', NULL, '455', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('72', '733758996514585', '6942233633', '15419452', NULL, '', '1.00', '200.00', '150', '200.00', '0.00', '', NULL, '0', '200.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('73', '682954324848157', '7167684116', 'SN191508964287', NULL, '', '1.00', '2600.00', '2000', '2600.00', '0.00', '', NULL, '2600', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('74', 'wJHUPdmchQKkXB0', 'AEYS0XT6FB', '1', NULL, '', '1.00', '1.00', NULL, NULL, '0.00', '0', '0.00', '50', '100.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('75', 'Qjm6xyaFuZt2xX', 'IFU2EJUMSR', '1', NULL, '', '1.00', '1.00', NULL, NULL, '0.00', '0', '0.00', '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('76', 'a9f16CsZEePspfR', 'TD6SQ1IDPR', '1', NULL, NULL, '1.00', '1.00', NULL, NULL, NULL, NULL, NULL, '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('77', 'f3EXFlJGBZscS2Q', 'TJSXGFD1F6', '15419452', NULL, NULL, '1.00', '100.00', NULL, NULL, NULL, NULL, NULL, '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('78', 'Zwv6LrUGZ1VzlBg', '1AGGWW2DR6', '15419452', NULL, NULL, '1.00', '100.00', NULL, NULL, NULL, NULL, NULL, '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('79', 'AByKV0Dp6PeXHLG', 'FO9Q0CZBAH', '15419452', NULL, '', '1.00', '100.00', '150', NULL, '0.00', '0', '0.00', '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('80', 'bESqWI3ZgAkfQMM', 'CYN1QFPGZE', '15419452', NULL, '', '1.00', '100.00', '150', NULL, '0.00', '0', '0.00', '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('81', 'wRl0clHgGJhpGra', 'QE2YQFXLDA', '[', NULL, '', '0.00', '0.00', NULL, NULL, '0.00', '0', '0.00', '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('82', 'RwJCSbcY7ykjbHT', 'JJ75B2XUKG', '15419452', NULL, '', '1.00', '100.00', '150', NULL, '0.00', '0', '0.00', '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('83', 'K70HLTEXZfyPpFC', 'ZNOOWPHIKK', '15419452', '23', '', '2.00', '200.00', '150', NULL, '0.00', '0', '0.00', '345345', '45658.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('84', 'XfttUheTCA4vM9I', 'KYNKY2F1EI', '15419452', '23', '', '2.00', '200.00', '150', NULL, '0.00', '0', '0.00', '345345', '45658.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('85', 'qsBTSzI8OtFAWCu', 'R6BFIRTLMP', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '150', '104750.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('86', 'uGQ8IHzpCWTgziI', 'HUJOSOYQPV', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '150', '139750.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('87', 'dnU22f7QVAP2xSP', 'RWKKAW90BR', '15419452', '23', 'txt fe', '2.00', '200.00', '150', '1000.00', '0.00', '0', '0.00', '345345', '45658.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('88', 'eiQMZHDdxG4wlAi', 'NDQSPVYGQR', '15419452', '23', 'txt fe', '2.00', '200.00', '150', '1000.00', '0.00', '0', '0.00', '345345', '45658.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('89', 'OruA0rIELSiERnD', 'WPOGYFR2EJ', '15419452', '23', 'txt fe', '2.00', '200.00', '150', '1000.00', '0.00', '0', '0.00', '345345', '45658.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('90', 'MiezNEYI4EryXLy', 'GHKCY7BNB8', '7898940742035', '23', 'txt fe', '2.00', '250.00', '400', '1000.00', '0.00', '0', '0.00', '500', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('91', 'oDPzw4e7KaBVBE2', '7FE92ZSJVO', '', NULL, 'txt fe', '0.00', '0.00', NULL, '1000.00', '0.00', '0', '0.00', '150', '104840.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('92', 'FIIX2MfpLrbrbjk', 'ETAJFQYI61', '', NULL, 'txt fe', '0.00', '0.00', NULL, '1000.00', '0.00', '0', '0.00', '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('93', 'YdNUNi4g5u9N1Uv', 'ZVEQPUB12A', '7898940742035', '300', '', '2.00', '150.00', '400', '300.00', '0.00', '0', '0.00', '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('125', 'XwiuiMCiBTJpfDw', 'KAKERT7VJI', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '400', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('126', 'Hu1w9UeLLVKdmWJ', 'MSSLK5UBVB', '38296235', 'fgafggfgafg12', '', '4.00', '35000.00', '20', '140000.00', '0.00', '0', '0.00', '355', '139645.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('96', 'yHdDd48WG1CuCo4', 'PGJOTFJMBS', '7898940742035', NULL, '', '2.00', '150.00', '400', '300.00', '0.00', '0', '0.00', '50', '50.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('127', 'KgyrSAzRlCyNPFH', 'EEAK1E4KLQ', '7898940742035', NULL, '', '4.00', '500.00', '400', '2000.00', '0.00', '0', '0.00', '500', '1500.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('128', 'rkJ1dGpTw1vizuh', 'KMS4ECHBZW', '38296235', 'fgafggfgafg12', '', '2.00', '35000.00', '20', '70000.00', '0.00', '0', '0.00', '580', '69420.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('129', '159853167587635', '5147385423', '38296235', 'reteytregf25436456546', '', '5.00', '35000.00', '20', '175000.00', '0.00', '', '0.00', '175900', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('130', 'P3i9wRrXFWgoqeo', '3QHCHGI5BM', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '500', '104500.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('131', '158941674233767', '5928235575', '38296235', NULL, '', '1.00', '35000.00', '20', '35000.00', '0.00', '', '0.00', '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('106', '5pjSLQ9R3CPaPBD', 'TA1UKNEC4K', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '0', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('132', '923484755832344', '6852499599', '38296235', 'reteytregf25436456546', '', '1.00', '35000.00', '20', '35000.00', '0.00', '', '0.00', '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('133', '848742989874436', '7348638525', '38296235', '5234535152112', '', '1.00', '35000.00', '20', '35000.00', '0.00', '', '0.00', '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('134', '657776536349848', '6979694511', '7898940742035', NULL, 'sdfdf', '1.00', '500.00', '400', '500.00', '0.00', '', '0.00', '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('135', '848359468359833', '7675268361', '7898940742035', NULL, 'sdfasd', '1.00', '500.00', '400', '500.00', '0.00', '', '0.00', '500', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('136', '563777318557197', '1699845521', '38296235', 'reteytregf25436456546', '', '1.00', '35000.00', '20', '35000.00', '0.00', '', '0.00', '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('137', '441699588965154', '3291158273', '38296235', 'reteytregf25436456546', 'sadfasdf', '1.00', '35000.00', '20', '35000.00', '0.00', '', '0.00', '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('138', '859187844194547', '7884756971', '38296235', '645645661411fdf', 'sdfsdf', '1.00', '35000.00', '20', '35000.00', '0.00', '', '0.00', '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('139', '124189873679981', '9836928819', '38296235', 'reteytregf25436456546', 'sdfsdf', '1.00', '35000.00', '20', '35000.00', '0.00', '', '0.00', '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('140', '251542111898817', '2466279899', '38296235', 'reteytregf25436456546', 'sfsdf', '1.00', '35000.00', '20', '35000.00', '0.00', '', '0.00', '35000', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('141', '623498975636813', '7126396398', '42156427', 'sfs', 'sdfsdf', '5.00', '100.00', '90', '500.00', '0.00', '', '5.00', '515', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('142', '574884884611426', '3496522264', '42156427', 'sfs', 'sdfsda', '1.00', '100.00', '90', '100.00', '0.00', '', '1.00', '103', '0.00', '1');
INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES ('143', '893387486338493', '1962163959', '42156427', 'dfsd', 'sdfs', '1.00', '100.00', '90', '100.00', '0.00', '', '1.00', '103', '0.00', '1');


#
# TABLE STRUCTURE FOR: invoice_details_p
#

DROP TABLE IF EXISTS `invoice_details_p`;

CREATE TABLE `invoice_details_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `qty` float NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `total` decimal(12,0) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `batch_id` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: invoice_p
#

DROP TABLE IF EXISTS `invoice_p`;

CREATE TABLE `invoice_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `patient_id` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `grandtotal` decimal(12,2) NOT NULL,
  `total_discount` decimal(8,0) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: issue_details
#

DROP TABLE IF EXISTS `issue_details`;

CREATE TABLE `issue_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `patient_id` text NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `batch_id` varchar(30) NOT NULL,
  `qty` float NOT NULL,
  `up` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: issue_p
#

DROP TABLE IF EXISTS `issue_p`;

CREATE TABLE `issue_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` varchar(30) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `batch_id` varchar(30) NOT NULL,
  `expire_date` date NOT NULL,
  `assign_by` varchar(20) NOT NULL,
  `grandtotal` decimal(10,2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: language
#

DROP TABLE IF EXISTS `language`;

CREATE TABLE `language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `phrase` text NOT NULL,
  `english` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=810 DEFAULT CHARSET=utf8;

INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('1', 'user_profile', 'User Profile');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('2', 'setting', 'Setting');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('3', 'language', 'Language');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('4', 'manage_users', 'Manage Users');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('5', 'add_user', 'Add User');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('6', 'manage_company', 'Manage Company');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('7', 'web_settings', 'Software Settings');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('8', 'manage_accounts', 'Manage Accounts');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('9', 'create_accounts', 'Create Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('10', 'manage_bank', 'Manage Bank');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('11', 'add_new_bank', 'Add New Bank');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('12', 'settings', 'Bank');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('13', 'closing_report', 'Closing Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('14', 'closing', 'Closing');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('15', 'cheque_manager', 'Cheque Manager');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('16', 'accounts_summary', 'Accounts Summary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('17', 'expense', 'Expense');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('18', 'income', 'Income');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('19', 'accounts', 'Accounts');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('20', 'stock_report', 'Stock Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('21', 'stock', 'Stock');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('22', 'pos_invoice', 'POS Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('23', 'manage_invoice', 'Manage Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('24', 'new_invoice', 'New Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('25', 'invoice', 'Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('26', 'manage_purchase', 'Manage Purchase');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('27', 'add_purchase', 'Add Purchase');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('28', 'purchase', 'Purchase');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('29', 'paid_customer', 'Paid Customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('30', 'manage_customer', 'Manage Customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('31', 'add_customer', 'Add Customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('32', 'customer', 'Customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('33', 'supplier_payment_actual', 'Supplier Payment Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('34', 'supplier_sales_summary', 'Supplier Sales Summary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('35', 'supplier_sales_details', 'Supplier Sales Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('36', 'supplier_ledger', 'Supplier Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('37', 'manage_supplier', 'Manage Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('38', 'add_supplier', 'Add Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('39', 'supplier', 'Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('40', 'product_statement', 'Product Statement');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('41', 'manage_product', 'Manage Product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('42', 'add_product', 'Add Product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('43', 'product', 'Product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('44', 'manage_category', 'Manage Category');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('45', 'add_category', 'Add Category');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('46', 'category', 'Category');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('47', 'sales_report_product_wise', 'Sales Report (Product Wise)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('48', 'purchase_report', 'Purchase Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('49', 'sales_report', 'Sales Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('50', 'todays_report', 'Todays Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('51', 'report', 'Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('52', 'dashboard', 'Dashboard');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('53', 'online', 'Online');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('54', 'logout', 'Logout');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('55', 'change_password', 'Change Password');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('56', 'total_purchase', 'Total Purchase');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('57', 'total_amount', 'Total Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('58', 'supplier_name', 'Supplier Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('59', 'invoice_no', 'Invoice No');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('60', 'purchase_date', 'Purchase Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('61', 'todays_purchase_report', 'Todays Purchase Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('62', 'total_sales', 'Total Sales');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('63', 'customer_name', 'Customer Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('64', 'sales_date', 'Sales Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('65', 'todays_sales_report', 'Todays Sales Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('66', 'home', 'Home');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('67', 'todays_sales_and_purchase_report', 'Todays sales and purchase report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('68', 'total_ammount', 'Total Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('69', 'rate', 'Rate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('70', 'product_model', 'Product Model');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('71', 'product_name', 'Product Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('72', 'search', 'Search');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('73', 'end_date', 'End Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('74', 'start_date', 'Start Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('75', 'total_purchase_report', 'Total Purchase Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('76', 'total_sales_report', 'Total Sales Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('77', 'total_seles', 'Total Sales');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('78', 'all_stock_report', 'All Stock Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('79', 'search_by_product', 'Search By Product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('80', 'date', 'Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('81', 'print', 'Print');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('82', 'stock_date', 'Stock Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('83', 'print_date', 'Print Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('84', 'sales', 'Sales');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('85', 'price', 'Price');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('86', 'sl', 'SL.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('87', 'add_new_category', 'Add new category');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('88', 'category_name', 'Category Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('89', 'save', 'Save');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('90', 'delete', 'Delete');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('91', 'update', 'Update');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('92', 'action', 'Action');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('93', 'manage_your_category', 'Manage your category ');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('94', 'category_edit', 'Category Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('95', 'status', 'Status');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('96', 'active', 'Active');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('97', 'inactive', 'Inactive');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('98', 'save_changes', 'Save Changes');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('99', 'save_and_add_another', 'Save And Add Another');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('100', 'model', 'Model');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('101', 'supplier_price', 'Supplier Price');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('102', 'sell_price', 'Sale Price');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('103', 'image', 'Image');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('104', 'select_one', 'Select One');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('105', 'details', 'Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('106', 'new_product', 'New Product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('107', 'add_new_product', 'Add new product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('108', 'barcode', 'Barcode');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('109', 'qr_code', 'Qr-Code');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('110', 'product_details', 'Product Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('111', 'manage_your_product', 'Manage your product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('112', 'product_edit', 'Product Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('113', 'edit_your_product', 'Edit your product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('114', 'cancel', 'Cancel');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('115', 'incl_vat', 'Incl. Vat');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('116', 'money', 'TK');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('117', 'grand_total', 'Grand Total');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('118', 'quantity', 'Qnty');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('119', 'product_report', 'Product Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('120', 'product_sales_and_purchase_report', 'Product sales and purchase report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('121', 'previous_stock', 'Previous Stock');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('122', 'out', 'Out');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('123', 'in', 'In');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('124', 'to', 'To');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('125', 'previous_balance', 'Previous Credit Balance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('126', 'customer_address', 'Customer Address');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('127', 'customer_mobile', 'Customer Mobile');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('128', 'customer_email', 'Customer Email');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('129', 'add_new_customer', 'Add new customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('130', 'balance', 'Balance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('131', 'mobile', 'Mobile');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('132', 'address', 'Address');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('133', 'manage_your_customer', 'Manage your customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('134', 'customer_edit', 'Customer Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('135', 'paid_customer_list', 'Paid Customer List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('136', 'ammount', 'Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('137', 'customer_ledger', 'Customer Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('138', 'manage_customer_ledger', 'Manage Customer Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('139', 'customer_information', 'Customer Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('140', 'debit_ammount', 'Debit Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('141', 'credit_ammount', 'Credit Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('142', 'balance_ammount', 'Balance Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('143', 'receipt_no', 'Receipt NO');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('144', 'description', 'Description');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('145', 'debit', 'Debit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('146', 'credit', 'Credit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('147', 'item_information', 'Item Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('148', 'total', 'Total');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('149', 'please_select_supplier', 'Please Select Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('150', 'submit', 'Submit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('151', 'submit_and_add_another', 'Submit And Add Another One');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('152', 'add_new_item', 'Add New Item');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('153', 'manage_your_purchase', 'Manage your purchase');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('154', 'purchase_edit', 'Purchase Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('155', 'purchase_ledger', 'Purchase Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('156', 'invoice_information', 'Sale Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('157', 'paid_ammount', 'Paid Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('158', 'discount', 'Dis./Pcs.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('159', 'save_and_paid', 'Save And Paid');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('160', 'payee_name', 'Payee Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('161', 'manage_your_invoice', 'Manage your Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('162', 'invoice_edit', 'Sale Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('163', 'new_pos_invoice', 'New POS Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('164', 'add_new_pos_invoice', 'Add new pos Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('165', 'product_id', 'Product ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('166', 'paid_amount', 'Paid Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('167', 'authorised_by', 'Authorised By');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('168', 'checked_by', 'Checked By');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('169', 'received_by', 'Received By');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('170', 'prepared_by', 'Prepared By');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('171', 'memo_no', 'Memo No');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('172', 'website', 'Website');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('173', 'email', 'Email');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('174', 'invoice_details', 'Sale Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('175', 'reset', 'Reset');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('176', 'payment_account', 'Payment Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('177', 'bank_name', 'Bank Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('178', 'cheque_or_pay_order_no', 'Cheque/Pay Order No');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('179', 'payment_type', 'Payment Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('180', 'payment_from', 'Payment From');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('181', 'payment_date', 'Payment Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('182', 'add_income', 'Add Income');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('183', 'cash', 'Cash');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('184', 'cheque', 'Cheque');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('185', 'pay_order', 'Pay Order');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('186', 'payment_to', 'Payment To');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('187', 'total_outflow_ammount', 'Total Expense Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('188', 'transections', 'Transections');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('189', 'accounts_name', 'Accounts Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('190', 'outflow_report', 'Expense Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('191', 'inflow_report', 'Income Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('192', 'all', 'All');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('193', 'account', 'Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('194', 'from', 'From');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('195', 'account_summary_report', 'Account Summary Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('196', 'search_by_date', 'Search By Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('197', 'cheque_no', 'Cheque No');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('198', 'name', 'Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('199', 'closing_account', 'Closing Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('200', 'close_your_account', 'Close your account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('201', 'last_day_closing', 'Last Day Closing');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('202', 'cash_in', 'Cash In');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('203', 'cash_out', 'Cash Out');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('204', 'cash_in_hand', 'Cash In Hand');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('205', 'add_new_bank', 'Add New Bank');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('206', 'day_closing', 'Day Closing');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('207', 'account_closing_report', 'Account Closing Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('208', 'last_day_ammount', 'Last Day Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('209', 'adjustment', 'Adjustment');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('210', 'pay_type', 'Pay Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('211', 'customer_or_supplier', 'Customer,Supplier Or Others');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('212', 'transection_id', 'Transections ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('213', 'accounts_summary_report', 'Accounts Summary Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('214', 'bank_list', 'Bank List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('215', 'bank_edit', 'Bank Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('216', 'debit_plus', 'Debit (+)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('217', 'credit_minus', 'Credit (-)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('218', 'account_name', 'Account Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('219', 'account_type', 'Account Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('220', 'account_real_name', 'Account Real Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('221', 'manage_account', 'Manage Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('222', 'company_name', 'Niha International');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('223', 'edit_your_company_information', 'Edit your company information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('224', 'company_edit', 'Company Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('225', 'admin', 'Admin');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('226', 'user', 'User');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('227', 'password', 'Password');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('228', 'last_name', 'Last Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('229', 'first_name', 'First Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('230', 'add_new_user_information', 'Add new user information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('231', 'user_type', 'User Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('232', 'user_edit', 'User Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('233', 'rtr', 'RTR');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('234', 'ltr', 'LTR');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('235', 'ltr_or_rtr', 'LTR/RTR');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('236', 'footer_text', 'Footer Text');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('237', 'favicon', 'Favicon');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('238', 'logo', 'Logo');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('239', 'update_setting', 'Update Setting');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('240', 'update_your_web_setting', 'Update your web setting');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('241', 'login', 'Login');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('242', 'your_strong_password', 'Your strong password');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('243', 'your_unique_email', 'Your unique email');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('244', 'please_enter_your_login_information', 'Please enter your login information.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('245', 'update_profile', 'Update Profile');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('246', 'your_profile', 'Your Profile');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('247', 're_type_password', 'Re-Type Password');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('248', 'new_password', 'New Password');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('249', 'old_password', 'Old Password');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('250', 'new_information', 'New Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('251', 'old_information', 'Old Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('252', 'change_your_information', 'Change your information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('253', 'change_your_profile', 'Change your profile');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('254', 'profile', 'Profile');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('255', 'wrong_username_or_password', 'Wrong User Name Or Password !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('256', 'successfully_updated', 'Successfully Updated.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('257', 'blank_field_does_not_accept', 'Blank Field Does Not Accept !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('258', 'successfully_changed_password', 'Successfully changed password.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('259', 'you_are_not_authorised_person', 'You are not authorised person !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('260', 'password_and_repassword_does_not_match', 'Passwor and re-password does not match !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('261', 'new_password_at_least_six_character', 'New Password At Least 6 Character.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('262', 'you_put_wrong_email_address', 'You put wrong email address !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('263', 'cheque_ammount_asjusted', 'Cheque amount adjusted.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('264', 'successfully_payment_paid', 'Successfully Payment Paid.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('265', 'successfully_added', 'Successfully Added.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('266', 'successfully_updated_2_closing_ammount_not_changeale', 'Successfully Updated -2. Note: Closing Amount Not Changeable.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('267', 'successfully_payment_received', 'Successfully Payment Received.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('268', 'already_inserted', 'Already Inserted !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('269', 'successfully_delete', 'Successfully Delete.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('270', 'successfully_created', 'Successfully Created.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('271', 'logo_not_uploaded', 'Logo not uploaded !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('272', 'favicon_not_uploaded', 'Favicon not uploaded !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('273', 'supplier_mobile', 'Supplier Mobile');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('274', 'supplier_address', 'Supplier Address');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('275', 'supplier_details', 'Supplier Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('276', 'add_new_supplier', 'Add New Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('277', 'manage_suppiler', 'Manage Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('278', 'manage_your_supplier', 'Manage your supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('279', 'manage_supplier_ledger', 'Manage supplier ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('280', 'invoice_id', 'Invoice ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('281', 'deposite_id', 'Deposite ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('282', 'supplier_actual_ledger', 'Supplier Payment Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('283', 'supplier_information', 'Supplier Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('284', 'event', 'Event');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('285', 'add_new_income', 'Add New Income');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('286', 'add_expese', 'Add Expense');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('287', 'add_new_expense', 'Add New Expense');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('288', 'total_inflow_ammount', 'Total Income Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('289', 'create_new_invoice', 'Create New Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('290', 'create_pos_invoice', 'Create POS Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('291', 'total_profit', 'Total Profit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('292', 'monthly_progress_report', 'Monthly Progress Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('293', 'total_invoice', 'Total Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('294', 'account_summary', 'Account Summary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('295', 'total_supplier', 'Total Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('296', 'total_product', 'Total Product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('297', 'total_customer', 'Total Customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('298', 'supplier_edit', 'Supplier Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('299', 'add_new_invoice', 'Add New Sale');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('300', 'add_new_purchase', 'Add new purchase');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('301', 'currency', 'Currency');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('302', 'currency_position', 'Currency Position');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('303', 'left', 'Left');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('304', 'right', 'Right');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('305', 'add_tax', 'Add Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('306', 'manage_tax', 'Manage Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('307', 'add_new_tax', 'Add new tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('308', 'enter_tax', 'Enter Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('309', 'already_exists', 'Already Exists !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('310', 'successfully_inserted', 'Successfully Inserted.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('311', 'tax', 'Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('312', 'tax_edit', 'Tax Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('313', 'product_not_added', 'Product not added !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('314', 'total_tax', 'Total Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('315', 'manage_your_supplier_details', 'Manage your supplier details.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('316', 'invoice_description', 'Lorem Ipsum is sim ply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is sim ply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('317', 'thank_you_for_choosing_us', 'Thank you very much for choosing us.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('318', 'billing_date', 'Billing Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('319', 'billing_to', 'Billing To');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('320', 'billing_from', 'Billing From');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('321', 'you_cant_delete_this_product', 'Sorry !!  You can\'t delete this product.This product already used in calculation system!');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('322', 'old_customer', 'Old Customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('323', 'new_customer', 'New Customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('324', 'new_supplier', 'New Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('325', 'old_supplier', 'Old Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('326', 'credit_customer', 'Credit Customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('327', 'account_already_exists', 'This Account Already Exists !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('328', 'edit_income', 'Edit Income');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('329', 'you_are_not_access_this_part', 'You are not authorised person !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('330', 'account_edit', 'Account Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('331', 'due', 'Due');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('332', 'expense_edit', 'Expense Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('333', 'please_select_customer', 'Please select customer !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('334', 'profit_report', 'Profit Report (Sale Wise)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('335', 'total_profit_report', 'Total profit report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('336', 'please_enter_valid_captcha', 'Please enter valid captcha.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('337', 'category_not_selected', 'Category not selected.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('338', 'supplier_not_selected', 'Supplier not selected.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('339', 'please_select_product', 'Please select product.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('340', 'product_model_already_exist', 'Product model already exist !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('341', 'invoice_logo', 'Sale Logo');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('342', 'available_qnty', 'Av. Qnty.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('343', 'you_can_not_buy_greater_than_available_cartoon', 'You can not select grater than available cartoon !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('344', 'customer_details', 'Customer details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('345', 'manage_customer_details', 'Manage customer details.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('346', 'site_key', 'Captcha Site Key');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('347', 'secret_key', 'Captcha Secret Key');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('348', 'captcha', 'Captcha');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('349', 'cartoon_quantity', 'Cartoon Quantity');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('350', 'total_cartoon', 'Total Cartoon');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('351', 'cartoon', 'Cartoon');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('352', 'item_cartoon', 'Item/Cartoon');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('353', 'product_and_supplier_did_not_match', 'Product and supplier did not match !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('354', 'if_you_update_purchase_first_select_supplier_then_product_and_then_quantity', 'If you update purchase,first select supplier then product and then update qnty.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('355', 'item', 'Item');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('356', 'manage_your_credit_customer', 'Manage your credit customer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('357', 'total_quantity', 'Total Quantity');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('358', 'quantity_per_cartoon', 'Quantity per cartoon');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('359', 'barcode_qrcode_scan_here', 'Barcode or QR-code scan here');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('360', 'synchronizer_setting', 'Synchronizer Setting');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('361', 'data_synchronizer', 'Data Synchronizer');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('362', 'hostname', 'Host name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('363', 'username', 'User Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('364', 'ftp_port', 'FTP Port');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('365', 'ftp_debug', 'FTP Debug');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('366', 'project_root', 'Project Root');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('367', 'please_try_again', 'Please try again');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('368', 'save_successfully', 'Save successfully');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('369', 'synchronize', 'Synchronize');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('370', 'internet_connection', 'Internet Connection');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('371', 'outgoing_file', 'Outgoing File');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('372', 'incoming_file', 'Incoming File');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('373', 'ok', 'Ok');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('374', 'not_available', 'Not Available');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('375', 'available', 'Available');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('376', 'download_data_from_server', 'Download data from server');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('377', 'data_import_to_database', 'Data import to database');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('378', 'data_upload_to_server', 'Data uplod to server');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('379', 'please_wait', 'Please Wait');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('380', 'ooops_something_went_wrong', 'Oooops Something went wrong !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('381', 'upload_successfully', 'Upload successfully');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('382', 'unable_to_upload_file_please_check_configuration', 'Unable to upload file please check configuration');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('383', 'please_configure_synchronizer_settings', 'Please configure synchronizer settings');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('384', 'download_successfully', 'Download successfully');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('385', 'unable_to_download_file_please_check_configuration', 'Unable to download file please check configuration');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('386', 'data_import_first', 'Data import past');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('387', 'data_import_successfully', 'Data import successfully');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('388', 'unable_to_import_data_please_check_config_or_sql_file', 'Unable to import data please check config or sql file');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('389', 'total_sale_ctn', 'Total Sale Ctn');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('390', 'in_qnty', 'In Qnty.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('391', 'out_qnty', 'Out Qnty.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('392', 'stock_report_supplier_wise', 'Stock Report (Supplier Wise)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('393', 'all_stock_report_supplier_wise', 'Stock Report (Suppler Wise)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('394', 'select_supplier', 'Select Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('395', 'stock_report_product_wise', 'Stock Report (Product Wise)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('396', 'phone', 'Phone');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('397', 'select_product', 'Select Product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('398', 'in_quantity', 'In Qnty.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('399', 'out_quantity', 'Out Qnty.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('400', 'in_taka', 'In TK.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('401', 'out_taka', 'Out TK.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('402', 'commission', 'Commission');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('403', 'generate_commission', 'Generate Commssion');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('404', 'commission_rate', 'Commission Rate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('405', 'total_ctn', 'Total Ctn.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('406', 'per_pcs_commission', 'Per PCS Commission');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('407', 'total_commission', 'Total Commission');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('408', 'enter', 'Enter');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('409', 'please_add_walking_customer_for_default_customer', 'Please add \'Walking Customer\' for default customer.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('410', 'supplier_ammount', 'Supplier Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('411', 'my_sale_ammount', 'My Sale Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('412', 'signature_pic', 'Signature Picture');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('413', 'branch', 'Branch');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('414', 'ac_no', 'A/C Number');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('415', 'ac_name', 'A/C Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('416', 'bank_transaction', 'Bank Transaction');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('417', 'bank', 'Bank');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('418', 'withdraw_deposite_id', 'Withdraw / Deposite ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('419', 'bank_ledger', 'Bank Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('420', 'note_name', 'Note Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('421', 'pcs', 'Pcs.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('422', '1', '1');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('423', '2', '2');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('424', '5', '5');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('425', '10', '10');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('426', '20', '20');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('427', '50', '50');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('428', '100', '100');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('429', '500', '500');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('430', '1000', '1000');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('431', 'total_discount', 'Total Discount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('432', 'product_not_found', 'Product not found !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('433', 'this_is_not_credit_customer', 'This is not credit customer !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('434', 'personal_loan', 'Personal Loan');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('435', 'add_person', 'Add Person');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('436', 'add_loan', 'Add Loan');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('437', 'add_payment', 'Add Payment');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('438', 'manage_person', 'Manage Person');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('439', 'personal_edit', 'Person Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('440', 'person_ledger', 'Person Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('441', 'backup_restore', 'Backup ');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('442', 'database_backup', 'Database backup');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('443', 'file_information', 'File information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('444', 'filename', 'Filename');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('445', 'size', 'Size');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('446', 'backup_date', 'Backup date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('447', 'backup_now', 'Backup now');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('448', 'restore_now', 'Restore now');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('449', 'are_you_sure', 'Are you sure ?');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('450', 'download', 'Download');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('451', 'backup_and_restore', 'Backup');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('452', 'backup_successfully', 'Backup successfully');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('453', 'delete_successfully', 'Delete successfully');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('454', 'stock_ctn', 'Stock/Qnt');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('455', 'unit', 'Unit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('456', 'meter_m', 'Meter (M)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('457', 'piece_pc', 'Piece (Pc)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('458', 'kilogram_kg', 'Kilogram (Kg)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('459', 'stock_cartoon', 'Stock Cartoon');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('460', 'add_product_csv', 'Add Product (CSV)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('461', 'import_product_csv', 'Import product (CSV)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('462', 'close', 'Close');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('463', 'download_example_file', 'Download example file.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('464', 'upload_csv_file', 'Upload CSV File');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('465', 'csv_file_informaion', 'CSV File Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('466', 'out_of_stock', 'Out Of Stock');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('467', 'others', 'Others');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('468', 'full_paid', 'Full Paid');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('469', 'successfully_saved', 'Your Data Successfully Saved');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('470', 'manage_loan', 'Manage Loan');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('471', 'receipt', 'Receipt');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('472', 'payment', 'Payment');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('473', 'cashflow', 'Daily Cash Flow');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('474', 'signature', 'Signature');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('475', 'supplier_reports', 'Supplier Reports');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('476', 'generate', 'Generate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('477', 'todays_overview', 'Todays Overview');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('478', 'last_sales', 'Last Sales');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('479', 'manage_transaction', 'Manage Transaction');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('480', 'daily_summary', 'Daily Summary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('481', 'daily_cash_flow', 'Daily Cash Flow');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('482', 'custom_report', 'Custom Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('483', 'transaction', 'Transaction');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('484', 'receipt_amount', 'Receipt Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('485', 'transaction_details_datewise', 'Transaction Details Datewise');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('486', 'cash_closing', 'Cash Closing');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('487', 'you_can_not_buy_greater_than_available_qnty', 'You can not buy greater than available qnty.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('488', 'supplier_id', 'Supplier ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('489', 'category_id', 'Category ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('490', 'select_report', 'Select Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('491', 'supplier_summary', 'Supplier summary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('492', 'sales_payment_actual', 'Sales payment actual');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('493', 'today_already_closed', 'Today already closed.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('494', 'root_account', 'Root Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('495', 'office', 'Office');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('496', 'loan', 'Loan');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('497', 'transaction_mood', 'Transaction Mood');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('498', 'select_account', 'Select Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('499', 'add_receipt', 'Add Receipt');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('500', 'update_transaction', 'Update Transaction');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('501', 'no_stock_found', 'No Stock Found !');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('502', 'admin_login_area', 'Admin Login Area');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('503', 'print_qr_code', 'Print QR Code');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('504', 'discount_type', 'Discount Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('505', 'discount_percentage', 'Discount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('506', 'fixed_dis', 'Fixed Dis.');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('507', 'return', 'Return');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('508', 'stock_return_list', 'Stock Return List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('509', 'wastage_return_list', 'Wastage Return List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('510', 'return_invoice', 'Sale Return');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('511', 'sold_qty', 'Sold Qty');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('512', 'ret_quantity', 'Return Qty');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('513', 'deduction', 'Deduction');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('514', 'check_return', 'Check Return');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('515', 'reason', 'Reason');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('516', 'usablilties', 'Usability');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('517', 'adjs_with_stck', 'Adjust With Stock');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('518', 'return_to_supplier', 'Return To Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('519', 'wastage', 'Wastage');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('520', 'to_deduction', 'Total Deduction ');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('521', 'nt_return', 'Net Return Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('522', 'return_list', 'Return List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('523', 'add_return', 'Add Return');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('524', 'per_qty', 'Purchase Qty');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('525', 'return_supplier', 'Supplier Return');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('526', 'stock_purchase', 'Stock Purchase Price');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('527', 'stock_sale', 'Stock Sale Price');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('528', 'supplier_return', 'Supplier Return');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('529', 'purchase_id', 'Purchase ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('530', 'return_id', 'Return ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('531', 'supplier_return_list', 'Supplier Return List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('532', 'c_r_slist', 'Stock Return Stock');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('533', 'wastage_list', 'Wastage List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('534', 'please_input_correct_invoice_id', 'Please Input a Correct Sale ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('535', 'please_input_correct_purchase_id', 'Please Input Your Correct  Purchase ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('536', 'add_more', 'Add More');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('537', 'prouct_details', 'Product Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('538', 'prouct_detail', 'Product Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('539', 'stock_return', 'Stock Return');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('540', 'choose_transaction', 'Select Transaction');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('541', 'transection_category', 'Select  Category');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('542', 'transaction_categry', 'Select Category');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('543', 'search_supplier', 'Search Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('544', 'customer_id', 'Customer ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('545', 'search_customer', 'Search Customer Invoice');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('546', 'serial_no', 'SN');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('547', 'item_discount', 'Item Discount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('548', 'invoice_discount', 'Sale Discount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('549', 'add_unit', 'Add Unit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('550', 'manage_unit', 'Manage Unit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('551', 'add_new_unit', 'Add New Unit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('552', 'unit_name', 'Unit Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('553', 'payment_amount', 'Payment Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('554', 'manage_your_unit', 'Manage Your Unit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('555', 'unit_id', 'Unit ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('556', 'unit_edit', 'Unit Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('557', 'vat', 'Vat');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('558', 'sales_report_category_wise', 'Sales Report (Category wise)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('559', 'purchase_report_category_wise', 'Purchase Report (Category wise)');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('560', 'category_wise_purchase_report', 'Category wise purchase report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('561', 'category_wise_sales_report', 'Category wise sales report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('562', 'best_sale_product', 'Best Sale Product');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('563', 'all_best_sales_product', 'All Best Sales Products');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('564', 'todays_customer_receipt', 'Todays Customer Receipt');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('565', 'not_found', 'Record not found');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('566', 'collection', 'Collection');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('567', 'increment', 'Increment');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('568', 'accounts_tree_view', 'Accounts Tree View');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('569', 'debit_voucher', 'Debit Voucher');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('570', 'voucher_no', 'Voucher No');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('571', 'credit_account_head', 'Credit Account Head');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('572', 'remark', 'Remark');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('573', 'code', 'Code');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('574', 'amount', 'Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('575', 'approved', 'Approved');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('576', 'debit_account_head', 'Debit Account Head');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('577', 'credit_voucher', 'Credit Voucher');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('578', 'find', 'Find');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('579', 'transaction_date', 'Transaction Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('580', 'voucher_type', 'Voucher Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('581', 'particulars', 'Particulars');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('582', 'with_details', 'With Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('583', 'general_ledger', 'General Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('584', 'general_ledger_of', 'General ledger of');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('585', 'pre_balance', 'Pre Balance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('586', 'current_balance', 'Current Balance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('587', 'to_date', 'To Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('588', 'from_date', 'From Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('589', 'trial_balance', 'Trial Balance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('590', 'authorized_signature', 'Authorized Signature');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('591', 'chairman', 'Chairman');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('592', 'total_income', 'Total Income');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('593', 'statement_of_comprehensive_income', 'Statement of Comprehensive Income');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('594', 'profit_loss', 'Profit Loss');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('595', 'cash_flow_report', 'Cash Flow Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('596', 'cash_flow_statement', 'Cash Flow Statement');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('597', 'amount_in_dollar', 'Amount In Dollar');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('598', 'opening_cash_and_equivalent', 'Opening Cash and Equivalent');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('599', 'coa_print', 'Coa Print');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('600', 'cash_flow', 'Cash Flow');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('601', 'cash_book', 'Cash Book');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('602', 'bank_book', 'Bank Book');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('603', 'c_o_a', 'Chart of Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('604', 'journal_voucher', 'Journal Voucher');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('605', 'contra_voucher', 'Contra Voucher');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('606', 'voucher_approval', 'Vouchar Approval');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('607', 'supplier_payment', 'Supplier Payment');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('608', 'customer_receive', 'Customer Receive');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('609', 'gl_head', 'General Head');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('610', 'account_code', 'Account Head');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('611', 'opening_balance', 'Opening Balance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('612', 'head_of_account', 'Head of Account');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('613', 'inventory_ledger', 'Inventory Ledger');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('614', 'newpassword', 'New Password');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('615', 'password_recovery', 'Password Recovery');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('616', 'forgot_password', 'Forgot Password ??');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('617', 'send', 'Send');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('618', 'due_report', 'Due Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('619', 'due_amount', 'Due Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('620', 'download_sample_file', 'Download Sample File');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('621', 'customer_csv_upload', 'Customer Csv Upload');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('622', 'csv_supplier', 'Csv Upload Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('623', 'csv_upload_supplier', 'Csv Upload Supplier');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('624', 'previous', 'Previous');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('625', 'net_total', 'Net Total');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('626', 'currency_list', 'Currency List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('627', 'currency_name', 'Currency Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('628', 'currency_icon', 'Currency Symbol');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('629', 'add_currency', 'Add Currency');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('630', 'role_permission', 'Role Permission');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('631', 'role_list', 'Role List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('632', 'user_assign_role', 'User Assign Role');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('633', 'permission', 'Permission');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('634', 'add_role', 'Add Role');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('635', 'add_module', 'Add Module');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('636', 'module_name', 'Module Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('637', 'office_loan', 'Office Loan');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('638', 'add_menu', 'Add Menu');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('639', 'menu_name', 'Menu Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('640', 'sl_no', 'Sl No');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('641', 'create', 'Create');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('642', 'read', 'Read');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('643', 'role_name', 'Role Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('644', 'qty', 'Quantity');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('645', 'max_rate', 'Max Rate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('646', 'min_rate', 'Min Rate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('647', 'avg_rate', 'Average Rate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('648', 'role_permission_added_successfully', 'Role Permission Successfully Added');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('649', 'update_successfully', 'Successfully Updated');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('650', 'role_permission_updated_successfully', 'Role Permission Successfully Updated ');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('651', 'shipping_cost', 'Shipping Cost');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('652', 'in_word', 'In Word ');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('653', 'shipping_cost_report', 'Shipping Cost Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('654', 'cash_book_report', 'Cash Book Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('655', 'inventory_ledger_report', 'Inventory Ledger Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('656', 'trial_balance_with_opening_as_on', 'Trial Balance With Opening As On');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('657', 'type', 'Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('658', 'taka_only', 'Taka Only');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('659', 'item_description', 'Desc');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('660', 'sold_by', 'Sold By');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('661', 'user_wise_sales_report', 'User Wise Sales Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('662', 'user_name', 'User Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('663', 'total_sold', 'Total Sold');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('664', 'user_wise_sale_report', 'User Wise Sales Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('665', 'barcode_or_qrcode', 'Barcode/QR-code');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('666', 'category_csv_upload', 'Category Csv  Upload');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('667', 'unit_csv_upload', 'Unit Csv Upload');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('668', 'invoice_return_list', 'Sales Return list');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('669', 'invoice_return', 'Sales Return');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('670', 'tax_report', 'Tax Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('671', 'select_tax', 'Select Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('672', 'hrm', 'HRM');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('673', 'employee', 'Employee');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('674', 'add_employee', 'Add Employee');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('675', 'manage_employee', 'Manage Employee');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('676', 'attendance', 'Attendance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('677', 'add_attendance', 'Attendance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('678', 'manage_attendance', 'Manage Attendance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('679', 'payroll', 'Payroll');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('680', 'add_payroll', 'Payroll');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('681', 'manage_payroll', 'Manage Payroll');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('682', 'employee_type', '');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('683', 'employee_designation', 'Employee Designation');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('684', 'designation', 'Designation');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('685', 'add_designation', 'Add Designation');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('686', 'manage_designation', 'Manage Designation');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('687', 'designation_update_form', 'Designation Update form');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('688', 'picture', 'Picture');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('689', 'country', 'Country');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('690', 'blood_group', 'Blood Group');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('691', 'address_line_1', 'Address Line 1');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('692', 'address_line_2', 'Address Line 2');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('693', 'zip', 'Zip code');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('694', 'city', 'City');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('695', 'hour_rate_or_salary', 'Houre Rate/Salary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('696', 'rate_type', 'Rate Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('697', 'hourly', 'Hourly');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('698', 'salary', 'Salary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('699', 'employee_update', 'Employee Update');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('700', 'checkin', 'Check In');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('701', 'employee_name', 'Employee Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('702', 'checkout', 'Check Out');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('703', 'confirm_clock', 'Confirm Clock');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('704', 'stay', 'Stay Time');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('705', 'sign_in', 'Sign In');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('706', 'check_in', 'Check In');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('707', 'single_checkin', 'Single Check In');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('708', 'bulk_checkin', 'Bulk Check In');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('709', 'successfully_checkout', 'Successfully Checkout');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('710', 'attendance_report', 'Attendance Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('711', 'datewise_report', 'Date Wise Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('712', 'employee_wise_report', 'Employee Wise Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('713', 'date_in_time_report', 'Date In Time Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('714', 'request', 'Request');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('715', 'sign_out', 'Sign Out');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('716', 'work_hour', 'Work Hours');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('717', 's_time', 'Start Time');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('718', 'e_time', 'In Time');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('719', 'salary_benefits_type', 'Benefits Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('720', 'salary_benefits', 'Salary Benefits');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('721', 'beneficial_list', 'Benefit List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('722', 'add_beneficial', 'Add Benefits');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('723', 'add_benefits', 'Add Benefits');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('724', 'benefits_list', 'Benefit List');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('725', 'benefit_type', 'Benefit Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('726', 'benefits', 'Benefit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('727', 'manage_benefits', 'Manage Benefits');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('728', 'deduct', 'Deduct');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('729', 'add', 'Add');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('730', 'add_salary_setup', 'Add Salary Setup');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('731', 'manage_salary_setup', 'Manage Salary Setup');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('732', 'basic', 'Basic');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('733', 'salary_type', 'Salary Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('734', 'addition', 'Addition');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('735', 'gross_salary', 'Gross Salary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('736', 'set', 'Set');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('737', 'salary_generate', 'Salary Generate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('738', 'manage_salary_generate', 'Manage Salary Generate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('739', 'sal_name', 'Salary Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('740', 'gdate', 'Generated Date');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('741', 'generate_by', 'Generated By');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('742', 'the_salary_of', 'The Salary of ');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('743', 'already_generated', ' Already Generated');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('744', 'salary_month', 'Salary Month');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('745', 'successfully_generated', 'Successfully Generated');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('746', 'salary_payment', 'Salary Payment');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('747', 'employee_salary_payment', 'Employee Salary Payment');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('748', 'total_salary', 'Total Salary');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('749', 'total_working_minutes', 'Total Working Hours');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('750', 'working_period', 'Working Period');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('751', 'paid_by', 'Paid By');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('752', 'pay_now', 'Pay Now ');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('753', 'confirm', 'Confirm');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('754', 'successfully_paid', 'Successfully Paid');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('755', 'add_incometax', 'Add Income Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('756', 'setup_tax', 'Setup Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('757', 'start_amount', 'Start Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('758', 'end_amount', 'End Amount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('759', 'tax_rate', 'Tax Rate');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('760', 'setup', 'Setup');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('761', 'manage_income_tax', 'Manage Income Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('762', 'income_tax_updateform', 'Income tax Update form');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('763', 'positional_information', 'Positional Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('764', 'personal_information', 'Personal Information');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('765', 'timezone', 'Time Zone');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('766', 'sms', 'SMS');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('767', 'sms_configure', 'SMS Configure');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('768', 'url', 'URL');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('769', 'sender_id', 'Sender ID');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('770', 'api_key', 'Api Key');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('771', 'gui_pos', 'GUI POS');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('772', 'manage_service', 'Manage Service');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('773', 'service', 'Service');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('774', 'add_service', 'Add Service');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('775', 'service_edit', 'Service Edit');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('776', 'service_csv_upload', 'Service CSV Upload');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('777', 'service_name', 'Service Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('778', 'charge', 'Charge');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('779', 'service_invoice', 'Service Invoice');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('780', 'service_discount', 'Service Discount');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('781', 'hanging_over', 'ETD');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('782', 'service_details', 'Service Details');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('783', 'tax_settings', 'Tax Settings');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('784', 'default_value', 'Default Value');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('785', 'number_of_tax', 'Number of Tax');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('786', 'please_select_employee', 'Please Select Employee');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('787', 'manage_service_invoice', 'Manage Service Invoice');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('788', 'update_service_invoice', 'Update Service Invoice');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('789', 'customer_wise_tax_report', 'Customer Wise  Tax Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('790', 'service_id', 'Service Id');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('791', 'invoice_wise_tax_report', 'Invoice Wise Tax Report');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('792', 'reg_no', 'Reg No');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('793', 'update_now', 'Update Now');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('794', 'import', 'Import');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('795', 'add_expense_item', 'Add Expense Item');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('796', 'manage_expense_item', 'Manage Expense Item');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('797', 'add_expense', 'Add Expense');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('798', 'manage_expense', 'Manage Expense');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('799', 'expense_statement', 'Expense Statement');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('800', 'expense_type', 'Expense Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('801', 'expense_item_name', 'Expense Item Name');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('802', 'stock_purchase_price', 'Stock Purchase Price');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('803', 'purchase_price', 'Purchase Price');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('804', 'customer_advance', 'Customer Advance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('805', 'advance_type', 'Advance Type');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('806', 'restore', 'Restore');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('807', 'supplier_advance', 'Supplier Advance');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('808', 'please_input_correct_invoice_no', 'Please Input Correct Invoice NO');
INSERT INTO `language` (`id`, `phrase`, `english`) VALUES ('809', 'backup', 'Back Up');


#
# TABLE STRUCTURE FOR: mail_setting
#

DROP TABLE IF EXISTS `mail_setting`;

CREATE TABLE `mail_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `protocol` varchar(20) NOT NULL,
  `mailpath` varchar(255) NOT NULL,
  `mailtype` varchar(20) NOT NULL,
  `validate_email` varchar(20) NOT NULL,
  `wordwrap` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: main_department
#

DROP TABLE IF EXISTS `main_department`;

CREATE TABLE `main_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: main_department_lang
#

DROP TABLE IF EXISTS `main_department_lang`;

CREATE TABLE `main_department_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `main_id` int(11) NOT NULL,
  `language` varchar(15) CHARACTER SET utf8 NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: manufacturer_information
#

DROP TABLE IF EXISTS `manufacturer_information`;

CREATE TABLE `manufacturer_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_name` varchar(150) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: medication
#

DROP TABLE IF EXISTS `medication`;

CREATE TABLE `medication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `medicine_id` int(11) NOT NULL,
  `dosage` int(3) NOT NULL,
  `per_day_intake` int(1) NOT NULL,
  `full_stomach` tinyint(1) DEFAULT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `other_instruction` text COLLATE utf8_unicode_ci NOT NULL,
  `prescribe_by` int(11) NOT NULL,
  `intake_time` time NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: medication_details
#

DROP TABLE IF EXISTS `medication_details`;

CREATE TABLE `medication_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medication_id` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `given_time` time NOT NULL,
  `next_due_time` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: medication_p
#

DROP TABLE IF EXISTS `medication_p`;

CREATE TABLE `medication_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `remarks` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: medicine_type
#

DROP TABLE IF EXISTS `medicine_type`;

CREATE TABLE `medicine_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: message
#

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `datetime` datetime NOT NULL,
  `sender_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=unseen, 1=seen, 2=delete',
  `receiver_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=unseen, 1=seen, 2=delete',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: module
#

DROP TABLE IF EXISTS `module`;

CREATE TABLE `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `directory` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('1', 'invoice', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('2', 'accounts', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('3', 'category', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('4', 'product', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('5', 'customer', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('6', 'unit', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('7', 'supplier', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('8', 'purchase', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('9', 'return', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('10', 'tax', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('11', 'stock', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('12', 'report', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('13', 'bank', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('14', 'commission', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('15', 'office_loan', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('16', 'personal_loan', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('18', 'data_synchronizer', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('19', 'web_settings', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('20', 'role_permission', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('21', 'hrm', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('22', 'attendance', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('23', 'payroll', NULL, NULL, NULL, '1');
INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES ('24', 'service', NULL, NULL, NULL, '1');


#
# TABLE STRUCTURE FOR: notes
#

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `cash_date` varchar(20) NOT NULL,
  `1000n` varchar(11) NOT NULL,
  `500n` varchar(11) NOT NULL,
  `100n` varchar(11) NOT NULL,
  `50n` varchar(11) NOT NULL,
  `20n` varchar(11) NOT NULL,
  `10n` varchar(11) NOT NULL,
  `5n` varchar(11) NOT NULL,
  `2n` varchar(11) NOT NULL,
  `1n` varchar(30) NOT NULL,
  `grandtotal` varchar(30) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `notes` (`note_id`, `cash_date`, `1000n`, `500n`, `100n`, `50n`, `20n`, `10n`, `5n`, `2n`, `1n`, `grandtotal`) VALUES ('1', '2019-08-18', '', '', '', '', '', '', '', '', '', '');


#
# TABLE STRUCTURE FOR: notice
#

DROP TABLE IF EXISTS `notice`;

CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `assign_by` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: pa_visit
#

DROP TABLE IF EXISTS `pa_visit`;

CREATE TABLE `pa_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(11) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(2) NOT NULL COMMENT '2=doctor and 5=nurse',
  `user_id` int(11) NOT NULL,
  `visit_date` date NOT NULL,
  `visit_time` time NOT NULL,
  `finding` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `instructions` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `fees` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: patient
#

DROP TABLE IF EXISTS `patient`;

CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(20) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `blood_group` varchar(10) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `affliate` varchar(50) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `refer_by` int(11) DEFAULT NULL,
  `is_insurance` tinyint(4) NOT NULL,
  `insurance_company` varchar(200) DEFAULT NULL,
  `insurance_policy` varchar(200) DEFAULT NULL,
  `user_role` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: payment_trans
#

DROP TABLE IF EXISTS `payment_trans`;

CREATE TABLE `payment_trans` (
  `transection_id` varchar(200) NOT NULL,
  `tracing_id` varchar(200) NOT NULL,
  `payment_type` varchar(10) NOT NULL,
  `date` varchar(50) DEFAULT NULL,
  `amount` float NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: payroll_tax_setup
#

DROP TABLE IF EXISTS `payroll_tax_setup`;

CREATE TABLE `payroll_tax_setup` (
  `tax_setup_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `start_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `end_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `status` varchar(30) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`tax_setup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: person_information
#

DROP TABLE IF EXISTS `person_information`;

CREATE TABLE `person_information` (
  `person_id` varchar(50) NOT NULL,
  `person_name` varchar(50) NOT NULL,
  `person_phone` varchar(50) NOT NULL,
  `person_address` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `person_information` (`person_id`, `person_name`, `person_phone`, `person_address`, `status`) VALUES ('JMGCK5MVCN', 'dhaka electronics', '0464644', '', '1');
INSERT INTO `person_information` (`person_id`, `person_name`, `person_phone`, `person_address`, `status`) VALUES ('VJY67W3YTE', 'tesco ', '01', '', '1');
INSERT INTO `person_information` (`person_id`, `person_name`, `person_phone`, `person_address`, `status`) VALUES ('KDN4CJGVRK', 'abul hardware', '014454', '', '1');


#
# TABLE STRUCTURE FOR: person_ledger
#

DROP TABLE IF EXISTS `person_ledger`;

CREATE TABLE `person_ledger` (
  `transaction_id` varchar(50) NOT NULL,
  `person_id` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `debit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `credit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `details` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=no paid,2=paid',
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `person_ledger` (`transaction_id`, `person_id`, `date`, `debit`, `credit`, `details`, `status`, `id`) VALUES ('KT84M3I2O7', 'JMGCK5MVCN', '2019-06-27', '150.00', '0.00', 'rijgiwjfrw', '1', '0');
INSERT INTO `person_ledger` (`transaction_id`, `person_id`, `date`, `debit`, `credit`, `details`, `status`, `id`) VALUES ('F6E1J7D1DX', 'VJY67W3YTE', '2019-06-27', '500.00', '0.00', 'aDSADADF', '1', '0');
INSERT INTO `person_ledger` (`transaction_id`, `person_id`, `date`, `debit`, `credit`, `details`, `status`, `id`) VALUES ('XJ4B8N4TWO', 'KDN4CJGVRK', '2019-06-27', '650.00', '0.00', 'fdfg', '1', '0');
INSERT INTO `person_ledger` (`transaction_id`, `person_id`, `date`, `debit`, `credit`, `details`, `status`, `id`) VALUES ('VS6VVPYSTP', 'JMGCK5MVCN', '2019-06-28', '555.00', '0.00', 'Thanks ', '1', '0');
INSERT INTO `person_ledger` (`transaction_id`, `person_id`, `date`, `debit`, `credit`, `details`, `status`, `id`) VALUES ('QV9JT8IFJE', 'VJY67W3YTE', '2019-06-28', '0.00', '333.00', 'good to see u ', '2', '0');


#
# TABLE STRUCTURE FOR: personal_loan
#

DROP TABLE IF EXISTS `personal_loan`;

CREATE TABLE `personal_loan` (
  `per_loan_id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(30) NOT NULL,
  `person_id` varchar(30) NOT NULL,
  `debit` decimal(12,2) DEFAULT '0.00',
  `credit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `date` varchar(30) NOT NULL,
  `details` varchar(100) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=no paid,2=paid',
  PRIMARY KEY (`per_loan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: pesonal_loan_information
#

DROP TABLE IF EXISTS `pesonal_loan_information`;

CREATE TABLE `pesonal_loan_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` varchar(50) NOT NULL,
  `person_name` varchar(50) NOT NULL,
  `person_phone` varchar(30) NOT NULL,
  `person_address` text NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: portfolio
#

DROP TABLE IF EXISTS `portfolio`;

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `language` varchar(15) NOT NULL,
  `title` varchar(60) NOT NULL,
  `institute` varchar(100) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: pr_case_study
#

DROP TABLE IF EXISTS `pr_case_study`;

CREATE TABLE `pr_case_study` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) DEFAULT NULL,
  `food_allergies` varchar(255) DEFAULT NULL,
  `tendency_bleed` varchar(255) DEFAULT NULL,
  `heart_disease` varchar(255) DEFAULT NULL,
  `high_blood_pressure` varchar(255) DEFAULT NULL,
  `diabetic` varchar(255) DEFAULT NULL,
  `surgery` varchar(255) DEFAULT NULL,
  `accident` varchar(255) DEFAULT NULL,
  `others` varchar(255) DEFAULT NULL,
  `family_medical_history` varchar(255) DEFAULT NULL,
  `current_medication` varchar(255) DEFAULT NULL,
  `female_pregnancy` varchar(255) DEFAULT NULL,
  `breast_feeding` varchar(255) DEFAULT NULL,
  `health_insurance` varchar(255) DEFAULT NULL,
  `low_income` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: pr_prescription
#

DROP TABLE IF EXISTS `pr_prescription`;

CREATE TABLE `pr_prescription` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appointment_id` varchar(30) DEFAULT NULL,
  `patient_id` varchar(30) NOT NULL,
  `patient_type` varchar(50) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `chief_complain` text,
  `insurance_id` int(11) DEFAULT NULL,
  `policy_no` varchar(255) DEFAULT NULL,
  `weight` varchar(50) DEFAULT NULL,
  `blood_pressure` varchar(255) DEFAULT NULL,
  `medicine` text,
  `diagnosis` text,
  `visiting_fees` float DEFAULT NULL,
  `patient_notes` text,
  `reference_by` varchar(50) DEFAULT NULL,
  `reference_to` varchar(50) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `refer_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: product_category
#

DROP TABLE IF EXISTS `product_category`;

CREATE TABLE `product_category` (
  `category_id` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `product_category` (`category_id`, `category_name`, `status`) VALUES ('KxSmHwjojMR79nJ', 'food', '1');
INSERT INTO `product_category` (`category_id`, `category_name`, `status`) VALUES ('dSvieq1d2aFVO5W', 'goodsd', '1');


#
# TABLE STRUCTURE FOR: product_information
#

DROP TABLE IF EXISTS `product_information`;

CREATE TABLE `product_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(100) NOT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `tax` float DEFAULT NULL COMMENT 'Tax in %',
  `serial_no` varchar(200) DEFAULT NULL,
  `product_model` varchar(100) NOT NULL,
  `product_details` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `tax0` text,
  `tax1` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `product_information` (`id`, `product_id`, `category_id`, `product_name`, `price`, `unit`, `tax`, `serial_no`, `product_model`, `product_details`, `image`, `status`, `tax0`, `tax1`) VALUES ('1', '42156427', 'KxSmHwjojMR79nJ', 'Test Product', '100', 'demos', '0', 'sfs,dfsd', 'Test Model', 'sdfsadf', 'http://localhost/saleserp_sas_v-2/my-assets/image/product.png', '1', '0.01', '0.02');
INSERT INTO `product_information` (`id`, `product_id`, `category_id`, `product_name`, `price`, `unit`, `tax`, `serial_no`, `product_model`, `product_details`, `image`, `status`, `tax0`, `tax1`) VALUES ('2', '33845288', '', 'Second Product', '120', 'demos', '0', 'asdfsd,sdfasdf,454', 'sdfsa', 'sdafsdf', 'http://localhost/saleserp_sas_v-2/my-assets/image/product.png', '1', '0.01', '0.02');


#
# TABLE STRUCTURE FOR: product_price_history
#

DROP TABLE IF EXISTS `product_price_history`;

CREATE TABLE `product_price_history` (
  `product_pr_his_id` int(11) NOT NULL,
  `product_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `supplier_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `supplier_price` double NOT NULL,
  `date_of_price_buy` varchar(30) CHARACTER SET latin1 NOT NULL,
  `affect_time_pc` varchar(30) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: product_purchase
#

DROP TABLE IF EXISTS `product_purchase`;

CREATE TABLE `product_purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` varchar(100) NOT NULL,
  `chalan_no` varchar(100) NOT NULL,
  `supplier_id` varchar(100) NOT NULL,
  `grand_total_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total_discount` decimal(10,2) DEFAULT '0.00',
  `purchase_date` varchar(50) NOT NULL,
  `purchase_details` text NOT NULL,
  `status` int(2) NOT NULL,
  `bank_id` varchar(30) DEFAULT NULL,
  `payment_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('21', '20190904103141', '54', '6688VNWJ24FA5LK7RSBQ', '472.00', NULL, '2019-09-04', 'From app', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('2', '20190903054128', '1', '6688VNWJ24FA5LK7RSBQ', '68.00', NULL, '2019-09-03', 'sdfsdf', '1', '', '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('3', '20190903054203', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', '0.00', '2019-09-03', 'sdfsdf', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('4', '20190903054240', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', '0.00', '2019-09-03', 'sdfsdf', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('5', '20190903054749', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', '0.00', '2019-09-03', 'sdfsdf', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('6', '20190903055022', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', '0.00', '2019-09-03', 'sdfsdf', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('7', '20190903055114', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', NULL, '2019-09-03', 'sdfsdf', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('8', '20190903055139', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', NULL, '2019-09-03', 'sdfsdf', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('22', '20190904103318', '56', '6688VNWJ24FA5LK7RSBQ', '60.00', NULL, '2019-09-04', 'From app', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('17', '20190903094416', '1', '6688VNWJ24FA5LK7RSBQ', '27303.00', NULL, '2019-09-03', 'From app', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('18', '20190904070737', '1', '6688VNWJ24FA5LK7RSBQ', '30.00', NULL, '2019-09-04', 'From app', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('19', '20190904071000', '1', '6688VNWJ24FA5LK7RSBQ', '254.00', NULL, '2019-09-04', 'From app', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('20', '20190904094916', '345', '6688VNWJ24FA5LK7RSBQ', '60.00', NULL, '2019-09-04', 'From app', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('15', '20190903061451', '1', '6688VNWJ24FA5LK7RSBQ', '960.00', NULL, '2019-09-03', 'From app', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('25', '20190904112702', '56', '6688VNWJ24FA5LK7RSBQ', '1089.00', NULL, '2019-09-05', 'From app', '1', NULL, '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('28', '20190924143851', '1', 'XUQB7JFFJSFXKGOZ9VD3', '4500.00', NULL, '2019-09-24', 'dfsdf', '1', '', '1');
INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES ('29', '20190925103121', '1', 'XUQB7JFFJSFXKGOZ9VD3', '3300.00', NULL, '2019-09-25', 'fdgsdf', '1', '', '1');


#
# TABLE STRUCTURE FOR: product_purchase_details
#

DROP TABLE IF EXISTS `product_purchase_details`;

CREATE TABLE `product_purchase_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_detail_id` varchar(100) NOT NULL,
  `purchase_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `quantity` decimal(10,2) NOT NULL DEFAULT '0.00',
  `rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` float DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('1', 'HwV9Lka9y0Q7EMX', '20190904071000', '7898940742035', '46.00', '5.00', '230.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('2', 'v6RoAsfxokjSNuY', '20190903054128', '38296235', '4.00', '5.00', '20.00', NULL, '0');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('3', 'CHquHtcMuCOCrOi', '20190903054203', '38296235', '4.00', '5.00', '20.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('4', 'yHXsMrhGt7ZJ3HO', '20190903054240', '38296235', '4.00', '5.00', '20.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('5', '5OtNQiBYfaebR8', '20190903054128', '7898940742035', '12.00', '4.00', '48.00', NULL, '0');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('6', 'wDcH2YQaw8p8jq5', '20190903054749', '38296235', '4.00', '5.00', '20.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('7', 'kJ27v3EU3qcaHus', '20190903055022', '38296235', '4.00', '5.00', '20.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('8', 'FirxTBPuH1Y2EL1', '20190903055114', '38296235', '4.00', '5.00', '20.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('9', 'mLWS3BmJqML2iDr', '20190903055139', '38296235', '4.00', '5.00', '20.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('10', 'G8AFobHGR5UWw8m', '20190904094916', '38296235', '12.00', '5.00', '60.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('11', 'Nl5yLHBNi94w68v', '20190904071000', '38296235', '3.00', '4.00', '12.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('12', 'kDmvqKLQjVjAQXz', '20190903094416', '38296235', '23.00', '24.00', '552.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('13', 'K62s3i2CpTsVNbT', '20190903094416', '7898940742035', '123.00', '213.00', '26199.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('14', '1g6TnSGhXbYWfbr', '20190904070737', '38296235', '5.00', '6.00', '30.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('15', 'USKT2mZi3N8L9km', '20190903061451', '7898940742035', '120.00', '8.00', '960.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('16', 'fodJqtjieRIu6dd', '20190904103141', '7898940742035', '46.00', '5.00', '230.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('17', 'pI9aUsHj123UkNI', '20190904103141', '38296235', '3.00', '4.00', '12.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('18', 'jpCCru2WcxMbvP', '20190904103318', '7898940742035', '5.00', '4.00', '20.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('19', '1jVMcML2BGRKHDV', '20190904103318', '34113672', '5.00', '4.00', '20.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('20', 'rMMDIWv35ViCTvr', '20190904112702', '38296235', '33.00', '33.00', '1089.00', '0', '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('21', 'HcSerknWOJ6dG6u', '20190924143851', '42156427', '10.00', '90.00', '4500.00', NULL, '1');
INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES ('22', 'eUY7wbvQLChrWms', '20190925103121', '33845288', '30.00', '110.00', '3300.00', NULL, '1');


#
# TABLE STRUCTURE FOR: product_return
#

DROP TABLE IF EXISTS `product_return`;

CREATE TABLE `product_return` (
  `return_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `product_id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `invoice_id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `purchase_id` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `date_purchase` varchar(20) CHARACTER SET latin1 NOT NULL,
  `date_return` varchar(30) CHARACTER SET latin1 NOT NULL,
  `byy_qty` float NOT NULL,
  `ret_qty` float NOT NULL,
  `customer_id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `supplier_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `product_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `deduction` float NOT NULL,
  `total_deduct` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_ret_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `net_total_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reason` text CHARACTER SET latin1 NOT NULL,
  `usablity` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `product_return` (`return_id`, `product_id`, `invoice_id`, `purchase_id`, `date_purchase`, `date_return`, `byy_qty`, `ret_qty`, `customer_id`, `supplier_id`, `product_rate`, `deduction`, `total_deduct`, `total_tax`, `total_ret_amount`, `net_total_amount`, `reason`, `usablity`) VALUES ('956269284242668', '19276692', '', '20190731114746', '2019-07-31', '2019-08-01', '50', '1', '', 'WPXXU7UC1Z4ID1HUVU53', '40000.00', '0', '0.00', '0.00', '40000.00', '40500.00', 'sdfasdf', '2');
INSERT INTO `product_return` (`return_id`, `product_id`, `invoice_id`, `purchase_id`, `date_purchase`, `date_return`, `byy_qty`, `ret_qty`, `customer_id`, `supplier_id`, `product_rate`, `deduction`, `total_deduct`, `total_tax`, `total_ret_amount`, `net_total_amount`, `reason`, `usablity`) VALUES ('465999969336718', '34113672', '', '20190731114746', '2019-07-31', '2019-08-01', '50', '2', '', 'WPXXU7UC1Z4ID1HUVU53', '250.00', '0', '0.00', '0.00', '500.00', '40500.00', 'sdfasdf', '2');
INSERT INTO `product_return` (`return_id`, `product_id`, `invoice_id`, `purchase_id`, `date_purchase`, `date_return`, `byy_qty`, `ret_qty`, `customer_id`, `supplier_id`, `product_rate`, `deduction`, `total_deduct`, `total_tax`, `total_ret_amount`, `net_total_amount`, `reason`, `usablity`) VALUES ('925134226593673', '38296235', '', '20190731114458', '2019-07-31', '2019-08-01', '20', '10', '', 'KRQGUNN4ZMS3WT8I2RK9', '30000.00', '0', '0.00', '0.00', '300000.00', '300000.00', '', '2');
INSERT INTO `product_return` (`return_id`, `product_id`, `invoice_id`, `purchase_id`, `date_purchase`, `date_return`, `byy_qty`, `ret_qty`, `customer_id`, `supplier_id`, `product_rate`, `deduction`, `total_deduct`, `total_tax`, `total_ret_amount`, `net_total_amount`, `reason`, `usablity`) VALUES ('863338834262391', '38296235', '5147385423', NULL, '2019-09-19', '2019-09-19', '5', '2', 'BIXQAYJ3XDFBYZN', '', '35000.00', '0', '0.00', '0.00', '70000.00', '70000.00', '', '3');


#
# TABLE STRUCTURE FOR: product_service
#

DROP TABLE IF EXISTS `product_service`;

CREATE TABLE `product_service` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `charge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tax0` text,
  `tax1` text,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: purchase_details
#

DROP TABLE IF EXISTS `purchase_details`;

CREATE TABLE `purchase_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  `batch_id` varchar(30) NOT NULL,
  `expiry` date NOT NULL,
  `qty` float NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: purchase_p
#

DROP TABLE IF EXISTS `purchase_p`;

CREATE TABLE `purchase_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `grandtotal` decimal(12,2) NOT NULL,
  `total_discount` decimal(5,2) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` int(11) NOT NULL,
  `acc_vaoucher` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: referrer_appointment
#

DROP TABLE IF EXISTS `referrer_appointment`;

CREATE TABLE `referrer_appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` varchar(20) DEFAULT NULL,
  `patient_id` varchar(20) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `serial_no` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `problem` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `create_date` date DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `refer_by` int(11) DEFAULT NULL,
  `is_approve` int(11) DEFAULT '0',
  `doctor_comments` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: referrer_information
#

DROP TABLE IF EXISTS `referrer_information`;

CREATE TABLE `referrer_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `speciality` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address1` text COLLATE utf8_unicode_ci NOT NULL,
  `address2` text COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `confirm_password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: role_permission
#

DROP TABLE IF EXISTS `role_permission`;

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_module_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create` tinyint(1) DEFAULT NULL,
  `read` tinyint(1) DEFAULT NULL,
  `update` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_module_id` (`fk_module_id`),
  KEY `fk_user_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1515 DEFAULT CHARSET=utf8;

INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('79', '1', '1', '1', '1', '1', '1');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('80', '2', '1', '1', '1', '1', '1');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('81', '3', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('82', '4', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('83', '5', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('84', '6', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('85', '7', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('86', '8', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('87', '9', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('88', '10', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('89', '11', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('90', '12', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('91', '13', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('92', '14', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('93', '15', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('94', '16', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('95', '17', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('96', '18', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('97', '19', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('98', '20', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('99', '21', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('100', '22', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('101', '23', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('102', '24', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('103', '25', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('104', '26', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('105', '27', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('106', '28', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('107', '29', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('108', '30', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('109', '31', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('110', '32', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('111', '33', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('112', '34', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('113', '35', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('114', '36', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('115', '37', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('116', '38', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('117', '39', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('118', '40', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('119', '41', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('120', '42', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('121', '43', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('122', '44', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('123', '45', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('124', '46', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('125', '47', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('126', '48', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('127', '49', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('128', '50', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('129', '51', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('130', '52', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('131', '53', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('132', '54', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('133', '55', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('134', '56', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('135', '57', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('136', '58', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('137', '59', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('138', '60', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('139', '61', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('140', '62', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('141', '63', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('142', '64', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('143', '65', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('144', '66', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('145', '67', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('146', '68', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('147', '69', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('148', '70', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('149', '71', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('150', '72', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('151', '73', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('152', '74', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('153', '75', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('154', '76', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('155', '77', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('156', '78', '1', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1405', '1', '2', '1', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1406', '2', '2', '0', '1', '1', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1407', '3', '2', '1', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1408', '4', '2', '1', '1', '1', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1409', '5', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1410', '6', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1411', '7', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1412', '8', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1413', '9', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1414', '10', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1415', '11', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1416', '12', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1417', '13', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1418', '14', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1419', '15', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1420', '16', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1421', '17', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1422', '18', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1423', '19', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1424', '20', '2', '1', '1', '1', '1');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1425', '21', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1426', '22', '2', '1', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1427', '23', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1428', '24', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1429', '25', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1430', '26', '2', '1', '1', '1', '1');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1431', '27', '2', '0', '1', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1432', '28', '2', '0', '1', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1433', '109', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1434', '29', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1435', '30', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1436', '31', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1437', '32', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1438', '33', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1439', '34', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1440', '110', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1441', '35', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1442', '36', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1443', '37', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1444', '38', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1445', '39', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1446', '40', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1447', '41', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1448', '42', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1449', '85', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1450', '86', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1451', '105', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1452', '106', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1453', '107', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1454', '43', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1455', '44', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1456', '45', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1457', '46', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1458', '47', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1459', '48', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1460', '49', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1461', '50', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1462', '51', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1463', '52', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1464', '53', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1465', '54', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1466', '55', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1467', '79', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1468', '80', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1469', '81', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1470', '82', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1471', '83', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1472', '84', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1473', '56', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1474', '57', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1475', '58', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1476', '59', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1477', '60', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1478', '61', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1479', '62', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1480', '63', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1481', '64', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1482', '65', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1483', '66', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1484', '67', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1485', '68', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1486', '108', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1487', '69', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1488', '70', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1489', '71', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1490', '72', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1491', '73', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1492', '74', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1493', '75', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1494', '76', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1495', '77', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1496', '78', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1497', '97', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1498', '98', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1499', '99', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1500', '100', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1501', '94', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1502', '95', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1503', '96', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1504', '87', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1505', '88', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1506', '89', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1507', '90', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1508', '91', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1509', '92', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1510', '93', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1511', '101', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1512', '102', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1513', '103', '2', '0', '0', '0', '0');
INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES ('1514', '104', '2', '0', '0', '0', '0');


#
# TABLE STRUCTURE FOR: salary_sheet_generate
#

DROP TABLE IF EXISTS `salary_sheet_generate`;

CREATE TABLE `salary_sheet_generate` (
  `ssg_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET latin1 NOT NULL,
  `gdate` varchar(30) DEFAULT NULL,
  `start_date` varchar(30) CHARACTER SET latin1 NOT NULL,
  `end_date` varchar(30) CHARACTER SET latin1 NOT NULL,
  `generate_by` varchar(30) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`ssg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: salary_type
#

DROP TABLE IF EXISTS `salary_type`;

CREATE TABLE `salary_type` (
  `salary_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `sal_name` varchar(100) NOT NULL,
  `salary_type` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`salary_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: schedule
#

DROP TABLE IF EXISTS `schedule`;

CREATE TABLE `schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `slot_id` int(11) NOT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `available_days` varchar(50) DEFAULT NULL,
  `per_patient_time` time DEFAULT NULL,
  `serial_visibility_type` tinyint(4) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sec_role
#

DROP TABLE IF EXISTS `sec_role`;

CREATE TABLE `sec_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `sec_role` (`id`, `type`) VALUES ('1', 'Test Role');
INSERT INTO `sec_role` (`id`, `type`) VALUES ('2', 'Coder IT');


#
# TABLE STRUCTURE FOR: sec_userrole
#

DROP TABLE IF EXISTS `sec_userrole`;

CREATE TABLE `sec_userrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `createby` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `createdate` datetime NOT NULL,
  UNIQUE KEY `ID` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `sec_userrole` (`id`, `user_id`, `roleid`, `createby`, `createdate`) VALUES ('1', '3vRVO2L8tsO5PP2', '2', '1', '2019-02-23 06:14:22');


#
# TABLE STRUCTURE FOR: service_invoice
#

DROP TABLE IF EXISTS `service_invoice`;

CREATE TABLE `service_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_no` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `employee_id` varchar(50) NOT NULL,
  `customer_id` varchar(30) NOT NULL,
  `total_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `total_discount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `invoice_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `due_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `shipping_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `previous` decimal(10,2) NOT NULL DEFAULT '0.00',
  `details` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: service_invoice_details
#

DROP TABLE IF EXISTS `service_invoice_details`;

CREATE TABLE `service_invoice_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) NOT NULL,
  `service_inv_id` varchar(30) NOT NULL,
  `qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `charge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: setting
#

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `site_align` varchar(50) DEFAULT NULL,
  `footer_text` varchar(255) DEFAULT NULL,
  `time_zone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sms_delivery
#

DROP TABLE IF EXISTS `sms_delivery`;

CREATE TABLE `sms_delivery` (
  `sms_delivery_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_id` int(11) NOT NULL,
  `delivery_date_time` datetime NOT NULL,
  `sms_info_id` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`sms_delivery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sms_gateway
#

DROP TABLE IF EXISTS `sms_gateway`;

CREATE TABLE `sms_gateway` (
  `gateway_id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_name` text NOT NULL,
  `user` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `authentication` text NOT NULL,
  `link` text NOT NULL,
  `default_status` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`gateway_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sms_info
#

DROP TABLE IF EXISTS `sms_info`;

CREATE TABLE `sms_info` (
  `sms_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `patient_id` varchar(30) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `appointment_id` varchar(30) NOT NULL,
  `appointment_date` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `sms_counter` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sms_info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sms_schedule
#

DROP TABLE IF EXISTS `sms_schedule`;

CREATE TABLE `sms_schedule` (
  `ss_id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_teamplate_id` int(11) NOT NULL,
  `ss_name` text NOT NULL,
  `ss_schedule` varchar(100) NOT NULL,
  `ss_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ss_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sms_setting
#

DROP TABLE IF EXISTS `sms_setting`;

CREATE TABLE `sms_setting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `appointment` tinyint(1) DEFAULT NULL,
  `registration` tinyint(1) DEFAULT NULL,
  `schedule` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sms_settings
#

DROP TABLE IF EXISTS `sms_settings`;

CREATE TABLE `sms_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `sender_id` varchar(100) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `isinvoice` int(11) NOT NULL DEFAULT '0',
  `ispurchase` int(11) NOT NULL DEFAULT '0',
  `isservice` int(11) NOT NULL DEFAULT '0',
  `ispayment` int(11) NOT NULL DEFAULT '0',
  `isreceive` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `sms_settings` (`id`, `url`, `sender_id`, `api_key`, `isinvoice`, `ispurchase`, `isservice`, `ispayment`, `isreceive`) VALUES ('1', 'http://sms.bdtask.com/smsapi', '880184716988421', '58C20029865c42c504afc711.77492546', '1', '1', '1', '1', '0');


#
# TABLE STRUCTURE FOR: sms_teamplate
#

DROP TABLE IF EXISTS `sms_teamplate`;

CREATE TABLE `sms_teamplate` (
  `teamplate_id` int(11) NOT NULL AUTO_INCREMENT,
  `teamplate_name` text,
  `teamplate` text,
  `type` varchar(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `default_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`teamplate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sms_users
#

DROP TABLE IF EXISTS `sms_users`;

CREATE TABLE `sms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `receiver` varchar(20) DEFAULT NULL,
  `message` text,
  `date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: sub_module
#

DROP TABLE IF EXISTS `sub_module`;

CREATE TABLE `sub_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `directory` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;

INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('1', '1', 'new_invoice', NULL, NULL, 'new_invoice', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('2', '1', 'manage_invoice', NULL, NULL, 'manage_invoice', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('3', '1', 'pos_invoice', NULL, NULL, 'pos_invoice', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('4', '2', 'c_o_a', NULL, NULL, 'show_tree', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('5', '2', 'supplier_payment', NULL, NULL, 'supplier_payment', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('6', '2', 'customer_receive', NULL, NULL, 'customer_receive', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('7', '2', 'debit_voucher', NULL, NULL, 'debit_voucher', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('8', '2', 'credit_voucher', NULL, NULL, 'credit_voucher', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('9', '2', 'voucher_approval', NULL, NULL, 'aprove_v', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('10', '2', 'contra_voucher', NULL, NULL, 'contra_voucher', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('11', '2', 'journal_voucher', NULL, NULL, 'journal_voucher', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('12', '2', 'report', NULL, NULL, 'ac_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('13', '2', 'cash_book', NULL, NULL, 'cash_book', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('14', '2', 'Inventory_ledger', NULL, NULL, 'inventory_ledger', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('15', '2', 'bank_book', NULL, NULL, 'bank_book', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('16', '2', 'general_ledger', NULL, NULL, 'general_ledger', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('17', '2', 'trial_balance', NULL, NULL, 'trial_balance', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('18', '2', 'cash_flow', NULL, NULL, 'cash_flow_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('19', '2', 'coa_print', NULL, NULL, 'coa_print', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('20', '3', 'add_category', NULL, NULL, 'create_category', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('21', '3', 'manage_category', NULL, NULL, 'manage_category', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('22', '4', 'add_product', NULL, NULL, 'create_product', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('23', '4', 'import_product_csv', NULL, NULL, 'add_product_csv', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('24', '4', 'manage_product', NULL, NULL, 'manage_product', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('25', '5', 'add_customer', NULL, NULL, 'add_customer', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('26', '5', 'manage_customer', NULL, NULL, 'manage_customer', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('27', '5', 'credit_customer', NULL, NULL, 'credit_customer', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('28', '5', 'paid_customer', NULL, NULL, 'paid_customer', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('29', '6', 'add_unit', NULL, NULL, 'add_unit', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('30', '6', 'manage_unit', NULL, NULL, 'manage_unit', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('31', '7', 'add_supplier', NULL, NULL, 'add_supplier', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('32', '7', 'manage_supplier', NULL, NULL, 'manage_supplier', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('33', '7', 'supplier_ledger', NULL, NULL, 'supplier_ledger_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('35', '8', 'add_purchase', NULL, NULL, 'add_purchase', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('36', '8', 'manage_purchase', NULL, NULL, 'manage_purchase', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('37', '9', 'return', NULL, NULL, 'add_return', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('38', '9', 'stock_return_list', NULL, NULL, 'return_list', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('39', '9', 'supplier_return_list', NULL, NULL, 'supplier_return_list', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('40', '9', 'wastage_return_list', NULL, NULL, 'wastage_return_list', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('41', '10', 'add_tax', NULL, NULL, 'add_tax', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('42', '10', 'manage_tax', NULL, NULL, 'manage_tax', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('43', '11', 'stock_report', NULL, NULL, 'stock_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('44', '11', 'stock_report_supplier_wise', NULL, NULL, 'stock_report_sp_wise', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('45', '11', 'stock_report_product_wise', NULL, NULL, 'stock_report_pro_wise', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('46', '12', 'closing', NULL, NULL, 'add_closing', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('47', '12', 'closing_report', NULL, NULL, 'closing_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('48', '12', 'todays_report', NULL, NULL, 'all_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('49', '12', 'todays_customer_receipt', NULL, NULL, 'todays_customer_receipt', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('50', '12', 'sales_report', NULL, NULL, 'todays_sales_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('51', '12', 'due_report', NULL, NULL, 'retrieve_dateWise_DueReports', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('52', '12', 'purchase_report', NULL, NULL, 'todays_purchase_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('53', '12', 'purchase_report_category_wise', NULL, NULL, 'purchase_report_category_wise', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('54', '12', 'sales_report_product_wise', NULL, NULL, 'product_sales_reports_date_wise', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('55', '12', 'sales_report_category_wise', NULL, NULL, 'sales_report_category_wise', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('56', '13', 'add_new_bank', NULL, NULL, 'add_bank', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('57', '13', 'bank_transaction', NULL, NULL, 'bank_transaction', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('58', '13', 'manage_bank', NULL, NULL, 'bank_list', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('59', '14', 'generate_commission', NULL, NULL, 'commission', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('60', '15', 'add_person', NULL, NULL, 'add1_person', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('61', '15', 'add_loan', NULL, NULL, 'add_office_loan', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('62', '15', 'add_payment', NULL, NULL, 'add_loan_payment', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('63', '15', 'manage_loan', NULL, NULL, 'manage1_person', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('64', '16', 'add_person', NULL, NULL, 'add_person', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('65', '16', 'add_loan', NULL, NULL, 'add_loan', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('66', '16', 'add_payment', NULL, NULL, 'add_payment', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('67', '16', 'manage_loan', NULL, NULL, 'manage_person', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('68', '18', 'backup_restore', NULL, NULL, 'back_up', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('69', '19', 'manage_company', NULL, NULL, 'manage_company', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('70', '19', 'add_user', NULL, NULL, 'add_user', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('71', '19', 'manage_users', NULL, NULL, 'manage_user', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('72', '19', 'language', NULL, NULL, 'add_language', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('73', '19', 'currency', NULL, NULL, 'add_currency', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('74', '19', 'setting', NULL, NULL, 'soft_setting', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('75', '20', 'add_role', NULL, NULL, 'add_role', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('76', '20', 'role_list', NULL, NULL, 'role_list', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('77', '20', 'user_assign_role', NULL, NULL, 'user_assign', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('78', '20', 'Permission', NULL, NULL, NULL, '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('79', '12', 'shipping_cost_report', NULL, NULL, 'shipping_cost_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('80', '12', 'user_wise_sales_report', NULL, NULL, 'user_wise_sales_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('81', '12', 'invoice_return', NULL, NULL, 'invoice_return', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('82', '12', 'supplier_return', NULL, NULL, 'supplier_return', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('83', '12', 'tax_report', NULL, NULL, 'tax_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('84', '12', 'profit_report', NULL, NULL, 'profit_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('85', '10', 'add_incometax', NULL, NULL, 'add_incometax', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('86', '10', 'manage_income_tax', NULL, NULL, 'manage_income_tax', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('87', '23', 'add_benefits', NULL, NULL, 'add_benefits', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('88', '23', 'manage_benefits', NULL, NULL, 'manage_benefits', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('89', '23', 'add_salary_setup', NULL, NULL, 'add_salary_setup', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('90', '23', 'manage_salary_setup', NULL, NULL, 'manage_salary_setup', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('91', '23', 'salary_generate', NULL, NULL, 'salary_generate', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('92', '23', 'manage_salary_generate', NULL, NULL, 'manage_salary_generate', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('93', '23', 'salary_payment', NULL, NULL, 'salary_payment', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('94', '22', 'add_attendance', NULL, NULL, 'add_attendance', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('95', '22', 'manage_attendance', NULL, NULL, 'manage_attendance', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('96', '22', 'attendance_report', NULL, NULL, 'attendance_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('97', '21', 'add_designation', NULL, NULL, 'add_designation', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('98', '21', 'manage_designation', NULL, NULL, 'manage_designation', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('99', '21', 'add_employee', NULL, NULL, 'add_employee', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('100', '21', 'manage_employee', NULL, NULL, 'manage_employee', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('101', '24', 'create_service', NULL, NULL, 'create_service', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('102', '24', 'manage_service', NULL, NULL, 'manage_service', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('103', '24', 'service_invoice', NULL, NULL, 'service_invoice', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('104', '24', 'manage_service_invoice', NULL, NULL, 'manage_service_invoice', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('105', '10', 'tax_report', NULL, NULL, 'tax_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('106', '10', 'invoice_wise_tax_report', NULL, NULL, 'invoice_wise_tax_report', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('107', '10', 'tax_settings', NULL, NULL, 'tax_settings', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('108', '18', 'restore', NULL, NULL, 'restore', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('109', '5', 'customer_advance', NULL, NULL, 'customer_advance', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('110', '7', 'supplier_advance', NULL, NULL, 'supplier_advance', '1');
INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES ('111', '5', 'customer_ledger', NULL, NULL, 'customer_ledger', '1');


#
# TABLE STRUCTURE FOR: supplier_information
#

DROP TABLE IF EXISTS `supplier_information`;

CREATE TABLE `supplier_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` varchar(100) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `details` varchar(255) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

INSERT INTO `supplier_information` (`id`, `supplier_id`, `supplier_name`, `address`, `mobile`, `details`, `status`) VALUES ('21', '2T6QHG45AP4M11FG6TPT', 'shahabuddin', '082545669', 'cfgg', '', '1');
INSERT INTO `supplier_information` (`id`, `supplier_id`, `supplier_name`, `address`, `mobile`, `details`, `status`) VALUES ('20', '6167UCENYO5JJITKRH21', 'hhhkkk', 'fsdf', '2343', '', '1');
INSERT INTO `supplier_information` (`id`, `supplier_id`, `supplier_name`, `address`, `mobile`, `details`, `status`) VALUES ('22', 'XUQB7JFFJSFXKGOZ9VD3', 'isahaq', '01852376598', 'khilkhet', '', '1');
INSERT INTO `supplier_information` (`id`, `supplier_id`, `supplier_name`, `address`, `mobile`, `details`, `status`) VALUES ('23', 'CMBQPCTZMZ3ALHEYT8BG', 'Ovi', '017744125', 'Dhaka', 'Android', '1');


#
# TABLE STRUCTURE FOR: supplier_ledger
#

DROP TABLE IF EXISTS `supplier_ledger`;

CREATE TABLE `supplier_ledger` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) NOT NULL,
  `supplier_id` varchar(100) NOT NULL,
  `chalan_no` varchar(100) DEFAULT NULL,
  `deposit_no` varchar(50) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `description` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `cheque_no` varchar(255) NOT NULL,
  `date` varchar(50) NOT NULL,
  `status` int(2) NOT NULL,
  `d_c` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('1', 'PCMBPBE6PY', 'WPXXU7UC1Z4ID1HUVU53', 'Adjustment ', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-25', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('2', '20190625090838', 'WPXXU7UC1Z4ID1HUVU53', '234234', NULL, '7500.00', 'sdfasd', '', '', '2019-06-25', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('3', '20190625090838', 'WPXXU7UC1Z4ID1HUVU53', '234234', NULL, '7500.00', 'Purchase From Supplier. sdfasd', '', '', '2019-06-25', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('4', '20190625132953', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '4000.00', '', '', '', '2019-06-25', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('5', '20190625132953', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '4000.00', 'Purchase From Supplier. ', '', '', '2019-06-25', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('6', 'PM-1', 'WPXXU7UC1Z4ID1HUVU53', NULL, 'PM-1', '250.00', 'Paid From Accounts', '1', '', '2019-06-25', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('7', '20190626100758', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '3500.00', '', '', '', '2019-06-26', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('8', '20190626100758', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '3500.00', 'Purchase From Supplier. ', '', '', '2019-06-26', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('9', '20190626101452', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '3900.00', '', '', '', '2019-06-26', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('10', '20190626101452', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '3900.00', 'Purchase From Supplier. ', '', '', '2019-06-26', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('11', '20190628054650', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '400000.00', '', '', '', '2019-06-28', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('12', 'V2WGALHBZ5', 'WPXXU7UC1Z4ID1HUVU53', NULL, NULL, '2500.00', 'Advance ', 'NA', 'NA', '2019-06-28', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('13', 'MPXB7A4FB2', 'KRQGUNN4ZMS3WT8I2RK9', 'Adjustment ', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-28', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('14', '20190628055844', 'KRQGUNN4ZMS3WT8I2RK9', '', NULL, '150000.00', '', '', '', '2019-06-28', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('15', '20190628055844', 'KRQGUNN4ZMS3WT8I2RK9', '', NULL, '150000.00', 'Purchase From Supplier. ', '', '', '2019-06-28', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('16', '20190628062109', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '1900.00', '', '', '', '2019-06-28', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('17', '20190628062109', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '1900.00', 'Purchase From Supplier. ', '', '', '2019-06-28', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('18', '20190629115435', 'WPXXU7UC1Z4ID1HUVU53', '16876936', NULL, '20000.00', '', '', '', '2019-06-29', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('19', '20190629115435', 'WPXXU7UC1Z4ID1HUVU53', '16876936', NULL, '20000.00', 'Purchase From Supplier. ', '', '', '2019-06-29', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('20', '20190731114458', 'KRQGUNN4ZMS3WT8I2RK9', 'sdfs', NULL, '0.00', '', '', '', '2019-07-31', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('21', '20190731114458', 'KRQGUNN4ZMS3WT8I2RK9', 'sdfs', NULL, '0.00', 'Purchase From Supplier. ', '', '', '2019-07-31', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('22', '20190731114746', 'WPXXU7UC1Z4ID1HUVU53', '234234', NULL, '2039100.00', '', '', '', '2019-07-31', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('23', '20190731114746', 'WPXXU7UC1Z4ID1HUVU53', '234234', NULL, '2039100.00', 'Purchase From Supplier. ', '', '', '2019-07-31', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('24', '20190731114746', 'WPXXU7UC1Z4ID1HUVU53', 'RRJBPNSH59', NULL, '-40500.00', 'Return', '', '', '2019-08-01', '1', '');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('25', '20190731114458', 'KRQGUNN4ZMS3WT8I2RK9', 'VCSJF5N5KO', NULL, '-300000.00', 'Return', '', '', '2019-08-01', '1', '');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('26', 'PM-2', 'WPXXU7UC1Z4ID1HUVU53', NULL, 'PM-2', '200.00', 'Paid to Test Supplier', '1', '', '2019-08-01', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('27', 'WCAX81XD3R', 'WPXXU7UC1Z4ID1HUVU53', NULL, NULL, '1000.00', 'Advance ', 'NA', 'NA', '2019-08-03', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('28', 'LRRQ4L5FO1', 'E4ZLUXYXX6GJU3HT2DQ8', 'Adjustment ', NULL, '10.00', 'Previous adjustment with software', 'NA', 'NA', '2019-08-03', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('29', 'Z9IYRG1MRI', '1H6YPZJIJIB7YW7TDM7Y', 'Adjustment ', NULL, '1.00', 'Previous adjustment with software', 'NA', 'NA', '2019-08-03', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('37', '5OMZ3G1GUN', 'QUT7N3K6WOTU6BXRD8BL', NULL, NULL, '232.00', 'Advance ', 'NA', 'NA', '2019-08-06', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('38', 'L26HEBCZJM', 'QUT7N3K6WOTU6BXRD8BL', NULL, NULL, '232.00', 'Advance ', 'NA', 'NA', '2019-08-06', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('36', 'IF2K2MG62S', 'QUT7N3K6WOTU6BXRD8BL', NULL, NULL, '232.00', 'Advance ', 'NA', 'NA', '2019-08-06', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('126', 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-08-05', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('133', 'PM-7', '', NULL, 'PM-7', '0.00', 'Paid From Accounts', '1', '', '2019-09-15', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('132', 'PM-5', '2T6QHG45AP4M11FG6TPT', NULL, 'PM-5', '324234.00', 'Paid to shahabuddin', '1', '', '2019-09-05', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('124', '36VPJ77O5F', '6167UCENYO5JJITKRH21', 'Adjustment ', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-09-05', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('125', 'PM-3', 'X', NULL, 'PM-3', '1.00', 'Paid From Accounts', '1', '', '2019-08-05', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('127', 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-09-05', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('128', 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-09-05', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('129', 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-09-05', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('130', 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-09-05', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('131', 'PM-4', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-4', '200.00', 'Paid to isahaq', '1', '', '2019-09-05', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('134', '20190924143851', 'XUQB7JFFJSFXKGOZ9VD3', '123123', NULL, '4500.00', 'dfsdf', '', '', '2019-09-24', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('135', '20190924143851', 'XUQB7JFFJSFXKGOZ9VD3', '123123', NULL, '4500.00', 'Purchase From Supplier. dfsdf', '', '', '2019-09-24', '1', 'd');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('136', '20190925103121', 'XUQB7JFFJSFXKGOZ9VD3', '1233', NULL, '3300.00', 'fdgsdf', '', '', '2019-09-25', '1', 'c');
INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES ('137', '20190925103121', 'XUQB7JFFJSFXKGOZ9VD3', '1233', NULL, '3300.00', 'Purchase From Supplier. fdgsdf', '', '', '2019-09-25', '1', 'd');


#
# TABLE STRUCTURE FOR: supplier_product
#

DROP TABLE IF EXISTS `supplier_product`;

CREATE TABLE `supplier_product` (
  `supplier_pr_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `products_model` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `supplier_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `supplier_price` float NOT NULL,
  PRIMARY KEY (`supplier_pr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `supplier_product` (`supplier_pr_id`, `product_id`, `products_model`, `supplier_id`, `supplier_price`) VALUES ('1', '42156427', 'Test Model', 'XUQB7JFFJSFXKGOZ9VD3', '90');
INSERT INTO `supplier_product` (`supplier_pr_id`, `product_id`, `products_model`, `supplier_id`, `supplier_price`) VALUES ('2', '33845288', 'sdfsa', 'XUQB7JFFJSFXKGOZ9VD3', '110');


#
# TABLE STRUCTURE FOR: synchronizer_setting
#

DROP TABLE IF EXISTS `synchronizer_setting`;

CREATE TABLE `synchronizer_setting` (
  `id` int(11) NOT NULL,
  `hostname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `port` varchar(10) NOT NULL,
  `debug` varchar(10) NOT NULL,
  `project_root` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: tax_collection
#

DROP TABLE IF EXISTS `tax_collection`;

CREATE TABLE `tax_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `customer_id` varchar(30) NOT NULL,
  `relation_id` varchar(30) NOT NULL,
  `tax0` text,
  `tax1` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('1', '2019-06-25', 'XWCEM9UVV5NAT5R', '3498244447', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('2', '2019-06-25', '6FGVC6K7OX5L9Z1', '4477997231', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('3', '2019-06-25', '6FGVC6K7OX5L9Z1', '3973611118', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('4', '2019-06-25', '6FGVC6K7OX5L9Z1', '3923586588', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('5', '2019-06-26', 'XWCEM9UVV5NAT5R', '6575276926', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('6', '2019-06-26', '6FGVC6K7OX5L9Z1', '6643494915', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('7', '2019-06-26', '6FGVC6K7OX5L9Z1', '1969912464', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('8', '2019-06-26', '6FGVC6K7OX5L9Z1', '6691959977', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('9', '2019-06-26', '6FGVC6K7OX5L9Z1', '2258111339', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('10', '2019-06-26', '6FGVC6K7OX5L9Z1', '9624435359', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('11', '2019-06-26', '6FGVC6K7OX5L9Z1', '8668653724', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('12', '2019-06-26', '6FGVC6K7OX5L9Z1', '8558184696', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('13', '2019-06-26', '6FGVC6K7OX5L9Z1', '9147771963', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('14', '2019-06-26', 'FYJ4EEBMOGTW25T', '3169824785', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('15', '2019-06-26', '6FGVC6K7OX5L9Z1', '3918198168', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('16', '2019-06-26', 'PVGGNNSWOESP45P', '5219125479', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('17', '2019-06-26', 'PVGGNNSWOESP45P', '8394724961', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('18', '2019-06-27', '6FGVC6K7OX5L9Z1', '7986446844', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('19', '2019-06-27', '6FGVC6K7OX5L9Z1', '6789217583', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('20', '2019-06-27', '6BPL9FJR54PBZJE', '4978495577', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('21', '2019-06-28', 'KFTUKN7RY1O2W6R', '2687213589', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('22', '2019-06-28', '6FGVC6K7OX5L9Z1', '3378453241', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('23', '2019-06-28', '6FGVC6K7OX5L9Z1', '4458233386', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('24', '2019-06-28', '6FGVC6K7OX5L9Z1', '1114135497', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('25', '2019-06-28', '6FGVC6K7OX5L9Z1', '4441511736', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('26', '2019-06-28', '6FGVC6K7OX5L9Z1', '8754679919', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('27', '2019-06-28', '6FGVC6K7OX5L9Z1', '3378767423', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('28', '2019-06-29', '6FGVC6K7OX5L9Z1', '9317933378', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('29', '2019-06-29', '6FGVC6K7OX5L9Z1', '1797561487', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('30', '2019-06-30', '6FGVC6K7OX5L9Z1', '9438755655', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('31', '2019-06-30', '6FGVC6K7OX5L9Z1', '5919433886', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('32', '2019-07-01', '6FGVC6K7OX5L9Z1', '2875718763', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('33', '2019-07-02', '6FGVC6K7OX5L9Z1', '1749144575', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('34', '2019-07-02', '6FGVC6K7OX5L9Z1', '9815582964', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('35', '2019-07-02', '6FGVC6K7OX5L9Z1', '8493678614', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('36', '2019-07-31', '6FGVC6K7OX5L9Z1', '9691144514', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('37', '2019-07-31', '6FGVC6K7OX5L9Z1', '5457199982', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('38', '2019-07-31', '6FGVC6K7OX5L9Z1', '2362693843', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('40', '2019-07-31', '6FGVC6K7OX5L9Z1', '5763389422', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('41', '2019-08-01', '6FGVC6K7OX5L9Z1', '6921223925', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('42', '2019-08-01', '6FGVC6K7OX5L9Z1', '2992478698', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('43', '2019-08-01', '6FGVC6K7OX5L9Z1', '2429117215', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('44', '2019-08-01', '6FGVC6K7OX5L9Z1', '8636821614', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('45', '2019-08-01', '6FGVC6K7OX5L9Z1', '2274125738', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('46', '2019-08-01', '6FGVC6K7OX5L9Z1', '6881727828', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('47', '2019-08-03', '6FGVC6K7OX5L9Z1', '6942233633', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('48', '2019-08-03', '6FGVC6K7OX5L9Z1', '7167684116', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('49', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'MBJEQBLEDT', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('50', '2019-08-27', 'BIXQAYJ3XDFBYZN', '3OXFJU9FLE', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('51', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'AEYS0XT6FB', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('52', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'IFU2EJUMSR', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('53', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'TD6SQ1IDPR', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('54', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'B8P4EGGRAY', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('55', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'R8OMFVYJXQ', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('56', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'VYMH47RI7Y', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('57', '2019-08-27', 'BIXQAYJ3XDFBYZN', '3FQSMSSQDT', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('58', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'JTXNMIW0DT', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('59', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'TJSXGFD1F6', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('60', '2019-08-27', 'BIXQAYJ3XDFBYZN', '1AGGWW2DR6', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('61', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'FO9Q0CZBAH', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('62', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'CYN1QFPGZE', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('63', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'QE2YQFXLDA', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('64', '2019-08-27', 'BIXQAYJ3XDFBYZN', 'JJ75B2XUKG', NULL, NULL);
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('65', '2019-09-19', 'BIXQAYJ3XDFBYZN', '5147385423', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('66', '2019-09-21', '6FGVC6K7OX5L9Z1', '5928235575', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('67', '2019-09-21', '6FGVC6K7OX5L9Z1', '6852499599', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('68', '2019-09-21', 'BIXQAYJ3XDFBYZN', '7348638525', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('69', '2019-09-21', '6FGVC6K7OX5L9Z1', '6979694511', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('70', '2019-09-21', '6FGVC6K7OX5L9Z1', '7675268361', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('71', '2019-09-21', '6FGVC6K7OX5L9Z1', '1699845521', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('72', '2019-09-21', '6FGVC6K7OX5L9Z1', '3291158273', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('73', '2019-09-21', '6FGVC6K7OX5L9Z1', '7884756971', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('74', '2019-09-21', '6FGVC6K7OX5L9Z1', '9836928819', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('75', '2019-09-21', '6FGVC6K7OX5L9Z1', '2466279899', '0.00', '0.00');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('76', '2019-09-24', '6FGVC6K7OX5L9Z1', '7126396398', '5.00', '9.90');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('77', '2019-09-26', '6FGVC6K7OX5L9Z1', '3496522264', '1.00', '1.98');
INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES ('78', '2019-09-26', '6FGVC6K7OX5L9Z1', '1962163959', '1.00', '1.98');


#
# TABLE STRUCTURE FOR: tax_information
#

DROP TABLE IF EXISTS `tax_information`;

CREATE TABLE `tax_information` (
  `tax_id` varchar(15) NOT NULL,
  `tax` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: tax_settings
#

DROP TABLE IF EXISTS `tax_settings`;

CREATE TABLE `tax_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_value` float NOT NULL,
  `tax_name` varchar(250) NOT NULL,
  `nt` int(11) NOT NULL,
  `reg_no` varchar(100) NOT NULL,
  `is_show` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `tax_settings` (`id`, `default_value`, `tax_name`, `nt`, `reg_no`, `is_show`) VALUES ('1', '1', 'Vat', '2', '745878', '1');
INSERT INTO `tax_settings` (`id`, `default_value`, `tax_name`, `nt`, `reg_no`, `is_show`) VALUES ('2', '2', 'Tax', '2', '879789', '1');


#
# TABLE STRUCTURE FOR: time_slot
#

DROP TABLE IF EXISTS `time_slot`;

CREATE TABLE `time_slot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slot_name` varchar(40) CHARACTER SET utf8 NOT NULL,
  `slot` varchar(15) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: transection
#

DROP TABLE IF EXISTS `transection`;

CREATE TABLE `transection` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(30) NOT NULL,
  `date_of_transection` varchar(30) NOT NULL,
  `transection_type` varchar(30) NOT NULL,
  `transection_category` varchar(30) NOT NULL,
  `transection_mood` varchar(25) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pay_amount` decimal(10,2) DEFAULT '0.00',
  `description` varchar(255) NOT NULL,
  `relation_id` varchar(30) NOT NULL,
  `is_transaction` int(2) NOT NULL COMMENT '0 = invoice and 1 = transaction',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;

INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('1', 'FS93V8F75ICGLDK', '2019-06-25', '2', '2', '1', '200.00', '0.00', 'Paid by customer', 'XWCEM9UVV5NAT5R', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('2', 'Q3IZ9DRCOSZ768G', '2019-06-25', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('3', 'QO6WH5CIWCPJ6Q1', '2019-06-25', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('4', 'S67BGX3BCKB5VDV', '2019-06-25', '2', '2', '1', '600.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('5', 'F1QPDTOHWZ16P9P', '2019-06-26', '2', '2', '1', '200.00', '0.00', 'Paid by customer', 'XWCEM9UVV5NAT5R', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('6', 'YYMQYK93V1ZM516', '2019-06-26', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('7', 'FE7CRWNMHEY38MD', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('8', 'AMUTK48CY2VYX8L', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('9', 'HVFX6UG8VG2NR6K', '2019-06-26', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('10', 'KWTMIGM4DFV4MMN', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('11', 'O6D1W158OH1A4SO', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('12', 'ZZGD9XYMVFUFHET', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('13', 'XPE32XHWMR1IIOD', '2019-06-26', '2', '2', '1', '450.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('14', 'Y2GQTYTL9QPDS3S', '2019-06-26', '2', '2', '1', '800.00', '0.00', 'Paid by customer', 'FYJ4EEBMOGTW25T', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('15', 'DIAUX4GUIHBKXM2', '2019-06-26', '2', '2', '1', '1450.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('16', '5ES9981RI25GKC4', '2019-06-26', '2', '2', '1', '450.00', '0.00', 'Paid by customer', 'PVGGNNSWOESP45P', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('17', 'DI85CJ5RMXQD7HB', '2019-06-26', '2', '2', '1', '1450.00', '0.00', 'Paid by customer', 'PVGGNNSWOESP45P', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('18', 'GUYXF9TXVLAJJEQ', '2019-06-27', '2', '2', '1', '800.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('19', '9P35ZCUMLIEJFWO', '2019-06-27', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('20', 'RDMKLIJFJCJSXPT', '2019-06-27', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6BPL9FJR54PBZJE', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('21', 'H2INB5JLI83B2JZ', '2019-06-28', '2', '2', '1', '30000.00', '0.00', 'Paid by customer', 'KFTUKN7RY1O2W6R', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('22', 'MSAGGFLPOJDKQII', '2019-06-28', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('23', 'STD9RZ3H1JBXP5R', '2019-06-28', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('24', 'UNSJVY7KRG63XFU', '2019-06-28', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('25', '2FA13JON5GQ6T9K', '2019-06-28', '2', '2', '1', '2210.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('26', 'VP9EK1NHG955C8E', '2019-06-28', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('27', 'EFZMHVZOZIHL9X9', '2019-06-28', '2', '2', '1', '50000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('28', 'FUFHSCJATY2LHY4', '2019-06-29', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('29', 'OFTJDX79S6JKXT5', '2019-06-29', '2', '2', '1', '36000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('30', 'YKFQ14NNAGXFATU', '2019-06-30', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('31', '126OOJOQE2XEQ26', '2019-06-30', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('32', 'B1CY534MOESSW5C', '2019-07-01', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('33', '2V7IRO1C13I2MDW', '2019-07-02', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('34', 'GFVMOPDBU2EB87U', '2019-07-02', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('35', 'SBW99OEQ3LR73IG', '2019-07-02', '2', '2', '1', '455.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('36', 'PZBG4F2CP4ZVSP8', '2019-07-31', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('37', '8SA4NBY3SH62KMW', '2019-07-31', '2', '2', '1', '195465.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('38', 'E64ODCUPGHL57QG', '2019-07-31', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('40', 'RZVZUC2CK73MFHK', '2019-07-31', '2', '2', '1', '455.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('41', 'UH22TNP1RNBWZMU', '2019-08-01', '2', '2', '1', '50455.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('42', '2FPNZL5FLJRFEY4', '2019-08-01', '2', '2', '1', '50655.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('43', 'YP2TDHXDFT7PQ56', '2019-08-01', '2', '2', '1', '51855.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('44', 'YCJUPGV3B5MBKQX', '2019-08-01', '2', '2', '1', '85000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('45', 'DO95YM46XX8JQRL', '2019-08-01', '2', '2', '1', '50000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('46', 'Z1PT7RD61Z4MZ3I', '2019-08-01', '2', '2', '1', '455.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('47', '37P7PM8L72D6ORF', '2019-08-03', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('48', '4D6FVY1AIMSJDKW', '2019-08-03', '2', '2', '1', '2600.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('49', '9SQL4HDK78GTX79', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('50', 'P14K3GNFMSCCLP3', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('51', 'OE3P77I88Q91OA4', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('52', 'S4TZR8MS4HYJO1C', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('53', 'KMW431HG76855QH', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('54', 'FQHWRZQDA26OVKK', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('55', '91CDZX4GY87FT9R', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('56', 'ZVMINW2RAKCCJ9K', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('57', 'FMSLB6U1LFMD8R8', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('58', '35EZAQVD9ORALB2', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('59', 'NGQ3TQ5IZN6X2EB', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('60', 'HVJJM7B256Y5T6B', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('61', 'MFLDBMSJWC3QXK2', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('62', '455YPJW2E7OEVEE', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('63', 'J3OSS89CCGIWBQQ', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('64', 'NC7539IIO4UVMEN', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('65', '3YHJ8EYDFZN7DCA', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('66', '7U8E91JYYA4GBEF', '2019/08/31', '2', '2', '1', '580.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('67', 'CBERCINUJUMHR83', '2019/08/31', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('68', 'RGBVSR1L1MX9OOB', '2019/08/31', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('69', '66YVTE7U13A8XTD', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('70', 'BCXJJU1D6UFEBPI', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('71', '4JWNCEWD6KRW1NF', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('72', '6BDDZXVNULKHG2G', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('73', 'EZJXGXR1FFX4J6Q', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('74', 'QNFG1QXHSO9E2Z1', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('75', 'MSFSBLBPLIECNHQ', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('76', 'Z5CVY71P199CZWX', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('77', 'DNDVCQQ3D5OWX9Q', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('78', '28YZ1ODJ98KMS3H', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('79', 'NVGRJWM9836VOWN', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('80', '78NB86JBG8CNGTY', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('81', 'C1QEH9LPOKFX2FK', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('82', 'SQOV2OFLA7PXTV8', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('83', 'U9S9NAFRL95MGRY', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('84', 'P3QEHEBLYFL8L71', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('85', '762KZOSIU1LAXZ8', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('86', '939OSHPFYWOOOYU', '2019-08-31', '2', '2', '1', '150.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('87', 'QSQ7K44IRXQQ2T1', '2019-08-31', '2', '2', '1', '150.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('88', 'KMNV6LBX9S255A1', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('89', '4B6ABYSTPNG8TXC', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('90', 'A855CI6XUZJQAKE', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('91', '43FXE59FSEPPIWW', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('92', 'NMMF9U8AKQD1QJE', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('93', 'CXYRSY7DS9WZN72', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('94', 'O6D1QLW2P1FMOW6', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('95', 'LUMEF2QHGTNEIO7', '2019-08-31', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('96', 'ZI5UU3G5W8IE7GQ', '2019-08-31', '2', '2', '1', '150.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('97', '6FJ8HHODBNTISOQ', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('98', 'VOXTI35ACPWGQBP', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('99', 'HH6GBLCIJJ9ZQM2', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('100', 'DR9WUTCVR8TM88W', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('101', 'TB4TZKKD9Z1DU7N', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('102', 'EWE127EPH5N5QZY', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('103', 'GZ9KC2KCUIGHT5Z', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('104', '1NBHNBURBZX9547', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('105', 'X6IKQC8LDQG7VL7', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('106', 'WZF1YKA1WQ2S3SU', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('107', 'ETDT9FUGDVM8O14', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('108', 'K5MFK1FTDELQEFC', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('109', '1R8DOQDR74PEWMF', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('112', 'SI7T2A7UM5A5ZZW', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('146', 'UG4ONQNHFXFZJH9', '2019-09-19', '2', '2', '1', '400.00', '0.00', 'Paid by customer', '93K7WTVUD86LBTC', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('147', '4FC3T1CMFY65B2T', '2019-09-19', '2', '2', '1', '355.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('148', '3BWOZ61PSP6P3R6', '2019-09-19', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('149', '6F3CVEXN8F6L7OJ', '2019-09-19', '2', '2', '1', '580.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('150', '6XMHQKVX3LDM22E', '2019-09-19', '2', '2', '1', '175900.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('151', 'F8ZQIVM5WVILDF2', '2019-09-21', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('152', 'SEY7BE7IMACFKIZ', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('153', 'YQK988EWI6LVZL5', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('154', 'CZXTX9S9BARD9J4', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('155', 'U6DCWDWEU75XH7P', '2019-09-21', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('156', 'R915QIBP9J2SPZF', '2019-09-21', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('157', 'VHWMMJ6IPKFR2NP', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('158', 'SHQV7VGBJM88D52', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('159', 'SC6U7J6V3H4S8SC', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('160', '2L1ZNYLEB91PUP5', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('161', 'CVWYE5KJL3D22O4', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('162', 'AGMXPFZLB5B3EO7', '2019-09-24', '2', '2', '1', '514.90', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('163', 'TIPZHE21WSRO5NF', '2019-09-26', '2', '2', '1', '102.98', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');
INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES ('164', 'C8RMSIZMIGUMKGZ', '2019-09-26', '2', '2', '1', '102.98', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', '0');


#
# TABLE STRUCTURE FOR: units
#

DROP TABLE IF EXISTS `units`;

CREATE TABLE `units` (
  `unit_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `unit_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `units` (`unit_id`, `unit_name`, `status`) VALUES ('XD2A5GLQWBMK8S3', 'gfdgdfg', '1');
INSERT INTO `units` (`unit_id`, `unit_name`, `status`) VALUES ('NRH49M29VCLDU1W', 'demos', '1');


#
# TABLE STRUCTURE FOR: user
#

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `user_role` tinyint(1) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `blood_group` varchar(10) DEFAULT NULL,
  `vacation` varchar(40) NOT NULL,
  `facebook` varchar(150) NOT NULL,
  `twitter` varchar(150) NOT NULL,
  `youtube` varchar(150) NOT NULL,
  `dribbble` varchar(150) NOT NULL,
  `behance` varchar(150) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `branch_id` int(11) DEFAULT '0',
  `rf_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: user_lang
#

DROP TABLE IF EXISTS `user_lang`;

CREATE TABLE `user_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `language` varchar(15) NOT NULL,
  `designation` varchar(120) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `mobile` varchar(25) NOT NULL,
  `career_title` varchar(200) NOT NULL,
  `short_biography` text NOT NULL,
  `specialist` varchar(200) NOT NULL,
  `degree` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: user_language
#

DROP TABLE IF EXISTS `user_language`;

CREATE TABLE `user_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `type` varchar(10) CHARACTER SET utf8 NOT NULL,
  `rating` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: user_log
#

DROP TABLE IF EXISTS `user_log`;

CREATE TABLE `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time NOT NULL,
  `date` date NOT NULL,
  `branch_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1=active and 0=inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: user_login
#

DROP TABLE IF EXISTS `user_login`;

CREATE TABLE `user_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(15) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` int(2) NOT NULL,
  `security_code` varchar(255) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('1', '1', 'admin@example.com', '41d99b369894eb1ec3f461135132d8bb', '1', '001', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('2', 'KAvUifzuUOhoUr4', 'hmisahaq01@gmail.com', '41d99b369894eb1ec3f461135132d8bb', '2', '190207062137', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('3', '3vRVO2L8tsO5PP2', 'sabuj@gmail.com', '5ebe9dd4ea7517bd2c30bc46985ef823', '2', '', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('4', 'bs1wW4rklPlBwOA', 'super@super.com', '41d99b369894eb1ec3f461135132d8bb', '2', '', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('5', 'wwjwSIbEuQ4S6sD', 'tanzil4091@gmail.com', '41d99b369894eb1ec3f461135132d8bb', '1', '', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('6', 'QfAvRrLHragkSYs', 'ovi@gmail.com', '41d99b369894eb1ec3f461135132d8bb', '1', '', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('7', 'eJGHZzMlqcM3d4x', 'ovi1@gmail.com', '41d99b369894eb1ec3f461135132d8bb', '1', '', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('8', 'hM90DNPq3VQsk9T', '', 'bbe587948a2490934834340b1b4c1643', '1', '', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('9', 'dYmgfQW7j5H8oc9', 'kobirdo@gmail.com', '5ebe9dd4ea7517bd2c30bc46985ef823', '1', '', '1');
INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES ('10', 'LaoMKlz7SCRxhjD', 'hmisahaq@yahoo.com', '035e7767f3f2940b3a6b2817cbdbe9e6', '1', '001', '1');


#
# TABLE STRUCTURE FOR: users
#

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(15) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `gender` int(2) NOT NULL,
  `date_of_birth` varchar(255) NOT NULL,
  `logo` varchar(250) DEFAULT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES ('1', '1', 'User', 'Admin', '0', '', 'http://softest8.bdtask.com/Updatesaleserp/assets/dist/img/profile_picture/a7ccdb3d01647307a02e5bd80ca89ac9.jpg', '1');
INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES ('2', 'bs1wW4rklPlBwOA', 'khan', 'boss', '0', '', NULL, '1');
INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES ('3', 'wwjwSIbEuQ4S6sD', 'bdtask', 'tanzil', '0', '', NULL, '1');
INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES ('4', 'QfAvRrLHragkSYs', 'ovi', 'isaq', '0', '', NULL, '1');
INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES ('5', 'eJGHZzMlqcM3d4x', 'ovi', 'isaq', '0', '', NULL, '1');
INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES ('6', 'hM90DNPq3VQsk9T', '', '', '0', '', NULL, '1');
INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES ('7', 'dYmgfQW7j5H8oc9', 'hi word', 'konir', '0', '', NULL, '1');
INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES ('8', 'LaoMKlz7SCRxhjD', 'Isahaq', 'Hm', '0', '', NULL, '1');


#
# TABLE STRUCTURE FOR: vital_sign
#

DROP TABLE IF EXISTS `vital_sign`;

CREATE TABLE `vital_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(30) NOT NULL,
  `f_heart_rate` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `temprature` varchar(30) NOT NULL,
  `pulse_rate` varchar(30) NOT NULL,
  `respiration_rate` varchar(30) NOT NULL,
  `blood_pressure` varchar(30) NOT NULL,
  `description` text NOT NULL,
  `branch_id` int(11) NOT NULL,
  `assign_by` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: web_setting
#

DROP TABLE IF EXISTS `web_setting`;

CREATE TABLE `web_setting` (
  `setting_id` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `invoice_logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `timezone` varchar(150) NOT NULL,
  `currency_position` varchar(10) DEFAULT NULL,
  `footer_text` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `rtr` varchar(255) DEFAULT NULL,
  `captcha` int(11) DEFAULT '1' COMMENT '0=active,1=inactive',
  `site_key` varchar(250) DEFAULT NULL,
  `secret_key` varchar(250) DEFAULT NULL,
  `discount_type` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `web_setting` (`setting_id`, `logo`, `invoice_logo`, `favicon`, `currency`, `timezone`, `currency_position`, `footer_text`, `language`, `rtr`, `captcha`, `site_key`, `secret_key`, `discount_type`) VALUES ('1', 'http://softest8.bdtask.com/Updatesaleserp/./my-assets/image/logo/dae881d99e85a0cd176993e1f2915700.png', 'http://softest8.bdtask.com/Updatesaleserp/./my-assets/image/logo/b78063bf95d5a20a35cb9d708c01488e.png', 'http://softest8.bdtask.com/Updatesaleserp/my-assets/image/logo/e3b869167120139d49987ec0d863f2e1.png', '৳', 'Asia/Dhaka', '0', 'Copyright© 2018-2019 Coder IT. All rights reserved.', 'english', '0', '1', '', '', '1');


#
# TABLE STRUCTURE FOR: ws_about
#

DROP TABLE IF EXISTS `ws_about`;

CREATE TABLE `ws_about` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `quotation` varchar(150) CHARACTER SET utf8 NOT NULL,
  `author_name` varchar(35) CHARACTER SET utf8 NOT NULL,
  `signature` varchar(200) CHARACTER SET utf8 NOT NULL,
  `image` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ws_appoint_instruction
#

DROP TABLE IF EXISTS `ws_appoint_instruction`;

CREATE TABLE `ws_appoint_instruction` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `title` varchar(30) NOT NULL,
  `short_instruction` varchar(150) NOT NULL,
  `instruction` text NOT NULL,
  `note` varchar(150) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_banner
#

DROP TABLE IF EXISTS `ws_banner`;

CREATE TABLE `ws_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ws_basic
#

DROP TABLE IF EXISTS `ws_basic`;

CREATE TABLE `ws_basic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `twitter_api` text,
  `google_map` text,
  `google_map_api` text NOT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `vimeo` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `dribbble` varchar(100) DEFAULT NULL,
  `skype` varchar(100) DEFAULT NULL,
  `google_plus` varchar(100) DEFAULT NULL,
  `direction` varchar(255) NOT NULL,
  `latitude` float(10,8) NOT NULL,
  `longitude` float(10,8) NOT NULL,
  `map_active` tinyint(4) NOT NULL COMMENT '1=embed, 0=api',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_comment
#

DROP TABLE IF EXISTS `ws_comment`;

CREATE TABLE `ws_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `date` datetime NOT NULL,
  `add_to_website` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_menu
#

DROP TABLE IF EXISTS `ws_menu`;

CREATE TABLE `ws_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `parent_id` int(5) NOT NULL,
  `position` int(2) NOT NULL,
  `fixed` tinyint(1) NOT NULL COMMENT '1=fixed and 0=Not fixed',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=active and 0=inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_menu_lang
#

DROP TABLE IF EXISTS `ws_menu_lang`;

CREATE TABLE `ws_menu_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `language` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_news
#

DROP TABLE IF EXISTS `ws_news`;

CREATE TABLE `ws_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 NOT NULL,
  `featured_image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `create_date` date NOT NULL,
  `create_by` int(5) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1=active and 0=inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ws_news_language
#

DROP TABLE IF EXISTS `ws_news_language`;

CREATE TABLE `ws_news_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `language` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_page_content
#

DROP TABLE IF EXISTS `ws_page_content`;

CREATE TABLE `ws_page_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `url` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `featured_image` varchar(250) NOT NULL,
  `create_date` date NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_partner
#

DROP TABLE IF EXISTS `ws_partner`;

CREATE TABLE `ws_partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `url` varchar(120) CHARACTER SET utf8 NOT NULL,
  `image` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: ws_section
#

DROP TABLE IF EXISTS `ws_section`;

CREATE TABLE `ws_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text,
  `featured_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_section_lang
#

DROP TABLE IF EXISTS `ws_section_lang`;

CREATE TABLE `ws_section_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `section_id` int(6) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_service
#

DROP TABLE IF EXISTS `ws_service`;

CREATE TABLE `ws_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `name` varchar(20) NOT NULL,
  `icon_image` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `position` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date` date DEFAULT NULL,
  `is_parent` int(11) NOT NULL COMMENT '0=Parent',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_setting
#

DROP TABLE IF EXISTS `ws_setting`;

CREATE TABLE `ws_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(15) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `meta_tag` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `text` varchar(25) NOT NULL,
  `fax` varchar(25) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `closed_day` varchar(30) NOT NULL,
  `open_day` varchar(30) NOT NULL,
  `copyright_text` varchar(255) DEFAULT NULL,
  `working_hour` text NOT NULL,
  `isQRCode` tinyint(1) NOT NULL COMMENT '1=active and 0=inactive',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_slider
#

DROP TABLE IF EXISTS `ws_slider`;

CREATE TABLE `ws_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(120) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_slider_lang
#

DROP TABLE IF EXISTS `ws_slider_lang`;

CREATE TABLE `ws_slider_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(5) NOT NULL,
  `language` varchar(15) NOT NULL,
  `title` varchar(120) NOT NULL,
  `subtitle` varchar(150) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_testimonial
#

DROP TABLE IF EXISTS `ws_testimonial`;

CREATE TABLE `ws_testimonial` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(35) NOT NULL,
  `url` varchar(120) NOT NULL,
  `image` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: ws_testimonial_lang
#

DROP TABLE IF EXISTS `ws_testimonial_lang`;

CREATE TABLE `ws_testimonial_lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tstml_id` int(11) NOT NULL,
  `language` varchar(15) NOT NULL,
  `title` varchar(50) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `quotation` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

