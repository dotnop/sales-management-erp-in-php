-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2019 at 06:51 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saleserp_sas_v2`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_2`
--

CREATE TABLE `account_2` (
  `account_id` int(11) NOT NULL,
  `account_name` varchar(40) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `parent_id` int(1) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acc_coa`
--

CREATE TABLE `acc_coa` (
  `HeadCode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHeadName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `HeadLevel` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL,
  `IsTransaction` tinyint(1) NOT NULL,
  `IsGL` tinyint(1) NOT NULL,
  `HeadType` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `IsBudget` tinyint(1) NOT NULL,
  `IsDepreciation` tinyint(1) NOT NULL,
  `DepreciationRate` decimal(18,2) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UpdateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_coa`
--

INSERT INTO `acc_coa` (`HeadCode`, `HeadName`, `PHeadName`, `HeadLevel`, `IsActive`, `IsTransaction`, `IsGL`, `HeadType`, `IsBudget`, `IsDepreciation`, `DepreciationRate`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`) VALUES
('5020200004', '2T6QHG45AP4M11FG6TPT-shahabuddin', 'Account Payable', 3, 1, 1, 0, 'L', 0, 0, '0.00', '1', '2019-09-05 05:43:01', '', '0000-00-00 00:00:00'),
('5020200003', '6167UCENYO5JJITKRH21-hhhkkk', 'Account Payable', 3, 1, 1, 0, 'L', 0, 0, '0.00', '1', '2019-09-05 05:40:40', '', '0000-00-00 00:00:00'),
('102030134', '6FGVC6K7OX5L9Z1-Walking customer', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-04-20 10:47:37', '', '0000-00-00 00:00:00'),
('4021403', 'AC', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:33:55', '', '0000-00-00 00:00:00'),
('50202', 'Account Payable', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 'admin', '2015-10-15 19:50:43', '', '0000-00-00 00:00:00'),
('10203', 'Account Receivable', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', '', '0000-00-00 00:00:00', 'admin', '2013-09-18 15:29:35'),
('1020201', 'Advance', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 'Zoherul', '2015-05-31 13:29:12', 'admin', '2015-12-31 16:46:32'),
('102020103', 'Advance House Rent', 'Advance', 4, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-10-02 16:55:38', 'admin', '2016-10-02 16:57:32'),
('10202', 'Advance, Deposit And Pre-payments', 'Current Asset', 2, 1, 0, 0, 'A', 0, 0, '0.00', '', '0000-00-00 00:00:00', 'admin', '2015-12-31 16:46:24'),
('4020602', 'Advertisement and Publicity', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:51:44', '', '0000-00-00 00:00:00'),
('1010410', 'Air Cooler', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-05-23 12:13:55', '', '0000-00-00 00:00:00'),
('4020603', 'AIT Against Advertisement', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:52:09', '', '0000-00-00 00:00:00'),
('1', 'Assets', 'COA', 0, 1, 0, 0, 'A', 0, 0, '0.00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('1010204', 'Attendance Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:49:31', '', '0000-00-00 00:00:00'),
('40216', 'Audit Fee', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 'admin', '2017-07-18 12:54:30', '', '0000-00-00 00:00:00'),
('4021002', 'Bank Charge', 'Financial Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:21:03', '', '0000-00-00 00:00:00'),
('30203', 'Bank Interest', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, '0.00', 'Obaidul', '2015-01-03 14:49:54', 'admin', '2016-09-25 11:04:19'),
('102030140', 'BIXQAYJ3XDFBYZN-Hm Isahaq', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-08-27 08:33:11', '', '0000-00-00 00:00:00'),
('1010104', 'Book Shelf', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:46:11', '', '0000-00-00 00:00:00'),
('1010407', 'Books and Journal', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:45:37', '', '0000-00-00 00:00:00'),
('4020604', 'Business Development Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:52:29', '', '0000-00-00 00:00:00'),
('404', 'C Bill', 'Expence', 1, 1, 1, 0, 'E', 1, 1, '1.00', '1', '2019-06-17 11:51:58', '', '0000-00-00 00:00:00'),
('4020606', 'Campaign Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:52:57', 'admin', '2016-09-19 14:52:48'),
('4020502', 'Campus Rent', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:46:53', 'admin', '2017-04-27 17:02:39'),
('40212', 'Car Running Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:28:43', '', '0000-00-00 00:00:00'),
('10201', 'Cash & Cash Equivalent', 'Current Asset', 2, 1, 0, 1, 'A', 0, 0, '0.00', '1', '2019-06-25 13:47:29', 'admin', '2015-10-15 15:57:55'),
('1020102', 'Cash At Bank', 'Cash & Cash Equivalent', 3, 1, 0, 1, 'A', 0, 0, '0.00', '1', '2019-03-18 06:08:18', 'admin', '2015-10-15 15:32:42'),
('1020101', 'Cash In Hand', 'Cash & Cash Equivalent', 3, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-01-26 07:38:48', 'admin', '2016-05-23 12:05:43'),
('1010207', 'CCTV', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:51:24', '', '0000-00-00 00:00:00'),
('102020102', 'CEO Current A/C', 'Advance', 4, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-09-25 11:54:54', '', '0000-00-00 00:00:00'),
('1010101', 'Class Room Chair', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:45:29', '', '0000-00-00 00:00:00'),
('4021407', 'Close Circuit Cemera', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:35:35', '', '0000-00-00 00:00:00'),
('5020200006', 'CMBQPCTZMZ3ALHEYT8BG-Ovi', 'Account Payable', 3, 1, 1, 0, 'L', 0, 0, '0.00', '1', '2019-09-05 13:44:59', '', '0000-00-00 00:00:00'),
('4020601', 'Commision on Admission', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:51:21', 'admin', '2016-09-19 14:42:54'),
('1010206', 'Computer', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:51:09', '', '0000-00-00 00:00:00'),
('4021410', 'Computer (R)', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'Zoherul', '2016-03-24 12:38:52', 'Zoherul', '2016-03-24 12:41:40'),
('1010102', 'Computer Table', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:45:44', '', '0000-00-00 00:00:00'),
('301020401', 'Continuing Registration fee - UoL (Income)', 'Registration Fee (UOL) Income', 4, 1, 1, 0, 'I', 0, 0, '0.00', 'admin', '2015-10-15 17:40:40', '', '0000-00-00 00:00:00'),
('4020904', 'Contratuall Staff Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:12:34', '', '0000-00-00 00:00:00'),
('4020709', 'Cultural Expense', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'nasmud', '2017-04-29 12:45:10', '', '0000-00-00 00:00:00'),
('102', 'Current Asset', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', '', '0000-00-00 00:00:00', 'admin', '2018-07-07 11:23:00'),
('502', 'Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
('1020301', 'Customer Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', '1', '2019-01-24 12:10:05', 'admin', '2018-07-07 12:31:42'),
('1020202', 'Deposit', 'Advance, Deposit And Pre-payments', 3, 1, 0, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:40:42', '', '0000-00-00 00:00:00'),
('4020605', 'Design & Printing Expense', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:55:00', '', '0000-00-00 00:00:00'),
('102010201', 'Dhaka Bank ', 'Cash At Bank', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-06-28 05:44:41', '', '0000-00-00 00:00:00'),
('4020404', 'Dish Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:58:21', '', '0000-00-00 00:00:00'),
('40215', 'Dividend', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 'admin', '2016-09-25 14:07:55', '', '0000-00-00 00:00:00'),
('4020403', 'Drinking Water Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:58:10', '', '0000-00-00 00:00:00'),
('1010211', 'DSLR Camera', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:53:17', 'admin', '2016-01-02 16:23:25'),
('4020908', 'Earned Leave', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:13:38', '', '0000-00-00 00:00:00'),
('4020607', 'Education Fair Expenses', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:53:42', '', '0000-00-00 00:00:00'),
('102030205', 'EIWA3A4HT1-jisan', 'Loan Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-02-01 11:28:12', '', '0000-00-00 00:00:00'),
('1010602', 'Electric Equipment', 'Electrical Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:44:51', '', '0000-00-00 00:00:00'),
('1010203', 'Electric Kettle', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:49:07', '', '0000-00-00 00:00:00'),
('10106', 'Electrical Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:43:44', '', '0000-00-00 00:00:00'),
('4020407', 'Electricity Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:59:31', '', '0000-00-00 00:00:00'),
('50204', 'Employee Ledger', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', '1', '2019-04-08 10:36:32', '', '0000-00-00 00:00:00'),
('403', 'Employee Salary', 'Expence', 1, 1, 1, 0, 'E', 0, 1, '1.00', '1', '2019-06-17 11:44:52', '', '0000-00-00 00:00:00'),
('40201', 'Entertainment', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 'admin', '2013-07-08 16:21:26', 'anwarul', '2013-07-17 14:21:47'),
('2', 'Equity', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('4', 'Expence', 'COA', 0, 1, 0, 0, 'E', 0, 0, '0.00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('4020903', 'Faculty,Staff Salary & Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:12:21', '', '0000-00-00 00:00:00'),
('4021404', 'Fax Machine', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:34:15', '', '0000-00-00 00:00:00'),
('4020905', 'Festival & Incentive Bonus', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:12:48', '', '0000-00-00 00:00:00'),
('1010103', 'File Cabinet', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:46:02', '', '0000-00-00 00:00:00'),
('40210', 'Financial Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'anwarul', '2013-08-20 12:24:31', 'admin', '2015-10-15 19:20:36'),
('1010403', 'Fire Extingushier', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:39:32', '', '0000-00-00 00:00:00'),
('4021408', 'Furniture', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:35:47', '', '0000-00-00 00:00:00'),
('10101', 'Furniture & Fixturers', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 'anwarul', '2013-08-20 16:18:15', 'anwarul', '2013-08-21 13:35:40'),
('102030136', 'FYJ4EEBMOGTW25T-md abdullah', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-06-26 10:17:16', '', '0000-00-00 00:00:00'),
('20201', 'General Reserve', 'Reserve & Surplus', 2, 1, 1, 0, 'L', 0, 0, '0.00', 'admin', '2016-09-25 14:07:12', 'admin', '2016-10-02 17:48:49'),
('10105', 'Generator', 'Non Current Assets', 2, 1, 1, 1, 'A', 0, 0, '0.00', 'Zoherul', '2016-02-27 16:02:35', 'admin', '2016-05-23 12:05:18'),
('4021414', 'Generator Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'Zoherul', '2016-06-16 10:21:05', '', '0000-00-00 00:00:00'),
('40213', 'Generator Running Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:29:29', '', '0000-00-00 00:00:00'),
('10103', 'Groceries and Cutleries', 'Non Current Assets', 2, 1, 1, 1, 'A', 0, 0, '0.00', '2', '2018-07-12 10:02:55', '', '0000-00-00 00:00:00'),
('1010408', 'Gym Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:46:03', '', '0000-00-00 00:00:00'),
('407', 'honda-22552500', 'Expence', 1, 1, 1, 0, 'E', 1, 1, '1.00', '1', '2019-07-01 06:22:04', '', '0000-00-00 00:00:00'),
('4020907', 'Honorarium', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:13:26', '', '0000-00-00 00:00:00'),
('40205', 'House Rent', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'anwarul', '2013-08-24 10:26:56', '', '0000-00-00 00:00:00'),
('3', 'Income', 'COA', 0, 1, 0, 0, 'I', 0, 0, '0.00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('30204', 'Income from Photocopy & Printing', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, '0.00', 'Zoherul', '2015-07-14 10:29:54', 'admin', '2016-09-25 11:04:28'),
('5020302', 'Income Tax Payable', 'Liabilities for Expenses', 3, 1, 0, 1, 'L', 0, 0, '0.00', 'admin', '2016-09-19 11:18:17', 'admin', '2016-09-28 13:18:35'),
('10107', 'Inventory', 'Non Current Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', '2', '2018-07-07 15:21:58', '', '0000-00-00 00:00:00'),
('102030206', 'JMGCK5MVCN-dhaka electronics', 'Loan Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-06-27 13:07:33', '', '0000-00-00 00:00:00'),
('102030208', 'KDN4CJGVRK-abul hardware', 'Loan Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-06-27 13:08:11', '', '0000-00-00 00:00:00'),
('102030139', 'KFTUKN7RY1O2W6R-samil khan', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-06-28 05:47:49', '', '0000-00-00 00:00:00'),
('5020200002', 'KRQGUNN4ZMS3WT8I2RK9-global band ', 'Account Payable', 3, 1, 1, 0, 'L', 0, 0, '0.00', '1', '2019-06-28 05:56:23', '', '0000-00-00 00:00:00'),
('1010210', 'LCD TV', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:52:27', '', '0000-00-00 00:00:00'),
('30103', 'Lease Sale', 'Store Income', 1, 1, 1, 1, 'I', 0, 0, '0.00', '2', '2018-07-08 07:51:52', '', '0000-00-00 00:00:00'),
('5', 'Liabilities', 'COA', 0, 1, 0, 0, 'L', 0, 0, '0.00', 'admin', '2013-07-04 12:32:07', 'admin', '2015-10-15 19:46:54'),
('50203', 'Liabilities for Expenses', 'Current Liabilities', 2, 1, 0, 0, 'L', 0, 0, '0.00', 'admin', '2015-10-15 19:50:59', '', '0000-00-00 00:00:00'),
('4020707', 'Library Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2017-01-10 15:34:54', '', '0000-00-00 00:00:00'),
('4021409', 'Lift', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:36:12', '', '0000-00-00 00:00:00'),
('1020302', 'Loan Receivable', 'Account Receivable', 3, 1, 0, 1, 'A', 0, 0, '0.00', '1', '2019-01-26 07:37:20', '', '0000-00-00 00:00:00'),
('50101', 'Long Term Borrowing', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 'admin', '2013-07-04 12:32:26', 'admin', '2015-10-15 19:47:40'),
('4020608', 'Marketing & Promotion Exp.', 'Promonational Expence', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:53:59', '', '0000-00-00 00:00:00'),
('4020901', 'Medical Allowance', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:11:33', '', '0000-00-00 00:00:00'),
('1010411', 'Metal Ditector', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'Zoherul', '2016-08-22 10:55:22', '', '0000-00-00 00:00:00'),
('4021413', 'Micro Oven', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'Zoherul', '2016-05-12 14:53:51', '', '0000-00-00 00:00:00'),
('30202', 'Miscellaneous (Income)', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, '0.00', 'anwarul', '2014-02-06 15:26:31', 'admin', '2016-09-25 11:04:35'),
('4020909', 'Miscellaneous Benifit', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:13:53', '', '0000-00-00 00:00:00'),
('4020701', 'Miscellaneous Exp', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2016-09-25 12:54:39', '', '0000-00-00 00:00:00'),
('40207', 'Miscellaneous Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'anwarul', '2014-04-26 16:49:56', 'admin', '2016-09-25 12:54:19'),
('1010401', 'Mobile Phone', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-01-29 10:43:30', '', '0000-00-00 00:00:00'),
('102020101', 'Mr Ashiqur Rahman', 'Advance', 4, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-12-31 16:47:23', 'admin', '2016-09-25 11:55:13'),
('1010212', 'Network Accessories', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-01-02 16:23:32', '', '0000-00-00 00:00:00'),
('4020408', 'News Paper Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2016-01-02 15:55:57', '', '0000-00-00 00:00:00'),
('101', 'Non Current Assets', 'Assets', 1, 1, 0, 0, 'A', 0, 0, '0.00', '', '0000-00-00 00:00:00', 'admin', '2015-10-15 15:29:11'),
('501', 'Non Current Liabilities', 'Liabilities', 1, 1, 0, 0, 'L', 0, 0, '0.00', 'anwarul', '2014-08-30 13:18:20', 'admin', '2015-10-15 19:49:21'),
('1010404', 'Office Decoration', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:40:02', '', '0000-00-00 00:00:00'),
('10102', 'Office Equipment', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 'anwarul', '2013-12-06 18:08:00', 'admin', '2015-10-15 15:48:21'),
('4021401', 'Office Repair & Maintenance', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:33:15', '', '0000-00-00 00:00:00'),
('30201', 'Office Stationary (Income)', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, '0.00', 'anwarul', '2013-07-17 15:21:06', 'admin', '2016-09-25 11:04:50'),
('302', 'Other Income', 'Income', 1, 1, 0, 0, 'I', 0, 0, '0.00', '2', '2018-07-07 13:40:57', 'admin', '2016-09-25 11:04:09'),
('40211', 'Others (Non Academic Expenses)', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'Obaidul', '2014-12-03 16:05:42', 'admin', '2015-10-15 19:22:09'),
('30205', 'Others (Non-Academic Income)', 'Other Income', 2, 1, 0, 1, 'I', 0, 0, '0.00', 'admin', '2015-10-15 17:23:49', 'admin', '2015-10-15 17:57:52'),
('10104', 'Others Assets', 'Non Current Assets', 2, 1, 0, 1, 'A', 0, 0, '0.00', 'admin', '2016-01-29 10:43:16', '', '0000-00-00 00:00:00'),
('4020910', 'Outstanding Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'Zoherul', '2016-04-24 11:56:50', '', '0000-00-00 00:00:00'),
('4021405', 'Oven', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:34:31', '', '0000-00-00 00:00:00'),
('4021412', 'PABX-Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'Zoherul', '2016-04-24 14:40:18', '', '0000-00-00 00:00:00'),
('4020902', 'Part-time Staff Salary', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:12:06', '', '0000-00-00 00:00:00'),
('1010202', 'Photocopy & Fax Machine', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:47:27', 'admin', '2016-05-23 12:14:40'),
('4021411', 'Photocopy Machine Repair', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'Zoherul', '2016-04-24 12:40:02', 'admin', '2017-04-27 17:03:17'),
('3020503', 'Practical Fee', 'Others (Non-Academic Income)', 3, 1, 1, 1, 'I', 0, 0, '0.00', 'admin', '2017-07-22 18:00:37', '', '0000-00-00 00:00:00'),
('1020203', 'Prepayment', 'Advance, Deposit And Pre-payments', 3, 1, 0, 1, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:40:51', 'admin', '2015-12-31 16:49:58'),
('1010201', 'Printer', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:47:15', '', '0000-00-00 00:00:00'),
('40202', 'Printing and Stationary', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 'admin', '2013-07-08 16:21:45', 'admin', '2016-09-19 14:39:32'),
('402', 'Product Purchase', 'Expence', 1, 1, 0, 0, 'E', 0, 0, '0.00', '2', '2018-07-07 14:00:16', 'admin', '2015-10-15 18:37:42'),
('303', 'Product Sale', 'Income', 1, 1, 1, 0, 'I', 0, 0, '0.00', '1', '2019-06-17 08:22:42', '', '0000-00-00 00:00:00'),
('3020502', 'Professional Training Course(Oracal-1)', 'Others (Non-Academic Income)', 3, 1, 1, 0, 'I', 0, 0, '0.00', 'nasim', '2017-06-22 13:28:05', '', '0000-00-00 00:00:00'),
('30207', 'Professional Training Course(Oracal)', 'Other Income', 2, 1, 0, 1, 'I', 0, 0, '0.00', 'nasim', '2017-06-22 13:24:16', 'nasim', '2017-06-22 13:25:56'),
('1010208', 'Projector', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:51:44', '', '0000-00-00 00:00:00'),
('40206', 'Promonational Expence', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'anwarul', '2013-07-11 13:48:57', 'anwarul', '2013-07-17 14:23:03'),
('102030137', 'PVGGNNSWOESP45P-alexix', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-06-26 10:52:38', '', '0000-00-00 00:00:00'),
('40214', 'Repair and Maintenance', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:32:46', '', '0000-00-00 00:00:00'),
('202', 'Reserve & Surplus', 'Equity', 1, 1, 0, 1, 'L', 0, 0, '0.00', 'admin', '2016-09-25 14:06:34', 'admin', '2016-10-02 17:48:57'),
('20102', 'Retained Earnings', 'Share Holders Equity', 2, 1, 1, 1, 'L', 0, 0, '0.00', 'admin', '2016-05-23 11:20:40', 'admin', '2016-09-25 14:05:06'),
('4020708', 'River Cruse', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2017-04-24 15:35:25', '', '0000-00-00 00:00:00'),
('102020105', 'Salary', 'Advance', 4, 1, 0, 0, 'A', 0, 0, '0.00', 'admin', '2018-07-05 11:46:44', '', '0000-00-00 00:00:00'),
('40209', 'Salary & Allowances', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'anwarul', '2013-12-12 11:22:58', '', '0000-00-00 00:00:00'),
('1010406', 'Security Equipment', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:41:30', '', '0000-00-00 00:00:00'),
('304', 'Service Income', 'Income', 1, 1, 1, 0, 'I', 0, 0, '0.00', '1', '2019-06-17 11:31:11', '', '0000-00-00 00:00:00'),
('20101', 'Share Capital', 'Share Holders Equity', 2, 1, 0, 1, 'L', 0, 0, '0.00', 'anwarul', '2013-12-08 19:37:32', 'admin', '2015-10-15 19:45:35'),
('201', 'Share Holders Equity', 'Equity', 1, 1, 0, 0, 'L', 0, 0, '0.00', '', '0000-00-00 00:00:00', 'admin', '2015-10-15 19:43:51'),
('405', 'shop rent ', 'Expence', 1, 1, 1, 0, 'E', 1, 1, '1.00', '1', '2019-06-28 05:40:09', '', '0000-00-00 00:00:00'),
('50201', 'Short Term Borrowing', 'Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 'admin', '2015-10-15 19:50:30', '', '0000-00-00 00:00:00'),
('40208', 'Software Development Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'anwarul', '2013-11-21 14:13:01', 'admin', '2015-10-15 19:02:51'),
('4020906', 'Special Allowances', 'Salary & Allowances', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:13:13', '', '0000-00-00 00:00:00'),
('50102', 'Sponsors Loan', 'Non Current Liabilities', 2, 1, 0, 1, 'L', 0, 0, '0.00', 'admin', '2015-10-15 19:48:02', '', '0000-00-00 00:00:00'),
('4020706', 'Sports Expense', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'nasmud', '2016-11-09 13:16:53', '', '0000-00-00 00:00:00'),
('301', 'Store Income', 'Income', 1, 1, 0, 0, 'I', 0, 0, '0.00', '2', '2018-07-07 13:40:37', 'admin', '2015-09-17 17:00:02'),
('3020501', 'Students Info. Correction Fee', 'Others (Non-Academic Income)', 3, 1, 1, 0, 'I', 0, 0, '0.00', 'admin', '2015-10-15 17:24:45', '', '0000-00-00 00:00:00'),
('1010601', 'Sub Station', 'Electrical Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:44:11', '', '0000-00-00 00:00:00'),
('4020704', 'TB Care Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2016-10-08 13:03:04', '', '0000-00-00 00:00:00'),
('30206', 'TB Care Income', 'Other Income', 2, 1, 1, 1, 'I', 0, 0, '0.00', 'admin', '2016-10-08 13:00:56', '', '0000-00-00 00:00:00'),
('4020501', 'TDS on House Rent', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:44:07', 'admin', '2016-09-19 14:40:16'),
('502030201', 'TDS Payable House Rent', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, '0.00', 'admin', '2016-09-19 11:19:42', 'admin', '2016-09-28 13:19:37'),
('502030203', 'TDS Payable on Advertisement Bill', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, '0.00', 'admin', '2016-09-28 13:20:51', '', '0000-00-00 00:00:00'),
('502030202', 'TDS Payable on Salary', 'Income Tax Payable', 4, 1, 1, 0, 'L', 0, 0, '0.00', 'admin', '2016-09-28 13:20:17', '', '0000-00-00 00:00:00'),
('406', 'Tea biscut', 'Expence', 1, 1, 1, 0, 'E', 1, 1, '1.00', '1', '2019-06-28 05:40:26', '', '0000-00-00 00:00:00'),
('4021402', 'Tea Kettle', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:33:45', '', '0000-00-00 00:00:00'),
('4020402', 'Telephone Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:57:59', '', '0000-00-00 00:00:00'),
('1010209', 'Telephone Set & PABX', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:51:57', 'admin', '2016-10-02 17:10:40'),
('10108', 'Test', 'Non Current Assets', 2, 1, 0, 0, 'A', 0, 0, '0.00', '1', '2019-09-19 06:03:07', '', '0000-00-00 00:00:00'),
('40203', 'Travelling & Conveyance', 'Other Expenses', 2, 1, 1, 1, 'E', 0, 0, '0.00', 'admin', '2013-07-08 16:22:06', 'admin', '2015-10-15 18:45:13'),
('4021406', 'TV', 'Repair and Maintenance', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 19:35:07', '', '0000-00-00 00:00:00'),
('1010205', 'UPS', 'Office Equipment', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:50:38', '', '0000-00-00 00:00:00'),
('40204', 'Utility Expenses', 'Other Expenses', 2, 1, 0, 1, 'E', 0, 0, '0.00', 'anwarul', '2013-07-11 16:20:24', 'admin', '2016-01-02 15:55:22'),
('4020503', 'VAT on House Rent Exp', 'House Rent', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:49:22', 'admin', '2016-09-25 14:00:52'),
('5020301', 'VAT Payable', 'Liabilities for Expenses', 3, 1, 0, 1, 'L', 0, 0, '0.00', 'admin', '2015-10-15 19:51:11', 'admin', '2016-09-28 13:23:53'),
('1010409', 'Vehicle A/C', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'Zoherul', '2016-05-12 12:13:21', '', '0000-00-00 00:00:00'),
('102030207', 'VJY67W3YTE-tesco ', 'Loan Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-06-27 13:07:51', '', '0000-00-00 00:00:00'),
('1010405', 'Voltage Stablizer', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-03-27 10:40:59', '', '0000-00-00 00:00:00'),
('1010105', 'Waiting Sofa - Steel', 'Furniture & Fixturers', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2015-10-15 15:46:29', '', '0000-00-00 00:00:00'),
('4020405', 'WASA Bill', 'Utility Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2015-10-15 18:58:51', '', '0000-00-00 00:00:00'),
('1010402', 'Water Purifier', 'Others Assets', 3, 1, 1, 0, 'A', 0, 0, '0.00', 'admin', '2016-01-29 11:14:11', '', '0000-00-00 00:00:00'),
('4020705', 'Website Development Expenses', 'Miscellaneous Expenses', 3, 1, 1, 0, 'E', 0, 0, '0.00', 'admin', '2016-10-15 12:42:47', '', '0000-00-00 00:00:00'),
('5020200001', 'WPXXU7UC1Z4ID1HUVU53-Test Supplier', 'Account Payable', 3, 1, 1, 0, 'L', 0, 0, '0.00', '1', '2019-06-25 09:07:26', '', '0000-00-00 00:00:00'),
('5020200005', 'XUQB7JFFJSFXKGOZ9VD3-isahaq', 'Account Payable', 3, 1, 1, 0, 'L', 0, 0, '0.00', '1', '2019-09-05 06:01:45', '', '0000-00-00 00:00:00'),
('102030135', 'XWCEM9UVV5NAT5R-Test customer', 'Customer Receivable', 4, 1, 1, 0, 'A', 0, 0, '0.00', '1', '2019-06-25 09:09:05', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `acc_income_expence`
--

CREATE TABLE `acc_income_expence` (
  `ID` int(11) NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Student_Id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `Paymode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Perpose` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci NOT NULL,
  `StoreID` int(11) NOT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Amount` decimal(10,2) NOT NULL,
  `IsApprove` tinyint(4) NOT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CreateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `acc_transaction`
--

CREATE TABLE `acc_transaction` (
  `ID` int(11) NOT NULL,
  `VNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Vtype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VDate` date DEFAULT NULL,
  `COAID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Narration` text COLLATE utf8_unicode_ci,
  `Debit` decimal(18,2) DEFAULT NULL,
  `Credit` decimal(18,2) DEFAULT NULL,
  `IsPosted` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `IsAppove` char(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acc_transaction`
--

INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES
(1, '20190625090838', 'Purchase', '2019-06-25', '10107', 'Inventory Debit For Supplier Test Supplier', '7500.00', '0.00', '1', '1', '2019-06-25 09:08:38', NULL, NULL, '1'),
(2, '20190625090838', 'Purchase', '2019-06-25', '5020200001', 'Supplier .Test Supplier', '0.00', '7500.00', '1', '1', '2019-06-25 00:00:00', NULL, NULL, '1'),
(3, '20190625090838', 'Purchase', '2019-06-25', '402', 'Company Credit For  Test Supplier', '7500.00', '0.00', '1', '1', '2019-06-25 09:08:38', NULL, NULL, '1'),
(4, '20190625090838', 'Purchase', '2019-06-25', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '7500.00', '1', '1', '2019-06-25 09:08:38', NULL, NULL, '1'),
(5, '20190625090838', 'Purchase', '2019-06-25', '5020200001', 'Supplier .Test Supplier', '7500.00', '0.00', '1', '1', '2019-06-25 00:00:00', NULL, NULL, '1'),
(6, '3498244447', 'INV', '2019-06-25', '10107', 'Inventory credit For Invoice No3498244447', '0.00', '150.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1'),
(7, '3498244447', 'INV', '2019-06-25', '102030135', 'Customer debit For  Test customer', '200.00', '0.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1'),
(8, '3498244447', 'INVOICE', '2019-06-25', '303', 'Sale Income For Test customer', '0.00', '200.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1'),
(9, '3498244447', 'INV', '2019-06-25', '102030135', 'Customer credit for Paid Amount For Customer Test customer', '0.00', '200.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1'),
(10, '3498244447', 'INV', '2019-06-25', '1020101', 'Cash in Hand in Sale for Test customer', '200.00', '0.00', '1', '1', '2019-06-25 09:09:28', NULL, NULL, '1'),
(11, '4477997231', 'INV', '2019-06-25', '10107', 'Inventory credit For Invoice No4477997231', '0.00', '150.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1'),
(12, '4477997231', 'INV', '2019-06-25', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1'),
(13, '4477997231', 'INVOICE', '2019-06-25', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1'),
(14, '4477997231', 'INV', '2019-06-25', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1'),
(15, '4477997231', 'INV', '2019-06-25', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-06-25 09:19:13', NULL, NULL, '1'),
(16, '20190625132953', 'Purchase', '2019-06-25', '10107', 'Inventory Debit For Supplier Test Supplier', '4000.00', '0.00', '1', '1', '2019-06-25 13:29:53', NULL, NULL, '1'),
(17, '20190625132953', 'Purchase', '2019-06-25', '5020200001', 'Supplier .Test Supplier', '0.00', '4000.00', '1', '1', '2019-06-25 00:00:00', NULL, NULL, '1'),
(18, '20190625132953', 'Purchase', '2019-06-25', '402', 'Company Credit For  Test Supplier', '4000.00', '0.00', '1', '1', '2019-06-25 13:29:53', NULL, NULL, '1'),
(19, '20190625132953', 'Purchase', '2019-06-25', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '4000.00', '1', '1', '2019-06-25 13:29:53', NULL, NULL, '1'),
(20, '20190625132953', 'Purchase', '2019-06-25', '5020200001', 'Supplier .Test Supplier', '4000.00', '0.00', '1', '1', '2019-06-25 00:00:00', NULL, NULL, '1'),
(21, '3973611118', 'INV', '2019-06-25', '10107', 'Inventory credit For Invoice No3973611118', '0.00', '400.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1'),
(22, '3973611118', 'INV', '2019-06-25', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1'),
(23, '3973611118', 'INVOICE', '2019-06-25', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1'),
(24, '3973611118', 'INV', '2019-06-25', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1'),
(25, '3973611118', 'INV', '2019-06-25', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-25 13:30:33', NULL, NULL, '1'),
(26, 'PM-1', 'PM', '2019-06-25', '5020200001', '', '250.00', '0.00', '1', '1', '2019-06-25 13:49:27', NULL, NULL, '1'),
(27, 'PM-1', 'PM', '2019-06-25', '1020101', 'Cash in Hand For Voucher NoPM-1', '0.00', '250.00', '1', '1', '2019-06-25 13:49:27', NULL, NULL, '1'),
(28, '3923586588', 'INV', '2019-06-25', '10107', 'Inventory credit For Invoice No3923586588', '0.00', '950.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1'),
(29, '3923586588', 'INV', '2019-06-25', '102030134', 'Customer debit For  Walking customer', '900.00', '0.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1'),
(30, '3923586588', 'INVOICE', '2019-06-25', '303', 'Sale Income For Walking customer', '0.00', '900.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1'),
(31, '3923586588', 'INV', '2019-06-25', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '600.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1'),
(32, '3923586588', 'INV', '2019-06-25', '1020101', 'Cash in Hand in Sale for Walking customer', '600.00', '0.00', '1', '1', '2019-06-25 13:52:59', NULL, NULL, '1'),
(33, '6575276926', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No6575276926', '0.00', '150.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1'),
(34, '6575276926', 'INV', '2019-06-26', '102030135', 'Customer debit For  Test customer', '200.00', '0.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1'),
(35, '6575276926', 'INVOICE', '2019-06-26', '303', 'Sale Income For Test customer', '0.00', '200.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1'),
(36, '6575276926', 'INV', '2019-06-26', '102030135', 'Customer credit for Paid Amount For Customer Test customer', '0.00', '200.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1'),
(37, '6575276926', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Test customer', '200.00', '0.00', '1', '1', '2019-06-26 05:34:09', NULL, NULL, '1'),
(38, '6643494915', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No6643494915', '0.00', '150.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1'),
(39, '6643494915', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1'),
(40, '6643494915', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1'),
(41, '6643494915', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1'),
(42, '6643494915', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-06-26 05:35:33', NULL, NULL, '1'),
(43, '1969912464', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No1969912464', '0.00', '400.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1'),
(44, '1969912464', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1'),
(45, '1969912464', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1'),
(46, '1969912464', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1'),
(47, '1969912464', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:36:30', NULL, NULL, '1'),
(48, '6691959977', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No6691959977', '0.00', '400.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1'),
(49, '6691959977', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1'),
(50, '6691959977', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1'),
(51, '6691959977', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1'),
(52, '6691959977', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:37:50', NULL, NULL, '1'),
(53, '2258111339', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No2258111339', '0.00', '150.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1'),
(54, '2258111339', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1'),
(55, '2258111339', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1'),
(56, '2258111339', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1'),
(57, '2258111339', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-06-26 05:38:33', NULL, NULL, '1'),
(58, '9624435359', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No9624435359', '0.00', '400.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1'),
(59, '9624435359', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1'),
(60, '9624435359', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1'),
(61, '9624435359', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1'),
(62, '9624435359', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:38:35', NULL, NULL, '1'),
(63, '8668653724', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No8668653724', '0.00', '400.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1'),
(64, '8668653724', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1'),
(65, '8668653724', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1'),
(66, '8668653724', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1'),
(67, '8668653724', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:41:41', NULL, NULL, '1'),
(68, '8558184696', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No8558184696', '0.00', '400.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1'),
(69, '8558184696', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1'),
(70, '8558184696', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1'),
(71, '8558184696', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1'),
(72, '8558184696', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-26 05:55:57', NULL, NULL, '1'),
(73, '20190626100758', 'Purchase', '2019-06-26', '10107', 'Inventory Debit For Supplier Test Supplier', '3500.00', '0.00', '1', '1', '2019-06-26 10:07:58', NULL, NULL, '1'),
(74, '20190626100758', 'Purchase', '2019-06-26', '5020200001', 'Supplier .Test Supplier', '0.00', '3500.00', '1', '1', '2019-06-26 00:00:00', NULL, NULL, '1'),
(75, '20190626100758', 'Purchase', '2019-06-26', '402', 'Company Credit For  Test Supplier', '3500.00', '0.00', '1', '1', '2019-06-26 10:07:58', NULL, NULL, '1'),
(76, '20190626100758', 'Purchase', '2019-06-26', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '3500.00', '1', '1', '2019-06-26 10:07:58', NULL, NULL, '1'),
(77, '20190626100758', 'Purchase', '2019-06-26', '5020200001', 'Supplier .Test Supplier', '3500.00', '0.00', '1', '1', '2019-06-26 00:00:00', NULL, NULL, '1'),
(78, '9147771963', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No9147771963', '0.00', '350.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1'),
(79, '9147771963', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '450.00', '0.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1'),
(80, '9147771963', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '450.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1'),
(81, '9147771963', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '450.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1'),
(82, '9147771963', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '450.00', '0.00', '1', '1', '2019-06-26 10:11:20', NULL, NULL, '1'),
(83, '20190626101452', 'Purchase', '2019-06-26', '10107', 'Inventory Debit For Supplier Test Supplier', '3900.00', '0.00', '1', '1', '2019-06-26 10:14:52', NULL, NULL, '1'),
(84, '20190626101452', 'Purchase', '2019-06-26', '5020200001', 'Supplier .Test Supplier', '0.00', '3900.00', '1', '1', '2019-06-26 00:00:00', NULL, NULL, '1'),
(85, '20190626101452', 'Purchase', '2019-06-26', '402', 'Company Credit For  Test Supplier', '3900.00', '0.00', '1', '1', '2019-06-26 10:14:52', NULL, NULL, '1'),
(86, '20190626101452', 'Purchase', '2019-06-26', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '3900.00', '1', '1', '2019-06-26 10:14:52', NULL, NULL, '1'),
(87, '20190626101452', 'Purchase', '2019-06-26', '5020200001', 'Supplier .Test Supplier', '3900.00', '0.00', '1', '1', '2019-06-26 00:00:00', NULL, NULL, '1'),
(88, '3169824785', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No3169824785', '0.00', '390.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1'),
(89, '3169824785', 'INV', '2019-06-26', '102030136', 'Customer debit For  md abdullah', '800.00', '0.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1'),
(90, '3169824785', 'INVOICE', '2019-06-26', '303', 'Sale Income For md abdullah', '0.00', '800.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1'),
(91, '3169824785', 'INV', '2019-06-26', '102030136', 'Customer credit for Paid Amount For Customer md abdullah', '0.00', '800.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1'),
(92, '3169824785', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for md abdullah', '800.00', '0.00', '1', '1', '2019-06-26 10:17:16', NULL, NULL, '1'),
(93, '3918198168', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No3918198168', '0.00', '890.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1'),
(94, '3918198168', 'INV', '2019-06-26', '102030134', 'Customer debit For  Walking customer', '1450.00', '0.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1'),
(95, '3918198168', 'INVOICE', '2019-06-26', '303', 'Sale Income For Walking customer', '0.00', '1450.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1'),
(96, '3918198168', 'INV', '2019-06-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '1450.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1'),
(97, '3918198168', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for Walking customer', '1450.00', '0.00', '1', '1', '2019-06-26 10:50:04', NULL, NULL, '1'),
(98, '5219125479', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No5219125479', '0.00', '350.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1'),
(99, '5219125479', 'INV', '2019-06-26', '102030137', 'Customer debit For  alexix', '450.00', '0.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1'),
(100, '5219125479', 'INVOICE', '2019-06-26', '303', 'Sale Income For alexix', '0.00', '450.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1'),
(101, '5219125479', 'INV', '2019-06-26', '102030137', 'Customer credit for Paid Amount For Customer alexix', '0.00', '450.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1'),
(102, '5219125479', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for alexix', '450.00', '0.00', '1', '1', '2019-06-26 10:52:46', NULL, NULL, '1'),
(103, '8394724961', 'INV', '2019-06-26', '10107', 'Inventory credit For Invoice No8394724961', '0.00', '890.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1'),
(104, '8394724961', 'INV', '2019-06-26', '102030137', 'Customer debit For  alexix', '1450.00', '0.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1'),
(105, '8394724961', 'INVOICE', '2019-06-26', '303', 'Sale Income For alexix', '0.00', '1450.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1'),
(106, '8394724961', 'INV', '2019-06-26', '102030137', 'Customer credit for Paid Amount For Customer alexix', '0.00', '1450.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1'),
(107, '8394724961', 'INV', '2019-06-26', '1020101', 'Cash in Hand in Sale for alexix', '1450.00', '0.00', '1', '1', '2019-06-26 10:55:06', NULL, NULL, '1'),
(108, '7986446844', 'INV', '2019-06-27', '10107', 'Inventory credit For Invoice No7986446844', '0.00', '390.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1'),
(109, '7986446844', 'INV', '2019-06-27', '102030134', 'Customer debit For  Walking customer', '800.00', '0.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1'),
(110, '7986446844', 'INVOICE', '2019-06-27', '303', 'Sale Income For Walking customer', '0.00', '800.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1'),
(111, '7986446844', 'INV', '2019-06-27', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '800.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1'),
(112, '7986446844', 'INV', '2019-06-27', '1020101', 'Cash in Hand in Sale for Walking customer', '800.00', '0.00', '1', '1', '2019-06-27 10:58:16', NULL, NULL, '1'),
(113, '6789217583', 'INV', '2019-06-27', '10107', 'Inventory credit For Invoice No6789217583', '0.00', '400.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1'),
(114, '6789217583', 'INV', '2019-06-27', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1'),
(115, '6789217583', 'INVOICE', '2019-06-27', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1'),
(116, '6789217583', 'INV', '2019-06-27', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1'),
(117, '6789217583', 'INV', '2019-06-27', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-27 12:28:36', NULL, NULL, '1'),
(118, 'KT84M3I2O7', 'LNR', '2019-06-27', '102030206', 'Loan for .dhaka electronics', '150.00', '0.00', '1', '1', '2019-06-27 13:08:47', NULL, NULL, '1'),
(119, 'KT84M3I2O7', 'LNR', '2019-06-27', '1020101', 'Cash in Hand Credit For dhaka electronics', '0.00', '150.00', '1', '1', '2019-06-27 13:08:47', NULL, NULL, '1'),
(120, 'F6E1J7D1DX', 'LNR', '2019-06-27', '102030207', 'Loan for .tesco ', '500.00', '0.00', '1', '1', '2019-06-27 13:09:01', NULL, NULL, '1'),
(121, 'F6E1J7D1DX', 'LNR', '2019-06-27', '1020101', 'Cash in Hand Credit For tesco ', '0.00', '500.00', '1', '1', '2019-06-27 13:09:01', NULL, NULL, '1'),
(122, 'XJ4B8N4TWO', 'LNR', '2019-06-27', '102030208', 'Loan for .abul hardware', '650.00', '0.00', '1', '1', '2019-06-27 13:09:12', NULL, NULL, '1'),
(123, 'XJ4B8N4TWO', 'LNR', '2019-06-27', '1020101', 'Cash in Hand Credit For abul hardware', '0.00', '650.00', '1', '1', '2019-06-27 13:09:12', NULL, NULL, '1'),
(124, '4978495577', 'INV', '2019-06-27', '10107', 'Inventory credit For Invoice No4978495577', '0.00', '750.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1'),
(125, '4978495577', 'INV', '2019-06-27', '102030138', 'Customer debit For  kamal', '950.00', '0.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1'),
(126, '4978495577', 'INVOICE', '2019-06-27', '303', 'Sale Income For kamal', '0.00', '950.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1'),
(127, '4978495577', 'INV', '2019-06-27', '102030138', 'Customer credit for Paid Amount For Customer kamal', '0.00', '500.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1'),
(128, '4978495577', 'INV', '2019-06-27', '1020101', 'Cash in Hand in Sale for kamal', '500.00', '0.00', '1', '1', '2019-06-27 13:10:49', NULL, NULL, '1'),
(129, '20190628054055', 'Expense', '2019-06-28', '406', 'Tea biscut Expense 20190628054055', '350.00', '0.00', '1', '1', '2019-06-28 05:40:55', NULL, NULL, '1'),
(130, '20190628054055', 'Expense', '2019-06-28', '1020101', 'Tea biscut Expense20190628054055', '0.00', '350.00', '1', '1', '2019-06-28 05:40:55', NULL, NULL, '1'),
(131, '20190628054109', 'Expense', '2019-06-28', '405', 'shop rent  Expense 20190628054109', '450.00', '0.00', '1', '1', '2019-06-28 05:41:09', NULL, NULL, '1'),
(132, '20190628054109', 'Expense', '2019-06-28', '1020101', 'shop rent  Expense20190628054109', '0.00', '450.00', '1', '1', '2019-06-28 05:41:09', NULL, NULL, '1'),
(133, 'VS6VVPYSTP', 'LNR', '2019-06-28', '102030206', 'Loan for .dhaka electronics', '555.00', '0.00', '1', '1', '2019-06-28 05:41:28', NULL, NULL, '1'),
(134, 'VS6VVPYSTP', 'LNR', '2019-06-28', '1020101', 'Cash in Hand Credit For dhaka electronics', '0.00', '555.00', '1', '1', '2019-06-28 05:41:28', NULL, NULL, '1'),
(135, 'QV9JT8IFJE', 'LNP', '2019-06-28', '102030207', 'Loan Payment from .tesco ', '0.00', '333.00', '1', '1', '2019-06-28 05:41:50', NULL, NULL, '1'),
(136, 'QV9JT8IFJE', 'LNR', '2019-06-28', '1020101', 'Cash in Hand Debit For tesco ', '333.00', '0.00', '1', '1', '2019-06-28 05:41:50', NULL, NULL, '1'),
(137, '20190628054650', 'Purchase', '2019-06-28', '10107', 'Inventory Debit For Supplier Test Supplier', '400000.00', '0.00', '1', '1', '2019-06-28 05:46:50', NULL, NULL, '1'),
(138, '20190628054650', 'Purchase', '2019-06-28', '5020200001', 'Supplier .Test Supplier', '0.00', '400000.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1'),
(139, '20190628054650', 'Purchase', '2019-06-28', '402', 'Company Credit For  Test Supplier', '400000.00', '0.00', '1', '1', '2019-06-28 05:46:50', NULL, NULL, '1'),
(140, '2687213589', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No2687213589', '0.00', '40000.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1'),
(141, '2687213589', 'INV', '2019-06-28', '102030139', 'Customer debit For  samil khan', '50000.00', '0.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1'),
(142, '2687213589', 'INVOICE', '2019-06-28', '303', 'Sale Income For samil khan', '0.00', '50000.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1'),
(143, '2687213589', 'INV', '2019-06-28', '102030139', 'Customer credit for Paid Amount For Customer samil khan', '0.00', '30000.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1'),
(144, '2687213589', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for samil khan', '30000.00', '0.00', '1', '1', '2019-06-28 05:48:12', NULL, NULL, '1'),
(145, 'G21MAEQN2P', 'Advance', '2019-06-28', '102030139', 'Customer Advance Forsamil khan', '111.00', '0.00', '1', '1', '2019-06-28 05:51:19', NULL, NULL, '1'),
(146, 'G21MAEQN2P', 'Advance', '2019-06-28', '1020101', 'Cash in Hand  For samil khan Advance', '111.00', '0.00', '1', '1', '2019-06-28 05:51:19', NULL, NULL, '1'),
(147, 'HJK5D3PFUW', 'Advance', '2019-06-28', '102030138', 'Customer Advance Forkamal', '0.00', '200.00', '1', '1', '2019-06-28 05:51:37', NULL, NULL, '1'),
(148, 'HJK5D3PFUW', 'Advance', '2019-06-28', '1020101', 'Cash in Hand  For kamal Advance', '0.00', '200.00', '1', '1', '2019-06-28 05:51:37', NULL, NULL, '1'),
(149, 'V2WGALHBZ5', 'Advance', '2019-06-28', '5020200001', 'supplier Advance For Test Supplier', '2500.00', '0.00', '1', '1', '2019-06-28 05:54:12', NULL, NULL, '1'),
(150, 'V2WGALHBZ5', 'Advance', '2019-06-28', '1020101', 'Cash in Hand  For Test Supplier Advance', '2500.00', '0.00', '1', '1', '2019-06-28 05:54:12', NULL, NULL, '1'),
(151, '20190628055844', 'Purchase', '2019-06-28', '10107', 'Inventory Debit For Supplier global band ', '150000.00', '0.00', '1', '1', '2019-06-28 05:58:44', NULL, NULL, '1'),
(152, '20190628055844', 'Purchase', '2019-06-28', '5020200002', 'Supplier .global band ', '0.00', '150000.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1'),
(153, '20190628055844', 'Purchase', '2019-06-28', '402', 'Company Credit For  global band ', '150000.00', '0.00', '1', '1', '2019-06-28 05:58:44', NULL, NULL, '1'),
(154, '20190628055844', 'Purchase', '2019-06-28', '1020101', 'Cash in Hand For Supplier global band ', '0.00', '150000.00', '1', '1', '2019-06-28 05:58:44', NULL, NULL, '1'),
(155, '20190628055844', 'Purchase', '2019-06-28', '5020200002', 'Supplier .global band ', '150000.00', '0.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1'),
(156, '20190628062109', 'Purchase', '2019-06-28', '10107', 'Inventory Debit For Supplier Test Supplier', '1900.00', '0.00', '1', '1', '2019-06-28 06:21:09', NULL, NULL, '1'),
(157, '20190628062109', 'Purchase', '2019-06-28', '5020200001', 'Supplier .Test Supplier', '0.00', '1900.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1'),
(158, '20190628062109', 'Purchase', '2019-06-28', '402', 'Company Credit For  Test Supplier', '1900.00', '0.00', '1', '1', '2019-06-28 06:21:09', NULL, NULL, '1'),
(159, '20190628062109', 'Purchase', '2019-06-28', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '1900.00', '1', '1', '2019-06-28 06:21:09', NULL, NULL, '1'),
(160, '20190628062109', 'Purchase', '2019-06-28', '5020200001', 'Supplier .Test Supplier', '1900.00', '0.00', '1', '1', '2019-06-28 00:00:00', NULL, NULL, '1'),
(161, '3378453241', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No3378453241', '0.00', '390.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1'),
(162, '3378453241', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1'),
(163, '3378453241', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1'),
(164, '3378453241', 'INV', '2019-06-28', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1'),
(165, '3378453241', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-28 06:24:01', NULL, NULL, '1'),
(166, '4458233386', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No4458233386', '0.00', '390.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1'),
(167, '4458233386', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1'),
(168, '4458233386', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1'),
(169, '4458233386', 'INV', '2019-06-28', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1'),
(170, '4458233386', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-06-28 06:24:10', NULL, NULL, '1'),
(171, '1114135497', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No1114135497', '0.00', '1520.00', '1', '1', '2019-06-28 10:57:32', NULL, NULL, '1'),
(172, '1114135497', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '2210.00', '0.00', '1', '1', '2019-06-28 10:57:32', NULL, NULL, '1'),
(173, '1114135497', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '2210.00', '1', '1', '2019-06-28 10:57:32', NULL, NULL, '1'),
(174, '4441511736', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No4441511736', '0.00', '1520.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1'),
(175, '4441511736', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '2210.00', '0.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1'),
(176, '4441511736', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '2210.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1'),
(177, '4441511736', 'INV', '2019-06-28', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '2210.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1'),
(178, '4441511736', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for Walking customer', '2210.00', '0.00', '1', '1', '2019-06-28 10:57:34', NULL, NULL, '1'),
(179, '8754679919', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No8754679919', '0.00', '40000.00', '1', '1', '2019-06-28 10:58:10', NULL, NULL, '1'),
(180, '8754679919', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '50000.00', '0.00', '1', '1', '2019-06-28 10:58:10', NULL, NULL, '1'),
(181, '8754679919', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '50000.00', '1', '1', '2019-06-28 10:58:10', NULL, NULL, '1'),
(182, '3378767423', 'INV', '2019-06-28', '10107', 'Inventory credit For Invoice No3378767423', '0.00', '40000.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1'),
(183, '3378767423', 'INV', '2019-06-28', '102030134', 'Customer debit For  Walking customer', '50000.00', '0.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1'),
(184, '3378767423', 'INVOICE', '2019-06-28', '303', 'Sale Income For Walking customer', '0.00', '50000.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1'),
(185, '3378767423', 'INV', '2019-06-28', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50000.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1'),
(186, '3378767423', 'INV', '2019-06-28', '1020101', 'Cash in Hand in Sale for Walking customer', '50000.00', '0.00', '1', '1', '2019-06-28 10:58:19', NULL, NULL, '1'),
(187, '9317933378', 'INV', '2019-06-29', '10107', 'Inventory credit For Invoice No9317933378', '0.00', '40000.00', '1', '1', '2019-06-29 05:30:07', NULL, NULL, '1'),
(188, '9317933378', 'INV', '2019-06-29', '102030134', 'Customer debit For  Walking customer', '50000.00', '0.00', '1', '1', '2019-06-29 05:30:07', NULL, NULL, '1'),
(189, '9317933378', 'INVOICE', '2019-06-29', '303', 'Sale Income For Walking customer', '0.00', '50000.00', '1', '1', '2019-06-29 05:30:07', NULL, NULL, '1'),
(190, '1797561487', 'INV', '2019-06-29', '10107', 'Inventory credit For Invoice No1797561487', '0.00', '30780.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1'),
(191, '1797561487', 'INV', '2019-06-29', '102030134', 'Customer debit For  Walking customer', '36000.00', '0.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1'),
(192, '1797561487', 'INVOICE', '2019-06-29', '303', 'Sale Income For Walking customer', '0.00', '36000.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1'),
(193, '1797561487', 'INV', '2019-06-29', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '36000.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1'),
(194, '1797561487', 'INV', '2019-06-29', '1020101', 'Cash in Hand in Sale for Walking customer', '36000.00', '0.00', '1', '1', '2019-06-29 11:45:22', NULL, NULL, '1'),
(195, '20190629115435', 'Purchase', '2019-06-29', '10107', 'Inventory Debit For Supplier Test Supplier', '20000.00', '0.00', '1', '1', '2019-06-29 11:54:35', NULL, NULL, '1'),
(196, '20190629115435', 'Purchase', '2019-06-29', '5020200001', 'Supplier .Test Supplier', '0.00', '20000.00', '1', '1', '2019-06-29 00:00:00', NULL, NULL, '1'),
(197, '20190629115435', 'Purchase', '2019-06-29', '402', 'Company Credit For  Test Supplier', '20000.00', '0.00', '1', '1', '2019-06-29 11:54:35', NULL, NULL, '1'),
(198, '20190629115435', 'Purchase', '2019-06-29', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '20000.00', '1', '1', '2019-06-29 11:54:35', NULL, NULL, '1'),
(199, '20190629115435', 'Purchase', '2019-06-29', '5020200001', 'Supplier .Test Supplier', '20000.00', '0.00', '1', '1', '2019-06-29 00:00:00', NULL, NULL, '1'),
(200, '9438755655', 'INV', '2019-06-30', '10107', 'Inventory credit For Invoice No9438755655', '0.00', '30000.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1'),
(201, '9438755655', 'INV', '2019-06-30', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1'),
(202, '9438755655', 'INVOICE', '2019-06-30', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1'),
(203, '9438755655', 'INV', '2019-06-30', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1'),
(204, '9438755655', 'INV', '2019-06-30', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-06-30 12:00:12', NULL, NULL, '1'),
(205, '5919433886', 'INV', '2019-06-30', '10107', 'Inventory credit For Invoice No5919433886', '0.00', '30000.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1'),
(206, '5919433886', 'INV', '2019-06-30', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1'),
(207, '5919433886', 'INVOICE', '2019-06-30', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1'),
(208, '5919433886', 'INV', '2019-06-30', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1'),
(209, '5919433886', 'INV', '2019-06-30', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-06-30 12:00:44', NULL, NULL, '1'),
(210, '2875718763', 'INV', '2019-07-01', '10107', 'Inventory credit For Invoice No2875718763', '0.00', '150.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1'),
(211, '2875718763', 'INV', '2019-07-01', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1'),
(212, '2875718763', 'INVOICE', '2019-07-01', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1'),
(213, '2875718763', 'INV', '2019-07-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1'),
(214, '2875718763', 'INV', '2019-07-01', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-07-01 06:14:50', NULL, NULL, '1'),
(215, '20190701062243', 'Expense', '2019-07-01', '407', 'honda-22552500 Expense 20190701062243', '350.00', '0.00', '1', '1', '2019-07-01 06:22:43', NULL, NULL, '1'),
(216, '20190701062243', 'Expense', '2019-07-01', '1020101', 'honda-22552500 Expense20190701062243', '0.00', '350.00', '1', '1', '2019-07-01 06:22:43', NULL, NULL, '1'),
(217, '20190701062405', 'Expense', '2019-07-01', '407', 'honda-22552500 Expense 20190701062405', '500.00', '0.00', '1', '1', '2019-07-01 06:24:05', NULL, NULL, '1'),
(218, '20190701062405', 'Expense', '2019-07-01', '1020101', 'honda-22552500 Expense20190701062405', '0.00', '500.00', '1', '1', '2019-07-01 06:24:05', NULL, NULL, '1'),
(219, '1749144575', 'INV', '2019-07-02', '10107', 'Inventory credit For Invoice No1749144575', '0.00', '150.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1'),
(220, '1749144575', 'INV', '2019-07-02', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1'),
(221, '1749144575', 'INVOICE', '2019-07-02', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1'),
(222, '1749144575', 'INV', '2019-07-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '200.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1'),
(223, '1749144575', 'INV', '2019-07-02', '1020101', 'Cash in Hand in Sale for Walking customer', '200.00', '0.00', '1', '1', '2019-07-02 05:31:13', NULL, NULL, '1'),
(224, '9815582964', 'INV', '2019-07-02', '10107', 'Inventory credit For Invoice No9815582964', '0.00', '30000.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1'),
(225, '9815582964', 'INV', '2019-07-02', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1'),
(226, '9815582964', 'INVOICE', '2019-07-02', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1'),
(227, '9815582964', 'INV', '2019-07-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1'),
(228, '9815582964', 'INV', '2019-07-02', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-07-02 05:32:17', NULL, NULL, '1'),
(229, '8493678614', 'INV', '2019-07-02', '10107', 'Inventory credit For Invoice No8493678614', '0.00', '350.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1'),
(230, '8493678614', 'INV', '2019-07-02', '102030134', 'Customer debit For  Walking customer', '455.00', '0.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1'),
(231, '8493678614', 'INVOICE', '2019-07-02', '303', 'Sale Income For Walking customer', '0.00', '455.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1'),
(232, '8493678614', 'INV', '2019-07-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '455.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1'),
(233, '8493678614', 'INV', '2019-07-02', '1020101', 'Cash in Hand in Sale for Walking customer', '455.00', '0.00', '1', '1', '2019-07-02 05:38:08', NULL, NULL, '1'),
(234, '9691144514', 'INV', '2019-07-31', '10107', 'Inventory credit For Invoice No9691144514', '0.00', '390.00', '1', '1', '2019-07-31 11:38:51', NULL, NULL, '1'),
(235, '9691144514', 'INV', '2019-07-31', '102030134', 'Customer debit For  Walking customer', '800.00', '0.00', '1', '1', '2019-07-31 11:38:51', NULL, NULL, '1'),
(236, '9691144514', 'INVOICE', '2019-07-31', '303', 'Sale Income For Walking customer', '0.00', '800.00', '1', '1', '2019-07-31 11:38:51', NULL, NULL, '1'),
(237, '20190731114458', 'Purchase', '2019-07-31', '10107', 'Inventory Debit For Supplier global band ', '0.00', '0.00', '1', '1', '2019-07-31 11:44:58', NULL, NULL, '1'),
(238, '20190731114458', 'Purchase', '2019-07-31', '5020200002', 'Supplier .global band ', '0.00', '0.00', '1', '1', '2019-07-31 00:00:00', NULL, NULL, '1'),
(239, '20190731114458', 'Purchase', '2019-07-31', '402', 'Company Credit For  global band ', '0.00', '0.00', '1', '1', '2019-07-31 11:44:58', NULL, NULL, '1'),
(240, '20190731114458', 'Purchase', '2019-07-31', '1020101', 'Cash in Hand For Supplier global band ', '0.00', '0.00', '1', '1', '2019-07-31 11:44:58', NULL, NULL, '1'),
(241, '20190731114458', 'Purchase', '2019-07-31', '5020200002', 'Supplier .global band ', '0.00', '0.00', '1', '1', '2019-07-31 00:00:00', NULL, NULL, '1'),
(242, '20190731114746', 'Purchase', '2019-07-31', '10107', 'Inventory Debit For Supplier Test Supplier', '2039100.00', '0.00', '1', '1', '2019-07-31 11:47:46', NULL, NULL, '1'),
(243, '20190731114746', 'Purchase', '2019-07-31', '5020200001', 'Supplier .Test Supplier', '0.00', '2039100.00', '1', '1', '2019-07-31 00:00:00', NULL, NULL, '1'),
(244, '20190731114746', 'Purchase', '2019-07-31', '402', 'Company Credit For  Test Supplier', '2039100.00', '0.00', '1', '1', '2019-07-31 11:47:46', NULL, NULL, '1'),
(245, '20190731114746', 'Purchase', '2019-07-31', '1020101', 'Cash in Hand For Supplier Test Supplier', '0.00', '2039100.00', '1', '1', '2019-07-31 11:47:46', NULL, NULL, '1'),
(246, '20190731114746', 'Purchase', '2019-07-31', '5020200001', 'Supplier .Test Supplier', '2039100.00', '0.00', '1', '1', '2019-07-31 00:00:00', NULL, NULL, '1'),
(247, '5457199982', 'INV', '2019-07-31', '10107', 'Inventory credit For Invoice No5457199982', '0.00', '73997.50', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1'),
(248, '5457199982', 'INV', '2019-07-31', '102030134', 'Customer debit For  Walking customer', '195465.00', '0.00', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1'),
(249, '5457199982', 'INVOICE', '2019-07-31', '303', 'Sale Income For Walking customer', '0.00', '195465.00', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1'),
(250, '5457199982', 'INV', '2019-07-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '195465.00', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1'),
(251, '5457199982', 'INV', '2019-07-31', '1020101', 'Cash in Hand in Sale for Walking customer', '195465.00', '0.00', '1', '1', '2019-07-31 11:50:38', NULL, NULL, '1'),
(252, '2362693843', 'INV', '2019-07-31', '10107', 'Inventory credit For Invoice No2362693843', '0.00', '30000.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1'),
(253, '2362693843', 'INV', '2019-07-31', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1'),
(254, '2362693843', 'INVOICE', '2019-07-31', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1'),
(255, '2362693843', 'INV', '2019-07-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1'),
(256, '2362693843', 'INV', '2019-07-31', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-07-31 13:24:11', NULL, NULL, '1'),
(262, '5763389422', 'INV', '2019-07-31', '10107', 'Inventory credit For Invoice No5763389422', '0.00', '352.50', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1'),
(263, '5763389422', 'INVOICE', '2019-07-31', '303', 'Sale Income From Walking customer', '0.00', '455.00', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1'),
(264, '5763389422', 'INV', '2019-07-31', '102030134', 'Customer debit For Walking customer', '455.00', '0.00', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1'),
(265, '5763389422', 'INV', '2019-07-31', '102030134', 'Customer credit for Paid Amount For Walking customer', '0.00', '455.00', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1'),
(266, '5763389422', 'INV', '2019-07-31', '1020101', 'Cash in Hand for sale for Walking customer', '455.00', '0.00', '1', '1', '2019-07-31 13:31:17', NULL, NULL, '1'),
(267, '6921223925', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No6921223925', '0.00', '40352.50', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1'),
(268, '6921223925', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '50455.00', '0.00', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1'),
(269, '6921223925', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '50455.00', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1'),
(270, '6921223925', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50455.00', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1'),
(271, '6921223925', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '50455.00', '0.00', '1', '1', '2019-08-01 04:46:47', NULL, NULL, '1'),
(272, '2992478698', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No2992478698', '0.00', '40502.50', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1'),
(273, '2992478698', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '50655.00', '0.00', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1'),
(274, '2992478698', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '50655.00', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1'),
(275, '2992478698', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50655.00', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1'),
(276, '2992478698', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '50655.00', '0.00', '1', '1', '2019-08-01 04:51:22', NULL, NULL, '1'),
(277, '2429117215', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No2429117215', '0.00', '41502.50', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1'),
(278, '2429117215', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '51855.00', '0.00', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1'),
(279, '2429117215', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '51855.00', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1'),
(280, '2429117215', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '51855.00', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1'),
(281, '2429117215', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '51855.00', '0.00', '1', '1', '2019-08-01 04:58:34', NULL, NULL, '1'),
(282, '8636821614', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No8636821614', '0.00', '70000.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1'),
(283, '8636821614', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '85000.00', '0.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1'),
(284, '8636821614', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '85000.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1'),
(285, '8636821614', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '85000.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1'),
(286, '8636821614', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '85000.00', '0.00', '1', '1', '2019-08-01 05:05:19', NULL, NULL, '1'),
(287, '2274125738', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No2274125738', '0.00', '40000.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1'),
(288, '2274125738', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '50000.00', '0.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1'),
(289, '2274125738', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '50000.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1'),
(290, '2274125738', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50000.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1'),
(291, '2274125738', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '50000.00', '0.00', '1', '1', '2019-08-01 06:12:13', NULL, NULL, '1'),
(292, 'PM-2', 'PM', '2019-08-01', '5020200001', 'Paid to Test Supplier', '200.00', '0.00', '1', '1', '2019-08-01 07:25:20', NULL, NULL, '1'),
(293, 'PM-2', 'PM', '2019-08-01', '1020101', 'Paid to Test Supplier', '0.00', '200.00', '1', '1', '2019-08-01 07:25:20', NULL, NULL, '1'),
(294, '6881727828', 'INV', '2019-08-01', '10107', 'Inventory credit For Invoice No6881727828', '0.00', '352.50', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1'),
(295, '6881727828', 'INV', '2019-08-01', '102030134', 'Customer debit For  Walking customer', '455.00', '0.00', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1'),
(296, '6881727828', 'INVOICE', '2019-08-01', '303', 'Sale Income For Walking customer', '0.00', '455.00', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1'),
(297, '6881727828', 'INV', '2019-08-01', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '455.00', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1'),
(298, '6881727828', 'INV', '2019-08-01', '1020101', 'Cash in Hand in Sale for Walking customer', '455.00', '0.00', '1', '1', '2019-08-01 11:46:10', NULL, NULL, '1'),
(299, 'WCAX81XD3R', 'Advance', '2019-08-03', '5020200001', 'supplier Advance For Test Supplier', '1000.00', '0.00', '1', NULL, '2019-08-03 06:08:24', NULL, NULL, '1'),
(300, 'WCAX81XD3R', 'Advance', '2019-08-03', '1020101', 'Cash in Hand  For Test Supplier Advance', '1000.00', '0.00', '1', NULL, '2019-08-03 06:08:24', NULL, NULL, '1'),
(301, 'LRRQ4L5FO1', 'PR Balance', '2019-08-03', '10107', 'Inventory credit For  ovi', '10.00', '0.00', '1', NULL, '2019-08-03 10:53:46', NULL, NULL, '1'),
(302, 'Z9IYRG1MRI', 'PR Balance', '2019-08-03', '10107', 'Inventory credit For  rttt', '1.00', '0.00', '1', NULL, '2019-08-03 10:54:54', NULL, NULL, '1'),
(303, '6942233633', 'INV', '2019-08-03', '10107', 'Inventory credit For Invoice No6942233633', '0.00', '150.00', '1', '1', '2019-08-03 11:23:56', NULL, NULL, '1'),
(304, '6942233633', 'INV', '2019-08-03', '102030134', 'Customer debit For  Walking customer', '200.00', '0.00', '1', '1', '2019-08-03 11:23:56', NULL, NULL, '1'),
(305, '6942233633', 'INVOICE', '2019-08-03', '303', 'Sale Income For Walking customer', '0.00', '200.00', '1', '1', '2019-08-03 11:23:56', NULL, NULL, '1'),
(306, '7167684116', 'INV', '2019-08-03', '10107', 'Inventory credit For Invoice No7167684116', '0.00', '2000.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1'),
(307, '7167684116', 'INV', '2019-08-03', '102030134', 'Customer debit For  Walking customer', '2600.00', '0.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1'),
(308, '7167684116', 'INVOICE', '2019-08-03', '303', 'Sale Income For Walking customer', '0.00', '2600.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1'),
(309, '7167684116', 'INV', '2019-08-03', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '2600.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES
(310, '7167684116', 'INV', '2019-08-03', '1020101', 'Cash in Hand in Sale for Walking customer', '2600.00', '0.00', '1', '1', '2019-08-03 11:30:17', NULL, NULL, '1'),
(311, '20190804092551', 'Purchase', '2019-08-04', '10107', 'Inventory Debit For Supplier test', '400.00', '0.00', '1', '1', '2019-08-04 09:25:51', NULL, NULL, '1'),
(312, '20190804092551', 'Purchase', '2019-08-04', '5020200003', 'Supplier .test', '0.00', '400.00', '1', '1', '2019-08-04 00:00:00', NULL, NULL, '1'),
(313, '20190804092551', 'Purchase', '2019-08-04', '402', 'Company Credit For  test', '400.00', '0.00', '1', '1', '2019-08-04 09:25:51', NULL, NULL, '1'),
(314, '20190804092551', 'Purchase', '2019-08-04', '1020101', 'Cash in Hand For Supplier test', '0.00', '400.00', '1', '1', '2019-08-04 09:25:51', NULL, NULL, '1'),
(315, '20190804092551', 'Purchase', '2019-08-04', '5020200003', 'Supplier .test', '400.00', '0.00', '1', '1', '2019-08-04 00:00:00', NULL, NULL, '1'),
(316, 'Z243GXXO9F', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For sdasds Advance', '0.00', '0.00', '1', NULL, '2019-08-06 07:05:42', NULL, NULL, '1'),
(317, 'PNW2N33NCX', 'Advance', '2019-08-06', '5020200003', 'supplier Advance For test', '232.00', '0.00', '1', NULL, '2019-08-06 07:06:45', NULL, NULL, '1'),
(318, 'PNW2N33NCX', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For test Advance', '232.00', '0.00', '1', NULL, '2019-08-06 07:06:45', NULL, NULL, '1'),
(319, 'IF2K2MG62S', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For  Advance', '232.00', '0.00', '1', NULL, '2019-08-06 10:45:59', NULL, NULL, '1'),
(320, '5OMZ3G1GUN', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For  Advance', '232.00', '0.00', '1', NULL, '2019-08-06 10:46:09', NULL, NULL, '1'),
(321, 'L26HEBCZJM', 'Advance', '2019-08-06', '1020101', 'Cash in Hand  For  Advance', '232.00', '0.00', '1', NULL, '2019-08-06 10:46:26', NULL, NULL, '1'),
(322, '20190807091741', 'Purchase', '2019-08-07', '10107', 'Inventory Debit For Supplier fghfg', '3000.00', '0.00', '1', '1', '2019-08-07 09:17:41', NULL, NULL, '1'),
(323, '20190807091741', 'Purchase', '2019-08-07', '402', 'Company Credit For  fghfg', '3000.00', '0.00', '1', '1', '2019-08-07 09:17:41', NULL, NULL, '1'),
(324, '20190807091741', 'Purchase', '2019-08-07', '1020101', 'Cash in Hand For Supplier fghfg', '0.00', '3000.00', '1', '1', '2019-08-07 09:17:41', NULL, NULL, '1'),
(325, '20190807104826', 'Purchase', '2019-08-07', '10107', 'Inventory Debit For Supplier fghfg', '9000.00', '0.00', '1', '1', '2019-08-07 10:48:26', NULL, NULL, '1'),
(326, '20190807104826', 'Purchase', '2019-08-07', '402', 'Company Credit For  fghfg', '9000.00', '0.00', '1', '1', '2019-08-07 10:48:26', NULL, NULL, '1'),
(327, '20190807104826', 'Purchase', '2019-08-07', '1020101', 'Cash in Hand For Supplier fghfg', '0.00', '9000.00', '1', '1', '2019-08-07 10:48:26', NULL, NULL, '1'),
(328, '2PSD6PZRDY', 'Advance', '2019-08-07', '1020101', 'Cash in Hand  For fghfg Advance', '5099.00', '0.00', '1', NULL, '2019-08-07 15:41:54', NULL, NULL, '1'),
(329, 'MBJEQBLEDT', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoMBJEQBLEDT', '0.00', '150.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1'),
(330, 'MBJEQBLEDT', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1'),
(331, 'MBJEQBLEDT', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1'),
(332, 'MBJEQBLEDT', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1'),
(333, 'MBJEQBLEDT', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 08:35:31', NULL, NULL, '1'),
(334, '3OXFJU9FLE', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice No3OXFJU9FLE', '0.00', '150.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1'),
(335, '3OXFJU9FLE', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1'),
(336, '3OXFJU9FLE', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1'),
(337, '3OXFJU9FLE', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1'),
(338, '3OXFJU9FLE', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 08:38:33', NULL, NULL, '1'),
(339, 'AEYS0XT6FB', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoAEYS0XT6FB', '0.00', '150.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1'),
(340, 'AEYS0XT6FB', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '150.00', '0.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1'),
(341, 'AEYS0XT6FB', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '150.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1'),
(342, 'AEYS0XT6FB', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1'),
(343, 'AEYS0XT6FB', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 08:57:53', NULL, NULL, '1'),
(344, 'IFU2EJUMSR', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoIFU2EJUMSR', '0.00', '150.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1'),
(345, 'IFU2EJUMSR', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1'),
(346, 'IFU2EJUMSR', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1'),
(347, 'IFU2EJUMSR', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1'),
(348, 'IFU2EJUMSR', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:01:43', NULL, NULL, '1'),
(349, 'TD6SQ1IDPR', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoTD6SQ1IDPR', '0.00', '150.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1'),
(350, 'TD6SQ1IDPR', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1'),
(351, 'TD6SQ1IDPR', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1'),
(352, 'TD6SQ1IDPR', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1'),
(353, 'TD6SQ1IDPR', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:02:54', NULL, NULL, '1'),
(354, 'B8P4EGGRAY', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoB8P4EGGRAY', '0.00', '150.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1'),
(355, 'B8P4EGGRAY', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1'),
(356, 'B8P4EGGRAY', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1'),
(357, 'B8P4EGGRAY', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1'),
(358, 'B8P4EGGRAY', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:04:08', NULL, NULL, '1'),
(359, 'R8OMFVYJXQ', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoR8OMFVYJXQ', '0.00', '150.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1'),
(360, 'R8OMFVYJXQ', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1'),
(361, 'R8OMFVYJXQ', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1'),
(362, 'R8OMFVYJXQ', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1'),
(363, 'R8OMFVYJXQ', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:05:03', NULL, NULL, '1'),
(364, 'VYMH47RI7Y', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoVYMH47RI7Y', '0.00', '150.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1'),
(365, 'VYMH47RI7Y', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '150.00', '0.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1'),
(366, 'VYMH47RI7Y', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '150.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1'),
(367, 'VYMH47RI7Y', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1'),
(368, 'VYMH47RI7Y', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:05:34', NULL, NULL, '1'),
(369, '3FQSMSSQDT', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice No3FQSMSSQDT', '0.00', '150.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1'),
(370, '3FQSMSSQDT', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1'),
(371, '3FQSMSSQDT', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1'),
(372, '3FQSMSSQDT', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1'),
(373, '3FQSMSSQDT', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:08:08', NULL, NULL, '1'),
(374, 'JTXNMIW0DT', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoJTXNMIW0DT', '0.00', '150.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1'),
(375, 'JTXNMIW0DT', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1'),
(376, 'JTXNMIW0DT', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1'),
(377, 'JTXNMIW0DT', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1'),
(378, 'JTXNMIW0DT', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:09:20', NULL, NULL, '1'),
(379, 'TJSXGFD1F6', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoTJSXGFD1F6', '0.00', '150.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1'),
(380, 'TJSXGFD1F6', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1'),
(381, 'TJSXGFD1F6', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1'),
(382, 'TJSXGFD1F6', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1'),
(383, 'TJSXGFD1F6', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:11:01', NULL, NULL, '1'),
(384, '1AGGWW2DR6', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice No1AGGWW2DR6', '0.00', '150.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1'),
(385, '1AGGWW2DR6', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1'),
(386, '1AGGWW2DR6', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1'),
(387, '1AGGWW2DR6', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1'),
(388, '1AGGWW2DR6', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:11:40', NULL, NULL, '1'),
(389, 'FO9Q0CZBAH', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoFO9Q0CZBAH', '0.00', '150.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1'),
(390, 'FO9Q0CZBAH', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1'),
(391, 'FO9Q0CZBAH', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1'),
(392, 'FO9Q0CZBAH', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1'),
(393, 'FO9Q0CZBAH', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:18:23', NULL, NULL, '1'),
(394, 'CYN1QFPGZE', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoCYN1QFPGZE', '0.00', '150.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1'),
(395, 'CYN1QFPGZE', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1'),
(396, 'CYN1QFPGZE', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1'),
(397, 'CYN1QFPGZE', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1'),
(398, 'CYN1QFPGZE', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', '1', '2019-08-27 09:22:27', NULL, NULL, '1'),
(399, 'QE2YQFXLDA', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoQE2YQFXLDA', '0.00', '0.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1'),
(400, 'QE2YQFXLDA', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1'),
(401, 'QE2YQFXLDA', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1'),
(402, 'QE2YQFXLDA', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1'),
(403, 'QE2YQFXLDA', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', NULL, '2019-08-27 09:57:40', NULL, NULL, '1'),
(404, 'JJ75B2XUKG', 'INV', '2019-08-27', '10107', 'Inventory credit For Invoice NoJJ75B2XUKG', '0.00', '150.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1'),
(405, 'JJ75B2XUKG', 'INV', '2019-08-27', '102030140', 'Customer debit For  Hm Isahaq', '100.00', '0.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1'),
(406, 'JJ75B2XUKG', 'INVOICE', '2019-08-27', '303', 'Sale Income For Hm Isahaq', '0.00', '100.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1'),
(407, 'JJ75B2XUKG', 'INV', '2019-08-27', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '50.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1'),
(408, 'JJ75B2XUKG', 'INV', '2019-08-27', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '50.00', '0.00', '1', NULL, '2019-08-27 13:52:16', NULL, NULL, '1'),
(409, 'ZNOOWPHIKK', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoZNOOWPHIKK', '0.00', '300.00', '1', '1', '2019-08-31 05:39:13', NULL, NULL, '1'),
(410, 'ZNOOWPHIKK', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:39:13', NULL, NULL, '1'),
(411, 'ZNOOWPHIKK', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:39:13', NULL, NULL, '1'),
(412, 'KYNKY2F1EI', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoKYNKY2F1EI', '0.00', '300.00', '1', '1', '2019-08-31 05:39:26', NULL, NULL, '1'),
(413, 'KYNKY2F1EI', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:39:26', NULL, NULL, '1'),
(414, 'KYNKY2F1EI', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:39:26', NULL, NULL, '1'),
(415, 'R6BFIRTLMP', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoR6BFIRTLMP', '0.00', '0.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1'),
(416, 'R6BFIRTLMP', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1'),
(417, 'R6BFIRTLMP', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1'),
(418, 'R6BFIRTLMP', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '150.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1'),
(419, 'R6BFIRTLMP', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '150.00', '0.00', '1', '1', '2019-08-31 05:41:40', NULL, NULL, '1'),
(420, 'HUJOSOYQPV', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoHUJOSOYQPV', '0.00', '0.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1'),
(421, 'HUJOSOYQPV', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1'),
(422, 'HUJOSOYQPV', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1'),
(423, 'HUJOSOYQPV', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '150.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1'),
(424, 'HUJOSOYQPV', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '150.00', '0.00', '1', '1', '2019-08-31 05:43:00', NULL, NULL, '1'),
(425, 'SOQYFD5DVR', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoSOQYFD5DVR', '0.00', '0.00', '1', '1', '2019-08-31 05:48:59', NULL, NULL, '1'),
(426, 'SOQYFD5DVR', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:48:59', NULL, NULL, '1'),
(427, 'SOQYFD5DVR', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:48:59', NULL, NULL, '1'),
(428, 'HBW2WPWYHE', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoHBW2WPWYHE', '0.00', '0.00', '1', '1', '2019-08-31 05:49:17', NULL, NULL, '1'),
(429, 'HBW2WPWYHE', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:49:17', NULL, NULL, '1'),
(430, 'HBW2WPWYHE', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:49:17', NULL, NULL, '1'),
(431, 'EMRLGS5W57', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoEMRLGS5W57', '0.00', '0.00', '1', '1', '2019-08-31 05:51:40', NULL, NULL, '1'),
(432, 'EMRLGS5W57', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:51:40', NULL, NULL, '1'),
(433, 'EMRLGS5W57', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:51:40', NULL, NULL, '1'),
(434, 'CEZC44VKAW', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoCEZC44VKAW', '0.00', '0.00', '1', '1', '2019-08-31 05:52:07', NULL, NULL, '1'),
(435, 'CEZC44VKAW', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:52:07', NULL, NULL, '1'),
(436, 'CEZC44VKAW', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:52:07', NULL, NULL, '1'),
(437, 'RWKKAW90BR', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoRWKKAW90BR', '0.00', '0.00', '1', '1', '2019-08-31 05:53:24', NULL, NULL, '1'),
(438, 'RWKKAW90BR', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:53:24', NULL, NULL, '1'),
(439, 'RWKKAW90BR', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:53:24', NULL, NULL, '1'),
(440, 'NDQSPVYGQR', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoNDQSPVYGQR', '0.00', '0.00', '1', '1', '2019-08-31 05:53:36', NULL, NULL, '1'),
(441, 'NDQSPVYGQR', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 05:53:36', NULL, NULL, '1'),
(442, 'NDQSPVYGQR', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 05:53:36', NULL, NULL, '1'),
(443, 'WPOGYFR2EJ', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoWPOGYFR2EJ', '0.00', '0.00', '1', '1', '2019-08-31 07:11:01', NULL, NULL, '1'),
(444, 'WPOGYFR2EJ', 'INVOICE', '2019-08-31', '303', 'Sale Income For ', '0.00', '35457568.00', '1', '1', '2019-08-31 07:11:01', NULL, NULL, '1'),
(445, 'WPOGYFR2EJ', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for ', '345345.00', '0.00', '1', '1', '2019-08-31 07:11:01', NULL, NULL, '1'),
(446, 'GHKCY7BNB8', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoGHKCY7BNB8', '0.00', '0.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1'),
(447, 'GHKCY7BNB8', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '1000.00', '0.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1'),
(448, 'GHKCY7BNB8', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '1000.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1'),
(449, 'GHKCY7BNB8', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1'),
(450, 'GHKCY7BNB8', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-08-31 07:30:13', NULL, NULL, '1'),
(451, '7FE92ZSJVO', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No7FE92ZSJVO', '0.00', '0.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1'),
(452, '7FE92ZSJVO', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1'),
(453, '7FE92ZSJVO', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1'),
(454, '7FE92ZSJVO', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '150.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1'),
(455, '7FE92ZSJVO', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '150.00', '0.00', '1', '1', '2019-08-31 08:30:51', NULL, NULL, '1'),
(456, 'ETAJFQYI61', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoETAJFQYI61', '0.00', '0.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1'),
(457, 'ETAJFQYI61', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1'),
(458, 'ETAJFQYI61', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1'),
(459, 'ETAJFQYI61', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1'),
(460, 'ETAJFQYI61', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:36:52', NULL, NULL, '1'),
(461, '4GGW3BKYYX', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No4GGW3BKYYX', '0.00', '0.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1'),
(462, '4GGW3BKYYX', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1'),
(463, '4GGW3BKYYX', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1'),
(464, '4GGW3BKYYX', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1'),
(465, '4GGW3BKYYX', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:37:42', NULL, NULL, '1'),
(466, 'IMXKKV3CWH', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoIMXKKV3CWH', '0.00', '0.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1'),
(467, 'IMXKKV3CWH', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1'),
(468, 'IMXKKV3CWH', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1'),
(469, 'IMXKKV3CWH', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1'),
(470, 'IMXKKV3CWH', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:39:16', NULL, NULL, '1'),
(471, 'OBRAR2MS1B', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoOBRAR2MS1B', '0.00', '0.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1'),
(472, 'OBRAR2MS1B', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1'),
(473, 'OBRAR2MS1B', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1'),
(474, 'OBRAR2MS1B', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1'),
(475, 'OBRAR2MS1B', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:57:39', NULL, NULL, '1'),
(476, 'LKHINKOQEZ', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoLKHINKOQEZ', '0.00', '0.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1'),
(477, 'LKHINKOQEZ', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1'),
(478, 'LKHINKOQEZ', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1'),
(479, 'LKHINKOQEZ', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1'),
(480, 'LKHINKOQEZ', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:58:07', NULL, NULL, '1'),
(481, 'K5ZUAPRFM3', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoK5ZUAPRFM3', '0.00', '0.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1'),
(482, 'K5ZUAPRFM3', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1'),
(483, 'K5ZUAPRFM3', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1'),
(484, 'K5ZUAPRFM3', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1'),
(485, 'K5ZUAPRFM3', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 08:59:01', NULL, NULL, '1'),
(486, 'ZVEQPUB12A', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoZVEQPUB12A', '0.00', '780.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1'),
(487, 'ZVEQPUB12A', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1'),
(488, 'ZVEQPUB12A', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1'),
(489, 'ZVEQPUB12A', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1'),
(490, 'ZVEQPUB12A', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:04:17', NULL, NULL, '1'),
(491, '7NOYHECZKX', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No7NOYHECZKX', '0.00', '0.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1'),
(492, '7NOYHECZKX', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1'),
(493, '7NOYHECZKX', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1'),
(494, '7NOYHECZKX', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1'),
(495, '7NOYHECZKX', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-08-31 09:14:12', NULL, NULL, '1'),
(496, 'LULH5EJTKL', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoLULH5EJTKL', '0.00', '0.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1'),
(497, 'LULH5EJTKL', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1'),
(498, 'LULH5EJTKL', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1'),
(499, 'LULH5EJTKL', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1'),
(500, 'LULH5EJTKL', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:15:07', NULL, NULL, '1'),
(501, 'PGJOTFJMBS', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoPGJOTFJMBS', '0.00', '780.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1'),
(502, 'PGJOTFJMBS', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1'),
(503, 'PGJOTFJMBS', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1'),
(504, 'PGJOTFJMBS', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1'),
(505, 'PGJOTFJMBS', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:15:55', NULL, NULL, '1'),
(506, 'G52D4TNFF9', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoG52D4TNFF9', '0.00', '0.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1'),
(507, 'G52D4TNFF9', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '1000.00', '0.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1'),
(508, 'G52D4TNFF9', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '1000.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1'),
(509, 'G52D4TNFF9', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1'),
(510, 'G52D4TNFF9', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:19:26', NULL, NULL, '1'),
(511, '4CP6Q9TXQE', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No4CP6Q9TXQE', '0.00', '0.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1'),
(512, '4CP6Q9TXQE', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1'),
(513, '4CP6Q9TXQE', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1'),
(514, '4CP6Q9TXQE', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1'),
(515, '4CP6Q9TXQE', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:27:41', NULL, NULL, '1'),
(516, 'LRYVFDX4LA', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoLRYVFDX4LA', '0.00', '780.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1'),
(517, 'LRYVFDX4LA', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1'),
(518, 'LRYVFDX4LA', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1'),
(519, 'LRYVFDX4LA', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1'),
(520, 'LRYVFDX4LA', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:32:53', NULL, NULL, '1'),
(521, 'VHASYKN18J', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoVHASYKN18J', '0.00', '780.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1'),
(522, 'VHASYKN18J', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1'),
(523, 'VHASYKN18J', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1'),
(524, 'VHASYKN18J', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1'),
(525, 'VHASYKN18J', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:33:46', NULL, NULL, '1'),
(526, 'QV7GKHPOAT', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoQV7GKHPOAT', '0.00', '780.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1'),
(527, 'QV7GKHPOAT', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1'),
(528, 'QV7GKHPOAT', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1'),
(529, 'QV7GKHPOAT', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1'),
(530, 'QV7GKHPOAT', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 09:34:08', NULL, NULL, '1'),
(531, 'TTBSFBYWQS', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoTTBSFBYWQS', '0.00', '0.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1'),
(532, 'TTBSFBYWQS', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1'),
(533, 'TTBSFBYWQS', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1'),
(534, 'TTBSFBYWQS', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '15.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1'),
(535, 'TTBSFBYWQS', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '15.00', '0.00', '1', '1', '2019-08-31 09:40:38', NULL, NULL, '1'),
(536, 'D3LGRQ7BOJ', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice NoD3LGRQ7BOJ', '0.00', '68015.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1'),
(537, 'D3LGRQ7BOJ', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '100.00', '0.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1'),
(538, 'D3LGRQ7BOJ', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '100.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1'),
(539, 'D3LGRQ7BOJ', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '50.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1'),
(540, 'D3LGRQ7BOJ', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '50.00', '0.00', '1', '1', '2019-08-31 10:14:10', NULL, NULL, '1'),
(541, '9EIQL1SSY3', 'INV', '2019-08-31', '10107', 'Inventory credit For Invoice No9EIQL1SSY3', '0.00', '67515.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1'),
(542, '9EIQL1SSY3', 'INV', '2019-08-31', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1'),
(543, '9EIQL1SSY3', 'INVOICE', '2019-08-31', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1'),
(544, '9EIQL1SSY3', 'INV', '2019-08-31', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1'),
(545, '9EIQL1SSY3', 'INV', '2019-08-31', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-08-31 10:15:16', NULL, NULL, '1'),
(546, 'YIBCDHM1DF', 'PR Balance', '2019-08-31', '10107', 'Inventory credit For  ovi', '100.00', '0.00', '1', NULL, '2019-08-31 12:16:20', NULL, NULL, '1'),
(547, 'AHLH2FBCER', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoAHLH2FBCER', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1'),
(548, 'AHLH2FBCER', 'INV', '2019-09-02', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1'),
(549, 'AHLH2FBCER', 'INVOICE', '2019-09-02', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1'),
(550, 'AHLH2FBCER', 'INV', '2019-09-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1'),
(551, 'AHLH2FBCER', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:02', NULL, NULL, '1'),
(552, 'L48BYLQOYA', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoL48BYLQOYA', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1'),
(553, 'L48BYLQOYA', 'INV', '2019-09-02', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1'),
(554, 'L48BYLQOYA', 'INVOICE', '2019-09-02', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1'),
(555, 'L48BYLQOYA', 'INV', '2019-09-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1'),
(556, 'L48BYLQOYA', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:03', NULL, NULL, '1'),
(557, 'TKVN9MS0S8', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoTKVN9MS0S8', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1'),
(558, 'TKVN9MS0S8', 'INV', '2019-09-02', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1'),
(559, 'TKVN9MS0S8', 'INVOICE', '2019-09-02', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1'),
(560, 'TKVN9MS0S8', 'INV', '2019-09-02', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1'),
(561, 'TKVN9MS0S8', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for Walking customer', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:37:27', NULL, NULL, '1'),
(562, 'XN5CZSOQT1', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoXN5CZSOQT1', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:40:55', NULL, NULL, '1'),
(563, 'XN5CZSOQT1', 'INVOICE', '2019-09-02', '303', 'Sale Income For kobir', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:40:55', NULL, NULL, '1'),
(564, 'XN5CZSOQT1', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for kobir', '35080.00', '0.00', '1', 'erterte', '2019-09-02 10:40:55', NULL, NULL, '1'),
(565, 'WIJXKFBHB2', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoWIJXKFBHB2', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:40:57', NULL, NULL, '1'),
(566, 'WIJXKFBHB2', 'INVOICE', '2019-09-02', '303', 'Sale Income For kobir', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:40:57', NULL, NULL, '1'),
(567, 'WIJXKFBHB2', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for kobir', '35080.00', '0.00', '1', 'erterte', '2019-09-02 10:40:57', NULL, NULL, '1'),
(568, 'VSFEVF9CKV', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoVSFEVF9CKV', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:41:01', NULL, NULL, '1'),
(569, 'VSFEVF9CKV', 'INVOICE', '2019-09-02', '303', 'Sale Income For kobir', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:41:01', NULL, NULL, '1'),
(570, 'VSFEVF9CKV', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for kobir', '35080.00', '0.00', '1', 'erterte', '2019-09-02 10:41:01', NULL, NULL, '1'),
(571, 'JPPFABLKAT', 'INV', '2019-09-02', '10107', 'Inventory credit For Invoice NoJPPFABLKAT', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:41:02', NULL, NULL, '1'),
(572, 'JPPFABLKAT', 'INVOICE', '2019-09-02', '303', 'Sale Income For kobir', '0.00', '0.00', '1', 'erterte', '2019-09-02 10:41:02', NULL, NULL, '1'),
(573, 'JPPFABLKAT', 'INV', '2019-09-02', '1020101', 'Cash in Hand in Sale for kobir', '35080.00', '0.00', '1', 'erterte', '2019-09-02 10:41:02', NULL, NULL, '1'),
(610, '20190903053506', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:35:06', NULL, NULL, '1'),
(611, '20190903053506', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:35:06', NULL, NULL, '1'),
(612, '20190903053506', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:35:06', NULL, NULL, '1'),
(613, '20190903053622', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:36:22', NULL, NULL, '1'),
(614, '20190903053622', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:36:22', NULL, NULL, '1'),
(615, '20190903053622', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:36:22', NULL, NULL, '1'),
(616, '20190903053809', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:38:09', NULL, NULL, '1'),
(617, '20190903053809', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:38:09', NULL, NULL, '1'),
(618, '20190903053809', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:38:09', NULL, NULL, '1'),
(619, '20190903053848', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:38:48', NULL, NULL, '1'),
(620, '20190903053848', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:38:48', NULL, NULL, '1'),
(621, '20190903053848', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:38:48', NULL, NULL, '1'),
(622, '20190903053939', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:39:39', NULL, NULL, '1'),
(623, '20190903053939', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:39:39', NULL, NULL, '1'),
(624, '20190903053939', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:39:39', NULL, NULL, '1'),
(631, '20190903054203', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:42:03', NULL, NULL, '1'),
(632, '20190903054203', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:42:03', NULL, NULL, '1'),
(633, '20190903054203', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:42:03', NULL, NULL, '1'),
(634, '20190903054240', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:42:40', NULL, NULL, '1'),
(635, '20190903054240', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:42:40', NULL, NULL, '1'),
(636, '20190903054240', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:42:40', NULL, NULL, '1'),
(637, '20190903054128', 'Purchase', '2019-09-03', '10107', 'Inventory Devit Supplier ovi', '68.00', '0.00', '1', '1', '2019-09-03 05:45:37', NULL, NULL, '1'),
(638, '20190903054128', 'Purchase', '2019-09-03', '402', 'Company Credit For Supplierovi', '68.00', '0.00', '1', '1', '2019-09-03 05:45:37', NULL, NULL, '1'),
(639, '20190903054128', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '68.00', '1', '1', '2019-09-03 05:45:37', NULL, NULL, '1'),
(640, '20190903054749', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:47:49', NULL, NULL, '1'),
(641, '20190903054749', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:47:49', NULL, NULL, '1'),
(642, '20190903054749', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:47:49', NULL, NULL, '1'),
(643, '20190903055022', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:50:22', NULL, NULL, '1'),
(644, '20190903055022', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:50:22', NULL, NULL, '1'),
(645, '20190903055022', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:50:22', NULL, NULL, '1'),
(646, '20190903055114', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:51:14', NULL, NULL, '1'),
(647, '20190903055114', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:51:14', NULL, NULL, '1'),
(648, '20190903055114', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:51:14', NULL, NULL, '1'),
(649, '20190903055139', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:51:39', NULL, NULL, '1'),
(650, '20190903055139', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '20.00', '0.00', '1', NULL, '2019-09-03 05:51:39', NULL, NULL, '1'),
(651, '20190903055139', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '20.00', '1', NULL, '2019-09-03 05:51:39', NULL, NULL, '1'),
(670, '20190903061451', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '960.00', '0.00', '1', NULL, '2019-09-03 06:14:51', NULL, NULL, '1'),
(671, '20190903061451', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '960.00', '0.00', '1', NULL, '2019-09-03 06:14:51', NULL, NULL, '1'),
(672, '20190903061451', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '960.00', '1', NULL, '2019-09-03 06:14:51', NULL, NULL, '1'),
(676, '20190903094416', 'Purchase', '2019-09-03', '10107', 'Inventory Debit For Supplier ovi', '27303.00', '0.00', '1', NULL, '2019-09-03 09:44:16', NULL, NULL, '1'),
(677, '20190903094416', 'Purchase', '2019-09-03', '402', 'Company Credit For  ovi', '27303.00', '0.00', '1', NULL, '2019-09-03 09:44:16', NULL, NULL, '1'),
(678, '20190903094416', 'Purchase', '2019-09-03', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '27303.00', '1', NULL, '2019-09-03 09:44:16', NULL, NULL, '1'),
(679, '20190904070737', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '30.00', '0.00', '1', NULL, '2019-09-04 07:07:37', NULL, NULL, '1'),
(680, '20190904070737', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '30.00', '0.00', '1', NULL, '2019-09-04 07:07:37', NULL, NULL, '1'),
(681, '20190904070737', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '30.00', '1', NULL, '2019-09-04 07:07:37', NULL, NULL, '1');
INSERT INTO `acc_transaction` (`ID`, `VNo`, `Vtype`, `VDate`, `COAID`, `Narration`, `Debit`, `Credit`, `IsPosted`, `CreateBy`, `CreateDate`, `UpdateBy`, `UpdateDate`, `IsAppove`) VALUES
(682, '20190904071000', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '254.00', '0.00', '1', NULL, '2019-09-04 07:10:00', NULL, NULL, '1'),
(683, '20190904071000', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '254.00', '0.00', '1', NULL, '2019-09-04 07:10:00', NULL, NULL, '1'),
(684, '20190904071000', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '254.00', '1', NULL, '2019-09-04 07:10:00', NULL, NULL, '1'),
(685, '20190904094916', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '60.00', '0.00', '1', NULL, '2019-09-04 09:49:16', NULL, NULL, '1'),
(686, '20190904094916', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '60.00', '0.00', '1', NULL, '2019-09-04 09:49:16', NULL, NULL, '1'),
(687, '20190904094916', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '60.00', '1', NULL, '2019-09-04 09:49:16', NULL, NULL, '1'),
(688, '20190904103141', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '472.00', '0.00', '1', NULL, '2019-09-04 10:31:41', NULL, NULL, '1'),
(689, '20190904103141', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '472.00', '0.00', '1', NULL, '2019-09-04 10:31:41', NULL, NULL, '1'),
(690, '20190904103141', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '472.00', '1', NULL, '2019-09-04 10:31:41', NULL, NULL, '1'),
(691, '20190904103318', 'Purchase', '2019-09-04', '10107', 'Inventory Debit For Supplier ovi', '60.00', '0.00', '1', NULL, '2019-09-04 10:33:18', NULL, NULL, '1'),
(692, '20190904103318', 'Purchase', '2019-09-04', '402', 'Company Credit For  ovi', '60.00', '0.00', '1', NULL, '2019-09-04 10:33:18', NULL, NULL, '1'),
(693, '20190904103318', 'Purchase', '2019-09-04', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '60.00', '1', NULL, '2019-09-04 10:33:18', NULL, NULL, '1'),
(724, '20190904112702', 'Purchase', '2019-09-05', '10107', 'Inventory Debit For Supplier ovi', '1089.00', '0.00', '1', NULL, '2019-09-05 04:30:17', NULL, NULL, '1'),
(725, '20190904112702', 'Purchase', '2019-09-05', '402', 'Company Credit For  ovi', '1089.00', '0.00', '1', NULL, '2019-09-05 04:30:17', NULL, NULL, '1'),
(726, '20190904112702', 'Purchase', '2019-09-05', '1020101', 'Cash in Hand For Supplier ovi', '0.00', '1089.00', '1', NULL, '2019-09-05 04:30:17', NULL, NULL, '1'),
(727, 'TA1UKNEC4K', 'INV', '2019-09-05', '10107', 'Inventory credit For Invoice NoTA1UKNEC4K', '0.00', '0.00', '1', '', '2019-09-05 05:35:13', NULL, NULL, '1'),
(728, 'TA1UKNEC4K', 'INVOICE', '2019-09-05', '303', 'Sale Income For ', '0.00', '0.00', '1', '', '2019-09-05 05:35:13', NULL, NULL, '1'),
(729, 'PM-3', 'PM', '2019-08-05', '5', 'test remarks', '1.00', '0.00', '1', '1', '2019-09-05 06:03:36', NULL, NULL, '1'),
(730, 'PM-3', 'PM', '2019-08-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '1.00', '1', '1', '2019-09-05 06:03:36', NULL, NULL, '1'),
(731, 'PM-3', 'PM', '2019-08-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:05:09', NULL, NULL, '1'),
(732, 'PM-3', 'PM', '2019-08-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:05:09', NULL, NULL, '1'),
(733, 'PM-3', 'PM', '2019-09-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:08:55', NULL, NULL, '1'),
(734, 'PM-3', 'PM', '2019-09-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:08:55', NULL, NULL, '1'),
(735, 'PM-3', 'PM', '2019-09-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:10:34', NULL, NULL, '1'),
(736, 'PM-3', 'PM', '2019-09-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:10:34', NULL, NULL, '1'),
(737, 'PM-3', 'PM', '2019-09-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:17:35', NULL, NULL, '1'),
(738, 'PM-3', 'PM', '2019-09-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:17:35', NULL, NULL, '1'),
(739, 'PM-3', 'PM', '2019-09-05', '5020200005', 'test remarks', '100.00', '0.00', '1', '1', '2019-09-05 06:21:09', NULL, NULL, '1'),
(740, 'PM-3', 'PM', '2019-09-05', '1020101', 'Cash in Hand For Voucher NoPM-3', '0.00', '100.00', '1', '1', '2019-09-05 06:21:09', NULL, NULL, '1'),
(741, 'PM-4', 'PM', '2019-09-05', '5020200005', 'Paid to isahaq', '200.00', '0.00', '1', '1', '2019-09-05 06:59:33', NULL, NULL, '1'),
(742, 'PM-4', 'PM', '2019-09-05', '1020101', 'Paid to isahaq', '0.00', '200.00', '1', '1', '2019-09-05 06:59:33', NULL, NULL, '1'),
(743, 'PM-5', 'PM', '2019-09-05', '5020200004', 'Paid to shahabuddin', '324234.00', '0.00', '1', '1', '2019-09-05 13:43:37', NULL, NULL, '1'),
(744, 'PM-6', 'PM', '2019-09-05', '1020101', 'Paid to shahabuddin', '0.00', '324234.00', '1', '1', '2019-09-05 13:43:37', NULL, NULL, '1'),
(745, '', 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 09:55:54', NULL, NULL, '1'),
(746, '', 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 09:55:54', NULL, NULL, '1'),
(747, '', 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 09:59:44', NULL, NULL, '1'),
(748, '', 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 09:59:44', NULL, NULL, '1'),
(749, '', 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 10:30:06', NULL, NULL, '1'),
(750, '', 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 10:30:06', NULL, NULL, '1'),
(751, '', 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 10:36:12', NULL, NULL, '1'),
(752, '', 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 10:36:12', NULL, NULL, '1'),
(753, NULL, 'PM', NULL, '', '', '0.00', '0.00', '1', '1', '2019-09-15 10:39:33', NULL, NULL, '1'),
(754, NULL, 'PM', NULL, '1020101', 'Cash in Hand For Voucher No', '0.00', '0.00', '1', '1', '2019-09-15 10:39:33', NULL, NULL, '1'),
(755, 'PM-7', 'PM', '2019-09-15', '', '', '0.00', '0.00', '1', '1', '2019-09-15 10:42:02', NULL, NULL, '1'),
(756, 'PM-7', 'PM', '2019-09-15', '1020101', 'Cash in Hand For Voucher NoPM-7', '0.00', '0.00', '1', '1', '2019-09-15 10:42:02', NULL, NULL, '1'),
(757, 'KFHXF3UIWU', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoKFHXF3UIWU', '0.00', '0.00', '1', '', '2019-09-18 05:39:52', NULL, NULL, '1'),
(758, 'KFHXF3UIWU', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:39:52', NULL, NULL, '1'),
(759, 'KFHXF3UIWU', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:39:52', NULL, NULL, '1'),
(760, 'K4GNPYU4A6', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoK4GNPYU4A6', '0.00', '0.00', '1', '', '2019-09-18 05:40:01', NULL, NULL, '1'),
(761, 'K4GNPYU4A6', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:40:01', NULL, NULL, '1'),
(762, 'K4GNPYU4A6', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:40:01', NULL, NULL, '1'),
(763, 'ETZ0PPT6LS', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoETZ0PPT6LS', '0.00', '0.00', '1', '', '2019-09-18 05:50:19', NULL, NULL, '1'),
(764, 'ETZ0PPT6LS', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:50:19', NULL, NULL, '1'),
(765, 'ETZ0PPT6LS', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 05:50:19', NULL, NULL, '1'),
(766, 'RH1VWKB0F4', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoRH1VWKB0F4', '0.00', '0.00', '1', '', '2019-09-18 06:38:46', NULL, NULL, '1'),
(767, 'RH1VWKB0F4', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 06:38:46', NULL, NULL, '1'),
(768, 'RH1VWKB0F4', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 06:38:46', NULL, NULL, '1'),
(769, 'KHMCMEOOXT', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoKHMCMEOOXT', '0.00', '0.00', '1', '', '2019-09-18 07:03:26', NULL, NULL, '1'),
(770, 'KHMCMEOOXT', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:03:26', NULL, NULL, '1'),
(771, 'KHMCMEOOXT', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:03:26', NULL, NULL, '1'),
(772, '1PGIEIHMX8', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice No1PGIEIHMX8', '0.00', '0.00', '1', '', '2019-09-18 07:25:05', NULL, NULL, '1'),
(773, '1PGIEIHMX8', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:25:05', NULL, NULL, '1'),
(774, '1PGIEIHMX8', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:25:05', NULL, NULL, '1'),
(775, 'PFYCGTPW4B', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoPFYCGTPW4B', '0.00', '0.00', '1', '', '2019-09-18 07:27:44', NULL, NULL, '1'),
(776, 'PFYCGTPW4B', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:27:44', NULL, NULL, '1'),
(777, 'PFYCGTPW4B', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:27:44', NULL, NULL, '1'),
(778, 'YNDSZTUPBE', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoYNDSZTUPBE', '0.00', '0.00', '1', '', '2019-09-18 07:31:18', NULL, NULL, '1'),
(779, 'YNDSZTUPBE', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:31:18', NULL, NULL, '1'),
(780, 'YNDSZTUPBE', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:31:18', NULL, NULL, '1'),
(781, '3LDBLLXXXC', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice No3LDBLLXXXC', '0.00', '0.00', '1', '', '2019-09-18 07:42:09', NULL, NULL, '1'),
(782, '3LDBLLXXXC', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:42:09', NULL, NULL, '1'),
(783, '3LDBLLXXXC', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:42:09', NULL, NULL, '1'),
(784, 'P0HMDJWIUP', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoP0HMDJWIUP', '0.00', '0.00', '1', '', '2019-09-18 07:42:46', NULL, NULL, '1'),
(785, 'P0HMDJWIUP', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:42:46', NULL, NULL, '1'),
(786, 'P0HMDJWIUP', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:42:46', NULL, NULL, '1'),
(787, 'KMWPIXOENF', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoKMWPIXOENF', '0.00', '0.00', '1', '', '2019-09-18 07:47:16', NULL, NULL, '1'),
(788, 'KMWPIXOENF', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:47:16', NULL, NULL, '1'),
(789, 'KMWPIXOENF', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 07:47:16', NULL, NULL, '1'),
(790, 'FSRL6Y46ZE', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoFSRL6Y46ZE', '0.00', '0.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1'),
(791, 'FSRL6Y46ZE', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1'),
(792, 'FSRL6Y46ZE', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1'),
(793, 'FSRL6Y46ZE', 'INV', '2019-09-18', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '1200.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1'),
(794, 'FSRL6Y46ZE', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Walking customer', '1200.00', '0.00', '1', '', '2019-09-18 08:23:33', NULL, NULL, '1'),
(795, 'A2YLFCEYN0', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoA2YLFCEYN0', '0.00', '0.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1'),
(796, 'A2YLFCEYN0', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1'),
(797, 'A2YLFCEYN0', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1'),
(798, 'A2YLFCEYN0', 'INV', '2019-09-18', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '210500.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1'),
(799, 'A2YLFCEYN0', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Walking customer', '210500.00', '0.00', '1', '', '2019-09-18 08:41:07', NULL, NULL, '1'),
(800, 'LFREZ3GFUG', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoLFREZ3GFUG', '0.00', '0.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1'),
(801, 'LFREZ3GFUG', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1'),
(802, 'LFREZ3GFUG', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1'),
(803, 'LFREZ3GFUG', 'INV', '2019-09-18', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '71500.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1'),
(804, 'LFREZ3GFUG', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Walking customer', '71500.00', '0.00', '1', '', '2019-09-18 08:42:39', NULL, NULL, '1'),
(805, 'RNOAKIZTU1', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoRNOAKIZTU1', '0.00', '0.00', '1', '', '2019-09-18 13:15:47', NULL, NULL, '1'),
(806, 'RNOAKIZTU1', 'INVOICE', '2019-09-18', '303', 'Sale Income For Isahaq', '0.00', '0.00', '1', '', '2019-09-18 13:15:47', NULL, NULL, '1'),
(807, 'RNOAKIZTU1', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Isahaq', '200.00', '0.00', '1', '', '2019-09-18 13:15:47', NULL, NULL, '1'),
(808, 'MVLCTPNENW', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoMVLCTPNENW', '0.00', '0.00', '1', '1', '2019-09-18 13:22:19', NULL, NULL, '1'),
(809, 'MVLCTPNENW', 'INVOICE', '2019-09-18', '303', 'Sale Income For Isahaq', '0.00', '0.00', '1', '1', '2019-09-18 13:22:19', NULL, NULL, '1'),
(810, 'MVLCTPNENW', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Isahaq', '1000.00', '0.00', '1', '1', '2019-09-18 13:22:19', NULL, NULL, '1'),
(811, 'A0XMF4HKBH', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoA0XMF4HKBH', '0.00', '0.00', '1', '1', '2019-09-18 13:26:41', NULL, NULL, '1'),
(812, 'A0XMF4HKBH', 'INVOICE', '2019-09-18', '303', 'Sale Income For Isahaq', '0.00', '0.00', '1', '1', '2019-09-18 13:26:41', NULL, NULL, '1'),
(813, 'A0XMF4HKBH', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Isahaq', '1500.00', '0.00', '1', '1', '2019-09-18 13:26:41', NULL, NULL, '1'),
(814, 'NO2EV8MWL4', 'INV', '2019-09-18', '10107', 'Inventory credit For Invoice NoNO2EV8MWL4', '0.00', '0.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1'),
(815, 'NO2EV8MWL4', 'INV', '2019-09-18', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1'),
(816, 'NO2EV8MWL4', 'INVOICE', '2019-09-18', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1'),
(817, 'NO2EV8MWL4', 'INV', '2019-09-18', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '1500.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1'),
(818, 'NO2EV8MWL4', 'INV', '2019-09-18', '1020101', 'Cash in Hand in Sale for Walking customer', '1500.00', '0.00', '1', '1', '2019-09-18 13:31:06', NULL, NULL, '1'),
(819, 'KAKERT7VJI', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice NoKAKERT7VJI', '0.00', '0.00', '1', '', '2019-09-19 04:20:47', NULL, NULL, '1'),
(820, 'KAKERT7VJI', 'INVOICE', '2019-09-19', '303', 'Sale Income For Isahaq', '0.00', '0.00', '1', '', '2019-09-19 04:20:47', NULL, NULL, '1'),
(821, 'KAKERT7VJI', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Isahaq', '400.00', '0.00', '1', '', '2019-09-19 04:20:47', NULL, NULL, '1'),
(822, 'MSSLK5UBVB', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice NoMSSLK5UBVB', '0.00', '34.15', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1'),
(823, 'MSSLK5UBVB', 'INV', '2019-09-19', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1'),
(824, 'MSSLK5UBVB', 'INVOICE', '2019-09-19', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1'),
(825, 'MSSLK5UBVB', 'INV', '2019-09-19', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '355.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1'),
(826, 'MSSLK5UBVB', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Walking customer', '355.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:24:51', NULL, NULL, '1'),
(827, 'EEAK1E4KLQ', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice NoEEAK1E4KLQ', '0.00', '159.33', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1'),
(828, 'EEAK1E4KLQ', 'INV', '2019-09-19', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1'),
(829, 'EEAK1E4KLQ', 'INVOICE', '2019-09-19', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1'),
(830, 'EEAK1E4KLQ', 'INV', '2019-09-19', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1'),
(831, 'EEAK1E4KLQ', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:26:09', NULL, NULL, '1'),
(832, 'KMS4ECHBZW', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice NoKMS4ECHBZW', '0.00', '17.08', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1'),
(833, 'KMS4ECHBZW', 'INV', '2019-09-19', '102030134', 'Customer debit For  Walking customer', '70000.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1'),
(834, 'KMS4ECHBZW', 'INVOICE', '2019-09-19', '303', 'Sale Income For Walking customer', '0.00', '70000.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1'),
(835, 'KMS4ECHBZW', 'INV', '2019-09-19', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '580.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1'),
(836, 'KMS4ECHBZW', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Walking customer', '580.00', '0.00', '1', 'eJGHZzMlqcM3d4x', '2019-09-19 04:48:52', NULL, NULL, '1'),
(837, '5147385423', 'INV', '2019-09-19', '10107', 'Inventory credit For Invoice No5147385423', '0.00', '42.69', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1'),
(838, '5147385423', 'INV', '2019-09-19', '102030140', 'Customer debit For  Hm Isahaq', '175000.00', '0.00', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1'),
(839, '5147385423', 'INVOICE', '2019-09-19', '303', 'Sale Income For Hm Isahaq', '0.00', '175000.00', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1'),
(840, '5147385423', 'INV', '2019-09-19', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '175900.00', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1'),
(841, '5147385423', 'INV', '2019-09-19', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '175900.00', '0.00', '1', '1', '2019-09-19 07:20:32', NULL, NULL, '1'),
(842, '5147385423', 'Return', '2019-09-19', '102030140', 'Customer debit For  Hm Isahaq', '70000.00', '0.00', '1', '1', '2019-09-19 07:22:43', NULL, NULL, '1'),
(843, 'CR-1', 'CR', '2019-09-19', '1', '', '0.00', '8.00', '1', '1', '2019-09-19 08:58:08', NULL, NULL, '1'),
(844, 'CR-1', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '8.00', '0.00', '1', '1', '2019-09-19 08:58:08', NULL, NULL, '1'),
(845, 'CR-2', 'CR', '2019-09-19', '1', '', '0.00', '7.00', '1', '1', '2019-09-19 08:59:31', NULL, NULL, '1'),
(846, 'CR-2', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '7.00', '0.00', '1', '1', '2019-09-19 08:59:31', NULL, NULL, '1'),
(847, 'DV-1', 'DV', '2019-09-19', '102010201', 'sdfasdf', '0.00', '123.00', '1', NULL, '2019-09-19 09:03:23', NULL, NULL, '0'),
(848, 'DV-1', 'DV', '2019-09-19', '5020200004', 'sdfasdf', '123.00', '0.00', '1', NULL, '2019-09-19 09:03:23', NULL, NULL, '0'),
(849, 'CV-1', 'CV', '2019-09-19', '102010201', 'sdfasdf', '23.00', '0.00', '1', NULL, '2019-09-19 09:04:34', NULL, NULL, '0'),
(850, 'CV-1', 'CV', '2019-09-19', '102030134', 'sdfasdf', '0.00', '23.00', '1', NULL, '2019-09-19 09:04:34', NULL, NULL, '0'),
(851, 'CR-2', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 09:24:55', NULL, NULL, '1'),
(852, 'CR-2', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 09:24:55', NULL, NULL, '1'),
(853, 'CR-3', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 09:58:34', NULL, NULL, '1'),
(854, 'CR-3', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 09:58:34', NULL, NULL, '1'),
(855, 'CR-3', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:13:08', NULL, NULL, '1'),
(856, 'CR-3', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 10:13:08', NULL, NULL, '1'),
(857, 'CR-3', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:20:26', NULL, NULL, '1'),
(858, 'CR-4', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:20:34', NULL, NULL, '1'),
(859, 'CR-4', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:21:50', NULL, NULL, '1'),
(860, 'CR-5', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:22:34', NULL, NULL, '1'),
(861, 'CR-6', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:23:40', NULL, NULL, '1'),
(862, 'CR-6', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 10:23:40', NULL, NULL, '1'),
(863, 'CR-7', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:25:43', NULL, NULL, '1'),
(864, 'CR-7', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 10:25:43', NULL, NULL, '1'),
(865, 'CR-8', 'CR', '2019-09-19', '', 'fsdfsdf', '0.00', '0.00', '1', '1', '2019-09-19 10:26:18', NULL, NULL, '1'),
(866, 'CR-8', 'CR', '2019-09-19', '1020101', 'Cash in Hand For  ', '0.00', '0.00', '1', '1', '2019-09-19 10:26:18', NULL, NULL, '1'),
(867, '3QHCHGI5BM', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No3QHCHGI5BM', '0.00', '0.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1'),
(868, '3QHCHGI5BM', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '0.00', '0.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1'),
(869, '3QHCHGI5BM', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '0.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1'),
(870, '3QHCHGI5BM', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1'),
(871, '3QHCHGI5BM', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '', '2019-09-21 05:43:10', NULL, NULL, '1'),
(872, '5928235575', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No5928235575', '0.00', '8.54', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1'),
(873, '5928235575', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1'),
(874, '5928235575', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1'),
(875, '5928235575', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1'),
(876, '5928235575', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 09:41:25', NULL, NULL, '1'),
(877, '6852499599', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No6852499599', '0.00', '8.54', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1'),
(878, '6852499599', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1'),
(879, '6852499599', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1'),
(880, '6852499599', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1'),
(881, '6852499599', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 12:25:52', NULL, NULL, '1'),
(882, '7348638525', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No7348638525', '0.00', '8.54', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1'),
(883, '7348638525', 'INV', '2019-09-21', '102030140', 'Customer debit For  Hm Isahaq', '105000.00', '0.00', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1'),
(884, '7348638525', 'INVOICE', '2019-09-21', '303', 'Sale Income For Hm Isahaq', '0.00', '105000.00', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1'),
(885, '7348638525', 'INV', '2019-09-21', '102030140', 'Customer credit for Paid Amount For Customer Hm Isahaq', '0.00', '35000.00', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1'),
(886, '7348638525', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Hm Isahaq', '35000.00', '0.00', '1', '1', '2019-09-21 12:29:00', NULL, NULL, '1'),
(887, '6979694511', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No6979694511', '0.00', '39.83', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1'),
(888, '6979694511', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1'),
(889, '6979694511', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1'),
(890, '6979694511', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1'),
(891, '6979694511', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-09-21 12:56:58', NULL, NULL, '1'),
(892, '7675268361', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No7675268361', '0.00', '39.83', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1'),
(893, '7675268361', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '500.00', '0.00', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1'),
(894, '7675268361', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '500.00', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1'),
(895, '7675268361', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '500.00', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1'),
(896, '7675268361', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '500.00', '0.00', '1', '1', '2019-09-21 12:58:30', NULL, NULL, '1'),
(897, '1699845521', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No1699845521', '0.00', '8.54', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1'),
(898, '1699845521', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1'),
(899, '1699845521', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1'),
(900, '1699845521', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1'),
(901, '1699845521', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:07:56', NULL, NULL, '1'),
(902, '3291158273', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No3291158273', '0.00', '8.54', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1'),
(903, '3291158273', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1'),
(904, '3291158273', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1'),
(905, '3291158273', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1'),
(906, '3291158273', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:14:07', NULL, NULL, '1'),
(907, '7884756971', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No7884756971', '0.00', '8.54', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1'),
(908, '7884756971', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1'),
(909, '7884756971', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1'),
(910, '7884756971', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1'),
(911, '7884756971', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:15:08', NULL, NULL, '1'),
(912, '9836928819', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No9836928819', '0.00', '8.54', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1'),
(913, '9836928819', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1'),
(914, '9836928819', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1'),
(915, '9836928819', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1'),
(916, '9836928819', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:17:09', NULL, NULL, '1'),
(917, '2466279899', 'INV', '2019-09-21', '10107', 'Inventory credit For Invoice No2466279899', '0.00', '8.54', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1'),
(918, '2466279899', 'INV', '2019-09-21', '102030134', 'Customer debit For  Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1'),
(919, '2466279899', 'INVOICE', '2019-09-21', '303', 'Sale Income For Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1'),
(920, '2466279899', 'INV', '2019-09-21', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '35000.00', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1'),
(921, '2466279899', 'INV', '2019-09-21', '1020101', 'Cash in Hand in Sale for Walking customer', '35000.00', '0.00', '1', '1', '2019-09-21 13:18:26', NULL, NULL, '1'),
(922, '20190924143851', 'Purchase', '2019-09-24', '10107', 'Inventory Debit For Supplier isahaq', '4500.00', '0.00', '1', '1', '2019-09-24 14:38:51', NULL, NULL, '1'),
(923, '20190924143851', 'Purchase', '2019-09-24', '5020200005', 'Supplier .isahaq', '0.00', '4500.00', '1', '1', '2019-09-24 00:00:00', NULL, NULL, '1'),
(924, '20190924143851', 'Purchase', '2019-09-24', '402', 'Company Credit For  isahaq', '4500.00', '0.00', '1', '1', '2019-09-24 14:38:51', NULL, NULL, '1'),
(925, '20190924143851', 'Purchase', '2019-09-24', '1020101', 'Cash in Hand For Supplier isahaq', '0.00', '4500.00', '1', '1', '2019-09-24 14:38:51', NULL, NULL, '1'),
(926, '20190924143851', 'Purchase', '2019-09-24', '5020200005', 'Supplier .isahaq', '4500.00', '0.00', '1', '1', '2019-09-24 00:00:00', NULL, NULL, '1'),
(927, '7126396398', 'INV', '2019-09-24', '10107', 'Inventory credit For Invoice No7126396398', '0.00', '450.00', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1'),
(928, '7126396398', 'INV', '2019-09-24', '102030134', 'Customer debit For  Walking customer', '514.90', '0.00', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1'),
(929, '7126396398', 'INVOICE', '2019-09-24', '303', 'Sale Income For Walking customer', '0.00', '514.90', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1'),
(930, '7126396398', 'INV', '2019-09-24', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '514.90', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1'),
(931, '7126396398', 'INV', '2019-09-24', '1020101', 'Cash in Hand in Sale for Walking customer', '514.90', '0.00', '1', '1', '2019-09-24 14:42:49', NULL, NULL, '1'),
(932, '20190925103121', 'Purchase', '2019-09-25', '10107', 'Inventory Debit For Supplier isahaq', '3300.00', '0.00', '1', '1', '2019-09-25 10:31:21', NULL, NULL, '1'),
(933, '20190925103121', 'Purchase', '2019-09-25', '5020200005', 'Supplier .isahaq', '0.00', '3300.00', '1', '1', '2019-09-25 00:00:00', NULL, NULL, '1'),
(934, '20190925103121', 'Purchase', '2019-09-25', '402', 'Company Credit For  isahaq', '3300.00', '0.00', '1', '1', '2019-09-25 10:31:21', NULL, NULL, '1'),
(935, '20190925103121', 'Purchase', '2019-09-25', '1020101', 'Cash in Hand For Supplier isahaq', '0.00', '3300.00', '1', '1', '2019-09-25 10:31:21', NULL, NULL, '1'),
(936, '20190925103121', 'Purchase', '2019-09-25', '5020200005', 'Supplier .isahaq', '3300.00', '0.00', '1', '1', '2019-09-25 00:00:00', NULL, NULL, '1'),
(937, '3496522264', 'INV', '2019-09-26', '10107', 'Inventory credit For Invoice No3496522264', '0.00', '90.00', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1'),
(938, '3496522264', 'INV', '2019-09-26', '102030134', 'Customer debit For  Walking customer', '102.98', '0.00', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1'),
(939, '3496522264', 'INVOICE', '2019-09-26', '303', 'Sale Income For Walking customer', '0.00', '102.98', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1'),
(940, '3496522264', 'INV', '2019-09-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '102.98', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1'),
(941, '3496522264', 'INV', '2019-09-26', '1020101', 'Cash in Hand in Sale for Walking customer', '102.98', '0.00', '1', '1', '2019-09-26 09:02:26', NULL, NULL, '1'),
(942, '1962163959', 'INV', '2019-09-26', '10107', 'Inventory credit For Invoice No1962163959', '0.00', '90.00', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1'),
(943, '1962163959', 'INV', '2019-09-26', '102030134', 'Customer debit For  Walking customer', '102.98', '0.00', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1'),
(944, '1962163959', 'INVOICE', '2019-09-26', '303', 'Sale Income For Walking customer', '0.00', '102.98', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1'),
(945, '1962163959', 'INV', '2019-09-26', '102030134', 'Customer credit for Paid Amount For Customer Walking customer', '0.00', '102.98', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1'),
(946, '1962163959', 'INV', '2019-09-26', '1020101', 'Cash in Hand in Sale for Walking customer', '102.98', '0.00', '1', '1', '2019-09-26 09:03:38', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `att_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `sign_in` varchar(30) NOT NULL,
  `sign_out` varchar(30) NOT NULL,
  `staytime` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bank_add`
--

CREATE TABLE `bank_add` (
  `bank_id` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `ac_name` varchar(250) DEFAULT NULL,
  `ac_number` varchar(250) DEFAULT NULL,
  `branch` varchar(250) DEFAULT NULL,
  `signature_pic` varchar(250) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank_add`
--

INSERT INTO `bank_add` (`bank_id`, `bank_name`, `ac_name`, `ac_number`, `branch`, `signature_pic`, `status`) VALUES
('5AYHFKX8PU', 'Dhaka Bank ', 'Mr Md Nur', '3554553', 'chowmuhani ', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bank_summary`
--

CREATE TABLE `bank_summary` (
  `bank_id` varchar(250) DEFAULT NULL,
  `description` text,
  `deposite_id` varchar(250) DEFAULT NULL,
  `date` varchar(250) DEFAULT NULL,
  `ac_type` varchar(50) DEFAULT NULL,
  `dr` float DEFAULT NULL,
  `cr` float DEFAULT NULL,
  `ammount` float DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cheque_manger`
--

CREATE TABLE `cheque_manger` (
  `cheque_id` varchar(100) NOT NULL,
  `transection_id` varchar(100) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `bank_id` varchar(100) NOT NULL,
  `cheque_no` varchar(100) NOT NULL,
  `date` varchar(250) DEFAULT NULL,
  `transection_type` varchar(100) NOT NULL,
  `cheque_status` int(10) NOT NULL,
  `amount` float NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company_information`
--

CREATE TABLE `company_information` (
  `company_id` varchar(250) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_information`
--

INSERT INTO `company_information` (`company_id`, `company_name`, `email`, `address`, `mobile`, `website`, `status`) VALUES
('1', 'coderit.net', 'coderit.net@gmail.com', 'Korim Pur Road  , Chowmuhani , Noakhali ', '01985121212 ,01730164612 015445441', 'https://www.coderit.net/', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency_tbl`
--

CREATE TABLE `currency_tbl` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(50) NOT NULL,
  `icon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency_tbl`
--

INSERT INTO `currency_tbl` (`id`, `currency_name`, `icon`) VALUES
(1, 'Taka', '৳'),
(2, 'Azerbaijan Manat', '₼');

-- --------------------------------------------------------

--
-- Table structure for table `customer_information`
--

CREATE TABLE `customer_information` (
  `id` int(11) NOT NULL,
  `customer_id` varchar(250) NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_address` varchar(255) NOT NULL,
  `customer_mobile` varchar(100) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `status` int(2) NOT NULL COMMENT '1=paid,2=credit',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_information`
--

INSERT INTO `customer_information` (`id`, `customer_id`, `customer_name`, `customer_address`, `customer_mobile`, `customer_email`, `status`, `create_date`) VALUES
(1, '6FGVC6K7OX5L9Z1', 'Walking customer', '', '', '', 1, '2019-04-19 20:47:38'),
(45, 'XWCEM9UVV5NAT5R', 'Test customer', 'Dhaka', '01550', 'testE@gmail.com2', 2, '2019-06-25 03:09:05'),
(46, 'FYJ4EEBMOGTW25T', 'md abdullah', 'cumilla', '01900000000', '', 1, '2019-06-26 04:17:16'),
(47, 'PVGGNNSWOESP45P', 'alexix', 'ukrain', '01200 000000', '', 1, '2019-06-26 04:52:38'),
(49, 'KFTUKN7RY1O2W6R', 'samil khan', '', '', '', 1, '2019-06-27 23:47:49'),
(56, 'MQ49MPE6YAYN9QN', 'kobir', 'dhaka', '123456', 'kobirdo@gmail. com', 2, '2019-09-02 04:38:33'),
(51, '1IVRRX4VOCVYHOW', 'Ovi', 'fdsfsd', '02234', 'dgd@gmail.com', 2, '2019-07-31 23:58:15'),
(52, '93K7WTVUD86LBTC', 'Isahaq', '', '', '', 2, '2019-08-05 23:57:28'),
(53, 'TJV9RCZST65TC4B', 'demo', 'gghh', '66544333', '', 2, '2019-08-07 09:37:49'),
(54, 'BIXQAYJ3XDFBYZN', 'Hm Isahaq', 'sdfadsf', '01955110016', 'hmisahaq@yahoo.com', 1, '2019-08-27 02:33:11'),
(55, '7DNRCVSN8GE2S6W', 'ggfsdgh', '', '', '', 2, '2019-08-27 04:29:08');

-- --------------------------------------------------------

--
-- Table structure for table `customer_ledger`
--

CREATE TABLE `customer_ledger` (
  `id` int(20) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `receipt_no` varchar(50) DEFAULT NULL,
  `amount` decimal(12,2) DEFAULT '0.00',
  `description` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `cheque_no` varchar(255) NOT NULL,
  `date` varchar(250) DEFAULT NULL,
  `receipt_from` varchar(50) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `d_c` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_ledger`
--

INSERT INTO `customer_ledger` (`id`, `transaction_id`, `customer_id`, `invoice_no`, `receipt_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `receipt_from`, `status`, `d_c`, `created_at`) VALUES
(1, 'SUBNIWB5ZF', 'XWCEM9UVV5NAT5R', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-25', NULL, 1, 'd', '2019-06-25 03:09:05'),
(2, 'FS93V8F75ICGLDK', 'XWCEM9UVV5NAT5R', '3498244447', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-25', NULL, 1, 'd', '2019-06-25 03:09:28'),
(3, 'FS93V8F75ICGLDK', 'XWCEM9UVV5NAT5R', '3498244447', 'EJFJS7V65M', '200.00', 'Paid by customer', '1', '', '2019-06-25', NULL, 1, 'c', '2019-06-25 03:09:28'),
(4, 'Q3IZ9DRCOSZ768G', '6FGVC6K7OX5L9Z1', '4477997231', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-25', NULL, 1, 'd', '2019-06-25 03:19:13'),
(5, 'Q3IZ9DRCOSZ768G', '6FGVC6K7OX5L9Z1', '4477997231', 'NB6KEBYU3M', '200.00', 'Paid by customer', '1', '', '2019-06-25', NULL, 1, 'c', '2019-06-25 03:19:13'),
(6, 'QO6WH5CIWCPJ6Q1', '6FGVC6K7OX5L9Z1', '3973611118', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-25', NULL, 1, 'd', '2019-06-25 07:30:33'),
(7, 'QO6WH5CIWCPJ6Q1', '6FGVC6K7OX5L9Z1', '3973611118', '7VOZQV8OLI', '500.00', 'Paid by customer', '1', '', '2019-06-25', NULL, 1, 'c', '2019-06-25 07:30:33'),
(8, 'S67BGX3BCKB5VDV', '6FGVC6K7OX5L9Z1', '3923586588', NULL, '900.00', 'Purchase by customer', '', '', '2019-06-25', NULL, 1, 'd', '2019-06-25 07:52:59'),
(9, 'S67BGX3BCKB5VDV', '6FGVC6K7OX5L9Z1', '3923586588', 'K2R4OPEMTM', '600.00', 'Paid by customer', '1', '', '2019-06-25', NULL, 1, 'c', '2019-06-25 07:52:59'),
(10, 'F1QPDTOHWZ16P9P', 'XWCEM9UVV5NAT5R', '6575276926', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-25 23:34:09'),
(11, 'F1QPDTOHWZ16P9P', 'XWCEM9UVV5NAT5R', '6575276926', 'UX74RVQAI8', '200.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-25 23:34:09'),
(12, 'YYMQYK93V1ZM516', '6FGVC6K7OX5L9Z1', '6643494915', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-25 23:35:33'),
(13, 'YYMQYK93V1ZM516', '6FGVC6K7OX5L9Z1', '6643494915', 'MNO2ZAMKMZ', '200.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-25 23:35:33'),
(14, 'FE7CRWNMHEY38MD', '6FGVC6K7OX5L9Z1', '1969912464', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-25 23:36:30'),
(15, 'FE7CRWNMHEY38MD', '6FGVC6K7OX5L9Z1', '1969912464', '62Y51O5X95', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-25 23:36:30'),
(16, 'AMUTK48CY2VYX8L', '6FGVC6K7OX5L9Z1', '6691959977', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-25 23:37:50'),
(17, 'AMUTK48CY2VYX8L', '6FGVC6K7OX5L9Z1', '6691959977', 'JBOUFCF2BI', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-25 23:37:50'),
(18, 'HVFX6UG8VG2NR6K', '6FGVC6K7OX5L9Z1', '2258111339', NULL, '200.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-25 23:38:33'),
(19, 'HVFX6UG8VG2NR6K', '6FGVC6K7OX5L9Z1', '2258111339', '9K9F3MK86F', '200.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-25 23:38:33'),
(20, 'KWTMIGM4DFV4MMN', '6FGVC6K7OX5L9Z1', '9624435359', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-25 23:38:35'),
(21, 'KWTMIGM4DFV4MMN', '6FGVC6K7OX5L9Z1', '9624435359', '66K2LR19XD', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-25 23:38:35'),
(22, 'O6D1W158OH1A4SO', '6FGVC6K7OX5L9Z1', '8668653724', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-25 23:41:41'),
(23, 'O6D1W158OH1A4SO', '6FGVC6K7OX5L9Z1', '8668653724', 'XN72D3TZMP', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-25 23:41:41'),
(24, 'ZZGD9XYMVFUFHET', '6FGVC6K7OX5L9Z1', '8558184696', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-25 23:55:57'),
(25, 'ZZGD9XYMVFUFHET', '6FGVC6K7OX5L9Z1', '8558184696', '14F5NOZ37S', '500.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-25 23:55:57'),
(26, 'XPE32XHWMR1IIOD', '6FGVC6K7OX5L9Z1', '9147771963', NULL, '450.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-26 04:11:20'),
(27, 'XPE32XHWMR1IIOD', '6FGVC6K7OX5L9Z1', '9147771963', '7232TK1WXR', '450.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-26 04:11:20'),
(28, 'DZIV6866V4', 'FYJ4EEBMOGTW25T', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-26', NULL, 1, 'd', '2019-06-26 04:17:16'),
(29, 'Y2GQTYTL9QPDS3S', 'FYJ4EEBMOGTW25T', '3169824785', NULL, '800.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-26 04:17:16'),
(30, 'Y2GQTYTL9QPDS3S', 'FYJ4EEBMOGTW25T', '3169824785', 'YOFRJ2TAAG', '800.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-26 04:17:16'),
(31, 'DIAUX4GUIHBKXM2', '6FGVC6K7OX5L9Z1', '3918198168', NULL, '1450.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-26 04:50:05'),
(32, 'DIAUX4GUIHBKXM2', '6FGVC6K7OX5L9Z1', '3918198168', 'SIMXB999D4', '1450.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-26 04:50:05'),
(33, 'BBDN1X3N59', 'PVGGNNSWOESP45P', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-26', NULL, 1, 'd', '2019-06-26 04:52:38'),
(34, '5ES9981RI25GKC4', 'PVGGNNSWOESP45P', '5219125479', NULL, '450.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-26 04:52:46'),
(35, '5ES9981RI25GKC4', 'PVGGNNSWOESP45P', '5219125479', 'CW6AK3SP6Q', '450.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-26 04:52:46'),
(36, 'DI85CJ5RMXQD7HB', 'PVGGNNSWOESP45P', '8394724961', NULL, '1450.00', 'Purchase by customer', '', '', '2019-06-26', NULL, 1, 'd', '2019-06-26 04:55:06'),
(37, 'DI85CJ5RMXQD7HB', 'PVGGNNSWOESP45P', '8394724961', '854LM2HXYW', '1450.00', 'Paid by customer', '1', '', '2019-06-26', NULL, 1, 'c', '2019-06-26 04:55:06'),
(38, 'GUYXF9TXVLAJJEQ', '6FGVC6K7OX5L9Z1', '7986446844', NULL, '800.00', 'Purchase by customer', '', '', '2019-06-27', NULL, 1, 'd', '2019-06-27 04:58:16'),
(39, 'GUYXF9TXVLAJJEQ', '6FGVC6K7OX5L9Z1', '7986446844', 'ZSU3NSRJMC', '800.00', 'Paid by customer', '1', '', '2019-06-27', NULL, 1, 'c', '2019-06-27 04:58:16'),
(40, '9P35ZCUMLIEJFWO', '6FGVC6K7OX5L9Z1', '6789217583', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-27', NULL, 1, 'd', '2019-06-27 06:28:36'),
(41, '9P35ZCUMLIEJFWO', '6FGVC6K7OX5L9Z1', '6789217583', 'NA14OJQGCP', '500.00', 'Paid by customer', '1', '', '2019-06-27', NULL, 1, 'c', '2019-06-27 06:28:36'),
(45, 'FPBKH6VMRF', 'KFTUKN7RY1O2W6R', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-28', NULL, 1, 'd', '2019-06-27 23:47:49'),
(46, 'H2INB5JLI83B2JZ', 'KFTUKN7RY1O2W6R', '2687213589', NULL, '50000.00', 'Purchase by customer', '', '', '2019-06-28', NULL, 1, 'd', '2019-06-27 23:48:12'),
(47, 'H2INB5JLI83B2JZ', 'KFTUKN7RY1O2W6R', '2687213589', '9KNGG4M1XF', '30000.00', 'Paid by customer', '1', '', '2019-06-28', NULL, 1, 'c', '2019-06-27 23:48:12'),
(48, 'G21MAEQN2P', 'KFTUKN7RY1O2W6R', 'NA', NULL, '111.00', 'Customer Advance', 'NA', 'NA', '2019-06-28', NULL, 1, 'd', '2019-06-27 23:51:19'),
(50, 'MSAGGFLPOJDKQII', '6FGVC6K7OX5L9Z1', '3378453241', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-28', NULL, 1, 'd', '2019-06-28 00:24:01'),
(51, 'MSAGGFLPOJDKQII', '6FGVC6K7OX5L9Z1', '3378453241', 'Z5FJ4HSG6M', '500.00', 'Paid by customer', '1', '', '2019-06-28', NULL, 1, 'c', '2019-06-28 00:24:01'),
(52, 'STD9RZ3H1JBXP5R', '6FGVC6K7OX5L9Z1', '4458233386', NULL, '500.00', 'Purchase by customer', '', '', '2019-06-28', NULL, 1, 'd', '2019-06-28 00:24:10'),
(53, 'STD9RZ3H1JBXP5R', '6FGVC6K7OX5L9Z1', '4458233386', 'D9LQ9H5LZ1', '500.00', 'Paid by customer', '1', '', '2019-06-28', NULL, 1, 'c', '2019-06-28 00:24:10'),
(54, 'UNSJVY7KRG63XFU', '6FGVC6K7OX5L9Z1', '1114135497', NULL, '2210.00', 'Purchase by customer', '', '', '2019-06-28', NULL, 1, 'd', '2019-06-28 04:57:32'),
(55, '2FA13JON5GQ6T9K', '6FGVC6K7OX5L9Z1', '4441511736', NULL, '2210.00', 'Purchase by customer', '', '', '2019-06-28', NULL, 1, 'd', '2019-06-28 04:57:34'),
(56, '2FA13JON5GQ6T9K', '6FGVC6K7OX5L9Z1', '4441511736', 'LMEIB4VOGV', '2210.00', 'Paid by customer', '1', '', '2019-06-28', NULL, 1, 'c', '2019-06-28 04:57:34'),
(57, 'VP9EK1NHG955C8E', '6FGVC6K7OX5L9Z1', '8754679919', NULL, '50000.00', 'Purchase by customer', '', '', '2019-06-28', NULL, 1, 'd', '2019-06-28 04:58:11'),
(58, 'EFZMHVZOZIHL9X9', '6FGVC6K7OX5L9Z1', '3378767423', NULL, '50000.00', 'Purchase by customer', '', '', '2019-06-28', NULL, 1, 'd', '2019-06-28 04:58:19'),
(59, 'EFZMHVZOZIHL9X9', '6FGVC6K7OX5L9Z1', '3378767423', 'B42F8EMH4I', '50000.00', 'Paid by customer', '1', '', '2019-06-28', NULL, 1, 'c', '2019-06-28 04:58:19'),
(60, 'FUFHSCJATY2LHY4', '6FGVC6K7OX5L9Z1', '9317933378', NULL, '50000.00', 'Purchase by customer', '', '', '2019-06-29', NULL, 1, 'd', '2019-06-28 23:30:07'),
(61, 'OFTJDX79S6JKXT5', '6FGVC6K7OX5L9Z1', '1797561487', NULL, '36000.00', 'Purchase by customer', '', '', '2019-06-29', NULL, 1, 'd', '2019-06-29 05:45:22'),
(62, 'OFTJDX79S6JKXT5', '6FGVC6K7OX5L9Z1', '1797561487', 'G5A4ND6CAT', '36000.00', 'Paid by customer', '1', '', '2019-06-29', NULL, 1, 'c', '2019-06-29 05:45:22'),
(63, 'YKFQ14NNAGXFATU', '6FGVC6K7OX5L9Z1', '9438755655', NULL, '35000.00', 'Purchase by customer', '', '', '2019-06-30', NULL, 1, 'd', '2019-06-30 06:00:12'),
(64, 'YKFQ14NNAGXFATU', '6FGVC6K7OX5L9Z1', '9438755655', '99W8I36UB1', '35000.00', 'Paid by customer', '1', '', '2019-06-30', NULL, 1, 'c', '2019-06-30 06:00:12'),
(65, '126OOJOQE2XEQ26', '6FGVC6K7OX5L9Z1', '5919433886', NULL, '35000.00', 'Purchase by customer', '', '', '2019-06-30', NULL, 1, 'd', '2019-06-30 06:00:44'),
(66, '126OOJOQE2XEQ26', '6FGVC6K7OX5L9Z1', '5919433886', '4T836JN4JJ', '35000.00', 'Paid by customer', '1', '', '2019-06-30', NULL, 1, 'c', '2019-06-30 06:00:44'),
(67, 'B1CY534MOESSW5C', '6FGVC6K7OX5L9Z1', '2875718763', NULL, '200.00', 'Purchase by customer', '', '', '2019-07-01', NULL, 1, 'd', '2019-07-01 00:14:50'),
(68, 'B1CY534MOESSW5C', '6FGVC6K7OX5L9Z1', '2875718763', '82GDNBKG11', '200.00', 'Paid by customer', '1', '', '2019-07-01', NULL, 1, 'c', '2019-07-01 00:14:50'),
(69, '2V7IRO1C13I2MDW', '6FGVC6K7OX5L9Z1', '1749144575', NULL, '200.00', 'Purchase by customer', '', '', '2019-07-02', NULL, 1, 'd', '2019-07-01 23:31:13'),
(70, '2V7IRO1C13I2MDW', '6FGVC6K7OX5L9Z1', '1749144575', 'CGN9PJCHT8', '200.00', 'Paid by customer', '1', '', '2019-07-02', NULL, 1, 'c', '2019-07-01 23:31:13'),
(71, 'GFVMOPDBU2EB87U', '6FGVC6K7OX5L9Z1', '9815582964', NULL, '35000.00', 'Purchase by customer', '', '', '2019-07-02', NULL, 1, 'd', '2019-07-01 23:32:17'),
(72, 'GFVMOPDBU2EB87U', '6FGVC6K7OX5L9Z1', '9815582964', '38IWAMW5QC', '35000.00', 'Paid by customer', '1', '', '2019-07-02', NULL, 1, 'c', '2019-07-01 23:32:17'),
(73, 'SBW99OEQ3LR73IG', '6FGVC6K7OX5L9Z1', '8493678614', NULL, '455.00', 'Purchase by customer', '', '', '2019-07-02', NULL, 1, 'd', '2019-07-01 23:38:08'),
(74, 'SBW99OEQ3LR73IG', '6FGVC6K7OX5L9Z1', '8493678614', 'ZZLJCKFRHQ', '455.00', 'Paid by customer', '1', '', '2019-07-02', NULL, 1, 'c', '2019-07-01 23:38:08'),
(76, 'PZBG4F2CP4ZVSP8', '6FGVC6K7OX5L9Z1', '9691144514', NULL, '800.00', 'Purchase by customer', '', '', '2019-07-31', NULL, 1, 'd', '2019-07-31 05:38:51'),
(77, '8SA4NBY3SH62KMW', '6FGVC6K7OX5L9Z1', '5457199982', NULL, '195465.00', 'Purchase by customer', '', '', '2019-07-31', NULL, 1, 'd', '2019-07-31 05:50:38'),
(78, '8SA4NBY3SH62KMW', '6FGVC6K7OX5L9Z1', '5457199982', 'OJFJJQCNNW', '195465.00', 'Paid by customer', '1', '', '2019-07-31', NULL, 1, 'c', '2019-07-31 05:50:38'),
(79, 'E64ODCUPGHL57QG', '6FGVC6K7OX5L9Z1', '2362693843', NULL, '35000.00', 'Purchase by customer', '', '', '2019-07-31', NULL, 1, 'd', '2019-07-31 07:24:11'),
(80, 'E64ODCUPGHL57QG', '6FGVC6K7OX5L9Z1', '2362693843', 'XCCLA5L58X', '35000.00', 'Paid by customer', '1', '', '2019-07-31', NULL, 1, 'c', '2019-07-31 07:24:11'),
(84, 'RZVZUC2CK73MFHK', '6FGVC6K7OX5L9Z1', '5763389422', NULL, '455.00', 'Purchase by customer', '1', '', '2019-07-31', NULL, 1, 'd', '2019-07-31 07:31:17'),
(83, 'RZVZUC2CK73MFHK', '6FGVC6K7OX5L9Z1', '5763389422', '1K2EJSWXP1', '455.00', 'Paid by customer', '1', '', '2019-07-31', NULL, 1, 'c', '2019-07-31 07:31:17'),
(85, 'UH22TNP1RNBWZMU', '6FGVC6K7OX5L9Z1', '6921223925', NULL, '50455.00', 'Purchase by customer', '', '', '2019-08-01', NULL, 1, 'd', '2019-07-31 22:46:47'),
(86, 'UH22TNP1RNBWZMU', '6FGVC6K7OX5L9Z1', '6921223925', 'HEYT5JXWVX', '50455.00', 'Paid by customer', '1', '', '2019-08-01', NULL, 1, 'c', '2019-07-31 22:46:47'),
(87, '2FPNZL5FLJRFEY4', '6FGVC6K7OX5L9Z1', '2992478698', NULL, '50655.00', 'Purchase by customer', '', '', '2019-08-01', NULL, 1, 'd', '2019-07-31 22:51:22'),
(88, '2FPNZL5FLJRFEY4', '6FGVC6K7OX5L9Z1', '2992478698', 'GPMG1LGY4U', '50655.00', 'Paid by customer', '1', '', '2019-08-01', NULL, 1, 'c', '2019-07-31 22:51:22'),
(89, 'YP2TDHXDFT7PQ56', '6FGVC6K7OX5L9Z1', '2429117215', NULL, '51855.00', 'Purchase by customer', '', '', '2019-08-01', NULL, 1, 'd', '2019-07-31 22:58:34'),
(90, 'YP2TDHXDFT7PQ56', '6FGVC6K7OX5L9Z1', '2429117215', 'SSTBYE5JAM', '51855.00', 'Paid by customer', '1', '', '2019-08-01', NULL, 1, 'c', '2019-07-31 22:58:34'),
(91, 'YCJUPGV3B5MBKQX', '6FGVC6K7OX5L9Z1', '8636821614', NULL, '85000.00', 'Purchase by customer', '', '', '2019-08-01', NULL, 1, 'd', '2019-07-31 23:05:19'),
(92, 'YCJUPGV3B5MBKQX', '6FGVC6K7OX5L9Z1', '8636821614', '1BO21D4KYM', '85000.00', 'Paid by customer', '1', '', '2019-08-01', NULL, 1, 'c', '2019-07-31 23:05:19'),
(93, 'WBUNPUTYW5', '1IVRRX4VOCVYHOW', 'NA', NULL, NULL, 'Previous adjustment with software', 'NA', 'NA', '2019-08-01', NULL, 1, 'd', '2019-07-31 23:58:15'),
(94, 'DO95YM46XX8JQRL', '6FGVC6K7OX5L9Z1', '2274125738', NULL, '50000.00', 'Purchase by customer', '', '', '2019-08-01', NULL, 1, 'd', '2019-08-01 00:12:13'),
(95, 'DO95YM46XX8JQRL', '6FGVC6K7OX5L9Z1', '2274125738', '9XYYNV7UR6', '50000.00', 'Paid by customer', '1', '', '2019-08-01', NULL, 1, 'c', '2019-08-01 00:12:13'),
(96, 'Z1PT7RD61Z4MZ3I', '6FGVC6K7OX5L9Z1', '6881727828', NULL, '455.00', 'Purchase by customer', '', '', '2019-08-01', NULL, 1, 'd', '2019-08-01 05:46:11'),
(97, 'Z1PT7RD61Z4MZ3I', '6FGVC6K7OX5L9Z1', '6881727828', '86X1REVPAW', '455.00', 'Paid by customer', '1', '', '2019-08-01', NULL, 1, 'c', '2019-08-01 05:46:11'),
(98, '37P7PM8L72D6ORF', '6FGVC6K7OX5L9Z1', '6942233633', NULL, '200.00', 'Purchase by customer', '', '', '2019-08-03', NULL, 1, 'd', '2019-08-03 05:23:56'),
(99, '4D6FVY1AIMSJDKW', '6FGVC6K7OX5L9Z1', '7167684116', NULL, '2600.00', 'Purchase by customer', '', '', '2019-08-03', NULL, 1, 'd', '2019-08-03 05:30:17'),
(100, '4D6FVY1AIMSJDKW', '6FGVC6K7OX5L9Z1', '7167684116', '4N5NMFBDXU', '2600.00', 'Paid by customer', '1', '', '2019-08-03', NULL, 1, 'c', '2019-08-03 05:30:17'),
(101, 'UIHYIVQC7V', '93K7WTVUD86LBTC', 'NA', NULL, NULL, 'Previous adjustment with software', 'NA', 'NA', '2019-08-06', NULL, 1, 'd', '2019-08-05 23:57:28'),
(102, 'URRZ8DNDBW', 'TJV9RCZST65TC4B', 'NA', NULL, NULL, 'Previous adjustment with software', 'NA', 'NA', '2019-08-07', NULL, 1, 'd', '2019-08-07 09:37:49'),
(103, '3K9OT4P4GQ', 'BIXQAYJ3XDFBYZN', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-08-27', NULL, 1, 'd', '2019-08-27 02:33:11'),
(104, '9SQL4HDK78GTX79', 'BIXQAYJ3XDFBYZN', 'MBJEQBLEDT', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 02:35:31'),
(105, '9SQL4HDK78GTX79', 'BIXQAYJ3XDFBYZN', 'MBJEQBLEDT', 'P9P3G6PXBM', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 02:35:31'),
(106, 'P14K3GNFMSCCLP3', 'BIXQAYJ3XDFBYZN', '3OXFJU9FLE', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 02:38:33'),
(107, 'P14K3GNFMSCCLP3', 'BIXQAYJ3XDFBYZN', '3OXFJU9FLE', 'EM9OWEYUFW', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 02:38:33'),
(108, 'OE3P77I88Q91OA4', 'BIXQAYJ3XDFBYZN', 'AEYS0XT6FB', NULL, '150.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 02:57:53'),
(109, 'OE3P77I88Q91OA4', 'BIXQAYJ3XDFBYZN', 'AEYS0XT6FB', 'NSIHB2WVVN', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 02:57:53'),
(110, 'S4TZR8MS4HYJO1C', 'BIXQAYJ3XDFBYZN', 'IFU2EJUMSR', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:01:43'),
(111, 'S4TZR8MS4HYJO1C', 'BIXQAYJ3XDFBYZN', 'IFU2EJUMSR', '3ZYUJYODR2', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:01:43'),
(112, 'KMW431HG76855QH', 'BIXQAYJ3XDFBYZN', 'TD6SQ1IDPR', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:02:54'),
(113, 'KMW431HG76855QH', 'BIXQAYJ3XDFBYZN', 'TD6SQ1IDPR', 'L97W4Y5T65', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:02:54'),
(114, 'FQHWRZQDA26OVKK', 'BIXQAYJ3XDFBYZN', 'B8P4EGGRAY', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:04:08'),
(115, 'FQHWRZQDA26OVKK', 'BIXQAYJ3XDFBYZN', 'B8P4EGGRAY', 'TCV44HQL19', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:04:08'),
(116, '91CDZX4GY87FT9R', 'BIXQAYJ3XDFBYZN', 'R8OMFVYJXQ', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:05:03'),
(117, '91CDZX4GY87FT9R', 'BIXQAYJ3XDFBYZN', 'R8OMFVYJXQ', 'D5A2LWYGI5', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:05:03'),
(118, 'ZVMINW2RAKCCJ9K', 'BIXQAYJ3XDFBYZN', 'VYMH47RI7Y', NULL, '150.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:05:34'),
(119, 'ZVMINW2RAKCCJ9K', 'BIXQAYJ3XDFBYZN', 'VYMH47RI7Y', 'EXED7HNSX1', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:05:34'),
(120, 'FMSLB6U1LFMD8R8', 'BIXQAYJ3XDFBYZN', '3FQSMSSQDT', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:08:08'),
(121, 'FMSLB6U1LFMD8R8', 'BIXQAYJ3XDFBYZN', '3FQSMSSQDT', 'AWLQDZO3ZV', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:08:08'),
(122, '35EZAQVD9ORALB2', 'BIXQAYJ3XDFBYZN', 'JTXNMIW0DT', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:09:20'),
(123, '35EZAQVD9ORALB2', 'BIXQAYJ3XDFBYZN', 'JTXNMIW0DT', '84ESG1Z1FR', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:09:20'),
(124, 'NGQ3TQ5IZN6X2EB', 'BIXQAYJ3XDFBYZN', 'TJSXGFD1F6', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:11:01'),
(125, 'NGQ3TQ5IZN6X2EB', 'BIXQAYJ3XDFBYZN', 'TJSXGFD1F6', '9S1YK67XMN', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:11:01'),
(126, 'HVJJM7B256Y5T6B', 'BIXQAYJ3XDFBYZN', '1AGGWW2DR6', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:11:40'),
(127, 'HVJJM7B256Y5T6B', 'BIXQAYJ3XDFBYZN', '1AGGWW2DR6', 'OR2FACM5KI', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:11:40'),
(128, 'MFLDBMSJWC3QXK2', 'BIXQAYJ3XDFBYZN', 'FO9Q0CZBAH', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:18:23'),
(129, 'MFLDBMSJWC3QXK2', 'BIXQAYJ3XDFBYZN', 'FO9Q0CZBAH', 'TH5C8D8MDM', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:18:23'),
(130, '455YPJW2E7OEVEE', 'BIXQAYJ3XDFBYZN', 'CYN1QFPGZE', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:22:27'),
(131, '455YPJW2E7OEVEE', 'BIXQAYJ3XDFBYZN', 'CYN1QFPGZE', 'WVC31I46Q3', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:22:27'),
(132, 'J3OSS89CCGIWBQQ', 'BIXQAYJ3XDFBYZN', 'QE2YQFXLDA', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 03:57:40'),
(133, 'J3OSS89CCGIWBQQ', 'BIXQAYJ3XDFBYZN', 'QE2YQFXLDA', 'R427RNN91P', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 03:57:40'),
(134, 'CWIGLTXWUW', '7DNRCVSN8GE2S6W', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-08-27', NULL, 1, 'd', '2019-08-27 04:29:08'),
(135, 'NC7539IIO4UVMEN', 'BIXQAYJ3XDFBYZN', 'JJ75B2XUKG', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-27', NULL, 1, 'd', '2019-08-27 07:52:16'),
(136, 'NC7539IIO4UVMEN', 'BIXQAYJ3XDFBYZN', 'JJ75B2XUKG', '7CH87X1RYS', '50.00', 'Paid by customer', '1', '', '2019-08-27', NULL, 1, 'c', '2019-08-27 07:52:16'),
(137, 'P3QEHEBLYFL8L71', 'fgdfdsf', 'ZNOOWPHIKK', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, 1, 'd', '2019-08-30 23:39:13'),
(138, 'P3QEHEBLYFL8L71', 'fgdfdsf', 'ZNOOWPHIKK', '1J6WO57KSM', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, 1, 'c', '2019-08-30 23:39:13'),
(139, '762KZOSIU1LAXZ8', 'fgdfdsf', 'KYNKY2F1EI', NULL, '35457568.00', 'Purchase by customer', '', '', '2019/08/22', NULL, 1, 'd', '2019-08-30 23:39:26'),
(140, '762KZOSIU1LAXZ8', 'fgdfdsf', 'KYNKY2F1EI', 'KAM48G8DCQ', '345345.00', 'Paid by customer', '1', '', '2019/08/22', NULL, 1, 'c', '2019-08-30 23:39:26'),
(141, '939OSHPFYWOOOYU', '6FGVC6K7OX5L9Z1', 'R6BFIRTLMP', NULL, '0.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-30 23:41:40'),
(142, '939OSHPFYWOOOYU', '6FGVC6K7OX5L9Z1', 'R6BFIRTLMP', 'ZV62YVZTTN', '150.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-30 23:41:40'),
(143, 'QSQ7K44IRXQQ2T1', '6FGVC6K7OX5L9Z1', 'HUJOSOYQPV', NULL, '0.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-30 23:43:00'),
(144, 'QSQ7K44IRXQQ2T1', '6FGVC6K7OX5L9Z1', 'HUJOSOYQPV', '5PKCX2MXH9', '150.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-30 23:43:00'),
(145, 'KMNV6LBX9S255A1', 'fgdfdsf', 'SOQYFD5DVR', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, 1, 'd', '2019-08-30 23:48:59'),
(146, 'KMNV6LBX9S255A1', 'fgdfdsf', 'SOQYFD5DVR', 'F8NWP6V4W8', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, 1, 'c', '2019-08-30 23:48:59'),
(147, '4B6ABYSTPNG8TXC', 'fgdfdsf', 'HBW2WPWYHE', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, 1, 'd', '2019-08-30 23:49:17'),
(148, '4B6ABYSTPNG8TXC', 'fgdfdsf', 'HBW2WPWYHE', 'FQH6NXF6FO', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, 1, 'c', '2019-08-30 23:49:17'),
(149, 'A855CI6XUZJQAKE', 'fgdfdsf', 'EMRLGS5W57', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, 1, 'd', '2019-08-30 23:51:40'),
(150, 'A855CI6XUZJQAKE', 'fgdfdsf', 'EMRLGS5W57', '7AJ4K3J54W', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, 1, 'c', '2019-08-30 23:51:40'),
(151, '43FXE59FSEPPIWW', 'fgdfdsf', 'CEZC44VKAW', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, 1, 'd', '2019-08-30 23:52:07'),
(152, '43FXE59FSEPPIWW', 'fgdfdsf', 'CEZC44VKAW', 'SGDEHX3PEA', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, 1, 'c', '2019-08-30 23:52:07'),
(153, 'NMMF9U8AKQD1QJE', 'fgdfdsf', 'RWKKAW90BR', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, 1, 'd', '2019-08-30 23:53:24'),
(154, 'NMMF9U8AKQD1QJE', 'fgdfdsf', 'RWKKAW90BR', '5MIXYN3EIA', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, 1, 'c', '2019-08-30 23:53:24'),
(155, 'CXYRSY7DS9WZN72', 'fgdfdsf', 'NDQSPVYGQR', NULL, '35457568.00', 'Purchase by customer', '', '', '2019-08-22', NULL, 1, 'd', '2019-08-30 23:53:36'),
(156, 'CXYRSY7DS9WZN72', 'fgdfdsf', 'NDQSPVYGQR', 'ASVFLWMIXP', '345345.00', 'Paid by customer', '1', '', '2019-08-22', NULL, 1, 'c', '2019-08-30 23:53:36'),
(157, 'O6D1QLW2P1FMOW6', 'fgdfdsf', 'WPOGYFR2EJ', NULL, '35457568.00', 'Purchase by customer', '', '', '2019/08/22', NULL, 1, 'd', '2019-08-31 01:11:01'),
(158, 'O6D1QLW2P1FMOW6', 'fgdfdsf', 'WPOGYFR2EJ', 'XW4BUABWET', '345345.00', 'Paid by customer', '1', '', '2019/08/22', NULL, 1, 'c', '2019-08-31 01:11:01'),
(159, 'LUMEF2QHGTNEIO7', '6FGVC6K7OX5L9Z1', 'GHKCY7BNB8', NULL, '1000.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 01:30:13'),
(160, 'LUMEF2QHGTNEIO7', '6FGVC6K7OX5L9Z1', 'GHKCY7BNB8', '62IST1Y5BI', '500.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 01:30:14'),
(161, 'ZI5UU3G5W8IE7GQ', '6FGVC6K7OX5L9Z1', '7FE92ZSJVO', NULL, '0.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 02:30:51'),
(162, 'ZI5UU3G5W8IE7GQ', '6FGVC6K7OX5L9Z1', '7FE92ZSJVO', 'LF23FNEKO3', '150.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 02:30:51'),
(163, '6FJ8HHODBNTISOQ', '6FGVC6K7OX5L9Z1', 'ETAJFQYI61', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 02:36:52'),
(164, '6FJ8HHODBNTISOQ', '6FGVC6K7OX5L9Z1', 'ETAJFQYI61', '9CYA9VNNJV', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 02:36:52'),
(165, 'VOXTI35ACPWGQBP', '6FGVC6K7OX5L9Z1', '4GGW3BKYYX', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 02:37:42'),
(166, 'VOXTI35ACPWGQBP', '6FGVC6K7OX5L9Z1', '4GGW3BKYYX', 'N73LWEQVLX', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 02:37:42'),
(167, 'HH6GBLCIJJ9ZQM2', '6FGVC6K7OX5L9Z1', 'IMXKKV3CWH', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 02:39:16'),
(168, 'HH6GBLCIJJ9ZQM2', '6FGVC6K7OX5L9Z1', 'IMXKKV3CWH', '3LVMRF19JX', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 02:39:16'),
(169, 'WZF1YKA1WQ2S3SU', '6FGVC6K7OX5L9Z1', 'OBRAR2MS1B', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 02:57:39'),
(170, 'WZF1YKA1WQ2S3SU', '6FGVC6K7OX5L9Z1', 'OBRAR2MS1B', '98XGG3HZJK', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 02:57:39'),
(171, 'ETDT9FUGDVM8O14', '6FGVC6K7OX5L9Z1', 'LKHINKOQEZ', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 02:58:07'),
(172, 'ETDT9FUGDVM8O14', '6FGVC6K7OX5L9Z1', 'LKHINKOQEZ', 'GZG2ZUN8OV', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 02:58:07'),
(173, 'K5MFK1FTDELQEFC', '6FGVC6K7OX5L9Z1', 'K5ZUAPRFM3', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 02:59:01'),
(174, 'K5MFK1FTDELQEFC', '6FGVC6K7OX5L9Z1', 'K5ZUAPRFM3', '5NYXOQZKWM', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 02:59:01'),
(175, '1R8DOQDR74PEWMF', '6FGVC6K7OX5L9Z1', 'ZVEQPUB12A', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 03:04:17'),
(176, '1R8DOQDR74PEWMF', '6FGVC6K7OX5L9Z1', 'ZVEQPUB12A', 'GGPXUWQOWM', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 03:04:17'),
(259, 'SEY7BE7IMACFKIZ', '6FGVC6K7OX5L9Z1', '5928235575', '7IWEKC33VJ', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 03:41:26'),
(241, '4FC3T1CMFY65B2T', '6FGVC6K7OX5L9Z1', 'MSSLK5UBVB', NULL, '0.00', 'Purchase by customer', '', '', '2019-09-19', NULL, 1, 'd', '2019-09-18 22:24:51'),
(239, 'UG4ONQNHFXFZJH9', '93K7WTVUD86LBTC', 'KAKERT7VJI', NULL, '0.00', 'Purchase by customer', '', '', '2019-09-19', NULL, 1, 'd', '2019-09-18 22:20:47'),
(240, 'UG4ONQNHFXFZJH9', '93K7WTVUD86LBTC', 'KAKERT7VJI', 'UWSNV21VZC', '400.00', 'Paid by customer', '1', '', '2019-09-19', NULL, 1, 'c', '2019-09-18 22:20:47'),
(181, 'SI7T2A7UM5A5ZZW', '6FGVC6K7OX5L9Z1', 'PGJOTFJMBS', NULL, '100.00', 'Purchase by customer', '', '', '2019-08-31', NULL, 1, 'd', '2019-08-31 03:15:55'),
(182, 'SI7T2A7UM5A5ZZW', '6FGVC6K7OX5L9Z1', 'PGJOTFJMBS', 'IWW255IV5Y', '50.00', 'Paid by customer', '1', '', '2019-08-31', NULL, 1, 'c', '2019-08-31 03:15:55'),
(242, '4FC3T1CMFY65B2T', '6FGVC6K7OX5L9Z1', 'MSSLK5UBVB', 'XNXKUH8UW8', '355.00', 'Paid by customer', '1', '', '2019-09-19', NULL, 1, 'c', '2019-09-18 22:24:51'),
(243, '3BWOZ61PSP6P3R6', '6FGVC6K7OX5L9Z1', 'EEAK1E4KLQ', NULL, '0.00', 'Purchase by customer', '', '', '2019-09-19', NULL, 1, 'd', '2019-09-18 22:26:09'),
(244, '3BWOZ61PSP6P3R6', '6FGVC6K7OX5L9Z1', 'EEAK1E4KLQ', 'G4U6U2LRYB', '500.00', 'Paid by customer', '1', '', '2019-09-19', NULL, 1, 'c', '2019-09-18 22:26:09'),
(245, '6F3CVEXN8F6L7OJ', '6FGVC6K7OX5L9Z1', 'KMS4ECHBZW', NULL, '70000.00', 'Purchase by customer', '', '', '2019-09-19', NULL, 1, 'd', '2019-09-18 22:48:52'),
(246, '6F3CVEXN8F6L7OJ', '6FGVC6K7OX5L9Z1', 'KMS4ECHBZW', '8QJAPIKHOA', '580.00', 'Paid by customer', '1', '', '2019-09-19', NULL, 1, 'c', '2019-09-18 22:48:52'),
(247, '6XMHQKVX3LDM22E', 'BIXQAYJ3XDFBYZN', '5147385423', NULL, '175000.00', 'Purchase by customer', '', '', '2019-09-19', NULL, 1, 'd', '2019-09-19 01:20:32'),
(248, '6XMHQKVX3LDM22E', 'BIXQAYJ3XDFBYZN', '5147385423', 'O823EJ3IXV', '175900.00', 'Paid by customer', '1', '', '2019-09-19', NULL, 1, 'c', '2019-09-19 01:20:32'),
(249, '6XMHQKVX3LDM22E', 'BIXQAYJ3XDFBYZN', '5147385423', NULL, '70000.00', 'Return', '', '', '2019-09-19', NULL, 1, 'c', '2019-09-19 01:22:43'),
(250, 'CR-1', '6', NULL, 'CR-1', '8.00', '', '1', '', '2019-09-19', 'receipt', 1, 'c', '2019-09-19 02:58:08'),
(251, 'CR-2', '6', NULL, 'CR-2', '7.00', '', '1', '', '2019-09-19', 'receipt', 1, 'c', '2019-09-19 02:59:31'),
(252, 'CR-2', '', NULL, 'CR-2', '0.00', 'fsdfsdf', '1', '', '2019-09-19', 'receipt', 1, 'c', '2019-09-19 03:24:55'),
(253, 'CR-3', '', NULL, 'CR-3', '0.00', 'fsdfsdf', '1', '', '2019-09-19', 'receipt', 1, 'c', '2019-09-19 03:58:34'),
(254, 'CR-3', '', NULL, 'CR-3', '0.00', 'fsdfsdf', '1', '', '2019-09-19', 'receipt', 1, 'c', '2019-09-19 04:13:08'),
(255, 'CR-8', '', NULL, 'CR-8', '0.00', 'fsdfsdf', '1', '', '2019-09-19', 'receipt', 1, 'c', '2019-09-19 04:26:18'),
(256, 'F8ZQIVM5WVILDF2', '6FGVC6K7OX5L9Z1', '3QHCHGI5BM', NULL, '0.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-20 23:43:10'),
(257, 'F8ZQIVM5WVILDF2', '6FGVC6K7OX5L9Z1', '3QHCHGI5BM', 'CXQM5ACDZG', '500.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-20 23:43:10'),
(258, 'SEY7BE7IMACFKIZ', '6FGVC6K7OX5L9Z1', '5928235575', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 03:41:26'),
(205, '9HIGVVA6YY', 'MQ49MPE6YAYN9QN', 'NA', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-09-02', NULL, 1, 'd', '2019-09-02 04:38:33'),
(260, 'YQK988EWI6LVZL5', '6FGVC6K7OX5L9Z1', '6852499599', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 10:25:52'),
(261, 'YQK988EWI6LVZL5', '6FGVC6K7OX5L9Z1', '6852499599', 'AXEY8QCF6O', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 10:25:52'),
(262, 'CZXTX9S9BARD9J4', 'BIXQAYJ3XDFBYZN', '7348638525', NULL, '105000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 10:29:00'),
(263, 'CZXTX9S9BARD9J4', 'BIXQAYJ3XDFBYZN', '7348638525', 'L7DSUBE7I6', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 10:29:00'),
(264, 'U6DCWDWEU75XH7P', '6FGVC6K7OX5L9Z1', '6979694511', NULL, '500.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 10:56:58'),
(265, 'U6DCWDWEU75XH7P', '6FGVC6K7OX5L9Z1', '6979694511', 'RDC6HZ5IGT', '500.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 10:56:58'),
(266, 'R915QIBP9J2SPZF', '6FGVC6K7OX5L9Z1', '7675268361', NULL, '500.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 10:58:31'),
(267, 'R915QIBP9J2SPZF', '6FGVC6K7OX5L9Z1', '7675268361', 'CD6T69LNB4', '500.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 10:58:31'),
(268, 'VHWMMJ6IPKFR2NP', '6FGVC6K7OX5L9Z1', '1699845521', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 11:07:56'),
(269, 'VHWMMJ6IPKFR2NP', '6FGVC6K7OX5L9Z1', '1699845521', 'E1M26ROIVJ', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 11:07:56'),
(270, 'SHQV7VGBJM88D52', '6FGVC6K7OX5L9Z1', '3291158273', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 11:14:07'),
(271, 'SHQV7VGBJM88D52', '6FGVC6K7OX5L9Z1', '3291158273', '3AJC7HLCX5', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 11:14:07'),
(272, 'SC6U7J6V3H4S8SC', '6FGVC6K7OX5L9Z1', '7884756971', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 11:15:08'),
(273, 'SC6U7J6V3H4S8SC', '6FGVC6K7OX5L9Z1', '7884756971', 'BOEHGJ4ZTW', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 11:15:08'),
(274, '2L1ZNYLEB91PUP5', '6FGVC6K7OX5L9Z1', '9836928819', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 11:17:09'),
(275, '2L1ZNYLEB91PUP5', '6FGVC6K7OX5L9Z1', '9836928819', 'RA36UP6J2P', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 11:17:10'),
(276, 'CVWYE5KJL3D22O4', '6FGVC6K7OX5L9Z1', '2466279899', NULL, '35000.00', 'Purchase by customer', '', '', '2019-09-21', NULL, 1, 'd', '2019-09-21 11:18:26'),
(277, 'CVWYE5KJL3D22O4', '6FGVC6K7OX5L9Z1', '2466279899', '2E1DA16MIY', '35000.00', 'Paid by customer', '1', '', '2019-09-21', NULL, 1, 'c', '2019-09-21 11:18:26'),
(278, 'AGMXPFZLB5B3EO7', '6FGVC6K7OX5L9Z1', '7126396398', NULL, '514.90', 'Purchase by customer', '', '', '2019-09-24', NULL, 1, 'd', '2019-09-24 12:42:49'),
(279, 'AGMXPFZLB5B3EO7', '6FGVC6K7OX5L9Z1', '7126396398', 'H2U2DFHQBO', '514.90', 'Paid by customer', '1', '', '2019-09-24', NULL, 1, 'c', '2019-09-24 12:42:50'),
(280, 'TIPZHE21WSRO5NF', '6FGVC6K7OX5L9Z1', '3496522264', NULL, '102.98', 'Purchase by customer', '', '', '2019-09-26', NULL, 1, 'd', '2019-09-26 07:02:26'),
(281, 'TIPZHE21WSRO5NF', '6FGVC6K7OX5L9Z1', '3496522264', 'BE1FEKEYQR', '102.98', 'Paid by customer', '1', '', '2019-09-26', NULL, 1, 'c', '2019-09-26 07:02:27'),
(282, 'C8RMSIZMIGUMKGZ', '6FGVC6K7OX5L9Z1', '1962163959', NULL, '102.98', 'Purchase by customer', '', '', '2019-09-26', NULL, 1, 'd', '2019-09-26 07:03:38'),
(283, 'C8RMSIZMIGUMKGZ', '6FGVC6K7OX5L9Z1', '1962163959', 'DTUYA1W6Z1', '102.98', 'Paid by customer', '1', '', '2019-09-26', NULL, 1, 'c', '2019-09-26 07:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `daily_banking_add`
--

CREATE TABLE `daily_banking_add` (
  `banking_id` varchar(255) NOT NULL,
  `date` datetime DEFAULT NULL,
  `bank_id` varchar(100) DEFAULT NULL,
  `deposit_type` varchar(255) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `description` text,
  `amount` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `daily_closing`
--

CREATE TABLE `daily_closing` (
  `closing_id` varchar(255) NOT NULL,
  `last_day_closing` float NOT NULL,
  `cash_in` float NOT NULL,
  `cash_out` float NOT NULL,
  `date` varchar(250) NOT NULL,
  `amount` float NOT NULL,
  `adjustment` float DEFAULT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `daily_closing`
--

INSERT INTO `daily_closing` (`closing_id`, `last_day_closing`, `cash_in`, `cash_out`, `date`, `amount`, `adjustment`, `status`) VALUES
('pNrtAYboRxG4JiA', 0, 0, 0, '2019-08-18', 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(11) NOT NULL,
  `designation` varchar(150) NOT NULL,
  `details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_history`
--

CREATE TABLE `employee_history` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `rate_type` int(11) NOT NULL,
  `hrate` float NOT NULL,
  `email` varchar(50) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `address_line_1` text NOT NULL,
  `address_line_2` text NOT NULL,
  `image` text,
  `country` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_salary_payment`
--

CREATE TABLE `employee_salary_payment` (
  `emp_sal_pay_id` int(11) NOT NULL,
  `generate_id` int(11) NOT NULL,
  `employee_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `total_salary` decimal(18,2) NOT NULL DEFAULT '0.00',
  `total_working_minutes` varchar(50) CHARACTER SET latin1 NOT NULL,
  `working_period` varchar(50) CHARACTER SET latin1 NOT NULL,
  `payment_due` varchar(50) CHARACTER SET latin1 NOT NULL,
  `payment_date` varchar(50) CHARACTER SET latin1 NOT NULL,
  `paid_by` varchar(50) CHARACTER SET latin1 NOT NULL,
  `salary_month` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_salary_setup`
--

CREATE TABLE `employee_salary_setup` (
  `e_s_s_id` int(11) UNSIGNED NOT NULL,
  `employee_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `sal_type` varchar(30) NOT NULL,
  `salary_type_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `create_date` date DEFAULT NULL,
  `update_date` datetime(6) DEFAULT NULL,
  `update_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `gross_salary` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `type` varchar(100) NOT NULL,
  `voucher_no` varchar(50) NOT NULL,
  `amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`id`, `date`, `type`, `voucher_no`, `amount`) VALUES
(1, '2019-06-28', 'Tea biscut', '20190628054055', 350),
(2, '2019-06-28', 'shop rent ', '20190628054109', 450),
(3, '2019-07-01', 'honda-22552500', '20190701062243', 350),
(4, '2019-07-01', 'honda-22552500', '20190701062405', 500);

-- --------------------------------------------------------

--
-- Table structure for table `expense_item`
--

CREATE TABLE `expense_item` (
  `id` int(11) NOT NULL,
  `expense_item_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_item`
--

INSERT INTO `expense_item` (`id`, `expense_item_name`) VALUES
(1, 'shop rent '),
(2, 'Tea biscut'),
(3, 'honda-22552500');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(30) NOT NULL,
  `invoice_id` varchar(100) NOT NULL,
  `customer_id` varchar(100) NOT NULL,
  `date` varchar(50) DEFAULT NULL,
  `total_amount` decimal(18,2) NOT NULL DEFAULT '0.00',
  `prevous_due` decimal(20,2) NOT NULL DEFAULT '0.00',
  `shipping_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `invoice` varchar(255) NOT NULL,
  `invoice_discount` decimal(10,2) DEFAULT '0.00' COMMENT 'invoice discount',
  `total_discount` decimal(10,2) DEFAULT '0.00' COMMENT 'total invoice discount',
  `total_tax` decimal(10,2) DEFAULT '0.00',
  `sales_by` varchar(50) NOT NULL,
  `invoice_details` text NOT NULL,
  `status` int(2) NOT NULL,
  `bank_id` varchar(30) DEFAULT NULL,
  `payment_type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `invoice_id`, `customer_id`, `date`, `total_amount`, `prevous_due`, `shipping_cost`, `invoice`, `invoice_discount`, `total_discount`, `total_tax`, `sales_by`, `invoice_details`, `status`, `bank_id`, `payment_type`) VALUES
(1, '3498244447', 'XWCEM9UVV5NAT5R', '2019-06-25', '200.00', '0.00', '0.00', '1000', '0.00', '0.00', '0.00', '1', 'sdfsdf', 1, NULL, 1),
(2, '4477997231', '6FGVC6K7OX5L9Z1', '2019-06-25', '200.00', '0.00', '0.00', '1001', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(3, '3973611118', '6FGVC6K7OX5L9Z1', '2019-06-25', '500.00', '0.00', '0.00', '1002', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(4, '3923586588', '6FGVC6K7OX5L9Z1', '2019-06-25', '900.00', '0.00', '0.00', '1003', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(5, '6575276926', 'XWCEM9UVV5NAT5R', '2019-06-26', '200.00', '0.00', '0.00', '1004', '0.00', '0.00', '0.00', '1', 'sdfsdf', 1, NULL, 1),
(6, '6643494915', '6FGVC6K7OX5L9Z1', '2019-06-26', '200.00', '0.00', '0.00', '1005', '0.00', '0.00', '0.00', '1', 'sdfsd', 1, NULL, 1),
(7, '1969912464', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1006', '0.00', '0.00', '0.00', '1', 'dfgdfg', 1, NULL, 1),
(8, '6691959977', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1007', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(9, '2258111339', '6FGVC6K7OX5L9Z1', '2019-06-26', '200.00', '0.00', '0.00', '1008', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(10, '9624435359', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1009', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(11, '8668653724', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1010', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(12, '8558184696', '6FGVC6K7OX5L9Z1', '2019-06-26', '500.00', '0.00', '0.00', '1011', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(13, '9147771963', '6FGVC6K7OX5L9Z1', '2019-06-26', '450.00', '0.00', '0.00', '1012', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(14, '3169824785', 'FYJ4EEBMOGTW25T', '2019-06-26', '800.00', '0.00', '0.00', '1013', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(15, '3918198168', '6FGVC6K7OX5L9Z1', '2019-06-26', '1450.00', '0.00', '0.00', '1014', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(16, '5219125479', 'PVGGNNSWOESP45P', '2019-06-26', '450.00', '0.00', '0.00', '1015', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(17, '8394724961', 'PVGGNNSWOESP45P', '2019-06-26', '1450.00', '0.00', '0.00', '1016', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(18, '7986446844', '6FGVC6K7OX5L9Z1', '2019-06-27', '800.00', '0.00', '0.00', '1017', '0.00', '0.00', '0.00', '1', 'sdfds', 1, NULL, 1),
(19, '6789217583', '6FGVC6K7OX5L9Z1', '2019-06-27', '500.00', '0.00', '0.00', '1018', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(20, '4978495577', '6BPL9FJR54PBZJE', '2019-06-27', '950.00', '0.00', '0.00', '1019', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(21, '2687213589', 'KFTUKN7RY1O2W6R', '2019-06-28', '50000.00', '0.00', '0.00', '1020', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(22, '3378453241', '6FGVC6K7OX5L9Z1', '2019-06-28', '500.00', '0.00', '0.00', '1021', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(23, '4458233386', '6FGVC6K7OX5L9Z1', '2019-06-28', '500.00', '0.00', '0.00', '1022', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(24, '1114135497', '6FGVC6K7OX5L9Z1', '2019-06-28', '2210.00', '0.00', '0.00', '1023', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(25, '4441511736', '6FGVC6K7OX5L9Z1', '2019-06-28', '2210.00', '0.00', '0.00', '1024', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(26, '8754679919', '6FGVC6K7OX5L9Z1', '2019-06-28', '50000.00', '0.00', '0.00', '1025', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(27, '3378767423', '6FGVC6K7OX5L9Z1', '2019-06-28', '50000.00', '0.00', '0.00', '1026', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(28, '9317933378', '6FGVC6K7OX5L9Z1', '2019-06-29', '50000.00', '0.00', '0.00', '1027', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(29, '1797561487', '6FGVC6K7OX5L9Z1', '2019-06-29', '36000.00', '0.00', '0.00', '1028', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(30, '9438755655', '6FGVC6K7OX5L9Z1', '2019-06-30', '35000.00', '0.00', '0.00', '1029', '0.00', '0.00', '0.00', '1', 'sdf', 1, NULL, 1),
(31, '5919433886', '6FGVC6K7OX5L9Z1', '2019-06-30', '35000.00', '0.00', '0.00', '1030', '0.00', '0.00', '0.00', '1', 'sdf', 1, NULL, 1),
(32, '2875718763', '6FGVC6K7OX5L9Z1', '2019-07-01', '200.00', '0.00', '0.00', '1031', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(33, '1749144575', '6FGVC6K7OX5L9Z1', '2019-07-02', '200.00', '0.00', '0.00', '1032', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(34, '9815582964', '6FGVC6K7OX5L9Z1', '2019-07-02', '35000.00', '0.00', '0.00', '1033', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(35, '8493678614', '6FGVC6K7OX5L9Z1', '2019-07-02', '455.00', '0.00', '0.00', '1034', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(36, '9691144514', '6FGVC6K7OX5L9Z1', '2019-07-31', '800.00', '0.00', '0.00', '1035', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(37, '5457199982', '6FGVC6K7OX5L9Z1', '2019-07-31', '195465.00', '0.00', '0.00', '1036', '0.00', '0.00', '0.00', '1', 'sdfasdf', 1, NULL, 1),
(38, '2362693843', '6FGVC6K7OX5L9Z1', '2019-07-31', '35000.00', '0.00', '0.00', '1037', '0.00', '0.00', '0.00', '1', 'sdfsdf', 1, NULL, 1),
(39, '5763389422', '6FGVC6K7OX5L9Z1', '2019-07-31', '455.00', '0.00', '0.00', '1038', '0.00', '0.00', '0.00', '1', 'dsfsd', 1, NULL, 1),
(40, '6921223925', '6FGVC6K7OX5L9Z1', '2019-08-01', '50455.00', '0.00', '0.00', '1039', '0.00', '0.00', '0.00', '1', 'dsff', 1, NULL, 1),
(41, '2992478698', '6FGVC6K7OX5L9Z1', '2019-08-01', '50655.00', '0.00', '0.00', '1040', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(42, '2429117215', '6FGVC6K7OX5L9Z1', '2019-08-01', '51855.00', '0.00', '0.00', '1041', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(43, '8636821614', '6FGVC6K7OX5L9Z1', '2019-08-01', '85000.00', '0.00', '0.00', '1042', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(44, '2274125738', '6FGVC6K7OX5L9Z1', '2019-08-01', '50000.00', '0.00', '0.00', '1043', '0.00', '0.00', '0.00', '1', 'gdsfasdf', 1, NULL, 1),
(45, '6881727828', '6FGVC6K7OX5L9Z1', '2019-08-01', '455.00', '0.00', '0.00', '1044', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(46, '6942233633', '6FGVC6K7OX5L9Z1', '2019-08-03', '200.00', '0.00', '0.00', '1045', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(47, '7167684116', '6FGVC6K7OX5L9Z1', '2019-08-03', '2600.00', '0.00', '0.00', '1046', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(48, 'MBJEQBLEDT', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1047', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(49, '3OXFJU9FLE', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1048', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(50, 'AEYS0XT6FB', 'BIXQAYJ3XDFBYZN', '2019-08-27', '150.00', '0.00', '0.00', '1049', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(51, 'IFU2EJUMSR', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1050', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(52, 'TD6SQ1IDPR', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1051', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(53, 'B8P4EGGRAY', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1052', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(54, 'R8OMFVYJXQ', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1053', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(55, 'VYMH47RI7Y', 'BIXQAYJ3XDFBYZN', '2019-08-27', '150.00', '0.00', '0.00', '1054', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(56, '3FQSMSSQDT', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1055', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(57, 'JTXNMIW0DT', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1056', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(58, 'TJSXGFD1F6', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1057', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(59, '1AGGWW2DR6', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1058', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(60, 'FO9Q0CZBAH', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1059', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(61, 'CYN1QFPGZE', 'BIXQAYJ3XDFBYZN', '2019-08-27', '100.00', '0.00', '0.00', '1060', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(62, 'ZNOOWPHIKK', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1061', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(63, 'KYNKY2F1EI', 'fgdfdsf', '2019/08/22', '35457568.00', '0.00', '0.00', '1062', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(64, 'R6BFIRTLMP', '6FGVC6K7OX5L9Z1', '2019-08-31', '0.00', '0.00', '0.00', '1063', '0.00', '100.00', '0.00', '1', '', 1, NULL, 1),
(65, 'HUJOSOYQPV', '6FGVC6K7OX5L9Z1', '2019-08-31', '0.00', '0.00', '0.00', '1064', '0.00', '100.00', '0.00', '1', '', 1, NULL, 1),
(66, 'SOQYFD5DVR', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1065', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(67, 'HBW2WPWYHE', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1066', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(68, 'EMRLGS5W57', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1067', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(69, 'CEZC44VKAW', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1068', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(70, 'RWKKAW90BR', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1069', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(71, 'NDQSPVYGQR', 'fgdfdsf', '2019-08-22', '35457568.00', '0.00', '0.00', '1070', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(72, 'WPOGYFR2EJ', 'fgdfdsf', '2019/08/22', '35457568.00', '0.00', '0.00', '1071', '0.00', '8768.00', '456.00', '1', '', 1, NULL, 1),
(73, 'GHKCY7BNB8', '6FGVC6K7OX5L9Z1', '2019-08-31', '1000.00', '0.00', '0.00', '1072', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(74, '7FE92ZSJVO', '6FGVC6K7OX5L9Z1', '2019-08-31', '0.00', '0.00', '0.00', '1073', '0.00', '10.00', '0.00', '1', '', 1, NULL, 1),
(75, 'ETAJFQYI61', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1074', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(76, '4GGW3BKYYX', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1075', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(77, 'IMXKKV3CWH', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1076', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(78, 'OBRAR2MS1B', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1077', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(79, 'LKHINKOQEZ', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1078', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(80, 'K5ZUAPRFM3', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1079', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(81, 'ZVEQPUB12A', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1080', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(118, 'KAKERT7VJI', '93K7WTVUD86LBTC', '2019-09-19', '0.00', '0.00', '0.00', '1084', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(119, 'MSSLK5UBVB', '6FGVC6K7OX5L9Z1', '2019-09-19', '0.00', '0.00', '0.00', '1085', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(84, 'PGJOTFJMBS', '6FGVC6K7OX5L9Z1', '2019-08-31', '100.00', '0.00', '0.00', '1083', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(120, 'EEAK1E4KLQ', '6FGVC6K7OX5L9Z1', '2019-09-19', '0.00', '0.00', '0.00', '1086', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(121, 'KMS4ECHBZW', '6FGVC6K7OX5L9Z1', '2019-09-19', '70000.00', '0.00', '0.00', '1087', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(122, '5147385423', 'BIXQAYJ3XDFBYZN', '2019-09-19', '175000.00', '900.00', '0.00', '1088', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(123, '3QHCHGI5BM', '6FGVC6K7OX5L9Z1', '2019-09-21', '0.00', '0.00', '0.00', '1089', '0.00', '0.00', '0.00', '1', '', 1, NULL, 1),
(124, '5928235575', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1090', '0.00', '0.00', '0.00', '1', 'sdfsdf', 1, NULL, 1),
(125, '6852499599', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1091', '0.00', '0.00', '0.00', '1', 'sadfasdf', 1, NULL, 1),
(126, '7348638525', 'BIXQAYJ3XDFBYZN', '2019-09-21', '35000.00', '-70000.00', '0.00', '1092', '0.00', '0.00', '0.00', '1', 'sdfsdf', 1, NULL, 1),
(127, '6979694511', '6FGVC6K7OX5L9Z1', '2019-09-21', '500.00', '0.00', '0.00', '1093', '0.00', '0.00', '0.00', '1', 'dsff', 1, NULL, 1),
(128, '7675268361', '6FGVC6K7OX5L9Z1', '2019-09-21', '500.00', '0.00', '0.00', '1094', '0.00', '0.00', '0.00', '1', 'sdfsadf', 1, NULL, 1),
(129, '1699845521', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1095', '0.00', '0.00', '0.00', '1', 'Gui Pos', 1, NULL, 1),
(130, '3291158273', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1096', '0.00', '0.00', '0.00', '1', 'asdfasdf', 1, NULL, 1),
(131, '7884756971', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1097', '0.00', '0.00', '0.00', '1', 'sadsadfasdf', 1, NULL, 1),
(132, '9836928819', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1098', '0.00', '0.00', '0.00', '1', 'sdfasdf', 1, NULL, 1),
(133, '2466279899', '6FGVC6K7OX5L9Z1', '2019-09-21', '35000.00', '0.00', '0.00', '1099', '0.00', '0.00', '0.00', '1', 'sadfasdf', 1, NULL, 1),
(134, '7126396398', '6FGVC6K7OX5L9Z1', '2019-09-24', '514.90', '0.00', '0.00', '1100', '0.00', '0.00', '14.90', '1', 'sdfsad', 1, NULL, 1),
(135, '3496522264', '6FGVC6K7OX5L9Z1', '2019-09-26', '102.98', '0.00', '0.00', '1101', '0.00', '0.00', '2.98', '1', 'sdfsadf', 1, NULL, 1),
(136, '1962163959', '6FGVC6K7OX5L9Z1', '2019-09-26', '102.98', '0.00', '0.00', '1102', '0.00', '0.00', '2.98', '1', 'sdfsadff', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` int(11) NOT NULL,
  `invoice_details_id` varchar(100) NOT NULL,
  `invoice_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `serial_no` varchar(30) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `quantity` decimal(10,2) DEFAULT NULL,
  `rate` decimal(10,2) DEFAULT NULL,
  `supplier_rate` float DEFAULT NULL,
  `total_price` decimal(12,2) DEFAULT NULL,
  `discount` decimal(10,2) DEFAULT NULL,
  `discount_per` varchar(15) DEFAULT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `paid_amount` decimal(12,0) DEFAULT NULL,
  `due_amount` decimal(10,2) DEFAULT NULL,
  `status` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `invoice_details_id`, `invoice_id`, `product_id`, `serial_no`, `description`, `quantity`, `rate`, `supplier_rate`, `total_price`, `discount`, `discount_per`, `tax`, `paid_amount`, `due_amount`, `status`) VALUES
(1, '137575625474673', '3498244447', '15419452', '23', 'sdf', '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '200', '0.00', 1),
(2, '581712284985499', '4477997231', '15419452', NULL, '', '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '200', '0.00', 1),
(3, '143252691329446', '3973611118', '7898940742035', NULL, '', '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(4, '851349838845756', '3923586588', '7898940742035', NULL, NULL, '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '600', '300.00', 1),
(5, '954661845789826', '3923586588', '15419452', NULL, NULL, '2.00', '200.00', 150, '400.00', '0.00', '', NULL, '600', '300.00', 1),
(6, '295222397713239', '6575276926', '15419452', '23', 'sdfsdf', '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '200', '0.00', 1),
(7, '641545319772768', '6643494915', '15419452', NULL, 'dfdfg', '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '200', '0.00', 1),
(8, '611174384339762', '1969912464', '7898940742035', NULL, 'dfgdf', '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(9, '762739765493419', '6691959977', '7898940742035', NULL, NULL, '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(10, '193332584279475', '2258111339', '15419452', NULL, NULL, '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '200', '0.00', 1),
(11, '496655922344734', '9624435359', '7898940742035', NULL, '', '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(12, '487111492258153', '8668653724', '7898940742035', NULL, '', '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(13, '889576258355181', '8558184696', '7898940742035', NULL, '', '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(14, '445417159836199', '9147771963', '4895180728259', NULL, NULL, '1.00', '450.00', 350, '450.00', '0.00', '', NULL, '450', '0.00', 1),
(15, '991859532998638', '3169824785', '76358837', NULL, '', '1.00', '800.00', 390, '800.00', '0.00', '', NULL, '800', '0.00', 1),
(16, '116265385854116', '3918198168', '76358837', NULL, NULL, '1.00', '800.00', 390, '800.00', '0.00', '', NULL, '1450', '0.00', 1),
(17, '329957471171172', '3918198168', '4895180728259', NULL, NULL, '1.00', '450.00', 350, '450.00', '0.00', '', NULL, '1450', '0.00', 1),
(18, '982618147622923', '3918198168', '15419452', NULL, NULL, '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '1450', '0.00', 1),
(19, '519889794784446', '5219125479', '4895180728259', NULL, NULL, '1.00', '450.00', 350, '450.00', '0.00', '', NULL, '450', '0.00', 1),
(20, '481442571474237', '8394724961', '76358837', NULL, NULL, '1.00', '800.00', 390, '800.00', '0.00', '', NULL, '1450', '0.00', 1),
(21, '978899887225483', '8394724961', '4895180728259', NULL, NULL, '1.00', '450.00', 350, '450.00', '0.00', '', NULL, '1450', '0.00', 1),
(22, '932166213359762', '8394724961', '15419452', NULL, NULL, '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '1450', '0.00', 1),
(23, '777521777487153', '7986446844', '76358837', NULL, 'sdfsd', '1.00', '800.00', 390, '800.00', '0.00', '', NULL, '800', '0.00', 1),
(24, '311894743623256', '6789217583', '7898940742035', NULL, '', '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(25, '952438843275488', '4978495577', '7898940742035', NULL, NULL, '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '450.00', 1),
(26, '513259341797653', '4978495577', '4895180728259', NULL, NULL, '1.00', '450.00', 350, '450.00', '0.00', '', NULL, '500', '450.00', 1),
(27, '974464646945377', '2687213589', '19276692', NULL, NULL, '1.00', '50000.00', 40000, '50000.00', '0.00', '', NULL, '30000', '20000.00', 1),
(28, '646777237458793', '3378453241', '7898940742035', NULL, '', '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(29, '591787248295113', '4458233386', '7898940742035', NULL, '', '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '500', '0.00', 1),
(30, '787175611967892', '1114135497', '7898940742035', NULL, NULL, '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '0', '2210.00', 1),
(31, '181795372137992', '1114135497', '4895180728259', NULL, NULL, '2.00', '455.00', 355, '910.00', '0.00', '', NULL, '0', '2210.00', 1),
(32, '361977447434223', '1114135497', '76358837', NULL, NULL, '1.00', '800.00', 390, '800.00', '0.00', '', NULL, '0', '2210.00', 1),
(33, '937664474256695', '4441511736', '7898940742035', NULL, NULL, '1.00', '500.00', 400, '500.00', '0.00', '', NULL, '2210', '0.00', 1),
(34, '744447778986528', '4441511736', '4895180728259', NULL, NULL, '2.00', '455.00', 355, '910.00', '0.00', '', NULL, '2210', '0.00', 1),
(35, '221886224549298', '4441511736', '76358837', NULL, NULL, '1.00', '800.00', 390, '800.00', '0.00', '', NULL, '2210', '0.00', 1),
(36, '246145685723245', '8754679919', '19276692', NULL, NULL, '1.00', '50000.00', 40000, '50000.00', '0.00', '', NULL, '0', '50000.00', 1),
(37, '849363479795543', '3378767423', '19276692', NULL, NULL, '1.00', '50000.00', 40000, '50000.00', '0.00', '', NULL, '50000', '0.00', 1),
(38, '446624776936233', '9317933378', '19276692', 'fgadgaf123213', NULL, '1.00', '50000.00', 40000, '50000.00', '0.00', '', NULL, '0', '50000.00', 1),
(39, '566944469776672', '1797561487', '38296235', NULL, NULL, '1.00', '35000.00', 30000, '35000.00', '0.00', '', NULL, '36000', '0.00', 1),
(40, '595263453284194', '1797561487', '7898940742035', NULL, NULL, '2.00', '500.00', 400, '1000.00', '0.00', '', NULL, '36000', '0.00', 1),
(41, '363928641128468', '9438755655', '38296235', NULL, 'dsf', '1.00', '35000.00', 30000, '35000.00', '0.00', '', NULL, '35000', '0.00', 1),
(42, '365142821263614', '5919433886', '38296235', NULL, 'sdf', '1.00', '35000.00', 30000, '35000.00', '0.00', '', NULL, '35000', '0.00', 1),
(43, '975648728417341', '2875718763', '15419452', NULL, NULL, '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '200', '0.00', 1),
(44, '394779538783591', '1749144575', '15419452', NULL, NULL, '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '200', '0.00', 1),
(45, '333396413857953', '9815582964', '38296235', NULL, NULL, '1.00', '35000.00', 30000, '35000.00', '0.00', '', NULL, '35000', '0.00', 1),
(46, '293897394397456', '8493678614', '4895180728259', NULL, NULL, '1.00', '455.00', 355, '455.00', '0.00', '', NULL, '455', '0.00', 1),
(47, '739137696854459', '9691144514', '76358837', NULL, 'sdfsdf', '1.00', '800.00', 390, '800.00', '0.00', '', NULL, '0', '800.00', 1),
(48, '448379374788561', '5457199982', '4895180728259', NULL, '', '2.00', '455.00', 355, '910.00', '0.00', '', '0.00', '195465', '0.00', 1),
(49, '717685695658682', '5457199982', '76358837', NULL, '', '1.00', '800.00', 390, '800.00', '0.00', '', '0.00', '195465', '0.00', 1),
(50, '865235676939658', '5457199982', '4895180728259', NULL, '', '1.00', '455.00', 355, '455.00', '0.00', '', '0.00', '195465', '0.00', 1),
(51, '662695419219481', '5457199982', '34113672', '123', '', '1.00', '300.00', 250, '300.00', '0.00', '', '0.00', '195465', '0.00', 1),
(52, '878665989248834', '5457199982', '19276692', 'tgart4tt34523423', '', '3.00', '50000.00', 40000, '150000.00', '0.00', '', '0.00', '195465', '0.00', 1),
(53, '994655353945159', '5457199982', '15419452', '23', '', '1.00', '200.00', 150, '200.00', '0.00', '', '0.00', '195465', '0.00', 1),
(54, '741823432513397', '5457199982', '38296235', '645645661411fdf', '', '1.00', '35000.00', 30000, '35000.00', '0.00', '', '0.00', '195465', '0.00', 1),
(55, '625671587752579', '5457199982', 'SN191508964287', 'gp-gstfs31120gntd', '', '3.00', '2600.00', 2000, '7800.00', '0.00', '', '0.00', '195465', '0.00', 1),
(56, '796788549382232', '2362693843', '38296235', '645645661411fdf', '', '1.00', '35000.00', 30000, '35000.00', '0.00', '', NULL, '35000', '0.00', 1),
(58, '315432755456982', '5763389422', '4895180728259', NULL, 'test one', '1.00', '455.00', 355, '455.00', '0.00', '', '0.00', '455', '0.00', 0),
(59, '715713361844324', '6921223925', '4895180728259', NULL, 'sdfsdf', '1.00', '455.00', 355, '455.00', '0.00', '', '0.00', '50455', '0.00', 1),
(60, '513417716724662', '6921223925', '19276692', 'fgadgaf123213', '', '1.00', '50000.00', 40000, '50000.00', '0.00', '', '0.00', '50455', '0.00', 1),
(61, '294424414463898', '2992478698', '15419452', '23', '', '1.00', '200.00', 150, '200.00', '0.00', '', '0.00', '50655', '0.00', 1),
(62, '968484347995985', '2992478698', '4895180728259', NULL, '', '1.00', '455.00', 355, '455.00', '0.00', '', '0.00', '50655', '0.00', 1),
(63, '183163522981473', '2992478698', '19276692', 'tgart4tt34523423', '', '1.00', '50000.00', 40000, '50000.00', '0.00', '', '0.00', '50655', '0.00', 1),
(64, '554872484972156', '2429117215', '15419452', '23', '', '1.00', '200.00', 150, '200.00', '0.00', '', '0.00', '51855', '0.00', 1),
(65, '897194938679971', '2429117215', '19276692', 'tgart4tt34523423', '', '1.00', '50000.00', 40000, '50000.00', '0.00', '', '0.00', '51855', '0.00', 1),
(66, '163854359763219', '2429117215', '34113672', 'werq', '', '4.00', '300.00', 250, '1200.00', '0.00', '', '0.00', '51855', '0.00', 1),
(67, '119869541418933', '2429117215', '4895180728259', NULL, '', '1.00', '455.00', 355, '455.00', '0.00', '', '0.00', '51855', '0.00', 1),
(68, '352582949319253', '8636821614', '38296235', 'reteytregf25436456546', '', '1.00', '35000.00', 30000, '35000.00', '0.00', '', NULL, '85000', '0.00', 1),
(69, '243882731263446', '8636821614', '19276692', 'fgadgaf123213', '', '1.00', '50000.00', 40000, '50000.00', '0.00', '', NULL, '85000', '0.00', 1),
(70, '924532871886139', '2274125738', '19276692', 'fgadgaf123213', '', '1.00', '50000.00', 40000, '50000.00', '0.00', '', NULL, '50000', '0.00', 1),
(71, '372741867629755', '6881727828', '4895180728259', NULL, '', '1.00', '455.00', 355, '455.00', '0.00', '', NULL, '455', '0.00', 1),
(72, '733758996514585', '6942233633', '15419452', NULL, '', '1.00', '200.00', 150, '200.00', '0.00', '', NULL, '0', '200.00', 1),
(73, '682954324848157', '7167684116', 'SN191508964287', NULL, '', '1.00', '2600.00', 2000, '2600.00', '0.00', '', NULL, '2600', '0.00', 1),
(74, 'wJHUPdmchQKkXB0', 'AEYS0XT6FB', '1', NULL, '', '1.00', '1.00', NULL, NULL, '0.00', '0', '0.00', '50', '100.00', 1),
(75, 'Qjm6xyaFuZt2xX', 'IFU2EJUMSR', '1', NULL, '', '1.00', '1.00', NULL, NULL, '0.00', '0', '0.00', '50', '50.00', 1),
(76, 'a9f16CsZEePspfR', 'TD6SQ1IDPR', '1', NULL, NULL, '1.00', '1.00', NULL, NULL, NULL, NULL, NULL, '50', '50.00', 1),
(77, 'f3EXFlJGBZscS2Q', 'TJSXGFD1F6', '15419452', NULL, NULL, '1.00', '100.00', NULL, NULL, NULL, NULL, NULL, '50', '50.00', 1),
(78, 'Zwv6LrUGZ1VzlBg', '1AGGWW2DR6', '15419452', NULL, NULL, '1.00', '100.00', NULL, NULL, NULL, NULL, NULL, '50', '50.00', 1),
(79, 'AByKV0Dp6PeXHLG', 'FO9Q0CZBAH', '15419452', NULL, '', '1.00', '100.00', 150, NULL, '0.00', '0', '0.00', '50', '50.00', 1),
(80, 'bESqWI3ZgAkfQMM', 'CYN1QFPGZE', '15419452', NULL, '', '1.00', '100.00', 150, NULL, '0.00', '0', '0.00', '50', '50.00', 1),
(81, 'wRl0clHgGJhpGra', 'QE2YQFXLDA', '[', NULL, '', '0.00', '0.00', NULL, NULL, '0.00', '0', '0.00', '50', '50.00', 1),
(82, 'RwJCSbcY7ykjbHT', 'JJ75B2XUKG', '15419452', NULL, '', '1.00', '100.00', 150, NULL, '0.00', '0', '0.00', '50', '50.00', 1),
(83, 'K70HLTEXZfyPpFC', 'ZNOOWPHIKK', '15419452', '23', '', '2.00', '200.00', 150, NULL, '0.00', '0', '0.00', '345345', '45658.00', 1),
(84, 'XfttUheTCA4vM9I', 'KYNKY2F1EI', '15419452', '23', '', '2.00', '200.00', 150, NULL, '0.00', '0', '0.00', '345345', '45658.00', 1),
(85, 'qsBTSzI8OtFAWCu', 'R6BFIRTLMP', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '150', '104750.00', 1),
(86, 'uGQ8IHzpCWTgziI', 'HUJOSOYQPV', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '150', '139750.00', 1),
(87, 'dnU22f7QVAP2xSP', 'RWKKAW90BR', '15419452', '23', 'txt fe', '2.00', '200.00', 150, '1000.00', '0.00', '0', '0.00', '345345', '45658.00', 1),
(88, 'eiQMZHDdxG4wlAi', 'NDQSPVYGQR', '15419452', '23', 'txt fe', '2.00', '200.00', 150, '1000.00', '0.00', '0', '0.00', '345345', '45658.00', 1),
(89, 'OruA0rIELSiERnD', 'WPOGYFR2EJ', '15419452', '23', 'txt fe', '2.00', '200.00', 150, '1000.00', '0.00', '0', '0.00', '345345', '45658.00', 1),
(90, 'MiezNEYI4EryXLy', 'GHKCY7BNB8', '7898940742035', '23', 'txt fe', '2.00', '250.00', 400, '1000.00', '0.00', '0', '0.00', '500', '50.00', 1),
(91, 'oDPzw4e7KaBVBE2', '7FE92ZSJVO', '', NULL, 'txt fe', '0.00', '0.00', NULL, '1000.00', '0.00', '0', '0.00', '150', '104840.00', 1),
(92, 'FIIX2MfpLrbrbjk', 'ETAJFQYI61', '', NULL, 'txt fe', '0.00', '0.00', NULL, '1000.00', '0.00', '0', '0.00', '50', '50.00', 1),
(93, 'YdNUNi4g5u9N1Uv', 'ZVEQPUB12A', '7898940742035', '300', '', '2.00', '150.00', 400, '300.00', '0.00', '0', '0.00', '50', '50.00', 1),
(125, 'XwiuiMCiBTJpfDw', 'KAKERT7VJI', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '400', '0.00', 1),
(126, 'Hu1w9UeLLVKdmWJ', 'MSSLK5UBVB', '38296235', 'fgafggfgafg12', '', '4.00', '35000.00', 20, '140000.00', '0.00', '0', '0.00', '355', '139645.00', 1),
(96, 'yHdDd48WG1CuCo4', 'PGJOTFJMBS', '7898940742035', NULL, '', '2.00', '150.00', 400, '300.00', '0.00', '0', '0.00', '50', '50.00', 1),
(127, 'KgyrSAzRlCyNPFH', 'EEAK1E4KLQ', '7898940742035', NULL, '', '4.00', '500.00', 400, '2000.00', '0.00', '0', '0.00', '500', '1500.00', 1),
(128, 'rkJ1dGpTw1vizuh', 'KMS4ECHBZW', '38296235', 'fgafggfgafg12', '', '2.00', '35000.00', 20, '70000.00', '0.00', '0', '0.00', '580', '69420.00', 1),
(129, '159853167587635', '5147385423', '38296235', 'reteytregf25436456546', '', '5.00', '35000.00', 20, '175000.00', '0.00', '', '0.00', '175900', '0.00', 1),
(130, 'P3i9wRrXFWgoqeo', '3QHCHGI5BM', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '500', '104500.00', 1),
(131, '158941674233767', '5928235575', '38296235', NULL, '', '1.00', '35000.00', 20, '35000.00', '0.00', '', '0.00', '35000', '0.00', 1),
(106, '5pjSLQ9R3CPaPBD', 'TA1UKNEC4K', '', NULL, '', '0.00', '0.00', NULL, '0.00', '0.00', '0', '0.00', '0', '0.00', 1),
(132, '923484755832344', '6852499599', '38296235', 'reteytregf25436456546', '', '1.00', '35000.00', 20, '35000.00', '0.00', '', '0.00', '35000', '0.00', 1),
(133, '848742989874436', '7348638525', '38296235', '5234535152112', '', '1.00', '35000.00', 20, '35000.00', '0.00', '', '0.00', '35000', '0.00', 1),
(134, '657776536349848', '6979694511', '7898940742035', NULL, 'sdfdf', '1.00', '500.00', 400, '500.00', '0.00', '', '0.00', '500', '0.00', 1),
(135, '848359468359833', '7675268361', '7898940742035', NULL, 'sdfasd', '1.00', '500.00', 400, '500.00', '0.00', '', '0.00', '500', '0.00', 1),
(136, '563777318557197', '1699845521', '38296235', 'reteytregf25436456546', '', '1.00', '35000.00', 20, '35000.00', '0.00', '', '0.00', '35000', '0.00', 1),
(137, '441699588965154', '3291158273', '38296235', 'reteytregf25436456546', 'sadfasdf', '1.00', '35000.00', 20, '35000.00', '0.00', '', '0.00', '35000', '0.00', 1),
(138, '859187844194547', '7884756971', '38296235', '645645661411fdf', 'sdfsdf', '1.00', '35000.00', 20, '35000.00', '0.00', '', '0.00', '35000', '0.00', 1),
(139, '124189873679981', '9836928819', '38296235', 'reteytregf25436456546', 'sdfsdf', '1.00', '35000.00', 20, '35000.00', '0.00', '', '0.00', '35000', '0.00', 1),
(140, '251542111898817', '2466279899', '38296235', 'reteytregf25436456546', 'sfsdf', '1.00', '35000.00', 20, '35000.00', '0.00', '', '0.00', '35000', '0.00', 1),
(141, '623498975636813', '7126396398', '42156427', 'sfs', 'sdfsdf', '5.00', '100.00', 90, '500.00', '0.00', '', '5.00', '515', '0.00', 1),
(142, '574884884611426', '3496522264', '42156427', 'sfs', 'sdfsda', '1.00', '100.00', 90, '100.00', '0.00', '', '1.00', '103', '0.00', 1),
(143, '893387486338493', '1962163959', '42156427', 'dfsd', 'sdfs', '1.00', '100.00', 90, '100.00', '0.00', '', '1.00', '103', '0.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) UNSIGNED NOT NULL,
  `phrase` text NOT NULL,
  `english` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `phrase`, `english`) VALUES
(1, 'user_profile', 'User Profile'),
(2, 'setting', 'Setting'),
(3, 'language', 'Language'),
(4, 'manage_users', 'Manage Users'),
(5, 'add_user', 'Add User'),
(6, 'manage_company', 'Manage Company'),
(7, 'web_settings', 'Software Settings'),
(8, 'manage_accounts', 'Manage Accounts'),
(9, 'create_accounts', 'Create Account'),
(10, 'manage_bank', 'Manage Bank'),
(11, 'add_new_bank', 'Add New Bank'),
(12, 'settings', 'Bank'),
(13, 'closing_report', 'Closing Report'),
(14, 'closing', 'Closing'),
(15, 'cheque_manager', 'Cheque Manager'),
(16, 'accounts_summary', 'Accounts Summary'),
(17, 'expense', 'Expense'),
(18, 'income', 'Income'),
(19, 'accounts', 'Accounts'),
(20, 'stock_report', 'Stock Report'),
(21, 'stock', 'Stock'),
(22, 'pos_invoice', 'POS Sale'),
(23, 'manage_invoice', 'Manage Sale'),
(24, 'new_invoice', 'New Sale'),
(25, 'invoice', 'Sale'),
(26, 'manage_purchase', 'Manage Purchase'),
(27, 'add_purchase', 'Add Purchase'),
(28, 'purchase', 'Purchase'),
(29, 'paid_customer', 'Paid Customer'),
(30, 'manage_customer', 'Manage Customer'),
(31, 'add_customer', 'Add Customer'),
(32, 'customer', 'Customer'),
(33, 'supplier_payment_actual', 'Supplier Payment Ledger'),
(34, 'supplier_sales_summary', 'Supplier Sales Summary'),
(35, 'supplier_sales_details', 'Supplier Sales Details'),
(36, 'supplier_ledger', 'Supplier Ledger'),
(37, 'manage_supplier', 'Manage Supplier'),
(38, 'add_supplier', 'Add Supplier'),
(39, 'supplier', 'Supplier'),
(40, 'product_statement', 'Product Statement'),
(41, 'manage_product', 'Manage Product'),
(42, 'add_product', 'Add Product'),
(43, 'product', 'Product'),
(44, 'manage_category', 'Manage Category'),
(45, 'add_category', 'Add Category'),
(46, 'category', 'Category'),
(47, 'sales_report_product_wise', 'Sales Report (Product Wise)'),
(48, 'purchase_report', 'Purchase Report'),
(49, 'sales_report', 'Sales Report'),
(50, 'todays_report', 'Todays Report'),
(51, 'report', 'Report'),
(52, 'dashboard', 'Dashboard'),
(53, 'online', 'Online'),
(54, 'logout', 'Logout'),
(55, 'change_password', 'Change Password'),
(56, 'total_purchase', 'Total Purchase'),
(57, 'total_amount', 'Total Amount'),
(58, 'supplier_name', 'Supplier Name'),
(59, 'invoice_no', 'Invoice No'),
(60, 'purchase_date', 'Purchase Date'),
(61, 'todays_purchase_report', 'Todays Purchase Report'),
(62, 'total_sales', 'Total Sales'),
(63, 'customer_name', 'Customer Name'),
(64, 'sales_date', 'Sales Date'),
(65, 'todays_sales_report', 'Todays Sales Report'),
(66, 'home', 'Home'),
(67, 'todays_sales_and_purchase_report', 'Todays sales and purchase report'),
(68, 'total_ammount', 'Total Amount'),
(69, 'rate', 'Rate'),
(70, 'product_model', 'Product Model'),
(71, 'product_name', 'Product Name'),
(72, 'search', 'Search'),
(73, 'end_date', 'End Date'),
(74, 'start_date', 'Start Date'),
(75, 'total_purchase_report', 'Total Purchase Report'),
(76, 'total_sales_report', 'Total Sales Report'),
(77, 'total_seles', 'Total Sales'),
(78, 'all_stock_report', 'All Stock Report'),
(79, 'search_by_product', 'Search By Product'),
(80, 'date', 'Date'),
(81, 'print', 'Print'),
(82, 'stock_date', 'Stock Date'),
(83, 'print_date', 'Print Date'),
(84, 'sales', 'Sales'),
(85, 'price', 'Price'),
(86, 'sl', 'SL.'),
(87, 'add_new_category', 'Add new category'),
(88, 'category_name', 'Category Name'),
(89, 'save', 'Save'),
(90, 'delete', 'Delete'),
(91, 'update', 'Update'),
(92, 'action', 'Action'),
(93, 'manage_your_category', 'Manage your category '),
(94, 'category_edit', 'Category Edit'),
(95, 'status', 'Status'),
(96, 'active', 'Active'),
(97, 'inactive', 'Inactive'),
(98, 'save_changes', 'Save Changes'),
(99, 'save_and_add_another', 'Save And Add Another'),
(100, 'model', 'Model'),
(101, 'supplier_price', 'Supplier Price'),
(102, 'sell_price', 'Sale Price'),
(103, 'image', 'Image'),
(104, 'select_one', 'Select One'),
(105, 'details', 'Details'),
(106, 'new_product', 'New Product'),
(107, 'add_new_product', 'Add new product'),
(108, 'barcode', 'Barcode'),
(109, 'qr_code', 'Qr-Code'),
(110, 'product_details', 'Product Details'),
(111, 'manage_your_product', 'Manage your product'),
(112, 'product_edit', 'Product Edit'),
(113, 'edit_your_product', 'Edit your product'),
(114, 'cancel', 'Cancel'),
(115, 'incl_vat', 'Incl. Vat'),
(116, 'money', 'TK'),
(117, 'grand_total', 'Grand Total'),
(118, 'quantity', 'Qnty'),
(119, 'product_report', 'Product Report'),
(120, 'product_sales_and_purchase_report', 'Product sales and purchase report'),
(121, 'previous_stock', 'Previous Stock'),
(122, 'out', 'Out'),
(123, 'in', 'In'),
(124, 'to', 'To'),
(125, 'previous_balance', 'Previous Credit Balance'),
(126, 'customer_address', 'Customer Address'),
(127, 'customer_mobile', 'Customer Mobile'),
(128, 'customer_email', 'Customer Email'),
(129, 'add_new_customer', 'Add new customer'),
(130, 'balance', 'Balance'),
(131, 'mobile', 'Mobile'),
(132, 'address', 'Address'),
(133, 'manage_your_customer', 'Manage your customer'),
(134, 'customer_edit', 'Customer Edit'),
(135, 'paid_customer_list', 'Paid Customer List'),
(136, 'ammount', 'Amount'),
(137, 'customer_ledger', 'Customer Ledger'),
(138, 'manage_customer_ledger', 'Manage Customer Ledger'),
(139, 'customer_information', 'Customer Information'),
(140, 'debit_ammount', 'Debit Amount'),
(141, 'credit_ammount', 'Credit Amount'),
(142, 'balance_ammount', 'Balance Amount'),
(143, 'receipt_no', 'Receipt NO'),
(144, 'description', 'Description'),
(145, 'debit', 'Debit'),
(146, 'credit', 'Credit'),
(147, 'item_information', 'Item Information'),
(148, 'total', 'Total'),
(149, 'please_select_supplier', 'Please Select Supplier'),
(150, 'submit', 'Submit'),
(151, 'submit_and_add_another', 'Submit And Add Another One'),
(152, 'add_new_item', 'Add New Item'),
(153, 'manage_your_purchase', 'Manage your purchase'),
(154, 'purchase_edit', 'Purchase Edit'),
(155, 'purchase_ledger', 'Purchase Ledger'),
(156, 'invoice_information', 'Sale Information'),
(157, 'paid_ammount', 'Paid Amount'),
(158, 'discount', 'Dis./Pcs.'),
(159, 'save_and_paid', 'Save And Paid'),
(160, 'payee_name', 'Payee Name'),
(161, 'manage_your_invoice', 'Manage your Sale'),
(162, 'invoice_edit', 'Sale Edit'),
(163, 'new_pos_invoice', 'New POS Sale'),
(164, 'add_new_pos_invoice', 'Add new pos Sale'),
(165, 'product_id', 'Product ID'),
(166, 'paid_amount', 'Paid Amount'),
(167, 'authorised_by', 'Authorised By'),
(168, 'checked_by', 'Checked By'),
(169, 'received_by', 'Received By'),
(170, 'prepared_by', 'Prepared By'),
(171, 'memo_no', 'Memo No'),
(172, 'website', 'Website'),
(173, 'email', 'Email'),
(174, 'invoice_details', 'Sale Details'),
(175, 'reset', 'Reset'),
(176, 'payment_account', 'Payment Account'),
(177, 'bank_name', 'Bank Name'),
(178, 'cheque_or_pay_order_no', 'Cheque/Pay Order No'),
(179, 'payment_type', 'Payment Type'),
(180, 'payment_from', 'Payment From'),
(181, 'payment_date', 'Payment Date'),
(182, 'add_income', 'Add Income'),
(183, 'cash', 'Cash'),
(184, 'cheque', 'Cheque'),
(185, 'pay_order', 'Pay Order'),
(186, 'payment_to', 'Payment To'),
(187, 'total_outflow_ammount', 'Total Expense Amount'),
(188, 'transections', 'Transections'),
(189, 'accounts_name', 'Accounts Name'),
(190, 'outflow_report', 'Expense Report'),
(191, 'inflow_report', 'Income Report'),
(192, 'all', 'All'),
(193, 'account', 'Account'),
(194, 'from', 'From'),
(195, 'account_summary_report', 'Account Summary Report'),
(196, 'search_by_date', 'Search By Date'),
(197, 'cheque_no', 'Cheque No'),
(198, 'name', 'Name'),
(199, 'closing_account', 'Closing Account'),
(200, 'close_your_account', 'Close your account'),
(201, 'last_day_closing', 'Last Day Closing'),
(202, 'cash_in', 'Cash In'),
(203, 'cash_out', 'Cash Out'),
(204, 'cash_in_hand', 'Cash In Hand'),
(205, 'add_new_bank', 'Add New Bank'),
(206, 'day_closing', 'Day Closing'),
(207, 'account_closing_report', 'Account Closing Report'),
(208, 'last_day_ammount', 'Last Day Amount'),
(209, 'adjustment', 'Adjustment'),
(210, 'pay_type', 'Pay Type'),
(211, 'customer_or_supplier', 'Customer,Supplier Or Others'),
(212, 'transection_id', 'Transections ID'),
(213, 'accounts_summary_report', 'Accounts Summary Report'),
(214, 'bank_list', 'Bank List'),
(215, 'bank_edit', 'Bank Edit'),
(216, 'debit_plus', 'Debit (+)'),
(217, 'credit_minus', 'Credit (-)'),
(218, 'account_name', 'Account Name'),
(219, 'account_type', 'Account Type'),
(220, 'account_real_name', 'Account Real Name'),
(221, 'manage_account', 'Manage Account'),
(222, 'company_name', 'Niha International'),
(223, 'edit_your_company_information', 'Edit your company information'),
(224, 'company_edit', 'Company Edit'),
(225, 'admin', 'Admin'),
(226, 'user', 'User'),
(227, 'password', 'Password'),
(228, 'last_name', 'Last Name'),
(229, 'first_name', 'First Name'),
(230, 'add_new_user_information', 'Add new user information'),
(231, 'user_type', 'User Type'),
(232, 'user_edit', 'User Edit'),
(233, 'rtr', 'RTR'),
(234, 'ltr', 'LTR'),
(235, 'ltr_or_rtr', 'LTR/RTR'),
(236, 'footer_text', 'Footer Text'),
(237, 'favicon', 'Favicon'),
(238, 'logo', 'Logo'),
(239, 'update_setting', 'Update Setting'),
(240, 'update_your_web_setting', 'Update your web setting'),
(241, 'login', 'Login'),
(242, 'your_strong_password', 'Your strong password'),
(243, 'your_unique_email', 'Your unique email'),
(244, 'please_enter_your_login_information', 'Please enter your login information.'),
(245, 'update_profile', 'Update Profile'),
(246, 'your_profile', 'Your Profile'),
(247, 're_type_password', 'Re-Type Password'),
(248, 'new_password', 'New Password'),
(249, 'old_password', 'Old Password'),
(250, 'new_information', 'New Information'),
(251, 'old_information', 'Old Information'),
(252, 'change_your_information', 'Change your information'),
(253, 'change_your_profile', 'Change your profile'),
(254, 'profile', 'Profile'),
(255, 'wrong_username_or_password', 'Wrong User Name Or Password !'),
(256, 'successfully_updated', 'Successfully Updated.'),
(257, 'blank_field_does_not_accept', 'Blank Field Does Not Accept !'),
(258, 'successfully_changed_password', 'Successfully changed password.'),
(259, 'you_are_not_authorised_person', 'You are not authorised person !'),
(260, 'password_and_repassword_does_not_match', 'Passwor and re-password does not match !'),
(261, 'new_password_at_least_six_character', 'New Password At Least 6 Character.'),
(262, 'you_put_wrong_email_address', 'You put wrong email address !'),
(263, 'cheque_ammount_asjusted', 'Cheque amount adjusted.'),
(264, 'successfully_payment_paid', 'Successfully Payment Paid.'),
(265, 'successfully_added', 'Successfully Added.'),
(266, 'successfully_updated_2_closing_ammount_not_changeale', 'Successfully Updated -2. Note: Closing Amount Not Changeable.'),
(267, 'successfully_payment_received', 'Successfully Payment Received.'),
(268, 'already_inserted', 'Already Inserted !'),
(269, 'successfully_delete', 'Successfully Delete.'),
(270, 'successfully_created', 'Successfully Created.'),
(271, 'logo_not_uploaded', 'Logo not uploaded !'),
(272, 'favicon_not_uploaded', 'Favicon not uploaded !'),
(273, 'supplier_mobile', 'Supplier Mobile'),
(274, 'supplier_address', 'Supplier Address'),
(275, 'supplier_details', 'Supplier Details'),
(276, 'add_new_supplier', 'Add New Supplier'),
(277, 'manage_suppiler', 'Manage Supplier'),
(278, 'manage_your_supplier', 'Manage your supplier'),
(279, 'manage_supplier_ledger', 'Manage supplier ledger'),
(280, 'invoice_id', 'Invoice ID'),
(281, 'deposite_id', 'Deposite ID'),
(282, 'supplier_actual_ledger', 'Supplier Payment Ledger'),
(283, 'supplier_information', 'Supplier Information'),
(284, 'event', 'Event'),
(285, 'add_new_income', 'Add New Income'),
(286, 'add_expese', 'Add Expense'),
(287, 'add_new_expense', 'Add New Expense'),
(288, 'total_inflow_ammount', 'Total Income Amount'),
(289, 'create_new_invoice', 'Create New Sale'),
(290, 'create_pos_invoice', 'Create POS Sale'),
(291, 'total_profit', 'Total Profit'),
(292, 'monthly_progress_report', 'Monthly Progress Report'),
(293, 'total_invoice', 'Total Sale'),
(294, 'account_summary', 'Account Summary'),
(295, 'total_supplier', 'Total Supplier'),
(296, 'total_product', 'Total Product'),
(297, 'total_customer', 'Total Customer'),
(298, 'supplier_edit', 'Supplier Edit'),
(299, 'add_new_invoice', 'Add New Sale'),
(300, 'add_new_purchase', 'Add new purchase'),
(301, 'currency', 'Currency'),
(302, 'currency_position', 'Currency Position'),
(303, 'left', 'Left'),
(304, 'right', 'Right'),
(305, 'add_tax', 'Add Tax'),
(306, 'manage_tax', 'Manage Tax'),
(307, 'add_new_tax', 'Add new tax'),
(308, 'enter_tax', 'Enter Tax'),
(309, 'already_exists', 'Already Exists !'),
(310, 'successfully_inserted', 'Successfully Inserted.'),
(311, 'tax', 'Tax'),
(312, 'tax_edit', 'Tax Edit'),
(313, 'product_not_added', 'Product not added !'),
(314, 'total_tax', 'Total Tax'),
(315, 'manage_your_supplier_details', 'Manage your supplier details.'),
(316, 'invoice_description', 'Lorem Ipsum is sim ply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is sim ply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy Lorem Ipsum is simply dummy'),
(317, 'thank_you_for_choosing_us', 'Thank you very much for choosing us.'),
(318, 'billing_date', 'Billing Date'),
(319, 'billing_to', 'Billing To'),
(320, 'billing_from', 'Billing From'),
(321, 'you_cant_delete_this_product', 'Sorry !!  You can\'t delete this product.This product already used in calculation system!'),
(322, 'old_customer', 'Old Customer'),
(323, 'new_customer', 'New Customer'),
(324, 'new_supplier', 'New Supplier'),
(325, 'old_supplier', 'Old Supplier'),
(326, 'credit_customer', 'Credit Customer'),
(327, 'account_already_exists', 'This Account Already Exists !'),
(328, 'edit_income', 'Edit Income'),
(329, 'you_are_not_access_this_part', 'You are not authorised person !'),
(330, 'account_edit', 'Account Edit'),
(331, 'due', 'Due'),
(332, 'expense_edit', 'Expense Edit'),
(333, 'please_select_customer', 'Please select customer !'),
(334, 'profit_report', 'Profit Report (Sale Wise)'),
(335, 'total_profit_report', 'Total profit report'),
(336, 'please_enter_valid_captcha', 'Please enter valid captcha.'),
(337, 'category_not_selected', 'Category not selected.'),
(338, 'supplier_not_selected', 'Supplier not selected.'),
(339, 'please_select_product', 'Please select product.'),
(340, 'product_model_already_exist', 'Product model already exist !'),
(341, 'invoice_logo', 'Sale Logo'),
(342, 'available_qnty', 'Av. Qnty.'),
(343, 'you_can_not_buy_greater_than_available_cartoon', 'You can not select grater than available cartoon !'),
(344, 'customer_details', 'Customer details'),
(345, 'manage_customer_details', 'Manage customer details.'),
(346, 'site_key', 'Captcha Site Key'),
(347, 'secret_key', 'Captcha Secret Key'),
(348, 'captcha', 'Captcha'),
(349, 'cartoon_quantity', 'Cartoon Quantity'),
(350, 'total_cartoon', 'Total Cartoon'),
(351, 'cartoon', 'Cartoon'),
(352, 'item_cartoon', 'Item/Cartoon'),
(353, 'product_and_supplier_did_not_match', 'Product and supplier did not match !'),
(354, 'if_you_update_purchase_first_select_supplier_then_product_and_then_quantity', 'If you update purchase,first select supplier then product and then update qnty.'),
(355, 'item', 'Item'),
(356, 'manage_your_credit_customer', 'Manage your credit customer'),
(357, 'total_quantity', 'Total Quantity'),
(358, 'quantity_per_cartoon', 'Quantity per cartoon'),
(359, 'barcode_qrcode_scan_here', 'Barcode or QR-code scan here'),
(360, 'synchronizer_setting', 'Synchronizer Setting'),
(361, 'data_synchronizer', 'Data Synchronizer'),
(362, 'hostname', 'Host name'),
(363, 'username', 'User Name'),
(364, 'ftp_port', 'FTP Port'),
(365, 'ftp_debug', 'FTP Debug'),
(366, 'project_root', 'Project Root'),
(367, 'please_try_again', 'Please try again'),
(368, 'save_successfully', 'Save successfully'),
(369, 'synchronize', 'Synchronize'),
(370, 'internet_connection', 'Internet Connection'),
(371, 'outgoing_file', 'Outgoing File'),
(372, 'incoming_file', 'Incoming File'),
(373, 'ok', 'Ok'),
(374, 'not_available', 'Not Available'),
(375, 'available', 'Available'),
(376, 'download_data_from_server', 'Download data from server'),
(377, 'data_import_to_database', 'Data import to database'),
(378, 'data_upload_to_server', 'Data uplod to server'),
(379, 'please_wait', 'Please Wait'),
(380, 'ooops_something_went_wrong', 'Oooops Something went wrong !'),
(381, 'upload_successfully', 'Upload successfully'),
(382, 'unable_to_upload_file_please_check_configuration', 'Unable to upload file please check configuration'),
(383, 'please_configure_synchronizer_settings', 'Please configure synchronizer settings'),
(384, 'download_successfully', 'Download successfully'),
(385, 'unable_to_download_file_please_check_configuration', 'Unable to download file please check configuration'),
(386, 'data_import_first', 'Data import past'),
(387, 'data_import_successfully', 'Data import successfully'),
(388, 'unable_to_import_data_please_check_config_or_sql_file', 'Unable to import data please check config or sql file'),
(389, 'total_sale_ctn', 'Total Sale Ctn'),
(390, 'in_qnty', 'In Qnty.'),
(391, 'out_qnty', 'Out Qnty.'),
(392, 'stock_report_supplier_wise', 'Stock Report (Supplier Wise)'),
(393, 'all_stock_report_supplier_wise', 'Stock Report (Suppler Wise)'),
(394, 'select_supplier', 'Select Supplier'),
(395, 'stock_report_product_wise', 'Stock Report (Product Wise)'),
(396, 'phone', 'Phone'),
(397, 'select_product', 'Select Product'),
(398, 'in_quantity', 'In Qnty.'),
(399, 'out_quantity', 'Out Qnty.'),
(400, 'in_taka', 'In TK.'),
(401, 'out_taka', 'Out TK.'),
(402, 'commission', 'Commission'),
(403, 'generate_commission', 'Generate Commssion'),
(404, 'commission_rate', 'Commission Rate'),
(405, 'total_ctn', 'Total Ctn.'),
(406, 'per_pcs_commission', 'Per PCS Commission'),
(407, 'total_commission', 'Total Commission'),
(408, 'enter', 'Enter'),
(409, 'please_add_walking_customer_for_default_customer', 'Please add \'Walking Customer\' for default customer.'),
(410, 'supplier_ammount', 'Supplier Amount'),
(411, 'my_sale_ammount', 'My Sale Amount'),
(412, 'signature_pic', 'Signature Picture'),
(413, 'branch', 'Branch'),
(414, 'ac_no', 'A/C Number'),
(415, 'ac_name', 'A/C Name'),
(416, 'bank_transaction', 'Bank Transaction'),
(417, 'bank', 'Bank'),
(418, 'withdraw_deposite_id', 'Withdraw / Deposite ID'),
(419, 'bank_ledger', 'Bank Ledger'),
(420, 'note_name', 'Note Name'),
(421, 'pcs', 'Pcs.'),
(422, '1', '1'),
(423, '2', '2'),
(424, '5', '5'),
(425, '10', '10'),
(426, '20', '20'),
(427, '50', '50'),
(428, '100', '100'),
(429, '500', '500'),
(430, '1000', '1000'),
(431, 'total_discount', 'Total Discount'),
(432, 'product_not_found', 'Product not found !'),
(433, 'this_is_not_credit_customer', 'This is not credit customer !'),
(434, 'personal_loan', 'Personal Loan'),
(435, 'add_person', 'Add Person'),
(436, 'add_loan', 'Add Loan'),
(437, 'add_payment', 'Add Payment'),
(438, 'manage_person', 'Manage Person'),
(439, 'personal_edit', 'Person Edit'),
(440, 'person_ledger', 'Person Ledger'),
(441, 'backup_restore', 'Backup '),
(442, 'database_backup', 'Database backup'),
(443, 'file_information', 'File information'),
(444, 'filename', 'Filename'),
(445, 'size', 'Size'),
(446, 'backup_date', 'Backup date'),
(447, 'backup_now', 'Backup now'),
(448, 'restore_now', 'Restore now'),
(449, 'are_you_sure', 'Are you sure ?'),
(450, 'download', 'Download'),
(451, 'backup_and_restore', 'Backup'),
(452, 'backup_successfully', 'Backup successfully'),
(453, 'delete_successfully', 'Delete successfully'),
(454, 'stock_ctn', 'Stock/Qnt'),
(455, 'unit', 'Unit'),
(456, 'meter_m', 'Meter (M)'),
(457, 'piece_pc', 'Piece (Pc)'),
(458, 'kilogram_kg', 'Kilogram (Kg)'),
(459, 'stock_cartoon', 'Stock Cartoon'),
(460, 'add_product_csv', 'Add Product (CSV)'),
(461, 'import_product_csv', 'Import product (CSV)'),
(462, 'close', 'Close'),
(463, 'download_example_file', 'Download example file.'),
(464, 'upload_csv_file', 'Upload CSV File'),
(465, 'csv_file_informaion', 'CSV File Information'),
(466, 'out_of_stock', 'Out Of Stock'),
(467, 'others', 'Others'),
(468, 'full_paid', 'Full Paid'),
(469, 'successfully_saved', 'Your Data Successfully Saved'),
(470, 'manage_loan', 'Manage Loan'),
(471, 'receipt', 'Receipt'),
(472, 'payment', 'Payment'),
(473, 'cashflow', 'Daily Cash Flow'),
(474, 'signature', 'Signature'),
(475, 'supplier_reports', 'Supplier Reports'),
(476, 'generate', 'Generate'),
(477, 'todays_overview', 'Todays Overview'),
(478, 'last_sales', 'Last Sales'),
(479, 'manage_transaction', 'Manage Transaction'),
(480, 'daily_summary', 'Daily Summary'),
(481, 'daily_cash_flow', 'Daily Cash Flow'),
(482, 'custom_report', 'Custom Report'),
(483, 'transaction', 'Transaction'),
(484, 'receipt_amount', 'Receipt Amount'),
(485, 'transaction_details_datewise', 'Transaction Details Datewise'),
(486, 'cash_closing', 'Cash Closing'),
(487, 'you_can_not_buy_greater_than_available_qnty', 'You can not buy greater than available qnty.'),
(488, 'supplier_id', 'Supplier ID'),
(489, 'category_id', 'Category ID'),
(490, 'select_report', 'Select Report'),
(491, 'supplier_summary', 'Supplier summary'),
(492, 'sales_payment_actual', 'Sales payment actual'),
(493, 'today_already_closed', 'Today already closed.'),
(494, 'root_account', 'Root Account'),
(495, 'office', 'Office'),
(496, 'loan', 'Loan'),
(497, 'transaction_mood', 'Transaction Mood'),
(498, 'select_account', 'Select Account'),
(499, 'add_receipt', 'Add Receipt'),
(500, 'update_transaction', 'Update Transaction'),
(501, 'no_stock_found', 'No Stock Found !'),
(502, 'admin_login_area', 'Admin Login Area'),
(503, 'print_qr_code', 'Print QR Code'),
(504, 'discount_type', 'Discount Type'),
(505, 'discount_percentage', 'Discount'),
(506, 'fixed_dis', 'Fixed Dis.'),
(507, 'return', 'Return'),
(508, 'stock_return_list', 'Stock Return List'),
(509, 'wastage_return_list', 'Wastage Return List'),
(510, 'return_invoice', 'Sale Return'),
(511, 'sold_qty', 'Sold Qty'),
(512, 'ret_quantity', 'Return Qty'),
(513, 'deduction', 'Deduction'),
(514, 'check_return', 'Check Return'),
(515, 'reason', 'Reason'),
(516, 'usablilties', 'Usability'),
(517, 'adjs_with_stck', 'Adjust With Stock'),
(518, 'return_to_supplier', 'Return To Supplier'),
(519, 'wastage', 'Wastage'),
(520, 'to_deduction', 'Total Deduction '),
(521, 'nt_return', 'Net Return Amount'),
(522, 'return_list', 'Return List'),
(523, 'add_return', 'Add Return'),
(524, 'per_qty', 'Purchase Qty'),
(525, 'return_supplier', 'Supplier Return'),
(526, 'stock_purchase', 'Stock Purchase Price'),
(527, 'stock_sale', 'Stock Sale Price'),
(528, 'supplier_return', 'Supplier Return'),
(529, 'purchase_id', 'Purchase ID'),
(530, 'return_id', 'Return ID'),
(531, 'supplier_return_list', 'Supplier Return List'),
(532, 'c_r_slist', 'Stock Return Stock'),
(533, 'wastage_list', 'Wastage List'),
(534, 'please_input_correct_invoice_id', 'Please Input a Correct Sale ID'),
(535, 'please_input_correct_purchase_id', 'Please Input Your Correct  Purchase ID'),
(536, 'add_more', 'Add More'),
(537, 'prouct_details', 'Product Details'),
(538, 'prouct_detail', 'Product Details'),
(539, 'stock_return', 'Stock Return'),
(540, 'choose_transaction', 'Select Transaction'),
(541, 'transection_category', 'Select  Category'),
(542, 'transaction_categry', 'Select Category'),
(543, 'search_supplier', 'Search Supplier'),
(544, 'customer_id', 'Customer ID'),
(545, 'search_customer', 'Search Customer Invoice'),
(546, 'serial_no', 'SN'),
(547, 'item_discount', 'Item Discount'),
(548, 'invoice_discount', 'Sale Discount'),
(549, 'add_unit', 'Add Unit'),
(550, 'manage_unit', 'Manage Unit'),
(551, 'add_new_unit', 'Add New Unit'),
(552, 'unit_name', 'Unit Name'),
(553, 'payment_amount', 'Payment Amount'),
(554, 'manage_your_unit', 'Manage Your Unit'),
(555, 'unit_id', 'Unit ID'),
(556, 'unit_edit', 'Unit Edit'),
(557, 'vat', 'Vat'),
(558, 'sales_report_category_wise', 'Sales Report (Category wise)'),
(559, 'purchase_report_category_wise', 'Purchase Report (Category wise)'),
(560, 'category_wise_purchase_report', 'Category wise purchase report'),
(561, 'category_wise_sales_report', 'Category wise sales report'),
(562, 'best_sale_product', 'Best Sale Product'),
(563, 'all_best_sales_product', 'All Best Sales Products'),
(564, 'todays_customer_receipt', 'Todays Customer Receipt'),
(565, 'not_found', 'Record not found'),
(566, 'collection', 'Collection'),
(567, 'increment', 'Increment'),
(568, 'accounts_tree_view', 'Accounts Tree View'),
(569, 'debit_voucher', 'Debit Voucher'),
(570, 'voucher_no', 'Voucher No'),
(571, 'credit_account_head', 'Credit Account Head'),
(572, 'remark', 'Remark'),
(573, 'code', 'Code'),
(574, 'amount', 'Amount'),
(575, 'approved', 'Approved'),
(576, 'debit_account_head', 'Debit Account Head'),
(577, 'credit_voucher', 'Credit Voucher'),
(578, 'find', 'Find'),
(579, 'transaction_date', 'Transaction Date'),
(580, 'voucher_type', 'Voucher Type'),
(581, 'particulars', 'Particulars'),
(582, 'with_details', 'With Details'),
(583, 'general_ledger', 'General Ledger'),
(584, 'general_ledger_of', 'General ledger of'),
(585, 'pre_balance', 'Pre Balance'),
(586, 'current_balance', 'Current Balance'),
(587, 'to_date', 'To Date'),
(588, 'from_date', 'From Date'),
(589, 'trial_balance', 'Trial Balance'),
(590, 'authorized_signature', 'Authorized Signature'),
(591, 'chairman', 'Chairman'),
(592, 'total_income', 'Total Income'),
(593, 'statement_of_comprehensive_income', 'Statement of Comprehensive Income'),
(594, 'profit_loss', 'Profit Loss'),
(595, 'cash_flow_report', 'Cash Flow Report'),
(596, 'cash_flow_statement', 'Cash Flow Statement'),
(597, 'amount_in_dollar', 'Amount In Dollar'),
(598, 'opening_cash_and_equivalent', 'Opening Cash and Equivalent'),
(599, 'coa_print', 'Coa Print'),
(600, 'cash_flow', 'Cash Flow'),
(601, 'cash_book', 'Cash Book'),
(602, 'bank_book', 'Bank Book'),
(603, 'c_o_a', 'Chart of Account'),
(604, 'journal_voucher', 'Journal Voucher'),
(605, 'contra_voucher', 'Contra Voucher'),
(606, 'voucher_approval', 'Vouchar Approval'),
(607, 'supplier_payment', 'Supplier Payment'),
(608, 'customer_receive', 'Customer Receive'),
(609, 'gl_head', 'General Head'),
(610, 'account_code', 'Account Head'),
(611, 'opening_balance', 'Opening Balance'),
(612, 'head_of_account', 'Head of Account'),
(613, 'inventory_ledger', 'Inventory Ledger'),
(614, 'newpassword', 'New Password'),
(615, 'password_recovery', 'Password Recovery'),
(616, 'forgot_password', 'Forgot Password ??'),
(617, 'send', 'Send'),
(618, 'due_report', 'Due Report'),
(619, 'due_amount', 'Due Amount'),
(620, 'download_sample_file', 'Download Sample File'),
(621, 'customer_csv_upload', 'Customer Csv Upload'),
(622, 'csv_supplier', 'Csv Upload Supplier'),
(623, 'csv_upload_supplier', 'Csv Upload Supplier'),
(624, 'previous', 'Previous'),
(625, 'net_total', 'Net Total'),
(626, 'currency_list', 'Currency List'),
(627, 'currency_name', 'Currency Name'),
(628, 'currency_icon', 'Currency Symbol'),
(629, 'add_currency', 'Add Currency'),
(630, 'role_permission', 'Role Permission'),
(631, 'role_list', 'Role List'),
(632, 'user_assign_role', 'User Assign Role'),
(633, 'permission', 'Permission'),
(634, 'add_role', 'Add Role'),
(635, 'add_module', 'Add Module'),
(636, 'module_name', 'Module Name'),
(637, 'office_loan', 'Office Loan'),
(638, 'add_menu', 'Add Menu'),
(639, 'menu_name', 'Menu Name'),
(640, 'sl_no', 'Sl No'),
(641, 'create', 'Create'),
(642, 'read', 'Read'),
(643, 'role_name', 'Role Name'),
(644, 'qty', 'Quantity'),
(645, 'max_rate', 'Max Rate'),
(646, 'min_rate', 'Min Rate'),
(647, 'avg_rate', 'Average Rate'),
(648, 'role_permission_added_successfully', 'Role Permission Successfully Added'),
(649, 'update_successfully', 'Successfully Updated'),
(650, 'role_permission_updated_successfully', 'Role Permission Successfully Updated '),
(651, 'shipping_cost', 'Shipping Cost'),
(652, 'in_word', 'In Word '),
(653, 'shipping_cost_report', 'Shipping Cost Report'),
(654, 'cash_book_report', 'Cash Book Report'),
(655, 'inventory_ledger_report', 'Inventory Ledger Report'),
(656, 'trial_balance_with_opening_as_on', 'Trial Balance With Opening As On'),
(657, 'type', 'Type'),
(658, 'taka_only', 'Taka Only'),
(659, 'item_description', 'Desc'),
(660, 'sold_by', 'Sold By'),
(661, 'user_wise_sales_report', 'User Wise Sales Report'),
(662, 'user_name', 'User Name'),
(663, 'total_sold', 'Total Sold'),
(664, 'user_wise_sale_report', 'User Wise Sales Report'),
(665, 'barcode_or_qrcode', 'Barcode/QR-code'),
(666, 'category_csv_upload', 'Category Csv  Upload'),
(667, 'unit_csv_upload', 'Unit Csv Upload'),
(668, 'invoice_return_list', 'Sales Return list'),
(669, 'invoice_return', 'Sales Return'),
(670, 'tax_report', 'Tax Report'),
(671, 'select_tax', 'Select Tax'),
(672, 'hrm', 'HRM'),
(673, 'employee', 'Employee'),
(674, 'add_employee', 'Add Employee'),
(675, 'manage_employee', 'Manage Employee'),
(676, 'attendance', 'Attendance'),
(677, 'add_attendance', 'Attendance'),
(678, 'manage_attendance', 'Manage Attendance'),
(679, 'payroll', 'Payroll'),
(680, 'add_payroll', 'Payroll'),
(681, 'manage_payroll', 'Manage Payroll'),
(682, 'employee_type', ''),
(683, 'employee_designation', 'Employee Designation'),
(684, 'designation', 'Designation'),
(685, 'add_designation', 'Add Designation'),
(686, 'manage_designation', 'Manage Designation'),
(687, 'designation_update_form', 'Designation Update form'),
(688, 'picture', 'Picture'),
(689, 'country', 'Country'),
(690, 'blood_group', 'Blood Group'),
(691, 'address_line_1', 'Address Line 1'),
(692, 'address_line_2', 'Address Line 2'),
(693, 'zip', 'Zip code'),
(694, 'city', 'City'),
(695, 'hour_rate_or_salary', 'Houre Rate/Salary'),
(696, 'rate_type', 'Rate Type'),
(697, 'hourly', 'Hourly'),
(698, 'salary', 'Salary'),
(699, 'employee_update', 'Employee Update'),
(700, 'checkin', 'Check In'),
(701, 'employee_name', 'Employee Name'),
(702, 'checkout', 'Check Out'),
(703, 'confirm_clock', 'Confirm Clock'),
(704, 'stay', 'Stay Time'),
(705, 'sign_in', 'Sign In'),
(706, 'check_in', 'Check In'),
(707, 'single_checkin', 'Single Check In'),
(708, 'bulk_checkin', 'Bulk Check In'),
(709, 'successfully_checkout', 'Successfully Checkout'),
(710, 'attendance_report', 'Attendance Report'),
(711, 'datewise_report', 'Date Wise Report'),
(712, 'employee_wise_report', 'Employee Wise Report'),
(713, 'date_in_time_report', 'Date In Time Report'),
(714, 'request', 'Request'),
(715, 'sign_out', 'Sign Out'),
(716, 'work_hour', 'Work Hours'),
(717, 's_time', 'Start Time'),
(718, 'e_time', 'In Time'),
(719, 'salary_benefits_type', 'Benefits Type'),
(720, 'salary_benefits', 'Salary Benefits'),
(721, 'beneficial_list', 'Benefit List'),
(722, 'add_beneficial', 'Add Benefits'),
(723, 'add_benefits', 'Add Benefits'),
(724, 'benefits_list', 'Benefit List'),
(725, 'benefit_type', 'Benefit Type'),
(726, 'benefits', 'Benefit'),
(727, 'manage_benefits', 'Manage Benefits'),
(728, 'deduct', 'Deduct'),
(729, 'add', 'Add'),
(730, 'add_salary_setup', 'Add Salary Setup'),
(731, 'manage_salary_setup', 'Manage Salary Setup'),
(732, 'basic', 'Basic'),
(733, 'salary_type', 'Salary Type'),
(734, 'addition', 'Addition'),
(735, 'gross_salary', 'Gross Salary'),
(736, 'set', 'Set'),
(737, 'salary_generate', 'Salary Generate'),
(738, 'manage_salary_generate', 'Manage Salary Generate'),
(739, 'sal_name', 'Salary Name'),
(740, 'gdate', 'Generated Date'),
(741, 'generate_by', 'Generated By'),
(742, 'the_salary_of', 'The Salary of '),
(743, 'already_generated', ' Already Generated'),
(744, 'salary_month', 'Salary Month'),
(745, 'successfully_generated', 'Successfully Generated'),
(746, 'salary_payment', 'Salary Payment'),
(747, 'employee_salary_payment', 'Employee Salary Payment'),
(748, 'total_salary', 'Total Salary'),
(749, 'total_working_minutes', 'Total Working Hours'),
(750, 'working_period', 'Working Period'),
(751, 'paid_by', 'Paid By'),
(752, 'pay_now', 'Pay Now '),
(753, 'confirm', 'Confirm'),
(754, 'successfully_paid', 'Successfully Paid'),
(755, 'add_incometax', 'Add Income Tax'),
(756, 'setup_tax', 'Setup Tax'),
(757, 'start_amount', 'Start Amount'),
(758, 'end_amount', 'End Amount'),
(759, 'tax_rate', 'Tax Rate'),
(760, 'setup', 'Setup'),
(761, 'manage_income_tax', 'Manage Income Tax'),
(762, 'income_tax_updateform', 'Income tax Update form'),
(763, 'positional_information', 'Positional Information'),
(764, 'personal_information', 'Personal Information'),
(765, 'timezone', 'Time Zone'),
(766, 'sms', 'SMS'),
(767, 'sms_configure', 'SMS Configure'),
(768, 'url', 'URL'),
(769, 'sender_id', 'Sender ID'),
(770, 'api_key', 'Api Key'),
(771, 'gui_pos', 'GUI POS'),
(772, 'manage_service', 'Manage Service'),
(773, 'service', 'Service'),
(774, 'add_service', 'Add Service'),
(775, 'service_edit', 'Service Edit'),
(776, 'service_csv_upload', 'Service CSV Upload'),
(777, 'service_name', 'Service Name'),
(778, 'charge', 'Charge'),
(779, 'service_invoice', 'Service Invoice'),
(780, 'service_discount', 'Service Discount'),
(781, 'hanging_over', 'ETD'),
(782, 'service_details', 'Service Details'),
(783, 'tax_settings', 'Tax Settings'),
(784, 'default_value', 'Default Value'),
(785, 'number_of_tax', 'Number of Tax'),
(786, 'please_select_employee', 'Please Select Employee'),
(787, 'manage_service_invoice', 'Manage Service Invoice'),
(788, 'update_service_invoice', 'Update Service Invoice'),
(789, 'customer_wise_tax_report', 'Customer Wise  Tax Report'),
(790, 'service_id', 'Service Id'),
(791, 'invoice_wise_tax_report', 'Invoice Wise Tax Report'),
(792, 'reg_no', 'Reg No'),
(793, 'update_now', 'Update Now'),
(794, 'import', 'Import'),
(795, 'add_expense_item', 'Add Expense Item'),
(796, 'manage_expense_item', 'Manage Expense Item'),
(797, 'add_expense', 'Add Expense'),
(798, 'manage_expense', 'Manage Expense'),
(799, 'expense_statement', 'Expense Statement'),
(800, 'expense_type', 'Expense Type'),
(801, 'expense_item_name', 'Expense Item Name'),
(802, 'stock_purchase_price', 'Stock Purchase Price'),
(803, 'purchase_price', 'Purchase Price'),
(804, 'customer_advance', 'Customer Advance'),
(805, 'advance_type', 'Advance Type'),
(806, 'restore', 'Restore'),
(807, 'supplier_advance', 'Supplier Advance'),
(808, 'please_input_correct_invoice_no', 'Please Input Correct Invoice NO');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `directory` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `name`, `description`, `image`, `directory`, `status`) VALUES
(1, 'invoice', NULL, NULL, NULL, 1),
(2, 'accounts', NULL, NULL, NULL, 1),
(3, 'category', NULL, NULL, NULL, 1),
(4, 'product', NULL, NULL, NULL, 1),
(5, 'customer', NULL, NULL, NULL, 1),
(6, 'unit', NULL, NULL, NULL, 1),
(7, 'supplier', NULL, NULL, NULL, 1),
(8, 'purchase', NULL, NULL, NULL, 1),
(9, 'return', NULL, NULL, NULL, 1),
(10, 'tax', NULL, NULL, NULL, 1),
(11, 'stock', NULL, NULL, NULL, 1),
(12, 'report', NULL, NULL, NULL, 1),
(13, 'bank', NULL, NULL, NULL, 1),
(14, 'commission', NULL, NULL, NULL, 1),
(15, 'office_loan', NULL, NULL, NULL, 1),
(16, 'personal_loan', NULL, NULL, NULL, 1),
(18, 'data_synchronizer', NULL, NULL, NULL, 1),
(19, 'web_settings', NULL, NULL, NULL, 1),
(20, 'role_permission', NULL, NULL, NULL, 1),
(21, 'hrm', NULL, NULL, NULL, 1),
(22, 'attendance', NULL, NULL, NULL, 1),
(23, 'payroll', NULL, NULL, NULL, 1),
(24, 'service', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `note_id` int(11) NOT NULL,
  `cash_date` varchar(20) NOT NULL,
  `1000n` varchar(11) NOT NULL,
  `500n` varchar(11) NOT NULL,
  `100n` varchar(11) NOT NULL,
  `50n` varchar(11) NOT NULL,
  `20n` varchar(11) NOT NULL,
  `10n` varchar(11) NOT NULL,
  `5n` varchar(11) NOT NULL,
  `2n` varchar(11) NOT NULL,
  `1n` varchar(30) NOT NULL,
  `grandtotal` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`note_id`, `cash_date`, `1000n`, `500n`, `100n`, `50n`, `20n`, `10n`, `5n`, `2n`, `1n`, `grandtotal`) VALUES
(1, '2019-08-18', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_trans`
--

CREATE TABLE `payment_trans` (
  `transection_id` varchar(200) NOT NULL,
  `tracing_id` varchar(200) NOT NULL,
  `payment_type` varchar(10) NOT NULL,
  `date` varchar(50) DEFAULT NULL,
  `amount` float NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_tax_setup`
--

CREATE TABLE `payroll_tax_setup` (
  `tax_setup_id` int(11) UNSIGNED NOT NULL,
  `start_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `end_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `rate` decimal(12,2) NOT NULL DEFAULT '0.00',
  `status` varchar(30) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `personal_loan`
--

CREATE TABLE `personal_loan` (
  `per_loan_id` int(11) NOT NULL,
  `transaction_id` varchar(30) NOT NULL,
  `person_id` varchar(30) NOT NULL,
  `debit` decimal(12,2) DEFAULT '0.00',
  `credit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `date` varchar(30) NOT NULL,
  `details` varchar(100) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=no paid,2=paid'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person_information`
--

CREATE TABLE `person_information` (
  `person_id` varchar(50) NOT NULL,
  `person_name` varchar(50) NOT NULL,
  `person_phone` varchar(50) NOT NULL,
  `person_address` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person_information`
--

INSERT INTO `person_information` (`person_id`, `person_name`, `person_phone`, `person_address`, `status`) VALUES
('JMGCK5MVCN', 'dhaka electronics', '0464644', '', 1),
('VJY67W3YTE', 'tesco ', '01', '', 1),
('KDN4CJGVRK', 'abul hardware', '014454', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `person_ledger`
--

CREATE TABLE `person_ledger` (
  `transaction_id` varchar(50) NOT NULL,
  `person_id` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL,
  `debit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `credit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `details` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=no paid,2=paid',
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person_ledger`
--

INSERT INTO `person_ledger` (`transaction_id`, `person_id`, `date`, `debit`, `credit`, `details`, `status`, `id`) VALUES
('KT84M3I2O7', 'JMGCK5MVCN', '2019-06-27', '150.00', '0.00', 'rijgiwjfrw', 1, 0),
('F6E1J7D1DX', 'VJY67W3YTE', '2019-06-27', '500.00', '0.00', 'aDSADADF', 1, 0),
('XJ4B8N4TWO', 'KDN4CJGVRK', '2019-06-27', '650.00', '0.00', 'fdfg', 1, 0),
('VS6VVPYSTP', 'JMGCK5MVCN', '2019-06-28', '555.00', '0.00', 'Thanks ', 1, 0),
('QV9JT8IFJE', 'VJY67W3YTE', '2019-06-28', '0.00', '333.00', 'good to see u ', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pesonal_loan_information`
--

CREATE TABLE `pesonal_loan_information` (
  `id` int(11) NOT NULL,
  `person_id` varchar(50) NOT NULL,
  `person_name` varchar(50) NOT NULL,
  `person_phone` varchar(30) NOT NULL,
  `person_address` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `category_id` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`category_id`, `category_name`, `status`) VALUES
('KxSmHwjojMR79nJ', 'food', 1),
('dSvieq1d2aFVO5W', 'goodsd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_information`
--

CREATE TABLE `product_information` (
  `id` int(11) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `category_id` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `tax` float DEFAULT NULL COMMENT 'Tax in %',
  `serial_no` varchar(200) DEFAULT NULL,
  `product_model` varchar(100) NOT NULL,
  `product_details` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `tax0` text,
  `tax1` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_information`
--

INSERT INTO `product_information` (`id`, `product_id`, `category_id`, `product_name`, `price`, `unit`, `tax`, `serial_no`, `product_model`, `product_details`, `image`, `status`, `tax0`, `tax1`) VALUES
(1, '42156427', 'KxSmHwjojMR79nJ', 'Test Product', 100, 'demos', 0, 'sfs,dfsd', 'Test Model', 'sdfsadf', 'http://localhost/saleserp_sas_v-2/my-assets/image/product.png', 1, '0.01', '0.02'),
(2, '33845288', '', 'Second Product', 120, 'demos', 0, 'asdfsd,sdfasdf,454', 'sdfsa', 'sdafsdf', 'http://localhost/saleserp_sas_v-2/my-assets/image/product.png', 1, '0.01', '0.02');

-- --------------------------------------------------------

--
-- Table structure for table `product_price_history`
--

CREATE TABLE `product_price_history` (
  `product_pr_his_id` int(11) NOT NULL,
  `product_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `supplier_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `supplier_price` double NOT NULL,
  `date_of_price_buy` varchar(30) CHARACTER SET latin1 NOT NULL,
  `affect_time_pc` varchar(30) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_purchase`
--

CREATE TABLE `product_purchase` (
  `id` int(11) NOT NULL,
  `purchase_id` varchar(100) NOT NULL,
  `chalan_no` varchar(100) NOT NULL,
  `supplier_id` varchar(100) NOT NULL,
  `grand_total_amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `total_discount` decimal(10,2) DEFAULT '0.00',
  `purchase_date` varchar(50) NOT NULL,
  `purchase_details` text NOT NULL,
  `status` int(2) NOT NULL,
  `bank_id` varchar(30) DEFAULT NULL,
  `payment_type` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_purchase`
--

INSERT INTO `product_purchase` (`id`, `purchase_id`, `chalan_no`, `supplier_id`, `grand_total_amount`, `total_discount`, `purchase_date`, `purchase_details`, `status`, `bank_id`, `payment_type`) VALUES
(21, '20190904103141', '54', '6688VNWJ24FA5LK7RSBQ', '472.00', NULL, '2019-09-04', 'From app', 1, NULL, 1),
(2, '20190903054128', '1', '6688VNWJ24FA5LK7RSBQ', '68.00', NULL, '2019-09-03', 'sdfsdf', 1, '', 1),
(3, '20190903054203', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', '0.00', '2019-09-03', 'sdfsdf', 1, NULL, 1),
(4, '20190903054240', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', '0.00', '2019-09-03', 'sdfsdf', 1, NULL, 1),
(5, '20190903054749', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', '0.00', '2019-09-03', 'sdfsdf', 1, NULL, 1),
(6, '20190903055022', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', '0.00', '2019-09-03', 'sdfsdf', 1, NULL, 1),
(7, '20190903055114', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', NULL, '2019-09-03', 'sdfsdf', 1, NULL, 1),
(8, '20190903055139', '1', '6688VNWJ24FA5LK7RSBQ', '20.00', NULL, '2019-09-03', 'sdfsdf', 1, NULL, 1),
(22, '20190904103318', '56', '6688VNWJ24FA5LK7RSBQ', '60.00', NULL, '2019-09-04', 'From app', 1, NULL, 1),
(17, '20190903094416', '1', '6688VNWJ24FA5LK7RSBQ', '27303.00', NULL, '2019-09-03', 'From app', 1, NULL, 1),
(18, '20190904070737', '1', '6688VNWJ24FA5LK7RSBQ', '30.00', NULL, '2019-09-04', 'From app', 1, NULL, 1),
(19, '20190904071000', '1', '6688VNWJ24FA5LK7RSBQ', '254.00', NULL, '2019-09-04', 'From app', 1, NULL, 1),
(20, '20190904094916', '345', '6688VNWJ24FA5LK7RSBQ', '60.00', NULL, '2019-09-04', 'From app', 1, NULL, 1),
(15, '20190903061451', '1', '6688VNWJ24FA5LK7RSBQ', '960.00', NULL, '2019-09-03', 'From app', 1, NULL, 1),
(25, '20190904112702', '56', '6688VNWJ24FA5LK7RSBQ', '1089.00', NULL, '2019-09-05', 'From app', 1, NULL, 1),
(28, '20190924143851', '1', 'XUQB7JFFJSFXKGOZ9VD3', '4500.00', NULL, '2019-09-24', 'dfsdf', 1, '', 1),
(29, '20190925103121', '1', 'XUQB7JFFJSFXKGOZ9VD3', '3300.00', NULL, '2019-09-25', 'fdgsdf', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_purchase_details`
--

CREATE TABLE `product_purchase_details` (
  `id` int(11) NOT NULL,
  `purchase_detail_id` varchar(100) NOT NULL,
  `purchase_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `quantity` decimal(10,2) NOT NULL DEFAULT '0.00',
  `rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` float DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_purchase_details`
--

INSERT INTO `product_purchase_details` (`id`, `purchase_detail_id`, `purchase_id`, `product_id`, `quantity`, `rate`, `total_amount`, `discount`, `status`) VALUES
(1, 'HwV9Lka9y0Q7EMX', '20190904071000', '7898940742035', '46.00', '5.00', '230.00', 0, 1),
(2, 'v6RoAsfxokjSNuY', '20190903054128', '38296235', '4.00', '5.00', '20.00', NULL, 0),
(3, 'CHquHtcMuCOCrOi', '20190903054203', '38296235', '4.00', '5.00', '20.00', 0, 1),
(4, 'yHXsMrhGt7ZJ3HO', '20190903054240', '38296235', '4.00', '5.00', '20.00', 0, 1),
(5, '5OtNQiBYfaebR8', '20190903054128', '7898940742035', '12.00', '4.00', '48.00', NULL, 0),
(6, 'wDcH2YQaw8p8jq5', '20190903054749', '38296235', '4.00', '5.00', '20.00', 0, 1),
(7, 'kJ27v3EU3qcaHus', '20190903055022', '38296235', '4.00', '5.00', '20.00', 0, 1),
(8, 'FirxTBPuH1Y2EL1', '20190903055114', '38296235', '4.00', '5.00', '20.00', 0, 1),
(9, 'mLWS3BmJqML2iDr', '20190903055139', '38296235', '4.00', '5.00', '20.00', 0, 1),
(10, 'G8AFobHGR5UWw8m', '20190904094916', '38296235', '12.00', '5.00', '60.00', 0, 1),
(11, 'Nl5yLHBNi94w68v', '20190904071000', '38296235', '3.00', '4.00', '12.00', 0, 1),
(12, 'kDmvqKLQjVjAQXz', '20190903094416', '38296235', '23.00', '24.00', '552.00', 0, 1),
(13, 'K62s3i2CpTsVNbT', '20190903094416', '7898940742035', '123.00', '213.00', '26199.00', 0, 1),
(14, '1g6TnSGhXbYWfbr', '20190904070737', '38296235', '5.00', '6.00', '30.00', 0, 1),
(15, 'USKT2mZi3N8L9km', '20190903061451', '7898940742035', '120.00', '8.00', '960.00', 0, 1),
(16, 'fodJqtjieRIu6dd', '20190904103141', '7898940742035', '46.00', '5.00', '230.00', 0, 1),
(17, 'pI9aUsHj123UkNI', '20190904103141', '38296235', '3.00', '4.00', '12.00', 0, 1),
(18, 'jpCCru2WcxMbvP', '20190904103318', '7898940742035', '5.00', '4.00', '20.00', 0, 1),
(19, '1jVMcML2BGRKHDV', '20190904103318', '34113672', '5.00', '4.00', '20.00', 0, 1),
(20, 'rMMDIWv35ViCTvr', '20190904112702', '38296235', '33.00', '33.00', '1089.00', 0, 1),
(21, 'HcSerknWOJ6dG6u', '20190924143851', '42156427', '10.00', '90.00', '4500.00', NULL, 1),
(22, 'eUY7wbvQLChrWms', '20190925103121', '33845288', '30.00', '110.00', '3300.00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_return`
--

CREATE TABLE `product_return` (
  `return_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `product_id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `invoice_id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `purchase_id` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `date_purchase` varchar(20) CHARACTER SET latin1 NOT NULL,
  `date_return` varchar(30) CHARACTER SET latin1 NOT NULL,
  `byy_qty` float NOT NULL,
  `ret_qty` float NOT NULL,
  `customer_id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `supplier_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `product_rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `deduction` float NOT NULL,
  `total_deduct` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_ret_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `net_total_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reason` text CHARACTER SET latin1 NOT NULL,
  `usablity` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_return`
--

INSERT INTO `product_return` (`return_id`, `product_id`, `invoice_id`, `purchase_id`, `date_purchase`, `date_return`, `byy_qty`, `ret_qty`, `customer_id`, `supplier_id`, `product_rate`, `deduction`, `total_deduct`, `total_tax`, `total_ret_amount`, `net_total_amount`, `reason`, `usablity`) VALUES
('956269284242668', '19276692', '', '20190731114746', '2019-07-31', '2019-08-01', 50, 1, '', 'WPXXU7UC1Z4ID1HUVU53', '40000.00', 0, '0.00', '0.00', '40000.00', '40500.00', 'sdfasdf', 2),
('465999969336718', '34113672', '', '20190731114746', '2019-07-31', '2019-08-01', 50, 2, '', 'WPXXU7UC1Z4ID1HUVU53', '250.00', 0, '0.00', '0.00', '500.00', '40500.00', 'sdfasdf', 2),
('925134226593673', '38296235', '', '20190731114458', '2019-07-31', '2019-08-01', 20, 10, '', 'KRQGUNN4ZMS3WT8I2RK9', '30000.00', 0, '0.00', '0.00', '300000.00', '300000.00', '', 2),
('863338834262391', '38296235', '5147385423', NULL, '2019-09-19', '2019-09-19', 5, 2, 'BIXQAYJ3XDFBYZN', '', '35000.00', 0, '0.00', '0.00', '70000.00', '70000.00', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_service`
--

CREATE TABLE `product_service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `charge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tax0` text,
  `tax1` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL,
  `fk_module_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `create` tinyint(1) DEFAULT NULL,
  `read` tinyint(1) DEFAULT NULL,
  `update` tinyint(1) DEFAULT NULL,
  `delete` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `fk_module_id`, `role_id`, `create`, `read`, `update`, `delete`) VALUES
(79, 1, 1, 1, 1, 1, 1),
(80, 2, 1, 1, 1, 1, 1),
(81, 3, 1, 0, 0, 0, 0),
(82, 4, 1, 0, 0, 0, 0),
(83, 5, 1, 0, 0, 0, 0),
(84, 6, 1, 0, 0, 0, 0),
(85, 7, 1, 0, 0, 0, 0),
(86, 8, 1, 0, 0, 0, 0),
(87, 9, 1, 0, 0, 0, 0),
(88, 10, 1, 0, 0, 0, 0),
(89, 11, 1, 0, 0, 0, 0),
(90, 12, 1, 0, 0, 0, 0),
(91, 13, 1, 0, 0, 0, 0),
(92, 14, 1, 0, 0, 0, 0),
(93, 15, 1, 0, 0, 0, 0),
(94, 16, 1, 0, 0, 0, 0),
(95, 17, 1, 0, 0, 0, 0),
(96, 18, 1, 0, 0, 0, 0),
(97, 19, 1, 0, 0, 0, 0),
(98, 20, 1, 0, 0, 0, 0),
(99, 21, 1, 0, 0, 0, 0),
(100, 22, 1, 0, 0, 0, 0),
(101, 23, 1, 0, 0, 0, 0),
(102, 24, 1, 0, 0, 0, 0),
(103, 25, 1, 0, 0, 0, 0),
(104, 26, 1, 0, 0, 0, 0),
(105, 27, 1, 0, 0, 0, 0),
(106, 28, 1, 0, 0, 0, 0),
(107, 29, 1, 0, 0, 0, 0),
(108, 30, 1, 0, 0, 0, 0),
(109, 31, 1, 0, 0, 0, 0),
(110, 32, 1, 0, 0, 0, 0),
(111, 33, 1, 0, 0, 0, 0),
(112, 34, 1, 0, 0, 0, 0),
(113, 35, 1, 0, 0, 0, 0),
(114, 36, 1, 0, 0, 0, 0),
(115, 37, 1, 0, 0, 0, 0),
(116, 38, 1, 0, 0, 0, 0),
(117, 39, 1, 0, 0, 0, 0),
(118, 40, 1, 0, 0, 0, 0),
(119, 41, 1, 0, 0, 0, 0),
(120, 42, 1, 0, 0, 0, 0),
(121, 43, 1, 0, 0, 0, 0),
(122, 44, 1, 0, 0, 0, 0),
(123, 45, 1, 0, 0, 0, 0),
(124, 46, 1, 0, 0, 0, 0),
(125, 47, 1, 0, 0, 0, 0),
(126, 48, 1, 0, 0, 0, 0),
(127, 49, 1, 0, 0, 0, 0),
(128, 50, 1, 0, 0, 0, 0),
(129, 51, 1, 0, 0, 0, 0),
(130, 52, 1, 0, 0, 0, 0),
(131, 53, 1, 0, 0, 0, 0),
(132, 54, 1, 0, 0, 0, 0),
(133, 55, 1, 0, 0, 0, 0),
(134, 56, 1, 0, 0, 0, 0),
(135, 57, 1, 0, 0, 0, 0),
(136, 58, 1, 0, 0, 0, 0),
(137, 59, 1, 0, 0, 0, 0),
(138, 60, 1, 0, 0, 0, 0),
(139, 61, 1, 0, 0, 0, 0),
(140, 62, 1, 0, 0, 0, 0),
(141, 63, 1, 0, 0, 0, 0),
(142, 64, 1, 0, 0, 0, 0),
(143, 65, 1, 0, 0, 0, 0),
(144, 66, 1, 0, 0, 0, 0),
(145, 67, 1, 0, 0, 0, 0),
(146, 68, 1, 0, 0, 0, 0),
(147, 69, 1, 0, 0, 0, 0),
(148, 70, 1, 0, 0, 0, 0),
(149, 71, 1, 0, 0, 0, 0),
(150, 72, 1, 0, 0, 0, 0),
(151, 73, 1, 0, 0, 0, 0),
(152, 74, 1, 0, 0, 0, 0),
(153, 75, 1, 0, 0, 0, 0),
(154, 76, 1, 0, 0, 0, 0),
(155, 77, 1, 0, 0, 0, 0),
(156, 78, 1, 0, 0, 0, 0),
(1405, 1, 2, 1, 0, 0, 0),
(1406, 2, 2, 0, 1, 1, 0),
(1407, 3, 2, 1, 0, 0, 0),
(1408, 4, 2, 1, 1, 1, 0),
(1409, 5, 2, 0, 0, 0, 0),
(1410, 6, 2, 0, 0, 0, 0),
(1411, 7, 2, 0, 0, 0, 0),
(1412, 8, 2, 0, 0, 0, 0),
(1413, 9, 2, 0, 0, 0, 0),
(1414, 10, 2, 0, 0, 0, 0),
(1415, 11, 2, 0, 0, 0, 0),
(1416, 12, 2, 0, 0, 0, 0),
(1417, 13, 2, 0, 0, 0, 0),
(1418, 14, 2, 0, 0, 0, 0),
(1419, 15, 2, 0, 0, 0, 0),
(1420, 16, 2, 0, 0, 0, 0),
(1421, 17, 2, 0, 0, 0, 0),
(1422, 18, 2, 0, 0, 0, 0),
(1423, 19, 2, 0, 0, 0, 0),
(1424, 20, 2, 1, 1, 1, 1),
(1425, 21, 2, 0, 0, 0, 0),
(1426, 22, 2, 1, 0, 0, 0),
(1427, 23, 2, 0, 0, 0, 0),
(1428, 24, 2, 0, 0, 0, 0),
(1429, 25, 2, 0, 0, 0, 0),
(1430, 26, 2, 1, 1, 1, 1),
(1431, 27, 2, 0, 1, 0, 0),
(1432, 28, 2, 0, 1, 0, 0),
(1433, 109, 2, 0, 0, 0, 0),
(1434, 29, 2, 0, 0, 0, 0),
(1435, 30, 2, 0, 0, 0, 0),
(1436, 31, 2, 0, 0, 0, 0),
(1437, 32, 2, 0, 0, 0, 0),
(1438, 33, 2, 0, 0, 0, 0),
(1439, 34, 2, 0, 0, 0, 0),
(1440, 110, 2, 0, 0, 0, 0),
(1441, 35, 2, 0, 0, 0, 0),
(1442, 36, 2, 0, 0, 0, 0),
(1443, 37, 2, 0, 0, 0, 0),
(1444, 38, 2, 0, 0, 0, 0),
(1445, 39, 2, 0, 0, 0, 0),
(1446, 40, 2, 0, 0, 0, 0),
(1447, 41, 2, 0, 0, 0, 0),
(1448, 42, 2, 0, 0, 0, 0),
(1449, 85, 2, 0, 0, 0, 0),
(1450, 86, 2, 0, 0, 0, 0),
(1451, 105, 2, 0, 0, 0, 0),
(1452, 106, 2, 0, 0, 0, 0),
(1453, 107, 2, 0, 0, 0, 0),
(1454, 43, 2, 0, 0, 0, 0),
(1455, 44, 2, 0, 0, 0, 0),
(1456, 45, 2, 0, 0, 0, 0),
(1457, 46, 2, 0, 0, 0, 0),
(1458, 47, 2, 0, 0, 0, 0),
(1459, 48, 2, 0, 0, 0, 0),
(1460, 49, 2, 0, 0, 0, 0),
(1461, 50, 2, 0, 0, 0, 0),
(1462, 51, 2, 0, 0, 0, 0),
(1463, 52, 2, 0, 0, 0, 0),
(1464, 53, 2, 0, 0, 0, 0),
(1465, 54, 2, 0, 0, 0, 0),
(1466, 55, 2, 0, 0, 0, 0),
(1467, 79, 2, 0, 0, 0, 0),
(1468, 80, 2, 0, 0, 0, 0),
(1469, 81, 2, 0, 0, 0, 0),
(1470, 82, 2, 0, 0, 0, 0),
(1471, 83, 2, 0, 0, 0, 0),
(1472, 84, 2, 0, 0, 0, 0),
(1473, 56, 2, 0, 0, 0, 0),
(1474, 57, 2, 0, 0, 0, 0),
(1475, 58, 2, 0, 0, 0, 0),
(1476, 59, 2, 0, 0, 0, 0),
(1477, 60, 2, 0, 0, 0, 0),
(1478, 61, 2, 0, 0, 0, 0),
(1479, 62, 2, 0, 0, 0, 0),
(1480, 63, 2, 0, 0, 0, 0),
(1481, 64, 2, 0, 0, 0, 0),
(1482, 65, 2, 0, 0, 0, 0),
(1483, 66, 2, 0, 0, 0, 0),
(1484, 67, 2, 0, 0, 0, 0),
(1485, 68, 2, 0, 0, 0, 0),
(1486, 108, 2, 0, 0, 0, 0),
(1487, 69, 2, 0, 0, 0, 0),
(1488, 70, 2, 0, 0, 0, 0),
(1489, 71, 2, 0, 0, 0, 0),
(1490, 72, 2, 0, 0, 0, 0),
(1491, 73, 2, 0, 0, 0, 0),
(1492, 74, 2, 0, 0, 0, 0),
(1493, 75, 2, 0, 0, 0, 0),
(1494, 76, 2, 0, 0, 0, 0),
(1495, 77, 2, 0, 0, 0, 0),
(1496, 78, 2, 0, 0, 0, 0),
(1497, 97, 2, 0, 0, 0, 0),
(1498, 98, 2, 0, 0, 0, 0),
(1499, 99, 2, 0, 0, 0, 0),
(1500, 100, 2, 0, 0, 0, 0),
(1501, 94, 2, 0, 0, 0, 0),
(1502, 95, 2, 0, 0, 0, 0),
(1503, 96, 2, 0, 0, 0, 0),
(1504, 87, 2, 0, 0, 0, 0),
(1505, 88, 2, 0, 0, 0, 0),
(1506, 89, 2, 0, 0, 0, 0),
(1507, 90, 2, 0, 0, 0, 0),
(1508, 91, 2, 0, 0, 0, 0),
(1509, 92, 2, 0, 0, 0, 0),
(1510, 93, 2, 0, 0, 0, 0),
(1511, 101, 2, 0, 0, 0, 0),
(1512, 102, 2, 0, 0, 0, 0),
(1513, 103, 2, 0, 0, 0, 0),
(1514, 104, 2, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `salary_sheet_generate`
--

CREATE TABLE `salary_sheet_generate` (
  `ssg_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET latin1 NOT NULL,
  `gdate` varchar(30) DEFAULT NULL,
  `start_date` varchar(30) CHARACTER SET latin1 NOT NULL,
  `end_date` varchar(30) CHARACTER SET latin1 NOT NULL,
  `generate_by` varchar(30) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `salary_type`
--

CREATE TABLE `salary_type` (
  `salary_type_id` int(11) NOT NULL,
  `sal_name` varchar(100) NOT NULL,
  `salary_type` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sec_role`
--

CREATE TABLE `sec_role` (
  `id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sec_role`
--

INSERT INTO `sec_role` (`id`, `type`) VALUES
(1, 'Test Role'),
(2, 'Coder IT');

-- --------------------------------------------------------

--
-- Table structure for table `sec_userrole`
--

CREATE TABLE `sec_userrole` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roleid` int(11) NOT NULL,
  `createby` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `createdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sec_userrole`
--

INSERT INTO `sec_userrole` (`id`, `user_id`, `roleid`, `createby`, `createdate`) VALUES
(1, '3vRVO2L8tsO5PP2', 2, '1', '2019-02-23 06:14:22');

-- --------------------------------------------------------

--
-- Table structure for table `service_invoice`
--

CREATE TABLE `service_invoice` (
  `id` int(11) NOT NULL,
  `voucher_no` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `employee_id` varchar(50) NOT NULL,
  `customer_id` varchar(30) NOT NULL,
  `total_amount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `total_discount` decimal(20,2) NOT NULL DEFAULT '0.00',
  `invoice_discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total_tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `due_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `shipping_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `previous` decimal(10,2) NOT NULL DEFAULT '0.00',
  `details` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `service_invoice_details`
--

CREATE TABLE `service_invoice_details` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `service_inv_id` varchar(30) NOT NULL,
  `qty` decimal(10,2) NOT NULL DEFAULT '0.00',
  `charge` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sms_settings`
--

CREATE TABLE `sms_settings` (
  `id` int(11) NOT NULL,
  `url` varchar(100) NOT NULL,
  `sender_id` varchar(100) NOT NULL,
  `api_key` varchar(100) NOT NULL,
  `isinvoice` int(11) NOT NULL DEFAULT '0',
  `ispurchase` int(11) NOT NULL DEFAULT '0',
  `isservice` int(11) NOT NULL DEFAULT '0',
  `ispayment` int(11) NOT NULL DEFAULT '0',
  `isreceive` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_settings`
--

INSERT INTO `sms_settings` (`id`, `url`, `sender_id`, `api_key`, `isinvoice`, `ispurchase`, `isservice`, `ispayment`, `isreceive`) VALUES
(1, 'http://sms.bdtask.com/smsapi', '880184716988421', '58C20029865c42c504afc711.77492546', 1, 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sub_module`
--

CREATE TABLE `sub_module` (
  `id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `image` varchar(100) DEFAULT NULL,
  `directory` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_module`
--

INSERT INTO `sub_module` (`id`, `mid`, `name`, `description`, `image`, `directory`, `status`) VALUES
(1, 1, 'new_invoice', NULL, NULL, 'new_invoice', 1),
(2, 1, 'manage_invoice', NULL, NULL, 'manage_invoice', 1),
(3, 1, 'pos_invoice', NULL, NULL, 'pos_invoice', 1),
(4, 2, 'c_o_a', NULL, NULL, 'show_tree', 1),
(5, 2, 'supplier_payment', NULL, NULL, 'supplier_payment', 1),
(6, 2, 'customer_receive', NULL, NULL, 'customer_receive', 1),
(7, 2, 'debit_voucher', NULL, NULL, 'debit_voucher', 1),
(8, 2, 'credit_voucher', NULL, NULL, 'credit_voucher', 1),
(9, 2, 'voucher_approval', NULL, NULL, 'aprove_v', 1),
(10, 2, 'contra_voucher', NULL, NULL, 'contra_voucher', 1),
(11, 2, 'journal_voucher', NULL, NULL, 'journal_voucher', 1),
(12, 2, 'report', NULL, NULL, 'ac_report', 1),
(13, 2, 'cash_book', NULL, NULL, 'cash_book', 1),
(14, 2, 'Inventory_ledger', NULL, NULL, 'inventory_ledger', 1),
(15, 2, 'bank_book', NULL, NULL, 'bank_book', 1),
(16, 2, 'general_ledger', NULL, NULL, 'general_ledger', 1),
(17, 2, 'trial_balance', NULL, NULL, 'trial_balance', 1),
(18, 2, 'cash_flow', NULL, NULL, 'cash_flow_report', 1),
(19, 2, 'coa_print', NULL, NULL, 'coa_print', 1),
(20, 3, 'add_category', NULL, NULL, 'create_category', 1),
(21, 3, 'manage_category', NULL, NULL, 'manage_category', 1),
(22, 4, 'add_product', NULL, NULL, 'create_product', 1),
(23, 4, 'import_product_csv', NULL, NULL, 'add_product_csv', 1),
(24, 4, 'manage_product', NULL, NULL, 'manage_product', 1),
(25, 5, 'add_customer', NULL, NULL, 'add_customer', 1),
(26, 5, 'manage_customer', NULL, NULL, 'manage_customer', 1),
(27, 5, 'credit_customer', NULL, NULL, 'credit_customer', 1),
(28, 5, 'paid_customer', NULL, NULL, 'paid_customer', 1),
(29, 6, 'add_unit', NULL, NULL, 'add_unit', 1),
(30, 6, 'manage_unit', NULL, NULL, 'manage_unit', 1),
(31, 7, 'add_supplier', NULL, NULL, 'add_supplier', 1),
(32, 7, 'manage_supplier', NULL, NULL, 'manage_supplier', 1),
(33, 7, 'supplier_ledger', NULL, NULL, 'supplier_ledger_report', 1),
(35, 8, 'add_purchase', NULL, NULL, 'add_purchase', 1),
(36, 8, 'manage_purchase', NULL, NULL, 'manage_purchase', 1),
(37, 9, 'return', NULL, NULL, 'add_return', 1),
(38, 9, 'stock_return_list', NULL, NULL, 'return_list', 1),
(39, 9, 'supplier_return_list', NULL, NULL, 'supplier_return_list', 1),
(40, 9, 'wastage_return_list', NULL, NULL, 'wastage_return_list', 1),
(41, 10, 'add_tax', NULL, NULL, 'add_tax', 1),
(42, 10, 'manage_tax', NULL, NULL, 'manage_tax', 1),
(43, 11, 'stock_report', NULL, NULL, 'stock_report', 1),
(44, 11, 'stock_report_supplier_wise', NULL, NULL, 'stock_report_sp_wise', 1),
(45, 11, 'stock_report_product_wise', NULL, NULL, 'stock_report_pro_wise', 1),
(46, 12, 'closing', NULL, NULL, 'add_closing', 1),
(47, 12, 'closing_report', NULL, NULL, 'closing_report', 1),
(48, 12, 'todays_report', NULL, NULL, 'all_report', 1),
(49, 12, 'todays_customer_receipt', NULL, NULL, 'todays_customer_receipt', 1),
(50, 12, 'sales_report', NULL, NULL, 'todays_sales_report', 1),
(51, 12, 'due_report', NULL, NULL, 'retrieve_dateWise_DueReports', 1),
(52, 12, 'purchase_report', NULL, NULL, 'todays_purchase_report', 1),
(53, 12, 'purchase_report_category_wise', NULL, NULL, 'purchase_report_category_wise', 1),
(54, 12, 'sales_report_product_wise', NULL, NULL, 'product_sales_reports_date_wise', 1),
(55, 12, 'sales_report_category_wise', NULL, NULL, 'sales_report_category_wise', 1),
(56, 13, 'add_new_bank', NULL, NULL, 'add_bank', 1),
(57, 13, 'bank_transaction', NULL, NULL, 'bank_transaction', 1),
(58, 13, 'manage_bank', NULL, NULL, 'bank_list', 1),
(59, 14, 'generate_commission', NULL, NULL, 'commission', 1),
(60, 15, 'add_person', NULL, NULL, 'add1_person', 1),
(61, 15, 'add_loan', NULL, NULL, 'add_office_loan', 1),
(62, 15, 'add_payment', NULL, NULL, 'add_loan_payment', 1),
(63, 15, 'manage_loan', NULL, NULL, 'manage1_person', 1),
(64, 16, 'add_person', NULL, NULL, 'add_person', 1),
(65, 16, 'add_loan', NULL, NULL, 'add_loan', 1),
(66, 16, 'add_payment', NULL, NULL, 'add_payment', 1),
(67, 16, 'manage_loan', NULL, NULL, 'manage_person', 1),
(68, 18, 'backup_restore', NULL, NULL, 'back_up', 1),
(69, 19, 'manage_company', NULL, NULL, 'manage_company', 1),
(70, 19, 'add_user', NULL, NULL, 'add_user', 1),
(71, 19, 'manage_users', NULL, NULL, 'manage_user', 1),
(72, 19, 'language', NULL, NULL, 'add_language', 1),
(73, 19, 'currency', NULL, NULL, 'add_currency', 1),
(74, 19, 'setting', NULL, NULL, 'soft_setting', 1),
(75, 20, 'add_role', NULL, NULL, 'add_role', 1),
(76, 20, 'role_list', NULL, NULL, 'role_list', 1),
(77, 20, 'user_assign_role', NULL, NULL, 'user_assign', 1),
(78, 20, 'Permission', NULL, NULL, NULL, 1),
(79, 12, 'shipping_cost_report', NULL, NULL, 'shipping_cost_report', 1),
(80, 12, 'user_wise_sales_report', NULL, NULL, 'user_wise_sales_report', 1),
(81, 12, 'invoice_return', NULL, NULL, 'invoice_return', 1),
(82, 12, 'supplier_return', NULL, NULL, 'supplier_return', 1),
(83, 12, 'tax_report', NULL, NULL, 'tax_report', 1),
(84, 12, 'profit_report', NULL, NULL, 'profit_report', 1),
(85, 10, 'add_incometax', NULL, NULL, 'add_incometax', 1),
(86, 10, 'manage_income_tax', NULL, NULL, 'manage_income_tax', 1),
(87, 23, 'add_benefits', NULL, NULL, 'add_benefits', 1),
(88, 23, 'manage_benefits', NULL, NULL, 'manage_benefits', 1),
(89, 23, 'add_salary_setup', NULL, NULL, 'add_salary_setup', 1),
(90, 23, 'manage_salary_setup', NULL, NULL, 'manage_salary_setup', 1),
(91, 23, 'salary_generate', NULL, NULL, 'salary_generate', 1),
(92, 23, 'manage_salary_generate', NULL, NULL, 'manage_salary_generate', 1),
(93, 23, 'salary_payment', NULL, NULL, 'salary_payment', 1),
(94, 22, 'add_attendance', NULL, NULL, 'add_attendance', 1),
(95, 22, 'manage_attendance', NULL, NULL, 'manage_attendance', 1),
(96, 22, 'attendance_report', NULL, NULL, 'attendance_report', 1),
(97, 21, 'add_designation', NULL, NULL, 'add_designation', 1),
(98, 21, 'manage_designation', NULL, NULL, 'manage_designation', 1),
(99, 21, 'add_employee', NULL, NULL, 'add_employee', 1),
(100, 21, 'manage_employee', NULL, NULL, 'manage_employee', 1),
(101, 24, 'create_service', NULL, NULL, 'create_service', 1),
(102, 24, 'manage_service', NULL, NULL, 'manage_service', 1),
(103, 24, 'service_invoice', NULL, NULL, 'service_invoice', 1),
(104, 24, 'manage_service_invoice', NULL, NULL, 'manage_service_invoice', 1),
(105, 10, 'tax_report', NULL, NULL, 'tax_report', 1),
(106, 10, 'invoice_wise_tax_report', NULL, NULL, 'invoice_wise_tax_report', 1),
(107, 10, 'tax_settings', NULL, NULL, 'tax_settings', 1),
(108, 18, 'restore', NULL, NULL, 'restore', 1),
(109, 5, 'customer_advance', NULL, NULL, 'customer_advance', 1),
(110, 7, 'supplier_advance', NULL, NULL, 'supplier_advance', 1),
(111, 5, 'customer_ledger', NULL, NULL, 'customer_ledger', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_information`
--

CREATE TABLE `supplier_information` (
  `id` int(11) NOT NULL,
  `supplier_id` varchar(100) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `details` varchar(255) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_information`
--

INSERT INTO `supplier_information` (`id`, `supplier_id`, `supplier_name`, `address`, `mobile`, `details`, `status`) VALUES
(21, '2T6QHG45AP4M11FG6TPT', 'shahabuddin', '082545669', 'cfgg', '', 1),
(20, '6167UCENYO5JJITKRH21', 'hhhkkk', 'fsdf', '2343', '', 1),
(22, 'XUQB7JFFJSFXKGOZ9VD3', 'isahaq', '01852376598', 'khilkhet', '', 1),
(23, 'CMBQPCTZMZ3ALHEYT8BG', 'Ovi', '017744125', 'Dhaka', 'Android', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_ledger`
--

CREATE TABLE `supplier_ledger` (
  `id` int(20) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `supplier_id` varchar(100) NOT NULL,
  `chalan_no` varchar(100) DEFAULT NULL,
  `deposit_no` varchar(50) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `description` varchar(255) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `cheque_no` varchar(255) NOT NULL,
  `date` varchar(50) NOT NULL,
  `status` int(2) NOT NULL,
  `d_c` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_ledger`
--

INSERT INTO `supplier_ledger` (`id`, `transaction_id`, `supplier_id`, `chalan_no`, `deposit_no`, `amount`, `description`, `payment_type`, `cheque_no`, `date`, `status`, `d_c`) VALUES
(1, 'PCMBPBE6PY', 'WPXXU7UC1Z4ID1HUVU53', 'Adjustment ', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-25', 1, 'c'),
(2, '20190625090838', 'WPXXU7UC1Z4ID1HUVU53', '234234', NULL, '7500.00', 'sdfasd', '', '', '2019-06-25', 1, 'c'),
(3, '20190625090838', 'WPXXU7UC1Z4ID1HUVU53', '234234', NULL, '7500.00', 'Purchase From Supplier. sdfasd', '', '', '2019-06-25', 1, 'd'),
(4, '20190625132953', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '4000.00', '', '', '', '2019-06-25', 1, 'c'),
(5, '20190625132953', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '4000.00', 'Purchase From Supplier. ', '', '', '2019-06-25', 1, 'd'),
(6, 'PM-1', 'WPXXU7UC1Z4ID1HUVU53', NULL, 'PM-1', '250.00', 'Paid From Accounts', '1', '', '2019-06-25', 1, 'd'),
(7, '20190626100758', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '3500.00', '', '', '', '2019-06-26', 1, 'c'),
(8, '20190626100758', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '3500.00', 'Purchase From Supplier. ', '', '', '2019-06-26', 1, 'd'),
(9, '20190626101452', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '3900.00', '', '', '', '2019-06-26', 1, 'c'),
(10, '20190626101452', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '3900.00', 'Purchase From Supplier. ', '', '', '2019-06-26', 1, 'd'),
(11, '20190628054650', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '400000.00', '', '', '', '2019-06-28', 1, 'c'),
(12, 'V2WGALHBZ5', 'WPXXU7UC1Z4ID1HUVU53', NULL, NULL, '2500.00', 'Advance ', 'NA', 'NA', '2019-06-28', 1, 'd'),
(13, 'MPXB7A4FB2', 'KRQGUNN4ZMS3WT8I2RK9', 'Adjustment ', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-06-28', 1, 'c'),
(14, '20190628055844', 'KRQGUNN4ZMS3WT8I2RK9', '', NULL, '150000.00', '', '', '', '2019-06-28', 1, 'c'),
(15, '20190628055844', 'KRQGUNN4ZMS3WT8I2RK9', '', NULL, '150000.00', 'Purchase From Supplier. ', '', '', '2019-06-28', 1, 'd'),
(16, '20190628062109', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '1900.00', '', '', '', '2019-06-28', 1, 'c'),
(17, '20190628062109', 'WPXXU7UC1Z4ID1HUVU53', '', NULL, '1900.00', 'Purchase From Supplier. ', '', '', '2019-06-28', 1, 'd'),
(18, '20190629115435', 'WPXXU7UC1Z4ID1HUVU53', '16876936', NULL, '20000.00', '', '', '', '2019-06-29', 1, 'c'),
(19, '20190629115435', 'WPXXU7UC1Z4ID1HUVU53', '16876936', NULL, '20000.00', 'Purchase From Supplier. ', '', '', '2019-06-29', 1, 'd'),
(20, '20190731114458', 'KRQGUNN4ZMS3WT8I2RK9', 'sdfs', NULL, '0.00', '', '', '', '2019-07-31', 1, 'c'),
(21, '20190731114458', 'KRQGUNN4ZMS3WT8I2RK9', 'sdfs', NULL, '0.00', 'Purchase From Supplier. ', '', '', '2019-07-31', 1, 'd'),
(22, '20190731114746', 'WPXXU7UC1Z4ID1HUVU53', '234234', NULL, '2039100.00', '', '', '', '2019-07-31', 1, 'c'),
(23, '20190731114746', 'WPXXU7UC1Z4ID1HUVU53', '234234', NULL, '2039100.00', 'Purchase From Supplier. ', '', '', '2019-07-31', 1, 'd'),
(24, '20190731114746', 'WPXXU7UC1Z4ID1HUVU53', 'RRJBPNSH59', NULL, '-40500.00', 'Return', '', '', '2019-08-01', 1, ''),
(25, '20190731114458', 'KRQGUNN4ZMS3WT8I2RK9', 'VCSJF5N5KO', NULL, '-300000.00', 'Return', '', '', '2019-08-01', 1, ''),
(26, 'PM-2', 'WPXXU7UC1Z4ID1HUVU53', NULL, 'PM-2', '200.00', 'Paid to Test Supplier', '1', '', '2019-08-01', 1, 'd'),
(27, 'WCAX81XD3R', 'WPXXU7UC1Z4ID1HUVU53', NULL, NULL, '1000.00', 'Advance ', 'NA', 'NA', '2019-08-03', 1, 'd'),
(28, 'LRRQ4L5FO1', 'E4ZLUXYXX6GJU3HT2DQ8', 'Adjustment ', NULL, '10.00', 'Previous adjustment with software', 'NA', 'NA', '2019-08-03', 1, 'c'),
(29, 'Z9IYRG1MRI', '1H6YPZJIJIB7YW7TDM7Y', 'Adjustment ', NULL, '1.00', 'Previous adjustment with software', 'NA', 'NA', '2019-08-03', 1, 'c'),
(37, '5OMZ3G1GUN', 'QUT7N3K6WOTU6BXRD8BL', NULL, NULL, '232.00', 'Advance ', 'NA', 'NA', '2019-08-06', 1, 'd'),
(38, 'L26HEBCZJM', 'QUT7N3K6WOTU6BXRD8BL', NULL, NULL, '232.00', 'Advance ', 'NA', 'NA', '2019-08-06', 1, 'd'),
(36, 'IF2K2MG62S', 'QUT7N3K6WOTU6BXRD8BL', NULL, NULL, '232.00', 'Advance ', 'NA', 'NA', '2019-08-06', 1, 'd'),
(126, 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-08-05', 1, 'd'),
(133, 'PM-7', '', NULL, 'PM-7', '0.00', 'Paid From Accounts', '1', '', '2019-09-15', 1, 'd'),
(132, 'PM-5', '2T6QHG45AP4M11FG6TPT', NULL, 'PM-5', '324234.00', 'Paid to shahabuddin', '1', '', '2019-09-05', 1, 'd'),
(124, '36VPJ77O5F', '6167UCENYO5JJITKRH21', 'Adjustment ', NULL, '0.00', 'Previous adjustment with software', 'NA', 'NA', '2019-09-05', 1, 'c'),
(125, 'PM-3', 'X', NULL, 'PM-3', '1.00', 'Paid From Accounts', '1', '', '2019-08-05', 1, 'd'),
(127, 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-09-05', 1, 'd'),
(128, 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-09-05', 1, 'd'),
(129, 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-09-05', 1, 'd'),
(130, 'PM-3', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-3', '100.00', 'Paid From Accounts', '1', '', '2019-09-05', 1, 'd'),
(131, 'PM-4', 'XUQB7JFFJSFXKGOZ9VD3', NULL, 'PM-4', '200.00', 'Paid to isahaq', '1', '', '2019-09-05', 1, 'd'),
(134, '20190924143851', 'XUQB7JFFJSFXKGOZ9VD3', '123123', NULL, '4500.00', 'dfsdf', '', '', '2019-09-24', 1, 'c'),
(135, '20190924143851', 'XUQB7JFFJSFXKGOZ9VD3', '123123', NULL, '4500.00', 'Purchase From Supplier. dfsdf', '', '', '2019-09-24', 1, 'd'),
(136, '20190925103121', 'XUQB7JFFJSFXKGOZ9VD3', '1233', NULL, '3300.00', 'fdgsdf', '', '', '2019-09-25', 1, 'c'),
(137, '20190925103121', 'XUQB7JFFJSFXKGOZ9VD3', '1233', NULL, '3300.00', 'Purchase From Supplier. fdgsdf', '', '', '2019-09-25', 1, 'd');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_product`
--

CREATE TABLE `supplier_product` (
  `supplier_pr_id` int(11) NOT NULL,
  `product_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `products_model` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `supplier_id` varchar(30) CHARACTER SET latin1 NOT NULL,
  `supplier_price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `supplier_product`
--

INSERT INTO `supplier_product` (`supplier_pr_id`, `product_id`, `products_model`, `supplier_id`, `supplier_price`) VALUES
(1, '42156427', 'Test Model', 'XUQB7JFFJSFXKGOZ9VD3', 90),
(2, '33845288', 'sdfsa', 'XUQB7JFFJSFXKGOZ9VD3', 110);

-- --------------------------------------------------------

--
-- Table structure for table `synchronizer_setting`
--

CREATE TABLE `synchronizer_setting` (
  `id` int(11) NOT NULL,
  `hostname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `port` varchar(10) NOT NULL,
  `debug` varchar(10) NOT NULL,
  `project_root` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tax_collection`
--

CREATE TABLE `tax_collection` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `customer_id` varchar(30) NOT NULL,
  `relation_id` varchar(30) NOT NULL,
  `tax0` text,
  `tax1` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_collection`
--

INSERT INTO `tax_collection` (`id`, `date`, `customer_id`, `relation_id`, `tax0`, `tax1`) VALUES
(1, '2019-06-25', 'XWCEM9UVV5NAT5R', '3498244447', NULL, NULL),
(2, '2019-06-25', '6FGVC6K7OX5L9Z1', '4477997231', NULL, NULL),
(3, '2019-06-25', '6FGVC6K7OX5L9Z1', '3973611118', NULL, NULL),
(4, '2019-06-25', '6FGVC6K7OX5L9Z1', '3923586588', NULL, NULL),
(5, '2019-06-26', 'XWCEM9UVV5NAT5R', '6575276926', NULL, NULL),
(6, '2019-06-26', '6FGVC6K7OX5L9Z1', '6643494915', NULL, NULL),
(7, '2019-06-26', '6FGVC6K7OX5L9Z1', '1969912464', NULL, NULL),
(8, '2019-06-26', '6FGVC6K7OX5L9Z1', '6691959977', NULL, NULL),
(9, '2019-06-26', '6FGVC6K7OX5L9Z1', '2258111339', NULL, NULL),
(10, '2019-06-26', '6FGVC6K7OX5L9Z1', '9624435359', NULL, NULL),
(11, '2019-06-26', '6FGVC6K7OX5L9Z1', '8668653724', NULL, NULL),
(12, '2019-06-26', '6FGVC6K7OX5L9Z1', '8558184696', NULL, NULL),
(13, '2019-06-26', '6FGVC6K7OX5L9Z1', '9147771963', NULL, NULL),
(14, '2019-06-26', 'FYJ4EEBMOGTW25T', '3169824785', NULL, NULL),
(15, '2019-06-26', '6FGVC6K7OX5L9Z1', '3918198168', NULL, NULL),
(16, '2019-06-26', 'PVGGNNSWOESP45P', '5219125479', NULL, NULL),
(17, '2019-06-26', 'PVGGNNSWOESP45P', '8394724961', NULL, NULL),
(18, '2019-06-27', '6FGVC6K7OX5L9Z1', '7986446844', NULL, NULL),
(19, '2019-06-27', '6FGVC6K7OX5L9Z1', '6789217583', NULL, NULL),
(20, '2019-06-27', '6BPL9FJR54PBZJE', '4978495577', NULL, NULL),
(21, '2019-06-28', 'KFTUKN7RY1O2W6R', '2687213589', NULL, NULL),
(22, '2019-06-28', '6FGVC6K7OX5L9Z1', '3378453241', NULL, NULL),
(23, '2019-06-28', '6FGVC6K7OX5L9Z1', '4458233386', NULL, NULL),
(24, '2019-06-28', '6FGVC6K7OX5L9Z1', '1114135497', NULL, NULL),
(25, '2019-06-28', '6FGVC6K7OX5L9Z1', '4441511736', NULL, NULL),
(26, '2019-06-28', '6FGVC6K7OX5L9Z1', '8754679919', NULL, NULL),
(27, '2019-06-28', '6FGVC6K7OX5L9Z1', '3378767423', NULL, NULL),
(28, '2019-06-29', '6FGVC6K7OX5L9Z1', '9317933378', NULL, NULL),
(29, '2019-06-29', '6FGVC6K7OX5L9Z1', '1797561487', NULL, NULL),
(30, '2019-06-30', '6FGVC6K7OX5L9Z1', '9438755655', NULL, NULL),
(31, '2019-06-30', '6FGVC6K7OX5L9Z1', '5919433886', NULL, NULL),
(32, '2019-07-01', '6FGVC6K7OX5L9Z1', '2875718763', NULL, NULL),
(33, '2019-07-02', '6FGVC6K7OX5L9Z1', '1749144575', NULL, NULL),
(34, '2019-07-02', '6FGVC6K7OX5L9Z1', '9815582964', NULL, NULL),
(35, '2019-07-02', '6FGVC6K7OX5L9Z1', '8493678614', NULL, NULL),
(36, '2019-07-31', '6FGVC6K7OX5L9Z1', '9691144514', NULL, NULL),
(37, '2019-07-31', '6FGVC6K7OX5L9Z1', '5457199982', NULL, NULL),
(38, '2019-07-31', '6FGVC6K7OX5L9Z1', '2362693843', NULL, NULL),
(40, '2019-07-31', '6FGVC6K7OX5L9Z1', '5763389422', NULL, NULL),
(41, '2019-08-01', '6FGVC6K7OX5L9Z1', '6921223925', NULL, NULL),
(42, '2019-08-01', '6FGVC6K7OX5L9Z1', '2992478698', NULL, NULL),
(43, '2019-08-01', '6FGVC6K7OX5L9Z1', '2429117215', NULL, NULL),
(44, '2019-08-01', '6FGVC6K7OX5L9Z1', '8636821614', NULL, NULL),
(45, '2019-08-01', '6FGVC6K7OX5L9Z1', '2274125738', NULL, NULL),
(46, '2019-08-01', '6FGVC6K7OX5L9Z1', '6881727828', NULL, NULL),
(47, '2019-08-03', '6FGVC6K7OX5L9Z1', '6942233633', NULL, NULL),
(48, '2019-08-03', '6FGVC6K7OX5L9Z1', '7167684116', NULL, NULL),
(49, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'MBJEQBLEDT', NULL, NULL),
(50, '2019-08-27', 'BIXQAYJ3XDFBYZN', '3OXFJU9FLE', NULL, NULL),
(51, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'AEYS0XT6FB', NULL, NULL),
(52, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'IFU2EJUMSR', NULL, NULL),
(53, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'TD6SQ1IDPR', NULL, NULL),
(54, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'B8P4EGGRAY', NULL, NULL),
(55, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'R8OMFVYJXQ', NULL, NULL),
(56, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'VYMH47RI7Y', NULL, NULL),
(57, '2019-08-27', 'BIXQAYJ3XDFBYZN', '3FQSMSSQDT', NULL, NULL),
(58, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'JTXNMIW0DT', NULL, NULL),
(59, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'TJSXGFD1F6', NULL, NULL),
(60, '2019-08-27', 'BIXQAYJ3XDFBYZN', '1AGGWW2DR6', NULL, NULL),
(61, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'FO9Q0CZBAH', NULL, NULL),
(62, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'CYN1QFPGZE', NULL, NULL),
(63, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'QE2YQFXLDA', NULL, NULL),
(64, '2019-08-27', 'BIXQAYJ3XDFBYZN', 'JJ75B2XUKG', NULL, NULL),
(65, '2019-09-19', 'BIXQAYJ3XDFBYZN', '5147385423', '0.00', '0.00'),
(66, '2019-09-21', '6FGVC6K7OX5L9Z1', '5928235575', '0.00', '0.00'),
(67, '2019-09-21', '6FGVC6K7OX5L9Z1', '6852499599', '0.00', '0.00'),
(68, '2019-09-21', 'BIXQAYJ3XDFBYZN', '7348638525', '0.00', '0.00'),
(69, '2019-09-21', '6FGVC6K7OX5L9Z1', '6979694511', '0.00', '0.00'),
(70, '2019-09-21', '6FGVC6K7OX5L9Z1', '7675268361', '0.00', '0.00'),
(71, '2019-09-21', '6FGVC6K7OX5L9Z1', '1699845521', '0.00', '0.00'),
(72, '2019-09-21', '6FGVC6K7OX5L9Z1', '3291158273', '0.00', '0.00'),
(73, '2019-09-21', '6FGVC6K7OX5L9Z1', '7884756971', '0.00', '0.00'),
(74, '2019-09-21', '6FGVC6K7OX5L9Z1', '9836928819', '0.00', '0.00'),
(75, '2019-09-21', '6FGVC6K7OX5L9Z1', '2466279899', '0.00', '0.00'),
(76, '2019-09-24', '6FGVC6K7OX5L9Z1', '7126396398', '5.00', '9.90'),
(77, '2019-09-26', '6FGVC6K7OX5L9Z1', '3496522264', '1.00', '1.98'),
(78, '2019-09-26', '6FGVC6K7OX5L9Z1', '1962163959', '1.00', '1.98');

-- --------------------------------------------------------

--
-- Table structure for table `tax_information`
--

CREATE TABLE `tax_information` (
  `tax_id` varchar(15) NOT NULL,
  `tax` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tax_settings`
--

CREATE TABLE `tax_settings` (
  `id` int(11) NOT NULL,
  `default_value` float NOT NULL,
  `tax_name` varchar(250) NOT NULL,
  `nt` int(11) NOT NULL,
  `reg_no` varchar(100) NOT NULL,
  `is_show` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tax_settings`
--

INSERT INTO `tax_settings` (`id`, `default_value`, `tax_name`, `nt`, `reg_no`, `is_show`) VALUES
(1, 1, 'Vat', 2, '745878', 1),
(2, 2, 'Tax', 2, '879789', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transection`
--

CREATE TABLE `transection` (
  `id` int(20) NOT NULL,
  `transaction_id` varchar(30) NOT NULL,
  `date_of_transection` varchar(30) NOT NULL,
  `transection_type` varchar(30) NOT NULL,
  `transection_category` varchar(30) NOT NULL,
  `transection_mood` varchar(25) NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pay_amount` decimal(10,2) DEFAULT '0.00',
  `description` varchar(255) NOT NULL,
  `relation_id` varchar(30) NOT NULL,
  `is_transaction` int(2) NOT NULL COMMENT '0 = invoice and 1 = transaction'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transection`
--

INSERT INTO `transection` (`id`, `transaction_id`, `date_of_transection`, `transection_type`, `transection_category`, `transection_mood`, `amount`, `pay_amount`, `description`, `relation_id`, `is_transaction`) VALUES
(1, 'FS93V8F75ICGLDK', '2019-06-25', '2', '2', '1', '200.00', '0.00', 'Paid by customer', 'XWCEM9UVV5NAT5R', 0),
(2, 'Q3IZ9DRCOSZ768G', '2019-06-25', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(3, 'QO6WH5CIWCPJ6Q1', '2019-06-25', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(4, 'S67BGX3BCKB5VDV', '2019-06-25', '2', '2', '1', '600.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(5, 'F1QPDTOHWZ16P9P', '2019-06-26', '2', '2', '1', '200.00', '0.00', 'Paid by customer', 'XWCEM9UVV5NAT5R', 0),
(6, 'YYMQYK93V1ZM516', '2019-06-26', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(7, 'FE7CRWNMHEY38MD', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(8, 'AMUTK48CY2VYX8L', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(9, 'HVFX6UG8VG2NR6K', '2019-06-26', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(10, 'KWTMIGM4DFV4MMN', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(11, 'O6D1W158OH1A4SO', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(12, 'ZZGD9XYMVFUFHET', '2019-06-26', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(13, 'XPE32XHWMR1IIOD', '2019-06-26', '2', '2', '1', '450.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(14, 'Y2GQTYTL9QPDS3S', '2019-06-26', '2', '2', '1', '800.00', '0.00', 'Paid by customer', 'FYJ4EEBMOGTW25T', 0),
(15, 'DIAUX4GUIHBKXM2', '2019-06-26', '2', '2', '1', '1450.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(16, '5ES9981RI25GKC4', '2019-06-26', '2', '2', '1', '450.00', '0.00', 'Paid by customer', 'PVGGNNSWOESP45P', 0),
(17, 'DI85CJ5RMXQD7HB', '2019-06-26', '2', '2', '1', '1450.00', '0.00', 'Paid by customer', 'PVGGNNSWOESP45P', 0),
(18, 'GUYXF9TXVLAJJEQ', '2019-06-27', '2', '2', '1', '800.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(19, '9P35ZCUMLIEJFWO', '2019-06-27', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(20, 'RDMKLIJFJCJSXPT', '2019-06-27', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6BPL9FJR54PBZJE', 0),
(21, 'H2INB5JLI83B2JZ', '2019-06-28', '2', '2', '1', '30000.00', '0.00', 'Paid by customer', 'KFTUKN7RY1O2W6R', 0),
(22, 'MSAGGFLPOJDKQII', '2019-06-28', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(23, 'STD9RZ3H1JBXP5R', '2019-06-28', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(24, 'UNSJVY7KRG63XFU', '2019-06-28', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(25, '2FA13JON5GQ6T9K', '2019-06-28', '2', '2', '1', '2210.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(26, 'VP9EK1NHG955C8E', '2019-06-28', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(27, 'EFZMHVZOZIHL9X9', '2019-06-28', '2', '2', '1', '50000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(28, 'FUFHSCJATY2LHY4', '2019-06-29', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(29, 'OFTJDX79S6JKXT5', '2019-06-29', '2', '2', '1', '36000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(30, 'YKFQ14NNAGXFATU', '2019-06-30', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(31, '126OOJOQE2XEQ26', '2019-06-30', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(32, 'B1CY534MOESSW5C', '2019-07-01', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(33, '2V7IRO1C13I2MDW', '2019-07-02', '2', '2', '1', '200.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(34, 'GFVMOPDBU2EB87U', '2019-07-02', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(35, 'SBW99OEQ3LR73IG', '2019-07-02', '2', '2', '1', '455.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(36, 'PZBG4F2CP4ZVSP8', '2019-07-31', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(37, '8SA4NBY3SH62KMW', '2019-07-31', '2', '2', '1', '195465.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(38, 'E64ODCUPGHL57QG', '2019-07-31', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(40, 'RZVZUC2CK73MFHK', '2019-07-31', '2', '2', '1', '455.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(41, 'UH22TNP1RNBWZMU', '2019-08-01', '2', '2', '1', '50455.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(42, '2FPNZL5FLJRFEY4', '2019-08-01', '2', '2', '1', '50655.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(43, 'YP2TDHXDFT7PQ56', '2019-08-01', '2', '2', '1', '51855.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(44, 'YCJUPGV3B5MBKQX', '2019-08-01', '2', '2', '1', '85000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(45, 'DO95YM46XX8JQRL', '2019-08-01', '2', '2', '1', '50000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(46, 'Z1PT7RD61Z4MZ3I', '2019-08-01', '2', '2', '1', '455.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(47, '37P7PM8L72D6ORF', '2019-08-03', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(48, '4D6FVY1AIMSJDKW', '2019-08-03', '2', '2', '1', '2600.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(49, '9SQL4HDK78GTX79', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(50, 'P14K3GNFMSCCLP3', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(51, 'OE3P77I88Q91OA4', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(52, 'S4TZR8MS4HYJO1C', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(53, 'KMW431HG76855QH', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(54, 'FQHWRZQDA26OVKK', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(55, '91CDZX4GY87FT9R', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(56, 'ZVMINW2RAKCCJ9K', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(57, 'FMSLB6U1LFMD8R8', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(58, '35EZAQVD9ORALB2', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(59, 'NGQ3TQ5IZN6X2EB', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(60, 'HVJJM7B256Y5T6B', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(61, 'MFLDBMSJWC3QXK2', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(62, '455YPJW2E7OEVEE', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(63, 'J3OSS89CCGIWBQQ', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(64, 'NC7539IIO4UVMEN', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(65, '3YHJ8EYDFZN7DCA', '2019-08-27', '2', '2', '1', '50.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(66, '7U8E91JYYA4GBEF', '2019/08/31', '2', '2', '1', '580.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(67, 'CBERCINUJUMHR83', '2019/08/31', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(68, 'RGBVSR1L1MX9OOB', '2019/08/31', '2', '2', '1', '0.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(69, '66YVTE7U13A8XTD', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(70, 'BCXJJU1D6UFEBPI', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(71, '4JWNCEWD6KRW1NF', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(72, '6BDDZXVNULKHG2G', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(73, 'EZJXGXR1FFX4J6Q', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(74, 'QNFG1QXHSO9E2Z1', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(75, 'MSFSBLBPLIECNHQ', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(76, 'Z5CVY71P199CZWX', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(77, 'DNDVCQQ3D5OWX9Q', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(78, '28YZ1ODJ98KMS3H', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(79, 'NVGRJWM9836VOWN', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(80, '78NB86JBG8CNGTY', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(81, 'C1QEH9LPOKFX2FK', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(82, 'SQOV2OFLA7PXTV8', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(83, 'U9S9NAFRL95MGRY', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(84, 'P3QEHEBLYFL8L71', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(85, '762KZOSIU1LAXZ8', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(86, '939OSHPFYWOOOYU', '2019-08-31', '2', '2', '1', '150.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(87, 'QSQ7K44IRXQQ2T1', '2019-08-31', '2', '2', '1', '150.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(88, 'KMNV6LBX9S255A1', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(89, '4B6ABYSTPNG8TXC', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(90, 'A855CI6XUZJQAKE', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(91, '43FXE59FSEPPIWW', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(92, 'NMMF9U8AKQD1QJE', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(93, 'CXYRSY7DS9WZN72', '2019-08-22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(94, 'O6D1QLW2P1FMOW6', '2019/08/22', '2', '2', '1', '345345.00', '0.00', 'Paid by customer', 'fgdfdsf', 0),
(95, 'LUMEF2QHGTNEIO7', '2019-08-31', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(96, 'ZI5UU3G5W8IE7GQ', '2019-08-31', '2', '2', '1', '150.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(97, '6FJ8HHODBNTISOQ', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(98, 'VOXTI35ACPWGQBP', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(99, 'HH6GBLCIJJ9ZQM2', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(100, 'DR9WUTCVR8TM88W', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(101, 'TB4TZKKD9Z1DU7N', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(102, 'EWE127EPH5N5QZY', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(103, 'GZ9KC2KCUIGHT5Z', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(104, '1NBHNBURBZX9547', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(105, 'X6IKQC8LDQG7VL7', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(106, 'WZF1YKA1WQ2S3SU', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(107, 'ETDT9FUGDVM8O14', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(108, 'K5MFK1FTDELQEFC', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(109, '1R8DOQDR74PEWMF', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(112, 'SI7T2A7UM5A5ZZW', '2019-08-31', '2', '2', '1', '50.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(146, 'UG4ONQNHFXFZJH9', '2019-09-19', '2', '2', '1', '400.00', '0.00', 'Paid by customer', '93K7WTVUD86LBTC', 0),
(147, '4FC3T1CMFY65B2T', '2019-09-19', '2', '2', '1', '355.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(148, '3BWOZ61PSP6P3R6', '2019-09-19', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(149, '6F3CVEXN8F6L7OJ', '2019-09-19', '2', '2', '1', '580.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(150, '6XMHQKVX3LDM22E', '2019-09-19', '2', '2', '1', '175900.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(151, 'F8ZQIVM5WVILDF2', '2019-09-21', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(152, 'SEY7BE7IMACFKIZ', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(153, 'YQK988EWI6LVZL5', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(154, 'CZXTX9S9BARD9J4', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', 'BIXQAYJ3XDFBYZN', 0),
(155, 'U6DCWDWEU75XH7P', '2019-09-21', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(156, 'R915QIBP9J2SPZF', '2019-09-21', '2', '2', '1', '500.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(157, 'VHWMMJ6IPKFR2NP', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(158, 'SHQV7VGBJM88D52', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(159, 'SC6U7J6V3H4S8SC', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(160, '2L1ZNYLEB91PUP5', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(161, 'CVWYE5KJL3D22O4', '2019-09-21', '2', '2', '1', '35000.00', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(162, 'AGMXPFZLB5B3EO7', '2019-09-24', '2', '2', '1', '514.90', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(163, 'TIPZHE21WSRO5NF', '2019-09-26', '2', '2', '1', '102.98', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0),
(164, 'C8RMSIZMIGUMKGZ', '2019-09-26', '2', '2', '1', '102.98', '0.00', 'Paid by customer', '6FGVC6K7OX5L9Z1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `unit_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `unit_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`unit_id`, `unit_name`, `status`) VALUES
('XD2A5GLQWBMK8S3', 'gfdgdfg', 1),
('NRH49M29VCLDU1W', 'demos', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_id` varchar(15) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `gender` int(2) NOT NULL,
  `date_of_birth` varchar(255) NOT NULL,
  `logo` varchar(250) DEFAULT NULL,
  `status` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `last_name`, `first_name`, `gender`, `date_of_birth`, `logo`, `status`) VALUES
(1, '1', 'User', 'Admin', 0, '', 'http://softest8.bdtask.com/Updatesaleserp/assets/dist/img/profile_picture/a7ccdb3d01647307a02e5bd80ca89ac9.jpg', 1),
(2, 'bs1wW4rklPlBwOA', 'khan', 'boss', 0, '', NULL, 1),
(3, 'wwjwSIbEuQ4S6sD', 'bdtask', 'tanzil', 0, '', NULL, 1),
(4, 'QfAvRrLHragkSYs', 'ovi', 'isaq', 0, '', NULL, 1),
(5, 'eJGHZzMlqcM3d4x', 'ovi', 'isaq', 0, '', NULL, 1),
(6, 'hM90DNPq3VQsk9T', '', '', 0, '', NULL, 1),
(7, 'dYmgfQW7j5H8oc9', 'hi word', 'konir', 0, '', NULL, 1),
(8, 'LaoMKlz7SCRxhjD', 'Isahaq', 'Hm', 0, '', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `id` int(11) NOT NULL,
  `user_id` varchar(15) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` int(2) NOT NULL,
  `security_code` varchar(255) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`id`, `user_id`, `username`, `password`, `user_type`, `security_code`, `status`) VALUES
(1, '1', 'admin@example.com', '41d99b369894eb1ec3f461135132d8bb', 1, '001', 1),
(2, 'KAvUifzuUOhoUr4', 'hmisahaq01@gmail.com', '41d99b369894eb1ec3f461135132d8bb', 2, '190207062137', 1),
(3, '3vRVO2L8tsO5PP2', 'sabuj@gmail.com', '5ebe9dd4ea7517bd2c30bc46985ef823', 2, '', 1),
(4, 'bs1wW4rklPlBwOA', 'super@super.com', '41d99b369894eb1ec3f461135132d8bb', 2, '', 1),
(5, 'wwjwSIbEuQ4S6sD', 'tanzil4091@gmail.com', '41d99b369894eb1ec3f461135132d8bb', 1, '', 1),
(6, 'QfAvRrLHragkSYs', 'ovi@gmail.com', '41d99b369894eb1ec3f461135132d8bb', 1, '', 1),
(7, 'eJGHZzMlqcM3d4x', 'ovi1@gmail.com', '41d99b369894eb1ec3f461135132d8bb', 1, '', 1),
(8, 'hM90DNPq3VQsk9T', '', 'bbe587948a2490934834340b1b4c1643', 1, '', 1),
(9, 'dYmgfQW7j5H8oc9', 'kobirdo@gmail.com', '5ebe9dd4ea7517bd2c30bc46985ef823', 1, '', 1),
(10, 'LaoMKlz7SCRxhjD', 'hmisahaq@yahoo.com', '035e7767f3f2940b3a6b2817cbdbe9e6', 1, '001', 1);

-- --------------------------------------------------------

--
-- Table structure for table `web_setting`
--

CREATE TABLE `web_setting` (
  `setting_id` int(11) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `invoice_logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `timezone` varchar(150) NOT NULL,
  `currency_position` varchar(10) DEFAULT NULL,
  `footer_text` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `rtr` varchar(255) DEFAULT NULL,
  `captcha` int(11) DEFAULT '1' COMMENT '0=active,1=inactive',
  `site_key` varchar(250) DEFAULT NULL,
  `secret_key` varchar(250) DEFAULT NULL,
  `discount_type` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_setting`
--

INSERT INTO `web_setting` (`setting_id`, `logo`, `invoice_logo`, `favicon`, `currency`, `timezone`, `currency_position`, `footer_text`, `language`, `rtr`, `captcha`, `site_key`, `secret_key`, `discount_type`) VALUES
(1, 'http://softest8.bdtask.com/Updatesaleserp/./my-assets/image/logo/dae881d99e85a0cd176993e1f2915700.png', 'http://softest8.bdtask.com/Updatesaleserp/./my-assets/image/logo/b78063bf95d5a20a35cb9d708c01488e.png', 'http://softest8.bdtask.com/Updatesaleserp/my-assets/image/logo/e3b869167120139d49987ec0d863f2e1.png', '৳', 'Asia/Dhaka', '0', 'Copyright© 2018-2019 Coder IT. All rights reserved.', 'english', '0', 1, '', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_2`
--
ALTER TABLE `account_2`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `acc_coa`
--
ALTER TABLE `acc_coa`
  ADD PRIMARY KEY (`HeadName`);

--
-- Indexes for table `acc_income_expence`
--
ALTER TABLE `acc_income_expence`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `acc_transaction`
--
ALTER TABLE `acc_transaction`
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`att_id`);

--
-- Indexes for table `currency_tbl`
--
ALTER TABLE `currency_tbl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_information`
--
ALTER TABLE `customer_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_ledger`
--
ALTER TABLE `customer_ledger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_history`
--
ALTER TABLE `employee_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_salary_payment`
--
ALTER TABLE `employee_salary_payment`
  ADD PRIMARY KEY (`emp_sal_pay_id`);

--
-- Indexes for table `employee_salary_setup`
--
ALTER TABLE `employee_salary_setup`
  ADD PRIMARY KEY (`e_s_s_id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_item`
--
ALTER TABLE `expense_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `payroll_tax_setup`
--
ALTER TABLE `payroll_tax_setup`
  ADD PRIMARY KEY (`tax_setup_id`);

--
-- Indexes for table `personal_loan`
--
ALTER TABLE `personal_loan`
  ADD PRIMARY KEY (`per_loan_id`);

--
-- Indexes for table `pesonal_loan_information`
--
ALTER TABLE `pesonal_loan_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_information`
--
ALTER TABLE `product_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_purchase`
--
ALTER TABLE `product_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_purchase_details`
--
ALTER TABLE `product_purchase_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_service`
--
ALTER TABLE `product_service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_module_id` (`fk_module_id`),
  ADD KEY `fk_user_id` (`role_id`);

--
-- Indexes for table `salary_sheet_generate`
--
ALTER TABLE `salary_sheet_generate`
  ADD PRIMARY KEY (`ssg_id`);

--
-- Indexes for table `salary_type`
--
ALTER TABLE `salary_type`
  ADD PRIMARY KEY (`salary_type_id`);

--
-- Indexes for table `sec_role`
--
ALTER TABLE `sec_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sec_userrole`
--
ALTER TABLE `sec_userrole`
  ADD UNIQUE KEY `ID` (`id`);

--
-- Indexes for table `service_invoice`
--
ALTER TABLE `service_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_invoice_details`
--
ALTER TABLE `service_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_settings`
--
ALTER TABLE `sms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_module`
--
ALTER TABLE `sub_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_information`
--
ALTER TABLE `supplier_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_ledger`
--
ALTER TABLE `supplier_ledger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_product`
--
ALTER TABLE `supplier_product`
  ADD PRIMARY KEY (`supplier_pr_id`);

--
-- Indexes for table `tax_collection`
--
ALTER TABLE `tax_collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_settings`
--
ALTER TABLE `tax_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transection`
--
ALTER TABLE `transection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_2`
--
ALTER TABLE `account_2`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acc_income_expence`
--
ALTER TABLE `acc_income_expence`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acc_transaction`
--
ALTER TABLE `acc_transaction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=947;

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `att_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency_tbl`
--
ALTER TABLE `currency_tbl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer_information`
--
ALTER TABLE `customer_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `customer_ledger`
--
ALTER TABLE `customer_ledger`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_history`
--
ALTER TABLE `employee_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_salary_payment`
--
ALTER TABLE `employee_salary_payment`
  MODIFY `emp_sal_pay_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_salary_setup`
--
ALTER TABLE `employee_salary_setup`
  MODIFY `e_s_s_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `expense_item`
--
ALTER TABLE `expense_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=809;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payroll_tax_setup`
--
ALTER TABLE `payroll_tax_setup`
  MODIFY `tax_setup_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_loan`
--
ALTER TABLE `personal_loan`
  MODIFY `per_loan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pesonal_loan_information`
--
ALTER TABLE `pesonal_loan_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_information`
--
ALTER TABLE `product_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_purchase`
--
ALTER TABLE `product_purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `product_purchase_details`
--
ALTER TABLE `product_purchase_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `product_service`
--
ALTER TABLE `product_service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1515;

--
-- AUTO_INCREMENT for table `salary_sheet_generate`
--
ALTER TABLE `salary_sheet_generate`
  MODIFY `ssg_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_type`
--
ALTER TABLE `salary_type`
  MODIFY `salary_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sec_role`
--
ALTER TABLE `sec_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sec_userrole`
--
ALTER TABLE `sec_userrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `service_invoice`
--
ALTER TABLE `service_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_invoice_details`
--
ALTER TABLE `service_invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sms_settings`
--
ALTER TABLE `sms_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sub_module`
--
ALTER TABLE `sub_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `supplier_information`
--
ALTER TABLE `supplier_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `supplier_ledger`
--
ALTER TABLE `supplier_ledger`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `supplier_product`
--
ALTER TABLE `supplier_product`
  MODIFY `supplier_pr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tax_collection`
--
ALTER TABLE `tax_collection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `tax_settings`
--
ALTER TABLE `tax_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transection`
--
ALTER TABLE `transection`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
